# Challenge 2 - Dataset and Documentation

This repository contains useful information for challenge #2 "DSA RAG Race" participants, part of the joint PEReN/European Commission hackathon "Digital Services (h)Acked".
It will be held in Brussels on February, 2nd 2024.


## Challenge description
The aim will be to develop a tool that draws on open source language models to provide substantiated answers (including pre-processing code sometinmes) to questions related to platform data, particularly that made available via APIs.

## Documentation
We'll regularly add documentation to this repository. For now you can find basic instructions to login to your Jean Zay account via our servers.

## Dataset
The train dataset is located in `dataset/train`. It contains 2 folders:
- `input`: with thea single file `questions.csv` that lists all questions;
- `output`: which contains two subfolders `sources` and `answers`. Each of these folders contains one file per question identified by the associated question id (`answers/123.txt` means that it's the answer file for question 123). Sometimes multiple answers are available for a question, then they will be listed in separate text files (for instance `answers/123/123-0.txt` and `answers/123/123-1.txt`).

## Evaluation
Evaluation criteria are listed in the [Rules](https://hackathon.peren.fr/hackathon2024_challenge2_v2501-EN.6267f634.pdf) (see Article 7)

## Need help?
Please contact our team at `hackathon2024.peren [at] finances.gouv.fr`