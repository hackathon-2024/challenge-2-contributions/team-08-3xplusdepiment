# Resource URL: https://www.linkedin.com/ad-library/
Ad Library
==========

LinkedIn’s Ad Library offers transparency in advertising by providing a searchable collection of ads.

Frequently asked questions
--------------------------

### What is the Ad Library and how do I use it?

The Ad Library is a place where you can search for ads that have run on LinkedIn. Ads remain in the Ad Library for one year after their last impression on LinkedIn.

### Do I need a LinkedIn account to use the Ad Library?

Anyone can explore the Ad Library, with or without a LinkedIn account.

### What information is shown in the Ad Library?

As part of our commitment to creating a safe and trusted ad experience, the Ad Library includes basic information about ads served on LinkedIn, including the advertiser, ad format and ad creative. For ads targeted to the EU, the Ad Library includes additional information, including information about ad impressions, ad targeting and dates the ad ran.

### What are all the ad formats supported by LinkedIn?

To learn more about the ad formats available on LinkedIn, see the [LinkedIn Ads Guide](https://business.linkedin.com/marketing-solutions/success/ads-guide).

### How long does it take for an ad to be available on the Ad Library?

An ad will typically appear in the Ad Library results within 24-48 hours from the time it gets its first impression. Any changes or updates made to an ad will also typically be reflected within 24-48 hours.

### How far back can I search ads?

The Ad Library allows users to search ads that were created on or after June 1, 2023.

### How are dates and times recorded in the Ad Library?

All dates and timestamps are recorded in Coordinated Universal Time (UTC).

### How are ad start dates and end dates determined in the Ad Library?

The ad start date is determined when the first impression is delivered, while the ad end date is determined when the last impression is delivered.

### How is ad targeting in the Ad Library determined?

For ads targeted to the EU, the Ad Library shows the top three targeting parameters selected by the advertiser in when targeting their ad. [Learn more](https://www.linkedin.com/help/lms/answer/a1517918).

### Can I opt out from my ads showing up in the Ad Library?

As part of our commitment to creating a safe and trusted ad experience, the Ad Library includes basic information about ads that have run on LinkedIn. Advertisers are not able to opt out of ads showing in the Ad Library.

### I have a question not answered here. Where can I find help?

You can reach out to our support team [here](https://www.linkedin.com/help/lms/ask).