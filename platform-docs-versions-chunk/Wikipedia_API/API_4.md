platform: Wikipedia
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Wikipedia_API/API.md
url: https://www.mediawiki.org/wiki/API:Main_page


### Getting started with MediaWiki Action API

Before you start using the MediaWiki Action API, you should review the following pages:

* [API etiquette and usage guidelines](https://www.mediawiki.org/wiki/Special:MyLanguage/API:Etiquette "Special:MyLanguage/API:Etiquette")
* [Frequently asked questions](https://www.mediawiki.org/wiki/Special:MyLanguage/API:FAQ "Special:MyLanguage/API:FAQ")
* [Input and output formats](https://www.mediawiki.org/wiki/Special:MyLanguage/API:Data_formats "Special:MyLanguage/API:Data formats")
* [Errors and warnings](https://www.mediawiki.org/wiki/Special:MyLanguage/API:Errors_and_warnings "Special:MyLanguage/API:Errors and warnings")
* Any policies that apply to the wiki you want to access, such as Wikimedia Foundation wikis' [terms of use](https://foundation.wikimedia.org/wiki/Special:MyLanguage/Terms_of_Use "wmf:Special:MyLanguage/Terms of Use") and [trademark policy](https://foundation.wikimedia.org/wiki/Special:MyLanguage/trademark_policy "wmf:Special:MyLanguage/trademark policy"). These terms apply to you when you access or edit using the API, just as they do when you use your web browser.