platform: Wikipedia
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Wikipedia_API/API.md
url: https://www.mediawiki.org/wiki/API:Main_page

## API documentation

* * *

|     |     |
| --- | --- |
| [![](//upload.wikimedia.org/wikipedia/commons/thumb/0/00/Oxygen480-categories-preferences-system.svg/20px-Oxygen480-categories-preferences-system.svg.png)](https://www.mediawiki.org/wiki/File:Oxygen480-categories-preferences-system.svg) | The following documentation is the output of [Special:ApiHelp/main](https://www.mediawiki.org/wiki/Special:ApiHelp/main "Special:ApiHelp/main"), automatically generated by the pre-release version of MediaWiki that is running on this site (MediaWiki.org). |