platform: Wikipedia
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Wikipedia_API/API.md
url: https://www.mediawiki.org/wiki/API:Main_page

## Introduction

The MediaWiki Action API is a [web service](https://en.wikipedia.org/wiki/Web_service "w:Web service") that allows access to some wiki features like authentication, page operations, and search. It can provide [meta information](https://www.mediawiki.org/wiki/Special:MyLanguage/API:Meta "Special:MyLanguage/API:Meta") about the wiki and the logged-in user.

### Uses for the MediaWiki Action API

* Monitor a MediaWiki installation
* [Create a bot](https://en.wikipedia.org/wiki/Wikipedia:Creating_a_bot "w:Wikipedia:Creating a bot") to maintain a MediaWiki installation
* Log in to a wiki, access data, and post changes by making HTTP requests to the web service