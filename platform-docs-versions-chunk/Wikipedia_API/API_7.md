platform: Wikipedia
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Wikipedia_API/API.md
url: https://www.mediawiki.org/wiki/API:Main_page


## Other APIs

If you do not find what you are looking for in this API documentation, there are many other APIs related to Wikimedia projects.

For the REST API included with MediaWiki 1.35 and later, see [API:REST API](https://www.mediawiki.org/wiki/Special:MyLanguage/API:REST_API "Special:MyLanguage/API:REST API") [](https://www.mediawiki.org/wiki/API:REST_API "API:REST API").

|     |     |     |     |
| --- | --- | --- | --- |This table: [view](https://www.mediawiki.org/wiki/Template:API_comparison "Template:API comparison") **·** [talk](https://www.mediawiki.org/wiki/Template_talk:API_comparison) **·** [edit](https://www.mediawiki.org/w/index.php?title=Template:API_comparison&action=edit)
| API | Availability | URL base | Example |
| [![](//upload.wikimedia.org/wikipedia/commons/thumb/c/c6/MediaWiki-2020-small-icon.svg/18px-MediaWiki-2020-small-icon.svg.png)](https://www.mediawiki.org/wiki/File:MediaWiki-2020-small-icon.svg) ****[MediaWiki Action API](https://www.mediawiki.org/wiki/Special:MyLanguage/API:Main_page "Special:MyLanguage/API:Main page")**** | Included with MediaWiki<br><br>Enabled on [Wikimedia projects](https://meta.wikimedia.org/wiki/Special:MyLanguage/Wikimedia_projects "m:Special:MyLanguage/Wikimedia projects") | /api.php | [https://en.wikipedia.org/w/api.php?action=query&prop=info&titles=Earth](https://en.wikipedia.org/w/api.php?action=query&prop=info&titles=Earth) |
| [![](//upload.wikimedia.org/wikipedia/commons/thumb/c/c6/MediaWiki-2020-small-icon.svg/18px-MediaWiki-2020-small-icon.svg.png)](https://www.mediawiki.org/wiki/File:MediaWiki-2020-small-icon.svg) **[MediaWiki REST API](https://www.mediawiki.org/wiki/Special:MyLanguage/API:REST_API "Special:MyLanguage/API:REST API")[](https://www.mediawiki.org/wiki/API:REST_API "API:REST API")** | Included with MediaWiki 1.35+<br><br>Enabled on [Wikimedia projects](https://meta.wikimedia.org/wiki/Special:MyLanguage/Wikimedia_projects "m:Special:MyLanguage/Wikimedia projects") | /rest.php | [https://en.wikipedia.org/w/rest.php/v1/page/Earth](https://en.wikipedia.org/w/rest.php/v1/page/Earth) |
| [![](//upload.wikimedia.org/wikipedia/commons/thumb/8/81/Wikimedia-logo.svg/18px-Wikimedia-logo.svg.png)](https://www.mediawiki.org/wiki/File:Wikimedia-logo.svg) **[Wikimedia REST API](https://www.mediawiki.org/wiki/Special:MyLanguage/Wikimedia_REST_API "Special:MyLanguage/Wikimedia REST API")[](https://www.mediawiki.org/wiki/Wikimedia_REST_API "Wikimedia REST API")** | Not included with MediaWiki<br><br>Available for [Wikimedia projects](https://meta.wikimedia.org/wiki/Special:MyLanguage/Wikimedia_projects "m:Special:MyLanguage/Wikimedia projects") only | /api/rest | [https://en.wikipedia.org/api/rest\_v1/page/title/Earth](https://en.wikipedia.org/api/rest_v1/page/title/Earth) |
| [![](//upload.wikimedia.org/wikipedia/commons/thumb/8/84/Wikimedia_Enterprise_squirrel.svg/18px-Wikimedia_Enterprise_squirrel.svg.png)](https://www.mediawiki.org/wiki/File:Wikimedia_Enterprise_squirrel.svg) For commercial-scale APIs for Wikimedia projects, see **[Wikimedia Enterprise](https://www.mediawiki.org/wiki/Special:MyLanguage/Wikimedia_Enterprise "Special:MyLanguage/Wikimedia Enterprise")[](https://www.mediawiki.org/wiki/Wikimedia_Enterprise "Wikimedia Enterprise")** |     |     |     |