platform: Wikipedia
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Wikipedia_API/API.md
url: https://www.mediawiki.org/wiki/API:Main_page

## Code stewardship

* Maintained by [API Platform Team](https://www.mediawiki.org/wiki/API_Platform_Team "API Platform Team").
* Live chat ([IRC](https://www.mediawiki.org/wiki/Special:MyLanguage/MediaWiki_on_IRC "Special:MyLanguage/MediaWiki on IRC")): [#mediawiki-core](ircs://irc.libera.chat:6697/mediawiki-core) [connect](https://web.libera.chat/?channel=#mediawiki-core)
* Issue tracker: [Phabricator MediaWiki-Action-API](https://phabricator.wikimedia.org/tag/mediawiki-action-api/) ([Report an issue](https://phabricator.wikimedia.org/maniphest/task/edit/form/1/?projects=mediawiki-action-api))

![](https://login.wikimedia.org/wiki/Special:CentralAutoLogin/start?type=1x1)

Retrieved from "[https://www.mediawiki.org/w/index.php?title=API:Main\_page&oldid=6161467](https://www.mediawiki.org/w/index.php?title=API:Main_page&oldid=6161467)"