platform: Wikipedia
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Wikipedia_API/API.md
url: https://www.mediawiki.org/wiki/API:Main_page

### Endpoint

**All Wikimedia wikis have endpoints that follow this pattern:** `https://www.example.org/w/api.php`

|     |     |
| --- | --- |Examples of Wikimedia Wiki Endpoints
| API Endpoint | Wiki |
| `[https://www.mediawiki.org/w/api.php](https://www.mediawiki.org/w/api.php)` | MediaWiki API |
| `[https://meta.wikimedia.org/w/api.php](https://meta.wikimedia.org/w/api.php)` | Meta-Wiki API |
| `[https://en.wikipedia.org/w/api.php](https://en.wikipedia.org/w/api.php)` | English Wikipedia API |
| `[https://nl.wikipedia.org/w/api.php](https://nl.wikipedia.org/w/api.php)` | Dutch Wikipedia API |
| `[https://commons.wikimedia.org/w/api.php](https://commons.wikimedia.org/w/api.php)` | Wikimedia Commons API |
| `[https://test.wikipedia.org/w/api.php](https://test.wikipedia.org/w/api.php)` | Test Wiki API |

To see the endpoint URL on a particular wiki, see section "Entry point URLs" on the Special:Version page.