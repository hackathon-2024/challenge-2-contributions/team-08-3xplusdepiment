platform: Wikipedia
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Wikipedia_API/API.md
url: https://www.mediawiki.org/wiki/API:Main_page

## Quick Start

Get the contents of an article on English Wikipedia in HTML:

**[api.php?action=parse&page=Pet\_door&format=json](https://en.wikipedia.org/w/api.php?action=parse&page=Pet_door&format=json)** [\[try in ApiSandbox\]](https://en.wikipedia.org/wiki/Special:ApiSandbox#action=parse&page=Pet_door&format=json)