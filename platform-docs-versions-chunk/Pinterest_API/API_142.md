platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/board_sections/list)List board sections

Get a list of all board sections from a board owned by the "operation user\_account" - or a group board that has been shared with this account. Optional: Business Access: Specify an ad\_account\_id to use the owner of that ad\_account as the "operation user\_account".

* By default, the "operation user\_account" is the token user\_account.

ratelimit-category: org\_read

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`boards:read`)

##### path Parameters

|     |     |
| --- | --- |
| board\_id<br><br>required | string^\\d+$<br><br>Unique identifier of a board. |

##### query Parameters

|     |     |
| --- | --- |
| ad\_account\_id | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |
| bookmark | string<br><br>Cursor used to fetch the next page of items |
| page\_size | integer \[ 1 .. 250 \]<br><br>Default: 25<br><br>Maximum number of items to include in a single page of the response. See documentation on [Pagination](https://developers.pinterest.com/docs/getting-started/pagination/) for more information. |