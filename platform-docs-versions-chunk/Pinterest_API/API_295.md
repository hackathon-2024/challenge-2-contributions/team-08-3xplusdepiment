platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Response samples

* 200
* 403
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "items": [          * {                  * "lead_form_id": "383791336903426390",                      * "webhook_url": "[https://webhook.example.com/xyz](https://webhook.example.com/xyz)",                      * "id": "8078432025948590686",                      * "user_account_id": "549755885175",                      * "ad_account_id": "549755885176",                      * "api_version": "v5",                      * "cryptographic_key": "ucvxbV2Tdss0vNeYsdh4Qfa/1Khm2b0PqXvXeTTZh54",                      * "cryptographic_algorithm": "AES-256-GCM",                      * "created_time": 1699209842000                               }                   ],      * "bookmark": "string"       }`