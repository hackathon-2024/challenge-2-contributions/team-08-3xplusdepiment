platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

Success

**default**

Unexpected error

get/ad\_accounts/{ad\_account\_id}/customer\_lists

https://api.pinterest.com/v5/ad\_accounts/{ad\_account\_id}/customer\_lists

### Response samples

* 200
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "items": [          * {                  * "ad_account_id": "549756359984",                      * "created_time": 1452208622,                      * "id": "643",                      * "name": "The Glengarry Glen Ross leads",                      * "num_batches": 2,                      * "num_removed_user_records": 0,                      * "num_uploaded_user_records": 11,                      * "status": "PROCESSING",                      * "type": "customerlist",                      * "updated_time": 1461269616,                      * "exceptions": { }                               }                   ],      * "bookmark": "string"       }`