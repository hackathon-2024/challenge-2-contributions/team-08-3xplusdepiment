platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

## [](#operation/integrations_commerce/del)Delete commerce integration

Delete commerce integration metadata for the given external business ID. Note: If you're interested in joining the beta, please reach out to your Pinterest account manager.

ratelimit-category: ads\_write

sandbox: disabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:write`)

##### path Parameters

|     |     |
| --- | --- |
| external\_business\_id<br><br>required | string<br><br>External business ID for the integration. |

### Responses

**204**

Commerce Integration deleted successfully

**default**

Unexpected error.

delete/integrations/commerce/{external\_business\_id}

https://api.pinterest.com/v5/integrations/commerce/{external\_business\_id}

### Response samples

* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "code": 0,      * "message": "string"       }`