platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/ssio_insertion_order/edit)Edit insertion order through SSIO.

Edit insertion order through SSIO for `ad_account_id`.

* The token's user\_account must either be the Owner of the specified ad account, or have one of the necessary roles granted to them via [Business Access](https://help.pinterest.com/en/business/article/share-and-manage-access-to-your-ad-accounts): Admin, Finance, Campaign.

ratelimit-category: ads\_write

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:write`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |

##### Request Body schema: application/json

Order line to create.

|     |     |
| --- | --- |
| start\_date | string^(\\d{4})-(\\d{2})-(\\d{2})$<br><br>Starting date of time period. Format: YYYY-MM-DD |
| end\_date | string^(\\d{4})-(\\d{2})-(\\d{2})$<br><br>End date of time period. Format: YYYY-MM-DD |
| po\_number | string<br><br>The po number |
| budget\_amount | number<br><br>If Budget order line, the budget amount. |
| billing\_contact\_firstname | string<br><br>The billing contact first name |
| billing\_contact\_lastname | string<br><br>The billing contact last name |
| billing\_contact\_email | string<br><br>The billing contact email |
| media\_contact\_firstname | string<br><br>The media contact first name |
| media\_contact\_lastname | string<br><br>The media contact last name |
| media\_contact\_email | string<br><br>The media contact email |
| agency\_link | string<br><br>URL link for agency |
| user\_email | string<br><br>The email of user submitting the insertion order |
| oracle\_line\_id | string<br><br>LineId in the Oracle DB |
| salesforce\_order\_id | string<br><br>OrderId in SFDC |
| salesforce\_order\_line\_id | string<br><br>OrderLineId in SFDC |
| ads\_manager\_order\_line\_id | string<br><br>Ads manager OrderLineId |