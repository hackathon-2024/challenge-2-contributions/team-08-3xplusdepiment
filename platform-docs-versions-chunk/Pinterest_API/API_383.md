platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Response samples

* 200
* 403
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "account_type": "PINNER",      * "id": "2783136121146311751",      * "profile_image": "string",      * "website_url": "string",      * "username": "string",      * "about": "string",      * "business_name": "string",      * "board_count": 14,      * "pin_count": 339,      * "follower_count": 10,      * "following_count": 347,      * "monthly_views": 163       }`