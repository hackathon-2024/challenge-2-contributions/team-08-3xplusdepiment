platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Response samples

* 201
* 400
* 401
* 403
* 409
* default

Content type

application/json

Example

feed\_based\_product\_group

feed\_based\_product\_group

catalog\_based\_product\_group

Copy

Expand all Collapse all

`{  * "id": "443727193917",      * "name": "Most Popular",      * "description": "string",      * "filters": {          * "any_of": [                  * {                          * "MIN_PRICE": {                                  * "inclusion": true,                                      * "values": 0,                                      * "negated": false                                                       }                                           }                               ]                   },      * "is_featured": true,      * "type": "TOP_SELLERS",      * "status": "ACTIVE",      * "created_at": 1621350033000,      * "updated_at": 1622742155000,      * "feed_id": "2680059592705",      * "catalog_type": "RETAIL"       }`