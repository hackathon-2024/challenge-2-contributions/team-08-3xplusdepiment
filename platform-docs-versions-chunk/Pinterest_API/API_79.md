platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


### Response samples

* 200
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "categories": [          * {                  * "key": "1234567",                      * "name": "travel",                      * "ratio": 0.551,                      * "index": 1.2,                      * "id": "1234567",                      * "subcategories": [                          * {                                  * "key": "958862518888",                                      * "name": "travel destinations",                                      * "ratio": 0.482,                                      * "index": 1.2,                                      * "id": "958862518888"                                                       }                                           ]                               }                   ],      * "demographics": {          * "ages": [                  * {                          * "name": "United States",                              * "key": "us",                              * "ratio": 0.551                                           }                               ],              * "genders": [                  * {                          * "name": "United States",                              * "key": "us",                              * "ratio": 0.551                                           }                               ],              * "devices": [                  * {                          * "name": "United States",                              * "key": "us",                              * "ratio": 0.551                                           }                               ],              * "metros": [                  * {                          * "name": "United States",                              * "key": "us",                              * "ratio": 0.551                                           }                               ],              * "countries": [                  * {                          * "name": "United States",                              * "key": "us",                              * "ratio": 0.551                                           }                               ]                   },      * "type": "YOUR_TOTAL_AUDIENCE",      * "date": "2022-10-09",      * "size": 10000,      * "size_is_upper_bound": true       }`