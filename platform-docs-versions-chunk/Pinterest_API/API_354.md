platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

## [](#operation/product_group_promotions/get)Get a product group promotion by id

Get a product group promotion by id

ratelimit-category: ads\_read

sandbox: disabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:read`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |
| product\_group\_promotion\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of a product group promotion |

### Responses

**200**

Success

**default**

Unexpected error

get/ad\_accounts/{ad\_account\_id}/product\_group\_promotions/{product\_group\_promotion\_id}

https://api.pinterest.com/v5/ad\_accounts/{ad\_account\_id}/product\_group\_promotions/{product\_group\_promotion\_id}