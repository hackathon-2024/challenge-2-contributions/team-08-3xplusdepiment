platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Response samples

* 200
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "bid_floors": [          * 100000,              * 200000                   ],      * "type": "bidfloor"       }`

# [](#tag/ads)ads

View, create or update ads.