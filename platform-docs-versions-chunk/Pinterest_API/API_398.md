platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

response

**default**

Unexpected error

get/user\_account/following

https://api.pinterest.com/v5/user\_account/following

### Response samples

* 200
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "items": [          * {                  * "username": "username",                      * "type": "user"                               }                   ],      * "bookmark": "string"       }`