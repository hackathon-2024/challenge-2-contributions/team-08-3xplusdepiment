platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/ad_groups_bid_floor/get)Get bid floors

List bid floors for your campaign configuration. Bid floors are given in microcurrency values based on the currency in the bid floor specification.

Microcurrency is used to track very small transactions, based on the currency set in the advertiser’s profile.

A microcurrency unit is 10^(-6) of the standard unit of currency selected in the advertiser’ s profile.

**Equivalency equations**, using dollars as an example currency:

* $1 = 1,000,000 microdollars
* 1 microdollar = $0.000001

**To convert between currency and microcurrency**, using dollars as an example currency:

* To convert dollars to microdollars, mutiply dollars by 1,000,000
* To convert microdollars to dollars, divide microdollars by 1,000,000

For more on bid floors see [Set your bid](https://help.pinterest.com/en/business/article/set-your-bid).

ratelimit-category: ads\_read

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:read`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |

##### Request Body schema: application/json

Parameters to get bid\_floor info

|     |     |
| --- | --- |
| bid\_floor\_specs<br><br>required | Array of objects (bid\_floor\_specs) |
| targeting\_spec | object (TargetingSpec)<br><br>Ad group targeting specification defining the ad group target audience. For example, {"APPTYPE":\["iphone"\], "GENDER":\["male"\], "LOCALE":\["en-US"\], "LOCATION":\["501"\], "AGE\_BUCKET":\["25-34"\]} |