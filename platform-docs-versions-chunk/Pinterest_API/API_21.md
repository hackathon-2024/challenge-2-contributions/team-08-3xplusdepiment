platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

OK

**400**

Invalid ad account id.

**default**

Unexpected error

delete/ad\_accounts/{ad\_account\_id}/sandbox

https://api.pinterest.com/v5/ad\_accounts/{ad\_account\_id}/sandbox

### Response samples

* 200
* 400
* default

Content type

application/json

Copy

Expand all Collapse all

`"Delete Success"`