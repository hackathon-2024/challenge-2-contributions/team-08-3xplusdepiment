platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

## [](#operation/ad_accounts_subscriptions/get_by_id)Get lead ads subscription

Get a specific lead ads subscription record.

* Only requests for the OWNER or ADMIN of the ad\_account will be allowed.

**This endpoint is currently in beta and not available to all apps. [Learn more](https://developers.pinterest.com/docs/new/about-beta-access/).**

ratelimit-category: ads\_read

sandbox: disabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:read`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |
| subscription\_id<br><br>required | string^\\d+$<br><br>Unique identifier of a subscription. |