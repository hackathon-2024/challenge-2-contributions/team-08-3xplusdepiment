platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


### Response samples

* 200
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "items": [          * {                  * "data": {                          * "id": "2680059592705",                              * "ad_group_id": "2680059592705",                              * "bid_in_micro_currency": 14000000,                              * "included": true,                              * "definition": "*/product_type_0='kitchen'/product_type_1='beverage appliances'",                              * "relative_definition": "product_type_1='beverage appliances'",                              * "parent_id": "1231234",                              * "slideshow_collections_title": "slideshow title",                              * "slideshow_collections_description": "slideshow description",                              * "is_mdl": true,                              * "status": "ACTIVE",                              * "tracking_url": "[https://www.pinterest.com](https://www.pinterest.com/)",                              * "catalog_product_group_id": "1231235",                              * "catalog_product_group_name": "catalogProductGroupName",                              * "creative_type": "REGULAR",                              * "collections_hero_pin_id": "123123",                              * "collections_hero_destination_url": "[http://www.pinterest.com](http://www.pinterest.com/)",                              * "grid_click_type": "CLOSEUP"                                           },                      * "exceptions": [                          * {                                  * "code": 2,                                      * "message": "Advertiser not found."                                                       }                                           ]                               }                   ]       }`