platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**204**

Board section deleted successfully

**403**

Not authorized to delete board section.

**404**

Board section not found.

**409**

Board section conflict.

**default**

Unexpected error

delete/boards/{board\_id}/sections/{section\_id}

https://api.pinterest.com/v5/boards/{board\_id}/sections/{section\_id}

### Response samples

* 403
* 404
* 409
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "code": 403,      * "message": "Not authorized to delete board section."       }`