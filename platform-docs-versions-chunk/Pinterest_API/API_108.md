platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


### Response samples

* 200
* 400
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "eligible": true,      * "can_edit": true,      * "billto_infos": [          * {                  * "id": "0011N00001LW8kAQAT",                      * "io_terms_id": "a2S1N000000bKHgUAM",                      * "io_terms": "The IO is governed by the terms available at https://business.pinterest.com/en/pinterest-advertising-services-agreement/. If a budget is listed on this IO, the parties agree that Advertiser (or if applicable, its Agency) may apply any of the budget to any auction bid type or ad product. Price will be determined by auction closing price, plus any applicable non-auction fees. The terms of the Agreement supersede any terms on this IO. ANY ADDITIONAL TERMS AND CONDITIONS ON THIS IO ARE NULL AND VOID.",                      * "us_terms_id": "a2S1N000000bKIOUA2",                      * "us_terms": "This Insertion Order (\"IO\") is subject to the Pinterest Addendum To IAB Standard Terms and Conditions for Internet Advertising For Media Buys One Year or Less (Version 3.0), as executed by Pinterest, Inc. and GroupM Worldwide LLC on May 7, 2014 and Amendment No. 1 to Pinterest Addendum to IAB Standard Terms and Conditions for Internet Advertising For Media Buys One Year or Less (Version 3.0) as executed by Pinterest, Inc. and GroupM Worldwide LLC on August 20, 2015. The parties agree that Agency may apply any of the budget listed on this IO to any auction bid type or ad product. Price will be determined by auction closing price, plus any applicable non-auction fees.The terms of the Addendum supersede any terms on this IO. ANY ADDITIONAL TERMS AND CONDITIONS ON THIS IO ARE NULL AND VOID.",                      * "row_terms_id": "a2S1N000000bKHhUAM",                      * "row_terms": "The IO is governed by the terms available at\r\nhttps://business.pinterest.com/en-gb/pinterest-advertising-services-agreement",                      * "io_type": "Pinterest Paper",                      * "addresses": [                          * {                                  * "display": "475 Brannan Street, San Francisco, CA 94103",                                      * "purpose": "Billing",                                      * "address_id": "a1C1N000004MUrLUAW",                                      * "order_legal_entity": "PIN US OU"                                                       }                                           ]                               }                   ],      * "currency": "USD",      * "pmp_names": [          * {                  * "name": "Bidalgo",                      * "id": "0011N00001LW2aSQAT"                               }                   ],      * "error": "No Error"       }`