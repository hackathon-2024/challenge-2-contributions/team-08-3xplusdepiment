platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

response

**403**

Not authorized to access Pins on board section.

**404**

Board or section not found.

**409**

Board section conflict.

**default**

Unexpected error

get/boards/{board\_id}/sections/{section\_id}/pins

https://api.pinterest.com/v5/boards/{board\_id}/sections/{section\_id}/pins