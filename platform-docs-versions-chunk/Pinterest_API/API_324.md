platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/pins/create)Create Pin

Create a Pin on a board or board section owned by the "operation user\_account".

Note: If the current "operation user\_account" (defined by the access token) has access to another user's Ad Accounts via Pinterest Business Access, you can modify your request to make use of the current operation\_user\_account's permissions to those Ad Accounts by including the ad\_account\_id in the path parameters for the request (e.g. .../?ad\_account\_id=12345&...).

* This function is intended solely for publishing new content created by the user. If you are interested in saving content created by others to your Pinterest boards, sometimes called 'curated content', please use our [Save button](https://developers.pinterest.com/docs/add-ons/save-button) instead. For more tips on creating fresh content for Pinterest, review our [Content App Solutions Guide](https://developers.pinterest.com/docs/content/content-creation/).

**[Learn more](https://developers.pinterest.com/docs/content/content-creation/#Creating%20video%20Pins)** about video Pin creation.

ratelimit-category: org\_write

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`boards:read``boards:write``pins:read``pins:write`)

##### query Parameters

|     |     |
| --- | --- |
| ad\_account\_id | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |

##### Request Body schema: application/json

Create a new Pin.

|     |     |
| --- | --- |
| link | string <= 2048 characters Nullable |
| title | string <= 100 characters Nullable |
| description | string <= 800 characters Nullable |
| dominant\_color | string Nullable<br><br>Dominant pin color. Hex number, e.g. "#6E7874". |
| alt\_text | string <= 500 characters Nullable |
| board\_id | string^\\d+$<br><br>The board to which this Pin belongs. |
| board\_section\_id | string Nullable ^\\d+$<br><br>The board section to which this Pin belongs. |
| media\_source | object<br><br>Pin media source. |
| parent\_pin\_id | string Nullable ^\\d+$<br><br>The source pin id if this pin was saved from another pin. [Learn more](https://help.pinterest.com/article/save-pins-on-pinterest). |
| note | string Nullable<br><br>Private note for this Pin. [Learn more](https://help.pinterest.com/en/article/add-notes-to-your-pins). |