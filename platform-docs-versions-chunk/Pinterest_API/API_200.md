platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

Success

**400**

Invalid parameters.

**401**

Unauthorized access.

**404**

Feed not found.

**default**

Unexpected error.

get/catalogs/feeds/{feed\_id}/processing\_results

https://api.pinterest.com/v5/catalogs/feeds/{feed\_id}/processing\_results