platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Response samples

* 200
* 404
* 409
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "id": "7329167449607351372",      * "external_business_id": "1238401984",      * "connected_merchant_id": "1445572885401",      * "connected_user_id": "871939315263957401",      * "connected_advertiser_id": "549764738871",      * "connected_lba_id": "871939315263957402",      * "connected_tag_id": "2412141155151",      * "partner_access_token_expiry": 1621350033000,      * "partner_refresh_token_expiry": 1621350033000,      * "scopes": "accounts:read",      * "created_timestamp": 1621350033000,      * "updated_timestamp": 1621350033000,      * "additional_id_1": "128464",      * "partner_metadata": ""       }`