platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**204**

Board deleted successfully

**403**

Not authorized to delete the board.

**404**

Board not found.

**409**

Could not get exclusive access to delete the board.

**429**

This request exceeded a rate limit. This can happen if the client exceeds one of the published rate limits or if multiple write operations are applied to an object within a short time window.

**default**

Unexpected error

delete/boards/{board\_id}

https://api.pinterest.com/v5/boards/{board\_id}

### Request samples

* Python SDK
* curl
* curl (Sandbox)

Copy

\# Follow this link for initial setup: https://github.com/pinterest/pinterest-python-sdk#getting-started

from pinterest.organic.boards import Board
\# Board information can be fetched from profile page or from create/list board method here:
\# https://developers.pinterest.com/docs/api/v5/#operation/boards/list
BOARD\_ID\="<Add your board id here>"

board\_delete\=Board.delete(board\_id\=BOARD\_ID)
print("Board was deleted? %s" % (board\_delete))