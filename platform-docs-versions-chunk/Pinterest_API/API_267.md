platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

## [](#operation/integrations_commerce/get)Get commerce integration

Get commerce integration metadata associated with the given external business ID. Note: If you're interested in joining the beta, please reach out to your Pinterest account manager.

ratelimit-category: ads\_read

sandbox: disabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:read`)

##### path Parameters

|     |     |
| --- | --- |
| external\_business\_id<br><br>required | string<br><br>External business ID for the integration. |

### Responses

**200**

Success

**404**

Integration not found.

**409**

Can't access this integration metadata.

**default**

Unexpected error.

get/integrations/commerce/{external\_business\_id}

https://api.pinterest.com/v5/integrations/commerce/{external\_business\_id}