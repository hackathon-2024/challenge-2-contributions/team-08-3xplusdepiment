platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

Success

**400**

Invalid ad account group parameters.

**default**

Unexpected error

get/ad\_accounts/{ad\_account\_id}/ad\_groups

https://api.pinterest.com/v5/ad\_accounts/{ad\_account\_id}/ad\_groups