platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**201**

response

**400**

The board name is invalid or duplicated.

**default**

Unexpected error

post/boards

https://api.pinterest.com/v5/boards

### Request samples

* Payload
* Python SDK
* curl
* curl (Sandbox)

Content type

application/json

Copy

Expand all Collapse all

`{  * "name": "Summer Recipes",      * "description": "My favorite summer recipes",      * "privacy": "PUBLIC"       }`