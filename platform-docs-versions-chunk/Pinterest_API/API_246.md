platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/conversion_tags/create)Create conversion tag

Create a conversion tag, also known as [Pinterest tag](https://help.pinterest.com/en/business/article/set-up-the-pinterest-tag), with the option to enable enhanced match.

The Pinterest Tag tracks actions people take on the ad account’ s website after they view the ad account's ad on Pinterest. The advertiser needs to customize this tag to track conversions.

For more information, see:

[Set up the Pinterest tag](https://help.pinterest.com/en/business/article/set-up-the-pinterest-tag)

[Pinterest Tag](https://developers.pinterest.com/docs/conversions/pinterest-tag/)

[Enhanced match](https://developers.pinterest.com/docs/conversions/enhanced-match/)

sandbox: disabled

ratelimit-category: ads\_write

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:write`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |

##### Request Body schema: application/json

Conversion Tag to create

|     |     |
| --- | --- |
| name<br><br>required | string (name)<br><br>Conversion tag name. |
| aem\_enabled | boolean (aem\_enabled) Nullable<br><br>Default: false<br><br>Whether Automatic Enhanced Match email is enabled. See [Enhanced match](https://help.pinterest.com/en/business/article/enhanced-match) for more information. |
| md\_frequency | number (md\_frequency) Nullable<br><br>Default: 1<br><br>Metadata ingestion frequency. |
| aem\_fnln\_enabled | boolean (aem\_fnln\_enabled) Nullable<br><br>Default: false<br><br>Whether Automatic Enhanced Match name is enabled. See [Enhanced match](https://help.pinterest.com/en/business/article/enhanced-match) for more information. |
| aem\_ph\_enabled | boolean (aem\_ph\_enabled) Nullable<br><br>Default: false<br><br>Whether Automatic Enhanced Match phone is enabled. See [Enhanced match](https://help.pinterest.com/en/business/article/enhanced-match) for more information. |
| aem\_ge\_enabled | boolean (aem\_ge\_enabled) Nullable<br><br>Default: false<br><br>Whether Automatic Enhanced Match gender is enabled. See [Enhanced match](https://help.pinterest.com/en/business/article/enhanced-match) for more information. |
| aem\_db\_enabled | boolean (aem\_db\_enabled) Nullable<br><br>Default: false<br><br>Whether Automatic Enhanced Match birthdate is enabled. See [Enhanced match](https://help.pinterest.com/en/business/article/enhanced-match) for more information. |
| aem\_loc\_enabled | boolean (aem\_loc\_enabled) Nullable<br><br>Default: false<br><br>Whether Automatic Enhanced Match location is enabled. See [Enhanced match](https://help.pinterest.com/en/business/article/enhanced-match) for more information. |