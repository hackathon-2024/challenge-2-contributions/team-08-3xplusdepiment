platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

Success

**default**

Unexpected error

get/ad\_accounts/{ad\_account\_id}/keywords

https://api.pinterest.com/v5/ad\_accounts/{ad\_account\_id}/keywords

### Response samples

* 200
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "items": [          * {                  * "archived": false,                      * "id": "383791336903426391",                      * "parent_id": "383791336903426391",                      * "parent_type": "campaign",                      * "type": "keyword",                      * "bid": 200000,                      * "match_type": "BROAD",                      * "value": "string"                               }                   ],      * "bookmark": "string"       }`