platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Response samples

* 200
* 400
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "report_status": "FINISHED",      * "token": "string",      * "message": "string"       }`