platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

## [](#operation/order_lines/get)Get order line

Get a specific existing order line associated with an ad account.

ratelimit-category: ads\_read

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:read`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |
| order\_line\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an order line. |

### Responses

**200**

Success

**default**

Unexpected error

get/ad\_accounts/{ad\_account\_id}/order\_lines/{order\_line\_id}

https://api.pinterest.com/v5/ad\_accounts/{ad\_account\_id}/order\_lines/{order\_line\_id}