platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/catalogs_product_groups/list)List product groups

Get a list of product groups for a given Catalogs Feed Id owned by the "operation user\_account".

* By default, the "operation user\_account" is the token user\_account.

Optional: Business Access: Specify an `ad_account_id` (obtained via [List ad accounts](https://developers.pinterest.com/docs/api/v5/#operation/ad_accounts/list)) to use the owner of that ad\_account as the "operation user\_account". In order to do this, the token user\_account must have one of the following [Business Access](https://help.pinterest.com/en/business/article/share-and-manage-access-to-your-ad-accounts) roles on the ad\_account: Owner, Admin, Catalogs Manager.

[Learn more](https://developers.pinterest.com/docs/shopping/catalog/)

ratelimit-category: catalogs\_read

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`catalogs:read`)

##### query Parameters

|     |     |
| --- | --- |
| feed\_id | string^\\d+$<br><br>Filter entities for a given feed\_id. If not given, all feeds are considered. |
| catalog\_id | string^\\d+$<br><br>Filter entities for a given catalog\_id. If not given, all catalogs are considered. |
| bookmark | string<br><br>Cursor used to fetch the next page of items |
| page\_size | integer \[ 1 .. 250 \]<br><br>Default: 25<br><br>Maximum number of items to include in a single page of the response. See documentation on [Pagination](https://developers.pinterest.com/docs/getting-started/pagination/) for more information. |
| ad\_account\_id | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |