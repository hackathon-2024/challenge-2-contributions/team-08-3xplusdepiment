platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/ad_accounts_subscriptions/post)Create lead ads subscription

Create a lead ads webhook subscription.

* Only requests for the OWNER or ADMIN of the ad\_account will be allowed.
* Advertisers can set up multiple integrations using ad\_account\_id + lead\_form\_id but only one integration per unique records.

**This endpoint is currently in beta and not available to all apps. [Learn more](https://developers.pinterest.com/docs/new/about-beta-access/).**

ratelimit-category: ads\_write

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:write`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |

##### Request Body schema: application/json

Subscription to create.

|     |     |
| --- | --- |
| webhook\_url<br><br>required | string (webhook\_url)<br><br>Standard HTTPS webhook URL. |
| lead\_form\_id | string (Lead form ID) ^\\d+$<br><br>Lead form ID. |
| partner\_access\_token | string<br><br>Partner access token. Only for clients that requires authentication. We recommend to avoid this param. |
| partner\_refresh\_token | string<br><br>Partner refresh token. Only for clients that requires authentication. We recommend to avoid this param. |