platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/campaigns/create)Create campaigns

Create multiple new campaigns. Every campaign has its own campaign\_id and houses one or more ad groups, which contain one or more ads. For more, see [Set up your campaign](https://help.pinterest.com/en/business/article/set-up-your-campaign/).

**Note:**

* The values for 'lifetime\_spend\_cap' and 'daily\_spend\_cap' are microcurrency amounts based on the currency field set in the advertiser's profile. (e.g. USD)
    
    Microcurrency is used to track very small transactions, based on the currency set in the advertiser’s profile.
    
    A microcurrency unit is 10^(-6) of the standard unit of currency selected in the advertiser’s profile.
    
    **Equivalency equations**, using dollars as an example currency:
    
    * $1 = 1,000,000 microdollars
    * 1 microdollar = $0.000001
    
    **To convert between currency and microcurrency**, using dollars as an example currency:
    
    * To convert dollars to microdollars, mutiply dollars by 1,000,000
    * To convert microdollars to dollars, divide microdollars by 1,000,000

ratelimit-category: ads\_write

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:write`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |

##### Request Body schema: application/json

Array of campaigns.

Array ()

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string^\\d+$<br><br>Campaign's Advertiser ID. If you want to create a campaign in a Business Account shared account you need to specify the Business Access advertiser ID in both the query path param as well as the request body schema. |
| name<br><br>required | string<br><br>Campaign name. |
| status | string<br><br>Default: "ACTIVE"<br><br>Enum: "ACTIVE" "PAUSED" "ARCHIVED" "DRAFT" "DELETED\_DRAFT"<br><br>Entity status |
| lifetime\_spend\_cap | integer Nullable<br><br>Campaign total spending cap. Required for Campaign Budget Optimization (CBO) campaigns. This and "daily\_spend\_cap" cannot be set at the same time. |
| daily\_spend\_cap | integer Nullable<br><br>Campaign daily spending cap. Required for Campaign Budget Optimization (CBO) campaigns. This and "lifetime\_spend\_cap" cannot be set at the same time. |
| order\_line\_id | string Nullable ^\\d+$<br><br>Order line ID that appears on the invoice. |
| tracking\_urls | object Nullable<br><br>Third-party tracking URLs. Up to three tracking URLs - with a max length of 2,000 - are supported for each event type. Tracking URLs set at the ad group or ad level can override those set at the campaign level. For more information, see [Third-party and dynamic tracking](https://help.pinterest.com/en/business/article/third-party-and-dynamic-tracking). |
| start\_time | integer Nullable<br><br>Campaign start time. Unix timestamp in seconds. Only used for Campaign Budget Optimization (CBO) campaigns. |
| end\_time | integer Nullable<br><br>Campaign end time. Unix timestamp in seconds. Only used for Campaign Budget Optimization (CBO) campaigns. |
| summary\_status | string<br><br>Enum: "RUNNING" "PAUSED" "NOT\_STARTED" "COMPLETED" "ADVERTISER\_DISABLED" "ARCHIVED" "DRAFT" "DELETED\_DRAFT"<br><br>Summary status for campaign |
| is\_flexible\_daily\_budgets | boolean<br><br>Default: false<br><br>Determine if a campaign has flexible daily budgets setup. |
| default\_ad\_group\_budget\_in\_micro\_currency | integer Nullable<br><br>When transitioning from campaign budget optimization to non-campaign budget optimization, the default\_ad\_group\_budget\_in\_micro\_currency will propagate to each child ad groups daily budget. Unit is micro currency of the associated advertiser account. |
| is\_automated\_campaign | boolean<br><br>Default: false<br><br>Specifies whether the campaign was created in the automated campaign flow |
| objective\_type<br><br>required | string (ObjectiveType)<br><br>Enum: "AWARENESS" "CONSIDERATION" "VIDEO\_VIEW" "WEB\_CONVERSION" "CATALOG\_SALES" "WEB\_SESSIONS"<br><br>Campaign objective type. If set as one of \["AWARENESS", "CONSIDERATION", "WEB\_CONVERSION", "CATALOG\_SALES"\] the campaign is considered as a Campaign Budget Optimization (CBO) campaign, meaning budget needs to be set at the campaign level rather than at the ad group level. \["WEB\_SESSIONS"\] in BETA. |