platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/


## [](#operation/ad_targeting_analytics/get)Get targeting analytics for ads

Get targeting analytics for one or more ads. For the requested ad(s) and metrics, the response will include the requested metric information (e.g. SPEND\_IN\_DOLLAR) for the requested target type (e.g. "age\_bucket") for applicable values (e.g. "45-49").

* The token's user\_account must either be the Owner of the specified ad account, or have one of the necessary roles granted to them via [Business Access](https://help.pinterest.com/en/business/article/share-and-manage-access-to-your-ad-accounts): Admin, Analyst, Campaign Manager.
* If granularity is not HOUR, the furthest back you can are allowed to pull data is 90 days before the current date in UTC time and the max time range supported is 90 days.
* If granularity is HOUR, the furthest back you can are allowed to pull data is 8 days before the current date in UTC time and the max time range supported is 3 days.

ratelimit-category: ads\_analytics

sandbox: enabled

##### Authorizations:

[pinterest\_oauth2](#section/Authentication/pinterest_oauth2) (`ads:read`)

##### path Parameters

|     |     |
| --- | --- |
| ad\_account\_id<br><br>required | string <= 18 characters ^\\d+$<br><br>Unique identifier of an ad account. |

##### query Parameters

|     |     |
| --- | --- |
| ad\_ids<br><br>required | Array of strings \[ 1 .. 100 \] items<br><br>List of Ad Ids to use to filter the results. |
| start\_date<br><br>required | string <date><br><br>Metric report start date (UTC). Format: YYYY-MM-DD. Cannot be more than 90 days back from today. |
| end\_date<br><br>required | string <date><br><br>Metric report end date (UTC). Format: YYYY-MM-DD. Cannot be more than 90 days past start\_date. |
| targeting\_types<br><br>required | Array of strings (AdsAnalyticsTargetingType) \[ 1 .. 15 \] items<br><br>Items Enum: "KEYWORD" "APPTYPE" "GENDER" "LOCATION" "PLACEMENT" "COUNTRY" "TARGETED\_INTEREST" "PINNER\_INTEREST" "AUDIENCE\_INCLUDE" "GEO" "AGE\_BUCKET" "REGION"<br><br>Example: targeting\_types=APPTYPE<br><br>Targeting type breakdowns for the report. The reporting per targeting type  <br>is independent from each other. |
| columns<br><br>required | Array of strings<br><br>Items Enum: "SPEND\_IN\_MICRO\_DOLLAR" "PAID\_IMPRESSION" "SPEND\_IN\_DOLLAR" "CPC\_IN\_MICRO\_DOLLAR" "ECPC\_IN\_MICRO\_DOLLAR" "ECPC\_IN\_DOLLAR" "CTR" "ECTR" "CAMPAIGN\_NAME" "PIN\_ID" "TOTAL\_ENGAGEMENT" "ENGAGEMENT\_1" "ENGAGEMENT\_2" "ECPE\_IN\_DOLLAR" "ENGAGEMENT\_RATE" "EENGAGEMENT\_RATE" "ECPM\_IN\_MICRO\_DOLLAR" "REPIN\_RATE" "CTR\_2" "CAMPAIGN\_ID" … 129 more<br><br>Example: columns=TOTAL\_CONVERSIONS<br><br>Columns to retrieve, encoded as a comma-separated string. **NOTE**: Any metrics defined as MICRO\_DOLLARS returns a value based on the advertiser profile's currency field. For USD,($1/1,000,000, or $0.000001 - one one-ten-thousandth of a cent). it's microdollars. Otherwise, it's in microunits of the advertiser's currency.  <br>For example, if the advertiser's currency is GBP (British pound sterling), all MICRO\_DOLLARS fields will be in GBP microunits (1/1,000,000 British pound).  <br>If a column has no value, it may not be returned |
| granularity<br><br>required | string (Granularity)<br><br>Enum: "TOTAL" "DAY" "HOUR" "WEEK" "MONTH"<br><br>Example: granularity=DAY<br><br>TOTAL - metrics are aggregated over the specified date range.  <br>DAY - metrics are broken down daily.  <br>HOUR - metrics are broken down hourly.  <br>WEEKLY - metrics are broken down weekly.  <br>MONTHLY - metrics are broken down monthly |
| click\_window\_days | integer<br><br>Default: 30<br><br>Enum: 0 1 7 14 30 60<br><br>Example: click\_window\_days=1<br><br>Number of days to use as the conversion attribution window for a pin click action. Applies to Pinterest Tag conversion metrics. Prior conversion tags use their defined attribution windows. If not specified, defaults to `30` days. |
| engagement\_window\_days | integer<br><br>Default: 30<br><br>Enum: 0 1 7 14 30 60<br><br>Number of days to use as the conversion attribution window for an engagement action. Engagements include saves, closeups, link clicks, and carousel card swipes. Applies to Pinterest Tag conversion metrics. Prior conversion tags use their defined attribution windows. If not specified, defaults to `30` days. |
| view\_window\_days | integer<br><br>Default: 1<br><br>Enum: 0 1 7 14 30 60<br><br>Number of days to use as the conversion attribution window for a view action. Applies to Pinterest Tag conversion metrics. Prior conversion tags use their defined attribution windows. If not specified, defaults to `1` day. |
| conversion\_report\_time | string<br><br>Default: "TIME\_OF\_AD\_ACTION"<br><br>Enum: "TIME\_OF\_AD\_ACTION" "TIME\_OF\_CONVERSION"<br><br>Example: conversion\_report\_time=TIME\_OF\_AD\_ACTION<br><br>The date by which the conversion metrics returned from this endpoint will be reported. There are two dates associated with a conversion event: the date that the user interacted with the ad, and the date that the user completed a conversion event. |
| attribution\_types | string (ConversionReportAttributionType)<br><br>Enum: "INDIVIDUAL" "HOUSEHOLD"<br><br>Example: attribution\_types=INDIVIDUAL<br><br>List of types of attribution for the conversion report |