platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

Success

**default**

Unexpected error

get/ad\_accounts/{ad\_account\_id}/insights/audiences

https://api.pinterest.com/v5/ad\_accounts/{ad\_account\_id}/insights/audiences

### Response samples

* 200
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "items": [          * {                  * "date": "2022-10-09",                      * "scope": "PARTNER",                      * "type": "IMPRESSION_PLUS_ENGAGEMENT"                               }                   ]       }`

# [](#tag/audiences)audiences

View, create, or update audiences.