platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Response samples

* 201
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "media_id": "12345",      * "media_type": "video",      * "upload_url": "[https://pinterest-media-upload.s3-accelerate.amazonaws.com/](https://pinterest-media-upload.s3-accelerate.amazonaws.com/)",      * "upload_parameters": {          * "x-amz-data": "20220127T185143Z",              * "x-amz-signature": "fcd6309a6aaee213348666a72abed8b44552a43acb6b340e8e1b288d21a5fe92",              * "key": "uploads/11/aa/22/3:video:203014033110991560:5212123920968240771",              * "policy": "eyJleHBpcmF0aW9uIjoiMj..==",              * "x-amz-credential": "ASIA6QZJ64OPIKV7FRVX/20220127/us-east-1/s3/aws4_request",              * "x-amz-security-token": "IQoJb3JpZ2luX2VjEJr...==",              * "x-amz-algorithm": "AWS4-HMAC-SHA256",              * "Content-Type": "multipart/form-data"                   }       }`