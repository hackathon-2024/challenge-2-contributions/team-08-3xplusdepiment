platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

Success

**404**

Product Group Not Found.

**409**

Can't access this feature without an existing catalog.

**default**

Unexpected error.

get/catalogs/product\_groups/{product\_group\_id}/product\_counts

https://api.pinterest.com/v5/catalogs/product\_groups/{product\_group\_id}/product\_counts

### Response samples

* 200
* 404
* 409
* default

Content type

application/json

Copy

Expand all Collapse all

`{  * "in_stock": 0,      * "out_of_stock": 0,      * "preorder": 0,      * "total": 0       }`