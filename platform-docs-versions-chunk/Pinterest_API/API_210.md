platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

Response containing the requested catalogs items batch

**401**

Not authenticated to access catalogs items batch

**403**

Not authorized to access catalogs items batch

**404**

Catalogs items batch not found

**405**

Method Not Allowed.

**default**

Unexpected error

get/catalogs/items/batch/{batch\_id}

https://api.pinterest.com/v5/catalogs/items/batch/{batch\_id}