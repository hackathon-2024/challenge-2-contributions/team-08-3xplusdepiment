platform: Pinterest
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Pinterest_API/API.md
url: https://developers.pinterest.com/docs/api/v5/

### Responses

**200**

Success

**400**

Invalid parameters.

**401**

Unauthorized access.

**404**

Catalogs product group not found.

**default**

Unexpected error.

get/catalogs/product\_groups/{product\_group\_id}/products

https://api.pinterest.com/v5/catalogs/product\_groups/{product\_group\_id}/products