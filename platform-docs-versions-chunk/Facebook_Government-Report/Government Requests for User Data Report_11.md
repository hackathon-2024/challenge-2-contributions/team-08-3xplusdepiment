platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/further-asked-questions/


## Our data disclosure process

What data does Meta disclose in response to government requests?

Meta scrutinizes every government request and produces only the information that is narrowly tailored to respond to each request. Depending on the request, Meta may produce:

Basic subscriber information: Such as name, length of service, payment information, email addresses, and recent login/logout IP addresses.

Records pertaining to account activity: Such as message headers and IP addresses.

The stored contents of an account: Such as messages, photos, videos, timeline posts, and location information.

For additional information on data we disclose in response to government requests, please see our [guidelines for government requests](https://www.facebook.com/safety/groups/law/guidelines/).

How does Meta respond to requests made under the US Foreign Intelligence Surveillance Act (FISA)?

FISA authorizes the US government to request data related to US National Security. Section 702 of FISA allows the US government to target specified electronic communication service provider (ECSPs) accounts of non-US persons located outside the United States to acquire foreign intelligence information (including the content of communications). The requests must be for specified accounts with valid account identifiers. In addition, the government is permitted to request only account information it has reason to believe will include certain types of intelligence (for example, terrorism). All requests must conform to the terms of a court-approved certification with minimization requirements. View [government requests data for the United States](https://transparency.fb.com/data/government-data-requests/country/us/) for more information on FISA requests.

If Meta were to receive a FISA request, including targeted requests under Section 702, we would follow the same process we do for all government requests -- we would review each request individually to ensure compliance with applicable laws and our policies; push back or challenge any requests that were legally defective or overbroad; and produce only information that is narrowly tailored to respond to the request.

Does Meta receive and respond to requests under Executive Order 12333?

No. E.O. 12333 provides a legal framework for governing US intelligence activities to be conducted outside of the United States, but does not impose any obligations on a service provider like Meta.

Does Meta produce user data in response to US National Security Letters (NSL)?

Meta follows the same process for all government requests - we review each request individually; these requests must be made in accordance with applicable laws and our policies; and we produce only information that is narrowly tailored to respond to the request. Meta would produce the user’s name and length of service in response to a valid NSL. View [government requests data for the United States](https://transparency.fb.com/data/government-data-requests/country/us/) for more information on NSL requests.

Does Meta encrypt data in transit?

Yes. Meta employs industry standard encryption algorithms and protocols designed to secure and maintain the confidentiality of data in transit over public networks. Employing advanced encryption algorithms enables Meta to secure user data in transit from access by third parties.

Does Meta preserve data at the request of government agencies?

In response to valid government requests to preserve user data, we take steps to preserve account records in accordance with applicable law and our policies (including [Meta’s Data Policy](https://www.facebook.com/policy.php)).