platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/preservation-requests/

# Preservation requests

We accept government requests to preserve account information pending receipt of formal legal process. When we receive a preservation request, we will preserve a temporary snapshot of the relevant account information but will not disclose any of the preserved records unless and until we receive formal and valid legal process. We have reported this information since 2016.

[Download (CSV)](https://transparency.fb.com/sr/government-requests/)

## Preservations

Amount of preservation requests we received

Total requests

Users/accounts requested