platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/country/

# Requests by country

Search for country specific information on government requests for user data.

[Download (CSV)](https://transparency.fb.com/sr/government-requests/)

Jan - Jun 2023

271,692

Total requests

469,372

Users/accounts requested

76.90%

Of requests where some data produced