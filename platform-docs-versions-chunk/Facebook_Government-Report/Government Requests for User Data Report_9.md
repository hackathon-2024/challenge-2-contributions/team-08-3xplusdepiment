platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/further-asked-questions/


## General

What is a government data request?

Government officials sometimes make requests for data about people who use Facebook as part of official investigations. The vast majority of these requests relate to criminal cases, such as robberies or kidnappings. In many of these cases, these government requests seek basic subscriber information, such as name, registration date and length of service. Other requests may also seek IP address logs or account content. We have strict [guidelines](https://www.facebook.com/safety/groups/law/guidelines) in place to deal with all government data requests.

What does it mean if my country is not listed?

Countries where our services are available are not included in the report where there are no requests for data or to preserve data.

Do you report data on US national security requests?

Yes. We report the number and nature of US national security data requests, including breakdowns of Foreign Intelligence Surveillance Act orders that seek the content of accounts or non-content information (such as subscriber name) and the number of National Security Letters we received. Pursuant to US Department of Justice requirements, these numbers are reported within ranges of 500 and FISA requests are subject to a six month reporting delay.

[View government requests data for the United States](https://transparency.fb.com/data/government-data-requests/country/us/).

Does this report include information about requests received by other Facebook apps?

This report includes information about requests related to our various products and services including Facebook, Instagram, Messenger, Oculus and Whatsapp unless otherwise noted.

What is non-content data?

Non-content data information such as name, length of service, credit card information, email address(es), and a recent login/logout IP addresses and other transactional information, not including the contents of communications (for example, message headers and IP addresses).

Do you report the number of requests you receive via the Mutual Legal Assistance Treaty (MLAT) process?

Yes. Requests received through the MLAT process are included in our report. We are unable to identify the precise number of requests we received through this channel since they result in the issuance of a search warrant or court order under US law and do not always indicate that they are the product of an MLAT request.

MLATs provide a formal mechanism for countries to cooperate in criminal cases. Countries that have an MLAT with the United States may use this channel to seek data from a provider such as Facebook.

What is a government data request?

Government officials sometimes make requests for data about people who use Facebook as part of official investigations. The vast majority of these requests relate to criminal cases, such as robberies or kidnappings. In many of these cases, these government requests seek basic subscriber information, such as name, registration date and length of service. Other requests may also seek IP address logs or account content. We have strict [guidelines](https://www.facebook.com/safety/groups/law/guidelines) in place to deal with all government data requests.

What does it mean if my country is not listed?

Countries where our services are available are not included in the report where there are no requests for data or to preserve data.

Do you report data on US national security requests?

Yes. We report the number and nature of US national security data requests, including breakdowns of Foreign Intelligence Surveillance Act orders that seek the content of accounts or non-content information (such as subscriber name) and the number of National Security Letters we received. Pursuant to US Department of Justice requirements, these numbers are reported within ranges of 500 and FISA requests are subject to a six month reporting delay.

[View government requests data for the United States](https://transparency.fb.com/data/government-data-requests/country/us/).

Does this report include information about requests received by other Facebook apps?

This report includes information about requests related to our various products and services including Facebook, Instagram, Messenger, Oculus and Whatsapp unless otherwise noted.

What is non-content data?

Non-content data information such as name, length of service, credit card information, email address(es), and a recent login/logout IP addresses and other transactional information, not including the contents of communications (for example, message headers and IP addresses).

Do you report the number of requests you receive via the Mutual Legal Assistance Treaty (MLAT) process?

Yes. Requests received through the MLAT process are included in our report. We are unable to identify the precise number of requests we received through this channel since they result in the issuance of a search warrant or court order under US law and do not always indicate that they are the product of an MLAT request.

MLATs provide a formal mechanism for countries to cooperate in criminal cases. Countries that have an MLAT with the United States may use this channel to seek data from a provider such as Facebook.

[Download (CSV)](https://transparency.fb.com/sr/government-requests/)