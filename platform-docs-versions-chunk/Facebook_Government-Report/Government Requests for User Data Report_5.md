platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/data-types/

## Request Types

Amount of requests by type

Legal process

Emergency disclosure

## Data Produced

Percentage of requests where some data produced

Legal process

Emergency disclosure