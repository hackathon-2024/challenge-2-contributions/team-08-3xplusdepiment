platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/further-asked-questions/


## How we review government requests

What government agencies submit requests for user data to Meta technologies?

Meta receives requests for user data from government agencies around the world. We share details about these requests and our responses twice a year in our Government Requests for User Data Report [Government Requests for User Data Report](https://transparency.fb.com/data/government-data-requests/). Meta requires government agencies that make requests to comply with applicable law, our policies (including our [Data Policy](https://www.facebook.com/policy.php)), and, in certain scenarios, to use the Mutual Legal Assistance Treaty process.

How does Meta respond to government requests for European Economic Area (EEA) users’ data?

Meta scrutinizes every government request we receive, regardless of which government makes the request, to make sure it is legally valid. Meta requires government agencies that make requests comply with applicable laws and our policies. We only produce narrowly tailored user information in response to such requests, and only when we have a good faith belief that the response is required by law in that jurisdiction, affects users in that jurisdiction, and is consistent with internationally recognized standards. In certain scenarios, we may also require such governments to use the Mutual Legal Assistance Treaty process

Who reviews government requests for user data at Meta?

Meta has a dedicated, trained Law Enforcement Response Team (LERT) that reviews and evaluates every government request for user data individually, whether the request was submitted related to an emergency or through legal process obtained by law enforcement or national security authorities. This team ensures that all requests are consistent with applicable law and our policies (including [Meta’s Data Policy](https://www.facebook.com/policy.php)).

Our LERT is well-resourced and continues to grow in order to continue to ensure the careful review and evaluation of each government request. Each LERT analyst receives extensive, rigorous training in Meta’s policies and the requirements for government requests. LERT analysts also receive annual privacy and data protection training. LERT is supported by both in-house and retained outside counsel. These lawyers are experts in the laws that apply to government requests for data.

Does Meta individually review each request?

Yes. We individually review all requests regardless of whether they are submitted related to emergencies or through legal process obtained by law enforcement or national security authorities to ensure that each request is consistent with applicable law and our policies (including [Meta’s Data Policy](https://www.facebook.com/policy.php).).

Does Meta receive requests for emergency disclosure?

Yes. In emergencies, law enforcement may submit requests without legal process. Based on the circumstances, we may voluntarily disclose information to law enforcement where we have a good faith reason to believe that the matter involves imminent risk of serious physical injury or death.

Does Meta notify users (including advertisers) when their data is requested?

We notify users (including advertisers) about requests for their information before disclosing it unless we are prohibited by law from doing so or in exceptional circumstances, such as where a child is at risk of harm, emergencies or when notice would be counterproductive. We will also provide delayed notice upon expiration of a specific non-disclosure period in a court order and when we have a good faith belief that exceptional circumstances no longer exist and we are not otherwise prohibited by law from doing so.

What about Workplace customers?

Meta’s policy is to redirect government requesters to the Workplace customer in the first instance. If Meta is required to respond to a request for information relating to Workplace user data, Meta applies the same rigorous process as we would for any other request.

Does Meta ever push back on or challenge a request?

Yes. If we determine that a government request is not consistent with applicable law or our policies, we push back and engage the governmental agency to address any apparent deficiencies. If the request is unlawful (for example, overly broad, or legally deficient in any way), we will challenge or reject the request. We encourage governmental agencies to submit only requests that are necessary, proportionate, specific, and strictly compliant with applicable laws, by publishing [guidelines for government requests](https://www.facebook.com/safety/groups/law/guidelines/).