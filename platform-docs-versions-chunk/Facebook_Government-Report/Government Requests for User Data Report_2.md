platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/

## Global Overview

Meta responds to government requests for data in accordance with applicable law and our terms of service. Each and every request we receive is carefully reviewed for legal sufficiency and we may reject or require greater specificity on requests that appear overly broad or vague. This chart provides data on the number of requests we received and the rate we complied with all or some of the government's request for each half by country or region. We publish this information in 6-month increments, subject to certain limitations, and we began reporting this information in 2013.

Jan - Jun 2023

% of requests where some data produced

Total requests

271,692

Total requests

469,372

Users/accounts requested

76.90%

Of requests where some data produced

## Learn more about this report

## DEEP DIVE

[Further asked questions](https://transparency.fb.com/data/government-data-requests/further-asked-questions)

## Additional resources