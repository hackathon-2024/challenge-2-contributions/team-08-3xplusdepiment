platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/

# Government Requests for User Data

As part of our ongoing effort to share more information about the requests we have received from governments around the world, Meta regularly produces this report on government requests for user data to provide information on the nature and extent of these requests and the strict policies and processes we have in place to handle them.

[Download (CSV)](https://transparency.fb.com/sr/government-requests/)