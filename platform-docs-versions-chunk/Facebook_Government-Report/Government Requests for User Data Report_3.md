platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/

## DEEP DIVE

[Law enforcement guidelines](https://www.facebook.com/safety/groups/law/)

[Accessing your information](https://www.facebook.com/help/930396167085762)

[Safety Center](https://www.facebook.com/safety)

[Case Studies](https://transparency.fb.com/data/government-data-requests/case-studies/)