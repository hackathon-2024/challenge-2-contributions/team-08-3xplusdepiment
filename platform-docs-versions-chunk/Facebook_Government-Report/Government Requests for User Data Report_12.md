platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/further-asked-questions/


## Transferring personal data to the United States

Does Meta transfer data outside of the European Economic Area (EEA)?

Yes. In order to be able to provide our global services, it is essential for us to be able to share information globally - both internally within Meta, and externally with our partners and with those you connect and share with around the world. We do this in accordance with our [Data Policy](https://www.facebook.com/policy.php). Information controlled by Meta Platforms Ireland will be transferred or transmitted to, or stored and processed in, countries outside of where you live (and the European Economic Area), including the United States, for the purposes described in our [Data Policy](https://www.facebook.com/policy.php). These data transfers are necessary to operate and provide to you the global services described in the [Terms of Service for Facebook](https://www.facebook.com/legal/terms/) and the [Terms of Use for Instagram](#).

What is the Schrems II Judgment on data transfers?

In July 2020, the Court of Justice of the EU (CJEU) clarified the basis on which organizations can transfer data outside the EEA. The CJEU confirmed the validity of the SCCs (on which we rely), but made clear that organizations are responsible for ensuring that any data transferred is appropriately protected. Therefore, while we consider that in most cases the SCCs alone will continue to provide sufficient protection for transfers outside the EEA, including to the United States, we also have appropriate safeguards in place to protect your data.

In the Judgment, the CJEU also invalidated [Privacy Shield](https://www.privacyshield.gov/welcome), a framework for regulating transatlantic transfers of personal data from the EEA to the United States. Although we relied on Privacy Shield as the data transfer mechanism for certain products such as our Workplace product, we did not rely on Privacy Shield for transfers of our own users’ data. To the extent we relied on Privacy Shield, we have now migrated to the SCCs for these products but remain certified and committed to complying with the Privacy Shield framework in connection with some products, including Workplace and certain ad products.

What measures and safeguards have we put in place to protect your data when it is transferred to the United States?

We have in place a number of safeguards and measures to ensure an adequate level of protection for user data being transferred outside the EEA to the United States, including:

Security: We have a comprehensive security program to protect the data stored on our systems, platforms and products.

Encryption of data in transit so it cannot be read: Meta employs industry standard encryption algorithms and protocols designed to secure and maintain the confidentiality of data in transit over public networks. Employing advanced encryption algorithms enables Meta to secure user data in transit from access by third parties.

Policies and procedures:We have robust policies and procedures in place to ensure user data is adequately protected in relation to requests from governmental agencies. For example, we will only comply with a governmental request for user data after we are satisfied that the request complies with applicable law and our policies. If the request is unlawful (for example, overly broad, or legally deficient in any way), we will push back or challenge the request. We encourage governmental agencies to submit only requests that are necessary, proportionate, specific, and strictly compliant with applicable laws, by publishing [guidelines for government requests](https://www.facebook.com/safety/groups/law/guidelines/).

Oversight: We have a dedicated, trained Law Enforcement Response Team (LERT) that reviews and evaluates every government request for user data individually, whether the request was submitted related to an emergency or through legal process obtained by law enforcement or national security authorities. This team ensures that all requests are consistent with applicable law and our policies, including [Meta’s Data Policy](https://www.facebook.com/policy.php).

Government Requests for User Data Report: We publish information on government requests we receive in our [Government Requests for User Data Report](https://transparency.fb.com/data/government-data-requests/). Information regarding requests made under the US Foreign Intelligence Surveillance Act (FISA) is included in the report with the maximum level of detail permitted under US law.

Advocacy: We appreciate the focus of governments across the globe on protecting and safeguarding people’s data, including in the US and Europe, and we work hard to do our part. We actively engage with governments to encourage practices that protect peoples’ rights. We belong to advocacy groups like [Global Network Initiative](https://globalnetworkinitiative.org/about-gni/), whose mission is to advance the freedom of expression and privacy rights of Internet users worldwide; and are a founding member of [Reform Government Surveillance](https://www.reformgovernmentsurveillance.com/), which advocates for government data requests to be rule bound, narrowly tailored, transparent, subject to strong oversight and protective of end-to-end encryption. We support surveillance reform and frequently engage with various government and regulatory bodies to advocate the same.

Your rights: In addition to your rights under EU law, the SCCs and US law, you also have the right to submit a complaint or request to the [Privacy Shield Ombudsperson](https://www.privacyshield.gov/article?id=How-to-Submit-a-Request-Relating-to-U-S-National-Security-Access-to-Data) in the United States.

Does Meta provide data in response to US government requests?

Your data may be subject to requests by US government agencies (including US national security authorities) when you use our products and services. We have robust policies to ensure every government request is scrutinized no matter which government makes the request. Meta must comply with valid and compulsory legal requests from US government agencies. These requests must be made in accordance with applicable law and our policies (including [Meta’s Data Policy](https://www.facebook.com/policy.php)), and we produce only the information that is narrowly tailored to respond to each request.

What government agencies submit requests for user data to Meta technologies?

Meta receives requests for user data from government agencies around the world. We share details about these requests and our responses twice a year in our Government Requests for User Data Report [Government Requests for User Data Report](https://transparency.fb.com/data/government-data-requests/). Meta requires government agencies that make requests to comply with applicable law, our policies (including our [Data Policy](https://www.facebook.com/policy.php)), and, in certain scenarios, to use the Mutual Legal Assistance Treaty process.

How does Meta respond to government requests for European Economic Area (EEA) users’ data?

Meta scrutinizes every government request we receive, regardless of which government makes the request, to make sure it is legally valid. Meta requires government agencies that make requests comply with applicable laws and our policies. We only produce narrowly tailored user information in response to such requests, and only when we have a good faith belief that the response is required by law in that jurisdiction, affects users in that jurisdiction, and is consistent with internationally recognized standards. In certain scenarios, we may also require such governments to use the Mutual Legal Assistance Treaty process

Who reviews government requests for user data at Meta?

Meta has a dedicated, trained Law Enforcement Response Team (LERT) that reviews and evaluates every government request for user data individually, whether the request was submitted related to an emergency or through legal process obtained by law enforcement or national security authorities. This team ensures that all requests are consistent with applicable law and our policies (including [Meta’s Data Policy](https://www.facebook.com/policy.php)).

Our LERT is well-resourced and continues to grow in order to continue to ensure the careful review and evaluation of each government request. Each LERT analyst receives extensive, rigorous training in Meta’s policies and the requirements for government requests. LERT analysts also receive annual privacy and data protection training. LERT is supported by both in-house and retained outside counsel. These lawyers are experts in the laws that apply to government requests for data.

Does Meta individually review each request?

Yes. We individually review all requests regardless of whether they are submitted related to emergencies or through legal process obtained by law enforcement or national security authorities to ensure that each request is consistent with applicable law and our policies (including [Meta’s Data Policy](https://www.facebook.com/policy.php).).

Does Meta receive requests for emergency disclosure?

Yes. In emergencies, law enforcement may submit requests without legal process. Based on the circumstances, we may voluntarily disclose information to law enforcement where we have a good faith reason to believe that the matter involves imminent risk of serious physical injury or death.

Does Meta notify users (including advertisers) when their data is requested?

We notify users (including advertisers) about requests for their information before disclosing it unless we are prohibited by law from doing so or in exceptional circumstances, such as where a child is at risk of harm, emergencies or when notice would be counterproductive. We will also provide delayed notice upon expiration of a specific non-disclosure period in a court order and when we have a good faith belief that exceptional circumstances no longer exist and we are not otherwise prohibited by law from doing so.

What about Workplace customers?

Meta’s policy is to redirect government requesters to the Workplace customer in the first instance. If Meta is required to respond to a request for information relating to Workplace user data, Meta applies the same rigorous process as we would for any other request.

Does Meta ever push back on or challenge a request?

Yes. If we determine that a government request is not consistent with applicable law or our policies, we push back and engage the governmental agency to address any apparent deficiencies. If the request is unlawful (for example, overly broad, or legally deficient in any way), we will challenge or reject the request. We encourage governmental agencies to submit only requests that are necessary, proportionate, specific, and strictly compliant with applicable laws, by publishing [guidelines for government requests](https://www.facebook.com/safety/groups/law/guidelines/).

What data does Meta disclose in response to government requests?

Meta scrutinizes every government request and produces only the information that is narrowly tailored to respond to each request. Depending on the request, Meta may produce:

Basic subscriber information: Such as name, length of service, payment information, email addresses, and recent login/logout IP addresses.

Records pertaining to account activity: Such as message headers and IP addresses.

The stored contents of an account: Such as messages, photos, videos, timeline posts, and location information.

For additional information on data we disclose in response to government requests, please see our [guidelines for government requests](https://www.facebook.com/safety/groups/law/guidelines/).

How does Meta respond to requests made under the US Foreign Intelligence Surveillance Act (FISA)?

FISA authorizes the US government to request data related to US National Security. Section 702 of FISA allows the US government to target specified electronic communication service provider (ECSPs) accounts of non-US persons located outside the United States to acquire foreign intelligence information (including the content of communications). The requests must be for specified accounts with valid account identifiers. In addition, the government is permitted to request only account information it has reason to believe will include certain types of intelligence (for example, terrorism). All requests must conform to the terms of a court-approved certification with minimization requirements. View [government requests data for the United States](https://transparency.fb.com/data/government-data-requests/country/us/) for more information on FISA requests.

If Meta were to receive a FISA request, including targeted requests under Section 702, we would follow the same process we do for all government requests -- we would review each request individually to ensure compliance with applicable laws and our policies; push back or challenge any requests that were legally defective or overbroad; and produce only information that is narrowly tailored to respond to the request.

Does Meta receive and respond to requests under Executive Order 12333?

No. E.O. 12333 provides a legal framework for governing US intelligence activities to be conducted outside of the United States, but does not impose any obligations on a service provider like Meta.

Does Meta produce user data in response to US National Security Letters (NSL)?

Meta follows the same process for all government requests - we review each request individually; these requests must be made in accordance with applicable laws and our policies; and we produce only information that is narrowly tailored to respond to the request. Meta would produce the user’s name and length of service in response to a valid NSL. View [government requests data for the United States](https://transparency.fb.com/data/government-data-requests/country/us/) for more information on NSL requests.

Does Meta encrypt data in transit?

Yes. Meta employs industry standard encryption algorithms and protocols designed to secure and maintain the confidentiality of data in transit over public networks. Employing advanced encryption algorithms enables Meta to secure user data in transit from access by third parties.

Does Meta preserve data at the request of government agencies?

In response to valid government requests to preserve user data, we take steps to preserve account records in accordance with applicable law and our policies (including [Meta’s Data Policy](https://www.facebook.com/policy.php)).

Does Meta transfer data outside of the European Economic Area (EEA)?

Yes. In order to be able to provide our global services, it is essential for us to be able to share information globally - both internally within Meta, and externally with our partners and with those you connect and share with around the world. We do this in accordance with our [Data Policy](https://www.facebook.com/policy.php). Information controlled by Meta Platforms Ireland will be transferred or transmitted to, or stored and processed in, countries outside of where you live (and the European Economic Area), including the United States, for the purposes described in our [Data Policy](https://www.facebook.com/policy.php). These data transfers are necessary to operate and provide to you the global services described in the [Terms of Service for Facebook](https://www.facebook.com/legal/terms/) and the [Terms of Use for Instagram](#).

What is the Schrems II Judgment on data transfers?

In July 2020, the Court of Justice of the EU (CJEU) clarified the basis on which organizations can transfer data outside the EEA. The CJEU confirmed the validity of the SCCs (on which we rely), but made clear that organizations are responsible for ensuring that any data transferred is appropriately protected. Therefore, while we consider that in most cases the SCCs alone will continue to provide sufficient protection for transfers outside the EEA, including to the United States, we also have appropriate safeguards in place to protect your data.

In the Judgment, the CJEU also invalidated [Privacy Shield](https://www.privacyshield.gov/welcome), a framework for regulating transatlantic transfers of personal data from the EEA to the United States. Although we relied on Privacy Shield as the data transfer mechanism for certain products such as our Workplace product, we did not rely on Privacy Shield for transfers of our own users’ data. To the extent we relied on Privacy Shield, we have now migrated to the SCCs for these products but remain certified and committed to complying with the Privacy Shield framework in connection with some products, including Workplace and certain ad products.

What measures and safeguards have we put in place to protect your data when it is transferred to the United States?

We have in place a number of safeguards and measures to ensure an adequate level of protection for user data being transferred outside the EEA to the United States, including:

Security: We have a comprehensive security program to protect the data stored on our systems, platforms and products.

Encryption of data in transit so it cannot be read: Meta employs industry standard encryption algorithms and protocols designed to secure and maintain the confidentiality of data in transit over public networks. Employing advanced encryption algorithms enables Meta to secure user data in transit from access by third parties.

Policies and procedures:We have robust policies and procedures in place to ensure user data is adequately protected in relation to requests from governmental agencies. For example, we will only comply with a governmental request for user data after we are satisfied that the request complies with applicable law and our policies. If the request is unlawful (for example, overly broad, or legally deficient in any way), we will push back or challenge the request. We encourage governmental agencies to submit only requests that are necessary, proportionate, specific, and strictly compliant with applicable laws, by publishing [guidelines for government requests](https://www.facebook.com/safety/groups/law/guidelines/).

Oversight: We have a dedicated, trained Law Enforcement Response Team (LERT) that reviews and evaluates every government request for user data individually, whether the request was submitted related to an emergency or through legal process obtained by law enforcement or national security authorities. This team ensures that all requests are consistent with applicable law and our policies, including [Meta’s Data Policy](https://www.facebook.com/policy.php).

Government Requests for User Data Report: We publish information on government requests we receive in our [Government Requests for User Data Report](https://transparency.fb.com/data/government-data-requests/). Information regarding requests made under the US Foreign Intelligence Surveillance Act (FISA) is included in the report with the maximum level of detail permitted under US law.

Advocacy: We appreciate the focus of governments across the globe on protecting and safeguarding people’s data, including in the US and Europe, and we work hard to do our part. We actively engage with governments to encourage practices that protect peoples’ rights. We belong to advocacy groups like [Global Network Initiative](https://globalnetworkinitiative.org/about-gni/), whose mission is to advance the freedom of expression and privacy rights of Internet users worldwide; and are a founding member of [Reform Government Surveillance](https://www.reformgovernmentsurveillance.com/), which advocates for government data requests to be rule bound, narrowly tailored, transparent, subject to strong oversight and protective of end-to-end encryption. We support surveillance reform and frequently engage with various government and regulatory bodies to advocate the same.

Your rights: In addition to your rights under EU law, the SCCs and US law, you also have the right to submit a complaint or request to the [Privacy Shield Ombudsperson](https://www.privacyshield.gov/article?id=How-to-Submit-a-Request-Relating-to-U-S-National-Security-Access-to-Data) in the United States.

Does Meta provide data in response to US government requests?

Your data may be subject to requests by US government agencies (including US national security authorities) when you use our products and services. We have robust policies to ensure every government request is scrutinized no matter which government makes the request. Meta must comply with valid and compulsory legal requests from US government agencies. These requests must be made in accordance with applicable law and our policies (including [Meta’s Data Policy](https://www.facebook.com/policy.php)), and we produce only the information that is narrowly tailored to respond to each request.