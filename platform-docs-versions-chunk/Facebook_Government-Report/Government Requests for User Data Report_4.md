platform: Facebook
topic: Government-Report
subtopic: Government Requests for User Data Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Government-Report/Government Requests for User Data Report.md
url: https://transparency.fb.com/reports/government-data-requests/data-types/

# Data types

Government requests for user data include both routine legal process and emergency disclosure requests. For both request types, we report the number of requests received, the number of users/accounts requested, and the percentage of requests where we produced some data. We have reported this information since 2016.

Legal process requests

Requests from governments that are accompanied by legal process, like a search warrant. We disclose account records solely in accordance with our Terms of Service and applicable law.

Emergency disclosure requests

In emergencies, law enforcement may submit requests without legal process. Based on the circumstances, we may voluntarily disclose information to law enforcement where we have a good faith reason to believe that the matter involves imminent risk of serious physical injury or death.

Legal process requests

Requests from governments that are accompanied by legal process, like a search warrant. We disclose account records solely in accordance with our Terms of Service and applicable law.

Emergency disclosure requests

In emergencies, law enforcement may submit requests without legal process. Based on the circumstances, we may voluntarily disclose information to law enforcement where we have a good faith reason to believe that the matter involves imminent risk of serious physical injury or death.

[Download (CSV)](https://transparency.fb.com/sr/government-requests/)