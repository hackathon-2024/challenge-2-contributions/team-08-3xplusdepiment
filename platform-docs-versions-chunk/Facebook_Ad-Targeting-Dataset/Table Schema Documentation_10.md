platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Table Schema Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Table Schema Documentation.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/table-schema


## Learn more

* [Custom audience](https://www.facebook.com/business/help/744354708981227)
    
* [Customer list](https://www.facebook.com/business/help/606443329504150)
    
* [Lookalike audience](https://www.facebook.com/business/help/164749007013531)
    
* [Ad creative](https://developers.facebook.com/docs/marketing-api/reference/ad-creative/)
    
* [Ad Library data](https://facebook.com/ads/library)
    
* [Facebook Terms of Service](https://www.facebook.com/terms.php)
    
* [ISO currency code](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.iso.org%2Fiso-4217-currency-codes.html%3Ffbclid%3DIwAR2dth6_P9G_7MsN9Vd_0KjbF_RfruHJjqUacKNZO-_i61kwVCQ75PVIlyQ&h=AT27IfCgYybIgAE2zECiPZZJeKe_EuAQLIVKfWfYTADMDFCafSXi_odHkO95sD2k1m7QRwF7eCfk_Jg6A_vaOApmRPIh8o7EV4_d9NxvjMH8DOZYmHVmGmVDc6s_ADs5fVugL136cWVJ7BnMuHn0fmnwEPUB1A)
    
* [Audience distribution](https://developers.facebook.com/docs/graph-api/reference/audience-distribution/)
    
* [Insights range value](https://developers.facebook.com/docs/graph-api/reference/insights-range-value/)
    
* [Estimated audience size](https://www.facebook.com/business/help/1665333080167380?id=176276233019487)
    
* [ISO 639-1 language codes](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.iso.org%2Fstandard%2F22109.html%3Ffbclid%3DIwAR3m8L9GO0eQRNpQ6JErJDiLs6n4U5nWZHp7ObPKwdO60O77MwULJ79rfCQ&h=AT3yFQ6jA7WAp4gNU9D_jbqwe9y8Yw52sKbwMRaT9Sy63kmgHlOe4RMYaqsY0ouP9VqMP0_UThc_zWuh3_2erD_UmcEZgFuV1Z_PgcBNlTihMkRpFZNQpvVdnQUB6Usa1iiYtCp5RSlKH-WY)
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)