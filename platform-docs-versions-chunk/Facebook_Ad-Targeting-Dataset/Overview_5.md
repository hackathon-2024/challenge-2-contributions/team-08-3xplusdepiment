platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Overview.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/overview

## Researcher Platform

You must use our Researcher Platform web app to access the dataset. The app runs on JupyterHub, which allows you to import our Python3 SQL query module into a Jupyter Notebook and run SQL queries against the dataset. You can access Researcher Platform [user documentation here](https://developers.facebook.com/docs/researcher-platform/).

The Researcher Platform URL is emailed to you once you have [completed the onboarding process](https://developers.facebook.com/docs/fort/get-access).

## VPN

You must access the Researcher Platform through our virtual private network (VPN) using OpenVPN. Follow our [OpenVPN Setup](https://developers.facebook.com/docs/fort/get-access/vpn) document to learn how to install and configure the OpenVPN client correctly.