platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Overview.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/overview


## Dataset

The dataset comprises a single table containing targeting data of social issue, electoral, and political ads. All data used to build the dataset has been processed to remove any personally identifiable user information.

Only ads concerning social issues, elections, and politics that were run on or after August 3, 2020 are included in the data. Ads included are from all countries in which we currently have our ad authorizations and disclaimer tools available. More information about the included countries is available [here.](https://www.facebook.com/business/help/2150157295276323?id=288762101909005)

Publication of this data began in May of 2022.

On the 1st of each month, an update to the table is published so that it includes all data through the 21st of the previous month. Beginning with the second month of published data (June 2022), there is also a delta table that describes ads that were reclassified as non-political since the previous update. The first three months of data (August 3 - November 1, 2020) were originally published as the Ad Targeting February 2021 Dataset (with U.S. data only).

**Ads are retained for seven years**

We store all social issue, electoral and political ads in our Ad Library for seven years. In line with this, data in the Ad Targeting dataset is not accessible after seven years.