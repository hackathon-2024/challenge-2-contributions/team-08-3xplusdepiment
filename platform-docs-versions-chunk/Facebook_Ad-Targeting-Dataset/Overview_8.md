platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Overview.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/january-2021-dataset

# Updates and differences in the datasets

The Ad Targeting February 2021 dataset consists of targeting data on social issue, electoral, and political ads from August 3 - November 1, 2020 (U.S. data only).

The Ad Targeting May 2022 dataset consists of targeting data on social issue, electoral, and political ads beginning August 3, 2020 through the present. The May 2022 dataset includes ads from all countries in which we currently have our ad authorizations and disclaimer tools available.

Both datasets include data from August 3 - November 1, 2020.