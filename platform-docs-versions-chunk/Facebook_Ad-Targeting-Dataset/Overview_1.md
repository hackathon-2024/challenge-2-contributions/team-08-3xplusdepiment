platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Overview.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/overview

# Overview

The Ad Targeting Dataset contains the ad targeting logic of all of the Social Issue, Electoral, and Political (SIEP) ads run beginning August 3, 2020 on the Facebook and Instagram platforms.

This document provides an overview of the dataset and its usage requirements.

**Get access**

To query the dataset, you must be an approved Meta Research Partner. See [Onboarding](https://developers.facebook.com/docs/fort/get-access) to get access.