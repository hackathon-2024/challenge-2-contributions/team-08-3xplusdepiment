platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Overview.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/overview

## How it works

Once you have [completed onboarding](https://developers.facebook.com/docs/fort/get-access), this is the general flow for querying the dataset:

1. Connect to our VPN.
2. Visit the Researcher Platform URL that was emailed to you and sign in with your Facebook account.
3. Create a Python3 Jupyter notebook.
4. Import our query module.
5. Use the module to create and execute SQL queries.
6. Examine the results returned to your Jupyter notebook.

## Next steps

Read our [Get started](https://developers.facebook.com/docs/fort-ads-targeting-dataset/get-started) guide to learn how to use the Researcher Platform and perform a basic SQL query.