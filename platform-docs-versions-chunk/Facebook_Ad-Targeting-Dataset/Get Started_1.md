platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Get Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Get Started.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/get-started

# Get started

This tutorial will show you how to get set up to the point that you can use Meta's Researcher Platform to perform basic SQL queries on the Ad Targeting dataset.

## Before you begin

Make sure you have completed the [onboarding process](https://developers.facebook.com/docs/ad-targeting-dataset/get-access) which includes creating or joining a Research App, completing individual and academic verifications, and App review.

## Install and configure OpenVPN

Our products can only be accessed through our Virtual Private Network (VPN). This guide shows you how to install and configure the OpenVPN client and connect to our VPN server. You use your Apps Dashboard to set up your VPN access.

While you are connected to our VPN server, all of your internet traffic is routed through it, so be sure to disconnect when you are finished.

### Step 1: Access your Apps Dashboard

Go to your [Apps Dashboard](https://developers.facebook.com/apps).