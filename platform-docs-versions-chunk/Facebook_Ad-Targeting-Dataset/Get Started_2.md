platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Get Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Get Started.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/get-started

### Step 2: Select the app

Select the app that has been granted access to the dataset of interest.

If your App has not been approved please refer to [App Review](https://developers.facebook.com/docs/ad-targeting-dataset/app-review) for instructions.

### Step 3: Install OpenVPN

Download the latest version of the [OpenVPN Client](https://l.facebook.com/l.php?u=https%3A%2F%2Fopenvpn.net%2Fvpn-client%2F&h=AT1LV61KhhNwoYuyLsjovWMMJWUYMDVHCFJk9AjMxO7Qlsp5QXRLVC0mVjMFgP0JBuxBs84YhhzB9Xs5IW3Ek24q-KQ0XvN1xqk5k4_pDKgkSzTopvS7rNy4F_sycKIJqouYqlivJn7x2DEj) and install the application on your computer.