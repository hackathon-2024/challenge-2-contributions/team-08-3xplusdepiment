platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Overview.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/overview

## Learn more

For the dataset's codebook, see [Ad Targeting Dataset Codebook.](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.8562-6/327966766_556592703070288_3426165002794598818_n.pdf?_nc_cat=102&ccb=1-7&_nc_sid=b8d81d&_nc_ohc=GRhe2aAlz-EAX8FVFbY&_nc_oc=AQk21F0OK0ES3OFmkk4RZcu7U6qEufNTl34flR44ZlaU067VEtkTCEvxN8VfaolXpY8&_nc_ht=scontent-cdg4-1.xx&oh=00_AfDEcqoyNQ1eJ2jpwnNqWi9Clihc3SwQRqbwB8qV_MSRCQ&oe=65BF8BE4)

Additional resources:

* [Updates and differences in the datasets](https://developers.facebook.com/docs/fort-ads-targeting-dataset/january-2021-dataset)
* [Onboarding for access to the Ad Targeting dataset](https://developers.facebook.com/docs/fort/get-access)
* [OpenVPN setup](https://developers.facebook.com/docs/fort/get-access/vpn)

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)