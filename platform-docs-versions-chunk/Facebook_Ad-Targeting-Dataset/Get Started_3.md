platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Get Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Get Started.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/get-started

### Step 4: Generate Credentials

In your App Dashboard, navigate to **Settings** > **Basic**. In the new page that displays, scroll down to the "Connect to our VPN" section and click **Generate Credentials**. The button looks similar to this:

This downloads a credentials file that you can import into the OpenVPN Client you installed.

If the Generate Credentials button is grayed out, your selected app has not completed app review. You can navigate to **Dashboard** from the left side navigation panel to check on the status.

### Step 5: Launch and connect

Launch **OpenVPN** and import the file you generated.

Check the **Connect after import** checkbox, then click **Add** to add the imported file to your profile.

Once the import is complete and the connection to the VPN is established, OpenVPN displays a clock indicating that you are connected. You should then be able to access Researcher Platform and the Ad Targeting dataset.

## Get started in Researcher Platform