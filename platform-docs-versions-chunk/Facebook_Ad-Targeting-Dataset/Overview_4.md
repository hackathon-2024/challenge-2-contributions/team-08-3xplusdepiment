platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Overview.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/overview


## Important dates

It might be helpful to know these important dates as you work with the dataset:

| Significance | Date |
| --- | --- |
| Beginning date of both the original February 2021 dataset and the new May 2022 dataset. | August 3, 2020 |
| Last date included in the original February 2021 dataset. | November 1, 2020 |
| Dates included in both the original and new datasets. | August 3 - November 1, 2020 |
| First publication of the new dataset. | May 31, 2022<br><br>  <br><br>Includes data from August 3, 2020 - April 21, 2022 |
| First monthly update to the new dataset. | June 1, 2022<br><br>  <br><br>Updates the dataset to include data from August 3, 2020 - May 21, 2022 |
| First publication of the [delta table](https://developers.facebook.com/docs/fort-ads-targeting-dataset/table-schema#delta-schema). | June 1, 2022 |
| Date we began to preserve custom audience information for audiences that were initially used and later removed by advertisers. | November 1, 2021<br><br>  <br><br>See [Updates and Differences in the Datasets](https://developers.facebook.com/docs/fort-ads-targeting-dataset/january-2021-dataset) for more information. |
| Date the `fbid` column name in the `ad_archive_api` table changed from `ad_archive_id` to `fbid`. | April 21, 2021<br><br>  <br><br>See [Joining Ads Targeting Dataset Data with Ad Library Data](https://developers.facebook.com/docs/fort-ads-targeting-dataset/table-schema#joining-ad-library) in the Table Schema section for more information. |
| Date changes were made to the table schema to more clearly and accurately represent included and excluded targeting criteria. | September 1, 2022<br><br>  <br><br>See [Changelog](https://developers.facebook.com/docs/fort-ads-targeting-dataset/support/changelog) for details. |
| Date as of which researchers could apply for access to the Ad Targeting Dataset by submitting a data access request and completing the required, self-serve onboarding process. | September 7, 2022<br><br>  <br><br>See [Meta for Developers Onboarding](https://developers.facebook.com/docs/fort/get-access) for more information. |