platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Overview.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/overview

## Issues with inclusions and exclusions of custom and lookalike audiences

For the dataset of targeting data on social issue, electoral and political ads for the 90-day period leading up to the US 2020 election, released in February 2021, there were some issues with inclusions and exclusions of custom and lookalike audiences.

Also, for ads created before November 1, 2021, data may be missing from some ads where advertisers initially used a custom or lookalike audience, but later deleted that audience.

We are addressing the issues we’ve discovered by providing updated documentation on how to proceed.

See [Updates and differences in the datasets](https://developers.facebook.com/docs/fort-ads-targeting-dataset/january-2021-dataset) for a comparison of the original and new datasets and more information about these issues.