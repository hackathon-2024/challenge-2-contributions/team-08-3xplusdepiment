platform: Facebook
topic: Ad-Targeting-Dataset
subtopic: Get Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Targeting-Dataset/Get Started.md
url: https://developers.facebook.com/docs/ad-targeting-dataset/get-started

## Next steps

If you are able to perform the query above, you are able to perform a basic search using our web app. We recommend that you now read a few [sample queries](https://developers.facebook.com/docs/ad-targeting-dataset/sample-queries) to get an idea of how to build your own custom queries.

## Learn more

* [Researcher Platform](https://developers.facebook.com/docs/researcher-platform/)