platform: Booking.com
topic: DSA-Home-Page
subtopic: Developer Terms
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_DSA-Home-Page/Developer Terms.md
url: <EMPTY>

### II. Registre des publicités

Booking.com a créé [ce registre des publicités](https://booking.com/ad-repository.html) afin d'être en conformité avec la législation européenne sur les services numériques, suite à sa désignation en tant que « très grande plateforme en ligne » (VLOP, pour « very large online platform »). Ce registre contient des informations sur les publicités qui ont été affichées sur le site de Booking.com.  
  

### III. Transparency report

Booking.com has prepared a transparency report to comply with its DSA obligations. This report provides insights into the content moderation activities that we engaged in during the reporting period, including the volume and nature of content removed from our platform and removal requests received from public authorities and users. The report can be downloaded [here](https://r-xx.bstatic.com/data/mobile/dsa_transparency_report_bf3fdc24.pdf).