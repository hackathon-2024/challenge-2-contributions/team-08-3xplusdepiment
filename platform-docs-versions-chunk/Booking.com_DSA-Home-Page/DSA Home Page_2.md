platform: Booking.com
topic: DSA-Home-Page
subtopic: DSA Home Page
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_DSA-Home-Page/DSA Home Page.md
url: https://www.booking.com/content/dsa.fr.html


### I. Destinataires mensuels actifs dans l'UE

Conformément aux obligations de Booking.com en vertu de la législation sur les services numériques de l’UE, nous estimons que le nombre moyen mensuel de destinataires\* du service Booking.com dans l’Union européenne, du 1er février 2023 au 31 juillet 2023 inclus, est nettement supérieur à 45 millions.  
  
Il ne s’agit que d’une estimation basée sur les données dont dispose Booking.com à l’heure actuelle et sur les directives limitées de la législation sur les services numériques (« DSA »). Cette estimation doit être publiée en vertu de la DSA et ne doit pas être utilisée à d’autres fins. Les méthodologies utilisées pour estimer le nombre moyen mensuel de destinataires tel que défini dans la DSA nécessitent un jugement et des données de conception importants, sont soumises à des données et à d’autres limitations, et sont intrinsèquement sujettes à des écarts statistiques et à des incertitudes. Cette estimation peut être révisée à la hausse ou à la baisse au fur et à mesure que Booking.com affine son approche et en réponse à la publication de la méthodologie par la Commission européenne.  
  
Veuillez consulter le site Web [Booking Holdings Inc. Investor Relations](https://ir.bookingholdings.com/overview/default.aspx) pour les mesures que nous considérons pertinentes pour les activités de Booking.com.  
  
\* « Destinataire du service » est défini dans la DSA comme « toute personne physique ou morale qui utilise un service intermédiaire, notamment aux fins de rechercher des informations et de les rendre accessibles ». Cela nécessite de compter les utilisateurs auxquels les informations ont été affichées par le service Booking.com, même si cet utilisateur n’a pas effectué de transaction.