platform: Bing
topic: DSA-Home-Page
subtopic: DSA Home Page
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_DSA-Home-Page/DSA Home Page.md
url: https://support.microsoft.com/en-gb/topic/eu-digital-services-act-information-6b16b41f-2fa5-4e64-a8d3-033958812642

## DSA Point of Contact – European Commission and Member State Authorities

Pursuant to Article 11 of the DSA, Microsoft Ireland Operations Limited has designated [DigitalServicesAct@microsoft.com](mailto:DigitalServicesAct@microsoft.com) as the single point of contact for direct communications with the European Commission, Member States’ Authorities, and the European Board for Digital Services in connection with the application of the DSA. English is the preferred language for communication with this point of contact.

When sending messages to [DigitalServicesAct@microsoft.com](mailto:DigitalServicesAct@microsoft.com) please be sure to include your full name and the name of the EU-based authority on whose behalf you are contacting us. We’ll also need an email address to contact you, which should be associated with the relevant EU-based authority. 

This point of contact is reserved for engagement with the authorities listed above. For other types of inquiries, please use the mechanisms described below.