platform: Bing
topic: DSA-Home-Page
subtopic: Developer Terms
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_DSA-Home-Page/Developer Terms.md
url: <EMPTY>

## EU Monthly Active Users

In accordance with the Digital Services Act (DSA), Microsoft Ireland Operations Limited publishes information semi-annually on its average monthly active users in the European Union, calculated over a six-month period. For the six-month period ending June 30, 2023, Microsoft Bing had approximately 119 million average monthly active users. This information was compiled pursuant to the DSA and thus may differ from other user metrics published by Microsoft Bing.