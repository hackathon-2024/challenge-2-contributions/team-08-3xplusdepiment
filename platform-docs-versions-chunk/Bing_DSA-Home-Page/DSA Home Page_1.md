platform: Bing
topic: DSA-Home-Page
subtopic: DSA Home Page
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_DSA-Home-Page/DSA Home Page.md
url: https://support.microsoft.com/en-gb/topic/eu-digital-services-act-information-6b16b41f-2fa5-4e64-a8d3-033958812642

## EU Monthly Active Users

In accordance with the Digital Services Act (DSA), Microsoft Ireland Operations Limited publishes information semi-annually on its average monthly active users in the European Union, calculated over a six-month period. For the six-month period ending June 30, 2023, Microsoft Bing had approximately 119 million average monthly active users. This information was compiled pursuant to the DSA and thus may differ from other user metrics published by Microsoft Bing.