platform: Bing
topic: DSA-Home-Page
subtopic: Developer Terms
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_DSA-Home-Page/Developer Terms.md
url: <EMPTY>

## Point of Contact for Bing Users

Bing offers a variety of ways for users to contact us.  If you have a concern about particular URLs or other information you encounter on Bing, you may report these to Bing using our [Report a Concern tool](https://go.microsoft.com/fwlink?LinkId=850876). Feedback about Bing can also be submitted directly using the "Feedback” link located at the bottom of most Bing search pages. Additional information is available at our “[How to report a concern or Contact Bing](https://support.microsoft.com/en-gb/topic/how-to-report-a-concern-or-contact-bing-1831f0fe-3c4d-46ae-8e57-16c487715729)” help page. 

## EU Advertising Repository

Please visit the [Microsoft Ad Library](https://adlibrary.ads.microsoft.com/) for a repository of advertisements served by the Microsoft Advertising Network to Bing users located in the European Union.