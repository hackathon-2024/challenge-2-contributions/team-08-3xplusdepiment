platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-access

# Get access

Meta is partnering with the Inter-university Consortium for Political and Social Research (ICPSR) at the University of Michigan to vet applications and assist in onboarding to Meta Content Library and API.