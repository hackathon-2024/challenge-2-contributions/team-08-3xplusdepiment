platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-access

### **Step 3: Gain access.**

Access to Meta Content Library and Content Library API will be provisioned once all access requirements are fulfilled. In order to continue accessing the tools, you will need to recertify your affiliation with your institution and research project on an annual basis.