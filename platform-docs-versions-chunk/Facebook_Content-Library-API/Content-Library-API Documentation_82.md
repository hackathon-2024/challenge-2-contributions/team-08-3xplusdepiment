platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/guide-search-object

## Learn more

* [Rate limiting and query budgeting](https://developers.facebook.com/docs/content-library-api/rate-limiting)
* [Advanced search guidelines](https://developers.facebook.com/docs/content-library-api/adv-search)
* [Search quality approach](https://developers.facebook.com/docs/content-library-api/search-quality)

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)