platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-access

### **Step 2: Fulfill additional data access requirements.**

The Meta Content Library API is available for researchers to access on ICPSR’s Virtual Data Enclave. A Data Use Agreement between ICPSR and your institution must be executed before you are granted access to the platform. More information about ICPSR’s requirements can be found in [ICPSR’s application guide](https://l.facebook.com/l.php?u=https%3A%2F%2Fdocs.google.com%2Fdocument%2Fd%2F1iN4KOvFaYGZro23cB4j1FveouXMBcZnKl-yTUyx6fCg%2Fedit%3Fusp%3Dsharing&h=AT2BrWHEFA13x0VKdcfzn2TLbZ1KFS_uI7U988YxliWGwxhQ99FW71E0CZwOY9lTjJRBHu9pnDx0wxvT1VPZyixyWl7KZQ4kQeWVQNty3Sg4MsuIhhGMPB4Qx_MR432iroeoKWR5LqVo5dAE).

Meta will follow up with approved researchers with information on how to access and onboard to the Meta Content Library user interface (UI). The UI can be used separately or in conjunction with the Content Library API.