platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/search-quality

### Advanced search

Advanced search includes searching keyword combinations with logic operators AND, OR and NOT. The following table shows all metric values across advanced search test queries by endpoint from November 8th, 2023 to November 15th, 2023. Representativeness is an average across all dimensions that are mentioned in the representativeness section.

| Entity type | Recall | Representativeness - Paired t-test | Representativeness - Welch t-test |
| --- | --- | --- | --- |
| Facebook Page | 95% | 77% | 94% |
| Facebook group | 93% | 71% | 93% |
| Facebook event | 90% | 59% | 83% |
| Facebook post | 83% | 70% | 93% |
| Instagram account | 98% | 79% | 99% |
| Instagram post | 78% | 83% | 99% |