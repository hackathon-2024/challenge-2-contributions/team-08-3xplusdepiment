platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data


## Facebook group

| Name | API field | Description | Products |
| --- | --- | --- | --- |
| Meta Content Library ID | id  | The Meta Content Library ID associated with the Facebook group. | Content Library API |
| Name | name | The name of the Facebook group. | Content Library<br><br>  <br><br>Content Library API |
| Description | description | The description of the Facebook group. | Content Library<br><br>  <br><br>Content Library API |
| Creation date | creation\_date | The date the Facebook group was created. | Content Library<br><br>  <br><br>Content Library API |
| Group original name | group\_transparency.original\_name | The original name of the Facebook group. | Content Library API |
| Group name change date | group\_transparency.name\_changes.data.date | The date the name of the Facebook group changed. | Content Library<br><br>  <br><br>Content Library API |
| Group name new | group\_transparency.name\_changes.data.new\_name | The new name of the Facebook group. | Content Library API |
| Group admin and moderator Page IDs | group\_transparency.admin\_and\_moderator\_page\_ids | The list of Meta Content Library IDs of Facebook Pages that are admins or moderators of the Facebook group. | Content Library API |
| Group owner type | group\_owners.data.type | The type of the group owner associated with the Facebook group.This field will display if the group owner is a professional profile or Page. Only the Meta Content Library Page ID will be shared. | Content Library API |
| Group owner ID | group\_owners.data.id | The Meta Content Library ID of the Facebook group owners.This field will display if the group owner is a professional profile or Page. Only the Meta Content Library Page ID will be shared. | Content Library API |
| Picture URL | picture\_url | The photo URL of the Facebook group. | Content Library API |
| Group members | member\_count | The number of members of the Facebook group. | Content Library<br><br>  <br><br>Content Library API |