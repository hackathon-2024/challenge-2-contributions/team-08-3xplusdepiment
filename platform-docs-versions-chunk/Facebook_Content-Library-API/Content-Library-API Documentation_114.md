platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data


## Facebook Page

| Name | API field | Description | Products |
| --- | --- | --- | --- |
| Meta Content Library ID | id  | The Meta Content Library ID associated with the Facebook Page. | Content Library API |
| Name | name | The name of the Facebook Page. | Content Library<br><br>  <br><br>Content Library API |
| About | about | The short paragraph in the About section of the Facebook Page. | Content Library<br><br>  <br><br>Content Library API |
| Website | website | The external URL from the Facebook Page’s About section. | Content Library API |
| Description | description | The long paragraph in the About section of the Facebook Page. | Content Library<br><br>  <br><br>Content Library API |
| Verification status | verification\_status | The verification status of the Facebook Page. A Facebook Page with a verified badge indicates that Facebook has confirmed that it is the authentic presence for that person or brand. [Learn more.](https://www.facebook.com/help/196050490547892) | Content Library<br><br>  <br><br>Content Library API |
| Page categories | page\_categories | The list of up to three categories of the Facebook Page, selected by the Page manager. | Content Library<br><br>  <br><br>Content Library API |
| Location city | location.city | The self-reported, publicly accessible city location associated with the Facebook Page. | Content Library API |
| Location country | location.country | The self-reported, publicly accessible country location associated with the Facebook Page. | Content Library API |
| Location country code | location.country\_code | The self-reported, publicly accessible country code location associated with the Facebook Page. | Content Library API |
| Location name | location.name | The self-reported, publicly accessible location name associated with the Facebook Page. | Content Library API |
| Location region | location.region | The self-reported, publicly accessible region location associated with the Facebook Page. | Content Library API |
| Location zip | location.zip | The self-reported, publicly accessible location zip associated with the Facebook Page. | Content Library API |
| Location street | location.street | The self-reported, publicly accessible street location associated with the Facebook Page. | Content Library API |
| Location state | location.state | The self-reported, publicly accessible state location associated with the Facebook Page. | Content Library API |
| Page name change date | page\_transparency.page\_name\_changes.data.date | The date the Facebook Page’s name changed. | Content Library API |
| Page name old | page\_transparency.page\_name\_changes.data.old\_name | The old name of the Facebook Page prior to the name change. | Content Library API |
| Page name new | page\_transparency.page\_name\_changes.data.new\_name | The new name of the Facebook Page, following the name change. | Content Library API |
| Page merged date | page\_transparency.page\_merges.data.date | The date another Facebook Page merged with this Page. | Content Library API |
| Page merged | page\_transparency.page\_merges.data.page\_merged | The name of the Facebook Page that merged with this Page. | Content Library API |
| Creation date | creation\_date | The date the Facebook Page was created. | Content Library<br><br>  <br><br>Content Library API |
| Page manager countries | page\_transparency.admin\_countries.data.country | The predicted primary country locations of people who manage this Facebook Page. | Content Library<br><br>  <br><br>Content Library API |
| Count of Page managers by countries | page\_transparency.admin\_countries.data.count | The number of people who manage this Facebook Page predicted to be from the associated country. | Content Library<br><br>  <br><br>Content Library API |
| Page owner | page\_transparency.confirmed\_page\_owner | The confirmed owner associated with the Facebook Page. | Content Library API |
| Has active ads | page\_transparency.has\_active\_ads | Whether the Facebook Page has active ads or not. | Content Library API |
| Has run political ads | page\_transparency.has\_run\_political\_ads | Whether the Facebook Page has run political ads or not. | Content Library API |
| Followers | follower\_count | The number of followers of the Facebook Page. | Content Library<br><br>  <br><br>Content Library API |
| Page likes | like\_count | The number of likes of the Facebook Page. | Content Library API |