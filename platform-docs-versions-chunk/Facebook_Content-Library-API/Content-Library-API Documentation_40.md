platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/guide-fb-events

# Guide to Facebook events data

You can perform Facebook event searches by calling the Meta Content Library API client's `search_events()` method. This document describes the `search_events()` method and its syntax and parameters, and shows how to perform basic queries using the method.

All of the examples in this document assume you have already created a Python or R Jupyter notebook and have created a client object. See [Getting started](https://developers.facebook.com/docs/content-library-api/quick-start) to learn more.

See [Data dictionary](https://developers.facebook.com/docs/content-library-api/data#dd-fb-event) for detailed information about the fields that are available on a Facebook event node.