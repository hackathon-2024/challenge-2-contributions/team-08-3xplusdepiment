platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/changelog

## Version v1.0 update, December 15 2023

The following changes were implemented:

* Search quality metrics were updated/improved. See [Search quality approach](https://developers.facebook.com/docs/content-library-api/search-quality).
    
* Due to the improved search quality, the "beta" designation on version 1.0 was removed.
    
* The citation format was updated with a new Digital Object Identifier (DOI) that does not include a beta designation. See [Citations](https://developers.facebook.com/docs/content-library-api/citations).
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)