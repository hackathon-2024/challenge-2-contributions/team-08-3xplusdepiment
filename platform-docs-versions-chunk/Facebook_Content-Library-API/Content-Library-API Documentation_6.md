platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/overview

## Learn more

* [Frequently asked questions](https://developers.facebook.com/docs/content-library-api/disclosures)
    
* [Search quality approach](https://developers.facebook.com/docs/content-library-api/search-quality)
    
* [Researcher Platform](https://developers.facebook.com/docs/researcher-platform)
    
* [Meta Content Library](https://www.facebook.com/transparency-tools/content-library/dataset/1119037145491882/about/)
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)