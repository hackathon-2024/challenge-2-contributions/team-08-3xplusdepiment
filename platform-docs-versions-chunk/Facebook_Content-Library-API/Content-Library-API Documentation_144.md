platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/search-quality

### Test search scope for endpoints

The following table summarizes the test search scope for all endpoints

| Endpoint | Test search scope |
| --- | --- |
| Facebook Page | Eligible Facebook pages created within the year prior to the query date |
| Facebook group | All eligible Facebook groups |
| Facebook event | Eligible Facebook events created within the year prior to the query date |
| Facebook post | Eligible posts created within two days prior to the given query date |
| Instagram account | Eligible Instagram Creator and Business accounts created within a year prior to the query date |
| Instagram post | Eligible posts created within a day prior to the given query date |