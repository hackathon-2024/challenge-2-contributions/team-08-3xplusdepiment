platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data-deletion

# Data deletion

Researcher Platform JupyterHub environment programmatically deletes all Meta Content Library API research output data and generated local files from JupyterHub environments every 30 days. This ensures that updates made to the visibility of content on Facebook or Instagram are carried over to Researcher Platform in accordance with Meta data deletion policies.

The data deletion is carried out during a 24-hour maintenance window the first day of every month beginning at 12:00 AM PST and ending at 11:59 PM PST on the same day, during which you cannot log in to your JupyterHub environment.