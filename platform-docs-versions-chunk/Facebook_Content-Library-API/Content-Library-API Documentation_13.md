platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-access


## Frequently asked questions about access, eligibility, and publication

#### Are global researchers eligible for access to Content Library and API?

We welcome global researchers, but access to or use of our tools and data must comply with all applicable laws, rules and regulations. In addition, the researcher applicant, and any academic university or institution with which the applicant is affiliated, must not be in a jurisdiction that is the target of sanctions imposed by the United States, United Kingdom, European Union or United Nations.

#### Do researchers have to pay to access Content Library and API?

No. There are no fees associated with access or computation.

#### Do researchers have to reapply for access if they change their research topic or proposal?

If the researcher has made any substantive changes to their research topic, proposal or research ethics documentation since having their application approved by the Inter-university Consortium for Political and Social Research (ICPSR), they must reapply for access.

#### Do global researchers affiliated with institutions not in the United States still need to apply through ICPSR?

Yes. All Content Library and API researcher applications are processed and reviewed by ICPSR.

#### How does the application and access process work for researchers who collaborate together or as part of a larger research team?

ICPSR will process and review applications from teams of researchers with a clearly identified research lead and collaborators. For more information please reference the ICPSR [application page.](https://l.facebook.com/l.php?u=https%3A%2F%2Fsomar.infoready4.com%2F%23freeformCompetitionDetail%2F1910793&h=AT0zCIa7wEHdY3xzVHqe519iM3KrQO1yMBASsu-Yne2ptRyUJUqGcBs41HJjyJYjf9IALNFxTS6Qj1EqjPr-Kk_i1_ep_Mbqz-wjZaGAYFuUXzz0HOnkVPj3fWmHbm75eA5HLZeiY_kedlDY)

#### Does Meta place any restrictions on how I can publish with data from the Meta Content Library and API?

Researchers may publish Research Outputs (such as tables, graphs, and analysis), but may not publish any Confidential Information or Personal Data. For publications based solely on Content Library and/or API, Meta will not ask to review research manuscripts prior to publication. However, we ask that researchers follow Meta’s attribution guidelines (see [Citations](https://developers.facebook.com/docs/content-library-api/citations)) and provide notice upon publishing.

Please refer to the [Product Terms for Meta Research Tools](https://l.facebook.com/l.php?u=https%3A%2F%2Ftransparency.fb.com%2Fresearchtools%2Fproduct-terms-meta-research&h=AT32BdF80pQD6DJkGa3ypSE9sksL7mnepiZrSdxEHMlqoy96YZfaf4_1Fk2xXqyH8BvatYw4vwp0gtR6qNUDLYkLlKVhK_susZxqiLrHeUQK11JmhklP06P6zSEn9Yf9XGOOeUG3dCSzKvoH) for the requirements and conditions related to publication.