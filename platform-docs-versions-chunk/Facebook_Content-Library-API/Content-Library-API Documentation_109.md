platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data-deletion

## The maintenance window

The maintenance window, during which you will be unable to log into your JupyterHub environment, begins on the first day of every month at 12:00 AM PST. It lasts for 24 hours and ends at 11:59 PM PST on the same day.

When the maintenance window begins, this message is displayed in JupyterHub and remains until the window ends:

![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.8562-6/357539214_1310321563203938_4085783015753068939_n.png?_nc_cat=110&ccb=1-7&_nc_sid=f537c7&_nc_ohc=stff3SxQt00AX_ADLTY&_nc_ht=scontent-cdg4-1.xx&oh=00_AfBy9VvF4VrCgyuroQq4ywuFK2G089dY8--OCzBJOuou2A&oe=65BF86A3)