platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-api-code

### Paste the snippet into a notebook cell

Paste the code representing your search (query) into a blank cell in your Jupyter notebook. Be sure the language (R or Python) of the notebook matches the language of the code you copied. If you have not already set up your Jupyter environment, see [Getting started](https://developers.facebook.com/docs/content-library-api/quick-start) for guidance.

![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.8562-6/278392598_315887847317792_4897557074894235014_n.png?_nc_cat=105&ccb=1-7&_nc_sid=f537c7&_nc_ohc=IWgWj5JbcNMAX9ybYB0&_nc_ht=scontent-cdg4-1.xx&oh=00_AfBdyZRLWw19weTAz07hYpqwf4BwAsLQkHYFmtCXLIxizw&oe=65BFA583)

### Run the code

Click the triangular button to run the code and display the results.