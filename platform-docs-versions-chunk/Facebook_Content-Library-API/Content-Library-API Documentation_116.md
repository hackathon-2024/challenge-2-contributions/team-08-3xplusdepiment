platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data


## Facebook event

| Name | API field | Description | Products |
| --- | --- | --- | --- |
| Meta Content Library ID | id  | The Meta Content Library ID associated with the Facebook event. | Content Library API |
| Name | name | The name of the Facebook event. | Content Library<br><br>  <br><br>Content Library API |
| Description | description | The description of the Facebook event. | Content Library<br><br>  <br><br>Content Library API |
| Creation time | creation\_time | The time the Facebook event was created. | Content Library API |
| Event start time | event\_start\_time | The start time of the Facebook event. Not available if the event is the parent of recurring event instances. [Learn more](https://developers.facebook.com/docs/content-library-api/guide-fb-events#recurring). | Content Library<br><br>  <br><br>Content Library API |
| Event end time | event\_end\_time | The end time of the Facebook event. Not available if the event is the parent of recurring event instances. [Learn more](https://developers.facebook.com/docs/content-library-api/guide-fb-events#recurring). | Content Library API |
| Going responses | going\_count | The number of Going responses on a Facebook event. Not available if the event is the parent of recurring event instances. [Learn more](https://developers.facebook.com/docs/content-library-api/guide-fb-events#recurring). | Content Library<br><br>  <br><br>Content Library API |
| Not going responses | not\_going\_count | The number of Not Going responses on a Facebook event. Not available if the event is the parent of recurring event instances. [Learn more](https://developers.facebook.com/docs/content-library-api/guide-fb-events#recurring). | Content Library API |
| Interested responses | interested\_count | The number of Interested responses on a Facebook event. Not available if the event is the parent of recurring event instances. [Learn more](https://developers.facebook.com/docs/content-library-api/guide-fb-events#recurring). | Content Library<br><br>  <br><br>Content Library API |
| Event type | event\_type | The type of Facebook event. Event types include single instance, recurring or instance of recurring. | Content Library API |
| Recurring event IDs | recurring\_event\_ids | The list of Meta Content Library IDs of the recurring instances of the Facebook event, if the event is recurring. Only available if the event is the parent of recurring event instances. [Learn more](https://developers.facebook.com/docs/content-library-api/guide-fb-events#recurring). | Content Library API |
| Parent event ID | parent\_event\_id | The Meta Content Library ID of the parent event of the Facebook event, if the event is recurring. Only available if the event is an instance of a recurring event. [Learn more](https://developers.facebook.com/docs/content-library-api/guide-fb-events#recurring). | Content Library API |
| Event owners type | event\_owners.data.type | The type of the event owner associated with the Facebook event. | Content Library API |
| Event owners ID | event\_owners.data.id | The Meta Content Library ID of the event owner associated with the Facebook event. This field will display if the event owner is a group, professional profile or Page. For events owned by professional profiles or Pages, only the Meta Content Library Page ID will be shared. | Content Library API |
| Place name | place.name | The self-reported, publicly accessible name of the place where the Facebook event is located. | Content Library API |
| Place location city | place.location.city | The self-reported, publicly accessible city where the Facebook event is located. | Content Library API |
| Place location country | place.location.country | The self-reported, publicly accessible country where the Facebook event is located. | Content Library API |
| Place location country code | place.location.country\_code | The self-reported, publicly accessible country code of the Facebook event’s location. | Content Library API |
| Place location name | place.location.name | The self-reported, publicly accessible name of the Facebook event’s location. | Content Library API |
| Place location region | place.location.region | The self-reported, publicly accessible region where the Facebook event is located. | Content Library API |
| Place location state | place.location.state | The self-reported, publicly accessible state where the Facebook event is located. | Content Library API |
| Place location street | place.location.street | The self-reported, publicly accessible street where the Facebook event is located. | Content Library API |
| Place location zip | place.location.zip | The self-reported, publicly accessible zip of the Facebook event’s location. | Content Library API |