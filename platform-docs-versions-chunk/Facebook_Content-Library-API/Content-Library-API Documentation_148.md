platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-api-code

### Select Python or R

A new window opens in which your search parameters are listed, and the automatically-generated Python and R code that corresponds to your current search parameters is displayed in a tabbed code block. Click the tab corresponding to your language selection.

![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.8562-6/362617090_680365873510245_5275517031591126227_n.png?_nc_cat=109&ccb=1-7&_nc_sid=f537c7&_nc_ohc=n5XEes0r9JQAX-PJsbS&_nc_ht=scontent-cdg4-2.xx&oh=00_AfD9XUM9CWw2ywbnkk0Sr23yQH6BEBnDmtHWDglDarVjkA&oe=65BFCE87)

### Copy the code snippet

With the correct tab selected, click **Copy to Clipboard** below the code block.

The code block also includes instructions for submitting the code to the API as an asynchronous search which processes in the background. See [Search guide](https://developers.facebook.com/docs/content-library-api/guide-search-object) to learn about the difference between synchronous (default) and asynchronous searches.