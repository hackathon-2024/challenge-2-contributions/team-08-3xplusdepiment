platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/guide-search-object

## Search objects in the API

The Content Library API uses search objects to extract a subset of data from an extremely large online database. Each search object specifies the search method to be used and sets the values of the parameters that determine which data will be returned.

The parameters accepted by each search method are described in the search method [guides](https://developers.facebook.com/docs/content-library-api/guides).

The following sections describe how to use search objects to achieve various objectives, providing query examples in both R and Python.

* [Basic synchronous search with paginated results](#sync-search)
    
* [Response formats](#res-formats)
    
* [Altering search parameters](#alter-search)
    
* [Asynchronous search](#async-search)
    
* [Estimating response size](#estimate)