platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/search-quality

# Search quality approach

This document describes Meta's approach to search quality for Meta Content Library and API.

## Definitions

* **Entities** Objects in the social graph returned by Meta Content Library and Meta Content Library API which generate or contain content and are associated with a unique ID in internal systems. Includes Facebook Pages, groups, events and posts, as well as Instagram accounts and posts.
    
* **Content** Text and other data associated with entities, returned as fields in Meta Content Library and API.
    
* **Eligible / Visible** Entities or content permitted to be returned in Meta Content Library and API which meet privacy commitments and legal requirements. See [Frequently asked questions](https://developers.facebook.com/docs/content-library-api/disclosures) for details.
    
* **Endpoint** Meta Content Library or API endpoint corresponding to a specific type of entity.