platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-access

## Learn more

* [Inter-university Consortium for Political and Social Research (ICPSR)](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.icpsr.umich.edu%2Fweb%2Fpages%2Fabout%2F&h=AT3Ct6QtFLXo0rcth7PygRVH-MbyPZ_cJfDNGCiCxkSVybXH1GusxnM-lUfKOduXx6lKGG2Rv55Uy0MUS_Eq6pUxIrW7Ga2RjAmGEi1ywVIJEw80b0zIjM7h0jamur3TxbZBvgQqSTfWfUaJ)
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)