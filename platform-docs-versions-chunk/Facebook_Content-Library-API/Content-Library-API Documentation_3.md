platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/overview

## VPN

You access the API through our virtual private network (VPN) using OpenVPN. Follow the OpenVPN Setup instructions in [Getting started](https://developers.facebook.com/docs/content-library-api/quick-start) to learn how to install and configure the OpenVPN client.

## Data dictionary

We provide a detailed [Data dictionary](https://developers.facebook.com/docs/content-library-api/data) that describes the data names displayed in the Meta Content Library (the Name column) if applicable, and the corresponding API fields returned in Content Library API search responses (the API field column).