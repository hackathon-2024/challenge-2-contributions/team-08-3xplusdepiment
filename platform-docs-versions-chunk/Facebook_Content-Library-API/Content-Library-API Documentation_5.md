platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/overview

## Code examples

Code examples in this documentation use a tabbed code block with tabs for R and Python. Click the tab for the language of your choice to display the appropriate code. This is an example:

RPython

    Click the R tab to display R code here

    Click the Python tab to display Python code here

You can copy the code in either tab and paste it into a Jupyter notebook cell for the same language (R or Python).