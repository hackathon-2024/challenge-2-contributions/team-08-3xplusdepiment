platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/citations

# Citations

Each release of each product has a registered Digital Object Identifier (DOI) included in the citation formats we provide on this page. It is important that you cite the product(s) and Meta when using our data.

## Meta Content Library API version v2.0

### Citation format

Meta Platforms, Inc., (Month Accessed, Year Accessed). Meta Content Library API version v2.0 [https://doi.org/10.48680/meta.metacontentlibraryapi.2.0](https://l.facebook.com/l.php?u=https%3A%2F%2Fdoi.org%2F10.48680%2Fmeta.metacontentlibraryapi.2.0&h=AT3-BJE937n9rHKRLXOEdTLIpMzcTdu7OzIjWKqwwyntwBmVycyIdWUoR6ocFTXDJUb7Fume16a86QLJpKugMkqq4ZOHxQJvi-gVUKUet6oN9RQolVvoJW3QhXtPNQiCgSE3uJbUjHZSQ9MU).

### Abstract

**Meta Content Library API** is an API for querying and analyzing Meta's full historical public content archive, supporting data analysis in Python and R in the Meta Researcher Platform, a secure digital clean room.