platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-api-code

### Define your search

Select the parameters for your search. The [**About**](https://www.facebook.com/transparency-tools/content-library/dataset/1119037145491882/about/) page in the web UI describes all the filters that are available.

### Open the Get API Code tool

Click **</>** in the top menu bar (mouse over the button to see the label).

![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.8562-6/397217799_6715388071870174_6446816218835772110_n.png?_nc_cat=109&ccb=1-7&_nc_sid=f537c7&_nc_ohc=Iv74FI63udAAX_04d0F&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBry9IlL0zXCN41KbqhYNlTbJYeKx2c-7B2Tc_RYg-vRg&oe=65BE64FC)