platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/search-quality


### Disclosures

Data governance and sources described in this document may change from time to time and the metrics presented here may not be representative of current operations at Meta. Due to issues such as erroneously logging the data sources from which these metrics are created, these metrics may suffer from fluctuating data quality and incompleteness, which may lead to fluctuating accuracy. We disclose known fluctuations where possible.

Researchers using this data are responsible for conducting standard and thorough data cleaning processes, and are responsible for ensuring that their analyses are accurate. It is expected that researchers may find issues with the data when conducting their analysis. We encourage researchers to share any findings with us. This may include, but is not necessarily limited to, data quality, validity, or fidelity issues. Given the historical nature of the data and lapsed retention periods, we may not be able to fix the issues identified, but in such cases we will work to disclose them. Please reach out to us through [Direct Support](https://developers.facebook.com/docs/content-library-and-api/direct-support).