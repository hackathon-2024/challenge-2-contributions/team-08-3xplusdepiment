platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/quick-start


## Create a Jupyter notebook

To create a new notebook:

1. In your Jupyter hub environment, click the blue "+" symbol in the upper left corner of the window.
    
2. Under **Notebook** in the Launcher tab, select **Python 3** or **R** according to your preferred language. This creates and opens a new Jupyter notebook.
    

![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.8562-6/361395333_529381645981378_6324697782074898886_n.png?_nc_cat=108&ccb=1-7&_nc_sid=f537c7&_nc_ohc=yDI4S7jmn3QAX-frxLf&_nc_ht=scontent-cdg4-1.xx&oh=00_AfD_xBmkdFz4rOQhblccjzFG1M06RvWivvY0PV8JzW927A&oe=65BE4391)

To name the notebook, right-click the notebook in the left navigation bar and select **Rename**.

![](https://scontent-cdg4-3.xx.fbcdn.net/v/t39.8562-6/362621684_301900822211537_8967768774365526112_n.png?_nc_cat=104&ccb=1-7&_nc_sid=f537c7&_nc_ohc=WrCzH_P36HYAX90hjz8&_nc_ht=scontent-cdg4-3.xx&oh=00_AfBFFf_Dpvxafr8MJ3oBNdsDlN8PY80VlYS_dOKHCoSU_A&oe=65BF009B)  
  

You enter queries in the blank cells of the notebook. To run a query, click the play icon.

![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.8562-6/278392598_315887847317792_4897557074894235014_n.png?_nc_cat=105&ccb=1-7&_nc_sid=f537c7&_nc_ohc=IWgWj5JbcNMAX9ybYB0&_nc_ht=scontent-cdg4-1.xx&oh=00_AfBdyZRLWw19weTAz07hYpqwf4BwAsLQkHYFmtCXLIxizw&oe=65BFA583)