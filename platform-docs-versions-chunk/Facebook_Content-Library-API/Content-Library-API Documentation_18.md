platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/quick-start

# Getting started

You can work with Meta Content Library API within Meta's Researcher Platform or within an approved third-party cleanroom environment. The getting started procedure documented here is specific to Researcher Platform which runs a modified version of Jupyter and provides you with a virtual data cleanroom where you can securely search for and analyze data. If you are accessing the Content Library API through a third-party cleanroom environment, you will be provided with getting started instructions from the cleanroom's system administrator.

To get up and running with Content Library API in Researcher Platform:

* [Set up OpenVPN](#vpn-cl-api)
    
* [Log in to the Researcher Platform URL](#log-in-api)
    
* [Create a Jupyter notebook](#notebook-cl-api)
    
* [Import the Content Library API client](#client-library)
    
* [Test with a basic search](#test-basic-example)