platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-access


## Eligibility requirements

To be eligible for product access, researchers must be affiliated with a qualified academic institution or a qualified research institution. Researchers from different disciplinary and professional backgrounds are welcome to apply.

For academic institutions, _qualified_ means the institution meets all of the following criteria. It is:

* Dedicated to the pursuit of education and research.
    
* Accredited, as indicated by recognized standards and evidence on the institution's website (such as a university), or as demonstrated by documentation provided to ICPSR.
    
* Qualified to grant academic degrees (such as undergraduate, graduate, doctoral).
    
* A not-for-profit endeavor (in other words, not a business whose sole purpose is to make a profit).
    

For research institutions, _qualified_ means the institution is a non-university organization, institute, or society/entity which operates as a not-for-profit entity and holds scientific or public interest research as a primary purpose or core activity.