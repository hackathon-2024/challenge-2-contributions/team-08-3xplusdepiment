platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/citations


## Citations for earlier releases

The citation formats in the following table pertain to earlier releases of Meta Content Library and API, and are provided here for reference in reverse chronological order.

| Product and version | Citation format | Notes |
| --- | --- | --- |
| Meta Content Library API version v1.0 | Meta Platforms, Inc., (Month Accessed, Year Accessed). Meta Content Library API version v1.0 [https://doi.org/10.48680/meta.metacontentlibraryapi.1.0](https://l.facebook.com/l.php?u=https%3A%2F%2Fdoi.org%2F10.48680%2Fmeta.metacontentlibraryapi.1.0&h=AT1osVbIbJM8M0JcZKyxoLQ6GMAo8YPk4CKRqKLuBjIF5RWO1zemUXHMpvnCr4_-vyEZOCcH3GPWwXP8juNGeced8QDuuLwdIRyMK1nhys53gxaj1pYwaG-7k9EZnFCVD6cPHXRmlFP1ITzKCjWne8_7-CWDYw) | Replaced by ver. 2.0 on 1/30/2024 |
| Meta Content Library version v1.0 | Meta Platforms, Inc., (Month Accessed, Year Accessed). Meta Content Library version v1.0 [https://doi.org/10.48680/meta.metacontentlibrary.1.0](https://l.facebook.com/l.php?u=https%3A%2F%2Fdoi.org%2F10.48680%2Fmeta.metacontentlibrary.1.0&h=AT2ceerYKaR-cR3iVeiG_Rxe3LXbPRdC4PendvGxUrIZZvx_Uy2RCkXf-mOsQnBYox5tIzrc5z1Ybxlo5NSVdymdmpVGe8g2AG38f8vyTA8SaO2nFO-sGyR7MNC0VuphyaKeqWZLGOBXphEM) | Replaced by ver. 2.0 on 1/30/2024 |
| Meta Content Library API beta version v1.0 | Meta Platforms, Inc., (Month Accessed, Year Accessed). Meta Content Library API Beta version v1.0 [https://doi.org/10.48680/meta.metacontentlibraryapibetaversionv1.0](https://l.facebook.com/l.php?u=https%3A%2F%2Fdoi.org%2F10.48680%2Fmeta.metacontentlibraryapibetaversionv1.0&h=AT1s7hYrJI3jDE_xCWdgFoxuCOsR6KwuJWQMh-xA2i9Ik4rGST7BAg5IY-QfdgpzSzoTqgNt_xUyKdlf4Dlc4fVtC-mucvV66LN3H3pMZF_VjasugKz6-6-h_5_2LMsn1WD_KtQ9P__7X-q5) | Replaced by ver. 1.0 on 12/15/2023 |
| Meta Content Library beta version v1.0 | Meta Platforms, Inc., (Month Accessed, Year Accessed). Meta Content Library Beta version v1.0 [https://doi.org/10.48680/meta.metacontentlibrarybetaversionv1.0](https://l.facebook.com/l.php?u=https%3A%2F%2Fdoi.org%2F10.48680%2Fmeta.metacontentlibrarybetaversionv1.0&h=AT30qzIsUjuxjP8qhOVd5_tBoUKG15b-gnjL-vF7UwUe8fMPPgw1cGZXh8dXwoSMxTJ5wqIukjfgaTETxDp6fFhrMuczhsckoaVQxM7rISYoggzU2JoOo6E4O1uurdj3S8WdpWPXOUmffoYt) | Replaced by ver. 1.0 on 12/15/2023 |
| Researcher API early beta 0.6 | Data from Facebook Open Research and Transparency Team Researcher API 0.6, data points from all public Facebook Pages, groups, events, and post-level Facebook data from the US and select EU countries [https://doi.org/10.48680/meta.researcherapi.0.6](https://l.facebook.com/l.php?u=https%3A%2F%2Fdoi.org%2F10.48680%2Fmeta.researcherapi.0.6&h=AT3tJlHzY8pQdTXoQTDAueDn_CpyUoa7u5dZstdbSXAAStPwz9ERc3F3Zj0s_t_4IVTsYVqTWRoEPl1jkhyO_fv9FdrtSmozDTsFWYO6XUsyOCr69gKNfmlfQjWFGBUX0DfmoDbG79BblpjN) | This precursor product will be deprecated as of 1/31/2024 |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)