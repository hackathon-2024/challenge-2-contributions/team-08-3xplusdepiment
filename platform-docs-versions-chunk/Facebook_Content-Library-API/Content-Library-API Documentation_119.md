platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data


## Instagram account

| Name | API field | Description | Products |
| --- | --- | --- | --- |
| Meta Content Library ID | id  | The Meta Content Library ID associated with the Instagram account. | Content Library API |
| Account type | account\_type | The type of public Instagram account. Creator, business, and personal accounts are valid types.<br><br>Public Instagram accounts include professional accounts for businesses and creators. They also include a subset of personal accounts that have privacy [set to public](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F517073653436611&h=AT2eaRNscgisi8R0c-d2h0w8VHWuzi15kYsBVixlOkUTM9a773Jozwknk8iLxD9fUp4h9r6qXD_7Vtau-65bETl2-ZZF61l-bTvrQpzMbr5FJQAUZ3_CZAgCxOnOFYiO6OPX3Ji31Ba2eet_) and have either a verified badge or 50,000 or more followers. A [verified badge](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F733907830039577%3Fhelpref%3Dfaq_content&h=AT0-r6ZnK90zwC-O7rmvuWB5PJMLpn_w5E43-1_1zyXFUyHREQHjbQ8ecZK_DoWZQyPr-r9N1k0Gumi4OF-jMRCH3Jn3taFTKmNf9H5kvlberDNy8HI2oIKva6MIP6pCVUicCnW0gfXj3-Ss) in this context refers to accounts confirmed as authentic and not those with a paid Meta Verified subscription. | Content Library API |
| Is verified | is\_verified | Whether the Instagram account has a verified badge. A verified badge in this context refers to accounts confirmed as authentic and not to those with a paid Meta Verified subscription. [Learn more.](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F854227311295302&h=AT1krWf12XlconGGQonMJslfgjneKdRolS6emu9PNMDUMD7pRmfRR_bcLUpUr3GUI7ln_pBk5oAm5Hiv07jCGsTZOsYeTZK3J_ck2HFuCqzyB-5fefsYfxBR4aYVzQ9sxqoyaoswnmE5UMUz) | Content Library<br><br>  <br><br>Content Library API |
| Followers | follower\_count | The number of followers of the Instagram account. | Content Library<br><br>  <br><br>Content Library API |
| Following | following\_count | The number of accounts the Instagram account is following. | Content Library API |
| Website | website | The external website URL of the Instagram account. | Content Library API |
| Name | name | The name of the Instagram account. | Content Library<br><br>  <br><br>Content Library API |
| Biography | biography | The description of the Instagram account. | Content Library<br><br>  <br><br>Content Library API |
| Creation date | creation\_date | The date the Instagram account was created. | Content Library API |