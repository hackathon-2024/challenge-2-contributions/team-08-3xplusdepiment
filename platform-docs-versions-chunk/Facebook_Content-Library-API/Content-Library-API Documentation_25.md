platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/guides

# Guides

This section provides a guide to each of the six types of data that Meta Content Library API supports plus some additional guides to further develop your expertise with the API.

|     |     |
| --- | --- |
| ### [Facebook pages data](https://developers.facebook.com/docs/content-library-api/guide-fb-pages)<br><br>Describes the `search_pages()` method including its syntax and parameters, and some query examples. | ### [Facebook groups data](https://developers.facebook.com/docs/content-library-api/guide-fb-groups)<br><br>Describes the `search_groups()` method including its syntax and parameters, and some query examples. |
| ### [Facebook events data](https://developers.facebook.com/docs/content-library-api/guide-fb-events)<br><br>Describes the `search_events()` method including its syntax and parameters, and some query examples. | ### [Facebook posts data](https://developers.facebook.com/docs/content-library-api/guide-fb-posts)<br><br>Describes the `search_posts()` method including its syntax and parameters, and some query examples. |
| ### [Instagram accounts data](https://developers.facebook.com/docs/content-library-api/guide-ig-accounts)<br><br>Describes the `search_ig_accounts()` method including its syntax and parameters, and some query examples. | ### [Instagram posts data](https://developers.facebook.com/docs/content-library-api/guide-ig-posts)<br><br>Describes the `search_ig_posts()` method including its syntax and parameters, and some query examples. |
| ### [Search guide](https://developers.facebook.com/docs/content-library-api/guide-search-object)<br><br>Information about how search works in the Content Library API and how to create search objects. | ### [Advanced search guidelines](https://developers.facebook.com/docs/content-library-api/adv-search)<br><br>Boolean operators supported by the search functionality and how to use them, including examples. |
| ### [Rate limiting and query budgeting](https://developers.facebook.com/docs/content-library-api/rate-limiting)<br><br>Rate-limiting and query-budgeting parameters for both Content Library and API. | ### [Data deletion](https://developers.facebook.com/docs/content-library-api/data-deletion)<br><br>Description of the fixed 30-day deletion schedule Meta employs. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)