platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/overview


## Content Library API client

When you access the API through Meta's Researcher Platform, you use our Content Library API Client which runs on Jupyter and with which you can create Jupyter notebooks. Once you have created a notebook, you can import our Python3 or R Content Library API client module which allows you to perform queries using the API. See [Getting started](https://developers.facebook.com/docs/content-library-api/quick-start).

The Content Library API Client allows you to access a variety of fields on the following data. Information and content from public entities listed below will be returned; however, certain data that could potentially identify users (such as tags in Facebook) will be redacted or omitted in API responses.

* Facebook Pages
* Facebook groups
* Facebook events
* Facebook posts
* Instagram business and creator accounts, and a subset of personal accounts that match qualification criteria
* Instagram posts

Note that public Instagram accounts include professional accounts for businesses and creators. They also include a subset of personal accounts that have privacy [set to public](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F517073653436611&h=AT3dyyMutGT_rdMejwa0iOefCSmGb7VnqRWLUt5lUWA04PiJSrO6i80IcC_-XQncNKD5slXJFEOCsp1MJHMEtFOECE-mhsKd8WIuRcGRs-uTbsV75-Jq-eig6sr2U2brFxyxLfIexBChdqMI) and have either a verified badge or 50,000 or more followers. A [verified badge](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F733907830039577%3Fhelpref%3Dfaq_content&h=AT1blgqtfrQ0FTPYYkSwV2zuDc3Ao8wLA4sb0SuqcOsjgn1wYQRkPkUFsWEonpVjJHU0puO3b8irHbGqgzTPTnuiP4iYW3K7MTF4JyE9CL1Mt_5WS1Mjm56moCEBiYqEhNoPDycFlE71Fv11) in this context refers to accounts confirmed as authentic and not those with a paid Meta Verified subscription.