platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/guide-ig-accounts

## Learn more

* [Getting started](https://developers.facebook.com/docs/content-library-api/quick-start)
* [Search guide](https://developers.facebook.com/docs/content-library-api/guide-search-object)
* [Data dictionary](https://developers.facebook.com/docs/content-library-api/data)
* [Advanced search guidelines](https://developers.facebook.com/docs/content-library-api/adv-search)
* [Guide to Instagram posts data](https://developers.facebook.com/docs/content-library-api/guide-ig-posts)

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)