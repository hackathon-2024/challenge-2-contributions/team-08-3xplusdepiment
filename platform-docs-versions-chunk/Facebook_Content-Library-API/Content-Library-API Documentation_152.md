platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-help

# Get Help

For support questions related to Meta Content Library and API, we use a tool called **Direct Support** to quickly triage your questions, routing each one to the most appropriate expert. To get set up with Direct Support and for instructions on using it effectively, see [Direct Support](https://developers.facebook.com/docs/content-library-and-api/direct-support).

**But first**:

* For answers to many common questions, see [Frequently asked questions.](https://developers.facebook.com/docs/content-library-api/disclosures)  
      
    
* For questions related to the application for access and eligibility requirements, please refer to the Meta Content Library and API [application page](https://l.facebook.com/l.php?u=https%3A%2F%2Fsomar.infoready4.com%2F%23freeformCompetitionDetail%2F1910793&h=AT2JVm8YKnz5E9x9rUOAqXDwSpEdTyZbtM4zrYXmCJ_wq4k7NI9njI_zJaiHwqUp8xfqcTAx-lBdgpoizIaonrFfsW1E91myb426APzmjJbZWkqzUn0MpkZW34Bm-6C6qGv0JjL1s_cJO_Vx) at the Inter-university Consortium for Political and Social Research (ICPSR).
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)