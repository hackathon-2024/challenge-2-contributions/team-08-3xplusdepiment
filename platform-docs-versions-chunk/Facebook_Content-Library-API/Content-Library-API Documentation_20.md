platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/quick-start

## Log in to the Researcher Platform URL

While connected to the VPN, go to the [Researcher Platform URL](https://l.facebook.com/l.php?u=https%3A%2F%2Fcontent-library-api.fb-researchtool.com%2F&h=AT2Y8XmPJZzm5Nc9u15cmbPiXZZXtnmDbbfLgDRVPmKIhAA9QqSdsGRrdVlqw7LwrMeYYp-VUjbI2Rah-wCbRfZnjelNaSnsamwCaYMIdN3Iq1jqSG1TU1qUkpjMXk8xoLIOkxkvOnnjvjoP).

Log in to the site using your Facebook credentials. This will spin up an instance of JupyterHub server for your use in the Researcher Platform.

You will be offered the choice of CPU or GPU server. You can learn more about the difference between the two [here](https://developers.facebook.com/docs/researcher-platform/features/GPU) and there is complete documentation on [Researcher Platform here](https://developers.facebook.com/docs/researcher-platform).