platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/overview

## Meta Content Library

[Meta Content Library](https://www.facebook.com/transparency-tools/content-library/dataset/1119037145491882/about/) is a web-based tool that allows researchers to explore and understand data across Facebook and Instagram by offering a comprehensive, visual, searchable collection of publicly accessible content—the same content that is also accessible through the Content Library API. The web-based user interface allows you to explore data, test out search parameters, and assess whether the resulting data is appropriate for your planned research. No knowledge of query or programming languages is needed.