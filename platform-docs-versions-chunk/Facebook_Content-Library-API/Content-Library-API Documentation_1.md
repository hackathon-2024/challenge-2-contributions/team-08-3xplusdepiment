platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/overview

# Overview

Meta Content Library API provides access to real-time data from public discussions on Facebook and Instagram, equipping researchers to study existing topics of interest as well as understand evolving or emerging topics on our platforms.

Content Library API can be accessed either through Meta's Researcher Platform or through an approved third-party cleanroom environment. The following third-party cleanroom environments are approved as of Content Library API version 2.0:

* Inter-university Consortium for Political and Social Research (ICPSR) at the University of Michigan

User documentation for third-party cleanroom interfaces is outside the scope of the Meta Content Library API documentation and can instead be provided by the third-party's system administrator.