platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/rate-limiting


### Content Library

Content Library warns you when you get close to the maximum number of data records retrieved per seven-day rolling window (query budget) with a banner at the top of the page. This is an example of what the banner looks like:

![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.8562-6/367743972_262423759986978_211809594479650483_n.png?_nc_cat=102&ccb=1-7&_nc_sid=f537c7&_nc_ohc=jsN-z2cH3YYAX-3c11L&_nc_ht=scontent-cdg4-1.xx&oh=00_AfAh6CHmWAq6-6uoaRmc5rrYfsBX94-1-abvRnynIms66A&oe=65BF5964)

When you reach your query budget, you will see a different banner at the top of the page that looks like this:

![](https://scontent-cdg4-3.xx.fbcdn.net/v/t39.8562-6/367729115_698453068806558_1327188573085519935_n.png?_nc_cat=111&ccb=1-7&_nc_sid=f537c7&_nc_ohc=nKpmcd2YsUoAX9oe-fw&_nc_ht=scontent-cdg4-3.xx&oh=00_AfAdyEBW3zDyv1y9YRT-MMh5UWdSDEbKrZJXa0paGoF02A&oe=65BEBBA9)

The banner has a "Learn more" link, which will open the following message with additional details about rate limits:

![](https://scontent-cdg4-3.xx.fbcdn.net/v/t39.8562-6/367733689_3623192734669772_7188247576546648393_n.png?_nc_cat=104&ccb=1-7&_nc_sid=f537c7&_nc_ohc=XOMdupwPXaAAX9j40ZS&_nc_ht=scontent-cdg4-3.xx&oh=00_AfA2Nj0WCJ6kluo-nZubUEDJgmOlLzAB6J8X9qX5RxCDFg&oe=65BE4BD5)