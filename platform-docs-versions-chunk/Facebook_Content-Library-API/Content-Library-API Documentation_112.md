platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/appendix


## Documentation Contents

|     |     |
| --- | --- |
| ### [Data dictionary](https://developers.facebook.com/docs/content-library-api/data)<br><br>Detailed information on the data fields available in the public content accessed by Meta Content Library and API. | ### [Field expansion](https://developers.facebook.com/docs/content-library-api/field-expansion)<br><br>Explains the concept of _field expansion_ which is available for some fields on certain nodes. |
| ### [Search quality](https://developers.facebook.com/docs/content-library-api/search-quality)<br><br>Review of the set of measures Meta uses for monitoring search quality and presentation of any known limitations to search functionality and performance. | ### [Get API code](https://developers.facebook.com/docs/content-library-api/get-api-code)<br><br>How to get an automatically generated code snippet for your Meta Content Library search that you can use in the Meta Content Library API. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)