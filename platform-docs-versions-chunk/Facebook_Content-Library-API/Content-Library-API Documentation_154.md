platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/citations

### Keywords

Meta, Facebook, Instagram, social media, Facebook Pages, Facebook groups, Facebook events, Facebook posts, Instagram accounts, Instagram posts, Instagram creator accounts, Instagram business accounts, Instagram personal accounts, reels

### Product limitations

Please see:

* [Search quality approach](https://developers.facebook.com/docs/content-library-api/search-quality)
    
* [Frequently asked questions](https://developers.facebook.com/docs/content-library-api/disclosures)
    

## Meta Content Library version v2.0

### Citation format

Meta Platforms, Inc., (Month Accessed, Year Accessed). Meta Content Library version v2.0 [https://doi.org/10.48680/meta.metacontentlibrary.2.0](https://l.facebook.com/l.php?u=https%3A%2F%2Fdoi.org%2F10.48680%2Fmeta.metacontentlibrary.2.0&h=AT3tROKdDrOYLjMHP0rHVRViIS2accCnnjr9m4jzPlCs2up9pRAI1b4P6m1fZt8L0Dx4RQe4DV2odOVQZl1WyAAZJm4ZGHMssi37cjl5f_04SnRYwQwSS4SsMo8hPPp3Um2Xs5KOAIhe6qwf).