platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/rate-limiting

## How to request a limit increase

If you have questions or concerns about the impact of your limits on your research, contact us through [Direct Support](https://developers.facebook.com/docs/content-library-and-api/direct-support).

## Limit reached messaging