platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/rate-limiting

## Tips for staying below the limits

In general, searching for common words and requesting to fetch all the results can exhaust your query budget. Consider narrowing your searches for more targeted results.