platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/quick-start


## Set up OpenVPN

Content Library API can only be accessed through a Virtual Private Network (VPN). This section shows you how to install and configure the OpenVPN client and connect to our VPN server. Once connected, you will be able to access Meta Content Library API.

#### Step 1: [Download and install OpenVPN](https://l.facebook.com/l.php?u=https%3A%2F%2Fopenvpn.net%2Fclient%2F%3Ffbclid%3DIwAR0Y0YhJ92gWqV68O7jjQXo-tv9aCO094M5q3ttl1wEwa9nDHLkOOlopi60&h=AT0Wkd4R7eRmdqBMaeplJfE9Ys1Nvlb51wigV_gOV8QeZttKWgyRKQT6jUJTIVrCjHl7Q8r7nB5gfvJPZntlZYbDfB9BQYDH8tyLLopu0eYTPiO7Nh5VV-2-3LAV2uQ4yEvEd5ULeeRhZDXN)

When the setup wizard completes, OpenVPN Connect launches and you will be required to accept the OpenVPN Inc. Data Collection, Use and Retention policy to continue.

#### Step 2: Set up your profile

In the Import Profile window, select the **UPLOAD FILE** tab.

![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.8562-6/359841127_304985435250473_5304206694181437663_n.png?_nc_cat=107&ccb=1-7&_nc_sid=f537c7&_nc_ohc=HVyfCGNKtuUAX_Ha_4J&_nc_ht=scontent-cdg4-2.xx&oh=00_AfCUlvtJLVqqitcH1hS472T0xZCK1AaUwjGcyhT0hv5tnw&oe=65BF53A2)  
  

#### Step 3: [Download the OpenVPN configuration file](https://facebook.com/transparency-tools/vpn-credentials)

Clicking this link downloads the OpenVPN configuration file (fortVpnCredentials.ovpn) to your computer (check your downloads folder). Once downloaded, drag and drop the file into the Import Profile window.

#### Step 4: Connect to the VPN

In the Imported Profile window, click **CONNECT**.

![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.8562-6/361947748_335117498837885_4826508458856907283_n.png?_nc_cat=107&ccb=1-7&_nc_sid=f537c7&_nc_ohc=bpa2hUI-G3IAX84bKLo&_nc_ht=scontent-cdg4-2.xx&oh=00_AfCGnvFOkljCRE5sHQwWGfBXu4PGBBgQykuGrCeYvl_7aA&oe=65BF0A8A)  
  

Once successfully connected, you will see this window:

![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.8562-6/361921794_727264049161457_5975480060212799981_n.png?_nc_cat=105&ccb=1-7&_nc_sid=f537c7&_nc_ohc=2dP9ltApeesAX-aCjq_&_nc_ht=scontent-cdg4-1.xx&oh=00_AfDUuCZk_Uc-dLJwdR40Sp8UVdwFWMs5DvGwZVdAd3sIfw&oe=65BE774E)

While you are connected to our VPN server, all of your internet traffic is routed through it, so be sure to disconnect when you are finished.

![](https://scontent-cdg4-3.xx.fbcdn.net/v/t39.8562-6/359843298_233572166287463_6401952988758247237_n.png?_nc_cat=111&ccb=1-7&_nc_sid=f537c7&_nc_ohc=uKZH-1uJiS8AX8N_wyr&_nc_ht=scontent-cdg4-3.xx&oh=00_AfCoLc717BjZv3SidQtl22_RIchzIKPqbWkjpNz0ppHG7g&oe=65BED4E4)