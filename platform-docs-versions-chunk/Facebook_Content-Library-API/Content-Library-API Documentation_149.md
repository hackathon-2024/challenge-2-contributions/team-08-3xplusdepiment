platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-api-code

### Go to Content Library API

At the bottom of the **Get API Code** window, there is a **Go to Content Library API** button that opens the Content Library API sign-in window.

VPN connection is required for Content Library API:

* The **Go to Content Library API** button is clickable if you are connected to VPN.
    
* If you believe you are connected, but the button is not clickable, try refreshing your browser.
    
* For Content Library API getting started information including VPN access, see [Getting started](https://developers.facebook.com/docs/content-library-api/quick-start).
    

## In Content Library API