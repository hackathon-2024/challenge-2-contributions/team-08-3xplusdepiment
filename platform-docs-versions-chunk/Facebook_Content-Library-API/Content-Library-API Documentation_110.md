platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data-deletion


## Managing your data to avoid research disruption

To minimize the disruption to your research, consider taking the following measures:

* Since your input query cells are not affected by the data deletion, you can plan to rerun your queries and regenerate new outputs every 30 days. The output will not match exactly what you had previously because it will not include any user data that no longer meets visibility requirements for Content Library API.
    
* You can plan ahead to accommodate the maintenance window each month, so you are not caught by surprise, unable to work.
    
* You can plan to perform analysis on query results before a maintenance window occurs. Although the raw data is affected by the data deletion, your code, figures, tables, graphs and statistics from your analyses are not.
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)