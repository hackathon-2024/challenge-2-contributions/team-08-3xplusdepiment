platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data

# Data dictionary

This data dictionary describes the data names displayed in the Meta Content Library web UI (the Name column) if applicable, and the corresponding API fields returned in Meta Content Library API search responses (the API field column). In the API field column, some fields have nested fields indicated by a dot notation. These are referred to as _expanded fields_. See [Field expansion](https://developers.facebook.com/docs/content-library-api/field-expansion) for information about how to include some or all of a parent field's expanded fields in your queries.