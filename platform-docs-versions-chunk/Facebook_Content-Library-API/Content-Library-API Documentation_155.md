platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/citations

### Abstract

**Meta Content Library** is a web-based tool that allows researchers to explore and understand data across Facebook and Instagram by offering a comprehensive, visual, searchable collection of publicly accessible content.

### Keywords

Meta, Facebook, Instagram, social media, Facebook Pages, Facebook groups, Facebook events, Instagram accounts, Instagram posts, Instagram creator accounts, Instagram business accounts, Instagram personal accounts, reels

### Product limitations

Please see:

* [Search quality approach](https://developers.facebook.com/docs/content-library-api/search-quality)
    
* [Frequently asked questions](https://developers.facebook.com/docs/content-library-api/disclosures)