platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/get-access


### **Step 1: Review application requirements and submit an application.**

To help ensure a fair and independent application and evaluation process, Meta has partnered with the Inter-university Consortium for Political and Social Research (ICPSR) based at the University of Michigan, to assess researcher qualifications and review submissions from global applicants. You can find an overview of the application process and data tools on ICPSR’s [Social Media Archive (SOMAR)](https://l.facebook.com/l.php?u=https%3A%2F%2Fsocialmediaarchive.org%2Frecord%2F52&h=AT1_7jhF518K63MvwoIYHoRQy48nUhAJ2xKqdwirZdhZro1rwJu5azJwnTitznBZq2WZO5Y5u9jDJQ7lrymwDAho219EjgHwZKLOsvWu6eBv4yegO9wSkrGfMwj1smk6L4Cm6x4j5eU6AOVY). We also encourage you to familiarize yourself with the Meta Content Library [application page](https://l.facebook.com/l.php?u=https%3A%2F%2Fsomar.infoready4.com%2F%23freeformCompetitionDetail%2F1910793&h=AT3mimCW7chqKjud0Ei_dDSATmOQJg1gDAT6_z5LL-lX9wWhKUKRJPZySy15pg0ihqbXAqWs53OWzaKVIcA6KeUYi-PsWX_-q9PJDkaVktFFiU8STZFnQ1dhh-M8q6iQuSfcEU2qLIhxTMnL) on the SOMAR site and [ICPSR’s application guide](https://l.facebook.com/l.php?u=https%3A%2F%2Fdocs.google.com%2Fdocument%2Fd%2F1iN4KOvFaYGZro23cB4j1FveouXMBcZnKl-yTUyx6fCg%2Fedit%3Fusp%3Dsharing&h=AT2sKmxh8ehYHpKRYcSLgUNTg1A2BBRMOrPqRFmPPMtfrEXKCz__bRnxRr-2B75PRRdE71snnEUUPSF6QJ0aRfxeee2_bTNAPgKO_3HLtSqVVp7roB6-09VsmTF-nQUtXRv7HFXGO1lYU0Y2) before applying.

ICPSR will notify Meta and the applicant if the application is approved. Researchers can expect the review process to take between four and six weeks.