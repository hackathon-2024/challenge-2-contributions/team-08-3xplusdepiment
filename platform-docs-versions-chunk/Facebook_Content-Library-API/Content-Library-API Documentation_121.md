platform: Facebook
topic: Content-Library-API
subtopic: Content-Library-API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Content-Library-API/Content-Library-API Documentation.md
url: https://developers.facebook.com/docs/content-library-api/data

## Instagram post third-party cleanroom only

The following data dictionary entries are only available when using the Content Library API in an approved third-party cleanroom that supports the specific functionality.

| Name | API field | Description | Products |
| --- | --- | --- | --- |
| Multimedia type | multimedia.type | The type (photo or video) of the multimedia content. | Content Library API |
| Multimedia ID | multimedia.id | Meta Content Library ID associated with the photo or video content. | Content Library API |
| Multimedia URL | multimedia.url | URL within a storage location to which the multimedia content has been downloaded by the third-party cleanroom if the cleanroom system is unable to provide the multimedia directly in the search results. | Content Library API |