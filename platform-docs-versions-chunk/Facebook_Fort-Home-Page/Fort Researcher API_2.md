platform: Facebook
topic: Fort-Home-Page
subtopic: Fort Researcher API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Home-Page/Fort Researcher API.md
url: https://fort.fb.com/researcher-apis

### Now Live: Researcher API Early Access Program

The Researcher API was specifically designed for academic needs. It equips qualified academics to conduct longitudinal research across all public Facebook Pages, Groups, Posts and Events in the US and select EU countries. Researchers can use this product to understand how public discussions on Facebook influence the social issues of the day. We offer this product via the Researcher Platform, which allows us to share privacy-protected data in a secure way.

We have invited a small group of qualified academics to test this product and provide feedback so we can iterate and improve it, before launching to a broader group of researchers.