platform: Facebook
topic: Fort-Home-Page
subtopic: Fort Researcher API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Home-Page/Fort Researcher API.md
url: https://fort.fb.com/researcher-apis

# Researcher API

#### RESEARCHER API

#### **One of the first Meta APIs designed for academics**

Qualifying academics can use the Researcher API to access near real-time data as well as billions of historical data points, which equips researchers to study topics of interest and gain a window into evolving or emerging topics on Facebook.

###### Product Roadmap

We know we have a lot of work to do and our tools are a work in progress. That’s why these products are informed by feedback from researchers all over the world, and we’re always looking for more. The input we’ve already received has helped us iterate and ensure the tools we’re building work for you - and we’ll be launching more soon.