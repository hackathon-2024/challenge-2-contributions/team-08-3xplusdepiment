platform: Facebook
topic: Fort-Home-Page
subtopic: Fort Researcher API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Home-Page/Fort Researcher API.md
url: https://fort.fb.com/researcher-apis

## Features

##### **Analytics**

A collection of API endpoints that provides researchers with aggregated insights on public forums like Groups & Pages

##### **Near real-time + historical data**

A collection of API endpoints that provides researchers access to raw, anonymized data from public forums on Facebook including Groups, Events & Pages

##### **Search**

A collection of API endpoints that gives researchers the ability to search across Facebook’s public forums in order to retrieve data that matches their search queries

###### Sign up to learn more

## Information about Facebook Open Research & Transparency, including access and eligibility

[Sign up](https://fort.fb.com/intake)