platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

joeColor string

An encoded value that defines colors for text and

background of the app as it appears on the App

Store.



iconPictureUrl string A resolved URL of the ad icon.



subtitle string The subtitle of the app as it appears on the App

Store.



primaryCategory string The app category on the App Store.



inAppPurchases boolean Indicates if the app has in-app purchases

enabled.



promotionalText string The app description on the App Store.



editorialBadge boolean The app's App Store metadata approval shown

next to the app icon.



shortDescription string

If promotionalText is not present, a

shortDescription matching the default

product page is displayed.



adAssets object A array of ad assets.



videoUrl string The resolved URL for a video asset.



pictureUrl string The resolved URL for the image asset.



order integer The order of the ad asset that was uploaded to

App Store Connect.



height integer The height of the ad asset that was uploaded to

App Store Connect.