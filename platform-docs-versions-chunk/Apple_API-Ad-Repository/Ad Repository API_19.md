platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>





{

"data": {

"adId": "397563857",

"appId": 1582276305,

"appName": "TripTrek - travel app",

"developerId": 819221,

"developerName": "Apple Inc.",

"legalName": "Apple Inc.",

"placement": "APPSTORE_SEARCH_RESULTS",

"format": " Icon Ad",

"countryOrRegion": "Spain",

"audienceRefinement": {

"ageTarget": "true",

"genderTarget": "false",

"locationTarget": "true",

"customerTypeTarget": "false"

},

"firstImpressionDate": "2022-08-25",

"lastImpressionDate": "2023-04-01",

"defaultLanguageTag": "es-ES",

"defaultLanguageDisplayName": "Spanish (Spain)",

"defaultPreviewDevice": "iPhone_6.5",

"adBanner": {

"joeColor": "b:ff4c00p:040404s:171c01t:361303q:452501",

"iconPictureUrl": "https://...x_.png/x.png",

"subtitle": "Planifique sus viajes con confianza.",

"primaryCategory": "viajar",

"inAppPurchases": "false",

"promotionalText": "Texto promocional que se muestra en la App Store.",

"editorialBadge": "false"

"shortDescription": "una descripción de la aplicación asociada con el

anuncio."

},

"adAssets": [

{

"videoUrl": "https://...x_.png/x.png",

"pictureUrl": "null",

"order": "1",

"height": "1920",

"width": "1080",

"orientation": "PORTRAIT"

}

],

"appIconVariations": [

"https://..._.png/x.png, https://..._x_.png/x.png"

],

"adLocaleVariations": [

{

"languageTag": "en-GB",

"languageDisplayName": "English (Great Britain)",

"appName": "TripTrek",

"subTitle": "Plan your trips with confidence.",

"primaryCategory": "travel",

"promotionalText": "App description shown on the App Store.",

"deviceAssets": [

{

"previewDevice": "iphone_6_5",

"adAssets": [