platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

Indicates app downloader type. Values are:

ALL_USERS

NEW_USERS

RETURNING_USERS

USERS_OF_MY_OTHER_APPS



firstImpressionDate string The first date of the first recorded delivered

impression for the subject advertising.



Ad Repository API January 2024 17

lastImpressionDate string



The last recorded delivered impression for the

subject advertising. This date will also

correspond to the last impression date for

advertising that was restricted but not

suspended, stopped, or removed.



defaultLanguageTag string

The locale code and default language used in the

advertiser's App Store geoterritory. For example,

es-ES .



defaultLanguageDisplayN

ame string

The default language used in the advertiser's

App Store geoterritory. For example, "Spanish

(Spain)".



defaultPreviewDevice string

The default device model used in the advertiser's

App Store geoterritory. For example,

"iPhone_6.5".



adBanner object The ad's App Store metadata properties.