platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

The reason on the basis of which the advertising

restriction was imposed. For example:

REASON_GOVERNMENT_ORDER

REASON_THIRD_PARTY_REPORT

REASON_POLICY_RESTRICTION_AGE

REASON_POLICY_RESTRICTION

REASON_TOS_INCOMPATABILITY



basis string The basis of the restriction. For example,

"Incompatible with Terms and Conditions".



details string Details of the restriction. For example, "Removed due

to governmental order".



firstImpressionDate string The first date of the first recorded delivered impression

for the subject advertising.



affectedPlacements object Metadata of the ad restriction’s App Store placement.



placement string

The placement of the ad on the App Store. Values are

APPSTORE_SEARCH_RESULTS, APPSTORE_TODAY_TAB,

APPSTORE_SEARCH_TAB



countriesOrRegions string

Countries or regions in the European Union in which

Apple-delivered advertising is available on the App

Store, expressed in ISO 3166-1 alpha-2 format. For

example: ES, SE, DE.