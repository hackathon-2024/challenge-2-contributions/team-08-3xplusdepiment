platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

{

"data": [

{

"name": "Austria",

"code": "AT"

},

{

"name": "Belgium",

"code": "BE"

},

{

"name": "Croatia",

"code": "HR"

},

{

"name": "Czech Republic",

"code": "CZ"

},

{

"name": "Denmark",

"code": "DK"

},

{

"name": "Finland",

"code": "FI"

},

{

"name": "France",

"code": "FR"

},

{

"name": "Germany",

"code": "DE"

},

{

"name": "Greece",

"code": "GR"

},

{

"name": "Hungary",

"code": "HU"

},

{

"name": "Ireland",

"code": "IE"

},

{

"name": "Italy",



Ad Repository API January 2024 7

"code": "IT"

},

{

"name": "Netherlands",

"code": "NL"

},

{

"name": "Poland",

"code": "PL"

},

{

"name": "Portugal",

"code": "PT"

},

{

"name": "Romania",

"code": "RO"

},

{

"name": "Spain",

"code": "ES"

},

{

"name": "Sweden",

"code": "SE"

}

]

}



Response properties



Parameter Type Description



name string

The name of the country or region in the European Union in which

Apple-delivered advertising is available on the App Store. For

example: " Sweden".