platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

Ad Repository API January 2024 10

Response properties



Parameter Type Description



adId string A unique ad identifier. Use adId as a resource in

the ad variations call.



appId integer The unique identifier of the app associated with

an ad.



appName string The name of the app associated with an ad.



developerId integer The unique identifier of the developer identified

on the App Store.



developerName string



The developer name refers to the developer as

identified on the App Store which is the entity on

whose behalf the subject advertising was

presented. In some instances, the

developerName may be different from the

legalName field for the same entity.



legalName string



The legal name of the person or entity identified

under the seller field on the App Store. In some

instances, the legalName may be different from

the developerName for the same entity.