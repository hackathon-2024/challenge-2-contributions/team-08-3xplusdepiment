platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>





{

"data": [

{

"adId": "76324182",

"appId": 1582276305,

"appName": "TripTrek - travel app",

"developerId": 819221,

"developerName": "Apple Inc",

"legalName": "Apple Inc.",

"placement": "APPSTORE_SEARCH_RESULT",

"format": "Icon Ad",

"countryOrRegion": "ES",

"audienceRefinement": {

"ageTarget": "false",

"genderTarget": "false",

"locationTarget": "true",

"customerTypeTarget": "false"

},

"firstImpressionDate": "2022-08-25",

"lastImpressionDate": "2023-04-01",

"defaultLanguageTag": "es-ES",

"defaultLanguageDisplayName": "Spanish (Spain)",

"defaultPreviewDevice": "iPhone_6.5",

"adBanner": {

"joeColor": "b:ff4c00p:040404s:171c01t:361303q:452501",

"iconPictureUrl": "",

"subtitle": "Planifique sus viajes con confianza.",

"primaryCategory": "viajar",

"inAppPurchases": "false",

"promotionalText": "Descripción de la aplicación mostrada en la App

Store.",

"editorialBadge": "false"

"shortDescription": "una descripción de la aplicación asociada con el

anuncio."

},

"adAssets": [

{

"videoUrl": "https://...x.png",

"pictureUrl": "null",

"order": 1,

"height": 1920,

"width": 1080,

"orientation": "LANDSCAPE"

}

],

"appIconVariations": [],

"adLocaleVariations": []

}

],

"dataStartDate": "2023-01-01",

"dataEndDate": "2023-04-01",

"pagination": {

"totalResults": 330,

"startIndex": 0,

"itemsPerPage": 15

}

}