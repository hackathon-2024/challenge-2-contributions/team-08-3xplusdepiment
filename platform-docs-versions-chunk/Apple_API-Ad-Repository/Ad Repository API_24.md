platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

width integer The width of the ad asset that was uploaded to

App Store Connect.



orientation string

The orientation of the ad asset that was

uploaded to App Store Connect. Values are

LANDSCAPE, PORTRAIT, UNKNOWN.



Ad Repository API January 2024 18

appIconVariations object The URL of the app icon on the App Store.



adLocaleVariations object The language metadata of ad variations on the

App Store.



languageTag string

The locale code of the language used in the ad

variation on the advertiser's App Store

geoterritory. For example, es-ES.



languageDisplayName string

The language used in the ad variation on the

advertiser's App Store geoterritory. "Spanish

(Spain)"



appName string The name of the app associated with an ad.



subTitle string The subtitle of the app that appears on the App

Store.



primaryCategory string The app category on the App Store.



promotionalText string The app description on the App Store.



deviceAssets object The device properties used in an ad campaign.