platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

Ad Repository API January 2024 15

{

"videoUrl": "https..._.png/x.png",

"pictureUrl": "null",

"order": "1",

"height": "1920",

"width": "1080",

"orientation": "PORTRAIT"

}

]

}

]

}

]

}

}



Ad Repository API January 2024 16

Response properties



Parameter Type Description



adId string A unique ad identifier. Use adId as a resource in

the ad variations call.



appId integer The unique identifier of the app associated with

an ad.



appName string The name of the app associated with an ad.



developerId integer The unique identifier of the developer as

identified on the App Store.



developerName string



The developer name refers to the developer as

identified on the App Store which is the entity on

whose behalf the subject advertising was

presented. In some instances, the

developerName may be different from the

legalName field for the same entity.