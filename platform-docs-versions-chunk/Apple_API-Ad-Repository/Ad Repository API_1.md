platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

Ad Repository API



January 2024



Version 1.0

Getting Started.................................................................................................................................................3



Versioning ....................................................................................................................................................3



Usability .......................................................................................................................................................3



Get a list of app and developer names.............................................................................................................4



Request example .........................................................................................................................................4



Request parameters ....................................................................................................................................4