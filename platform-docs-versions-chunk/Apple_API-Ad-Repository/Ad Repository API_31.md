platform: Apple
topic: API-Ad-Repository
subtopic: Ad Repository API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_API-Ad-Repository/Ad Repository API.md
url: <EMPTY>

No part of this publication may be reproduced, stored in a retrieval system, or transmitted, in any form or by any means, mechanical,

electronic, photocopying, recording, or otherwise, without prior written permission of Apple Inc., with the following exceptions: Any person

is hereby authorized to store documentation on a single computer or device for personal use only and to print copies of documentation for

personal use provided that the documentation contains Apple’s copyright notice. No licenses, express or implied, are granted with respect

to any of the technology described in this document. Apple retains all intellectual property rights associated with the technology

described in this document. This document is intended to assist publishers and partners with trafficking Advertising on Apple News.



Apple Inc.

Ad Platforms

One Apple Park Way

Cupertino, CA 95014, USA