platform: Linkedin
topic: Ad-Library
subtopic: Ad Library
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Ad-Library/Ad Library.md
url: https://www.linkedin.com/ad-library/

# Ad Library

LinkedIn’s Ad Library offers transparency in advertising by providing a searchable collection of ads.

## Frequently asked questions

### What is the Ad Library and how do I use it?

The Ad Library is a place where you can search for ads that have run on LinkedIn. Ads remain in the Ad Library for one year after their last impression on LinkedIn.

### Do I need a LinkedIn account to use the Ad Library?

Anyone can explore the Ad Library, with or without a LinkedIn account.

### What information is shown in the Ad Library?

As part of our commitment to creating a safe and trusted ad experience, the Ad Library includes basic information about ads served on LinkedIn, including the advertiser, ad format and ad creative. For ads targeted to the EU, the Ad Library includes additional information, including information about ad impressions, ad targeting and dates the ad ran.