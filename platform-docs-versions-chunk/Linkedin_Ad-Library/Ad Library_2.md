platform: Linkedin
topic: Ad-Library
subtopic: Ad Library
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Ad-Library/Ad Library.md
url: https://www.linkedin.com/ad-library/

### What are all the ad formats supported by LinkedIn?

To learn more about the ad formats available on LinkedIn, see the [LinkedIn Ads Guide](https://business.linkedin.com/marketing-solutions/success/ads-guide).

### How long does it take for an ad to be available on the Ad Library?

An ad will typically appear in the Ad Library results within 24-48 hours from the time it gets its first impression. Any changes or updates made to an ad will also typically be reflected within 24-48 hours.

### How far back can I search ads?

The Ad Library allows users to search ads that were created on or after June 1, 2023.

### How are dates and times recorded in the Ad Library?

All dates and timestamps are recorded in Coordinated Universal Time (UTC).

### How are ad start dates and end dates determined in the Ad Library?

The ad start date is determined when the first impression is delivered, while the ad end date is determined when the last impression is delivered.