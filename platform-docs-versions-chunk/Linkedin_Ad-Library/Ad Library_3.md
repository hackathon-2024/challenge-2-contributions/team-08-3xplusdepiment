platform: Linkedin
topic: Ad-Library
subtopic: Ad Library
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Ad-Library/Ad Library.md
url: https://www.linkedin.com/ad-library/

### How is ad targeting in the Ad Library determined?

For ads targeted to the EU, the Ad Library shows the top three targeting parameters selected by the advertiser in when targeting their ad. [Learn more](https://www.linkedin.com/help/lms/answer/a1517918).

### Can I opt out from my ads showing up in the Ad Library?

As part of our commitment to creating a safe and trusted ad experience, the Ad Library includes basic information about ads that have run on LinkedIn. Advertisers are not able to opt out of ads showing in the Ad Library.

### I have a question not answered here. Where can I find help?

You can reach out to our support team [here](https://www.linkedin.com/help/lms/ask).