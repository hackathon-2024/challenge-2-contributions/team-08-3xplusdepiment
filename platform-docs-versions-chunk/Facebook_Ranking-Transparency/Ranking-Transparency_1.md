platform: Facebook
topic: Ranking-Transparency
subtopic: Ranking-Transparency
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ranking-Transparency/Ranking-Transparency.md
url: https://transparency.fb.com/features/explaining-ranking

# Our approach to explaining ranking

UPDATED DECEMBER 31, 2023

Artificial intelligence (AI) systems inform the ranking of content for many experiences on Meta's products, such as viewing Facebook Feed, watching reels on Instagram or browsing Facebook Marketplace.