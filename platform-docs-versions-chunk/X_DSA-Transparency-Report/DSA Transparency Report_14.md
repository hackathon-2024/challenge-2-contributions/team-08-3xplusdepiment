platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html


### Training and Support provided to those Persons performing Content Moderation Activities for our TIUC Terms of Service and Rules

Training is a critical component of how X maintains the health and safety of the public conversation through enabling Trust and Safety agents to accurately and efficiently moderate content posted on our platform. Training at X aims to improve the agents’ and X’s policy enforcement performance and quality scores by enhancing agents’ understanding and application of X rules through robust training and quality programs and a continuous monitoring of quality scores.

TRAINING PROCESS

There is a robust training program and system in place for every workflow to provide content moderators with the adequate work skills and job knowledge required for processing user cases. All agents must be trained in their assigned workflows. These focus areas ensure that X agents are set up for success before and during the content moderation lifecycle, which includes:

* Training analysis/design focused on agent and learning needs;
* Classroom training with expert trainers;
* Nesting period to apply new skills;
* Cross-skilling opportunities;
* Upskilling opportunities;
* Refresher programs;
* New launch/update roll-outs process; and
* Remediation plans.

TRAINING ANALYSIS & DESIGN

Before commencing design work on any agent program or resource, a rigorous learner analysis is conducted in close collaboration with training specialists and quality analysts to identify performance gaps and learning needs. Each program is designed with key stakeholder engagement and alignment. The design objective is to adhere to visual and learning design principles to maximise learning outcomes and ensure that agents can perform their tasks with accuracy and efficiency. This is achieved by making sure that the content is: 

1. Easy to experience
2. Easy to understand
3. Easy to apply

X’s training programs and resources are designed based on needs, and a variety of modalities are employed to diversify the agent learning experience, including:

* Self-led learning: microlearning, scenario-based learning, e-learning modules, and gamification (where appropriate);
* Virtual live instructor-led trainings;
* Face-to-face classroom training; and
* Videos.

CLASSROOM TRAINING

Classroom training is delivered either virtually or face-to-face by expert trainers. Classroom training activities can include:

* Instructor-led policy training;
* Interactive e-learnings;
* Scenario-based learning sets;
* Shadowing sessions with seasoned agents;
* Guided casework sessions with trainers; and
* Knowledge checks, quizzes and assessments.

NESTING (ON-THE-JOB TRAINING)

When agents successfully complete their classroom training program, they undergo a nesting period. The nesting phase includes case study by observation, demonstration and hands-on training on live cases. Nesting activities include agent shadowing, guided case work, Question and Answer sessions with their trainer, coaching, feedback sessions, etc. Quality audits are conducted for each nesting agent and agents must be coached for any mis-action spotted in their quality scores the same day that the case was reviewed. Trainers conduct needs assessment for each nesting agent and prepare refresher training accordingly. After the nesting period, content is evaluated on an ongoing basis with a team of Quality Analysts to identify gaps and address potential problem areas. There is a continuous feedback loop with quality analysts across the different workflows to identify challenges and opportunities to improve materials and address performance gaps.

UP-SKILLING

When an agent needs to be upskilled they receive training of a specific workflow within the same pillar that the agent is currently working. The training includes a classroom training phase and nesting phase which is specified above.

REFRESHER SESSIONS

Refresher sessions take place when an agent has previously been trained, has access to all the necessary tools, but would need a review of some ro all topics. This may happen for content moderators who have been on prolonged leave, transferred temporarily to another content moderation policy workflow, or ones who have recurring errors in the quality scores. After a needs assessment, trainers are able to pinpoint what the agent needs and prepare a session targeting their needs and gaps. 

NEW LAUNCH / UPDATE ROLL-OUTS

There are also processes that require new and/or specific product training and certification. These new launches and updates are identified by X and the knowledge is transferred to the agents.

REMEDIATION PLANS

There are remediation plans in place to support agents who do not pass the training or nesting phase, or are not meeting quality requirements.