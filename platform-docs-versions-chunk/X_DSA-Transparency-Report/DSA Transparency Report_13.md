platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html


### Organisation, Team Resources, Expertise, Training and Support of our Team that Reviews and Responds to Reports of Illegal Content

Description of the team

X has built a specialised team made up of individuals who have received specific training in order to assess and take action on illegal content that we become aware of via reports or other processes such as on our own initiative. This team consists of different tier groups, with higher tiers consisting of more senior, or more specialised, individuals.

When handling a report of illegal content or a complaint against a previous decision, content and senior content reviewers first assess the content under X’s Rules and policies. If no violation of X’s Rules and policies is determined warranting a global removal of the content, the content reviewers assess the content for potential illegality. If the content is not manifestly illegal, it can be escalated for second or third opinions. If more detailed investigation is required, content reviewers can escalate reports to experienced policy and/or legal request specialists who have also undergone in-depth training. These individuals take appropriate action after carefully reviewing the report or complaint and available context in close detail. In cases where this specialist team still cannot determine a decision regarding the potential illegality of the reported content, the report can be discussed with in-house legal counsel. Everyone involved in this process works closely together with daily exchanges through meetings and other channels to ensure the timely and accurate handling of reports.

Furthermore, all teams involved in solving these reports closely collaborate with a variety of other policy  teams at X who focus on safety, privacy, authenticity rules and policies. This cross-team effort is particularly important in the aftermath of tragic events, such as violent attacks, to ensure alignment and swift action on violative content.

Content reviewers are supported by team leads, subject matter experts, quality auditors and trainers. We hire people with diverse backgrounds in fields such as law, political science, psychology, communications, sociology and cultural studies, and languages.

Training and support of persons processing legal requests

All team members, i.e. all employees hired by X as well as vendor partners working on these reports, are trained and retrained regularly on our tools, processes, rules and policies, including special sessions on cultural and historical context. Initially when joining the team at X, each individual follows an onboarding program and receives individual mentoring during this period, as well as thereafter through our Quality Assurance program (for external employees), in house and external counsels (for internal employees). 

All team members have direct access to robust training and workflow documentation for the entirety of their employment, and are able to seek guidance at any time from trainers, leads, and internal specialist legal and policy teams as outlined above as well as managerial support.

Updates about significant current events or rules and policy changes are shared with all content reviewers in real time, to give guidance and facilitate balanced and informed decision making. In the case of rules and policy changes, all training materials and related documentation is updated. Calibration sessions are carried out frequently during the reporting period. These sessions aim to increase collective understanding and focus on the needs of the content reviewers in their day-to-day work.

The entire team also participates in obligatory X rules and policies refresher training as the need arises or whenever rules and policies are updated. These trainings are delivered by the relevant policy specialists who were directly involved in the development of the rules and policy change. For these sessions we also employ the “train the trainer” method to ensure timely training delivery to the whole team across all of the shifts. All team members use the same training materials to ensure consistency.

Quality Assurance (QA) is a critical measure to the business to help ensure that we are delivering a consistent service at the desired level of quality to our key stakeholders, both externally and internally as it pertains to our case work. We have a dedicated QA Team within our vendor team to help us identify areas of opportunity for training and potential defect detection in our workflow or rules and policies. The QA specialists perform quality assurance checks of reports to ensure that content is actioned appropriately.

The standards and procedures within the QA team ensure the team’s QA is assessed equally, objectively, efficiently and transparently. In case of any mis-alignments, additional training is scheduled, to ensure the team understands the issues and can handle reports accurately.

In addition, given the nature and sensitivity of their work, the entire team has access to online resources and regular onsite group and individual sessions related to resilience and well-being. These are provided by mental health professionals. Content reviewers also participate in resilience, self-care, and vicarious trauma sessions as part of our mandatory wellness plan during the reporting period.