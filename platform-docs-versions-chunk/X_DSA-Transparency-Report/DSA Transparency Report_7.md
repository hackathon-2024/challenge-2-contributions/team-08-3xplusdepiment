platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html


## Complaints received through our internal complaint-handling system.

COMPLAINTS OF ACTIONS TAKEN FOR ILLEGAL CONTENT RECEIVED

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Illegal Content Complaints Received - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
|     | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | EU  | Finland | France | Germany | Greece | Ireland | Italy | Latvia | Luxembourg | Netherlands | Poland | Portugal | Slovenia | Spain | Sweden | Grand Total |
| Complaints | 3   | 8   | 1   | 1   | 1   | 1   | 5   | 3   | 33  | 1   | 52  | 33  | 1   | 10  | 15  | 2   | 5   | 5   | 5   | 6   | 1   | 14  | 2   | 208 |

COMPLAINTS OF ACTIONS TAKEN FOR ILLEGAL CONTENT DECISIONS

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Illegal Content Complaints Actioned - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
|     | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | EU  | Finland | France | Germany | Greece | Ireland | Italy | Latvia | Luxembourg | Netherlands | Poland | Portugal | Slovenia | Spain | Sweden | Grand Total |
| Overturned Appeal | 1   | 3   |     |     |     |     |     |     | 2   |     | 3   | 13  |     | 1   |     | 1   | 2   |     | 1   | 3   |     | 4   | 1   | 35  |
| Rejected Appeal | 2   | 5   | 1   | 1   | 1   | 1   | 5   | 3   | 31  | 1   | 49  | 20  | 1   | 9   | 15  | 1   | 3   | 5   | 4   | 3   | 1   | 10  | 1   | 173 |

COMPLAINTS OF ACTIONS TAKEN FOR ILLEGAL CONTENT MEDIAN HANDLE TIME

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Illegal Content Complaints Median Handle Time (Hours) - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
|     | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | EU  | Finland | France | Germany | Greece | Ireland | Italy | Latvia | Luxembourg | Netherlands | Poland | Portugal | Slovenia | Spain | Sweden |
| Complaints | 3.8 | 15.4 | 329 | 0.9 | 0   | 131.6 | 1.7 | 168.4 | 13.2 | 68  | 4.7 | 2   | 24.1 | 3.4 | 16  | 71.1 | 0   | 4.3 | 8.2 | 5.7 | 199.2 | 9.4 | 8.4 |

COMPLAINTS OF ACTIONS TAKEN FOR TIUC TERMS OF SERVICE AND RULES VIOLATIONS RECEIVED

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| TIUC Terms of Service and Rules Action Complaints - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Category | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Latvia | Lithuania | Luxembourg | Malta | Netherlands | Poland | Portugal | Romania | Slovakia | Slovenia | Spain | Sweden | Grand Total |
| Account Suspension Complaints | 1,006 | 1,758 | 741 | 407 | 242 | 952 | 1,010 | 321 | 1,101 | 16,340 | 24,594 | 1,067 | 874 | 1,456 | 5,837 | 318 | 535 | 965 | 122 | 13,939 | 6,688 | 2,457 | 1,501 | 333 | 208 | 12,365 | 2,202 | 99,339 |
| Content Action Complaints | 70  | 149 | 16  | 19  | 15  | 70  | 54  | 10  | 45  | 1,296 | 960 | 50  | 27  | 176 | 177 | 10  | 17  | 13  | 8   | 340 | 180 | 135 | 68  | 22  | 12  | 1,068 | 108 | 5,115 |
| Live Feature Action Complaints | 1   | 4   | 1   | 2   |     |     | 1   |     |     | 45  | 32  | 1   | 1   | 2   | 10  | 1   | 1   | 3   |     | 20  | 8   | 6   | 4   | 1   |     | 7   | 11  | 162 |
| Restricted Reach Complaints | 48  | 86  | 21  | 35  | 10  | 57  | 50  | 14  | 66  | 371 | 470 | 41  | 17  | 195 | 145 | 10  | 8   | 12  | 4   | 350 | 217 | 65  | 38  | 15  | 30  | 454 | 188 | 3,017 |
| Sensitive Media Action Complaints | 5   | 11  | 1   | 4   | 3   | 12  | 4   | 3   | 7   | 49  | 129 | 4   | 3   | 15  | 22  |     |     | 3   |     | 58  | 21  | 3   | 6   | 3   | 1   | 23  | 21  | 411 |
| Grand Total | 1,130 | 2,008 | 780 | 467 | 270 | 1,091 | 1,119 | 348 | 1,219 | 18,101 | 26,185 | 1,163 | 922 | 1,844 | 6,191 | 339 | 561 | 996 | 134 | 14,707 | 7,114 | 2,666 | 1,617 | 374 | 251 | 13,917 | 2,530 | 108,044 |

COMPLAINTS OF ACTIONS TAKEN FOR TIUC TERMS OF SERVICE AND RULES VIOLATIONS DECISIONS

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Decisions |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Category | Overturned | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Latvia | Lithuania | Luxembourg | Malta | Netherlands | Poland | Portugal | Romania | Slovakia | Slovenia | Spain | Sweden | Grand Total |
| Account Suspension Complaints | No  | 928 | 1,607 | 699 | 367 | 222 | 847 | 938 | 306 | 1,013 | 15,047 | 23,503 | 969 | 817 | 1,333 | 5,382 | 283 | 500 | 942 | 108 | 13,426 | 6,289 | 2,166 | 1,347 | 311 | 187 | 10,784 | 2,055 | 92,376 |
| Yes | 74  | 130 | 37  | 35  | 18  | 92  | 62  | 14  | 74  | 1,114 | 948 | 84  | 51  | 97  | 368 | 31  | 31  | 19  | 13  | 443 | 352 | 257 | 138 | 19  | 16  | 1,374 | 131 | 6,022 |
| Content Action Complaints | No  | 60  | 122 | 15  | 14  | 11  | 58  | 38  | 8   | 36  | 968 | 782 | 43  | 16  | 133 | 136 | 9   | 12  | 10  | 5   | 265 | 152 | 92  | 53  | 15  | 12  | 776 | 77  | 3,918 |
| Yes | 8   | 26  | 1   | 4   | 4   | 11  | 16  | 2   | 8   | 287 | 162 | 6   | 10  | 42  | 40  | 1   | 5   | 2   | 3   | 68  | 28  | 42  | 15  | 7   |     | 277 | 29  | 1,104 |
| Live Feature Action Complaints | No  | 1   | 4   | 1   | 2   |     |     | 1   |     |     | 44  | 30  | 1   | 1   | 2   | 10  | 1   | 1   | 3   |     | 20  | 7   | 6   | 4   | 1   |     | 7   | 11  | 158 |
| Yes |     |     |     |     |     |     |     |     |     | 1   | 1   |     |     |     |     |     |     |     |     |     | 1   |     |     |     |     |     |     | 3   |
| Restricted Reach Complaints | No  | 24  | 38  | 12  | 23  | 5   | 20  | 29  | 5   | 25  | 203 | 232 | 18  | 9   | 96  | 82  | 7   | 5   | 7   | 1   | 176 | 92  | 45  | 18  | 8   | 15  | 231 | 95  | 1,521 |
| Yes | 24  | 48  | 9   | 12  | 4   | 37  | 21  | 9   | 40  | 168 | 235 | 23  | 8   | 99  | 63  | 3   | 3   | 5   | 3   | 171 | 125 | 20  | 20  | 7   | 15  | 221 | 93  | 1,486 |
| Sensitive Media Action Complaints | No  | 2   | 5   |     | 2   | 3   | 12  | 4   | 3   | 3   | 32  | 72  | 3   | 3   | 11  | 9   |     |     | 2   |     | 37  | 15  | 2   | 2   | 2   |     | 10  | 9   | 243 |
| Yes | 3   | 6   | 1   | 2   |     |     |     |     | 4   | 13  | 50  | 1   |     | 3   | 13  |     |     |     |     | 16  | 6   | 1   | 4   | 1   | 1   | 12  | 11  | 148 |
| Grand Total | Total | 1,124 | 1,986 | 775 | 461 | 267 | 1,077 | 1,109 | 347 | 1,203 | 17,877 | 26,015 | 1,148 | 915 | 1,816 | 6,103 | 335 | 557 | 990 | 133 | 14,622 | 7,067 | 2,631 | 1,601 | 371 | 246 | 13,692 | 2,511 | 106,979 |

COMPLAINTS OF ACTIONS TAKEN FOR TIUC TERMS OF SERVICE AND RULES VIOLATIONS MEDIAN HANDLE TIME

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| TIUC Terms of Service and Rules Complaints Median Handle Time (Hours) |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Category | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Latvia | Lithuania | Luxembourg | Malta | Netherlands | Poland | Portugal | Romania | Slovakia | Slovenia | Spain | Sweden |
| Account Suspension Complaints | 0.14 | 0.07 | 0.00 | 0.17 | 0.16 | 0.12 | 0.12 | 0.47 | 0.18 | 0.07 | 0.00 | 0.12 | 0.08 | 0.08 | 0.10 | 0.23 | 0.32 | 0.00 | 0.34 | 0.00 | 0.03 | 0.08 | 0.14 | 0.38 | 0.25 | 0.10 | 0.13 |
| Content Action Complaints | 0.25 | 0.33 | 0.90 | 0.16 | 0.48 | 0.07 | 0.05 | 0.28 | 0.46 | 0.55 | 1.04 | 0.77 | 0.48 | 0.15 | 0.35 | 0.13 | 0.30 | 1.20 | 0.27 | 0.42 | 0.62 | 0.30 | 0.57 | 0.35 | 0.30 | 0.43 | 0.27 |
| Live Feature Action Complaints | 1.21 | 9.11 | 4.76 | 3.71 |     |     | 12.87 |     |     | 5.51 | 6.40 | 0.07 | 4.51 | 4.30 | 7.98 | 8.86 | 3.58 | 1.22 |     | 4.98 | 3.74 | 6.31 | 11.72 | 1.72 |     | 2.35 | 7.74 |
| Restricted Reach Complaints | 0.08 | 0.05 | 0.10 | 0.17 | 0.04 | 0.08 | 0.03 | 0.05 | 0.07 | 0.08 | 0.08 | 0.05 | 0.05 | 0.07 | 0.08 | 0.05 | 0.17 | 0.10 | 0.15 | 0.08 | 0.07 | 0.07 | 0.05 | 0.05 | 0.08 | 0.08 | 0.07 |
| Sensitive Media Action Complaints | 5.82 | 0.13 | 11.75 | 0.27 | 2.78 | 0.22 | 4.80 | 2.18 | 1.13 | 0.93 | 1.88 | 1.65 | 3.70 | 1.17 | 0.45 |     |     | 0.18 |     | 0.82 | 0.47 | 0.68 | 2.68 | 0.98 | 4.08 | 1.30 | 1.32 |

Important Notes about Complaints:

1. Information on the basis of complaints is not provided due to the wide variety of underlying reasoning contained in the open text field in the complaint form.
2. To improve clarity, we've omitted countries and violation types with zero complaints from the tables above.
3. The COMPLAINTS OF ACTIONS TAKEN FOR TIUC TERMS OF SERVICE AND RULES VIOLATIONS RECEIVED/DECISIONS tables were updated on 1 November 2023 to show additional data regarding complaints of actions taken based on the CSE policy that were not shown in the original version. The table COMPLAINTS OF ACTIONS TAKEN FOR TIUC TERMS OF SERVICE AND RULES VIOLATIONS MEDIAN HANDLE TIME has been updated following updates to the tables COMPLAINTS OF ACTIONS TAKEN FOR TIUC TERMS OF SERVICE AND RULES VIOLATIONS RECEIVED/DECISIONS.