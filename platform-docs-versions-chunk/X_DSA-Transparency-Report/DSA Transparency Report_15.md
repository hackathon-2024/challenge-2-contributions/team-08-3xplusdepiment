platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html


### Monthly Active Recipients

During the period from April 20th, 2023 through October 20th, 2023 there were an average of 115.2M active recipients of the service (AMARS) in the EU.

|     |     |     |     |
| --- | --- | --- | --- |
|     | Logged In Users | Logged Out Users | Total AMARs |
| Austria | 753,735 | 999,100 | 1,752,835 |
| Belgium | 1,597,896 | 1,799,541 | 3,397,437 |
| Bulgaria | 450,528 | 321,878 | 772,406 |
| Cyprus | 180,205 | 210,831 | 391,036 |
| Czechia | 1,040,762 | 1,444,542 | 2,485,304 |
| Germany | 8,940,624 | 7,408,877 | 16,349,501 |
| Denmark | 769,813 | 613,974 | 1,383,787 |
| Estonia | 161,490 | 184,943 | 346,433 |
| Spain | 9,783,481 | 13,197,990 | 22,981,471 |
| Finland | 896,337 | 1,250,770 | 2,147,107 |
| France | 11,473,346 | 10,459,939 | 21,933,285 |
| Greece | 986,351 | 1,689,822 | 2,676,172 |
| Croatia | 291,167 | 725,785 | 1,016,951 |
| Hungary | 690,582 | 928,674 | 1,619,256 |
| Ireland | 1,451,149 | 1,868,565 | 3,319,714 |
| Italy | 5,128,290 | 4,017,433 | 9,145,723 |
| Lithuania | 385,819 | 227,342 | 613,161 |
| Luxembourg | 195,112 | 120,554 | 315,666 |
| Latvia | 228,835 | 236,542 | 465,376 |
| Malta | 83,311 | 67,049 | 150,360 |
| Netherlands | 4,011,930 | 4,917,265 | 8,929,195 |
| Poland | 6,447,687 | 7,489,627 | 13,937,314 |
| Portugal | 1,634,243 | 1,401,741 | 3,035,984 |
| Romania | 1,555,457 | 822,888 | 2,378,344 |
| Sweden | 1,648,209 | 1,553,716 | 3,201,925 |
| Slovenia | 198,566 | 499,247 | 697,813 |
| Slovakia | 272,136 | 405,259 | 677,395 |
|     | 61,257,062 | 64,863,889 | 126,120,951 |

Important Note: Due to technical issues, for this report we were unable to provide the AMARS for each EU member state over the past six months. Instead, we provided AMARS for each EU member state from 19 September 2023 until 27 October 2023. We have resolved the technical issues for future transparency reports.

The AMARS for the entire EU over the past six months is 115.2M. The difference between the total AMARs for the EU and the cumulative total AMARs for all EU member states is due to double counting of logged out users accessing X from various EU countries within the relevant time period.

\- - - - - - - - - - - - - - - - - - Appendix - - - - - - - - - - - - - - - - -

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| TIUC Terms of Service and Rules Content Removal Actions - Sep 5 to Sep 23 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Enforcement Process | Policy | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Latvia | Lithuania | Luxembourg | Malta | Netherlands | Poland | Portugal | Romania | Slovakia | Slovenia | Spain | Sweden | Grand Total |
| Automated Means | Abuse & Harassment | 2   | 3   | 2   |     |     | 1   | 2   |     | 1   | 11  | 44  | 1   | 1   | 1   | 17  |     |     | 1   |     | 13  | 3   | 2   | 2   | 1   | 4   | 67  | 3   | 182 |
| Hateful Conduct |     | 3   |     | 3   |     |     | 2   |     | 10  | 3   | 2   |     |     |     | 3   | 1   |     |     |     | 7   |     | 5   |     |     | 10  |     | 18  | 67  |
| Non-Consensual Nudity |     |     | 1   | 1   |     |     | 5   |     |     | 2   | 1   |     | 1   |     |     |     |     |     |     | 2   | 1   |     |     |     |     | 3   | 1   | 18  |
| Other |     | 1   |     |     |     |     |     |     | 1   | 2   | 2   |     |     | 1   | 2   |     |     |     |     |     | 1   |     |     |     |     | 2   |     | 12  |
| Private Information & media |     |     |     |     |     |     |     |     |     | 1   | 6   |     |     |     | 1   |     |     |     |     | 2   |     |     |     |     |     |     |     | 10  |
| Sensitive Media | 41  | 103 | 54  | 25  | 11  | 68  | 68  | 6   | 46  | 1,002 | 723 | 82  | 78  | 61  | 420 | 11  | 25  | 11  | 7   | 268 | 429 | 113 | 134 | 25  | 5   | 632 | 94  | 4,542 |
| Violent Speech | 114 | 286 | 71  | 64  | 20  | 116 | 151 | 32  | 109 | 3,326 | 1,054 | 114 | 87  | 311 | 347 | 20  | 62  | 21  | 20  | 632 | 500 | 261 | 229 | 43  | 36  | 2,351 | 285 | 10,662 |
| Manual Review | Abuse & harassment | 195 | 124 | 42  | 26  | 41  | 169 | 269 | 4   | 119 | 2,169 | 1,311 | 65  | 53  | 115 | 366 | 365 | 230 | 14  | 1   | 1,621 | 686 | 165 | 229 | 7   | 2   | 1,379 | 93  | 9,860 |
| Child Sexual Exploitation |     | 1   | 1   | 1   |     |     | 2   |     |     | 15  | 6   |     |     | 4   | 16  |     | 1   | 2   |     | 8   | 3   |     |     |     |     | 7   | 1   | 68  |
| Counterfeit |     | 1   |     |     |     |     |     |     |     | 2   | 8   |     |     | 1   | 1   | 2   |     |     |     | 11  | 1   |     | 5   |     |     | 1   |     | 33  |
| Deceased Individuals |     |     |     |     |     | 2   |     |     |     | 3   | 2   |     |     |     |     |     |     | 1   |     |     | 1   |     | 1   |     |     |     |     | 10  |
| Hateful Conduct | 17  | 42  | 4   | 6   |     | 14  | 16  | 3   | 22  | 678 | 337 | 16  | 8   | 34  | 61  | 6   | 1   | 1   | 3   | 126 | 114 | 23  | 14  | 3   |     | 131 | 44  | 1,724 |
| Illegal or certain regulated goods and services | 24  | 32  | 10  | 1   | 5   | 55  | 33  |     | 3   | 414 | 336 | 4   | 71  | 29  | 31  | 282 | 240 | 2   | 2   | 344 | 95  | 29  | 20  | 1   |     | 130 | 17  | 2,210 |
| Misleading & Deceptive Identities |     | 1   | 1   |     |     | 1   |     |     |     | 7   | 8   | 2   | 1   | 2   | 3   |     |     |     |     | 3   | 3   | 1   | 3   |     |     | 1   |     | 37  |
| Non-Consensual Nudity | 11  | 11  | 9   |     | 9   | 16  | 8   |     | 12  | 112 | 192 | 36  | 23  | 9   | 45  | 2   | 16  |     | 2   | 108 | 90  | 6   | 63  | 8   | 1   | 105 | 6   | 900 |
| Perpetrators of Violent Attacks |     |     |     |     |     |     |     |     |     | 2   | 1   |     |     |     | 1   |     |     |     |     |     |     |     |     |     |     |     |     | 4   |
| Private information & media | 12  | 15  | 1   | 1   | 1   | 4   | 2   |     |     | 118 | 66  | 10  |     | 8   | 12  | 1   |     |     | 1   | 26  | 17  | 6   | 12  |     |     | 44  | 5   | 362 |
| Sensitive Media | 53  | 180 | 44  | 33  | 21  | 145 | 83  | 20  | 77  | 1,226 | 932 | 108 | 110 | 87  | 461 | 21  | 26  | 18  | 7   | 472 | 485 | 176 | 205 | 30  | 13  | 795 | 146 | 5,974 |
| Suicide & Self Harm | 7   | 13  | 7   | 5   | 1   | 10  | 11  | 1   | 10  | 107 | 96  | 4   |     | 25  | 41  | 4   | 2   | 1   | 2   | 42  | 294 | 14  | 17  | 3   | 1   | 71  | 20  | 809 |
| Violent & Hateful Entities |     |     |     |     | 1   |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 1   | 2   |
| Violent Speech | 53  | 47  | 7   | 9   | 6   | 49  | 36  | 7   | 26  | 774 | 1,062 | 30  | 12  | 68  | 167 | 4   | 8   | 5   |     | 176 | 703 | 47  | 33  | 3   | 6   | 245 | 67  | 3,650 |
| Grand Total |     | 529 | 863 | 254 | 175 | 116 | 650 | 688 | 73  | 436 | 9,974 | 6,189 | 472 | 445 | 756 | 1,995 | 719 | 611 | 77  | 45  | 3,861 | 3,427 | 848 | 967 | 124 | 78  | 5,964 | 801 | 41,136 |

Important Note: Due to a data extraction limitation that is currently under review data ranging from Aug 28 to Sept 5 is not included above.