platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html

## Further Information on Suspensions

During the applicable reporting period (Aug 28 to Oct 20), there were zero actions taken for: provision of manifestly unfounded reports or complaints; or manifestly illegal content. While manifestly illegal content is not a category that we have taken action on during the reporting period, we suspended 60,377 accounts for violating our Child Sexual Exploitation policy and 2,878 for violating our Violent and Hateful Entity policy.

## Disputes submitted to out-of-court dispute settlement bodies.

To date, zero disputes have been submitted to the out-of-court settlement bodies.

## Reports received by trusted flaggers.

To date, we have received zero reports from Article 22 DSA approved trusted flaggers. Once Article 22 DSA awarded trusted flaggers information is published, we are prepared to enrol them in our trusted flaggers program, which ensures prioritisation of human review.