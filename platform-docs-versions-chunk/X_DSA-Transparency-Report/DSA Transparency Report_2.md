platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html


## Description of our Content Moderation Practices

X's purpose is to serve the public conversation. Violence, harassment, and other similar types of behaviour discourage people from expressing themselves, and ultimately diminish the value of global public conversation. Our rules are designed to ensure all people can participate in the public conversation freely and safely.

X has policies protecting user safety as well as platform and account integrity. The X Rules and Policies are [publicly accessible](https://www.google.com/url?q=https://help.twitter.com/en/rules-and-policies&sa=D&source=editors&ust=1700092924585164&usg=AOvVaw26Q9npAi66LMuVw_SKP0S5) on our Help Center, and we are making sure that they are written in an easily understandable way. We also keep our Help Center regularly updated anytime we modify our rules.

Additionally, you will find explanations in our Help Center on our policy development process and rules enforcement philosophy. Creating a new policy or making a policy change requires in-depth research around trends in online behaviour, developing clear external language that sets expectations around what’s allowed, and creating enforcement guidance for reviewers that can be scaled across millions of pieces of content and accounts. Our policies are dynamic, and we continually review them to ensure that they are up-to-date, necessary, and proportional.

We consider diverse perspectives around the changing nature of online speech, including how our Rules are applied and interpreted in different cultural and social contexts. We then test the proposed rule with samples of potentially violative content to measure the policy effectiveness, and once we determine it meets our expectations, we build and operationalise product changes to support the update. Finally, we train our global review teams, update the X Rules, and start enforcing the relevant policy.

While we aim to enable open discussion of differing opinions and viewpoints, we are committed to the objective, timely, and consistent enforcement of our rules. This approach allows many forms of speech to exist on our platform and, in particular, promotes counterspeech: speech that presents facts to correct misstatements or misperceptions, points out hypocrisy or contradictions, warns of offline or online consequences, denounces hateful or dangerous speech, or helps change minds and disarm.

Thus, context matters. When determining whether to take enforcement action, we may consider a number of factors, including (but not limited to) whether:

* The behaviour is directed at an individual, group, or protected category of people;
* The report has been filed by the target of the abuse or a bystander;
* The user has a history of violating our policies;
* The severity of the violation;
* The content may be a topic of legitimate public interest.

When we take [enforcement actions](https://www.google.com/url?q=https://help.twitter.com/en/rules-and-policies/enforcement-options&sa=D&source=editors&ust=1700092924587024&usg=AOvVaw1c9quIwRomi33E3ujzwu42), we may do so either on a specific piece of content (e.g., an individual post or Direct Message) or on an account. We may employ a combination of these options. In most cases, this is because the behaviour violates the X Rules.

X strives to provide an environment where people can feel free to express themselves. If abusive behaviour happens, we want to make it easy for people to report it to us. EU users can also report any violation of our rules or their local laws, no matter where such violations appear, and we’ve recently improved our reporting flow to make it easier to use in several key ways. It now takes less steps to [report](https://www.google.com/url?q=https://help.twitter.com/en/rules-and-policies/twitter-report-violation%23violations&sa=D&source=editors&ust=1700092924587636&usg=AOvVaw3tzpgMMnJUxeP85-U2zuvD) most content, with extra steps only when it helps us take the right action. We now have clearer choices that match directly to our policies and how they’re communicated externally. We’ve also included new options that were previously only available at help.x.com.

EXERCISE OF MODERATION

To enforce our rules, we are using a combination of machine learning and human review. Our systems are able to surface content to human moderators who use important context to make decisions about potential rule violations. This work is led by an international, cross-functional team with 24-hour coverage and the ability to cover multiple languages. We also have a complaints process for any potential errors that may occur.

Examples of actions we may take:

* Placing a post behind a notice: We may place some forms of [sensitive media](https://www.google.com/url?q=https://help.twitter.com/en/rules-and-policies/media-policy.html&sa=D&source=editors&ust=1700092924588616&usg=AOvVaw1KDuz4XnQ4ro5PlihJqCt9) like adult content or graphic violence behind an interstitial advising viewers to be aware that they will see sensitive media if they click through. We also give users the option [](https://www.google.com/url?q=https://help.twitter.com/en/safety-and-security/sensitive-media.html&sa=D&source=editors&ust=1700092924588889&usg=AOvVaw2XrbwTJk8PcctKqzvl3kGT)[to control whether they see sensitive media](https://www.google.com/url?q=https://help.twitter.com/en/safety-and-security/sensitive-media.html&sa=D&source=editors&ust=1700092924589112&usg=AOvVaw0qXXpEBvWqLkNCgB5CT3BS).
* Withholding a post based on age: We restrict views of specific forms of sensitive media such as adult content for viewers who are under 18, or who do not include a birth date on their profile, with [interstitials](https://www.google.com/url?q=https://help.twitter.com/en/rules-and-policies/notices-on-x.html&sa=D&source=editors&ust=1700092924589554&usg=AOvVaw1QKiko0ds-sM1Xkk_TRmj6). 
* Withholding a post or account in a country: We may withhold access to certain content in a particular country if we receive a valid and properly scoped request from an authorised entity in that country. Read more about [country withheld content](https://www.google.com/url?q=https://help.twitter.com/en/rules-and-policies/tweet-withheld-by-country.html&sa=D&source=editors&ust=1700092924590035&usg=AOvVaw1x5L55BpPFDQ83aMzzINSL).

To ensure that our human reviewers are prepared to perform their duties we provide them with a robust support system. Each human reviewer goes through extensive training and refreshers, they are provided with a suite of tools that enable them to do their jobs effectively, and they have a suite of wellness initiatives available to them. For further information on our human review resources, see the section titled “Human resources dedicated to Content Moderation”.

We always aim to exercise moderation with transparency. Where our systems or teams take action against content or an account as a result of violating our rules or in response to a valid and properly scoped request from an authorised entity in a given country, we strive to provide context to users. Our [Help Center article](https://www.google.com/url?q=https://help.twitter.com/en/rules-and-policies/notices-on-x&sa=D&source=editors&ust=1700092924590886&usg=AOvVaw1ufLflKIK1WX2LlTWxNuqb) explains notices that users may encounter following actions taken. We will also promptly notify affected users about legal requests to withhold content, including a copy of the original request, unless we are legally prohibited from doing so.

COOPERATION WITH PUBLIC AUTHORITIES

Cooperation with law enforcement authorities within the EU is crucial to X. We work closely with law enforcement, and we do our best to assist them in identifying users whose content may be in violation of local laws. Any law enforcement authority or agency can find [guidelines](https://www.google.com/url?q=https://help.twitter.com/en/rules-and-policies/twitter-law-enforcement-support%2315&sa=D&source=editors&ust=1700092924591771&usg=AOvVaw3L_bYPCK74lz395tE1QySj) on our Help Center specifically for law enforcement and can reach out to X using a dedicated form.

TIUC is headquartered in Dublin, Ireland, and processes law enforcement requests relating to users who live in the EU. We receive and respond to requests related to user data from EU law enforcement agencies and judicial authorities wherever there is a valid legal process. We have existing processes in place, including a dedicated online portal for law enforcement, and expert teams with global coverage across all timezones that review and respond to reports in diverse languages.

Law enforcement can use our dedicated portal to submit their legal demands and can request the following information:

* Information requests: Requests/orders for user personal and private information (e.g Basic Subscriber Information).
* Content removal requests: Requests/orders to remove content based on TIUC Terms of Service and Rules or EU local laws.
* Preservation requests: Requests to preserve data for the purposes of an investigation.
* Emergency requests: Process through which, when there is an imminent threat to life or serious bodily harm, X may disclose user information to law enforcement without receiving formal legal process.