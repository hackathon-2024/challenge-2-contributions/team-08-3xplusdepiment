platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html

## Opening Remarks

X was founded on a commitment to transparency. We also want people on X to feel they are able to freely express themselves, while also ensuring that conversations on X are safe, legal and unregretted. When you think about some of the world’s most powerful moments, movements, and memes, they prevailed because people had a place to express their ideas, challenge conventional norms, and demand better. That’s why free expression matters.

We also believe, and we’re proving, that free expression and platform safety can coexist. X is reflective of real conversations happening in the world, and that sometimes includes perspectives that may be offensive, controversial, and/or narrow-minded to others. While we welcome everyone to express themselves on X, we will not tolerate behaviour that harasses, threatens, dehumanises or uses fear to silence the voices of others. Our TIUC Terms of Service and Rules - which are continually being reviewed, and are informed by feedback from the people who use X - help ensure everyone feels safe expressing themselves.

We are committed to fair, informative, responsive, and accountable enforcement. In the past, we too often got caught in a binary paradigm of whether to leave content up, or take it down.

* The risks of getting it wrong at the extremes are great: on one hand, you can leave up content that’s really dangerous; on the other, you run the risk of censorship.
* Our point is: if you do either, you need to be right. And we live in a world with many shades of grey.

To be clear, we do continue to remove dangerous and illegal content and accounts. X also responds to reports of illegal content and takes action on content that violates local laws. But what we’ve learned is that there are other types of content where a range of potential reasonable, proportionate, and effective approaches, that also seek to balance fundamental rights, can be appropriate.

You can think about how we moderate on X in three buckets: content and accounts that remain, are restricted, and are removed.

1. Remain: The overwhelming majority of content on X is healthy—meaning it does not violate our TIUC Terms of Service and Rules or our policies such as Hateful Conduct, Abuse & Harassment, and more. Keep in mind: just because a post doesn’t violate a policy, doesn’t mean everyone will like it.
2. Restrict: This is where our new [Freedom of Speech, Not Reach enforcement philosophy](https://www.google.com/url?q=https://blog.twitter.com/en_us/topics/product/2023/freedom-of-speech-not-reach-an-update-on-our-enforcement-philosophy&sa=D&source=editors&ust=1700092924582794&usg=AOvVaw3Al_cOnucyf_BBbLGKpyHm) is used. For content that may be interpreted as potentially violating our policies—meaning it’s awful, but lawful—we restrict the reach of posts by making the content less discoverable, and we’re making this action more transparent to everyone. When we decide to restrict a piece of content, a restricted reach label is applied, the ability to engage with the content is taken away, and its reach is restricted to views occurring directly on the author's profile. Restricted reach labels are not in use for all policies; our restricted reach labels were initially only applied to Hateful Conduct, but we have since expanded application to our Abuse & Harassment, Civic Integrity, and Violent Speech Policies. That said, restricting content—or even a whole account—is something we’ve done for a long time, and we have a range of enforcement options for the variety of use cases that we face every day. For example, we may also place an account in read-only mode, temporarily limiting its ability to post, Repost, or Like.
3. Remove: If reported content is illegal, we withhold access to it in the respective jurisdictions. We also know that certain types of content, such as targeted violent threats, targeted harassment, or privacy violations, can be extremely harmful if not removed and we either suspend outright or require that this content be deleted before returning to the platform.

We've made significant progress towards improving the safeguards to protect our users and our platform, but we know that this critical work will never be done. X is committed to ensuring the safety and health of the platform and fulfilment of its DSA Compliance obligations through our continued investment in human and automated protections.

This report covers the content moderation activities of X’s international entity Twitter International Unlimited Company (TIUC) under the Digital Services Act (DSA), during the date range August 28, 2023 to October 20, 2023.

We refer to “notices” as defined in the DSA as “user reports” and “reports”.