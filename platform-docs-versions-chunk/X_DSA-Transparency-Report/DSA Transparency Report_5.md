platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html


## Orders received from Member States’ authorities including orders issued in accordance with Articles 9 (Removal Orders) and 10 (Information Requests)

REMOVAL ORDERS, Art. 9 DSA

| Removal Orders Received - Aug 28 to Oct 20 |     |     |     |     |
| --- | --- | --- | --- | --- |
| Illegal Content Category | France | Italy | Spain | Grand Total |
| Unsafe and/or Illegal Products | 1   |     |     | 1   |
| Illegal or Harmful Speech |     | 4   | 1   | 5   |
| Grand Total | 1   | 4   | 1   | 6   |

| Removal Orders Median Handle Time (Hours) - Aug 28 to Oct 20 |     |     |     |
| --- | --- | --- | --- |
| Illegal Content Category | France | Italy | Spain |
| --- | --- | --- | --- |
| Unsafe and/or Illegal Products | 32  |     | 124 |
| --- | --- | --- | --- |
| Illegal or Harmful Speech |     | 73  |     |
| --- | --- | --- | --- |

|     |
| --- |
| Removal Orders Median Time to Acknowledge Receipt - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |
| X provides an automated acknowledgement of receipt of removal orders submitted by law enforcement through our [Legal Request submission porta](https://www.google.com/url?q=https://legalrequests.twitter.com/forms/landing_disclaimer&sa=D&source=editors&ust=1700092925289587&usg=AOvVaw0fjHXz41A0Y4UKXZIIj3_S)l. As a consequence of this immediate acknowledgement of receipt, the median time is zero. |     |     |     |     |     |     |     |     |     |     |     |     |     |

Important Notes about Removal Orders:

1. To improve clarity, we've omitted countries and violation types with no legal requests from the tables above.
2. The table “Removal Orders Median Handle Time” shows the category which we considered to fit best and under which we handled the order. This category might deviate from the information provided by the authority when submitting the order via the X online submission platform.
3. In the cases from France and Spain, we asked the submitting authority to fulfil Article 9 information requirements but did not receive responses in the reporting period.

INFORMATION REQUESTS, Art. 10 DSA

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Information Requests Received - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Content Category | Austria | Belgium | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Malta | Netherlands | Poland | Portugal | Spain | Grand Total |
| Data Protection and Privacy Violations |     |     |     |     | 2   | 1   |     |     | 1   |     |     |     |     | 1   | 5   |
| Illegal or Harmful Speech | 4   | 3   | 1   | 43  | 623 | 4   |     | 1   | 6   |     |     | 9   |     | 6   | 700 |
| Intellectual Property Infringements |     |     |     |     | 2   |     |     |     |     |     |     |     |     | 1   | 3   |
| Negative Effects on Civic Discourse or Elections |     |     |     |     | 2   |     |     |     |     |     |     | 1   |     |     | 3   |
| Non-Consensual Behaviour |     |     |     | 3   | 23  | 1   |     |     |     |     |     |     |     | 1   | 28  |
| Not Specified | 1   |     |     | 4   | 7   |     | 1   |     |     |     |     | 3   |     | 4   | 20  |
| Other |     | 2   |     | 8   | 4   |     |     |     |     |     |     | 1   | 1   |     | 16  |
| Pornography or Sexualized Content |     |     |     |     | 13  |     |     |     |     |     |     |     |     |     | 13  |
| Protection of Minors |     | 7   |     | 1   | 31  |     |     |     | 2   |     |     | 1   | 1   |     | 43  |
| Risk for Public Security |     | 19  |     | 654 | 16  | 1   |     |     | 17  | 1   | 1   |     |     | 7   | 716 |
| Scams and/or Fraud | 1   |     | 1   | 2   | 7   | 1   |     | 1   | 1   |     |     |     |     | 1   | 15  |
| Self-Harm |     |     |     | 1   |     |     |     |     |     |     |     |     |     |     | 1   |
| Unsafe and/or Illegal Products |     |     |     |     | 4   |     |     |     |     |     |     |     |     | 2   | 6   |
| Violence |     | 1   |     | 71  | 61  | 1   |     | 2   | 6   |     | 8   | 7   | 1   | 1   | 159 |
| Grand Total | 6   | 32  | 2   | 787 | 795 | 9   | 1   | 4   | 33  | 1   | 9   | 22  | 3   | 24  | 1,728 |

|     |
| --- |
| Information Request Median Time to Acknowledge Receipt - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |
| X provides an automated acknowledgement of receipt of information requests submitted by law enforcement through our [Legal Request submission portal](https://www.google.com/url?q=https://legalrequests.twitter.com/forms/landing_disclaimer&sa=D&source=editors&ust=1700092925357860&usg=AOvVaw1AFoki2ueF9JKTZcr1u5jZ). As a consequence of this immediate acknowledgement of receipt, the median time is zero. |     |     |     |     |     |     |     |     |     |     |     |     |     |

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Information Request Median Handle Time (Hours) - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Content Category | Austria | Belgium | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Malta | Netherlands | Poland | Portugal | Spain |
| Data Protection and Privacy Violations |     |     |     |     | 152 | 141 |     |     | 146 |     |     |     |     | 173 |
| Illegal or Harmful Speech | 146 | 42  | 146 | 138 | 127 | 64  |     | 73  | 164 |     |     | 78  |     | 129 |
| Intellectual Property Infringements |     |     |     |     | 114 |     |     |     |     |     |     |     |     | 175 |
| Negative Effects on Civic Discourse or Elections |     |     |     |     | 183 |     |     |     |     |     |     | 20  |     |     |
| Non-Consensual Behaviour |     |     |     | 21  | 117 | 124 |     |     |     |     |     |     |     | 170 |
| Not Specified | 219 |     |     | 35  | 24  |     | 5   |     |     |     |     | 1   |     | 2   |
| Other |     | 170 |     | 152 | 149 |     |     |     |     |     |     | 2   | 165 |     |
| Pornography or Sexualized Content |     |     |     |     | 56  |     |     |     |     |     |     |     |     |     |
| Protection of Minors |     | 2   |     | 49  | 4   |     |     |     | 2   |     |     | 26  | 209 |     |
| Risk for Public Security |     | 5   |     | 8   | 47  | 18  |     |     | 149 | 43  | 126 |     |     | 74  |
| Scams and/or Fraud | 30  |     | 172 | 169 | 120 | 120 |     | 20  | 124 |     |     |     |     | 73  |
| Self-Harm |     |     |     | 194 |     |     |     |     |     |     |     |     |     |     |
| Unsafe and/or Illegal Products |     |     |     |     | 146 |     |     |     |     |     |     |     |     | 126 |
| Violence |     | 154 |     | 132 | 119 | 51  |     | 19  | 147 |     | 19  | 48  | 241 | 190 |

Important Notes about Information Requests:

1. The content category for each request is determined by the information law enforcement provides while submitting such requests through the X online submission platform.
2. The median handling time is the time between receiving the order and either: 1) disclosing information to law enforcement if the order is valid; or 2) pushing back due to legal issues. The median handling time does not include extra time where X pushes back due to legal issues, receives a valid order later, and disclosure is eventually made.
3. To improve clarity, we've omitted countries and violation types with zero legal requests from the tables above.
4. The “Not Specified” category shows cases where the illegal content category could not be determined based on the information law enforcement provided during the submission process.
5. The “Other” category here shows cases where law enforcement selects “Cybercrime” as the content category during the case submission process without providing more details to determine a more specific content category.