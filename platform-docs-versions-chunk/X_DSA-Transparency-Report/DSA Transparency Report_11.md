platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html

### Linguistics Expertise of our Content Moderation Team

X’s scaled operations team possesses a variety of skills, experiences, and tools that allow them to effectively review and take action on reports across all of our rules and policies. X has analysed which languages are most common  in reports reviewed by our content moderators and has hired content moderation specialists who have professional proficiency in the commonly spoken languages. The following table is a summary of the the number of people in our content moderation team who possess professional proficiency in the most commonly spoken languages in the EU on our platform:

|     |     |
| --- | --- |
| Primary Language | People |
| Arabic | 12  |
| Bulgarian | 2   |
| Croatian | 1   |
| Dutch | 1   |
| English | 2,294 |
| French | 52  |
| German | 81  |
| Hebrew | 2   |
| Italian | 2   |
| Latvian | 1   |
| Polish | 1   |
| Portuguese | 41  |
| Spanish | 20  |