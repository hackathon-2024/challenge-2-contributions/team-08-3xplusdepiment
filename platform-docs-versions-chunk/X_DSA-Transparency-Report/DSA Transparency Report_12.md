platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html

### Qualifications of our Content Moderation Team

| Content Moderation Team Qualifications |     |
| --- | --- |
| Years in Current Role | Headcount |
| --- | --- |
| 7 or more | 48  |
| --- | --- |
| 6 to 7 | 51  |
| --- | --- |
| 5 to 6 | 131 |
| 4 to 5 | 264 |
| 3 to 4 | 326 |
| 2 to 3 | 443 |
| 1 to 2 | 638 |
| 0 to 1 | 393 |