platform: X
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_DSA-Transparency-Report/DSA Transparency Report.md
url: https://transparency.twitter.com/dsa-transparency-report.html


## Reports submitted in accordance with Article 16 (Illegal Content)

ACTIONS TAKEN ON ILLEGAL CONTENT:

* During the reporting period, we took action on 12,099 items of content following reports of illegality (excluding intellectual property infringements). We globally deleted 99 items of content and withheld access to 11,998 items of content in EU Member States. 
* In respect of intellectual property infringements, we globally withheld content in response to 1,829 reports.
* We also proactively withheld access to 319 posts in one Member State.

ACTIONS TAKEN ON ACCOUNTS FOR POSTING ILLEGAL CONTENT: We suspended accounts in response to 855 reports of Intellectual Property Infringements. This was the only type of violation of local law that resulted in account suspension as many types of illegal behaviour are addressed in our policies, such as account suspensions for posting CSE. On our own initiative, we withheld 1 account for breaching local laws connected to unsafe and/or illegal products.

Also, we withheld 15 accounts in one Member State each for provision of illegal content.

REPORTS OF ILLEGAL CONTENT

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Illegal Content Reports Received - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Content Category | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | EU  | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Latvia | Lithuania | Luxembourg | Malta | Netherlands | Poland | Portugal | Romania | Slovakia | Slovenia | Spain | Sweden | Grand Total |
| Animal Welfare | 14  | 4   | 1   | 2   | 4   | 2   | 4   | 1   | 88  | 4   | 96  | 58  | 3   | 1   | 11  | 10  | 1   | 1   | 1   | 1   | 15  | 16  | 3   | 2   | 2   | 2   | 56  | 3   | 392 |
| Data Protection & Privacy Violations | 18  | 50  | 7   | 5   | 6   | 16  | 21  | 6   | 500 | 13  | 727 | 592 | 45  | 9   | 90  | 98  | 5   | 0   | 1   | 0   | 164 | 94  | 60  | 17  | 3   | 10  | 703 | 27  | 3,269 |
| Illegal or Harmful Speech | 397 | 448 | 46  | 34  | 32  | 205 | 175 | 46  | 5,258 | 133 | 9,499 | 11,265 | 198 | 32  | 335 | 1203 | 60  | 41  | 35  | 7   | 995 | 893 | 626 | 96  | 27  | 26  | 3088 | 203 | 35,006 |
| Intellectual Property Infringements | 16  | 19  | 4   | 14  | 17  | 8   | 14  | 2   | 0   | 35  | 737 | 872 | 19  | 7   | 64  | 185 | 7   | 29  | 5   | 4   | 701 | 601 | 262 | 52  | 0   | 0   | 835 | 21  | 4,531 |
| Negative Effects on Civic Discourse or Elections | 27  | 33  | 6   | 1   | 3   | 25  | 12  | 7   | 475 | 14  | 314 | 934 | 15  | 7   | 26  | 132 | 3   | 5   | 2   | 1   | 219 | 514 | 24  | 16  | 14  | 3   | 127 | 8   | 2,940 |
| Non-Consensual Behaviour | 15  | 16  | 2   | 4   | 4   | 2   | 11  | 4   | 179 | 9   | 196 | 143 | 7   | 15  | 35  | 36  | 0   | 2   | 0   | 0   | 34  | 17  | 16  | 2   | 1   | 0   | 186 | 22  | 943 |
| Pornography or Sexualized Content | 38  | 50  | 9   | 3   | 4   | 25  | 23  | 1   | 468 | 10  | 865 | 641 | 44  | 109 | 55  | 145 | 5   | 3   | 2   | 2   | 113 | 107 | 67  | 48  | 8   | 1   | 324 | 26  | 3,158 |
| Protection of Minors | 43  | 49  | 11  | 7   | 3   | 20  | 24  | 3   | 462 | 22  | 672 | 564 | 24  | 7   | 65  | 57  | 12  | 0   | 1   | 0   | 107 | 78  | 17  | 8   | 2   | 13  | 305 | 17  | 2,550 |
| Risk for Public Security | 39  | 105 | 8   | 4   | 4   | 46  | 13  | 8   | 414 | 24  | 981 | 950 | 17  | 8   | 22  | 59  | 9   | 5   | 1   | 0   | 120 | 111 | 35  | 9   | 3   | 4   | 181 | 22  | 3,163 |
| Scams and/or Fraud | 96  | 140 | 12  | 23  | 33  | 83  | 90  | 20  | 833 | 48  | 1292 | 749 | 46  | 42  | 233 | 356 | 8   | 65  | 34  | 1   | 520 | 300 | 177 | 79  | 7   | 9   | 743 | 70  | 6,013 |
| Scope of Platform Service | 3   | 2   | 1   | 0   | 0   | 0   | 0   | 0   | 53  | 1   | 31  | 35  | 0   | 0   | 3   | 9   | 4   | 0   | 0   | 0   | 10  | 4   | 8   | 0   | 0   | 0   | 28  | 0   | 189 |
| Self-Harm | 1   | 4   | 0   | 1   | 2   | 4   | 5   | 0   | 74  | 2   | 41  | 72  | 0   | 0   | 4   | 8   | 1   | 0   | 1   | 0   | 7   | 11  | 4   | 2   | 0   | 0   | 56  | 6   | 305 |
| Unsafe and Illegal Products | 5   | 20  | 0   | 0   | 2   | 6   | 2   | 2   | 126 | 5   | 600 | 179 | 4   | 0   | 21  | 18  | 8   | 3   | 0   | 1   | 55  | 21  | 19  | 5   | 1   | 4   | 105 | 17  | 1224 |
| Violence | 57  | 177 | 12  | 4   | 4   | 45  | 41  | 7   | 1095 | 47  | 2274 | 1448 | 37  | 10  | 78  | 219 | 8   | 3   | 5   | 11  | 182 | 135 | 94  | 16  | 5   | 6   | 743 | 64  | 6,770 |
| Grand Total | 769 | 1117 | 119 | 102 | 118 | 487 | 435 | 107 | 10,025 | 367 | 18,325 | 18,502 | 459 | 247 | 1042 | 2,536 | 131 | 157 | 88  | 28  | 3,242 | 2,902 | 1412 | 352 | 73  | 78  | 7,480 | 506 | 71,206 |

REPORTS RESOLVED BY ACTIONS TAKEN ON ILLEGAL CONTENT

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Actions Taken on Illegal Content - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Enforcement Process | Action Type | Reason Code | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | EU  | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Latvia | Lithuania | Luxembourg | Malta | Netherlands | Poland | Portugal | Romania | Slovakia | Slovenia | Spain | Sweden | Grand Total |
| Automated Means | Global content deletion based on a violation of TIUC Terms of Service and Rules | Illegal or Harmful Speech | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 3   |
| Non-Consensual Behaviour | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 8   | 0   | 8   |
| Self-Harm | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 2   |
| Violence | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   |
| Country withheld Content | Data Protection & Privacy Violations | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 1   |
| Illegal or Harmful Speech | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   |
| No Violation Found | Animal Welfare | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 0   | 1   | 0   | 0   | 7   | 0   | 14  |
| Data Protection & Privacy Violations | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 1   | 1   | 1   | 0   | 0   | 0   | 3   | 0   | 8   |
| Illegal or Harmful Speech | 5   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 202 | 0   | 0   | 0   | 0   | 0   | 3   | 3   | 0   | 0   | 0   | 0   | 1   | 0   | 1   | 0   | 0   | 0   | 13  | 0   | 230 |
| Non-Consensual Behaviour | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 3   |
| Pornography or Sexualized Content | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 2   | 0   | 0   | 0   | 1   | 5   | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 7   | 1   | 19  |
| Protection of Minors | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 9   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 8   | 0   | 18  |
| Risk for Public Security | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   |
| Scams and Fraud | 8   | 5   | 1   | 0   | 2   | 0   | 4   | 1   | 33  | 3   | 0   | 0   | 0   | 1   | 5   | 4   | 0   | 0   | 3   | 0   | 35  | 3   | 1   | 12  | 0   | 0   | 15  | 5   | 141 |
| Scope of Platform Service | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 7   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 1   | 0   | 9   |
| Self-Harm | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 3   |
| Unsafe and Illegal Products | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 3   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 6   |
| Violence | 0   | 1   | 0   | 0   | 0   | 0   | 2   | 0   | 9   | 0   | 0   | 0   | 0   | 0   | 1   | 1   | 0   | 0   | 0   | 0   | 1   | 3   | 0   | 0   | 0   | 0   | 10  | 0   | 28  |
| Manual Closure | Global content deletion based on TIUC Terms of Service and Rules | Animal Welfare | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 14  | 0   | 11  | 8   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 3   | 1   | 0   | 0   | 1   | 0   | 0   | 0   | 38  |
| Data Protection & Privacy Violations | 1   | 2   | 0   | 0   | 0   | 3   | 3   | 0   | 15  | 1   | 15  | 33  | 4   | 0   | 3   | 4   | 0   | 0   | 0   | 0   | 5   | 7   | 7   | 1   | 0   | 1   | 15  | 3   | 123 |
| Illegal or Harmful Speech | 26  | 14  | 2   | 4   | 3   | 10  | 5   | 1   | 231 | 7   | 440 | 1,270 | 9   | 0   | 10  | 37  | 2   | 5   | 0   | 0   | 40  | 62  | 41  | 6   | 0   | 10  | 73  | 29  | 2,337 |
| Negative Effects on Civic Discourse or Elections | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 3   | 8   | 0   | 1   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 1   | 20  |
| Non-Consensual Behaviour | 1   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 35  | 0   | 13  | 14  | 1   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 1   | 1   | 0   | 0   | 0   | 0   | 13  | 0   | 81  |
| Pornography or Sexualized Content | 16  | 1   | 0   | 1   | 0   | 3   | 8   | 1   | 80  | 4   | 55  | 108 | 4   | 0   | 6   | 11  | 1   | 0   | 0   | 0   | 19  | 13  | 4   | 9   | 1   | 0   | 30  | 6   | 381 |
| Protection of Minors | 5   | 8   | 3   | 1   | 0   | 3   | 5   | 1   | 211 | 12  | 152 | 308 | 6   | 2   | 17  | 5   | 1   | 0   | 0   | 0   | 51  | 23  | 5   | 3   | 0   | 1   | 98  | 7   | 928 |
| Risk for Public Security | 0   | 3   | 0   | 1   | 0   | 1   | 0   | 0   | 28  | 0   | 56  | 87  | 2   | 0   | 3   | 2   | 0   | 0   | 0   | 0   | 5   | 5   | 6   | 0   | 0   | 0   | 33  | 5   | 237 |
| Scams and Fraud | 2   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 12  | 0   | 3   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 27  | 1   | 0   | 0   | 0   | 0   | 2   | 0   | 49  |
| Scope of Platform Service | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   |
| Self-Harm | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 8   | 0   | 2   | 5   | 0   | 0   | 1   | 0   | 1   | 0   | 0   | 0   | 0   | 3   | 1   | 0   | 0   | 0   | 12  | 1   | 35  |
| Unsafe and Illegal Products | 1   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 3   | 1   | 69  | 19  | 0   | 0   | 7   | 0   | 0   | 0   | 0   | 0   | 3   | 0   | 0   | 1   | 0   | 0   | 0   | 2   | 107 |
| Violence | 11  | 7   | 0   | 0   | 1   | 4   | 3   | 0   | 120 | 9   | 215 | 192 | 4   | 1   | 8   | 26  | 0   | 1   | 0   | 0   | 34  | 25  | 12  | 2   | 0   | 1   | 48  | 17  | 741 |
| Temporary suspension and global content deletion based on TIUC Terms of Service and Rules | Data Protection & Privacy Violations | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   |
| Illegal or Harmful Speech | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 16  | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 20  |
| Pornography or Sexualized Content | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   |
| Protection of Minors | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   |
| Risk for Public Security | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   |
| Scams and Fraud | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   |
| Violence | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 28  | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 29  |
| [Offer of help in case of self-harm and suicide concern](https://www.google.com/url?q=https://help.twitter.com/en/safety-and-security/self-harm-and-suicide&sa=D&source=editors&ust=1700092925879477&usg=AOvVaw0nL1yBZyeploWrYvZCQFnW) based on TIUC Terms of Service and Rules | Protection of minors | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 1   |
| Self-Harm | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 5   | 0   | 1   | 3   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 11  |
| Content removed globally | Animal Welfare | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   |
| Data Protection & Privacy Violations | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   |
| Illegal or Harmful Speech | 0   | 2   | 0   | 0   | 3   | 0   | 2   | 0   | 0   | 0   | 1   | 5   | 1   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 3   | 0   | 18  |
| Non-Consensual Behaviour | 0   | 3   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 3   |
| Pornography or Sexualized Content | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 5   | 3   | 0   | 1   | 0   | 2   | 0   | 0   | 0   | 0   | 5   | 0   | 0   | 6   | 0   | 0   | 1   | 0   | 26  |
| Protection of Minors | 0   | 5   | 0   | 0   | 0   | 2   | 0   | 0   | 7   | 0   | 0   | 8   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 23  |
| Risk for Public Security | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 6   | 0   | 3   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 9   |
| Scams and Fraud | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 7   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 12  |
| Self-Harm | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   |
| Unsafe and Illegal Products | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 5   | 0   | 0   | 0   | 0   | 0   | 5   |
| Country withheld Account | Illegal or Harmful Speech | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   |
| Pornography or Sexualized Content | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 8   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 8   |
| Scams and fraud | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 1   |
| Violence | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   |
| Country withheld Content | Animal Welfare | 1   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 5   | 0   | 2   | 6   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 2   | 0   | 0   | 0   | 0   | 8   | 0   | 27  |
| Data Protection & Privacy Violations | 0   | 3   | 1   | 0   | 1   | 3   | 2   | 0   | 30  | 1   | 61  | 91  | 2   | 5   | 24  | 12  | 0   | 0   | 0   | 0   | 24  | 7   | 13  | 5   | 0   | 0   | 46  | 2   | 333 |
| Illegal or Harmful Speech | 84  | 92  | 2   | 9   | 2   | 39  | 32  | 4   | 1,131 | 21  | 2,433 | 2,962 | 32  | 5   | 90  | 315 | 9   | 3   | 6   | 0   | 209 | 204 | 138 | 16  | 12  | 2   | 698 | 49  | 8,599 |
| Negative Effects on Civic Discourse or Elections | 5   | 2   | 0   | 0   | 0   | 1   | 0   | 0   | 26  | 0   | 8   | 88  | 1   | 0   | 2   | 7   | 0   | 0   | 1   | 0   | 6   | 30  | 6   | 0   | 0   | 0   | 7   | 0   | 190 |
| Non-Consensual Behaviour | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 2   | 36  | 1   | 29  | 32  | 1   | 0   | 4   | 2   | 0   | 1   | 0   | 0   | 7   | 3   | 0   | 1   | 0   | 0   | 14  | 0   | 135 |
| Pornography or Sexualized Content | 6   | 9   | 6   | 2   | 1   | 3   | 4   | 0   | 104 | 3   | 116 | 230 | 3   | 4   | 10  | 23  | 3   | 2   | 1   | 0   | 14  | 24  | 15  | 8   | 1   | 0   | 100 | 6   | 698 |
| Protection of Minors | 1   | 6   | 0   | 2   | 0   | 1   | 2   | 0   | 19  | 2   | 21  | 50  | 2   | 0   | 10  | 5   | 0   | 0   | 1   | 0   | 3   | 5   | 5   | 3   | 0   | 0   | 15  | 3   | 156 |
| Risk for Public Security | 2   | 8   | 0   | 0   | 1   | 2   | 0   | 0   | 19  | 0   | 46  | 89  | 3   | 0   | 0   | 4   | 1   | 0   | 0   | 0   | 3   | 12  | 2   | 0   | 0   | 0   | 2   | 3   | 197 |
| Scams and Fraud | 6   | 2   | 0   | 0   | 0   | 6   | 5   | 1   | 29  | 2   | 34  | 54  | 1   | 0   | 3   | 5   | 0   | 2   | 0   | 0   | 16  | 24  | 2   | 3   | 0   | 2   | 16  | 3   | 216 |
| Scope of Platform Service | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 4   | 0   | 0   | 0   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 1   | 0   | 0   | 0   | 0   | 0   | 8   |
| Self-Harm | 0   | 0   | 1   | 0   | 1   | 0   | 0   | 0   | 6   | 0   | 5   | 2   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 2   | 2   | 19  |
| Unsafe and Illegal Products | 0   | 2   | 0   | 0   | 0   | 3   | 0   | 0   | 16  | 1   | 277 | 33  | 0   | 0   | 2   | 0   | 1   | 0   | 0   | 0   | 13  | 6   | 0   | 1   | 0   | 0   | 8   | 2   | 365 |
| Violence | 5   | 6   | 3   | 0   | 0   | 8   | 8   | 0   | 81  | 15  | 518 | 212 | 5   | 1   | 11  | 27  | 3   | 0   | 1   | 0   | 19  | 19  | 18  | 3   | 1   | 0   | 68  | 23  | 1,055 |
| Globally withheld content | Intellectual Property Infringements | 4   | 10  | 0   | 0   | 0   | 6   | 5   | 0   | 0   | 31  | 561 | 167 | 17  | 2   | 17  | 84  | 2   | 17  | 0   | 3   | 101 | 450 | 38  | 22  | 0   | 0   | 283 | 9   | 1,829 |
| Account Suspension | Intellectual Property Infringements | 1   | 1   | 0   | 6   | 5   | 0   | 1   | 0   | 0   | 1   | 28  | 498 | 0   | 0   | 7   | 22  | 1   | 7   | 0   | 0   | 27  | 39  | 88  | 19  | 0   | 0   | 104 | 0   | 855 |
| No Violation Found | Animal Welfare | 13  | 3   | 1   | 2   | 4   | 2   | 4   | 1   | 61  | 4   | 56  | 45  | 4   | 1   | 9   | 9   | 1   | 1   | 1   | 1   | 10  | 15  | 3   | 1   | 1   | 2   | 42  | 3   | 300 |
| Data Protection & Privacy Violations | 17  | 47  | 5   | 5   | 4   | 10  | 15  | 6   | 452 | 11  | 470 | 443 | 39  | 2   | 63  | 82  | 5   | 0   | 3   | 0   | 135 | 78  | 49  | 12  | 3   | 9   | 632 | 22  | 2,619 |
| Illegal or Harmful Speech | 280 | 340 | 44  | 20  | 25  | 165 | 133 | 40  | 3,729 | 109 | 5,460 | 6,887 | 154 | 27  | 234 | 872 | 52  | 35  | 33  | 7   | 721 | 645 | 456 | 80  | 16  | 16  | 2,356 | 125 | 23,061 |
| Negative Effects on Civic Discourse or Elections | 21  | 30  | 8   | 1   | 3   | 25  | 13  | 7   | 462 | 15  | 259 | 833 | 14  | 5   | 23  | 129 | 3   | 5   | 1   | 1   | 211 | 496 | 17  | 17  | 15  | 3   | 121 | 9   | 2,747 |
| Non-Consensual Behaviour | 13  | 10  | 1   | 5   | 2   | 2   | 12  | 2   | 94  | 8   | 102 | 87  | 5   | 15  | 27  | 34  | 0   | 1   | 0   | 0   | 27  | 13  | 15  | 1   | 1   | 0   | 147 | 21  | 645 |
| Pornography or Sexualized Content | 16  | 40  | 3   | 0   | 3   | 19  | 10  | 0   | 283 | 3   | 321 | 293 | 41  | 93  | 41  | 103 | 1   | 1   | 1   | 2   | 75  | 69  | 48  | 24  | 6   | 2   | 176 | 12  | 1,686 |
| Protection of Minors | 36  | 25  | 8   | 4   | 3   | 15  | 17  | 2   | 229 | 9   | 293 | 198 | 16  | 5   | 39  | 46  | 12  | 0   | 0   | 0   | 55  | 45  | 6   | 2   | 2   | 12  | 185 | 7   | 1,271 |
| Risk for Public Security | 37  | 90  | 8   | 3   | 3   | 42  | 14  | 9   | 363 | 24  | 729 | 756 | 12  | 6   | 19  | 53  | 9   | 5   | 1   | 0   | 115 | 97  | 28  | 9   | 3   | 4   | 148 | 12  | 2,599 |
| Scams and Fraud | 65  | 130 | 11  | 19  | 31  | 76  | 79  | 19  | 730 | 42  | 488 | 646 | 45  | 37  | 184 | 311 | 8   | 63  | 30  | 1   | 365 | 262 | 141 | 61  | 7   | 7   | 660 | 61  | 4,579 |
| Scope of Platform Service | 2   | 2   | 1   | 0   | 0   | 0   | 0   | 0   | 38  | 1   | 9   | 30  | 0   | 0   | 3   | 10  | 4   | 0   | 0   | 0   | 10  | 3   | 5   | 0   | 0   | 0   | 27  | 0   | 145 |
| Self-Harm | 1   | 3   | 0   | 1   | 1   | 4   | 4   | 0   | 57  | 2   | 28  | 61  | 0   | 0   | 3   | 12  | 0   | 0   | 3   | 0   | 7   | 8   | 4   | 2   | 0   | 0   | 37  | 3   | 241 |
| Unsafe and Illegal Products | 3   | 16  | 0   | 0   | 1   | 3   | 1   | 2   | 106 | 4   | 179 | 126 | 5   | 1   | 12  | 18  | 7   | 3   | 0   | 1   | 38  | 15  | 11  | 3   | 1   | 4   | 98  | 13  | 671 |
| Violence | 39  | 156 | 7   | 4   | 3   | 33  | 27  | 7   | 901 | 25  | 1,225 | 1,034 | 28  | 8   | 59  | 170 | 5   | 2   | 7   | 11  | 125 | 92  | 63  | 10  | 4   | 5   | 588 | 24  | 4,662 |
| Grand Total |     |     | 737 | 1,095 | 117 | 90  | 105 | 495 | 424 | 106 | 10,066 | 372 | 14,866 | 18,043 | 462 | 228 | 964 | 2,459 | 132 | 154 | 93  | 27  | 2,579 | 2,813 | 1,257 | 344 | 75  | 81  | 6,997 | 491 | 65,672 |

REPORTS OF ILLEGAL CONTENT MEDIAN HANDLE TIME

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Reports of Illegal Content Median Handle Time (Hours) - Aug 28 to Oct 20 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Enforcement Process | Action Type | Reason Code | Austria | Belgium | Bulgaria | Croatia | Cyprus | Czechia | Denmark | Estonia | EU  | Finland | France | Germany | Greece | Hungary | Ireland | Italy | Latvia | Lithuania | Luxembourg | Malta | Netherlands | Poland | Portugal | Romania | Slovakia | Slovenia | Spain | Sweden |
| Automated Means | Global content deletion based on TIUC Terms of Service and Rules | Illegal or Harmful Speech | 34.3 |     |     |     |     |     |     |     | 29.5 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Non-Consensual Behaviour |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 92.8 |     |
| Self-Harm |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 92.7 |     |
| Violence |     |     |     |     |     |     |     |     | 21.4 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Country withheld Content | Data Protection & Privacy Violations |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 117.0 |     |
| Illegal or Harmful Speech |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 106.3 |     |     |     |     |     |     |     |
| No violation found | Animal Welfare |     |     |     |     |     |     |     |     | 45.7 |     |     |     |     |     |     |     |     |     |     |     | 82.9 |     |     | 26.5 |     |     | 47.9 |     |
| Data Protection & Privacy Violations |     |     |     |     |     |     |     |     | 30.1 |     |     |     |     |     |     | 32.9 |     |     |     |     | 50.3 | 33.7 | 104.0 |     |     |     | 39.4 |     |
| Illegal or Harmful Speech | 34.4 | 33.1 |     |     |     |     |     |     | 36.6 |     |     |     |     |     | 24.3 | 27.7 |     |     |     |     | 106.3 |     | 79.0 |     |     |     | 43.0 |     |
| Non-Consensual Behaviour |     |     | 44.5 |     |     |     |     |     | 29.5 |     |     |     |     |     |     |     |     |     |     |     |     |     | 30.5 |     |     |     |     |     |
| Pornography or Sexualized Content |     |     |     |     |     |     | 25.7 |     | 22.6 |     |     |     | 69.5 | 48.7 |     | 89.2 |     |     |     |     |     |     |     |     |     |     | 32.1 | 22.3 |
| Protection of Minors | 22.1 |     |     |     |     |     |     |     | 27.0 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 24.0 |     |
| Risk for Public Security |     |     |     |     |     |     |     |     | 96.1 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Scams and Fraud | 75.6 | 73.5 | 102.2 |     | 202.8 |     | 26.1 | 91.4 | 44.3 | 28.7 |     |     |     | 73.4 | 57.8 | 35.1 |     |     | 223.8 |     | 45.2 | 34.1 | 52.3 | 75.4 |     |     | 44.0 | 27.2 |
| Scope of Platform Service |     |     |     |     |     |     |     |     | 42.5 |     |     |     |     |     |     |     |     |     |     |     |     | 85.2 |     |     |     |     | 36.0 |     |
| Self-Harm |     | 52.2 |     |     |     |     |     |     | 21.3 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 49.1 |     |
| Unsafe and Illegal Products |     |     |     |     | 35.4 |     |     |     | 40.8 |     |     |     |     |     |     |     |     |     |     |     | 29.8 |     |     |     |     |     | 19.2 |     |
| Violence |     | 70.6 |     |     |     |     | 31.0 |     | 115.2 |     |     |     |     |     | 47.8 | 24.1 |     |     |     |     | 20.5 | 28.2 |     |     |     |     | 42.8 |     |
| Manual Closure | Global content deletion based on TIUC Terms of Service and Rules | Animal Welfare |     |     |     |     |     |     |     |     | 3.0 |     | 8.6 | 8.4 |     |     |     |     |     |     |     |     | 91.0 |     |     |     | 13.2 |     |     |     |
| Data Protection & Privacy Violations | 10.7 | 0.5 |     |     |     | 17.2 | 13.7 |     | 12.3 | 37.7 | 6.4 | 3.2 | 42.3 |     | 11.1 | 14.8 |     |     |     |     | 16.0 | 158.7 | 3.1 | 1.8 |     | 0.9 | 50.0 | 2.2 |
| Illegal or Harmful Speech | 13.3 | 10.5 | 7.6 | 15.2 | 12.6 | 2.1 | 2.7 | 0.2 | 8.0 | 9.6 | 4.8 | 3.1 | 11.2 |     | 12.6 | 9.6 | 45.0 | 5.4 |     |     | 13.0 | 12.8 | 6.3 | 9.0 |     | 22.8 | 10.4 | 14.6 |
| Negative Effects on Civic Discourse or Elections |     | 37.3 |     |     |     |     |     |     | 178.3 |     | 13.0 | 6.3 |     | 0.1 | 5.8 |     |     |     |     |     |     | 22.5 |     |     |     |     |     | 11.2 |
| Non-Consensual Behaviour | 3.1 | 53.4 |     |     | 33.2 |     |     |     | 12.0 |     | 4.1 | 10.6 | 26.9 |     | 88.3 |     |     |     |     |     | 15.7 | 1.3 |     |     |     |     | 11.5 |     |
| Pornography or Sexualized Content | 10.8 | 10.4 |     | 11.2 |     | 15.3 | 9.6 | 3.0 | 10.8 | 3.3 | 7.9 | 5.1 | 8.3 |     | 11.0 | 2.8 | 1.2 |     |     |     | 19.4 | 2.4 | 10.3 | 14.3 | 13.6 |     | 13.3 | 11.3 |
| Protection of Minors | 9.6 | 12.4 | 15.9 | 75.5 |     | 13.8 | 15.4 | 20.5 | 7.7 | 4.8 | 3.9 | 3.8 | 3.3 | 28.0 | 9.7 | 11.0 |     |     |     |     | 8.6 | 2.1 | 10.0 | 14.8 |     | 16.5 | 9.9 | 12.0 |
| Risk for Public Security |     | 5.6 |     | 2.3 |     | 0.8 |     |     | 11.8 |     | 4.1 | 1.5 | 14.5 |     | 10.1 | 0.7 |     |     |     |     | 1.4 | 6.0 | 4.3 |     |     |     | 0.3 | 11.8 |
| Scams and Fraud | 2.8 |     |     |     |     | 1.2 |     |     | 62.2 |     | 1.3 | 1.4 |     |     |     |     |     |     |     |     | 63.0 | 180.9 |     |     |     |     | 36.7 |     |
| Scope of Platform Service |     |     |     |     |     |     |     |     | 62.4 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Self-Harm |     |     |     |     |     |     | 1.5 |     | 3.0 |     | 13.5 | 8.9 |     |     | 10.8 |     | 16.9 |     |     |     |     | 17.7 | 0.9 |     |     |     | 36.8 | 0.2 |
| Unsafe and Illegal Products | 1.2 |     |     |     |     |     | 21.5 |     | 13.1 | 5.2 | 4.1 | 1.3 |     |     | 5.3 |     |     |     |     |     | 12.6 |     |     | 1.5 |     |     |     | 31.7 |
| Violence | 11.7 | 6.3 | 0.0 |     | 22.8 | 0.5 | 5.2 |     | 12.3 | 4.3 | 3.1 | 2.8 | 6.8 | 51.1 | 14.6 | 15.7 |     | 16.2 |     |     | 11.8 | 11.3 | 6.1 | 0.3 |     | 17.6 | 11.3 | 1.7 |
| Temporary suspension and global content deletion based on TIUC Terms of Service and Rules | Data Protection & Privacy Violations |     |     |     |     |     |     | 4.5 |     |     |     |     | 1.1 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Illegal or Harmful Speech |     |     |     |     |     |     |     |     |     |     | 1.4 | 2.7 |     |     |     |     |     |     |     |     | 18.7 |     |     |     |     |     |     |     |
| Pornography or Sexualized Content |     |     |     |     |     |     |     |     |     |     | 4.6 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Protection of Minors |     |     |     |     |     |     |     |     |     |     | 14.4 | 17.1 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Risk for Public Security |     |     |     |     |     |     |     |     |     |     | 26.5 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Scams and Fraud |     |     |     |     |     |     |     |     |     |     | 14.1 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Violence |     |     |     |     |     |     |     |     |     |     | 4.5 | 1.3 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| [Offer of help in case of self-harm and suicide concern](https://www.google.com/url?q=https://help.twitter.com/en/safety-and-security/self-harm-and-suicide&sa=D&source=editors&ust=1700092926605695&usg=AOvVaw2k_OxfY3x3xTzk1P_-ecMp) based on TIUC Terms of Service and Rules | Protection of minors |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 22.2 |     |
| Self-harm |     |     |     |     |     |     |     |     | 12.9 |     | 4.5 | 3.9 |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 8.4 |     |
| Content removed globally | Animal Welfare |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 0.0 |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Data Protection & Privacy Violations |     |     |     |     | 0.0 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Illegal or Harmful Speech |     | 74.8 |     |     | 0.4 |     | 11.7 |     |     |     | 34.0 | 0.1 | 0.0 |     |     | 24.4 |     |     |     |     |     |     |     |     |     |     | 155.3 |     |
| Non-Consensual Behaviour |     | 102.9 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Pornography or Sexualized Content |     | 52.2 |     |     |     |     |     |     | 88.1 |     | 4.0 | 16.2 |     | 0.4 |     | 160.4 |     |     |     |     | 146.5 |     |     | 23.8 |     |     | 13.3 |     |
| Protection of Minors |     | 2.0 |     |     |     | 8.9 |     |     | 23.9 |     |     | 17.2 |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 27.6 |     |
| Risk for Public Security |     |     |     |     |     |     |     |     | 32.1 |     | 26.5 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Scams and Fraud |     |     |     |     |     |     |     |     | 502.2 |     | 19.6 |     |     |     |     |     |     |     |     |     | 101.8 |     |     |     |     |     |     |     |
| Self-Harm |     |     |     |     |     |     |     |     |     |     |     | 1.2 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Unsafe and Illegal Products |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 18.6 |     |     |     |     |     |
| Country withheld Account | Illegal or Harmful Speech |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 51.5 |     |     |     |     |     |     |     |     |     |     |     |     |
| Pornography or Sexualized Content |     |     |     |     |     |     |     |     |     |     |     | 31.0 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Scams and Fraud |     |     |     |     |     |     |     |     |     |     | 33.0 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Violence |     |     |     |     |     |     |     |     |     |     | 6.9 |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| Country withheld Content | Animal Welfare | 0.0 | 10.1 |     |     |     |     |     |     | 18.7 |     | 13.0 | 9.1 |     |     |     | 6.6 |     |     |     |     |     | 7.5 |     |     |     |     | 10.8 |     |
| Data Protection & Privacy Violations |     | 17.2 | 157.6 |     | 0.4 | 139.4 | 0.3 |     | 90.6 | 53.3 | 6.6 | 2.2 | 104.2 | 33.9 | 125.9 | 52.8 |     |     |     |     | 10.2 | 123.5 | 9.7 | 26.3 |     |     | 55.4 | 108.2 |
| Illegal or Harmful Speech | 12.9 | 3.6 | 4.1 | 12.4 | 22.9 | 14.3 | 10.3 | 72.7 | 126.2 | 13.3 | 8.2 | 3.0 | 2.7 | 1.2 | 11.2 | 5.8 | 11.2 | 21.3 | 0.3 |     | 17.6 | 11.8 | 9.9 | 10.2 | 1.5 | 7.4 | 49.9 | 11.4 |
| Negative Effects on Civic Discourse or Elections | 8.4 | 17.1 |     |     |     | 1.8 |     |     | 157.0 |     | 15.1 | 2.8 | 1.8 |     | 35.4 | 4.7 |     |     | 11.7 |     | 9.6 | 12.7 | 3.1 |     |     |     | 47.4 |     |
| Non-Consensual Behaviour |     | 12.7 |     |     |     |     |     | 110.7 | 128.0 | 5.0 | 10.9 | 3.0 | 134.9 |     | 38.2 | 43.0 |     | 16.2 |     |     | 5.0 | 121.6 |     | 12.5 |     |     | 46.8 |     |
| Pornography or Sexualized Content | 12.3 | 17.0 | 18.8 | 9.9 | 47.3 | 1.8 | 35.2 |     | 88.0 | 3.8 | 12.4 | 5.2 | 7.9 | 49.0 | 10.1 | 4.1 | 12.5 | 51.5 | 4.3 |     | 13.5 | 19.5 | 1.5 | 1.9 | 77.4 |     | 13.8 | 31.1 |
| Protection of Minors | 8.1 | 12.1 |     | 0.6 |     | 162.2 | 28.9 |     | 44.2 | 138.5 | 12.1 | 10.2 | 4.8 |     | 9.0 | 93.0 |     |     | 0.2 |     | 11.2 | 4.1 | 11.9 | 4.6 |     |     | 17.2 | 19.3 |
| Risk for Public Security | 96.3 | 2.8 |     |     | 167.6 | 73.5 |     |     | 141.0 |     | 3.1 | 6.8 | 2.1 |     | 143.2 | 7.2 | 13.0 |     |     |     | 146.6 | 27.0 | 6.6 |     |     |     | 19.2 | 2.4 |
| Scams and Fraud | 109.1 | 124.0 |     |     |     | 161.3 | 72.8 | 137.1 | 141.1 | 37.3 | 11.3 | 128.6 | 6.0 |     | 185.6 | 150.2 |     | 161.2 |     |     | 130.1 | 71.8 | 80.3 | 170.9 |     | 79.4 | 135.4 | 138.5 |
| Scope of Platform Service | 6.3 |     |     |     |     |     |     |     |     |     |     | 0.9 |     |     |     | 23.9 |     |     |     |     |     |     | 31.2 |     |     |     |     |     |
| Self-Harm |     |     |     |     | 1.8 |     |     |     | 67.2 |     | 9.6 | 0.3 |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 27.8 | 14.8 |
| Unsafe and Illegal Products |     | 165.7 |     |     |     | 49.0 |     |     | 111.2 |     | 2.0 | 12.4 |     |     | 0.5 |     | 66.9 |     |     |     | 87.8 | 18.8 |     | 2.5 |     |     | 97.1 | 90.0 |
| Violence | 59.1 | 1.2 | 13.3 |     |     | 12.0 | 51.1 |     | 122.0 | 4.9 | 2.4 | 4.7 | 3.0 | 0.2 | 1.6 | 11.5 | 18.5 |     | 3.1 |     | 15.8 | 16.4 | 8.9 | 0.3 | 20.9 |     | 18.8 | 14.6 |
| Globally withheld Content | Intellectual Property Infringements | 5.6 | 3.1 |     |     |     | 5.6 | 6.3 |     |     | 2.8 | 0.6 | 2.6 | 3.6 | 0.4 | 2.8 | 1.5 | 53.2 | 0.4 |     | 7.6 | 2.6 | 0.5 | 3.8 | 1.8 |     |     | 2.5 | 1.6 |
| Account Suspension | Intellectual Property Infringements | 81.2 | 14.7 |     | 31.8 | 58.8 |     | 77.7 |     |     | 63.3 | 81.7 | 50.7 |     |     | 29.5 | 66.7 | 68.6 | 86.3 |     |     | 94.6 | 56.5 | 32.8 | 43.1 |     |     | 77.4 |     |
| No violation found | Animal Welfare | 53.6 | 16.2 | 20.5 | 14.4 | 18.6 | 10.3 | 9.3 | 20.5 | 18.3 | 16.3 | 17.2 | 1.0 | 40.3 | 20.4 | 0.0 | 20.4 | 20.4 | 20.4 | 20.4 | 20.4 | 5.1 | 13.5 | 10.4 | 20.4 | 20.3 | 16.5 | 16.4 | 20.5 |
| Data Protection & Privacy Violations | 3.1 | 11.1 | 73.6 | 8.7 | 3.3 | 5.1 | 24.2 | 7.6 | 13.0 | 9.7 | 13.5 | 2.4 | 1.9 | 59.8 | 47.1 | 15.2 | 2.1 |     | 31.3 |     | 12.9 | 17.8 | 13.5 | 12.1 | 0.4 | 13.3 | 16.2 | 8.3 |
| Illegal or Harmful Speech | 9.6 | 7.6 | 13.1 | 12.2 | 16.7 | 9.2 | 4.4 | 12.8 | 11.1 | 14.3 | 11.0 | 2.7 | 8.4 | 16.4 | 11.0 | 8.4 | 11.8 | 10.9 | 2.4 | 119.7 | 14.8 | 12.7 | 8.0 | 11.6 | 2.0 | 13.9 | 9.9 | 4.4 |
| Intellectual property infringements | 28.7 | 4.5 | 2.1 | 31.7 | 101.8 | 47.2 | 52.8 | 6.1 |     | 4.5 | 37.2 | 33.9 | 1.1 | 9.5 | 25.6 | 10.4 | 48.6 | 36.1 | 38.4 | 30.2 | 53.0 | 35.4 | 25.9 | 43.2 |     |     | 53.5 | 56.8 |
| Negative Effects on Civic Discourse or Elections | 9.2 | 8.8 | 14.4 | 4.4 | 0.2 | 2.2 | 2.0 | 10.1 | 7.7 | 14.3 | 19.2 | 2.1 | 4.3 | 14.4 | 10.4 | 2.8 | 6.1 | 12.2 | 259.9 | 63.1 | 10.9 | 5.9 | 10.2 | 27.5 | 2.3 | 2.7 | 11.1 | 0.7 |
| Non-Consensual Behaviour | 4.7 | 8.1 | 16.9 | 63.0 | 6.5 | 5.7 | 17.1 | 5.9 | 16.0 | 45.5 | 11.8 | 4.9 | 12.3 | 84.0 | 20.7 | 14.2 |     | 143.8 |     |     | 11.8 | 17.3 | 19.1 | 0.1 | 0.4 |     | 11.4 | 9.6 |
| Pornography or Sexualized Content | 5.4 | 13.3 | 13.8 |     | 68.7 | 15.0 | 4.4 |     | 12.9 | 4.1 | 11.0 | 10.6 | 21.7 | 15.6 | 11.8 | 17.1 | 12.3 | 15.5 | 49.4 | 2.4 | 16.3 | 11.0 | 14.2 | 11.2 | 54.1 | 11.7 | 13.7 | 11.3 |
| Protection of Minors | 11.2 | 8.6 | 8.6 | 14.6 | 8.6 | 9.0 | 13.5 | 8.8 | 13.7 | 6.3 | 12.5 | 3.7 | 6.6 | 11.3 | 10.2 | 11.6 | 11.1 |     |     |     | 14.0 | 13.1 | 13.4 | 2.2 | 9.8 | 17.7 | 14.2 | 12.1 |
| Risk for Public Security | 5.0 | 9.0 | 1.5 | 15.4 | 3.6 | 3.1 | 2.3 | 8.2 | 8.7 | 3.5 | 10.9 | 4.2 | 16.0 | 13.2 | 7.7 | 6.3 | 10.3 | 9.9 | 4.1 |     | 13.0 | 11.6 | 11.7 | 10.6 | 0.3 | 5.4 | 9.9 | 5.2 |
| Scams and Fraud | 17.5 | 18.7 | 3.6 | 3.4 | 12.3 | 12.6 | 14.8 | 1.5 | 18.7 | 88.6 | 16.0 | 5.4 | 20.6 | 16.8 | 12.7 | 20.2 | 21.4 | 108.5 | 3.6 | 14.0 | 17.2 | 13.2 | 24.2 | 4.7 | 16.6 | 48.6 | 13.7 | 75.0 |
| Scope of Platform Service | 114.5 | 73.2 | 7.5 |     |     |     |     |     | 9.4 | 4.6 | 5.9 | 7.3 |     |     | 15.2 | 18.5 | 15.6 |     |     |     | 4.5 | 15.8 | 27.0 |     |     |     | 23.6 |     |
| Self-Harm | 2.1 | 13.6 |     | 0.4 | 1.0 | 41.4 | 47.4 |     | 11.2 | 1.5 | 11.6 | 9.5 |     |     | 15.8 | 27.8 |     |     | 1.8 |     | 23.5 | 12.7 | 2.3 | 23.9 |     |     | 11.1 | 2.9 |
| Unsafe and Illegal Products | 12.5 | 13.4 |     |     | 77.1 | 10.7 | 7.8 | 6.8 | 13.4 | 12.6 | 18.7 | 7.4 | 15.5 |     | 12.5 | 142.9 | 7.0 | 13.9 |     | 1.7 | 4.2 | 4.2 | 22.5 | 1.0 | 10.9 | 17.1 | 10.7 | 20.2 |
| Violence | 11.9 | 9.8 | 13.1 | 32.8 | 89.8 | 10.3 | 49.9 | 83.4 | 10.8 | 8.5 | 10.4 | 3.7 | 13.2 | 7.7 | 10.4 | 10.7 | 2.5 | 0.9 | 1.6 | 2.1 | 11.0 | 12.9 | 10.6 | 10.3 | 12.5 | 1.7 | 10.9 | 9.1 |

Important Notes about Actions taken on illegal content:

1. Disparity between reports received and reports handled is caused by the pending cases at the end of the reporting period.
2. We only use automated means to close user reports of illegal content where: (i) reported content is no longer accessible to the reporter following other means/workflows; or (ii) reporter displays bad actor patterns.
3. The numbers of “Intellectual property infringements” reflect reports instead of individual items of content and accounts. Actions taken against intellectual property infringements are made globally meaning that media that infringes copyright and accounts that infringe trademarks will be disabled globally.
4. Action Types: actions that do not reference TIUC Terms of Service and Rules have been taken based on illegality.
5. To improve clarity, we've omitted countries and violation types with zero reports from the tables above.
6. The tables REPORTS RESOLVED BY ACTIONS TAKEN ON ILLEGAL CONTENT and REPORTS OF ILLEGAL CONTENT MEDIAN HANDLE TIME were updated on 13 November 2023 to replace an undefined description "reported content" with the relevant enforcement method "manual closure".