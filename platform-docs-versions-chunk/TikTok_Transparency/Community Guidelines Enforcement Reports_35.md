platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-2/


### Sécurité

TikTok associe technologie et intervention humaine pour faire respecter ses Règles Communautaires et préserver une communauté accueillante et bienveillante. Pour y parvenir efficacement à grande échelle, nous continuons d’investir dans les technologies de signalement et de modération. Lorsque nos logiciels permettent de détecter avec fiabilité tout contenu illicite, nous utilisons des dispositifs de modération automatisés afin de pouvoir supprimer rapidement les contenus qui enfreignent nos politiques. Nos capacités en matière de détection et de protection se sont améliorées en conséquence.

L’utilisation des technologies de machine learning a été déterminant dans la lutte contre la circulation de fausses informations. Nous avons amélioré les capacités d’adaptation de nos systèmes pour faire face au caractère imprévisible et en constante évolution des contenus de désinformation, en particulier lors de crises ou d’événements exceptionnels (par exemple, pendant la guerre en Ukraine ou à l’occasion d’une élection). Nous avons également développé plus encore nos capacités à détecter les contenus audiovisuels avérés trompeurs afin de limiter la manipulation des contenus sur la plateforme. Au vu de ces investissements, nous enregistrons ce trimestre des améliorations concernant l’application de nos politiques d’intégrité et d’authenticité : la suppression proactive des vidéos progresse, passant de 83,6 % au premier trimestre à 89,1 % au deuxième trimestre ; la suppression des vidéos sans aucune lecture s’améliore, passant de 60,8 % au premier trimestre à 74,7 % au deuxième trimestre ; enfin, la suppression des vidéos en moins de 24 heures est passée de 71,9 % à 83,9 %.