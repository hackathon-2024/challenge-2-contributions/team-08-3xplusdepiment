platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2022-1/

### **Analyse**

Par rapport à la dernière période de rapport, nous avons constaté une augmentation du nombre de demandes de retrait justifiées fondées sur une infraction aux lois sur le droit d’auteur et sur les marques. Les facteurs qui ont contribué à cette augmentation du volume sont les suivants : les améliorations apportées à nos processus de vérification des e-mails, la redirection des rapports liés aux annonces vers le [formulaire Web](https://www.tiktok.com/business/en/report) pertinent et les interactions avec les détenteurs de droits afin de partager les bonnes pratiques pour soumettre des demandes valides.