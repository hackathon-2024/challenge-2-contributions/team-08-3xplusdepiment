platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/


### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nupfpxbog/French_LIPGR_2021_H1.xlsx)

#### Notifications de retrait de contenus pour infraction aux lois sur le droit d’auteur

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays/région** | **Période** | **Total des rapports de demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur** | **Total des de demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur** | **Pourcentage moyen des demandes honorées de retrait de contenus pour infraction aux lois sur le droit d’auteur** |
| Global | 1er janv. 2021-2 mai 2021 | 78140 | 16662 | 21,32% |

_REMARQUE : En raison des limites_ _techniques_ _des outils utilisés_ _dans ces calculs__, toutes les notifications de retrait de contenus pour infraction aux lois sur le droit d’auteur sont incluses dans les statistiques de retraits présentées ci-dessus._

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays/région** | **Période** | **Total des demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur** | **Demandes honorées de retrait de contenus pour infraction aux lois sur le droit d’auteur** | **Pourcentage moyen des demandes honorées de retrait de contenus pour infraction aux lois sur les droits d’auteur** |
| Global | 3 mai 2021-30 juin 2021 | 11646 | 10667 | 91,59% |

_REMARQUE : Depuis le 3 mai 2021, seules les notifications justifiées de retrait de contenus pour infraction aux lois sur le droit d’auteur sont incluses dans les statistiques de retraits présentées ci-dessus. Les notifications de retrait de contenus pour infraction aux lois sur le droit d’auteur désignent des notifications qui reprennent les éléments statutaires définis dans le DMCA, la directive européenne sur les droits d’auteur et d’autres lois similaires, nécessaires pour signaler une atteinte présumée au droit d’auteur. En réponse aux notifications incomplètes de retrait de contenus pour infraction aux lois sur le droit d’auteur ou qui ne concernent pas des questions relatives aux marques ou que nous estimons frauduleuses, nous ne supprimerons pas le contenu visé, nous n’en désactiverons pas l’accès ni ne cesserons de le diffuser ou de le mettre à la disposition du public. Nous analysons scrupuleusement chaque notification et effectuons un suivi auprès du plaignant, s’il y a lieu._

#### Notifications de retrait de contenus pour infraction aux lois sur les marques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays/région** | **Période** | **Total des rapports de demandes de retrait de contenus pour infraction aux lois sur les marques** | **Demandes honorées de retrait de contenus pour infraction aux lois sur les marques** | **Pourcentage moyen des demandes honorées de retrait de contenus pour infraction aux lois sur les marques** |
| Global | 1er janv. 2021-2 mai 2021 | 4 366 | 620 | 14,20% |

_REMARQUE :_ _En raison des limites techniques des outils utilisés dans ces calculs__, toutes les notifications de retrait de contenus pour infraction aux lois sur les marques sont incluses dans les statistiques de retraits présentées ci-dessus._

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays/région** | **Période** | **Total des rapports de demandes de retrait de contenus pour infraction aux lois sur les marques** | **Demandes honorées de retrait de contenus pour infraction aux lois sur les marques** | **Pourcentage moyen des demandes honorées de retrait de contenus pour infraction aux lois sur les marques** |
| Global | 3 mai 2021-30 juin 2021 | 1 168 | 958 | 82,02% |

_REMARQUE : Depuis le 3 mai 2021, seules les notifications justifiées de retrait de contenus pour infraction aux lois sur les marques sont incluses dans les statistiques de retraits présentées ci-dessus. Les notifications justifiées de retrait de contenus pour infraction aux lois sur les marques désignent les notifications accompagnées de suffisamment d’information pour nous permettre d’évaluer si, en vertu des lois applicables, un contenu a porté atteinte à une marque. En réponse aux notifications incomplètes de retrait de contenus pour infraction aux lois sur les marques ou qui ne concernent pas des questions relatives aux marques ou que nous estimons frauduleuses, nous ne supprimerons pas le contenu visé, nous n’en désactiverons pas l’accès ni ne cesserons de le diffuser ou de le mettre à la disposition du public. Nous analysons scrupuleusement chaque notification et effectuons un suivi auprès du plaignant, s’il y a lieu._