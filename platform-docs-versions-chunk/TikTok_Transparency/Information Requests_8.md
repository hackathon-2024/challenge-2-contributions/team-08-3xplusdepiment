platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2022-2/


### **Données les plus récentes**

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_LIPGR_2022H2-2/2022H2_raw_data_law_French.csv)

#### **Demandes de renseignements sur les utilisateurs par les forces de l’ordre**

_REMARQUE : le taux de divulgation indique les cas où au moins certaines données ont été communiquées. Les cas de non-divulgation comprennent les situations où TikTok a demandé des informations supplémentaires ou des éclaircissements concernant la demande de communication des données et où les forces de l’ordre requérantes n’ont pas fourni les informations nécessaires conformément à nos [directives destinées aux forces de l’ordre](https://www.tiktok.com/legal/law-enforcement?lang=en) et/ou où TikTok a rejeté la demande en raison d’une procédure judiciaire incomplète ou invalide. Les demandes de conservation de données sont des demandes officielles faites par les forces de l’ordre pour conserver les données afin qu’elles aient suffisamment de temps pour lancer une procédure judiciaire valide de communication des données._

#### **Définitions**

* **Demandes légales :** demandes de communication des données des utilisateurs émanant des forces de l’ordre.
* **Demandes d’urgence :** demandes des forces de l’ordre faisant référence à une situation d’urgence et concernant la communication des données des utilisateurs.
* **Demandes de préservation des données :** demandes de préservation des données des utilisateurs émanant des forces de l’ordre.
* **Total des demandes :** Somme desdemandes légales, des demandes d’urgence et des demandes de préservation des données émanant des forces de l’ordre.
* **Pourcentage de demandes légales où certaines données ont été communiquées :** taux des demandes légales ayant abouti à une quelconque communication des données des utilisateurs.
* **Pourcentage de demandes d’urgence où certaines données ont été communiquées :** taux des demandes d’urgence ayant abouti à une quelconque communication des données des utilisateurs.
* **Compte concerné par la demande légale :** les identifiants de compte (par exemple, le numéro de téléphone, l’adresse e-mail, les URL des vidéos ou le nom d’utilisateur) envoyés par les forces de l’ordre dans le cadre d’une demande légale. Si les forces de l’ordre fournissent plusieurs identifiants de compte liés au même compte TikTok, cela est comptabilisé comme 1 compte concerné.
* **Compte concerné par la demande d’urgence :** les identifiants de compte (par exemple, le numéro de téléphone, l’adresse e-mail, les URL des vidéos ou le nom d’utilisateur) envoyés par les forces de l’ordre dans le cadre d’une demande d’urgence. Si les forces de l’ordre fournissent plusieurs identifiants de compte liés au même compte TikTok, cela est comptabilisé comme 1 compte concerné.
* **Compte concerné par la demande de préservation de données :** les identifiants de compte (par exemple, le numéro de téléphone, l’adresse e-mail, les URL des vidéos ou le nom d’utilisateur) envoyés par les forces de l’ordre dans le cadre d’une demande de préservation de données. Si les forces de l’ordre fournissent plusieurs identifiants de compte liés au même compte TikTok, cela est comptabilisé comme 1 compte concerné.