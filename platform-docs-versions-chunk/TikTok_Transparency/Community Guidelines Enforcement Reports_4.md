platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-3/


## **Sécurité**

Dans la continuité de l’engagement pris dans le dernier rapport, nous publions notre deuxième mise à jour trimestrielle sur la mise en œuvre de TikTok LIVE. TikTok LIVE continue d’être une partie essentielle de l’expérience TikTok, favorisant le divertissement en temps réel qui connecte les créateurs à leurs communautés. Malgré l’augmentation du nombre de diffusons en direct suspendues ce trimestre, la proportion du nombre total de diffusions en direct suspendues est restée stable à 1,5 %, ce qui indique que nos mesures de contrôle s’adaptent à la croissance de la plateforme.

#### **Suppressions de vidéos et suspensions LIVE**

#### **Total des suppressions et des rétablissements, par type de contenu**

#### **Taux de suppression et délais de réponse aux signalements**

#### **Politique en matière de suppression**

#### **Modération par marché et par langue**

#### **Opérations d’influence cachées**

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer depuis la Russie et cibler une audience européenne, dont l’Allemagne, l’Italie et l’Ukraine. Les individus derrière ce réseau ont créé un grand nombre de faux comptes pour amplifier artificiellement des opinions pro-russes sur la politique étrangère en Europe dans le contexte de la guerre entre la Russie et l’Ukraine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne/Externe | 12,820 | 847,760 |

Comptes ciblant le discours politique au Cambodge

Ce réseau semblait opérer depuis le Cambodge et cibler une audience cambodgienne. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu afin d’amplifier artificiellement des récits spécifiques favorables au Parti du peuple cambodgien, en ciblant le discours sur les élections au Cambodge.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 3,046 | 1,930,179 |

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer depuis l’Ukraine et cibler une audience ukrainienne. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, en ukrainien, amplifiant artificiellement des récits visant à collecter des fonds pour l’armée ukrainienne.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 2,350 | 81,570 |

Comptes ciblant le discours politique en Indonésie

Ce réseau semblait opérer depuis l’Indonésie et cibler une audience indonésienne. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, en amplifiant artificiellement les récits favorables à un candidat présidentiel spécifique, en ciblant le discours des élections indonésiennes.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 706 | 10,699 |

Comptes ciblant le discours politique en Chine

Ce réseau semblait opérer depuis la Chine et cibler une audience mondiale, dont les États-Unis et le Japon. Les individus derrière ce réseau ont créé de faux comptes et émis des publications sur de multiples plateformes en ligne afin d’amplifier artificiellement des récits favorables à la Chine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 636 | 29,757 |

Comptes ciblant le discours politique en Thaïlande

Ce réseau semblait opérer depuis la Thaïlande et cibler une audience thaïlandaise. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier artificiellement des récits spécifiques favorables à un candidat royaliste thaïlandais, en ciblant le discours sur les élections en Thaïlande.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 457 | 4,069 |

Comptes ciblant le discours politique en Thaïlande

Ce réseau semblait opérer depuis la Thaïlande et cibler une audience thaïlandaise. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier artificiellement des récits spécifiques favorables au parti politique « Change » thaïlandais, en ciblant le discours sur les élections en Thaïlande.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 377 | 741,225 |

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer depuis l’Ukraine et cibler une audience ukrainienne. Les individus derrière ce réseau ont créé de faux comptes afin d’amplifier des vidéos de désinformation, ciblant le discours sur la guerre en cours entre la Russie et l’Ukraine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 302 | 57,806 |

Comptes ciblant le discours politique en Iraq

Ce réseau semblait opérer depuis l’Iraq et cibler une audience iraquienne. Les individus derrière ce réseau ont créé de faux comptes afin de promouvoir secrètement les objectifs militaires iraniens au Moyen-Orient.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 229 | 242,409 |

Comptes ciblant le discours politique en Équateur

Ce réseau semblait opérer depuis l’Équateur et cibler une audience équatorienne. Les individus derrière ce réseau ont créé de faux comptes pour amplifier les récits critiques à l’égard d’un mouvement politique de gauche.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 143 | 12,353 |

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer à partir de la Russie et ciblait les audiences russe et ukrainienne, ainsi que l’audience russophone en Europe. Les individus derrière ce réseau ont créé de faux comptes afin d’amplifier artificiellement des discours pro-russes, ciblant le discours sur la guerre en cours entre la Russie et l’Ukraine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 133 | 199,569 |

Comptes ciblant le discours politique en Serbie

Ce réseau semblait opérer depuis la Serbie et cibler une audience serbe. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier les récits favorables au Parti progressiste serbe, en ciblant le discours des élections serbes.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 77  | 5,009 |

Comptes ciblant les relations étrangères de la Slovaquie avec la Russie

Ce réseau semblait opérer depuis la Slovaquie et cibler une audience slovaque. Les individus derrière ce réseau ont créé des comptes opaques qui utilisent de fausses méthodes de création d’audience afin d’amplifier de manière inauthentique des contenus anti-UE, anti-OTAN et pro-russes en Slovaquie.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 69  | 2,154 |

Comptes ciblant le discours politique en Malaisie

Ce réseau semblait opérer depuis la Malaisie et cibler une audience malaisienne. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier les récits critiques à l’égard des partis politiques Barisan Nasional, United Malays National Organization et Pakatan Harapan, et favorables au Perikatan Nasional, en ciblant le discours sur les élections en Malaisie.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 53  | 174,472 |

Comptes ciblant le discours politique en Serbie

Ce réseau semblait opérer depuis la Serbie et cibler une audience serbe. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier artificiellement les récits critiques à l’égard de l’opposition du Parti progressiste serbe et à l’égard des médias indépendants.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 38  | 37,361 |

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer depuis la Russie et cibler les utilisateurs d’Allemagne, d’Italie, de Turquie, de Serbie, de Tchéquie, de Pologne et de Grèce en utilisant les langues locales. Les individus derrière ce réseau ont créé de faux comptes, y compris de fausses nouvelles agences, afin d’amplifier artificiellement des récits pro-russes, ciblant le discours sur la guerre en cours entre la Russie et l’Ukraine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 19  | 217,008 |

REMARQUE : Le retrait d’une opération d’influence cachée nécessite des opérations à plusieurs niveaux, comprenant l’enquête, la suppression et l’analyse à l’issue de l’intervention. Nous indiquons l’intervention sur ces réseaux au cours du trimestre où le processus complet des opérations a été achevé.

**Définitions**

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

* * *