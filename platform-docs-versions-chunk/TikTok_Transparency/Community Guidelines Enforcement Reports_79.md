platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2019-2/


### Application de nos règles

A travers le monde, des dizaines de milliers de vidéos sont téléchargées sur TikTok chaque minute. Chacune de ces vidéos exige une responsabilité accrue de notre part pour protéger la sécurité et le bien-être de nos utilisateurs. Pour faire respecter nos Règles Communautaires, nous utilisons un ensemble de technologies et de systèmes de modération de contenu qui identifient et suppriment les contenus et les comptes qui les transgresseraient.

#### Technologie

Le volet technologique est un élément clé dans la mise en œuvre de nos politiques ; nos systèmes sont développés pour signaler automatiquement les contenus qui pourraient transgresser nos Règles Communautaires. Ces systèmes prennent en compte des modèles d’identification de formes ou de comportements afin de signaler les contenus susceptibles d’enfreindre nos Règles Communautaires. Ceci nous permet de prendre des mesures rapides et de limiter les préjudices éventuels. Par exemple, la grande majorité des contenus tels que les spams et les escroqueries sont automatiquement supprimés. Nous étudions régulièrement l’évolution des tendances, les résultats de travaux universitaires et les meilleures pratiques du secteur pour améliorer constamment nos systèmes.