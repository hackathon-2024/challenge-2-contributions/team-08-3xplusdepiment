platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2022-1/

### Analyse

Nous avons constaté une augmentation des demandes d’information par rapport à la période de rapport précédente, ce que nous attribuons à plusieurs facteurs : la croissance globale de la plateforme, les investissements réalisés pour familiariser les forces de l’ordre avec nos [directives destinées aux forces de l’ordre](https://www.tiktok.com/legal/law-enforcement) et avec nos processus, ainsi que la publication de ces directives en japonais, coréen, bahasa, thaï, vietnamien, urdu, bengali, arabe, turc, allemand, français, russe, italien, polonais, espagnol et portugais.