platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2022-1/

### **Analyse**

Un plus grand nombre de marchés ont fait des demandes de retrait par rapport à notre dernière période de rapport. L’assouplissement des restrictions COVID-19 sur certains marchés a entraîné une diminution des volumes de demandes, tandis que d’autres marchés en conflit ont vu leurs demandes de retrait augmenter.