platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2020-1/


### À propos de ce rapport

Des centaines de millions de personnes à travers le monde se rendent sur TikTok pour se divertir, s’exprimer et échanger. Notre priorité absolue est d’aider à leur assurer en environnement sûr, une expérience positive qui leur procurera le sentiment d’appartenir à une communauté internationale qui ne cesse de croître. Nous ne pouvons y parvenir sans faire preuve de transparence.

TikTok s’efforce d’être l’entreprise la plus transparente et la plus responsable de son secteur sur des sujets tels que la protection de nos utilisateurs. Nous avons mis nos actes en accord avec nos paroles en ouvrant nos [Centres sur la transparence et la responsabilité](https://www.tiktok.com/transparency), où nous levons le voile sur nos pratiques en matière de modération de contenus, d’algorithmes, de confidentialité et de sécurité. Nous publions des Rapports de transparence comme celui-ci afin de transmettre des informations sur la quantité et la nature des contenus supprimés pour avoir enfreint nos Règles Communautaires ou à nos Conditions de service. Nous y dressons aussi un bilan des actions engagées au premier trimestre 2020 en vue de renforcer la sécurité de notre plateforme et d’y promouvoir un environnement positive.