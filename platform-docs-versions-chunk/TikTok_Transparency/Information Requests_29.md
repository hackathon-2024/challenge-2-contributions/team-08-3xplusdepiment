platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2020-1/


### À propos de ce rapport

TikTok s’engage à se conformer aux demandes des autorités publiques dès lors qu’elles sont légalement recevables, tout en respectant la confidentialité et les droits de ses utilisateurs. Pour obtenir des informations non publiques sur un utilisateur, l’entité requérante est tenue de produire les justificatifs juridiques correspondant au type d’information recherché, comme une assignation à produire un document, une décision de justice ou une commission rogatoire, ou d’adresser une demande urgente. Toute demande réceptionnée est soigneusement examinée afin de déterminer si elle est justifiée d’un point de vue légal ; par exemple si l’entité requérante est autorisée à recueillir des éléments de preuve dans le cadre d’une enquête policière ou à enquêter sur une situation d’urgence présentant un danger imminent.

C’est pourquoi nous demandons aux autorités publiques de soumettre par écrit leur demande accompagnée des documents légaux appropriés. Nous ne prenons pas en compte des demandes qui ne sont effectuées par les voies appropriées. Pour en savoir plus sur nos politiques et pratiques, veuillez consulter nos [Directives relatives aux demandes de données des services de lutte contre la criminalité](https://www.tiktok.com/legal/law-enforcement).