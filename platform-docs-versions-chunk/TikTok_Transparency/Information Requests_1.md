platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2023-1/

Demandes d’information

_1 janvier 2023 – 30 juin 2023_

_Date de publication : 10 novembre 2023_