platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2022-2/


### **Données les plus récentes**

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_LIPGR_2022H2-2-IP/2022H2_raw_data_ip_French.csv)

#### **Demandes de retrait de contenus pour infraction aux droits d’auteur et aux lois sur les marques**

#### **Définitions**

* **Droit d’auteur** : un droit légal qui protège les œuvres originales de l’auteur (par exemple, la musique, les vidéos, etc.). En général, le droit d’auteur protège l’expression originale d’une idée (par exemple, la manière spécifique d’exprimer ou de créer une vidéo ou une musique), mais ne protège pas les idées ou les faits sous-jacents.
* **Marque** : un mot, un symbole, un slogan, un design ou une combinaison de ces éléments qui identifie la source d’un produit ou d’un service et le distingue des autres produits ou services.
* **Total des demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur** : nombre de demandes de retrait où des droits d’auteur ont été revendiqués.
* **Total des demandes de retrait de contenus pour infraction aux lois sur les marques** : nombre de demandes de retrait où des marques ont été revendiquées.
* **Demandes honorées de retrait de contenus pour infraction aux lois sur le droit d’auteur** : nombre de demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur qui ont abouti à la suppression du contenu signalé et/ou à la suspension du compte.
* **Demandes honorées de retrait de contenus pour infraction aux lois sur les marques** : nombre de demandes de retrait de contenus pour infraction aux lois sur les marques qui ont abouti à la suppression du contenu signalé et/ou à la suspension du compte.
* **Pourcentage des demandes honorées de retrait de contenus pour infraction aux lois sur le droit d’auteur** : taux des demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur qui ont abouti à la suppression du contenu signalé et/ou à la suspension du compte.
* **Pourcentage des demandes honorées de retrait de contenus pour infraction aux lois sur les marques** : taux des demandes de retrait de contenus pour infraction aux lois sur les marques qui ont abouti à la suppression du contenu signalé et/ou à la suspension du compte.