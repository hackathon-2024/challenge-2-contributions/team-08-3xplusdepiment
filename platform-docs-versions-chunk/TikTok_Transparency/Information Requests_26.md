platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2020-2/


### Rapport sur les données

#### Demandes légales d’information sur les utilisateurs

|     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- |
| **Pays / Marché** | **Total des demandes légales** | **Total des demandes d’urgence** | **Total des demandes** | **Total des comptes indiqués** | **Pourcentage de demandes légales pour lesquelles certaines données ont été produites** | **Pourcentage de demandes d’urgence pour lesquelles certaines données ont été produites** |
| Albanie | 1   | 0   | 1   | 5   | 0%  | 0%  |
| Argentine | 0   | 3   | 3   | 2   | 0%  | 33% |
| Australie | 4   | 8   | 12  | 20  | 25% | 75% |
| Autriche | 3   | 0   | 3   | 3   | 0%  | 0%  |
| Bangladesh | 4   | 0   | 4   | 3   | 0%  | 0%  |
| Belgique | 0   | 1   | 1   | 10  | 0%  | 100% |
| Brésil | 3   | 4   | 7   | 4   | 33% | 50% |
| Canada | 0   | 40  | 40  | 43  | 0%  | 92% |
| Chili | 3   | 0   | 3   | 3   | 0%  | 0%  |
| Colombie | 1   | 0   | 1   | 1   | 0%  | 0%  |
| République Tchèque | 1   | 0   | 1   | 1   | 0%  | 0%  |
| Danemark | 2   | 0   | 2   | 2   | 0%  | 0%  |
| Finlande | 2   | 4   | 6   | 6   | 0%  | 100% |
| France | 19  | 9   | 28  | 47  | 0%  | 89% |
| Allemagne | 45  | 25  | 70  | 103 | 0%  | 80% |
| Grèce | 4   | 6   | 10  | 9   | 0%  | 83% |
| Hong Kong | 1   | 0   | 1   | 0   | 0%  | 0%  |
| Hongrie | 0   | 1   | 1   | 1   | 0%  | 100% |
| Inde | 103 | 1   | 104 | 103 | 62% | 0%  |
| Irlande | 1   | 0   | 1   | 1   | 0%  | 0%  |
| Israël | 0   | 73  | 73  | 75  | 0%  | 81% |
| Italie | 11  | 5   | 16  | 13  | 0%  | 60% |
| Japon | 2   | 10  | 12  | 12  | 50% | 90% |
| Jordan | 2   | 1   | 3   | 3   | 0%  | 0%  |
| Liban | 0   | 1   | 1   | 1   | 0%  | 0%  |
| Luxembourg | 1   | 0   | 1   | 1   | 0%  | 0%  |
| Malte | 9   | 0   | 9   | 19  | 0%  | 0%  |
| Mexique | 2   | 4   | 6   | 7   | 0%  | 25% |
| Népal | 9   | 1   | 10  | 12  | 0%  | 100% |
| Pays-Bas | 0   | 5   | 5   | 4   | 0%  | 80% |
| Nouvelle-Zélande | 2   | 0   | 2   | 2   | 0%  | 0%  |
| Norvège | 1   | 4   | 5   | 5   | 0%  | 75% |
| Pakistan | 7   | 8   | 15  | 15  | 14% | 0%  |
| Paraguay | 0   | 1   | 1   | 1   | 0%  | 100% |
| Philippines | 0   | 2   | 2   | 2   | 0%  | 0%  |
| Pologne | 2   | 2   | 4   | 2   | 0%  | 50% |
| Qatar | 2   | 0   | 2   | 3   | 0%  | 0%  |
| Roumanie | 0   | 1   | 1   | 1   | 0%  | 100% |
| Russie | 12  | 2   | 14  | 14  | 67% | 100% |
| Serbie | 1   | 0   | 1   | 1   | 0%  | 0%  |
| Singapour | 21  | 0   | 21  | 29  | 72% | 0%  |
| Afrique du Sud | 0   | 1   | 1   | 1   | 0%  | 100% |
| Corée du Sud | 3   | 0   | 3   | 5   | 0%  | 0%  |
| Espagne | 11  | 0   | 11  | 9   | 9%  | 0%  |
| Sri Lanka | 2   | 0   | 2   | 2   | 0%  | 0%  |
| Suède | 2   | 2   | 4   | 4   | 0%  | 100% |
| Suisse | 6   | 1   | 7   | 7   | 0%  | 100% |
| Taïwan | 1   | 0   | 1   | 1   | 0%  | 0%  |
| Émirats Arabes Unis | 0   | 3   | 3   | 4   | 0%  | 67% |
| Turquie | 1   | 0   | 1   | 1   | 0%  | 0%  |
| Royaume-Uni | 49  | 42  | 91  | 93  | 12% | 76% |
| États Unis | 409 | 137 | 546 | 560 | 83% | 81% |
| Ouzbékistan | 0   | 1   | 1   | 1   | 0%  | 100% |