platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-2/

Demandes de retrait de contenu portant atteinte à la propriété intellectuelle

_1er juillet – 31 décembre 2021_  
_Date de publication : 17 mai 2022_