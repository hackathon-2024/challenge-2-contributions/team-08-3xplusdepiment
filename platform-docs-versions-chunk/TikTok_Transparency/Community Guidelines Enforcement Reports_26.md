platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-4/


### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des publicités sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Au cours du dernier trimestre de 2022, le volume total de contenus publicitaires retirés pour non-respect de nos politiques publicitaires et le nombre de contenus publicitaires supprimés en raison d’actions au niveau du compte ont diminué. Les progrès réalisés dans nos capacités de détection et de suppression proactive nous ont permis d’améliorer notre paysage publicitaire grâce à une suppression plus précise et plus rapide des contenus publicitaires contraires à nos politiques. Nous sommes conscients que, pour garantir la sécurité de nos annonceurs et de nos utilisateurs, nous devons faire des efforts constants. Nous nous engageons donc à régulièrement analyser et renforcer nos systèmes de lutte contre les contenus publicitaires ne respectant pas nos politiques, ainsi qu’à créer un environnement où les utilisateurs associent les marques qu’ils voient à de la qualité et de la positivité.