platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2023-1/

Demandes de retrait de contenu portant atteinte à la propriété intellectuelle

_1 janvier 2023 – 30 juin 2023_

_Date de publication : 10 novembre 2023_