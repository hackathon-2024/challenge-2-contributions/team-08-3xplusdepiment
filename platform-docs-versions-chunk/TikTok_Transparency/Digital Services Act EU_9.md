platform: TikTok
topic: Transparency
subtopic: Digital Services Act EU
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Digital Services Act EU.md
url: https://www.tiktok.com/transparency/en/tco-report/

Terrorist Content Online Regulation

_7 June, 2022 – 31 December, 2022_  
_Published February, 2023_