platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-1/

### Sécurité

Dans ce rapport, nous vous présentons nos nouvelles données avec la volonté de transparence que nous continuons à appliquer dans la manière dont nous gérons et protégeons la plateforme. Tout d’abord, nous vous donnons un aperçu des compétences linguistiques des modérateurs de TikTok, dont le travail contribue non seulement à rendre la plateforme plus sûre, mais joue également un rôle essentiel dans l’amélioration de nos systèmes de modération automatisés. Ensuite, nous vous fournirons des données concernant les délais de traitement de suppression des contenus violents signalés par notre communauté à l’aide de nos outils de signalement en ligne et dans l’application.

Au cours de la période couverte par le présent rapport, nous avons mis à jour les [Règles communautaires](https://newsroom.tiktok.com/en-us/community-guidelines-update) de TikTok. Ces règles sont entrées en vigueur en avril 2023 et seront prises en compte dans notre prochain rapport.