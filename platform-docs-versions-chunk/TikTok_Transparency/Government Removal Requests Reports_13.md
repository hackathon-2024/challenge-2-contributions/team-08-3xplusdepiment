platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2022-1/


### **Données les plus récentes**

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_LIPGR_2022H2/French_LIPGR_2022_H1.xlsx)

#### **Demandes de retrait ou de restriction de contenu ou de comptes, émanant de gouvernements**

_REMARQUE : en mars 2022, TikTok a suspendu le livestreaming et la création de nouvelles vidéos en Russie. Par conséquent, la grande majorité des demandes de retrait du gouvernement russe que nous avons reçues au cours de cette période de rapport concernaient des contenus et des comptes créés avant la guerre. TikTok n’a pas reçu de demande de retrait ou de restriction de contenu ou de compte émanant de gouvernements de pays ou de marchés autres que ceux indiqués ci-dessus._

#### **Définitions**

* **Total des demandes reçues :** Les demandes de retrait ou de restriction de contenu ou de comptes émanant de gouvernements, y compris les demandes accompagnées d’adresses URL inexactes.
* **Total du contenu reçu :** Adresses URL de contenus valides demandées, à l’exclusion de toute forme acceptable de compte (c’est-à-dire URL, identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Contenu sanctionné pour infraction aux Règles communautaires :** Adresses URL de contenus valides vérifiées et sanctionnées pour violation de nos Règles communautaires.
* **Contenu sanctionné pour infraction aux lois (locales) :** Toutes les adresses URL de contenus valides vérifiées et sanctionnées pour violation d’une loi locale.
* **Contenu non sanctionné :** Adresses URL de contenus valides vérifiées et jugées comme n’enfreignant pas les Règles communautaires et les Conditions de services de TikTok ou les lois locales.
* **Total des comptes reçus :** Adresses URL de comptes valides et toute autre forme acceptable d’un compte (c’est-à-dire identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Comptes sanctionnés pour infraction aux Règles communautaires :** Adresses URL de comptes valides examinées et sanctionnées pour infraction aux Règles communautaires.
* **Comptes sanctionnés pour infraction aux lois (locales) :** Adresses URL de comptes valides examinées et sanctionnées pour infraction à une loi locale.
* **Comptes non sanctionnés :** Adresses URL de comptes valides examinées et jugées comme n’enfreignant pas les Règles communautaires de TikTok ou les lois locales.
* **Taux de retrait :** Taux de contenus ou de comptes que TikTok a supprimés ou restreint en réponse à des demandes émanant de gouvernements.