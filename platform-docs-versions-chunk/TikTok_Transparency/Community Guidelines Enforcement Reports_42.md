platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-1/


### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhaeh7pflk/Transparency_CGE_2022_Q1/French_CGE_2022Q1.xlsx)

#### Nombre total de vidéos supprimés / total de vidéos par trimestre

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok._

#### Nombre total de vidéos supprimées/remises en ligne, par type et trimestre

_REMARQUE : les vidéos rétablies sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne dans le tableau ci-dessus._

#### Nombre total de suppressions de vidéos, par règle et politique

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas rares, comme des situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### Nombre total de suppressions et taux de vidéos, par sous-règle et politique

_REMARQUE : Seules les vidéos qui ont été vérifiées par des modérateurs sont incluses dans le tableau de bord des différentes règles et dispositions. Nos règles de sécurité concernant les mineurs ont pour but de fournir un niveau supérieur de sécurité et de bien-être aux adolescents. La disposition « nudité et activité sexuelle impliquant des mineurs » interdit un grand nombre de contenus, notamment « les mineurs peu vêtus » et « les danses sexuellement explicites » ; ces deux catégories représentent la majorité des contenus supprimés conformément à cette disposition. Les contenus pédopornographiques (CSAM) sont signalés séparément._

#### Taux de suppression, par trimestre/règle

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### Volume et taux de suppression, par pays

_REMARQUE : ce graphique dresse la liste des trente pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 80 % du volume total des suppressions._

#### Nombre total de comptes supprimés, par trimestre et par motif

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### Fausse activité

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « suiveurs » et les demandes de « suivi » si nous estimons que l’activité est automatisée ou frauduleuse._

#### Activité des comptes spam

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

#### Application des règles concernant les contenus publicitaires

_REMARQUE : en 2022, le volume total d’annonces supprimées a augmenté en raison de l’évolution de notre approche en matière d’application des règles relatives aux publicités et au renforcement de nos capacités de contrôle au niveau des comptes. Le retrait peut se faire de deux façons : l’annonce particulière est supprimée, ou la suppression se fait en bloc par une action appliquée à l’intégralité du compte de l’annonceur._