platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2019-2/


### Rapport sur les données

#### Vidéos

Sur la seconde partie de l’année (du 1e juillet au 31 décembre 2019), nous avons supprimé 49.247.689 de vidéos au global ce qui représente moins de 1% de l’ensemble des vidéos créées par nos utilisateurs. Ces vidéos ont été supprimées pour avoir avoir avoir enfreint nos Règles Communautaires ou nos conditions de services. Nos systèmes ont proactivement identifié et supprimé 98.2% de ces vidéos avant qu’un utilisateur ne les signale. Sur l’ensemble des vidéos, supprimées, 89.4% l’ont été avant qu’elles aient été vues.

Le tableau ci-après présente les 5 marchés avec le plus grand nombre de vidéos supprimées.

|     |     |
| --- | --- |
| **Pays** | **Total des retraits** |
| Inde | 16,453,360 |
| États-Unis | 4,576,888 |
| Pakistan | 3,728,162 |
| Royaume Uni | 2,022,728 |
| Russie | 1,258,853 |

Fin 2019, nous avons mis place un nouveau système de modération de contenu qui nous permet d’offrir plus de transparence lors de l’indication des raisons pour lesquelles un contenu a été retiré. Lorsqu’une vidéo va à l’encontre de nos Règles Communautaires, elle est identifiée en fonction de la règle ou des différentes règles qu’elle enfreint et est ensuite supprimée. Ainsi, une même vidéo peut apparaitre sous de multiples catégories. Pour le mois de décembre 2019, lorsque nous avons mis en place notre nouveau système de modération de contenu, la répartition des règles enfreintes s’appuie sur ces nouvelles catégories.

Au cours du mois de décembre, 25,5% des vidéos retirées l’ont été sous la catégorie ‘Nudité Adulte et activités sexuelles’. Pour préserver la sécurité des enfants, 24,8% des vidéos ont été supprimées car elles enfreignaient nos règles relatives à la protection des mineurs. Ceci couvre des contenus tels que des comportements à risque, dangereux ou des comportements illégaux pour des mineurs comme la consommation d’alcool ou de drogue ou bien encore plus graves et pour lesquels nous avons décidé un retrait immédiat et la fermeture des comptes incriminés. Ces derniers ont été signalés auprès de la NCMEC et des autorités compétentes lorsque nécessaire. Les contenus liés aux activités illégales et marchandises réglementées représentent 21,5% des suppressions. En outre, 15,6% des vidéos retirées l’ont été pour avoir enfreint nos règles relatives au suicide, à l’automutilation ou pour comportement dangereux qui reflète principalement la suppression de challenges à risque. Enfin, 8,6% des vidéos supprimées ont enfreint nos règles liées aux contenus violents et visuellement explicites ; 3% pour harcèlement ou intimidation ; et moins de 1% pour atteinte à nos règles relatives aux discours haineux, à l’intégrité et à l’authenticité ou relatives aux personnes ou organisations dangereuses.

Depuis, la majorité de nos contenus ont été traités par notre nouveau système de modération. Nos rapports ultérieurs comprendront des données plus détaillées, tant pour le contenu que les comptes et porteront sur l’ensemble de la période concernée.