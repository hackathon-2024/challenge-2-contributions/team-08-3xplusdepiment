platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-1/


### Sûreté

Nous restons vigilants dans nos efforts pour protéger la plateforme contre les menaces, y compris la présence de comptes et d’engagements inauthentiques ou faux. Ces menaces attaquent nos systèmes et les mettent à l’épreuve de manière continue, ce qui peut entraîner des fluctuations ponctuelles dans les indicateurs rapportés dans ces domaines. Néanmoins, nous restons déterminés à identifier et à supprimer rapidement tout compte, contenu ou activité visant à accroître artificiellement la popularité sur notre plateforme.

La protection de notre communauté est notre priorité. Bien que TikTok soit une plateforme mondiale, nous adoptons une approche locale de la conformité réglementaire en collaborant avec les parties prenantes afin de mieux comprendre les préoccupations locales et respecter nos obligations légales. Aux États-Unis, nous avons créé une division spécialisée appelée [TikTok U.S. Data Security (USDS)](https://newsroom.tiktok.com/en-us/our-approach-to-keeping-us-data-secure) pour renforcer nos politiques et protocoles de protection des données, protéger davantage nos utilisateurs et renforcer la confiance dans nos systèmes et contrôles aux États-Unis. En outre, nous avons lancé le [projet Clover](https://newsroom.tiktok.com/en-eu/setting-a-new-standard-in-european-data-security-with-project-clover), une initiative visant à créer un environnement protégé spécialement conçu pour les données des utilisateurs européens de TikTok. À travers le projet Clover, nous concevons et mettons en œuvre une série de nouvelles mesures visant à renforcer les protections existantes, à en ajouter de nouvelles et à aligner notre approche globale de la gouvernance des données sur le principe de la souveraineté européenne en matière de données.

Dans ce contexte de menaces en constante évolution, TikTok s’engage à préserver l’intégrité de sa communauté et à renforcer ses défenses contre les menaces extérieures. Nous nous engageons à préserver la confiance que nous accordent nos utilisateurs et nous continuerons à investir dans des mécanismes solides qui donnent la priorité à la sécurité des utilisateurs, à la protection des données et au respect de la réglementation.