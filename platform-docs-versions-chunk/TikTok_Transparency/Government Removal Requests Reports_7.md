platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2022-2/

### **Analyse**

Au cours du second semestre 2022, nous avons reçu moins de demandes de suppression ou de restriction de contenus ou de comptes que lors de notre dernier rapport. Nous pensons que cela est dû en partie à la suspension de la diffusion en direct et des publications de nouveaux contenus sur notre service vidéo en Russie, qui a entraîné une baisse globale du nombre total de demandes reçues.