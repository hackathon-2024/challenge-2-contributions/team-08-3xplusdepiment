platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2020-2/


### Rapport sur les données

#### Avis de retrait de contenu protégé par les droits d’auteur

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Plage de dates** | **Total des rapports de demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur** | **Total des de demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur** | **Pourcentage des demandes honorées de retrait de contenus pour infraction aux lois sur le droit d’auteur** |
| États-Unis et Canada | July 1 – July 12, 2020 | 107 | 88  | 82.24% |
| Reste du monde | July 1 – Aug 12, 2020 | 6131 | 1574 | 25.67% |

_Remarque : seuls les avis de retrait pour violation des droits d’auteur émanant des détenteurs de droits d’auteur, de leurs agences ou de leurs avocats sont inclus dans les statistiques de retrait ci-dessus._

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Plage de dates** | **Total des rapports de demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur** | **Total des de demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur** | **Pourcentage des demandes honorées de retrait de contenus pour infraction aux lois sur le droit d’auteur** |
| États-Unis et Canada | July 13 – Dec 31, 2020 | 9290 | 1830 | 19.70% |
| Reste du monde | Aug 13 – Dec 31, 2020 | 25394 | 11626 | 45.78% |

_Remarque : en raison des limitations de notre outil statistique, tous les avis de retrait pour violation des droits d’auteur sont inclus dans les statistiques des avis de retrait._

#### Avis de retrait de contenu protégé par le droit des marques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Plage de dates** | **Total des rapports de demandes de retrait de contenus pour infraction aux lois sur les marques** | **Demandes honorées de retrait de contenus pour infraction aux lois sur les marques** | **Pourcentage des demandes honorées de retrait de contenus pour infraction aux lois sur les marques** |
| États-Unis et Canada | July 1 – July 12, 2020 | 37  | 9   | 24.30% |
| Reste du monde | July 1 – Aug 12, 2020 | 438 | 72  | 16.44% |

_Remarque : seuls les avis de retrait pour violation du droit des marques émanant des propriétaires de marques, de leurs agences ou de leurs avocats sont inclus dans les statistiques de retrait ci-dessus._

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Plage de dates** | **Total des rapports de demandes de retrait de contenus pour infraction aux lois sur les marques** | **Demandes honorées de retrait de contenus pour infraction aux lois sur les marques** | **Pourcentage des demandes honorées de retrait de contenus pour infraction aux lois sur les marques** |
| États-Unis et Canada | July 13 – Dec 31, 2020 | 1002 | 253 | 25.20% |
| Reste du monde | Aug 13 – Dec 31, 2020 | 1026 | 122 | 11.89% |

_Remarque : en raison des limitations de notre outil statistique, tous les avis de retrait pour violation du droit des marques sont inclus dans les statistiques des avis de retrait._