platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2020-2/


### Rapport sur les données

#### Demandes de retrait de contenu émanant d’autorités publiques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Demandes de gouvernements** | **Total de comptes spécifiés** | **Comptes supprimés ou restreints** | **Contenus supprimés ou restreints** |
| Arménie | 4   | 20  | 0   | 8   |
| Australie | 32  | 98  | 74  | 16  |
| Brésil | 1   | 1   | 1   | 1   |
| Bangladesh | 1   | 1   | 1   | 0   |
| Belgique | 1   | 1   | 1   | 0   |
| Canada | 4   | 4   | 4   | 0   |
| Chypre | 1   | 1   | 0   | 0   |
| Egypte | 1   | 1   | 1   | 1   |
| Estonie | 1   | 6   | 6   | 0   |
| Finlande | 1   | 1   | 1   | 0   |
| France | 9   | 20  | 6   | 23  |
| Allemagne | 1   | 1   | 1   | 0   |
| Indonésie | 2   | 3   | 0   | 26  |
| Israël | 15  | 15  | 10  | 10  |
| Islande | 1   | 1   | 1   | 1   |
| Japon | 2   | 0   | 0   | 8   |
| Malaisie | 2   | 2   | 2   | 0   |
| Nouvelle Zélande | 4   | 4   | 4   | 0   |
| Norvège | 27  | 61  | 56  | 10  |
| Népal | 15  | 15  | 11  | 14  |
| Pakistan | 97  | 50  | 24  | 14263 |
| Russie | 135 | 375 | 94  | 429 |
| Sri Lanka | 10  | 11  | 6   | 4   |
| Suède | 2   | 2   | 1   | 1   |
| Thailande | 10  | 1   | 0   | 24  |
| Turquie | 13  | 14  | 6   | 16  |
| Taiwan | 1   | 39  | 21  | 0   |
| Vietnam | 2   | 2   | 1   | 0   |
| Emirats Arabes Unis | 2   | 4   | 1   | 25  |
| Royaume-Uni | 4   | 4   | 4   | 0   |
| Etats-Unis | 5   | 5   | 4   | 0   |
| Uzbekistan | 6   | 98  | 34  | 82  |