platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2020-2/


### À propos de ce rapport

TikTok repose sur la créativité de ses utilisateurs. Notre plateforme encourage pleinement leur expression personnelle, et nous faisons tout pour la protéger. Nos Règles Communautaires et nos Conditions de services interdisent tout contenu qui enfreigne les règles de propriété intellectuelle de tiers. Nous traitons les demandes de retrait justifiées, sur la base du non-respect de la législation sur les droits d’auteur et sur les marques. Dès réception d’une requête, en bonne et due forme, émanant d’un détenteur de droits portant sur une éventuelle atteinte à la propriété intellectuelle, TikTok retirera le contenu présumé en infraction dans les meilleurs délais.Toute activité qui enfreint les droits de propriété intellectuelle d’autrui peut entraîner la suspension ou la suppression du compte. Pour plus d’informations sur la manière dont nous évaluons les allégations de violation de la propriété intellectuelle, veuillez consulter notre [Politique sur la propriété intellectuelle](https://www.tiktok.com/legal/copyright-policy).