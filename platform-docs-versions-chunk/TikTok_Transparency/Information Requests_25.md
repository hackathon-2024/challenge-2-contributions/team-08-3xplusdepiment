platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2020-2/


### À propos de ce rapport

TikTok s’engage à se conformer aux demandes légitimes des autorités juridiques, tout en respectant la vie privée et les droits de ses utilisateurs. Pour obtenir des informations non publiques sur les données personnelles des utilisateurs, les autorités juridiques doivent fournir les documents légaux requis pour le type d’informations recherchées, tels qu’une citation à comparaître, une ordonnance du tribunal ou un mandat, ou soumettre une demande d’urgence. Toute demande d’information que nous recevons est soigneusement examinée afin de déterminer si elle est justifiée d’un point de vue légal, par exemple si l’entité requérante est autorisée à recueillir des éléments de preuve dans le cadre d’une enquête policière ou à enquêter sur une situation d’urgence présentant un danger imminent.

Dans des situations d’urgence exceptionnelles, TikTok divulguera les données personnelles des utilisateurs sans procédure légale. Cela peut se produire lorsque, en toute bonne foi, nous avons des raisons d’estimer que le partage d’informations est nécessaire face à un risque imminent de mort ou de préjudice physique grave. Pour plus d’informations sur nos règles et procédures, veuillez consulter nos [Directives relatives aux demandes de données des services de lutte contre la criminalité](https://www.tiktok.com/legal/law-enforcement?lang=fr).