platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-3/


### À propos de ce rapport

Chez TikTok, notre mission est d’inspirer la créativité et de divertir. Nous donnons la priorité à la sécurité, au bien-être et à l’intégrité afin que notre communauté puisse se sentir libre de créer, de se connecter les uns les autres et de se divertir.

Plus de 40 000 professionnels en charge de la sécurité, travaillent avec des technologies innovantes afin de maintenir et faire respecter nos [Règles Communautaires](https://www.tiktok.com/community-guidelines/en/), [Conditions Générales](https://www.tiktok.com/legal/page/eea/terms-of-service/en) et [Politiques Publicitaires](https://ads.tiktok.com/help/article/tiktok-advertising-policies-ad-creatives-landing-page-prohibited-content?lang=en), qui s’appliquent à tous les contenus de notre plateforme. Ce dernier rapport donne un aperçu de ces efforts et montre comment nous continuons à maintenir la confiance, l’authenticité et la responsabilité.

Dans le cadre de nos efforts continus pour faciliter l’analyse de la plateforme TikTok, nous fournissons désormais des données supplémentaires concernant la suppression de données par catégorie de politique pour les 50 marchés ayant les volumes les plus élevés de contenu supprimé (cf. Fichier en téléchargement ci-dessous). Ces marchés représentent environ 90 % de toutes les suppressions de contenu pour ce trimestre.

Au cours de la période considérée, nous avons élargi l’éligibilité à notre [API de recherche](https://developers.tiktok.com/products/research-api/) aux chercheurs universitaires en Europe, afin de soutenir la recherche sur des sujets liés aux tendances de consommation, à la désinformation, à la santé mentale, etc. Nous avons également lancé une nouvelle [API de contenu commercial](https://developers.tiktok.com/products/commercial-content-api) et ouvert notre [Bibliothèque de contenu commercial](https://library.tiktok.com/) en Europe pour apporter une plus grande transparence à la publicité payante et aux autres contenus commerciaux sur TikTok.