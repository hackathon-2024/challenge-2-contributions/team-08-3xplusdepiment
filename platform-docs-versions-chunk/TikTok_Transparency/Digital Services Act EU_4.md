platform: TikTok
topic: Transparency
subtopic: Digital Services Act EU
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Digital Services Act EU.md
url: https://www.tiktok.com/transparency/en/copd-eu/


### Reports

As part of our commitments to the Code, we will publish a transparency report every six months to provide granular data for EU/EEA countries about our efforts to combat online misinformation. These reports are available [here](https://www.tiktok.com/transparency/en/reports/), as well as on a separate [transparency centre](https://disinfocode.eu/), alongside reports from other platforms.

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Download](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhsllrta/COPD%20zip%20test/TikTok%20COPD%20reports.zip)

#### Was this helpful?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Yes![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)No