platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2020-1/

Demandes de retrait émanant des autorités publiques

_Du 1e janvier au 30 juin 2020_  
_Publié le 22 Septembre 2020_