platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2019-2/

### À propos de ce rapport

TikTok repose sur la créativité de ses utilisateurs. La plateforme leur permet de s’exprimer en toute liberté et nous faisons tout pour la protéger. Nos Règles Communautaires et Conditions de services interdisent les contenus qui enfreignent les règles de propriété intellectuelle.

Nous traitons les demandes de retrait justifiées, sur la base du non-respect de la législation sur les droits d’auteur, notamment le Digital Millennium Copyright Act (DMCA). Dès réception d’une requête émanant d’un détenteur de droits portant sur une éventuelle atteinte à la propriété intellectuelle, TikTok retirera le contenu en infraction dans les meilleurs délais.

Toute activité qui enfreint les droits d’auteur de tiers peut entraîner la suspension ou la suppression du compte. Pour plus d’informations sur la manière dont nous évaluons les plaintes pour violation des droits d’auteur, veuillez consulter notre [Politique sur la propriété intellectuelle](https://www.tiktok.com/legal/copyright-policy).