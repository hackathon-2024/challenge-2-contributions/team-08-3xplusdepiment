platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-2/

### Sûreté

Nous continuons à développer et à adapter nos mesures de protection en investissant dans des systèmes automatisés pour détecter, bloquer et supprimer les comptes et les interactions artificielles, et en améliorant notre temps de réaction et notre réponse à l’évolution des menaces. Au deuxième trimestre 2022, les attaques contre nos systèmes ont entraîné une augmentation du volume total de faux abonnés supprimés. Nous avons mis en place des mesures de protection sur nos systèmes pour empêcher les acteurs malveillants de comprendre nos capacités de détection. Cela s’est traduit par une diminution des comptes de spam bloqués à l’inscription et une augmentation des faux comptes supprimés.