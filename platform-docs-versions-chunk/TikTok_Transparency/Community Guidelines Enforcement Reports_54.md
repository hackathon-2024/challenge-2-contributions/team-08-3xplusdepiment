platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-2/


### Rapport sur les données

#### Vidéos

D’avril à juin 2021, 81 518 334 vidéos ont été supprimées dans le monde pour avoir enfreint nos Règles Communautaires ou nos conditions d’utilisation, soit moins de 1 % de toutes les vidéos mises en ligne. Parmi ces vidéos, nous avons identifié et supprimé 93,0 % d’entre elles dans les 24 heures suivant leur publication et 94,1 % avant qu’un utilisateur ne les signale. 87,5 % des contenus supprimés n’ont pas été vus, ce qui représente une amélioration par rapport au premier trimestre de cette année (81,8 %), et ce grâce au renforcement des systèmes de détection. Nous avons également continué à déployer sur d’autres marchés une technologie qui détecte et supprime automatiquement certaines catégories de contenu non conforme. Ainsi, 16 957 950 des suppressions ont été traitées par cette technologie.

Ce graphique montre les cinq marchés ayant les plus gros volumes de vidéos supprimées.

|     |     |
| --- | --- |
| **Pays** | **Total des retraits** |
| États-Unis | 11,431,198 |
| Pakistan | 9,851,404 |
| Brésil | 7,488,608 |
| Russie | 4,759,623 |
| Indonésie | 4,743,066 |

TikTok offre aux utilisateurs la possibilité de faire appel lors de la suppression de leur vidéo et s’engage à rétablir le contenu qui aurait été supprimé par erreur. D’avril à juin, 4 663 387 vidéos ont été rétablies après avoir fait l’objet d’un appel. Nous nous efforçons d’être cohérents et équitables dans nos pratiques de modération et de réduire les faux positifs grâce à des formations continues sur les politiques et à des contrôles de qualité.

Le graphique suivant montre le volume de vidéos supprimées pour avoir enfreint des règles. Veuillez noter qu’une vidéo peut enfreindre plusieurs règles et que chaque violation est indiquée.

Le graphique suivant montre le taux de suppression des vidéos pour avoir enfreint nos politiques. La suppression proactive signifie qu’une vidéo en infraction est identifiée et supprimée avant qu’elle ne nous soit signalée. La suppression dans les 24 heures signifie que la vidéo est supprimée dans les 24 heures suivant sa publication sur notre plateforme. La suppression à zéro vue signifie que la vidéo est supprimée après avoir été publiée, mais avant que quiconque ne l’ait vue.

|     |     |     |     |
| --- | --- | --- | --- |
|     | **Taux de suppression proactive** | **Taux de suppression dans les 24 heures** | **Taux de suppression à zéro vue** |
| Nudité adulte et activités sexuelles | 90.30% | 90.00% | 78.50% |
| Harcèlement et intimidation | 73.30% | 83.80% | 61.40% |
| Comportement haineux | 72.90% | 80.80% | 60.60% |
| Activités illégales et biens réglementés | 97.10% | 95.70% | 92.30% |
| Intégrité et authenticité | 88.30% | 86.20% | 67.90% |
| Sécurité des mineurs | 97.60% | 95.40% | 93.90% |
| Suicide, automutilation et actes dangereux | 94.20% | 90.80% | 81.80% |
| Contenu violent et choquant | 94.90% | 94.30% | 86.60% |
| Extrémisme violent | 89.40% | 90.10% | 79.50% |

[Nudité adulte et activités sexuelles](https://www.tiktok.com/community-guidelines?lang=fr#30)  
La nudité, le contenu sexuellement explicite et la pornographie ne sont pas autorisés sur TikTok. Sur l’ensemble des vidéos supprimées, 14 % étaient en infraction de cette règle. 90 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 78,5 % ont été supprimées à zéro vues, et 90,3 % ont été supprimées avant tout signalement.

[Harcèlement et intimidation](https://www.tiktok.com/community-guidelines?lang=fr#36)  
Nous souhaitons favoriser une communauté ouverte à l’expression créative et ne tolérons pas que les membres de notre communauté soient humiliés, intimidés ou harcelés. Sur l’ensemble des vidéos supprimées, 6,8 % violaient cette politique. 83,8 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 61,4 % ont été supprimées à zéro vues et 73,3 % ont été supprimées avant tout signalement.

[Comportement haineux](https://www.tiktok.com/community-guidelines?lang=fr#38)  
TikTok est une communauté diverse et inclusive. Les comportements haineux, y compris les discours de haine et les idéologies haineuses n’ont pas leur place sur notre plateforme. 2,2 % des vidéos supprimées ont violé cette politique. 80,8 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 60,6 % ont été supprimées à zéro vues, et 72,9 % ont été supprimées avant tout signalement. Il s’agit d’un domaine très complexe et nuancé dans lequel nous devons continuer à nous améliorer. Nous nous engageons à renforcer nos mécanismes de détection des discours haineux tout en améliorant notre capacité à modérer correctement les contre-discours et les réappropriations.

[Activités illégales et biens réglementés](https://www.tiktok.com/community-guidelines?lang=fr#32)  
Nous veillons à ce que TikTok ne facilite pas les activités criminelles ou la promotion ou le commerce de drogues, de tabac, d’alcool et d’autres substances contrôlées ou biens réglementés. Au cours du deuxième trimestre de cette année, 20,9% des vidéos supprimées étaient non conforme à cette règle. 95,7 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 92,3 % ont été supprimées à zéro vues et 97,1 % ont été supprimées avant tout signalement.

[Intégrité et authenticité](https://www.tiktok.com/community-guidelines?lang=fr#37)  
Nous estimons que la bonne foi constitue le socle de notre communauté, et de ce fait nous n’autorisons point les contenus ou les comptes qui impliquent un faux engagement, une usurpation d’identité voire une désinformation nuisible ou dangereuse. Parmi les vidéos supprimées, 0,8 % ont enfreint cette règle. 86,2 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication et 67,9 % ont été supprimées à zéro vues. 88,3 % ont été supprimées avant tout signalement.

[Sécurité des mineurs](https://www.tiktok.com/community-guidelines?lang=fr#31)  
Nous sommes profondément attachés à la sécurité des mineurs et renforçons régulièrement nos processus afin d’assurer leur sécurité. Au cours du deuxième trimestre de 2021, 41,3 % des contenus retirés enfreignaient notre politique de sécurité des mineurs. 95,4 % de ces vidéos ont été supprimées dans les 24 heures suivant leur mise en ligne, 93,9 % ont été supprimées à zéro vues et 97,6 % ont été supprimées avant tout signalement.

[Suicide, automutilation et actes dangereux](https://www.tiktok.com/community-guidelines?lang=fr#34)  
Nous tenons le bien-être des individus qui font partie de notre communauté à cœur et nous nous efforçons de créer un environnement de bienveillant. Parmi les vidéos supprimées d’avril à juin, 5,3 % étaient en infraction de cette règle. 90,8 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 81,8 % ont été supprimées à zéro vues. 94,2 % ont été supprimées avant tout signalement.

[Contenu violent et choquant](https://www.tiktok.com/community-guidelines?lang=fr#35)  
TikTok est une plateforme qui célèbre la créativité, mais pas les contenus choquants ou violents. Sur l’ensemble des vidéos supprimées, 7,7 % étaient en non-conformité avec cette règle. 94,3 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 86,6 % ont été supprimées à zéro vues, et 94,9 % ont été supprimées avant tout signalement.

[Extrémisme violent](https://www.tiktok.com/community-guidelines?lang=fr#39)  
TikTok interdit strictement l’extrémisme violent et nous nous efforçons de faire le nécessaire pour empêcher que notre plateforme soit utilisée pour menacer quiconque ou inciter à la violence. Sur l’ensemble des vidéos supprimées, 1,1% ont enfreint cette règle. 90,1 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication et 79,5 % ont été supprimées à zéro vues. 89,4 % ont été supprimées avant tout signalement.

#### Comptes

Au cours du deuxième trimestre de 2021, 14 871 412 comptes ont été supprimés pour non-respect de nos règles communautaires ou nos conditions d’utilisation. Ce chiffre inclut 11 205 597 comptes soupçonnés d’appartenir à des mineurs et qui ont été supprimés pour avoir potentiellement appartenu à une personne de moins de 13 ans, ce qui représente moins de 1 % de tous les comptes sur TikTok.

#### Fausse activité

Nous continuons à faire évoluer et à adapter nos mesures de protection en investissant dans des défenses automatisées pour détecter, bloquer et supprimer les comptes et les engagements inauthentiques, et en améliorant notre vitesse et notre réactivité par rapport aux menaces qui ne cessent d’évoluer. D’avril à juin, nous avons empêché la création de 148 759 987 faux comptes. Nous avons également supprimé 8 542 037 vidéos publiées par des comptes spam.

Nous avons également identifié et empêché 632 416 873 tentatives de demandes de suivi par de faux comptes et supprimé 71 935 583 comptes qui suivaient d’autres utilisateurs. Nous avons également empêché 9 612 942 242 tentatives de “likes” et corrigé 91 812 066 “likes” inauthentiques par ces comptes.

#### Contenus publicitaires

TikTok a mis en place des [politiques strictes](https://ads.tiktok.com/help/article?aid=10000962) pour protéger les utilisateurs contre les contenus publicitaires mensongers, frauduleux ou trompeurs. Les comptes d’annonceurs et les publicités payantes sont soumis à ces règles et doivent se conformer à nos Règles Communautaires, nos [Règles Publicitaires](https://ads.tiktok.com/help/article?aid=10000962) ainsi que nos conditions d’utilisation. Au cours du second trimestre 2021, nous avons rejeté 1 829 219 de contenus publicitaires pour infraction à nos politiques et règles en matière de publicité. Nous nous engageons à ce que nos utilisateurs aient une expérience positive, authentique et agréable lorsqu’ils regardent des publicités sur TikTok, à cet égard nous continuerons à investir non seulement dans nos ressources humaines mais également dans les moyens technologiques afin d’assurer un contenu publicitaire qui soit conforme à nos attentes.