platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2019-2/


### Autres contrôles de contenus

Les créateurs de contenu doivent avoir le contrôle sur la façon dont les autres utilisateurs peuvent interagir avec leurs vidéos. TikTok propose aux utilisateurs de nombreuses options pour paramétrer leurs comptes et vidéos comme par exemple la possibilité de limiter les commentaires ou de décider de qui peut réaliser des duos. Ils peuvent également activer des filtres de commentaires en créant une liste personnalisée de mots clés qui seront automatiquement bloqués. Ils peuvent également désactiver les commentaires sur une vidéo spécifique, limiter les commentaires à un public sélectionné ou désactiver entièrement les commentaires sur leurs vidéos.

De plus, nous sensibilisons régulièrement nos utilisateurs aux options de sécurité et de bien-être digital à leur disposition via des formats vidéo diffusés sur l’application et le [Centre de sécurité](https://www.tiktok.com/safety/resources). Par exemple, nous avons réalisé avec l’aide de créateurs une série de vidéos qui encouragent les utilisateurs à maîtriser leur temps d’écran. Notre fonction de gestion du temps d’écran permet aux utilisateurs de fixer un seuil maximum d’utilisation de l’application. Ils peuvent également décider d’activer le mode restreint qui limite l’apparition de contenus qui pourraient ne pas convenir à tous les publics. Ces fonctionnalités sont disponibles en permanence dans la section Bien-être numérique des paramètres de l’application.