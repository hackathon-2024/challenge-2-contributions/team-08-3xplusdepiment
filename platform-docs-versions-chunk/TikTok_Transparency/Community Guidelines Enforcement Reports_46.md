platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-4/


### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhaeh7pflk/Transparency_CGE_Q4/French.xlsx)

#### Nombre total de vidéos supprimées/remises en ligne, par type et trimestre

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok._

#### Nombre total de suppressions de vidéos, par règle

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, aussi le graphique indique chaque infraction. Dans certains cas rares, comme des situations d’urgence ou des pannes matérielles, il peut arriver que la catégorie d’infraction d’une vidéo supprimée ne soit pas enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### Taux de suppressions, par trimestre/règle

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### Suppressions par pays

|     |     |
| --- | --- |
| **Pays / Marché** | **1er octobre 2021 – 31 décembre 2021** |
| États-Unis | 12 943 295 |
| Indonésie | 6 657 310 |
| Pakistan | 6 563 594 |
| Russie | 5 977 996 |
| Philippines | 5 479 091 |
| Brésil | 3 941 136 |
| Bangladesh | 2 636 372 |
| Mexique | 2 567 999 |
| Vietnam | 1 995 738 |
| Royaume-Uni | 1 963 799 |

_REMARQUE : ce graphique dresse la liste des dix marchés avec le plus grand nombre de vidéos supprimées._

#### Nombre total de comptes supprimés, par trimestre et par motif

_REMARQUE : en plus du retrait des comptes qui enfreignent les Règles communautaires, nous procédons à la suppression descomptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### Fausse activité

|     |     |
| --- | --- |
|     | **1er octobre 2021 – 31 décembre 2021** |
| Comptes spam évités | 152 118 769 |
| Suppressions de vidéos publiées par des comptes spam | 46 382 130 |
| Suppressions de faux abonnés | 441 985 135 |
| Demandes de suivi par de faux comptes évitées | 2 794 306 196 |
| Suppressions de faux « j’aime » | 285 323 840 |
| Faux « j’aime » évités | 11 917 679 868 |

#### Application des règles concernant les contenus publicitaires

|     |     |
| --- | --- |
|     | **1er octobre 2021 – 31 décembre 2021** |
| Total des publicités supprimées | 3 181 858 |