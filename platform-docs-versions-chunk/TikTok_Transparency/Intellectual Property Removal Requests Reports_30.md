platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2019-2/

Demandes de retrait de contenu portant atteinte à la propriété intellectuelle

_1er juillet 2019 –31 décembre 2019_  
_Date de publication : 9 juin 2020_