platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-1/


### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des publicités sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Au cours du premier trimestre de 2023, le volume total de contenus publicitaires retirés pour non-respect de nos politiques publicitaires et le nombre de contenus publicitaires supprimés en raison d’actions au niveau du compte ont diminué. Nous sommes conscients qu’assurer la sécurité des annonceurs et des utilisateurs nécessite un engagement permanent. C’est pourquoi nous nous engageons à revoir et à renforcer constamment nos systèmes afin de pouvoir supprimer rapidement et avec précision les contenus publicitaires contraires à nos politiques. En appliquant des politiques strictes, en tirant parti de mécanismes de détection avancés et en améliorant continuellement nos systèmes, nous nous efforçons de favoriser une expérience publicitaire sûre, agréable et conforme aux valeurs de notre passionnante communauté TikTok.