platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-3/

Application des Règles communautaires

_1er juillet 2022 – 30 septembre 2022_  
_Publication du 19 décembre 2022_

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de sa communauté. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles Communautaires ou nos Conditions de service. Par ailleurs, nous publions régulièrement des informations sur les mesures prises afin de rendre compte à notre communauté.

TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles. Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.