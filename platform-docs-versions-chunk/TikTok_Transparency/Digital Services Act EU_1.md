platform: TikTok
topic: Transparency
subtopic: Digital Services Act EU
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Digital Services Act EU.md
url: https://www.tiktok.com/transparency/en/dsa-transparency/

Digital Services Act

**Learn more about the actions we take upon content and accounts that violate our policies or local laws and how we strive to foster a fun and inclusive environment where people can create, find community, and be entertained.**

* * *

#### **About the** **Digital Services Act**

The Digital Services Act (the ‘**DSA**’) is designed to further strengthen the safety of people on platforms like ours. A key objective of the DSA is to give users of digital services even greater insight into the work we do to keep them safe, and provide them with additional tools – and ultimately, greater choice – over their own experience of the platform. This includes requiring greater transparency about the content moderation activities we engage in.

Very Large Online Platforms under the DSA are required to make publicly available a report which outlines the content moderation activities it engages in. These reports include information on the following:

* Our approach to content moderation generally (including the training and support we provide to our content moderators).
* An overview of the kinds of restrictions we impose in response to content and accounts which violate our policies or local laws, either as a result of the content moderation we engage in at our own initiative or in response to law enforcement requests for information and government requests for content removals.
* An overview of the number of reports we have received through our new dedicated ‘illegal content reporting’ channel and the action we have taken in response to those notices.
* A breakdown of the number of monthly active users in each individual EU Member State.

We’re proud of our strong track record of being transparent about the steps we take to keep TikTok safe and entertaining, publishing our first transparency report in [2019](https://www.tiktok.com/transparency/en-us/community-guidelines-enforcement-2019-2/). We will continue to develop our policies, and we strive to enforce these rules equitably, consistently and fairly to maintain a safe and inclusive environment for our community.

#### **Reports**

Building on our proactive approach to transparency and as part of our obligations under the DSA, we will publish a transparency report every six months to provide granular data for EU countries about our content moderation activities. These reports are available here:

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Download](https://sf16-va.tiktokcdn.com/obj/eden-va2/fsslreh7uulsn/DSA%20Report%20October%202023/DSA%20draft%20Transparency%20report%20-%2025%20October%202023.pdf)

#### Was this helpful?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Yes![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)No