platform: TikTok
topic: Transparency
subtopic: Digital Services Act EU
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Digital Services Act EU.md
url: https://www.tiktok.com/transparency/en/copd-eu/


### About the Code

The Code of Practice on Disinformation (the ‘Code’) is a significant step forward for the industry, taking an innovative approach to combat disinformation by bringing together major online platforms, emerging and specialised platforms, advertising industry participants, fact-checkers and researchers.

Signatories to the Code undertake to deliver against a range of commitments across different issues related to online misinformation, including:

* Demonetisation of disinformation
* Transparency of Political Advertising
* Integrity of Services
* Empowering Users
* Empowering Researchers
* Empowering the Fact-Checking Community

We’re proud to have played a part in the co-regulatory process to draft the strengthened Code of Practice on Disinformation, building on our work as a signatory to the previous Code. We will continue to invest extensively as we work together with others to combat disinformation, promote authentic online experiences for our communities, and meet our obligations as a responsible and trusted platform.