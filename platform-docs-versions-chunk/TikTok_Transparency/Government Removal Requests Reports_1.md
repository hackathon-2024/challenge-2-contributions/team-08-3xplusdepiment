platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2023-1/

Demandes de retrait émanant des autorités publiques

_1 janvier 2023 – 30 juin 2023_

_Date de publication : 10 novembre 2023_