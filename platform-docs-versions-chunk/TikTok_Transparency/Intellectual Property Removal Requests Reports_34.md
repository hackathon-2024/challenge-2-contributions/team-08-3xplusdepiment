platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2019-1/

Demandes de retrait de contenu portant atteinte à la propriété intellectuelle

_January 1, 2019 – June 30, 2019_  
_Released: December 30, 2019_

### About this report

TikTok is built on the creativity of our users. Accordingly, our Community Guidelines prohibit content that infringes third party intellectual property and we have a robust copyright protection policy.

We honor valid take-down requests based on violations of copyright law, such as the Digital Millennium Copyright Act (DMCA). Upon receiving an effective notice from a rights holder of potential intellectual property infringement, TikTok will remove the infringing content in a timely manner.

Any activity that infringes the copyrights of others may lead to account suspension or removal. For more information on how we evaluate copyright infringement allegations, please see our [Intellectual Property Policy](https://www.tiktok.com/legal/copyright-policy).