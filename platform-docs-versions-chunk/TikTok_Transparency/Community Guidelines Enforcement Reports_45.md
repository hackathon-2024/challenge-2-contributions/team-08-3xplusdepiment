platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-4/


### Analyse

Les Règles communautaires de TikTok sont conçues pour favoriser une expérience qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus, et nous nous efforçons d’être cohérents et équitables dans leur application.

#### Sécurité

Nous associons technologie et intervention humaine pour identifier et supprimer les infractions aux Règles communautaires, et nous investissons continuellement dans nos politiques, nos produits et nos partenariats pour renforcer la sécurité de l’ensemble de notre communauté et de notre plateforme. Au quatrième trimestre de 2021, nous avons continué à améliorer nos techniques de modération par apprentissage automatique (machine learning), ce qui a permis d’améliorer la pertinence et la précision de nos systèmes de suppression automatisés.

Grâce aux améliorations continues de la reconnaissance et de la détection audio et vocale, nous avons perfectionné de manière significative la détection proactive et la suppression des contenus liés au suicide, à l’automutilation, aux actes dangereux, aux comportements haineux ou constitutifs de harcèlement et d’intimidation. Par ailleurs, l’optimisation continue de nos modèles de détection relatifs à la sécurité des mineurs a permis d’améliorer l’efficacité de notre dispositif de modération. Le nombre de vidéos rétablies après avoir fait l’objet d’un appel a diminué par rapport au trimestre précédent, ce que nous attribuons à une réduction des faux positifs et à une amélioration de la précision de notre système de détection et de suppression automatique.

#### Sûreté

Nous continuons à développer et à adapter nos mesures de protection en investissant dans des systèmes automatisés pour détecter, bloquer et supprimer les comptes et les interactions artificielles, et en améliorant notre temps de réaction et notre réponse à l’évolution des menaces. Au quatrième trimestre 2021, nous avons développé des systèmes de surveillance supplémentaires et amélioré les plateformes et processus existants de traitement des tickets d’assistance technique. Cela a renforcé notre capacité à détecter la création de comptes d’utilisateurs frauduleux lors de l’inscription et à supprimer les faux suiveurs, les faux « j’aime » et les vidéos associées à ces comptes.

#### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus mensongers, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des annonces sont soumis à ces politiques et doivent respecter nos Règles communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Malgré une augmentation globale des publicités publiées sur TikTok au cours du quatrième trimestre 2021, le volume total d’annonces supprimées pour non-respect de nos politiques et directives publicitaires a diminué. Cela est dû en partie aux efforts de plusieurs équipes consacrées à l’amélioration de notre écosystème publicitaire et qui contribuent à créer de meilleures expériences pour les utilisateurs et les annonceurs sur notre plateforme. Nous nous engageons à créer un environnement sûr et positif pour nos utilisateurs, et nous révisons et renforçons régulièrement nos systèmes pour combattre les publicités qui transgressent nos règles.