platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-2/


### Rapport sur les données

#### Sécurité des plus jeunes

Afin de maintenir un environnement sûr et positif pour notre communauté, et en particulier pour les adolescents, nous veillons chaque jour à apprendre, adapter et renforcer nos politiques et pratiques. Nous pensons que nous sommes plus efficaces lorsque nous collaborons avec des pairs du secteur pour faire avancer ces efforts. Ainsi, en mai 2021, nous avons rejoint la [“Technology Coalition”](https://newsroom.tiktok.com/fr-fr/tiktok-rejoint-la-technology-coalition), une organisation qui œuvre à la protection des enfants contre l’exploitation et les abus sexuels en ligne, afin d’approfondir notre approche en matière d’intervention fondée sur des données probantes. Nous avons également expliqué les approches multiples que nous adoptons pour promouvoir des expériences adaptées à l’âge des utilisateurs et faire en sorte que l’expérience TikTok continue à être accessible aux à partir de 13 ans.

Au cours du second trimestre 2021, nous avons également actualisé notre Centre de sécurité. Nous sommes conscients des défis auxquels sont confrontés les parents à l’ère numérique et, dans le cadre de nos efforts pour soutenir et sensibiliser les familles sur notre plateforme, nous avons introduit le [Guide à destination des parents](https://www.tiktok.com/safety/fr-fr/guardians-guide/), un espace unique pour tout savoir sur TikTok. Vous y trouverez toutes les informations afin de débuter sur la plateforme, une présentation de nos outils et fonctionnalités de sécurité et de protection de la vie privée, tels que le [Mode Connexion Famille](https://newsroom.tiktok.com/fr-fr/de-nouvelles-ressources-sur-le-mode-connexion-famille-de-tiktok), et de nombreuses ressources pour répondre aux questions concernant la sécurité en ligne.

#### Bien-être

Chez TikTok, nous mettons l’accent sur le bien-être des utilisateurs afin que notre communauté puisse continuer à se développer, à s’épanouir et à surprendre le monde par sa créativité. Aussi, nous sommes toujours à la recherche de nouvelles façons de contribuer au bien-être de notre communauté. Au cours du second trimestre 2021, nous avons :

* Ajouté de nouveaux paramètres afin notamment de [pouvoir signaler ou supprimer plusieurs commentaires simultanément](https://newsroom.tiktok.com/fr-fr/de-nouveaux-outils-pour-lutter-contre-le-harcelement), et que les comptes qui publient des commentaires d’intimidation ou d’autres commentaires négatifs puissent être bloqués en même temps (jusqu’à 100 à la fois).
* Publié un guide sur la prévention de l’intimidation pour aider les familles à apprendre à identifier l’intimidation ainsi que des outils permettant d’y faire face et d’aider les victimes ou les témoins de l’intimidation.
* Lancé [#CreateKindness](https://newsroom.tiktok.com/fr-fr/encourager-la-bienveillance-sur-tiktok), une campagne internationale et une série de vidéos créatives visant à sensibiliser les utilisateurs aux dangers de l’intimidation en ligne et les encourager à être bienveillants, aimables, les uns envers les autres.

Nous avons pour but d’offrir un espace sûr où les utilisateurs se sentent les bienvenus et peuvent s’exprimer comme tels qu’ils sont. Nous souhaitons que la diversité de notre communauté puisse se sentir libre de partager et de s’exprimer. Au second trimestre 2021, nous avons célébré le mois des fiertés avec notre campagne #FreeToBe. Nous avons mis en avant les créateurs LGBTQ+ et organisé notre défilé mondial de la Pride TikTok, un LIVE de 12 heures qui a permis de collecter des fonds pour les organisations qui soutiennent la communauté LGBTQ+.

#### Protéger l’intégrité de la plateforme

Nous nous efforçons de construire de manière responsable, ouverte et équitable afin que les utilisateurs TikTok continuent à aimer créer et visionner les contenus qui leur tiennent à cœur. Afin de soutenir nos équipes qui élaborent nos stratégies de sécurité et qui sont responsables de l’application de nos politiques, nous avons établi un partenariat avec la [Trust and Safety Professional Association (TSPA)](https://newsroom.tiktok.com/fr-fr/tiktok-rejoint-la-trust-and-safety-professional-association), de sorte que chaque professionnel travaillant sur des problématiques de sécurité soit désormais membre de la TSPA. Cette adhésion leur permet d’accéder à des ressources pour le développement de leur carrière, de participer à des ateliers et à des événements, et de se connecter à un réseau de pairs du secteur. Notre soutien à l’association enrichira aussi la communauté TikTok en nous permettant de continuer d’apprendre et de faire évoluer notre approche de la sécurité.