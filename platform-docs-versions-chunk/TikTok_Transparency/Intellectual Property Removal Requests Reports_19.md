platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/

Demandes de retrait de contenu portant atteinte à la propriété intellectuelle

_1er janvier 2021-30 juin 2021  
__Publication : 2 décembre 2021_