platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2021-1/


### À propos de ce rapport

TikTok repose sur la créativité de notre communauté et nous nous employons à encourager et protéger l’expression personnelle et le divertissement. Toutefois, dans certains cas, nous retirerons ou restreindrons le contenu signalé par des organismes gouvernementaux. Nous honorons les demandes de retrait ou de restriction de contenu qui nous sont adressées par les voies indiquées dans notre Centre d’aide et lorsque la loi l’exige. Lorsque nous recevons de telles demandes émanant de gouvernements, nous les étudions et prenons des mesures relatives au contenu signalé dans le respect de nos [Règles communautaires](https://www.tiktok.com/community-guidelines), de nos [Conditions générales](https://www.tiktok.com/legal/terms-of-service) et des lois applicables. Si nous pensons qu’une demande n’est pas légalement recevable ou n’enfreint pas nos normes, nous pourrions restreindre la disponibilité du contenu signalé dans le pays où il serait considéré comme illégal ou encore, nous pourrions rejeter la demande.

Les sections ci-dessous indiquent le volume des demandes de retrait ou de restriction reçues de gouvernement et la nature de la réponse donnée par TikTok.