platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-1/


### Synthèse

#### Sécurité des mineurs

Chez TikTok, nous nous attachons à créer un environnement adapté à l’âge des utilisateurs en concevant des politiques et des outils qui contribuent à promouvoir une expérience sûre et positive sur notre plateforme. Depuis janvier 2021, tous les paramètres de confidentialité par défaut de nos utilisateurs âgés de 13 à 15 ans sont automatiquement en Mode Privé, limitant ainsi les personnes autorisées à télécharger leurs vidéos et à interargir avec leur contenu par commentaires, ou les fonction Duos et Collage. Pour les utilisateurs âgés de 16 à 17 ans, les fonctions Duo et Collage sont paramétrées sur Amis. Par défaut, l’option permettant de choisir les personnes autorisées à télécharger est mise sur Off.

Il s’agit d’une initiative importante pour renforcer la sécurité et la protection de la vie privée des mineurs. Ces mises à jour s’appuient sur de précédentes fonctionnalités de restriction par l’âge, dont la suppression de la Messagerie Directe, streaming en direct (« livestream ») et les cadeaux virtuels pour tous les utilisateurs de, respectivement, moins de 16 ou 18 ans. Notre ambition est d’aider les utilisateurs à faire des choix éclairés sur ce qu’ils veulent partager et avec qui ils souhaitent le faire. En impliquant les adolescents dès le début, nous espérons leur donner les moyens de gérer avec précaution leur présence en ligne. Nous souhaitons également accompagner les parents en leur fournissant des [outils et des ressources](https://www.tiktok.com/safety/en-us/guardians-guide/) qui leur permettront de dialoguer avec les adolescents sur leur sécurité en ligne et la culture numérique. Par exemple, le [Mode Connexion famille](https://newsroom.tiktok.com/fr-fr/connexion-famille-mise-a-jour) permet aux parents de lier leur compte TikTok à celui de l’adolescent pour définir ensemble certains paramètres de confidentialité, de navigation et de temps d’écran.

#### Bien-être

La grande diversité de notre communauté contribue à faire de TikTok un environnement privilégié où explorer et apprécier du contenu divertissant, que ce soit des recettes de cuisine comme des idées de jardinage. Nous mettons tout en oeuvre pour faire de TikTok un environnement sûr et positif, et dans cette perspective, nous avons élaboré de nouveaux outils pour promouvoir la bienveillance et le respect au sein de notre communauté.

* Nous avons développé [une notification](https://newsroom.tiktok.com/fr-fr/filtres-tous-commentaires) qui invite les utilisateurs à reconsiderer l’impact de leurs mots avant de publier un commentaire qui pourrait être désagréable.
* Nous avons introduit un moyen pour les créateurs de filtrer tous les commentaires sur leur contenu afin que seuls ceux approuvés apparaissent sur leurs vidéos.
* Nous avons annoncé un partenariat avec le Cyberbullying Research Center afin de faire progresser nos connaissances sur l’intimidation et de développer de nouvelles initiatives de lutte contre le harcèlement.

Nous voulons que chacun se sente à l’aise et en confiance pour s’exprimer pleinement sur TikTok. Pour encourager [la diversité corporelle sur TikTok](https://newsroom.tiktok.com/en-us/supporting-nedawareness-and-body-inclusivity-on-tiktok), nous proposons depuis février des ressources sur le bien-être, développées avec le concours d’experts en troubles alimentaires, afin d’aider les utilisateurs à identifier les propos négatifs qu’ils tiennent sur eux-mêmes, à prendre conscience de leurs propres atouts et forces, ou à soutenir un ami qui pourrait traverser une période difficile. Nous avons également publié des messages d’intérêt public permanents sur des hashtags pertinents afin de sensibiliser le public ou d’encourager le soutien de la communauté aux personnes souffrant de troubles alimentaires et à leur guérison.

#### Protéger l’intégrité de la plateforme

Nous maintenons nos efforts pour lutter contre les contenus et comportements qui visent à mettre en péril l’intégrité de notre plateforme. Pour atteindre cet objectif en matière d’activités illégales et de produits réglementés, nous avons rejoint la [Coalition to End Wildlife Trafficking Online](https://newsroom.tiktok.com/en-us/tiktok-joins-the-coalition-to-end-wildlife-trafficking-online), ce qui nous permet de collaborer avec d’autres acteurs du secteur sur les meilleures pratiques ainsi que les contenus et tendances émergents.

Par ailleurs, TikTok supprime ou limite les informations trompeuses dès qu’elles sont identifiées. Nous travaillons avec 11 organisations accréditées par l’International Fact-Checking Network afin de vérifier les faits dans 21 langues et 57 marchés en Amérique, Asie, Europe, au Moyen-Orient et en Afrique. Si les enquêtes confirment qu’un contenu est faux, celui-ci est retiré de la plateforme. Cette année, au cours des trois premiers mois, nous [avons introduit une fonctionnalité](https://newsroom.tiktok.com/en-us/new-prompts-to-help-people-consider-before-they-share) visant à réduire la diffusion de contenus non fondés.