platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2020-1/


### Rapport sur les données

#### Demandes d’information sur les utilisateurs émanant d’autorités publiques

Le tableau suivant indique le nombre de demandes d’informations sur les utilisateurs reçues au cours du premier semestre 2020 (du 1er janvier au 30 juin 2020) et la pourcentage de demandes traitées.

|     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- |
| **Pays / Marché** | **Total des demandes légales** | **Total des demandes d’urgence** | **Total des demandes** | **Total des comptes indiqués** | **Pourcentage de demandes légales pour lesquelles certaines données ont été produites** |
| Argentine | 3   | 0   | 3   | 2   | 0%  |
| Australie | 7   | 6   | 13  | 19  | 31% |
| Autriche | 1   | 0   | 1   | 1   | 0%  |
| Bangladesh | 1   | 0   | 1   | 2   | 0%  |
| Belgique | 3   | 1   | 4   | 4   | 0%  |
| Brésil | 1   | 0   | 1   | 1   | 0%  |
| Bulgarie | 1   | 0   | 1   | 1   | 0%  |
| Canada | 0   | 11  | 11  | 13  | 73% |
| Colombie | 0   | 1   | 1   | 1   | 100% |
| Chypre | 1   | 0   | 1   | 1   | 0%  |
| Danemark | 6   | 0   | 6   | 12  | 0%  |
| Finlande | 2   | 0   | 2   | 2   | 0%  |
| France | 6   | 7   | 13  | 11  | 54% |
| Allemagne | 30  | 7   | 37  | 34  | 16% |
| Grèce | 0   | 3   | 3   | 3   | 100% |
| Hongrie | 0   | 1   | 1   | 0   | 0%  |
| Inde | 1187 | 19  | 1206 | 1851 | 79% |
| Israël | 2   | 39  | 41  | 41  | 85% |
| Italie | 9   | 3   | 12  | 11  | 25% |
| Japon | 12  | 6   | 18  | 17  | 44% |
| Malte | 3   | 2   | 5   | 5   | 40% |
| Mexique | 2   | 0   | 2   | 2   | 0%  |
| Népal | 5   | 0   | 5   | 5   | 0%  |
| Nouvelle-Zélande | 1   | 0   | 1   | 7   | 0%  |
| Norvège | 4   | 3   | 7   | 14  | 43% |
| Pakistan | 5   | 1   | 6   | 6   | 17% |
| Pologne | 3   | 1   | 4   | 4   | 25% |
| Portugal | 3   | 0   | 3   | 2   | 0%  |
| Qatar | 1   | 0   | 1   | 1   | 0%  |
| Roumanie | 0   | 2   | 2   | 2   | 0%  |
| Russie | 1   | 0   | 1   | 1   | 100% |
| Serbie | 1   | 0   | 1   | 1   | 0%  |
| Singapour | 8   | 0   | 8   | 9   | 63% |
| Slovaquie | 1   | 1   | 2   | 2   | 50% |
| Corée du Sud | 6   | 2   | 8   | 16  | 13% |
| Espagne | 4   | 1   | 5   | 6   | 20% |
| Suède | 0   | 3   | 3   | 3   | 33% |
| Suisse | 2   | 0   | 2   | 2   | 0%  |
| Turquie | 4   | 0   | 4   | 4   | 0%  |
| Taïwan | 1   | 0   | 1   | 1   | 0%  |
| Royaume-Uni | 12  | 19  | 31  | 29  | 58% |
| États Unis | 222 | 68  | 290 | 327 | 85% |