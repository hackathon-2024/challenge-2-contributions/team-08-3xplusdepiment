platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2022-2/

### **Analyse**

Nous avons constaté une augmentation des demandes d’informations par rapport à la période couverte par le dernier rapport. Nous attribuons celle-ci à la croissance globale de la plateforme et aux investissements réalisés pour familiariser les forces de l’ordre avec nos [Lignes directrices destinées aux forces de l’ordre](https://www.tiktok.com/legal/law-enforcement) et aux processus liés.