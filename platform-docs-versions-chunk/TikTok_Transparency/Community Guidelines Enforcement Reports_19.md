platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-1/


### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_CGE_2023Q1/2023Q1_raw_data_cger_French.csv)

#### **Nombre total de vidéos supprimés / total de vidéos par trimestre**

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok. Le nombre de vidéos supprimées mentionné dans ce rapport comprend le contenu vidéo au format court (notamment les vidéos et les stories contenant des photos)._

#### **Nombre total de vidéos supprimées / remises en ligne, par type et trimestre**

_REMARQUE : dans le tableau ci-dessous, les vidéos remises en ligne sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne._

#### **Taux de suppression, par trimestre / règle**

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### **Nombre total de suppressions de vidéos, par règle et politique**

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas néanmoins rares, comme lors de situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### **Nombre total de suppressions et taux de vidéos, par sous-règle et politique**

_REMARQUE : seules les vidéos qui ont été examinées et supprimées par des modérateurs sont incluses dans le tableau de bord des sous-règles et politiques._

#### **Volume et taux de suppression, par marché**

_REMARQUE : ce graphique dresse la liste des cinquante pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 90 % du volume total des suppressions._

#### _**Répartition**_ _**des langues**_ _**couvertes par**_ _**la modération humaine**_

_REMARQUE : Ce tableau tient compte de la langue principale des modérateurs qui travaillent sur les vidéos de courte durée, les diffusions en direct, les commentaires, les comptes et TikTok Now. Il n’intègre pas les modérateurs qui peuvent être amenés à travailler dans plusieurs langues. Les langues qui constituent la langue principale de moins de 2 % de tous les modérateurs ont été regroupées dans la catégorie « Autres langues ». La modération peut également s’effectuer dans des langues autres que celles qui ont été définies comme langues principales possibles pour les modérateurs._

#### _**Délai de traitement des contenus signalés par la communauté**_

_REMARQUE : Seules les vidéos qui ont été signalées pour la première fois par des utilisateurs et qui ont ensuite fait l’objet d’une mesure de retrait sont incluses. Les données indiquent le temps passé entre le moment où TikTok a reçu un rapport d’utilisateur et le moment où le contenu a été supprimé. Maintenir l’équilibre entre les signalements et les réponses dans le cadre des contrôles humains nécessite des capacités, des formations et du contenu adaptés, ce qui peut parfois entraîner des délais de contrôle plus longs._

#### **Nombre total de comptes supprimés, par trimestre et par motif**

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### **Fausse activité**

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « abonnés » et les demandes « d’abonnement » si nous estimons que l’activité est automatisée ou frauduleuse._

#### **Activité des comptes spam**

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

#### **Opérations d’influence cachées**

|     |     |     |     |
| --- | --- | --- | --- |
| **Réseaux d’opérations d’influence cachées identifiés et supprimés** **au premier trimestre 2023** | **Source de la détection** | **Comptes du réseau** | **Abonnés du réseau** |
| Ce réseau semblait opérer depuis la Russie et cibler principalement les audiences russes. Le réseau a été partiellement constitué avant que nous ne suspendions la diffusion en direct et les nouveaux contenus en Russie et il a largement eu recours à la tactique du masquage de l’origine géographique. Les individus derrière ce réseau ont utilisé de fausses identités, y compris de faux organes d’information, afin d’amplifier artificiellement des opinions pro-russes dans le discours sur la guerre en Ukraine. | Interne | 15  | 38,326 |
| Ce réseau semblait opérer depuis la Russie et cibler les audiences russes. Le réseau a largement utilisé le masquage de l’origine géographique comme tactique pour tromper les systèmes de TikTok. Les individus derrière ce réseau ont utilisé de faux comptes pour publier massivement du contenu afin de promouvoir artificiellement le film de guerre « The Best in Hell » d’Evgueni Viktorovitch Prigojine et d’amplifier artificiellement des opinions pro-russes dans le contexte la guerre en Ukraine. | Interne | 254 | 34,110 |
| Ce réseau semblait opérer depuis la Pologne et cibler les audiences polonaises. Les individus derrière ce réseau ont créé de fausses identités et publié massivement des commentaires au contenu similaire afin d’amplifier artificiellement des opinions anti-russes. | Interne | 41  | 40,256 |
| Ce réseau semblait opérer depuis Israël et cibler les audiences israéliennes. Les individus derrière ce réseau ont utilisé de fausses identités afin d’amplifier artificiellement des opinions pro-israéliennes spécifiques dans le discours sur le conflit en cours en Palestine dans le contexte des dernières élections israéliennes. | Interne | 362 | 168,202 |
| Ce réseau semblait opérer depuis la Russie et cibler les audiences moldaves. Les personnes à l’origine de ce réseau se sont fait passer pour de fausses agences de presse et ont créé de fausses identités afin de créer une base d’abonnés sur TikTok pour ensuite contourner l’interdiction régionale de TikTok en Russie en redirigeant ces abonnés hors de la plateforme. | Interne | 11  | 55,066 |
| Ce réseau semblait opérer depuis la Russie et cibler les audiences russes. Les individus derrière ce réseau ont créé de faux comptes et publié massivement du contenu afin d’amplifier artificiellement certaines opinions pro-russes dans le contexte de la guerre en Ukraine en abusant de la fonction de partage et en utilisant l’application web afin de contourner l’interdiction régionale de TikTok en Russie. | Interne | 1,351 | 226,838 |
| Ce réseau semblait opérer depuis le Royaume-Uni et le Nigeria et cibler principalement les audiences nigérianes. Les individus derrière ce réseau ont utilisé de fausses identités et se sont fait passer pour de faux organes d’information afin d’amplifier artificiellement des opinions liées à la région du Biafra dans le discours sur les élections au Nigeria. | Interne | 15  | 47,103 |
| Ce réseau semblait opérer depuis la Russie et cibler différents pays européens, tels que l’Allemagne, l’Italie et le Royaume-Uni. Les individus derrière ce réseau ont usurpé des identités afin d’amplifier artificiellement certaines opinions liées au président ukrainien Zelensky, aux sanctions économiques actuellement imposées à la Russie et aux réfugiés ukrainiens. | Interne | 12  | 1,480 |
| Ce réseau semblait opérer depuis l’Irlande et cibler les audiences irlandaises. Les personnes à l’origine de ce réseau ont créé de faux comptes, publié massivement des contenus qui sèment la discorde au sujet du nationalisme dans l’Irlande, le Japon, la Russie et Taïwan et publié massivement des commentaires contenant des contenus de mauvaise qualité similaires, et ce dans le but de rediriger les utilisateurs TikTok hors de la plateforme et d’intensifier les conflits sociaux. | Interne | 72  | 94,743 |
| Ce réseau semblait opérer depuis la Malaisie et cibler les audiences malaisiennes. Les personnes à l’origine de ce réseau ont créé de faux comptes et ont publié massivement des commentaires identiques en anglais et en malais sur de nombreuses vidéos au sujet des élections en Malaisie. | Interne | 175 | 285,511 |
| Ce réseau semblait opérer depuis l’Ukraine et cibler les audiences ukrainiennes. Les individus derrière ce réseau ont créé de faux comptes et publié massivement du contenu afin d’amplifier artificiellement certaines opinions liées au gouvernement ukrainien et de promouvoir une image positive du président Zelensky. | Interne | 119 | 90,303 |
| Ce réseau semblait opérer depuis l’Allemagne et cibler les audiences égyptiennes. Les individus derrière ce réseau ont crée de faux comptes afin d’amplifier artificiellement les appels à rejoindre des manifestations inexistantes en Égypte en ciblant le discours sur le gouvernement égyptien actuel. | Interne | 6   | 368,644 |

REMARQUE : le retrait d’une opération d’influence cachée nécessite des opérations à plusieurs niveaux, comprenant l’enquête, la suppression et l’analyse à l’issue de l’intervention. Nous indiquons l’intervention sur ces réseaux au cours du trimestre où le processus complet des opérations a été achevé.