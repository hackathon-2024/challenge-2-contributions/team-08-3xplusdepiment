platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2019-2/

### Rapport sur les données

#### Demandes de retrait de contenu qui enfreint les lois relatives à la propriété Intellectuelle

|     |     |
| --- | --- |
| Copyrighted content take-down notices | Percentage of requests where some content was removed |
| 1338 | 82.60% |

_Remarque_ _: Seuls les signalements de violation des droits d’auteur émis par les détenteurs de droits d’auteur, leurs mandataires ou leurs avocats sont inclus dans nos chiffres relatifs au retrait des contenus protégés par des droits d’auteur._