platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-2/


### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhaeh7pflk/Transparency_LIPGR_2021_H2/French_LIPGR_2021_H2.xlsx)

#### Notifications de retrait de contenus pour infraction aux lois sur le droit d’auteur

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Pays/région | Période | Total des demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur | Total des de demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur | Pourcentage moyen des demandes honorées de retrait de contenus pour infraction aux lois sur le droit d’auteur |
| Global | 1er juillet – 31 décembre 2021 | 49821 | 40469 | 81.23% |

#### Notifications de retrait de contenus pour infraction aux lois sur les marques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Pays/région | Période | Total des demandes de retrait de contenus pour infraction aux lois sur les marques | Demandes honorées de retrait de contenus pour infraction aux lois sur les marques | Pourcentage moyen des demandes honorées de retrait de contenus pour infraction aux lois sur les marques |
| Global | 1er juillet – 31 décembre 2021 | 6379 | 5372 | 84.21% |