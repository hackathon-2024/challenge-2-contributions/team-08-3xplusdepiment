platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-1/


### Rapport sur les données

#### Vidéos

Au cours du premier trimestre 2021, **61 951 327** vidéos ont été supprimées dans le monde pour infraction à nos Règles communautaires ou Conditions d’utilisation, soit moins de **1 %** de toutes les vidéos téléchargées sur TikTok. Parmi ces vidéos, nous en avons identifié et supprimé **91,3 %** avant même qu’un utilisateur ne les signale, **81,8 %** avant qu’elles ne soient visionnées et **93,1 %** dans les 24 heures suivant leur publication.

Ce graphique indique les cinq marchés présentant le plus grand nombre de vidéos supprimées.

|     |     |
| --- | --- |
| ****Pays / Marché**** | **Suppression total** |
| États-Unis | 8,540,088 |
| Pakistan | 6,495,992 |
| Brésil | 6,128,568 |
| Russie | 3,747,364 |
| Indonesia | 2,757,292 |

Nous continuons de nous appuyer sur les technologies pour détecter et supprimer automatiquement les contenus en infraction sur certains marchés. **8 832 345** de vidéos supplémentaires ont été signalées et retirées automatiquement pour avoir enfreint nos Règles Communautaires à travers le monde.

TikTok permet aux créateurs de contester le retrait de leur vidéo. Lorsque nous recevons une demande de révision, nous examinons la vidéo une seconde fois et la rétablissons si s’avère qu’elle a été supprimée par erreur. Au cours du dernier trimestre, nous avons remis en ligne **2 833 837** vidéos ayant fait l’objet d’une demande de révision. Notre objectif est d’être cohérent et équitable dans nos pratiques de modération et nous poursuivons nos efforts pour réduire le nombre de “faux positifs” et former en continu nos équipes de modération.

Le graphique ci-dessous illustre le nombre de vidéos retirées pour infraction à notre politique. Une vidéo peut enfreindre plusieurs règles et chaque infraction est reflétée dans ce tableau.

Parmi les vidéos supprimées par notre équipe de modérateurs, le tableau suivant indique le taux de suppression proactive des vidéos pour des motifs enfreignant nos Règles Communautaires. Un retrait proactif consiste à détecter et à supprimer une vidéo avant qu’elle ne soit signalée. Un retrait dans les 24 heures signifie que la vidéo est retirée dans les 24 heures suivant sa publication sur notre plateforme.

|     |     |     |
| --- | --- | --- |
|     | **Retrait proactif** | **Retrait dans les 24 heures** |
| Nudité et activités sexuelles des utilisateurs adultes | 86.10% | 89.80% |
| Harcèlement et intimidation | 66.20% | 83.80% |
| Comportement haineux | 67.30% | 83.90% |
| Activités illégales et produits réglementés | 96.00% | 95.60% |
| Intégrité et authenticité | 78.50% | 88.90% |
| Sécurité des mineurs | 97.10% | 96.20% |
| Suicide, automutilation et actes dangereux | 93.70% | 91.70% |
| Contenu violent et visuellement explicite | 93.40% | 93.60% |
| Extrémisme violent | 82.10% | 87.10% |

[Nudité et activités sexuelles des utilisateurs adultes](https://www.tiktok.com/community-guidelines?lang=fr#30)Nous nous efforçons de construire une plateforme accueillante et sécurisée. Ainsi, nous supprimons tout contenu de nudité et à caractère sexuellement explicite. Parmi les vidéos supprimées, 15,6 % enfreignaient ces règles, représentant une baisse par rapport aux 20,5 % dans notre précédent rapport. TikTok retire tous les contenus à caractère pornographique et de nudité, et ce sans exception. Pour les contenus qui seraient plus appropriés pour des audiences matures, nous pouvons en limiter la diffusion dans le fil « Pour toi » de tout utilisateur, plutôt que de les supprimer. Nous avons supprimé 86,1 % de ces vidéos avant qu’elles ne nous soient signalées, tandis que 89,8 % ont été supprimées dans les 24 heures suivant leur publication, ce qui représente une légère baisse depuis notre dernier rapport.  

[Harcèlement et intimidation](https://www.tiktok.com/community-guidelines?lang=fr#36)  
Nous prônons une communauté inclusive où l’on peut s’exprimer librement et nous ne tolérons pas que les membres de notre communauté soient humiliés, intimidés ou harcelés. Parmi les vidéos que nous avons supprimées, 8 % enfreignaient cette règle, marquant une hausse par rapport aux 6,6 % sur le second semestre 2020. Cette augmentation reflète les mises à jour apportées à nos règles en fin d’année, dans le but de lutter contre l’intimidation, ainsi que les légères améliorations dans notre capacité à détecter le harcèlement ou l’intimidation. Parmi ces vidéos, 66,2 % ont été supprimées avant qu’elles ne nous soient signalées, et 83,8 % ont été supprimées dans les 24 heures suivant leur publication. Pour prévenir les comportements d’intimidation, et dans le cadre de nos efforts pour améliorer la détection proactive dans ce domaine, nous avons déployé plusieurs outils pour aider les créateurs à mieux contrôler leur expérience TikTok, dont la gestion des commentaires, le signalement et le blocage d’utilisateurs.  

[Comportement haineux](https://www.tiktok.com/community-guidelines?lang=fr#38)  
TikTok est une communauté plurielle et inclusive qui ne tolère pas de comportement haineux. Parmi les vidéos que nous avons supprimées, 2,3 % enfreignaient cette règle, représentant une légère hausse par rapport aux 2 % sur le second semestre 2020. Nous attribuons cette augmentation des retraits aux mesures de protection renforcées contre les discours haineux et les idéologies haineuses ainsi qu’à notre nouveau modèle qui nous permet de mieux repérer les discours haineux. Nous avons supprimé 67,3 % des vidéos illustrant des comportements haineux avant qu’elles ne nous soient signalées, bien que 83,9 % aient été supprimées dans les 24 heures suivant leur publication.  

[Activités illégales et produits réglementés](https://www.tiktok.com/community-guidelines?lang=fr#32)  
Nous veillons à ce que TikTok ne permette pas des activités criminelles ou faisant la promotion de drogues, tabac, d’alcool ou de tout autre produit réglementé. Sur les trois premiers mois de l’année 2021, 21,1 % des vidéos que nous avons supprimées enfreignaient cette règle. Ce chiffre est en hausse par rapport aux 17,9 % du second semestre 2020, et nous avons constaté une augmentation des tentatives de publication de ce type de contenu sur la plateforme. Parmi ces vidéos, 96 % ont été supprimées avant de nous être signalées, et 95,6 % ont été supprimées dans les 24 heures suivant leur publication. Nous attribuons ces améliorations au perfectionnement continu de nos systèmes de détection.  

[Intégrité et authenticité](https://www.tiktok.com/community-guidelines?lang=fr#37)  
Nous estimons que la confiance constitue le socle de notre communauté, et nous n’autorisons aucun contenu ou compte qui implique une interaction artificielle, une usurpation d’identité et une activité de désinformation. Parmi les vidéos supprimées, 2 % enfreignaient cette règle, soit une baisse par rapport aux 2,4 % sur le second semestre 2020. Parmi ces vidéos, 78,5 % ont été supprimées avant de nous être signalées, et 88,9 % ont été supprimées dans les 24 heures suivant leur publication. Nous avons continué d’améliorer nos modèles de détection des informations trompeuses, et ces efforts ont contribué à une sensible augmentation des repérages proactifs au cours du premier trimestre 2021 par rapport aux six derniers mois de 2020.  

[Sécurité des mineurs](https://www.tiktok.com/community-guidelines?lang=fr#31)  
Nous sommes résolument engagés à assurer la sécurité des mineurs et mettons régulièrement à jour notre politique et nos procédures pour les protéger. Au cours du premier trimestre 2021, 36,8 % des contenus enfreignaient notre politique en matière de sécurité des mineurs, contre 36 % pour la seconde moitié de 2020. Parmi ces vidéos, 97,1 % ont été supprimées avant de nous être signalées, et 96,2 % ont été supprimées dans les 24 heures suivant leur publication. Ces progrès en matière de détection proactive sont le résultat de l’amélioration de nos modèles qui permettent d’identifier et de signaler les infractions dès le téléchargement du contenu, avant toute visualisation.  

[Suicide, automutilation et actes dangereux](https://www.tiktok.com/community-guidelines?lang=fr#34)  
La santé et le bien-être des individus qui composent notre communauté nous tiennent particulièrement à cœur. À cet égard, nous nous employons à favoriser un environnement solidaire. Parmi les vidéos supprimées, 5,7 % enfreignaient cette règle, soit une baisse par rapport aux 6,2 % sur le second 2020. Nous estimons que ces résultats sont liées à une diminution du volume global de ces vidéos, car nous déployons des efforts concertés pour supprimer rapidement les contenus et rediriger les personnes qui les recherchent vers des services d’assistance spécialisés et des ressources d’urgence. Parmi ces vidéos, 93,7 % ont été supprimées avant de nous être signalées, et 91,7 % ont été supprimées dans les 24 heures suivant leur publication  

[Extrémisme violent](https://www.tiktok.com/community-guidelines?lang=fr#39)  
Nous nous opposons fermement à toute forme de violence sur et en dehors de TikTok. Parmi les vidéos supprimées, 0,5 % enfreignaient cette règle, contre 0,3 % sur le second semestre 2020. Parmi ces vidéos, 82,1 % ont été supprimées avant de nous être signalées, et 87,1 % ont été supprimées dans les 24 heures suivant leur publication. Nous estimons que cette augmentation des suppressions est due au fait que nos règles précisent désormais de manière plus détaillée ce qui est considéré comme une menace violente et/ou une incitation à la violence en matière de contenus et de comportements interdits. Nous développons des modèles de détection proactive qui permettent de signaler ce type de contenu, et examinons également les signalements soumis par les utilisateurs et les organisations non gouvernementales

[Contenu violent et visuellement explicite](https://www.tiktok.com/community-guidelines?lang=fr#35)  
TikTok est une plateforme qui encourage l’expression créative mais condamne les contenus choquants ou violents. Parmi les vidéos supprimées, 8,1 % enfreignaient cette règle, ce qui correspond aux chiffres du second semestre 2020. Parmi ces vidéos, 93,4 % ont été supprimées avant de nous être signalées, et 93,6 % ont été supprimées dans les 24 heures suivant leur publication. Il s’agit d’une amélioration par rapport à notre dernier rapport, liée au renforcement de nos politiques et d’une meilleure détection de ce type de contenus.

#### Comptes

Au cours du premier trimestre 2021, **11 149 514** comptes ont été supprimés pour infraction à nos Règles Communautaires ou Conditions d’utilisation. Ce chiffre comprend **7 263 952** comptes retirés car ils étaient soupçonnés d’être détenus par des utilisateurs de moins de 13 ans, soit moins de 1 % des comptes sur TikTok.

En complément, nous avons empêché la création de **71 470 161** de comptes par des méthodes automatisées et avons supprimé **12 378 928** de vidéos supplémentaires publiées par des spammeurs.

#### Contenus publicitaires

TikTok a mis en place des [mesures strictes](https://ads.tiktok.com/help/article?aid=10000962) pour protéger les utilisateurs contre les contenus publicitaires mensongers, frauduleux ou trompeurs. Les comptes d’annonceurs et les publicités payantes sont soumis à ces règles et doivent se conformer à nos Règles Communautaires, [Politiques de publicité TikTok](https://ads.tiktok.com/help/article?aid=10000962) et Conditions d’utilisation. Au cours du premier trimestre 2021, nous avons rejeté **1 921 900** de contenus publicitaires pour infraction à nos politiques et règles en matière de publicité.

Nous nous engageons à ce que nos utilisateurs aient une expérience positive, authentique et agréable lorsqu’ils regardent des publicités sur TikTok. Ainsi, nous continuerons à investir tant dans nos ressources humaines que dans nos moyens technologiques pour veiller à ce que les contenus publicitaires de la plateforme répondent à nos attentes.

#### Sécurité

En octobre 2020, nous avons lancé [le programme mondial « bug bounty »](https://newsroom.tiktok.com/en-au/security-is-our-priority-all-year-long-au) (le « butin des bogues ») qui est le prolongement de notre programme de gestion des vulnérabilités. Son objectif est de repérer de manière proactive les vulnérabilités de sécurité et de les résoudre. Ce programme renforce notre degré de maturité en matière de sécurité en encourageant les chercheurs en sécurité du monde entier à identifier et à signaler de manière responsable les bogues à nos équipes afin que nous puissions les résoudre avant que des malfaiteurs ne les exploitent.

Au cours du premier trimestre 2021, TikTok a reçu **33** signalements de bogue valides dont **29** ont été résolus. Nous avons reçu **8** demandes de divulgation publique, lesquelles ont toutes été publiées. En moyenne, TikTok procède au règlement des récompenses dans un délai de **3 jours**. Quant au temps de réponse de TikTok, en moyenne, le délai de première réponse est de **8 heures** et le délai de résolution de **30 jours**.