platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2020-1/


### À propos de ce rapport

Les sections ci-dessous présentent le volume et la nature des demandes reçues de la part des gouvernements et autorités locales au cours du premier semestre 2020, et la nature de notre réponse. Nous recevons des demandes émanants de gouvernement et d’agences gouvernementales de partout dans le monde entier. Nous honorons les demandes qui nous sont adressées dans le respect des règles et quand la loi l’exige.

De temps en temps, les autorités publiques nous soumettent des demandes en vue de limiter ou supprimer de notre plateforme du contenu contrevenant à la législation locale. Nous examinons ces éléments conformément à nos Règles Communautaires, à nos Conditions de service, ainsi qu’à la législation en vigueur et prenons la décision adaptée. Si nous pensons qu’un signalement n’est pas légalement recevable ou ne va pas à l’encontre de nos standards, nous pouvons décider soit de limiter la disponibilité du contenu signalé dans le pays où elle est supposément illégale, soit de ne prendre aucune mesure.