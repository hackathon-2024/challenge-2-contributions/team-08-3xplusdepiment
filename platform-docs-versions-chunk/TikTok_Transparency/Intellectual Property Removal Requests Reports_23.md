platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2020-2/

Demandes de retrait de contenu portant atteinte à la propriété intellectuelle

_1e juillet – 31 décembre 2020  
__Publié le 24 février 2021_