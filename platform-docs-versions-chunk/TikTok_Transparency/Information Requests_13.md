platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2022-1/


### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_LIPGR_2022H2/French_LIPGR_2022_H1.xlsx)

#### **Demandes de renseignements sur les utilisateurs par les forces de l’ordre**

_REMARQUE : le taux de divulgation indique les cas où au moins certaines données ont été communiquées. Les cas de non-divulgation comprennent les situations où TikTok a demandé des informations supplémentaires ou des éclaircissements concernant la demande de communication des données et où les forces de l’ordre requérantes n’ont pas fourni les informations nécessaires conformément à nos [Lignes directives destinées aux forces de l’ordre](https://www.tiktok.com/legal/law-enforcement?lang=en) et/ou où TikTok a rejeté la demande en raison d’une procédure judiciaire incomplète ou invalide. Les demandes de conservation de données sont des demandes officielles faites par les forces de l’ordre pour conserver les données afin qu’elles aient suffisamment de temps pour lancer une procédure judiciaire valide de communication des données._