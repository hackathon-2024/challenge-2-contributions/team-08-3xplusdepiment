platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2019-2/


### Responsabiliser la communauté via différents outils

En plus des technologies et des systèmes de modération mis en place (voir ci-dessous), de nombreuses fonctionnalités sont à la disposition des utilisateurs pour qu’ils puissent prendre le contrôle de leur expérience en ligne. Il s’agit de moyens très simples à activer pour restreindre l’accès à leur contenu, définir des filtres de commentaires automatiques, désactiver la fonction de messagerie ou bloquer d’autres utilisateurs. De plus, s’ils constatent un manquement à nos Règles Communautaires, nous les invitons à le signaler auprès de nos équipes, directement depuis l’application.

#### Confidentialité des comptes

TikTok propose un large éventail de paramètres de confidentialité que les utilisateurs peuvent activer lors de la création de leur compte ou à tout autre moment. Actuellement, concernant les comptes privés, seuls les abonnés autorisés peuvent visionner ou commenter les vidéos de l’utilisateur concerné ou lui envoyer un message direct. La fonctionnalité de messagerie peut être très simplement désactivée ou bien encore limitée ; cette fonctionnalité est désactivée et inaccessible sur les comptes des utilisateurs de moins de 16 ans. Par ailleurs, les utilisateurs peuvent supprimer un abonné ou bien encore empêcher un autre utilisateur de les contacter.