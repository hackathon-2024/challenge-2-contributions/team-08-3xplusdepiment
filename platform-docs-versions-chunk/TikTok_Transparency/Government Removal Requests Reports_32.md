platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2019-2/


### À propos de ce rapport

Les sections ci-dessous présentent le volume des demandes légales reçues des gouvernements et autorités locales dans le monde entier au cours du second semestre 2019 et la nature de notre réponse. Nous respectons les droits et la vie privée de nos utilisateurs. C’est pourquoi nous demandons aux autorités publiques de soumettre par écrit leur demande accompagnée des documents légaux appropriés. Nous ne prenons pas en compte des demandes qui ne sont effectuée par les voies appropriées. De temps en temps, les autorités publiques nous soumettent des demandes en vue de retirer du contenu de notre plateforme comme par exemple concernant des législations locales interdisant les comportements obscènes, les discours haineux, des contenus d’adulte, etc… Nous examinons ces éléments conformément à nos Règles Communautaires, à nos Conditions de services, ainsi qu’à la législation en vigueur et prenons la décision adaptée. Si nous pensons qu’un signalement n’est pas légalement recevable ou ne va pas à l’encontre de nos standards, nous ne prenons aucune mesure.