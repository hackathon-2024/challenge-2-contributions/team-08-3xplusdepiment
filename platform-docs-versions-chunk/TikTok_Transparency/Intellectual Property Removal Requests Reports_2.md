platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2023-1/


### **À propos de ce rapport**

La créativité de notre communauté est au cœur de TikTok; c’est ce qui en fait un lieu de divertissement unique. Notre plateforme encourage les utilisateurs à laisser libre cours à leur créativité et nous faisons de notre mieux pour la protéger. Nos Règles communautaires et Conditions générales interdisent tout contenu qui porte atteinte aux droits de propriété intellectuelle d’un tiers. Nous honorons les demandes de retrait justifiées fondées sur une infraction aux lois sur le droit d’auteur et sur les marques. À la réception d’une notification valide d’un détenteur de droits ou d’un représentant autorisé concernant une éventuelle atteinte à la propriété intellectuelle, TikTok peut retirer le contenu visé et suspendre temporairement ou définitivement le compte en infraction. Pour savoir comment nous évaluons les allégations d’atteinte aux droits de propriété intellectuelle, veuillez consulter notre [Politique en matière de propriété intellectuelle](https://www.tiktok.com/legal/copyright-policy?lang=en). Les sections ci-dessous indiquent les demandes de retrait de contenus pour infraction aux lois sur le droit d’auteur et sur les marques, et le taux de contenus supprimés.