platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-4/


### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_CGE_2022Q4/2022Q4_raw_data_cger_French.csv)

#### **Nombre total de vidéos supprimés / total de vidéos par trimestre**

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok. Le nombre de vidéos supprimées mentionné dans ce rapport comprend le contenu vidéo au format court (notamment les vidéos et les stories contenant des photos)._

#### Nombre total de vidéos supprimées / remises en ligne, par type et trimestre

_REMARQUE : dans le tableau ci-dessous, les vidéos remises en ligne sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne._

#### Taux de suppression, par trimestre / règle

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### Nombre total de suppressions de vidéos, par règle et politique

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas néanmoins rares, comme lors de situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### Nombre total de suppressions et taux de vidéos, par sous-règle et politique

_REMARQUE : seules les vidéos qui ont été examinées et supprimées par des modérateurs sont incluses dans le tableau de bord des sous-règles et politiques._

#### Volume et taux de suppression, par marché

_REMARQUE : ce graphique dresse la liste des cinquante pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 90 % du volume total des suppressions._

#### Nombre total de comptes supprimés, par trimestre et par motif

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### Fausse activité

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « abonnés » et les demandes « d’abonnement » si nous estimons que l’activité est automatisée ou frauduleuse._

#### Activité des comptes spam

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

#### **Opérations d’influence cachées**

|     |     |     |     |
| --- | --- | --- | --- |
| Réseaux d’opérations d’influence cachées identifiés et supprimés au quatrième trimestre 2022 | Source de la détection | Comptes du réseau | Abonnés du réseau |
| Ce réseau semblait opérer depuis le Kenya et cibler principalement les audiences du Kenya. Les individus derrière ces comptes ont partagé du contenu en swahili et en anglais via de faux comptes usurpant l’identité de personnalités politiques, ainsi que celle d’organes d’information et de jeunes femmes. Le réseau avait probablement des motivations financières et visait le discours civique au Kenya. | Externe | 33  | 19,120 |
| Ce réseau semblait opérer depuis le Brésil et cibler principalement les audiences brésiliennes. Les individus derrière ces comptes ont partagé du contenu en portugais et en anglais via de faux comptes avec des photos de banques d’images comme images de profil afin d’apparaître comme des personnes vivant au Brésil. Le réseau a diffusé des récits qui ont semé le doute quant à la fiabilité du processus électoral au Brésil. | Interne | 138 | 642,941 |
| Ce réseau semblait opérer depuis l’Azerbaïdjan et cibler principalement les audiences azerbaïdjanaises. Les personnes à l’origine de ces comptes utilisaient de faux comptes et publiaient des contenus de manière répétitive dans le but de diffuser des récits de soutien à l’Azerbaïdjan et à son gouvernement actuel dans le contexte du conflit entre l’Arménie et l’Azerbaïdjan dans la région du Haut-Karabakh. | Interne | 112 | 23,348 |
| Ce réseau semblait opérer depuis la Russie et cibler les pays européens, principalement l’Allemagne. Les individus derrière ces comptes ont créé de faux comptes localisés et ont partagé du faux contenu localisé en allemand sur la guerre actuelle en Ukraine, ainsi que sur les conséquences de celle-ci sur les économies des pays de l’UE. | Interne | 3,181 | 418,196 |
| Ce réseau semblait opérer depuis le Kazakhstan et cibler principalement les audiences kazakhes. Les personnes à l’origine de ces comptes ont utilisé de faux comptes et se sont fait passer pour des groupes d’activistes civils du pays afin d’inciter à des manifestations contre le gouvernement actuel en promouvant artificiellement des hashtags et des récits critiquant le gouvernement kazakh actuel, la structure de l’armée dans le pays et les élections à venir. | Interne | 6   | 38,003 |

_REMARQUE : le retrait d’une opération d’influence cachée nécessite des opérations à plusieurs niveaux, comprenant l’enquête, la suppression et l’analyse à l’issue de l’intervention. Nous indiquons l’intervention sur ces réseaux au cours du trimestre où le processus complet des opérations a été achevé._

#### Définitions

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

#### Application des règles concernant les contenus publicitaires

_REMARQUE : Les contenus peuvent être supprimés individuellement ou en bloc en appliquant des mesures à l’ensemble d’un compte d’annonceur._