platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2019-2/

Demandes d’information

_1er juillet 2019 –31 décembre 2019_  
_Date de publication : 9 juin 2020_