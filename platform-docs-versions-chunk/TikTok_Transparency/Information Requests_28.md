platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2020-1/

Demandes d’information

_Du 1e janvier au 30 juin 2020_  
_Mise en ligne: 22 Septembre 2020_