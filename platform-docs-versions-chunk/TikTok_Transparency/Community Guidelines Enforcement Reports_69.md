platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2020-1/


### Rapport sur les données

#### Vidéo

Au premier semestre 2020 (du 1er janvier au 30 juin 2020), 104 543 719 vidéos ont été supprimées à l’échelle internationale pour avoir enfreint nos Règles Communautaires ou nos Conditions de service, soit moins de 1 % des vidéos postées sur TikTok. 96,4 % de ces vidéos ont été identifiées et supprimées par nos soins avant même de faire l’objet d’un signalement, et 90,3 % ont été retirées de la plateforme avant d’avoir été visionnées ne serait-ce qu’une fois.

Le tableau ci-après recense le nombre de vidéos supprimées pour avoir enfreints nos politiques. Quand une vidéo enfreint nos Règles Communautaires, elle est catégorisée en fonction de la règle (ou des règles) enfreinte(s), puis supprimée. Cela signifie qu’une même vidéo peut être comptabilisée dans plusieurs catégories de règles.

Du fait de la pandémie de coronavirus, nous avons eu plus largement recours à la technologie pour détecter et supprimer automatiquement les contenus en infraction avec nos règles sur des marchés comme l’Inde, le Brésil et le Pakistan. Sur l’ensemble des vidéos retirées de la plateforme, 10 698 297 ont été signalées et supprimées automatiquement pour avoir enfreint nos Règles Communautaires. Ces vidéos ne sont pas comptabilisées dans le tableau ci-dessous.

Le tableau ci-après présente les 5 pays/marchés comptant le plus grand nombre de vidéos supprimées.

|     |     |
| --- | --- |
| **Pays** | **Total des retraits** |
| Inde | 37,682,924 |
| États-Unis | 9,822,996 |
| Pakistan | 6,454,384 |
| Brésil | 5,525,783 |
| Royaume Uni | 2,949,620 |