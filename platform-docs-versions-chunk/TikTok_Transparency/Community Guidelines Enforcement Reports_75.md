platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2019-2/

### Synthèse

Pour TikTok, rien n’est plus important que d’assurer la sécurité de ses utilisateurs. En tant que plateforme mondiale, des milliers de collaborateurs travaillent au maintien d’un environnement sûr. Les contenus et comportements en ligne sont traités par un ensemble de règles, de technologies et de modérations, qui peuvent mener à la suppression de vidéos, au bannissement de comptes qui iraient à l’encontre de nos [Règles Communautaires](https://www.tiktok.com/community-guidelines) et de nos [Conditions de Services](https://www.tiktok.com/legal/terms-of-use).