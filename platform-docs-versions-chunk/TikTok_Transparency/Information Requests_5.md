platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2022-2/

Demandes d’information

_1er juillet 2022 – 31 décembre 2022_  
_Publication du 15 mai 2023_