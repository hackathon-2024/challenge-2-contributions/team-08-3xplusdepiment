platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2022-2/

Demandes de retrait émanant des autorités publiques

_1er juillet 2022 – 31 décembre 2022_  
_Publication du 15 mai 2023_