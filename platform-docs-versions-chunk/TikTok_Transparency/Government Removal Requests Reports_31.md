platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2019-2/

Demandes de retrait émanant des autorités publiques

_1er juillet 2019 –31 décembre 2019 Date de publication : 9 juin 2020_