platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-2/


### **Sécurité**

[Les informations présentées dans ce rapport reflètent nos](https://newsroom.tiktok.com/en-us/community-guidelines-update)[règles communautaires actualisées](https://www.tiktok.com/community-guidelines/en/), la [sécurité et le bien-être des jeunes](https://www.tiktok.com/community-guidelines/en/youth-safety/?cgversion=2023) demeurant une priorité absolue pour nous. Nous nous engageons à faire en sorte que TikTok propose une expérience sûre et positive pour les moins de 18 ans.

Dans le cadre de cet engagement, nos politiques interdisent strictement tout contenu susceptible de compromettre le bien-être des jeunes, y compris le risque d’exploitation ou de dommages psychologiques, physiques ou développementaux potentiels. Nous accordons la priorité à la sécurité et au bien-être des jeunes tout au long de notre processus d’élaboration de nos politiques. Cela comprend l’interdiction du matériel pédopornographique, de la maltraitance des jeunes, du harcèlement, des activités et défis dangereux, de l’exposition à des thèmes trop matures et de la consommation d’alcool, de tabac, de drogues ou de substances réglementées. Lorsque nous identifions des cas d’exploitation de jeunes sur notre plateforme, nous prenons des mesures immédiates, y compris le bannissement du compte incriminé et de tous les comptes associés.

Nous ajoutons également des informations sur l’application de la règlementation pour TikTok LIVE, qui permet aux utilisateurs et aux créateurs d’interagir en temps réel. Au cours du deuxième trimestre 2023, nous avons suspendu 8 074 632 sessions LIVE. Cela représente 1,5 % du nombre total de sessions LIVE au cours de cette même période. Parmi elles, 252 045 suspensions ont fait l’objet d’un recours et ont été levées. Pour que l’expérience TikTok soit la meilleure possible, nous sommes ravis de partager plus d’informations sur nos efforts de modération LIVE et nous continuerons à développer ce sujet dans les prochains rapports.

#### **Suppression des vidéos relatives à la publication**

#### **Nombre total de vidéos et de comptes supprimés et restaurés**

#### **Taux de suppression et délais de réponse aux signalements**

#### **Politique en matière de suppression**

#### **Modération par marché et par langue**

#### Covert influence operations

**Comptes ciblant les relations étrangères de l’Europe avec la Russie**

Nous estimons que ce réseau opérait à partir de la Russie et ciblait des audiences russes en Russie, aux Pays-Bas, en Roumanie, en France et au Royaume-Uni. Les personnes à l’origine de ce réseau ont créé de faux comptes et ont posté en masse du contenu afin d’amplifier artificiellement des points de vue pro-russes spécifiques dans le contexte de la guerre en Ukraine, en contournant l’interdiction régionale d’utiliser TikTok en Russie.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 588 | 36,331 |

**Comptes ciblant les relations étrangères de l’Irak avec la Russie**

Nous estimons que ce réseau opérait depuis l’Irak et ciblait des audiences irakiennes. Les personnes à l’origine de ce réseau ont créé de faux comptes, y compris de faux personas et de fausses entités médiatiques, afin d’amplifier artificiellement des contenus soulignant l’importance stratégique de la Russie en Irak.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 364 | 524,513 |

**Comptes ciblant les discours sur la Chine**

Nous estimons que ce réseau opérait depuis la Chine et ciblait des audiences américaines. Les personnes à l’origine de ce réseau ont créé de faux comptes afin de susciter l’intérêt des utilisateurs pour les points de vue pro-chinois, le tourisme pro-chinois, la culture pro-chinoise et les récits anti-occidentaux.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 108 | 141,621 |

**Comptes ciblant les discours politiques en Grèce**

Nous estimons que ce réseau opérait depuis la Grèce et ciblait des audiences grecques. Les personnes à l’origine de ce réseau ont créé de faux comptes et ont posté en masse du contenu pour amplifier artificiellement des récits spécifiques favorables au parti politique « Nouvelle Démocratie » et critiques à l’égard du parti d’opposition « Syriza », en ciblant les discours des élections grecques.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 69  | 25,898 |

**Comptes ciblant les discours politiques en Turquie**

Nous estimons que ce réseau opérait depuis la Belgique et ciblait des audiences turques. Les personnes à l’origine de ce réseau ont créé de faux comptes, y compris de faux personas et de fausses entités médiatiques, afin d’amplifier artificiellement des contenus favorables au président Erdogan et en relation avec le Parti des travailleurs du Kurdistan (PKK), ciblant le discours sur les élections en Turquie.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 68  | 61,379 |

**Comptes ciblant les discours politiques en Thaïlande**

Nous estimons que ce réseau opérait depuis la Thaïlande et ciblait des audiences thaïlandaises. Les personnes à l’origine de ce réseau ont créé de faux comptes, y compris la création étendue de faux personas, afin d’amplifier artificiellement des points de vue spécifiques du parti pro-PPRP, en ciblant le discours sur les élections générales thaïlandaises.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 31  | 1,817 |

**Comptes ciblant le conflit au Soudan**

Nous estimons que ce réseau opérait depuis le Soudan et ciblait des audiences soudanaises. Les personnes à l’origine de ce réseau ont créé de faux comptes, y compris de faux personas et de fausses entités médiatiques, afin d’amplifier artificiellement des contenus favorables au groupe paramilitaire Rapid Support Force (RSF), ciblant le discours sur le conflit en cours entre les forces RSF et les forces armées soudanaises.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 17  | 10,213 |

REMARQUE: Le retrait d’une opération d’influence cachée nécessite des opérations à plusieurs niveaux, comprenant l’enquête, la suppression et l’analyse à l’issue de l’intervention. Nous indiquons l’intervention sur ces réseaux au cours du trimestre où le processus complet des opérations a été achevé.

**Définitions**

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

* * *