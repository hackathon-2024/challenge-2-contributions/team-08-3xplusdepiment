platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2020-2/


### Rapport sur les données

#### Vidéos

Au cours du second semestre 2020 (du 1er juillet au 31 décembre), **89 132 938** de vidéos ont été supprimées dans le monde entier pour avoir enfreint nos Règles Communautaires ou nos Conditions de services, soit moins de **1 %** de toutes les vidéos téléchargées sur TikTok. Parmi ces vidéos, nous en avons identifiées et retirées **92,4 %** avant leur signalement par un utilisateur, **83,3 %** ont été retirées avant qu’elles ne soient visionnées et **93,5 %** ont été retirées dans les 24 heures suivant leur publication.

Ce graphique illustre les cinq marchés détenant le plus grand nombre de vidéos supprimées.

|     |     |
| --- | --- |
| **Pays** | **Total des retraits** |
| Etats-Unis | 11,775,777 |
| Pakistan | 8,215,633 |
| Brésil | 7,506,599 |
| Russie | 4,574,690 |
| Indonésie | 3,860,156 |

En raison de la pandémie, nous continuons de nous appuyer sur les technologies pour détecter et supprimer automatiquement les contenus en infraction sur certains marchés, tels que le Brésil et le Pakistan. **8 295 164** de vidéos supplémentaires ont été signalées et retirées automatiquement pour avoir enfreint nos Règles Communautaires. Ces vidéos ne sont pas comptabilisées dans les tableaux ci-dessous.

TikTok permet aux créateurs de contester le retrait de leur vidéo. Lorsque nous recevons une demande de révision, nous examinons la vidéo une seconde fois et la rétablissons si elle n’enfreint pas nos règles. Au cours du dernier semestre, nous avons remis en ligne **2 927 391** de vidéos ayant fait l’objet d’une demande de révision.

Ce graphique illustre le nombre de vidéos retirées pour infraction à notre politique. Une vidéo peut enfreindre plusieurs règles et chaque infraction est reflétée dans ce tableau.

Parmi les vidéos supprimées par notre équipe de modérateurs, le tableau suivant indique le taux de suppression proactive des vidéos pour des motifs enfreignant nos Règles Communautaires. Un retrait proactif consiste à détecter et à supprimer une vidéo avant qu’elle ne soit signalée. Un retrait dans les 24 heures signifie que la vidéo est retirée dans les 24 heures suivant sa publication sur notre plateforme.

|     |     |     |
| --- | --- | --- |
|     | **Taux de vidéos supprimées proactivement** | **Taux de suppression** |
| [Nudité et activités sexuelles des utilisateurs adultes](https://www.tiktok.com/community-guidelines?lang=fr#30) | 88.30% | 90.60% |
| [Harcèlement et intimidation](https://www.tiktok.com/community-guidelines?lang=fr#36) | 66.50% | 84.10% |
| [Comportement haineux](https://www.tiktok.com/community-guidelines?lang=fr#38) | 72.90% | 83.50% |
| [Activités illégales et produits réglementés](https://www.tiktok.com/community-guidelines?lang=fr#32) | 96.30% | 94.80% |
| [Intégrité et authenticité](https://www.tiktok.com/community-guidelines?lang=fr#37) | 70.50% | 91.30% |
| [Sécurité des mineurs](https://www.tiktok.com/community-guidelines?lang=fr#31) | 97.10% | 95.80% |
| [Suicide, automutilation et actes dangereux](https://www.tiktok.com/community-guidelines?lang=fr#34) | 94.40% | 91.90% |
| [Contenu violent et visuellement explicite](https://www.tiktok.com/community-guidelines?lang=fr#35) | 93.20% | 92.70% |
| [Extrémisme violent](https://www.tiktok.com/community-guidelines?lang=fr#39) | 86.90% | 89.40% |

**[Nudité et activités sexuelles des utilisateurs adultes](https://www.tiktok.com/community-guidelines?lang=fr#30)**  
Nous nous efforçons de créer une plateforme sûre et bienveillante, et nous supprimons tout contenu de nudité et à caractère sexuellement explicite. Parmi les vidéos supprimées, **20,5 %** enfreignaient ces règles, ce qui correspond à une baisse de **30,9 %** par rapport au premier semestre 2020. L’une des raisons de cette diminution est l’amélioration de nos systèmes de triage qui séparent les contenus de nudité des utilisateurs adultes de ceux des utilisateurs mineurs. Nous avons supprimé **88,3 %** de ces vidéos avant qu’elles ne nous soient signalées, et **90,6 %** ont été supprimées dans les 24 heures suivant leur publication.

**[Harcèlement et intimidation](https://www.tiktok.com/community-guidelines?lang=fr#36)**  
Nous prônons une communauté inclusive où l’on peut s’exprimer librement sans craindre de subir d’abus. Nous ne tolérons pas que les membres de notre communauté soient humiliés, intimidés ou harcelés. Parmi les vidéos que nous avons supprimées, **6,6 %** enfreignaient ces règles, contre **2,5 %** au premier semestre 2020. Cette augmentation reflète les ajustements apportés aux règles relatives au harcèlement sexuel, aux menaces de piratage informatique et aux victimes de déclarations d’intimidation, qui sont désormais plus complètes. En outre, nous avons constaté de légères améliorations dans notre capacité à proactivement détecter le harcèlement ou l’intimidation, ce qui reste un défi compte tenu des nuances linguistiques et culturelles. Parmi ces vidéos, **66,5 %** ont été supprimées avant qu’elles ne nous soient signalées, et **84,1 %** ont été supprimées dans les 24 heures suivant leur publication. Nous sommes déterminés à réduire cet écart et nous tiendrons notre communauté informée des évolutions dans ce domaine.

**[Comportement haineux](https://www.tiktok.com/community-guidelines?lang=fr#38)**  
TikTok est une communauté plurielle et inclusive qui ne tolère aucun comportement haineux. L’année dernière, nous avons modifié notre politique, passant de « discours haineux » à son nom actuel de « comportement haineux », afin d’adopter une approche plus large pour lutter contre les idéologies haineuses et les activités hors de la plateforme. En conséquence, **2 %** des vidéos que nous avons supprimées enfreignaient cette règle, contre **0,8 %** au premier semestre 2020. Nous disposons de systèmes de détection de symboles haineux, tels que les drapeaux et les icônes, mais la détection proactive de discours haineux reste un défi et nous poursuivons nos investissements afin de nous améliorer. Nous avons supprimé **72,9 %** de vidéos illustrant des comportements haineux avant qu’elles ne nous soient signalées, bien que **83,5 %** aient été supprimées moins de 24 heures suivant leur publication.

**[Activités illégales et produits réglementés](https://www.tiktok.com/community-guidelines?lang=fr#32)**  
Nous veillons à ce que TikTok ne permette pas des activités qui enfreignent les lois ou réglementations en vigueur, telles que la fraude ou les arnaques. **17,9 %** des vidéos que nous avons supprimées enfreignaient cette règle. Un chiffre en légère baisse par rapport aux **19,6 %** du premier semestre 2020. Nous attribuons ce résultat à l’amélioration de nos systèmes d’automatisation et de détection ainsi qu’à des flux de travail plus performants. Parmi ces vidéos, **96,3 %** ont été retirées avant d’être signalées, et **94,8 %** ont été retirées dans les 24 heures suivant leur publication.

**[Intégrité et authenticité](https://www.tiktok.com/community-guidelines?lang=fr#37)**  
Nous estimons que la confiance constitue le socle de notre communauté, et nous n’autorisons aucun contenu ou compte qui implique une interaction artificielle, une usurpation d’identité et une activité de désinformation. Parmi les vidéos supprimées, **2,4 %** enfreignaient cette règle, contre **1,2 %** au premier semestre 2020. Nous avons intégré de nouveaux partenaires vérificateurs de faits sur de nouveaux marchés et disposons désormais d’un réseau en 16 langues. Cela nous aide à évaluer plus précisément les contenus et à retirer les informations trompeuses. Nous avons également amélioré notre capacité à détecter et à supprimer les interactions artificielles et les spams. Parmi les vidéos supprimées, **70,5 %** l’ont été avant de nous être signalées, et **91,3 %** dans les 24 heures suivant leur publication. Nous investissons dans nos infrastructures pour améliorer notre système de détection proactive, en particulier lorsqu’il s’agit d’identifier des informations trompeuses.

**[Sécurité des mineurs](https://www.tiktok.com/community-guidelines?lang=fr#31)**  
Nous sommes résolument engagés à assurer la sécurité des mineurs et mettons régulièrement à jour notre politique et nos procédures pour les protéger. Par exemple, nous avons ajouté une section dédiée aux activités dangereuses et nous supprimons tout contenu représentant ou faisant la promotion de la consommation de boissons alcoolisés, tabac, ou drogues ou tout autre comportement qui pourrait mettre en péril le bien-être des mineurs. En conséquence, **36 %** des contenus que nous avons supprimés enfreignaient cette règle, contre **22,3 %** au cours du premier semestre 2020. Parmi ces vidéos, **97,1 %** ont été supprimées avant de nous être signalées, et **95,8 %** ont été supprimées dans les 24 heures suivant leur publication.

TikTok s’appuie sur [PhotoDNA](https://www.microsoft.com/en-us/photodna), une technologie qui aide à identifier et supprimer les contenus qui exploitent des enfants, pour se protéger des matériaux d’abus sexuels d’enfants. Nous poursuivons d’investir dans nos systèmes internes de détection de ce type de contenus. Ces efforts ont considérablement amélioré notre capacité à supprimer et à signaler les contenus et les comptes concernés au Centre national pour les enfants disparus et exploités (NCMEC) et aux autorités juridiques compétentes. En conséquence, nous avons effectué **22 692 signalements** au NCMEC en 2020, contre **596** en 2019.

**[Suicide, automutilation et actes dangereux](https://www.tiktok.com/community-guidelines?lang=fr#34)**  
La santé et le bien-être des individus qui composent notre communauté nous tiennent particulièrement à cœur. Au cours du second semestre 2020, nous avons mis à jour nos Règles Communautaires sur l’automutilation, le suicide et les troubles alimentaires afin de prendre en compte les commentaires et langages utilisés par les experts en santé mentale. Nous avons également établi des partenariats avec certaines associations afin de soutenir les personnes en difficulté. Nous redirigeons par exemple les recherches et les hashtags concernés vers la ligne d’assistance téléphonique de la « National Eating Disorder Association », la « Crisis Textline » ou la « National Suicide Prevention Lifeline ». Parmi les vidéos supprimées, **6,2 %** enfreignaient ces règles, ce qui représente une baisse par rapport aux **13,4 %** du premier semestre 2020, et s’explique notamment car nous supprimons désormais tout contenu mettant en scène des mineurs aux comportements dangereux ou risqués, selon les termes de notre nouvelle politique de sécurité des mineurs face aux activités à risques. Parmi ces vidéos, **94,4 %** ont été supprimées avant d’être signalées, et **91,9 %** ont été supprimées dans les 24 heures suivant leur publication.

**[Extrémisme violent](https://www.tiktok.com/community-guidelines?lang=fr#39)**  
Nous nous opposons fermement à toute forme de violence sur et en dehors de TikTok. L’automne dernier, lors de la mise à jour de nos Règles Communautaires, nous avons clarifié notre politique contre les personnes et organisations violentes, pour adresser de façon globale les défis de l’extrémisme violent et avons précisé ce que TikTok considère comme une menace ou une incitation à la violence. Sur l’ensemble des vidéos supprimées, **0,3 %** enfreignaient cette règle, ce qui est comparable au contenu supprimé au premier semestre 2020. Parmi ces vidéos, **86,9 %** ont été supprimées avant d’être signalées, et **89,4 %** ont été supprimées dans les 24 heures suivant leur publication.

**[Contenu violent et visuellement explicite](https://www.tiktok.com/community-guidelines?lang=fr#35)**  
TikTok est une plateforme qui encourage l’expression créative mais condamne les contenus choquants ou violents. Parmi les vidéos supprimées, **8,1 %** enfreignaient cette règle, contre **8,7 %** au premier semestre 2020. Parmi ces vidéos, **93,2 %** ont été supprimées avant d’être signalées, et **92,7 %** ont été supprimées dans les 24 heures suivant leur publication. Nous autorisons sur notre plateforme, à des fins documentaires, les vidéos représentant des manifestations violentes, des scènes d’animaux chassant dans la nature et d’autres contenus de ce type. Nous avons d’ailleurs introduit des [systèmes de visionnage personnalisés](https://newsroom.tiktok.com/en-us/refreshing-our-policies-to-support-community-well-being) pour permettre aux utilisateurs de mieux choisir les vidéos qu’ils regardent.

#### Comptes

Au cours du second semestre 2020, **6 144 040** de comptes ont été supprimés pour violation de nos Règles Communautaires. En complément, **9 499 881** de comptes de spam ont été supprimés, ainsi que **5 225 800** de vidéos de spam publiées par ces comptes. Nous avons empêché **173 246 894** de comptes d’être créés par des spammeurs.

#### Contenus publicitaires

TikTok a mis en place des mesures strictes pour protéger les utilisateurs contre les contenus mensongers, frauduleux ou trompeurs, dont les publicités. Les comptes d’annonceurs et les contenus publicitaires sont soumis à ces règles et doivent se conformer à nos Règles Communautaires, Règles publicitaires et à nos Conditions de services. Au cours du second semestre 2020, nous avons supprimé **3 501 477** de contenus publicitaires pour violation de nos politiques et règles en matière de publicité. Nous nous engageons à créer un environnement sûr et bienveillant pour nos utilisateurs, et nous réévaluons et améliorons régulièrement nos systèmes de lutte contre les contenus publicitaires qui enfreignent nos politiques.