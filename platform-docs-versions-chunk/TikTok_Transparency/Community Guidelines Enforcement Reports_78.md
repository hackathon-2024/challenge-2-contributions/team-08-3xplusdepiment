platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2019-2/

### Accorder nos Règles Communautaires avec les attentes de notre communauté

TikTok est une communauté mondiale dont les membres sont à la recherche d’une expérience authentique et positive. Notre engagement envers notre communauté débute avec nos Règles Communautaires. Elles constituent un code de conduite qui permet de proposer un environnement sécurisé et bienveillant. Ces Règles Communautaires sont régulièrement mises à jour afin de protéger nos utilisateurs de tendances et contenus qui pourraient être dangereux. Elles sont destinées à favoriser la confiance, le respect et la dimension positive de la communauté TikTok. Nous attendons de l’ensemble de nos utilisateurs qu’ils respectent nos Règles Communautaires pour que TikTok reste un lieu divertissant, fun, où chacun est le bienvenu. Le non-respect de ces règles peut entraîner la suppression de contenus et de comptes.