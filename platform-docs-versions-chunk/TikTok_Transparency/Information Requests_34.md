platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2019-2/


### Rapport sur les données

#### Demandes légales d’information sur les utilisateurs

|     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- |
| **Pays / Marché** | **Total des demandes légales** | **Total des demandes d’urgence** | **Total des demandes** | **Total des comptes indiqués** | **Pourcentage de demandes légales pour lesquelles certaines données ont été produites** |
| Argentine | 2   | 0   | 2   | 2   | 0%  |
| Australie | 2   | 0   | 2   | 2   | 0%  |
| Autriche | 0   | 1   | 1   | 1   | 100% |
| Canada | 1   | 4   | 5   | 5   | 80% |
| Chili | 1   | 0   | 1   | 1   | 0%  |
| République Tchèque | 1   | 1   | 2   | 2   | 50% |
| Finlande | 0   | 2   | 2   | 2   | 100% |
| France | 1   | 3   | 4   | 4   | 75% |
| Allemagne | 13  | 2   | 15  | 17  | 13% |
| Grèce | 1   | 2   | 3   | 3   | 67% |
| Inde | 291 | 11  | 302 | 408 | 90% |
| Israël | 1   | 5   | 6   | 6   | 83% |
| Italie | 4   | 1   | 5   | 5   | 20% |
| Japon | 6   | 10  | 16  | 18  | 50% |
| Népal | 1   | 0   | 1   | 5   | 0%  |
| Pays-Bas | 1   | 1   | 2   | 3   | 50% |
| Norvège | 7   | 3   | 10  | 10  | 20% |
| Pakistan | 1   | 0   | 1   | 1   | 0%  |
| Corée du Sud | 3   | 0   | 3   | 3   | 0%  |
| Espagne | 1   | 0   | 1   | 2   | 0%  |
| Suisse | 3   | 0   | 3   | 5   | 0%  |
| Suède | 1   | 0   | 1   | 1   | 0%  |
| Taïwan | 1   | 0   | 1   | 1   | 0%  |
| Turquie | 1   | 0   | 1   | 1   | 0%  |
| Royaume-Uni | 8   | 2   | 10  | 22  | 20% |
| États Unis | 78  | 22  | 100 | 107 | 82% |

_Remarque : TikTok n’a pas reçu de demande légale d’obtention de données de compte de la part de pays ou de marchés autres que ceux indiqués dans la liste ci-dessus._