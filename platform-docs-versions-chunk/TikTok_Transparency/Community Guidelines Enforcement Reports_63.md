platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2020-2/


### À propos de ce rapport

TikTok possède une communauté internationale, d’une grande diversité, où chacun peut exprimer sa créativité. Nous mettons tout en oeuvre pour maintenir un environnement où chacun se sent libre et en sécurité de créer des vidéos, trouver une communauté et se divertir. Nous pensons qu’il est essentiel de se sentir en sécurité pour s’exprimer librement de manière authentique, et c’est pourquoi nous nous efforçons de faire respecter nos [Règles Communautaires](https://www.tiktok.com/community-guidelines?lang=fr) en supprimant les comptes et les contenus qui les enfreignent. Notre objectif est que TikTok reste un espace source d’inspiration, de créativité et de divertissement pour tous.

Nous nous engageons à faire preuve de transparence sur la façon dont nos règles sont appliquées, car cela contribue à instaurer la confiance avec notre communauté et nous rend responsable de nos actions. Nous publions des Rapports de Transparence qui donnent un aperçu du volume et de la nature des contenus supprimés pour infraction à nos Règles Communautaires ou à nos [Conditions de services](https://www.tiktok.com/legal/terms-of-use?lang=fr).