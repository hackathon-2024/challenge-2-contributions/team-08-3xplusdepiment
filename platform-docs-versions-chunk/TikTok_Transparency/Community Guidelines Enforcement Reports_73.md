platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2019-2/


### À propos de ce rapport

TikTok est une plateforme d’expression créative et nous encourageons nos utilisateurs à y partager ce qui les rend uniques. Evoluer dans un environnement sûr permet à nos utilisateurs de se sentir à l’aise, libre de s’exprimer et de laisser libre cours à leur créativité. C’est pourquoi notre priorité absolue est de leur offrir un environnement sûr et une expérience positive.

Notre avons publié notre premier rapport de transparence le 30 décembre 2019 et nous nous sommes engagés à informer notre communauté de façon régulière afin de lui présenter la façon dont nous répondons aux demandes d’obtention de données et dont nous protégeons la propriété intellectuelle et ce, de façon responsable.

Le rapport de transparence a été complété pour fournir des informations concernant :

* Notre approche et nos politiques pour assurer la sécurité de notre communauté ;
* La manière dont nous définissons et renforçons nos Règles Communautaires ;
* Les outils mis à disposition de notre communauté et les démarches éducatives et de sensibilisation mises en oeuvre ;
* Le volume de vidéos supprimées pour non-respect de nos Règles Communautaires.

En complément, nous commençons à prendre en compte des éléments de mesure en lien avec neuf catégories de contenus.

Fin 2019, nous avons mis en place une nouvelle infrastructure de modération de contenu qui nous permet d’offrir plus de transparence lorsque nous indiquons les raisons pour lesquelles un contenu a été retiré. Dans ce rapport, nous mettons en avant les raisons qui ont conduit à la suppression de vidéos durant le mois de décembre au regard de cette nouvelle infrastructure de modération. Dans nos prochains rapports nous pourrons présenter ses données sur une période complète de six mois.

Nous mettons tout en oeuvre pour mériter chaque jour la confiance de notre communauté. Aussi, nous continuerons sans cesse à faire évoluer nos rapports de transparence afin de répondre aux questions de nos utilisateurs, du législateur et d’experts. Notre but principal est que TikTok reste un lieu source d’inspiration et de divertissement pour tous.