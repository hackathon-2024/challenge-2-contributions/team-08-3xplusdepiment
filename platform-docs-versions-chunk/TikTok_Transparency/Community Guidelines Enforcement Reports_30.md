platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-3/


### **Analyse**

Les Règles Communautaires de TikTok sont définies pour favoriser une expérience divertissante qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus, et nous nous efforçons d’être cohérents et justes dans leur application. Cette analyse propose des éléments contextuels pour faciliter l’appréciation des données de ce rapport.

#### **Sécurité**

Ce rapport a été réalisé pour apporter davantage de transparence à nos actions, nos avancées et nos défis, prendre nos responsabilités vis-à-vis de notre communauté. Pour fournir des informations utiles et pertinentes, nous proposons désormais des informations supplémentaires concernant les retraits des vidéos pour plus de 50 marchés, ce qui représente environ 90 % de l’ensemble des retraits de contenus.

De plus, ce rapport inclut maintenant des informations relatives aux opérations d’influence cachées que nous avons identifiées et supprimées de notre plateforme sur la période de juillet à septembre. Pour enquêter sur ces opérations et les supprimer, nous nous concentrons sur les comportements, et évaluons les relations entre les comptes et les techniques utilisées, pour déterminer si les utilisateurs concernés agissent de manière coordonnée pour tromper les systèmes de TikTok ou notre communauté. Pour chaque cas, nous sommes convaincus que les individus à l’origine de ces activités collaborent afin de donner une fausse image d’eux-mêmes et de leurs actions. Nous savons que les opérations d’influence cachées continueront de se développer dans le but de contourner nos systèmes de détection et certains réseaux pourraient essayer de rétablir leur présence sur notre plateforme. Par exemple, en plus des réseaux dénoncés dans ce rapport, 365 comptes supplémentaires ont été identifiés comme appartenant à des réseaux précédemment retirés et ont donc été supprimés. Face à la hausse de ces nouveaux comportements fallacieux, nous continuerons d’adapter nos réactions, de renforcer notre capacité d’application des règles et de publier les résultats de nos enquêtes.

#### **Sûreté**

Nous continuons à développer et à adapter nos mesures de protection en investissant dans des systèmes automatisés pour détecter, bloquer et supprimer les comptes et les interactions artificielles, et en améliorant notre temps de réaction et notre réponse à l’évolution des menaces. En raison du caractère conflictuel des utilisateurs malveillants, nous allons modifier les données d’application des règles pour les faux comptes. Cela renforce notre engagement visant à retirer tout contenu ou toute activité cherchant à accroître artificiellement sa popularité sur la plateforme.

#### **Contenus publicitaires**

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des annonces sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Bien que le volume total de contenus publicitaires retirés pour non-respect de nos politiques publicitaires ait diminué lors de ce trimestre, le nombre de contenus publicitaires supprimés en raison d’actions au niveau du compte a augmenté. Cela est dû en partie à notre nouvelle approche concernant le non-respect de nos règles publicitaires et au renforcement de nos capacités d’application au niveau des comptes. Nous sommes conscients que, pour garantir la sécurité de nos annonceurs et de nos utilisateurs, nous devons fournir des efforts constants. Nous nous engageons donc à régulièrement analyser et renforcer nos systèmes de lutte contre les contenus publicitaires ne respectant pas nos politiques.