platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2019-1/

### Latest data

#### Government requests for content removal

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Country** | **Government requests** | **Total accounts specified** | **Accounts removed or restricted** | **Content removed or restricted** |
| Australia | 2   | 2   | 2   | 0   |
| France | 2   | 2   | 2   | 0   |
| Germany | 1   | 1   | 0   | 1   |
| India | 11  | 9   | 8   | 4   |
| Israel | 1   | 1   | 0   | 1   |
| Italy | 1   | 1   | 1   | 0   |
| Japan | 3   | 5   | 4   | 1   |
| United Kingdom | 1   | 1   | 1   | 0   |
| United States | 6   | 7   | 7   | 1   |

_Note: TikTok did not receive any government requests to remove or restrict content from countries other than those on the list above._