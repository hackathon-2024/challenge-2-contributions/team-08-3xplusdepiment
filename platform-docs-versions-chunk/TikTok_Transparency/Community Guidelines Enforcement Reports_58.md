platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-1/


### À propos de ce rapport

Des millions de personnes dans le monde se rassemblent sur TikTok pour exprimer pleinement leur créativité et se divertir. Nos équipes oeuvrent au quotidien à faire respecter nos [Règles communautaires](https://www.tiktok.com/community-guidelines), en supprimant les comptes et contenus qui ne les respecteraient pas, pour proposer un environnement sûr et bienveillant à notre communauté. Nous veillons à être transparents sur la façon dont nous appliquons nos politiques pour renforcer les liens de confiance avec les membres de notre communauté. Aussi, nous publions régulièrement ces rapports.

Désormais nous passerons à rythme de publication trimestriel – tous les trois mois au lieu de tous les six mois – pour rendre compte de l’application de nos Règles communautaires. En raison des contraintes liées au traitement des demandes, les informations liées aux requêtes soumises par les autorités publiques et gouvernements, ainsi que les demandes de retrait pour violation de la propriété intellectuelle continueront d’être publiées de façon semestrielle.

Nos Règles communautaires s’appliquent à tous les utilisateurs et à tous les contenus présents sur la plateforme. Nos équipes TikTok d’experts en sûreté, sécurité et politique, travaillent ensemble pour concevoir des règles équitables qui puissent être systématiquement appliquées. Nos politiques prennent en compte les retours issus d’experts externes en sécurité numérique et droits de l’Homme, et nous veillons à respecter les cultures des pays où nous opérons. En aucun cas nous n’élaborons des politiques à la demande expresse d’un gouvernement, d’un individu ou d’une organisation ; de plus, aucun collaborateur TikTok n’est autorisé à modifier les Règles communautaires de manière unilatérale. Notre principal objectif est que nos règles fassent de TikTok un espace bienveillant qui encourage la créativité et l’authenticité.

Ce rapport apporte de la visibilité sur le volume et la nature des contenus et des comptes retirés de la plateforme au cours des trois premiers mois de l’année 2021 (du 1er janvier au 31 mars). Pour la première fois, nous publions le nombre de comptes soupçonnés d’être détenus par des utilisateurs de moins de 13 ans, car nous nous efforçons de faire de TikTok un espace uniquement réservé aux personnes de plus de 13 ans.