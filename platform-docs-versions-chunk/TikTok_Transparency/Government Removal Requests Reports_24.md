platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2020-2/

### À propos de ce rapport

Les rubriques ci-dessous donnent un aperçu du volume et de la nature des demandes juridiques que nous avons reçues au cours du second semestre 2020 et de la façon dont nous les avons traitées. Nous recevons des demandes des pouvoirs publics et autorités juridiques dans le monde entier. Nous honorons les demandes qui nous sont adressées par les voies appropriées et lorsque la loi l’exige.

Quand nous recevons des demandes des autorités publiques visant à restreindre ou à supprimer des contenus sur notre plateforme, conformément aux lois locales, nous examinons tous les documents conformément à nos Règles Communautaires, nos Conditions de services, ainsi que la législation en vigueur, et prenons les mesures appropriées. Si nous estimons qu’une demande n’est pas légalement recevable ou qu’elle ne va pas à l’encontre de nos règles, nous pouvons choisir de limiter la diffusion du contenu signalé dans le pays où il est présumé illégal ou bien choisir de ne prendre aucune mesure.