platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-2/

### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des annonces sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Au cours du deuxième trimestre 2022, le volume total d’annonces supprimées pour non-respect de nos politiques et directives publicitaires a diminué. Cela est dû en partie à nos efforts pour renforcer les stratégies de détection et de gestion des comptes frauduleux, qui contribuent à améliorer notre écosystème publicitaire et à créer de meilleures expériences pour les utilisateurs et les annonceurs. Nous nous engageons à créer un environnement sûr et positif pour nos utilisateurs, et nous révisons et renforçons régulièrement nos systèmes pour combattre les publicités qui transgressent nos règles.