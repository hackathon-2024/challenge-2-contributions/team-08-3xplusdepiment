platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2019-2/

### Rapport sur les données

#### Demandes de retrait de contenu émanants d’autorités publiques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Demande de gouvernements** | **Total de comptes spécifiés** | **Comptes supprimés ou restreints** | **Contenus supprimés ou restreints** |
| Australie | 6   | 6   | 5   | 1   |
| Belgique | 1   | 1   | 0   | 0   |
| Canada | 1   | 8   | 0   | 8   |
| Chypre | 1   | 1   | 0   | 1   |
| Inde | 30  | 35  | 28  | 19  |
| Norvège | 1   | 5   | 5   | 0   |
| Pays-Bas | 1   | 78  | 78  | 0   |
| Sri Lanka | 1   | 1   | 1   | 0   |
| Turquie | 2   | 4   | 3   | 16  |
| États Unis | 1   | 1   | 1   | 0   |

_Remarque_ _: TikTok n’a reçu aucune demande de la part d’autorités publiques de retrait ou de restriction de contenu provenant de pays ou de marchés autres que ceux présents dans la liste ci-dessus._