platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-3/

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de sa communauté plurielle. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles communautaires ou nos Conditions d’utilisation ; par ailleurs, nous publions régulièrement des informations sur les mesures prises par notre société de manière à rendre compte à notre communauté et gagner sa confiance. TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles.

Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.