platform: TikTok
topic: Transparency
subtopic: Digital Services Act EU
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Digital Services Act EU.md
url: https://www.tiktok.com/transparency/en/eu-mau-2023-7/


### Information about TikTok’s Monthly Active Recipients number for the European Union

* * *

In accordance with our obligations under the Digital Services Act (Article 24(2)), TikTok Technology Limited reports that it had on average 134 million ‘monthly active recipients’ in the European Union member state countries between February 2023 and July 2023.

We have produced this calculation for the purposes of complying with our DSA requirements and it should not be relied on for other purposes. Where we have shared user metrics in other contexts, the methodology and scope may have differed.

Our approach to producing this calculation may evolve or may require altering over time, for example, because of product changes or new technologies.

#### Was this helpful?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Yes![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)No