platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-4/

### Sûreté

Nous continuons à tester et à améliorer nos capacités de sûreté face à l’évolution constante des menaces en investissant dans des défenses automatisées pour détecter, bloquer et supprimer les faux comptes ainsi que les fausses activités. Quasiment à chaque fois que nous endiguons des menaces, les acteurs malveillants font preuve de persistance et font évoluer dans leurs tactiques afin de contourner nos systèmes de détection et de contrôle. Par conséquent, les fluctuations trimestrielles du volume total de fausses activités contre lesquelles nous prenons des mesures peuvent varier. Cependant, nous restons déterminés à affiner constamment les politiques et les outils de contrôle que nous utilisons pour supprimer tout contenu ou activité visant à gonfler artificiellement la popularité sur la plateforme.