platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2019-1/

Demandes d’information

_January 1, 2019 – June 30, 2019  
Released: December 30, 2019_

### About this report

We take any request from government bodies extremely seriously, and closely review each such request we receive to determine whether, for example, the request adheres to the required legal process or the content violates a local law. TikTok is committed to assisting law enforcement in appropriate circumstances while respecting the privacy and rights of our users.

The sections below provide insight into the requests we received from government bodies in the markets where the TikTok app operates during the first half of 2019. We did not receive any requests from countries other than those listed in the chart below. In addition, we have also included a description of our copyright take-down process.