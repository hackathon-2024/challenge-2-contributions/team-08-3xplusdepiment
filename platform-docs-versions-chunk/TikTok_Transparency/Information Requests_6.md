platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2022-2/


### **À propos de ce rapport**

TikTok s’engage à répondre aux demandes de divulgation ou de préservation d’informations concernant les utilisateurs faites par les forces de l’ordre tout en respectant la vie privée et les droits des membres de sa communauté. Toute demande de renseignement reçue est soigneusement examinée au cas par cas. Nos [politiques et procédures](https://www.tiktok.com/legal/law-enforcement?lang=en) définissent les modalités de traitement et de réponse de TikTok aux demandes des forces de l’ordre et exigent que TikTok ne divulgue ou ne préserve les données des utilisateurs que lorsqu’une demande est motivée par une procédure judiciaire valide ou en cas d’urgence.

Dans des situations d’urgence exceptionnelles, TikTok divulguera les données personnelles des utilisateurs sans procédure légale. Cela peut se produire lorsque, en toute bonne foi, nous avons des raisons d’estimer que le partage d’informations est nécessaire face à un risque imminent de mort ou de préjudice physique grave. Pour en savoir plus sur nos politiques et nos pratiques, veuillez consulter nos [Directives sur les demandes de données par les forces de l’ordre](https://support.tiktok.com/fr/safety-hc/account-and-user-safety/law-enforcement-data-request-guidelines).

Les sections ci-dessous fournissent des précisions sur le volume et les types de demandes d’information sur les utilisateurs reçues et indiquent si les données ont été divulguées ou conservées.