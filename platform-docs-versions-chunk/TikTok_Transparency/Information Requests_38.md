platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2019-1/

### Other reports

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport sur les demandes de retrait de contenus protégés par la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non