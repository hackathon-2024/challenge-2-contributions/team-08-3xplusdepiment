platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2020-2/


### Synthèse

Nous faisons sans cesse évoluer nos politiques, nos produits et nos partenariats pour garantir le bien-être de notre communauté. Voici quelques-unes des principales mises à jour que nous avons effectuées au cours du second semestre 2020.

#### Garantir le bien-être de la communauté

Nous avons renforcé nos [Règles C](https://www.tiktok.com/community-guidelines?lang=en)[ommunautaires](https://www.tiktok.com/community-guidelines?lang=fr) afin de promouvoir le bien-être de la communauté, en prenant en compte les comportements observés sur la plateforme et les commentaires de nos utilisateurs et d’experts. Par exemple, la mise à jour de nos politiques relatives à l’automutilation, le suicide et les troubles alimentaires reflètent les recommandations émanant d’experts en santé mentale. Nos [politiques publicitaires](https://newsroom.tiktok.com/en-us/coming-together-to-support-body-positivity-on-tiktok) interdisent désormais les annonces publicitaires d’applications sur le jeûne et les compléments alimentaires pour la perte de poids. Nous avons également amélioré la façon dont nous [informons nos utilisateurs](https://newsroom.tiktok.com/en-us/adding-clarity-to-content-removals) afin de les aider à comprendre les raisons pour lesquelles leur vidéo a été supprimée.

TikTok s’est associée à différentes organisations pour venir en aide aux personnes qui pourraient être aux prises avec un trouble alimentaire, un comportement d’automutilation ou des pensées suicidaires. Désormais, les recherches de contenus et les hashtags liés à ces thématiques sont redirigés vers des lignes téléphoniques d’assistance telles que « Suicide Ecoute » ou « S.O.S Amitié » notamment, qui permettent aux utilisateurs d’accéder à une aide gratuite et confidentielle. Nous mettons en avant des conseils pour améliorer le bien-être émotionnel de la personne.

#### Accompagner les familles

Nous discutons régulièrement avec des parents, des adolescents et des experts en sécurité des jeunes afin de proposer aux familles des outils efficaces pour qu’elles bénéficient d’une expérience TikTok adaptée. Fin 2020, nous avons élargi les fonctionnalités de notre [Mode Connexion Famille](https://newsroom.tiktok.com/fr-fr/connexion-famille-mise-a-jour) pour permettre aux parents de renforcer les protections sur les contenus et les paramètres de confidentialité de leurs adolescents. Grâce à des dispositifs permettant de limiter les recherches, les commentaires et le temps d’écran, nous espérons que ces outils encourageront les familles à dialoguer plus ouvertement sur la sécurité numérique.