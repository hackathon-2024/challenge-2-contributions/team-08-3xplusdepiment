platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2019-1/

### Latest data

#### Copyrighted content take-down notices

|     |     |
| --- | --- |
| **Copyrighted content take-down notices** | **Percentage of requests where some content was removed** |
| 3345 | 85% |

_Note: Only copyright infringement take-down notices from copyright owners, their agencies, or attorneys are included in our copyrighted content take-down statistics._