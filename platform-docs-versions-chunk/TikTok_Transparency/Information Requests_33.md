platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2019-2/


### À propos de ce rapport

Les sections ci-dessous présentent le volume des demandes légales reçues des gouvernements et autorités locales dans le monde entier au cours du second semestre 2019 et la nature de notre réponse. Nous respectons les droits et la vie privée de nos utilisateurs. C’est pourquoi nous demandons aux autorités publiques de soumettre par écrit leur demande accompagnée des documents légaux appropriés. Nous ne prenons pas en compte des demandes qui ne sont effectuée par les voies appropriées.

Toute demande d’information que nous recevons est soigneusement examinée afin de déterminer si elle est justifiée d’un point de vue légal ; par exemple si l’entité requérante est autorisée à recueillir des éléments de preuve dans le cadre d’une enquête policière ou à enquêter sur une situation d’urgence présentant un danger imminent. Pour en savoir plus sur nos politiques et pratiques, veuillez consulter nos [Directives relatives aux demandes de données des services de lutte contre la criminalité](https://www.tiktok.com/legal/law-enforcement).