platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-1/


### Analyse

Les Règles Communautaires de TikTok sont définies pour favoriser une expérience divertissante qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus, et nous nous efforçons d’être cohérents et équitables dans leur application.

#### Réponse à la guerre en Ukraine

En tant que plateforme, notre réponse à la guerre en Ukraine a été de renforcer les mesures de protection et de sécurité afin de garantir que les utilisateurs puissent s’exprimer et partager leurs expériences. Aussi, nous nous efforçons de combattre les fausses informations qui portent préjudice et toute autre violation de nos politiques. La guerre nous a imposé de faire face à un monde complexe et en constante évolution. Nous avons ainsi augmenté nos investissements auprès de nos partenaires de vérification des faits et qui contribuent à évaluer l’exactitude des contenus. Nous avons adapté nos méthodes de détection afin de prendre des mesures plus rapides contre les comptes qui tentent d’escroquer ou d’induire en erreur notre communauté par le biais de Lives (vidéos en direct) et de contenus vidéo non originaux. Par ailleurs, nous avons lancé un projet pilote de notre politique relative aux médias contrôlés par un État en désignant par des étiquettes le contenu de certains comptes afin d’apporter des éléments de contexte importants à nos utilisateurs. En réponse à la loi russe sur les « fake news » et de ses conséquences sur la sécurité, nous n’avons eu d’autre choix, à partir du 6 mars, que de suspendre les diffusions en direct et les nouveaux contenus sur notre service vidéo en Russie. La sécurité de nos employés et de nos créateurs est notre priorité absolue. C’est pourquoi, les contenus diffusés par des comptes établis en dehors de la Russie ne sont actuellement pas accessibles en Russie.

Du 24 février au 31 mars 2022, nous avons pris les mesures suivantes pour protéger notre communauté :

* Notre équipe de sécurité, mobilisée sur la guerre en Ukraine, a retiré 41 191 vidéos, dont 87 % enfreignaient nos politiques de lutte contre la désinformation. La grande majorité (78 %) a été identifiée de manière proactive.
* Nos partenaires de vérification des faits ont contribué à l’analyse de 13 738 vidéos dans le monde.
* Nous avons ajouté des avertissements sur 5 600 vidéos pour signaler aux utilisateurs que le contenu ne pouvait pas être validé par nos partenaires de vérification.
* Nous avons identifié au moyen d’étiquettes le contenu de 49 comptes de médias contrôlés par l’État russe.
* Nous avons identifié et supprimé 6 réseaux et 204 comptes dans le monde pour cause de tentative coordonnée d’influencer l’opinion publique et de tromper les utilisateurs sur leur identité.

#### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des annonces sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Au cours du premier trimestre 2022, le volume total d’annonces supprimées pour non-respect de nos politiques et directives publicitaires a augmenté. Cela est dû à l’évolution de notre approche en matière d’application des règles relatives aux publicités et au renforcement de nos capacités de contrôle des comptes. Auparavant, tout compte jugé contraire à nos politiques entraînait le blocage de tout le trafic publicitaire associé à ce compte. Désormais, avec notre nouvelle approche, nous sommes en mesure de supprimer de nos systèmes internes toutes les publicités associées à ces comptes. Bien que le résultat final soit le même pour nos utilisateurs, nous estimons que cette mesure améliore l’écosystème publicitaire et témoigne de notre volonté de créer un environnement sûr et positif pour nos utilisateurs.