platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2020-1/

Demandes de retrait de contenu portant atteinte à la propriété intellectuelle

_Du 1e janvier au 30 juin 2020_  
_Mise en ligne: 22 Septembre 2020_