platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2022-1/


### **À propos de ce rapport**

TikTok repose sur la créativité de notre communauté et nous nous employons à encourager et protéger l’expression personnelle et le divertissement. Toutefois, dans certains cas, nous retirerons ou restreindrons le contenu signalé par des organismes publics. Nous traitons les demandes qui nous sont adressées par les voies appropriées et lorsque la loi l’exige. Lorsque nous recevons de telles demandes de la part des pouvoirs publics, nous les étudions et prenons des mesures relatives au contenu signalé dans le respect de nos [Règles communautaires](https://www.tiktok.com/community-guidelines), de nos [Conditions générales](https://www.tiktok.com/legal/terms-of-service) et des lois applicables. Si nous estimons qu’une demande n’enfreint pas les dispositions définies dans nos Règles communautaires mais enfreint la législation en vigueur, nous pourrons imposer des restrictions sur la diffusion du contenu signalé dans le pays où celui-ci est considéré comme contraire à la loi. Si nous estimons qu’une demande n’est pas justifiée sur le plan juridique ou qu’il n’y a pas infraction de nos Règles communautaires, de nos Conditions générales ou de la législation en vigueur, nous nous réservons le droit de rejeter ladite demande.

Les sections ci-dessous indiquent le volume des demandes de retrait ou de restriction reçues de gouvernements et la nature de la réponse donnée par TikTok.