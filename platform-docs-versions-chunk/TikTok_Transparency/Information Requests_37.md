platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2019-1/


### Latest data

#### Legal requests for user information

Any information request we receive is carefully reviewed for legal sufficiency, to determine whether, for example, the requesting entity is authorized to gather evidence in connection with a law enforcement investigation or to investigate an emergency involving imminent harm. For more on our policies and practices, please see our [Law Enforcement Data Request Guidelines](https://www.tiktok.com/legal/law-enforcement).

The following chart shows the number of information requests we received, by country, in the first half of 2019 (January 1 – June 30, 2019) and the rate with which we complied with those requests.

|     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- |
| **Country** | **Legal requests** | **Emergency requests** | **Total requests** | **Total accounts specified** | **Percentage of requests where some information was produced** |
| Australia | 5   | 0   | 5   | 5   | 0%  |
| Austria | 1   | 0   | 1   | 1   | 0%  |
| Belgium | 1   | 0   | 1   | 1   | 0%  |
| Canada | 0   | 1   | 1   | 1   | 100% |
| Czech Republic | 1   | 0   | 1   | 1   | 0%  |
| Denmark | 1   | 0   | 1   | 1   | 0%  |
| France | 5   | 3   | 8   | 17  | 12% |
| Germany | 9   | 3   | 12  | 12  | 25% |
| Hungary | 1   | 0   | 1   | 1   | 0%  |
| Iceland | 0   | 1   | 1   | 1   | 100% |
| India | 99  | 8   | 107 | 143 | 47% |
| Israel | 0   | 3   | 3   | 3   | 100% |
| Italy | 2   | 1   | 3   | 3   | 33% |
| Japan | 28  | 7   | 35  | 39  | 21% |
| Jordan | 1   | 0   | 1   | 1   | 0%  |
| South Korea | 4   | 2   | 6   | 6   | 33% |
| New Zealand | 1   | 0   | 1   | 1   | 0%  |
| Norway | 6   | 5   | 11  | 15  | 33% |
| Pakistan | 1   | 0   | 1   | 1   | 0%  |
| Poland | 2   | 0   | 2   | 3   | 0%  |
| Russia | 1   | 0   | 1   | 1   | 0%  |
| Serbia | 1   | 0   | 1   | 1   | 0%  |
| Singapore | 1   | 0   | 1   | 1   | 100% |
| Sweden | 4   | 2   | 6   | 6   | 17% |
| Switzerland | 1   | 0   | 1   | 1   | 0%  |
| Turkey | 0   | 1   | 1   | 1   | 100% |
| United Kingdom | 6   | 0   | 6   | 7   | 0%  |
| United States | 68  | 11  | 79  | 255 | 86% |

_Note: TikTok did not receive any legal requests for account information from countries other than those on the list above._