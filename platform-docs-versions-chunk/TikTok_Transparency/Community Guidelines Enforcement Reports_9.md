platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-2/


### À propos de ce rapport

Chez TikTok, notre mission est d’inspirer la créativité et de divertir. La sécurité et le bien-être de notre communauté sont notre priorité, et nous avons plus de 40 000 professionnels de la sécurité qui travaillent pour protéger nos utilisateurs.

Ce rapport reflète la mise à jour de nos règles communautaires, qui sont entrées en vigueur en avril. Elles offrent à notre communauté une plus grande transparence sur nos règles et la manière dont nous les appliquons. Après avoir consulté plus de 100 organisations dans le monde, nous avons revu la manière dont nous organisons nos politiques, simplifié le langage que nous utilisons et ajouté de la granularité pour aider, notamment les créateurs ou bien encore les chercheurs, à accéder facilement aux informations dont ils ont besoin. Nous avons également rafraîchi plusieurs visualisations de données pour les rendre plus lisibles et compréhensibles, y compris pour les personnes qui souffrent d’une déficience de la vision des couleurs.

Ces mises à jour importante soulignent notre engagement à respecter les valeurs de confiance, d’authenticité et de responsabilité qui sont au cœur de notre communauté TikTok.

* * *