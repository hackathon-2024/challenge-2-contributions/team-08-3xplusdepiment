platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-3/


### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhaeh7pflk/Transparency_CGE_2021_Q3/French_CGE_2021Q3.xlsx)

#### Nombre total de vidéos supprimées/remises en ligne, par type et par trimestre

_REMARQUE : Le total de videos supprimées représente approximativement 1% de l’ensemble des vidéos publiées sur TikTok._

#### Nombre total de vidéos supprimées par catégorie de règle

_REMARQUE : Ce graphique montre le volume de vidéos supprimées par règle enfreinte. Une vidéo peut enfreindre plusieurs règles et chaque violation est comptabilisée._

#### Taux de suppressions, par trimestre/règle

_REMARQUE : lasuppression proactive signifie qu’une vidéo eninfraction est identifiée etsupprimée avant qu’elle ne nous soitsignalée. La suppression dans les 24heures signifie que la vidéoest supprimée dans les 24 heures suivant sapublication sur notre plateforme._

#### Suppressions par pays

|     |     |
| --- | --- |
| **Pays / Marché** | **1er juillet – 30 septembre 2021** |
| Etats- Unis | 13,918,537 |
| Russie | 6,881,904 |
| Indonésie | 6,173,414 |
| Pakistan | 6,019,754 |
| Brésil | 5,906,859 |

_REMARQUE : Ce tableau mentionne les cinq marchés avec le plus grand nombre de vidéos supprimées._

#### Nombre total de comptes supprimés, par trimestre et par motif

 _REMARQUE : nousprocédons à la suppression des comptes qui enfreignent les Règlescommunautaires et les Conditions d’utilisation, y compris les comptes dont lesactivités de spam sont avérées, ainsi que les vidéos de spam publiées par cescomptes. Nous prenonségalement des mesures proactives pour empêcher lacréation de comptes spam par des moyens automatisés._

#### Fausse activité

|     |     |
| --- | --- |
|     | **1er juillet – 30 septembre 2021** |
| Comptes spam évités | 226,557,055 |
| Suppressions de vidéos publiées par des comptes spam | 11,895,555 |
| Suppressions de faux abonnés | 231,150,220 |
| Demandes de suivi par de faux comptes évitées | 2,078,453,724 |
| Suppressions de faux « j’aime » | 203,708,379 |
| Faux « j’aime » évités | 16,594,976,202 |

#### Application des règles concernant les contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus publicitaires mensongers, frauduleux ou trompeurs, y compris les publicités. Les comptes d’annonceurs et les contenus publicitaires sont soumis à ces règles et doivent se conformer à nos Règles Communautaires, nos Règles Publicitaires ainsi qu’à nos Conditions d’utilisation. Nous nous engageons à créer un environnement sûr et positif pour nos utilisateurs, et nous examinons et renforçons régulièrement nos systèmes pour lutter contre les publicités qui transgressent nos règles.

|     |     |
| --- | --- |
|     | **1er juillet – 30 septembre 2021** |
| Total des publicités supprimées | 3,392,630 |

#### COVID-19

|     |     |
| --- | --- |
| **Application des Règles communautaires** | **1er juillet – 30 septembre 2021** |
| Suppressions de vidéos pour désinformation sur la COVID-19 et la vaccination | 46,577 |
| Taux de suppression proactive | 82.86% |
| Taux de suppression à zéro vue | 69.42% |
| Taux de suppression dans les 24 heures | 84.62% |

|     |     |
| --- | --- |
| **Connexions à des sources d’information fiables** | **1er juillet – 30 septembre 2021** |
| Visites du [centre d’information TikTok dédié à la COVID-19](https://www.tiktok.com/safety/fr-fr/covid-19/) | 2,883,650 |
| Ajouts de bannières COVID-19 aux vidéos | 3,560,364 |
| Nombre de bannières sur la COVID-19 visualisées | 22,631,317,279 |
| Nombre de messages d’intérêt public visualisés du hashtag COVID-19 | 31,007,383,296 |