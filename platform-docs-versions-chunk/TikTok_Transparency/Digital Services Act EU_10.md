platform: TikTok
topic: Transparency
subtopic: Digital Services Act EU
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Digital Services Act EU.md
url: https://www.tiktok.com/transparency/en/tco-report/


### About this report

This report is published by TikTok Technology Limited (hereafter “**TikTok**”) under the EU Terrorist Content Online Regulation (EU) 2021/784 (“**TCO Regulation**”). 

#### A. Combatting violent extremism

TikTok takes an uncompromising stance against enabling violent extremism on or off our platform. Our mission is to inspire creativity and bring joy to people, and any attempt to promote violence runs counter to that mission and our values. TikTok thrives because of the diversity of our global community, and we aim to provide a safe space where people feel welcomed and empowered to express themselves and connect with others. That’s why we take a strong stance against attempts to promote violence or hateful behaviour on TikTok.

Terrorist content has no place on TikTok, and we will remove any content – including video, audio, livestream, images, comments, links, or other text – that violates our Community Guidelines. When there is a threat to public safety or an account is used to promote or glorify off-platform violence, we ban the account.

#### B. Information about TikTok’s measures to tackle terrorist content

##### 1\. How we define violent extremist content 

As explained in our [Community Guidelines](https://www.tiktok.com/community-guidelines?lang=en#39), we do not allow people to use our platform to threaten or incite violence, or to promote violent extremism.  

**Threats and incitement to violence**

We consider incitement to violence as advocating for, directing, or encouraging other people to commit violence. We do not allow threats of violence or incitement to violence on our platform that may result in serious physical harm. This includes but is not limited to 

* Statements of intent to inflict physical injuries on an individual or a group
* Statements or imagery that encourage others to commit or that advocate for violence
* Conditional or aspirational statements that encourage other people to commit violence
* Calls to bring weapons to a location with the intent to intimidate or threaten an individual or group with violence
* Instructions on how to make or use weapons that may incite violence

**Violent extremist organisations and individuals** 

We do not allow organisations or individuals on our platform who promote or engage in violence, including terrorist organisations, organised hate groups, criminal organisations, and other non-state armed groups that target civilians: 

* We define terrorists and terrorist organisations as non-state actors that threaten violence, use violence, and/or commit serious crimes (such as crimes against humanity) against civilian populations in pursuit of political, religious, ethnic, or ideological objectives. 
* We use the term “organised hate” to refer to those individuals and organisations who attack people based on protected attributes, such as race, ethnicity, national origin, religion, caste, sexual orientation, sex, gender, gender identity, or immigration status. We consider attacks to include actions that incite violence or hatred, that aim to dehumanize individuals or groups, or that embrace a hateful ideology.
* We define criminal organizations are transnational, national, or local groups that have engaged in serious crimes, including violent crimes (e.g., homicide, rape, robbery, assault), trafficking (e.g., human, organ, drug, weapons), kidnapping, financial crimes (e.g., extortion, blackmail, fraud, money laundering), or cybercrime.

This includes but is not limited to: 

* Content that praises, promotes, glorifies, or supports violent acts or extremist organizations or individuals
* Content that encourages participation in, or intends to recruit individuals to, violent extremist organisations
* Content with names, symbols, logos, flags, slogans, uniforms, gestures, salutes, illustrations, portraits, songs, music, lyrics, or other objects meant to represent violent extremist organisations or individuals

##### 2\. How we identify violent extremist content

To enforce our rules against violent extremism, we use a combination of technology, safety experts and security professionals, alongside threat-detection partners.

* We use computer vision models to help detect visual signals, emblems, logos that are known to be associated with extremist and hate groups, so that we can remove such content.
* We use text-based technologies, including keyword lists and natural language processing (NLP), to detect language used to promote extremist ideologies or hate groups. This enables us to find near or exact matches of terms such as slurs (or even emoji combinations) and to remove them from comments, video captions, and profile descriptions.
* Where we have previously detected content that violates our policies on hate or extremism, we use de-duplication and hashing technologies that enable us to recognize copies or near copies of such content. We work with external groups, such as [Tech Against Terrorism](https://www.tiktok.com/transparency/en-us/combatting-hate-violent-extremism/), who help us to more quickly detect and remove hate or violent extremist content that has already been identified off the platform.
* We also take measures to disrupt the discoverability of such content on the platform. One way that we do this is by blocking searches for terms related to hateful keywords, or names and organisations associated with hate and violent extremism.
* In limited emergency situations, we will disclose user information when we have reason to believe, in good faith, that the disclosure of information is necessary to prevent the imminent risk of death or serious physical injury to any person. For more on our policies and practices, please see our [TikTok Law Enforcement Guidelines](https://www.tiktok.com/legal/page/global/law-enforcement/en).

In addition, we encourage our community members to use the [tools we provide on our platform](https://support.tiktok.com/en/safety-hc/report-a-problem) to report any content or account they believe violates our Community Guidelines, which includes our policies on violent extremist content as outlined above. In Germany, Austria and France we have introduced content reporting mechanisms that enable users to report certain content to us alleging violations of local laws (which includes certain local criminal law provisions related to terrorist content). For example: In Germany users can report certain content to us alleging violations such Sections [129a](https://www.gesetze-im-internet.de/stgb/__129a.html) (Forming terrorist organisations) and [129b](https://www.gesetze-im-internet.de/stgb/__129b.html) (Foreign criminal and terrorist organisations) of Germany’s Criminal Code. 

##### 3\. Operational expertise

We understand that violent extremism and hateful behaviour are complex and ever-evolving online harms, which is why we continually look for how we can improve our policies and strengthen our enforcement. This includes regularly training for our content moderation teams to better detect evolving hateful behaviour, symbols, terms, and offensive stereotypes. We also consult academics and experts from across the globe to keep abreast of evolving trends and to help us regularly evaluate our practices. We take into account publicly available information from organisations, including the United Nations Security Council and the European Union, to designate dangerous or hateful individuals and organisations, including terrorist groups.

##### 4\. Standing together against violent extremism

At TikTok, we believe that collaboration is critical to solving today’s most pressing challenges, including violent extremism. We are proud to be members of Tech Against Terrorism, which brings together technology companies, civil society, and academics over the shared goal of countering violent extremism. Coming together with others in the industry reflects our desire to engage with the wider community of experts in a responsible way. Our aim is to help eradicate terrorist content online and share learnings with others along the way.

#### **C. Numbers relating to removal of terrorist content** 

The numbers below cover the reporting period: 7 June, 2022 – 31 December, 2022.

##### **1\. Orders from competent authorities** 

TikTok did not receive any removal orders from competent authorities under the TCO Regulation. 

##### **2\. Community Guidelines and local law violations** 

Below are the numbers for the items of terrorist content removed by TikTok for violations of our Community Guidelines on violent extremism relating to terrorist content. The numbers in brackets indicate the items of terrorist content removed and/or in relation to which access was disabled for violations of local laws relating to violent extremism in Germany (Sections 129a and 129b of the German Criminal Code), Austria (Sections 278b, 278f and § 282a of the Austrian Criminal Code), and France (Section 421-2-5 of the French Criminal Code):

|     |     |     |
| --- | --- | --- |
| **Total number of removals (restrictions)** | **Number of user appeals** | **Number of appeals where content was reinstated** |
| 53385 (3746) | 11816 (975) | 6153 (244) |

\*The numbers in brackets are a subset of the total number and included for reference only.

#### Was this helpful?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Yes![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)No