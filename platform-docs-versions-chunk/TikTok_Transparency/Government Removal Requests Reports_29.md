platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2020-1/

### Rapport sur les données

#### Demandes de retrait partiel de contenu émanant d’autorités publiques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Demande de gouvernements** | **Total de comptes spécifiés** | **Comptes supprimés ou restreints** | **Contenus supprimés ou restreints** |
| Australie | 13  | 14  | 8   | 5   |
| Belgique | 10  | 10  | 2   | 8   |
| Canada | 1   | 1   | 0   | 1   |
| Allemagne | 4   | 4   | 1   | 3   |
| Danemark | 1   | 1   | 0   | 1   |
| Inde | 55  | 244 | 8   | 225 |
| Norvège | 14  | 24  | 17  | 1   |
| Nouvelle Zélande | 1   | 1   | 0   | 1   |
| Pakistan | 4   | 40  | 2   | 129 |
| Russie | 15  | 259 | 9   | 296 |
| Singapour | 1   | 2   | 2   | 0   |
| Turquie | 9   | 36  | 2   | 34  |
| Taiwan | 1   | 6   | 0   | 0   |
| Royaume-Uni | 2   | 5   | 2   | 3   |
| Etats-Unis | 4   | 4   | 2   | 4   |