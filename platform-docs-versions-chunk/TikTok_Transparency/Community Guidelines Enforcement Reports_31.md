platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-3/


### **Données les plus récentes**

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_CGE_2022Q3/French_CGE_2022Q3.xlsx)

#### **Nombre total de vidéos supprimés / total de vidéos par trimestre**

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok. le nombre de vidéos supprimées mentionné dans ce rapport comprend le contenu vidéo au format court (notamment les vidéos et les stories contenant des photos)._

#### **Nombre total de vidéos supprimées / remises en ligne, par type et trimestre**

_REMARQUE : dans le tableau ci-dessous, les vidéos remises en ligne sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne._

#### **Nombre total de suppressions de vidéos, par règle et politique**

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas néanmoins rares, comme lors de situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### **Taux de suppression, par trimestre / règle**

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### **Nombre total de suppressions et taux de vidéos, par sous-règle et politique**

_REMARQUE: Seules les vidéos qui ont été vérifiées par des modérateurs sont incluses dans le tableau de bord des différentes règles et dispositions._

#### **Volume et taux** **de** **suppression,** **par** **pays**

_REMARQUE : ce graphique dresse la liste des cinquante pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 90 % du volume total des suppressions._

#### **Nombre total de comptes supprimés, par trimestre et par motif**

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### **Fausse activité**

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « abonnés » et les demandes « d’abonnement » si nous estimons que l’activité est automatisée ou frauduleuse._

#### **Activité des comptes spam**

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

|     |     |     |     |
| --- | --- | --- | --- |
| Réseaux d’opérations d’influence cachées identifiés et supprimés au troisième semestre 2022 | Source de la détection | Comptes du réseau | Abonnés du réseau |
| Ce réseau semblait opérer depuis la Russie et cibler les pays européens, principalement l’Allemagne, l’Italie et le Royaume-Uni. Les personnes derrière ces comptes ont créé de faux comptes localisés et ont partagé du contenu en allemand, en italien et en anglais grâce à la synthèse vocale, présentant un fort point de vue pro-russe ciblant des discours sur la guerre en Ukraine. | Interne | 1 686 | 133 564 |
| Ce réseau semblait opérer depuis Taïwan et cibler principalement les audiences taïwanaises. Les personnes derrière ces comptes ont utilisé de faux comptes pour écrire de nombreux commentaires désobligeants et partager du contenu en chinois traditionnel ciblant les discours civiques à Taïwan. | Interne | 59  | 60 765 |
| Ce réseau semblait opérer depuis le Kenya et cibler principalement les audiences du Kenya. Les personnes derrière ces comptes ont partagé du contenu en anglais via de faux comptes et ont utilisé des comportements frauduleux pour toucher une audience, notamment la manipulation des médias, ciblant des discours relatifs aux élections au Kenya. | Externe | 14  | 50 200 |
| Ce réseau semblait principalement opérer depuis l’Espagne et cibler les audiences des États-Unis. Les personnes derrière ces comptes ont créé et amplifié du contenu civique partisan des États-Unis en anglais et en espagnol via des comptes se définissant comme des partis politiques, redirigeant les utilisateurs vers des sites externes à la plateforme grâce à des liens de collecte de fonds, des liens de marchandise et des canaux d’information fermés, probablement dans le but de générer un bénéfice financier en ciblant des discours civiques aux États-Unis. | Interne | 104 | 1 731 144 |
| Ce réseau semblait opérer depuis la Géorgie et cibler les audiences russophones principalement basées au Kazakhstan, en Biélorussie et en Ukraine. Les personnes derrière ces comptes ont partagé du contenu en russe via de faux comptes se présentant comme de nouveaux points de vente, présentant un fort point de vue pro-russe ciblant des discours sur la guerre en Ukraine. | Interne | 18  | 85 068 |

**Définitions**

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

#### **Application des règles concernant les contenus publicitaires**

_REMARQUE : Les contenus peuvent être supprimés individuellement ou en bloc en appliquant des mesures à l’ensemble d’un compte d’annonceur._