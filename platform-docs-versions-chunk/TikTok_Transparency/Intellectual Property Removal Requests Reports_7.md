platform: TikTok
topic: Transparency
subtopic: Intellectual Property Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Intellectual Property Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2022-2/


### **Analyse**

Par rapport à la dernière période de rapport, nous avons constaté une augmentation du nombre de demandes de retrait fondées sur une infraction aux lois sur le droit d’auteur et sur les marques. Nous pensons que cela est dû en partie à la tenue d’événements sportifs très médiatisés (par exemple la Coupe du monde de la FIFA), à nos efforts destinés à améliorer notre engagement avec les détenteurs de droits autour du partage des bonnes pratiques pour la soumission de demandes valides et à notre travail pour améliorer l’efficacité de nos processus de vérification des e-mails. Par exemple, nous demandons désormais aux utilisateurs de procéder à une vérification de leur adresse e-mail avant de pouvoir soumettre un rapport, ce qui a permis de réduire le volume de spams et de rapports non valides que nous recevons. En outre, nous avons constaté une diminution du nombre de demandes honorées en matière de suppressions liées aux droits d’auteur et aux lois sur les marques par rapport à la période précédente. Nous pensons que cela peut être attribué en partie à nos efforts pour déployer de nouveaux outils qui améliorent notre capacité à détecter les faux propriétaires et les demandes multiples de suppression d’un même contenu.