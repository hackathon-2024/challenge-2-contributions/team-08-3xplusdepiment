platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-3/


## **Contenus publicitaires**

Les comptes d’annonceurs et le contenu des publicités doivent être conformes à nos Règles Communautaires, [Politiques Publicitaires](https://ads.tiktok.com/help/article/tiktok-advertising-policies-industry-entry) et nos Conditions d’utilisation. TikTok a mis en place des politiques strictes pour protéger sa communauté des contenus publicitaires faux, frauduleux ou trompeurs. Garantir la sécurité des utilisateurs et des annonceurs est un engagement permanent. Au cours du troisième trimestre de 2023, nous avons constaté une augmentation de contenus publicitaires retirés pour non-respect de nos Politiques Publicitaires et une diminution du nombre de contenus publicitaires supprimés en raison d’actions au niveau du compte. Nous revoyons et renforçons continuellement nos systèmes afin d’identifier de nouveaux modèles et de supprimer rapidement et avec précision les contenus publicitaires qui enfreignent nos politiques. En appliquant des politiques strictes, en tirant parti de mécanismes de détection avancés et en améliorant continuellement nos systèmes, nous nous efforçons de favoriser une expérience publicitaire sûre, agréable et conforme aux valeurs de notre passionnante communauté TikTok.

#### **Application des règles concernant les contenus publicitaires**