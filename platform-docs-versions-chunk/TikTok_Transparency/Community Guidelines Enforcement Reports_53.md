platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-2/

### À propos de ce rapport

TikTok est la plateforme qui permet, où que l’on se trouve dans le monde, de se divertir grâce à des contenus authentiques et créatifs. Nous nous efforçons de favoriser une expérience sûre et inclusive pour les créateurs ainsi que les utilisateurs en prenant des mesures contre les contenus ou les comptes qui enfreignent nos [Règles Communautaires](https://www.tiktok.com/community-guidelines?lang=fr).

Le présent Rapport sur l’application des [Règles Communautaires](https://www.tiktok.com/community-guidelines?lang=fr) met en évidence le contenu et les comptes supprimés au cours du second trimestre 2021 (avril-juin) pour avoir enfreint nos règles communautaires. Nous publions également des informations supplémentaires sur le volume de fausses activités auxquelles nous avons mis fin ou supprimées dans le cadre de nos efforts pour préserver l’authenticité de TikTok. En outre, ce rapport comprend désormais les chiffres relatifs aux contenus supprimés avant d’avoir été visionnés.