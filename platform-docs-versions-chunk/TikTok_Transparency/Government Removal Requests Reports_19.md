platform: TikTok
topic: Transparency
subtopic: Government Removal Requests Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Government Removal Requests Reports.md
url: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2021-1/

Demandes de retrait émanant des autorités publiques

_1er janvier 2021-30 juin 2021  
__Publication : 2 décembre 2021_