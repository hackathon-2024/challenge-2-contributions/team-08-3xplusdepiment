platform: TikTok
topic: Transparency
subtopic: Community Guidelines Enforcement Reports
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Community Guidelines Enforcement Reports.md
url: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-2/

Application des Règles communautaires

_1e avril 2023 – 30 juin 2023_

_publié le 12 octobre 2023_