platform: TikTok
topic: Transparency
subtopic: Information Requests
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Transparency/Information Requests.md
url: https://www.tiktok.com/transparency/fr-fr/information-requests-2022-1/


### Définitions

* **Demandes légales :** demandes de communication des données des utilisateurs émanant des forces de l’ordre.
* **Demandes d’urgence :** demandes des forces de l’ordre faisant référence à une situation d’urgence et concernant la communication des données des utilisateurs.
* **Demandes de préservation des données :** demandes de préservation des données des utilisateurs émanant des forces de l’ordre.
* **Total des demandes :** Somme desdemandes légales, des demandes d’urgence et des demandes de préservation des données émanant des forces de l’ordre.
* **Pourcentage de demandes légales où certaines données ont été communiquées :** taux des demandes légales ayant abouti à une quelconque communication des données des utilisateurs.
* **Pourcentage de demandes d’urgence où certaines données ont été communiquées :** taux des demandes d’urgence ayant abouti à une quelconque communication des données des utilisateurs.
* **Compte concerné par la demande légale :** les identifiants de compte (par exemple, le numéro de téléphone, l’adresse e-mail, les URL des vidéos ou le nom d’utilisateur) envoyés par les forces de l’ordre dans le cadre d’une demande légale. Si les forces de l’ordre fournissent plusieurs identifiants de compte liés au même compte TikTok, cela est comptabilisé comme 1 compte concerné.
* **Compte concerné par la demande d’urgence :** les identifiants de compte (par exemple, le numéro de téléphone, l’adresse e-mail, les URL des vidéos ou le nom d’utilisateur) envoyés par les forces de l’ordre dans le cadre d’une demande d’urgence. Si les forces de l’ordre fournissent plusieurs identifiants de compte liés au même compte TikTok, cela est comptabilisé comme 1 compte concerné.
* **Compte concerné par la demande de préservation de données :** les identifiants de compte (par exemple, le numéro de téléphone, l’adresse e-mail, les URL des vidéos ou le nom d’utilisateur) envoyés par les forces de l’ordre dans le cadre d’une demande de préservation de données. Si les forces de l’ordre fournissent plusieurs identifiants de compte liés au même compte TikTok, cela est comptabilisé comme 1 compte concerné.