platform: Snapchat
topic: Transparency
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency/european-union

Union européenne

Dernière mise à jour : 25 octobre 2023

Bienvenue sur notre page de transparence pour l'Union européenne (UE), où nous publions des informations spécifiques à l'UE requises par le règlement sur les services numériques (DSA) de l'UE.  

Bénéficiaires actifs mensuels moyens 

Au 1er août 2023, nous comptions 102 millions de bénéficiaires actifs mensuels moyens (« AMAR ») de notre application [Snapchat](http://www.snapchat.com/?lang=fr-FR) dans l’UE. Cela signifie qu'en moyenne sur les 6 derniers mois, 102 millions d'utilisateurs enregistrés dans l'UE ont ouvert l'application Snapchat au moins une fois au cours d'un mois donné.

Ces chiffres se décomposent comme suit par État-membre :

| État membre | AMAR de février à juillet |
| --- | --- |
| Autriche | 2,114,634 |
| Belgique | 3,419,546 |
| Bulgarie | 866,540 |
| Croatie | 710,953 |
| Chypre | 308,768 |
| République Tchèque | 1,236,895 |
| Danemark | 2,779,541 |
| Estonie | 278,558 |
| Finlande | 1,919,110 |
| France | 28,817,327 |
| Allemagne | 21,084,985 |
| Grèce | 1,291,141 |
| Hongrie | 1,223,300 |
| Irlande | 2,093,791 |
| Italie | 4,618,477 |
| Lettonie | 315,356 |
| Lituanie | 538,132 |
| Luxembourg | 296,619 |
| Malte | 95,859 |
| Pays-Bas | 7,635,391 |
| Pologne | 6,167,537 |
| Portugal | 1,239,238 |
| Roumanie | 2,810,219 |
| Slovaquie | 592,399 |
| Slovénie | 444,642 |
| Espagne | 4,499,952 |
| Suède | 4,574,610 |
| TOTAL | 101,973,520 |

Ces chiffres servent à répondre aux règles actuelles du DSA et ne doivent être utilisés qu’à des fins de DSA. Nous pouvons modifier la manière dont nous évaluons ce chiffre au fil du temps, y compris en réponse à l’évolution des recommandations des régulateurs et de la technologie. Cela peut également différer des estimations utilisées pour d’autres chiffres d’utilisateur actif que nous publions à d’autres fins.

Représentant légal 

Snap Group Limited a désigné Snap B.V. comme son représentant légal aux fins du DSA. Vous pouvez contacter le représentant sur dsa-enquiries \[at\] snapchat.com, via notre Site d’assistance \[[ici](https://help.snapchat.com/hc/requests/new?start=5749439348080640%3Futm_source%3Dweb&utm_medium=snap&utm_campaign=transparency&lang=fr-FR)\], ou à :

Snap B.V.  
Keizersgracht 165, 1016 DP  
Amsterdam, Pays-Bas

Si vous êtes un organisme chargé de l’application de la loi, veuillez suivre les étapes décrites [ici](https://values.snap.com/fr-FR/en-GB/safety/safety-enforcement).

Autorités de régulation

Pour le DSA, nous sommes réglementés par la Commission européenne et l’Autorité néerlandaise pour les consommateurs et les marchés (ACM). 

Rapport sur la transparence du DSA

Snap est tenu, en vertu des articles 15, 24 et 42 du DSA, de publier des rapports renfermant des informations prescrites sur la modération de contenu de Snap pour les services de Snapchat qui sont qualifiées de « plateformes en ligne », c’est-à-dire Spotlight, Pour vous, les profils publics, les cartes, les Lenses et la publicité. Ce rapport doit être publié tous les 6 mois, à partir du 25 octobre 2023.

Snap publie des rapports sur la transparence deux fois par an pour fournir des informations sur les efforts de sécurité de Snap, ainsi que sur la nature et le volume du contenu signalé sur notre plateforme. Notre dernier rapport pour le premier semestre 2023 (1er janvier - 30 juin) est disponible [ici](https://values.snap.com/fr-FR/privacy/transparency?lang=en-US). Ce rapport comprend les informations suivantes :

* Les demandes gouvernementales, ce qui inclut les demandes d'informations et de suppression de contenu ; 
    
* Les violations de contenu, ce qui inclut les mesures prises en relation avec le contenu illégal et le temps de réponse moyen ; 
    
* Les appels, qui sont reçus et traités dans le cadre de notre processus de traitement des plaintes interne.
    

Ces sections sont pertinentes pour les informations requises par l’article 15.1(a), (b) et (d) du DSA. Notez qu’ils ne comportent pas encore d’ensemble de données complet, car le dernier rapport couvre le premier semestre 2023, ce qui est antérieur à l’entrée en vigueur du DSA. 

Nous fournissons ci-dessous quelques informations supplémentaires sur les aspects non couverts par notre rapport sur la transparence pour le premier semestre 2023 :

Modération de contenu (Article 15.1(c) et (e), Article 42.2)

Tous les contenus sur Snapchat doivent adhérer à nos [Règles communautaires](https://values.snap.com/fr-FR/privacy/transparency/community-guidelines) et à nos [Conditions d'utilisation du service](https://snap.com/en-US/terms?lang=en-US) ainsi qu'aux conditions, règles et explications y afférentes. Les mécanismes de détection proactifs et les signalements de contenu ou de comptes illégaux ou en violation de la loi incitent à un examen, auquel cas, nos systèmes d'outils traitent la demande, recueillent les métadonnées pertinentes et acheminent le contenu à notre équipe de modération via une interface utilisateur structurée conçue pour faciliter les opérations d'examen efficaces et efficientes. Lorsque nos équipes de modération déterminent, soit par un examen humain ou des moyens automatisés, qu'un utilisateur a enfreint nos Conditions, nous pouvons supprimer le contenu ou le compte en infraction, résilier ou limiter la visibilité du compte en question, et/ou aviser les forces de l'ordre comme indiqué dans notre [présentation sur la modération, l'exécution et les appels de Snapchat](https://values.snap.com/fr-FR/privacy/transparency/community-guidelines/moderation).  Les utilisateurs dont les comptes sont verrouillés par notre équipe de sécurité en raison de violations des Règles communautaires peuvent soumettre un [appel sur le compte verrouillé](https://help.snapchat.com/hc/en-us/articles/17988958753684-How-to-Submit-a-Locked-Account-Appeal?lang=fr-FR#:~:text=Locked%20account%20appeals%20can%20be,to%20start%20the%20appeal%20process.), et les utilisateurs peuvent [faire appel de certaines mesures d'application de contenu](https://help.snapchat.com/hc/en-us/articles/18120518120340-How-to-Submit-a-Content-Appeal?lang=fr-FR).

Outils de modération de contenu automatisés

Sur nos surfaces de contenu public, le contenu passe généralement à la fois par une modération automatique et un examen humain avant de pouvoir être diffusé à un large public. Les outils automatisés sont :

* Détection proactive des contenus illégaux et en violation de l'utilisation de l'apprentissage automatique ;
    
* Des outils de hachage (comme PhotoDNA et le CSAI Match de Google) ;
    
* Détection de langage abusif pour refuser le contenu sur la base d'une liste de mots-clés abusifs identifiée et régulièrement mise à jour, y compris les emojis.
    

Pendant la période de notre dernier [Rapport sur la transparence](https://values.snap.com/fr-FR/privacy/transparency?lang=en-US) (S1 2023), il n'était pas nécessaire de rassembler les indicateurs formels/les taux d'erreur pour ces systèmes automatisés. Toutefois, nous surveillons régulièrement ces systèmes pour détecter les problèmes et nos mesures de modération humaine sont régulièrement conçues pour des raisons de précision.

  
Modération humaine

Notre équipe de modération de contenu opère dans le monde entier, ce qui nous permet de garantir la sécurité des Snapchatters 24h/24, 7j/7. Ci-dessous, vous trouverez la répartition de nos ressources de modération humaine par les spécialités linguistiques des modérateurs (veuillez noter que certains modérateurs se spécialisent dans plusieurs langues) à partir d'août 2023 :

| Assistance linguistique | Nombre de personnes |
| --- | --- |
| Anglais | 1,011 |
| Arabe | 529 |
| Français | 250 |
| Deutsch | 72  |
| Español | 70  |
| Danois | 23  |
| Nederlands | 24  |
| Finnois | 12  |
| Hébreu | 1   |
| Hindi | 29  |
| Indonésien | 7   |
| Italiano | 18  |
| Japonais | 4   |
| Mandarin | 4   |
| Norvégien | 32  |
| Polski | 4   |
| Portugais | 36  |
| Pendjabi | 15  |
| Roumain | 4   |
| Russe | 10  |
| Suédois | 21  |
| Tagalog | 5   |
| Tamoul | 7   |
| Turc | 7   |
| Ukrainien | 3   |
| Total | 2198 |

Les chiffres ci-dessus fluctuent, car nous affichons les tendances en termes de volume entrant ou les soumissions par langue/pays. Dans les situations où nous avons besoin d’une assistance linguistique supplémentaire, nous utilisons des services de traduction.

Les modérateurs sont recrutés sur la base d’une description de poste standard qui inclut une exigence linguistique (en fonction du besoin). L’exigence linguistique stipule que le candidat doit être en mesure de démontrer sa maîtrise écrite et orale de la langue et avoir au moins un an d’expérience professionnelle pour les postes de premier niveau. Les candidats doivent également satisfaire aux exigences en matière de formation et d'expérience pour que leur candidature soit prise en considération. Les candidats doivent également faire montre d'une compréhension des événements en cours pour le pays ou la région où ils participent à la modération du contenu. 

Notre équipe de modération applique nos politiques et nos mesures d’exécution pour aider à protéger notre communauté Snapchat. La formation se déroule sur plusieurs semaines et les nouveaux membres de l’équipe sont formés aux politiques, aux outils et aux procédures d’escalade de Snap. Après la formation, chaque modérateur doit passer un examen de certification avant d’être autorisé à examiner le contenu. Notre équipe de modération participe régulièrement à une formation de remise à niveau en rapport à leurs flux de travail, notamment lorsque nous faisons face à des cas à la limite de nos politiques et à des situations dépendantes du contexte. Nous organisons également des programmes de perfectionnement, des sessions de certification et des quiz pour nous assurer que tous les modérateurs sont à jour et qu’ils sont conformes à toutes les politiques mises à jour. Enfin, lorsque des tendances de contenu urgentes font surface sur des événements en cours, nous diffusons rapidement des clarifications de politique afin que les équipes puissent répondre en fonction des politiques de Snap.

Nous fournissons à notre équipe de modération de contenu, les « premiers répondants numériques » de Snap, un soutien et des ressources importants, y compris un soutien en matière de bien-être sur le lieu de travail et un accès facile aux services de santé mentale. 

  
Mesures de protection en matière de modération du contenu

Nous sommes conscients qu’il y a des risques associés à la modération du contenu, y compris des risques pour la liberté d’expression qui peuvent être causés par la partialité des modérateurs automatisés et humains et par des rapports abusifs, notamment par des gouvernements, des constitutions politiques ou des personnes bien organisées. Snapchat n’est généralement pas un lieu de contenu politique ou activiste, en particulier dans nos espaces publics. 

Néanmoins, pour prévenir ces risques, Snap a mis en place des tests et des formations, et dispose de procédures robustes et cohérentes pour le traitement des rapports sur le contenu illégal ou en violation de la loi, y compris auprès des autorités gouvernementales. Nous évaluons et améliorons constamment nos algorithmes de modération de contenu. Bien qu'il soit difficile de détecter les atteintes potentielles à la liberté d'expression, nous n'avons pas connaissance de problèmes importants et nous mettons à la disposition de nos utilisateurs des moyens de signaler les erreurs éventuelles.

Nos politiques et nos systèmes favorisent une application cohérente et équitable et, comme décrit ci-dessus, offrent aux Snapchatters la possibilité de contester de manière significative les résultats de l'application par le biais de procédures de notification et d'appel qui visent à sauvegarder les intérêts de notre communauté tout en protégeant les droits individuels des Snapchatters.

Nous nous efforçons en permanence d’améliorer nos politiques et nos procédures d’exécution. Nous avons également fait de grands progrès pour combattre le contenu et les activités potentiellement nuisibles et illégales sur Snapchat. Cela se reflète dans la hausse de nos chiffres de signalement et d’exécution de notre dernier [Rapport sur la transparence](https://values.snap.com/fr-FR/privacy/transparency) et la diminution des taux de prévalence pour les violations sur Snapchat dans son ensemble. 

  
Notifications des signaleurs de confiance (Article 15.1(b))

Pendant la période de notre dernier [Rapport sur la transparence](https://values.snap.com/fr-FR/en-GB/privacy/transparency) (S1 2023), aucun signaleur de confiance n’a été officiellement désigné en vertu du DSA. De ce fait, le nombre de notifications soumises par ces signaleurs de confiance était de zéro (0) au cours de cette période.

  
Litiges extrajudiciaires (Article 24.1(a))

Pendant la période de notre dernier [Rapport sur la transparence](https://values.snap.com/fr-FR/en-GB/privacy/transparency) (S1 2023), aucun organe de règlement des litiges extrajudiciaires n’a été officiellement désigné en vertu du DSA De ce fait, le nombre de litiges soumis à ces organes était de zéro (0) au cours de cette période.

  
Suspensions de compte en vertu de l’article 23 (Article 24.1(b))

Pendant la période de notre dernier [Rapport sur la transparence](https://values.snap.com/fr-FR/en-GB/privacy/transparency) (S1 2023), il n’était pas nécessaire de suspendre les comptes en vertu de l’article 23 du DSA pour la fourniture de contenu manifestement illégal, d'avis non fondés ou de plaintes non fondées. De ce fait, le nombre de ces suspensions était de zéro (0). Toutefois, Snap prend les mesures d’exécution appropriées à l’égard des comptes comme détaillé dans notre [Note explicative sur la modération, l’exécution et les appels de Snapchat](https://values.snap.com/fr-FR/en-GB/privacy/transparency/community-guidelines/moderation) et les informations sur le niveau d’exécution du compte Snap se trouvent dans notre [Rapport sur la transparence](https://values.snap.com/fr-FR/en-GB/privacy/transparency) (S1 2023).