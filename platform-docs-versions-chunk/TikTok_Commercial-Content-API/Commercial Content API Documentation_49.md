platform: TikTok
topic: Commercial-Content-API
subtopic: Commercial Content API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Commercial-Content-API/Commercial Content API Documentation.md
url: https://developers.tiktok.com/doc/commercial-content-api-supported-countries/

## Continental Europe

The United Kingdom (England, Wales, Scotland, and Northern Ireland) and Switzerland are not in the EU or the EEA but are part of continental Europe.

|     |     |
| --- | --- |
| United Kingdom | GB  |
| Switzerland | CH  |