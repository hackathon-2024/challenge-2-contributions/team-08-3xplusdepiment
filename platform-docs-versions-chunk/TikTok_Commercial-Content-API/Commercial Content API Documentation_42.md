platform: TikTok
topic: Commercial-Content-API
subtopic: Commercial Content API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Commercial-Content-API/Commercial Content API Documentation.md
url: https://developers.tiktok.com/doc/commercial-content-api-query-commercial-content/

# Response

|     |     |     |
| --- | --- | --- |
| **Key** | **Type** | **Example** |
| data | QueryCommercialContentData | See the response example below. |
| error | ErrorStructV2 | See the response example below. |