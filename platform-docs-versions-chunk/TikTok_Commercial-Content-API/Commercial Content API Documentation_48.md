platform: TikTok
topic: Commercial-Content-API
subtopic: Commercial Content API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Commercial-Content-API/Commercial Content API Documentation.md
url: https://developers.tiktok.com/doc/commercial-content-api-supported-countries/

# Supported Countries

## European Union (EU)

|     |     |
| --- | --- |
| **Country** | **Country Code** |
| Austria | AT  |
| Belgium | BE  |
| Bulgaria | BG  |
| Croatia | HR  |
| Republic of Cyprus | CY  |
| Czech Repub­lic | CZ  |
| Denmark | DK  |
| Estonia | EE  |
| Finland | FI  |
| France | FR  |
| Germany | DE  |
| Greece | GR  |
| Hungary | HU  |
| Ireland | IE  |
| Italy | IT  |
| Latvia | LV  |
| Lithuania | LT  |
| Luxembourg | LU  |
| Malta | MT  |
| Netherlands | NL  |
| Poland | PL  |
| Portugal | PT  |
| Romania | RO  |
| Slovakia | SK  |
| Slovenia | SI  |
| Spain | ES  |
| Sweden | SE  |

## European Economic Area (EEA)

Comprised of the European Union (EU) member states and Iceland, Liechtenstein, and Norway

|     |     |
| --- | --- |
| **Country** | **Country Code** |
| Norway | NO  |
| Iceland | IS  |
| Liechtenstein | LI  |