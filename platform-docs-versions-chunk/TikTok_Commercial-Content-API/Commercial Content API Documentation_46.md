platform: TikTok
topic: Commercial-Content-API
subtopic: Commercial Content API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Commercial-Content-API/Commercial Content API Documentation.md
url: https://developers.tiktok.com/doc/commercial-content-api-query-commercial-content/

### CommercialContentCreator

|     |     |     |     |
| --- | --- | --- | --- |
| **Key** | **Type** | **Description** | **Example** |
| username | string | The commercial content creator's TikTok handler. | joe123 |

### CommercialContentVideo

|     |     |     |     |
| --- | --- | --- | --- |
| **Key** | **Type** | **Description** | **Example** |
| id  | i64 | The commercial content video's ID. | 19384729204821234 |
| status | string | The commercial content video's status. | active |
| url | string | The commercial content video's URL. | https://www.tiktok.com/@joe1234567/video/19384729204821234 |
| cover\_image\_url | string | The commercial content video's cover image URL. | https://asdfcdn.com/17392712.jpeg?x-expires=1679169600\\u0026x-signature=asdf |