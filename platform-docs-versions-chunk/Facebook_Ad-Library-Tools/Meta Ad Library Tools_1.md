platform: Facebook
topic: Ad-Library-Tools
subtopic: Meta Ad Library Tools
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Library-Tools/Meta Ad Library Tools.md
url: https://transparency.fb.com/fr-fr/researchtools/ad-library-tools

### Ad Library

[Meta Ad Library](https://www.facebook.com/adlibrary) is a comprehensive, searchable database for ads transparency. People can use the Ad Library to get more information about the ads they see across Meta technologies.

People can search for all active ads running across products from Meta. For ads about social issues, elections or politics, Meta provides additional information, including spend, reach and funding entities. These ads are visible whether they’re active or inactive and are stored in the Ad Library for 7 years.

Meta offers more information for ads that deliver an impression in the EU or associated territories. These ads are displayed in the Ad Library while active and archived for one year upon the delivery of their last impression. The Ad Library also has a searchable database that displays all active, public branded content running on Facebook and Instagram with a paid partnership label.

[Learn more](https://www.facebook.com/business/help/2405092116183307?id=288762101909005.) about the types of data available in the Ad Library.