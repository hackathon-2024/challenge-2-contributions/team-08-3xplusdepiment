platform: Facebook
topic: Ad-Library-Tools
subtopic: Meta Ad Library Tools
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Library-Tools/Meta Ad Library Tools.md
url: https://transparency.fb.com/fr-fr/researchtools/ad-library-tools

### Ad Library API

The [Ad Library API](https://www.facebook.com/ads/library/api/) is an application programming interface that allows for a deeper analysis of ads about social issues, elections or politics, as well as ads that deliver to the [EU and associated territories](https://www.facebook.com/business/help/605021638170961). Authorized users can also analyze active and public branded content running on Facebook and Instagram via the API.

### Ad Library Report

The [Ad Library Report](https://www.facebook.com/ads/library/report/) provides an aggregated and comprehensive view of ads about social issues, elections or politics in a selected country for a given time period. The Ad Library Report, which can be downloaded, is available in [all countries](https://www.facebook.com/business/help/2150157295276323) that require authorization to run ads about social issues, elections or politics.