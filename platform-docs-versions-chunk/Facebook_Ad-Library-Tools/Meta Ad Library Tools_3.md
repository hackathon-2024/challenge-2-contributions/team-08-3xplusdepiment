platform: Facebook
topic: Ad-Library-Tools
subtopic: Meta Ad Library Tools
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Ad-Library-Tools/Meta Ad Library Tools.md
url: https://transparency.fb.com/fr-fr/researchtools/ad-library-tools

### Ad Targeting dataset

The [Ad Targeting](https://developers.facebook.com/docs/fort-ads-targeting-dataset) dataset allows approved researchers to analyze targeting information selected by advertisers who ran ads about social issues, elections or politics any time after August 2020 in more than 120 countries. All active and inactive social issues, as well as electoral and political ads with at least one impression are included and remain in the dataset for 7 years.