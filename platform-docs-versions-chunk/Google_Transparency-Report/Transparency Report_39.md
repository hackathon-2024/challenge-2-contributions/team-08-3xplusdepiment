platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Automated tools used to process for Legal-related Content Removal Requests

Automation plays a role in legal content moderation to help Google work at scale, and focus our e orts

on actionable, authentic requests. There are a few ways that automation might be used while handling a

removal request. The most common way is that Google uses automation to route a request to the right

team. Google has subject ma er experts in di erent types of content and languages, and using

automation ensures the request is sent to the people best positioned to review it.