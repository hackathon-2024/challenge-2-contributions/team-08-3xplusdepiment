platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 39

6.3 Number of suspensions for Manifestly Unfounded Complaints imposed

pursuant to Article 23

Article 24(1), point (b)



Users who intentionally misuse webforms and processes by repeatedly  ling manifestly unfounded

complaints will also be  agged, and their requests will be closed without assessment.



During the reporting period, there were 950 suspensions of an EU-based user’s ability to submit Article

20 complaints due to the repeated submission of manifestly unfounded complaints. In addition, 5,845

appeals were suspended during the reporting period re ecting the number of appeals that were not

processed.



EU DSA Report • 40

Section 7: Average monthly active recipients of Google

services in the Union

Article 24(2)



The average number of monthly active recipients of Google services in each European Union Member

State is provided in the DSA Monthly Active Recipients report published on 16 August 2023.



EU DSA Report • 41

Section 8: Additional Notes