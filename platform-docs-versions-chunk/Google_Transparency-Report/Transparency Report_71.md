platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Identifying the human resources who evaluate content across Google services is a highly complex

process. Content moderators may review content for multiple policy violations or focus on one speci c

topic; they may review content that appears across one or more services; and content assigned for

their review may have been posted in several di erent languages. In some cases and where

appropriate, translation tools may be used to assist in the review process and allow us to moderate

content 24/7 and at scale.



Table 3.3.1 re ects the human resources evaluating content across the o cial EU Member State

languages, for each service. For Google Maps, the metric presented is the number of content

moderators whose primary language of review is an EU language and who reviewed at least one piece

of content during the period from 28 August 2023 to 10 September 2023. For YouTube, the metric

re ects the number of content moderators who reviewed at least 10 videos posted in an o cial EU