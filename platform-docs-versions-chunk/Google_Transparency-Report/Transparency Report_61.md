platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

3. Preventing re-uploads of known violative content: YouTube utilises technology to prevent

re-uploads of known violative content as quickly as possible. For example, YouTube leverages

hashes (or ”digital  ngerprints”) to detect and automatically remove child sexual abuse imagery

(CSAI) videos on YouTube. YouTube has long used this technology to prevent the spread of

violative content like CSAI or terrorist content. More information is available here.



To improve the accuracy of our automated systems and understand what investments to make in

machine learning, YouTube evaluates the amount of violative content that gets viewed before it is

detected by automated technology and removed.