platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

YouTube continues to invest in automated detection systems, and rely on both human evaluators and

machine learning to detect and take action on problematic content at scale while simultaneously training

our systems on new data. The vast majority of content reviewed and enforced on YouTube is  rst

detected by automated systems. However, a er potentially violative content has been detected by

automated systems, content moderators may review the content to con rm the decision. As models

continuously learn and adapt based on content moderator feedback, this collaborative approach helps

improve the accuracy of these models over time. It also means that the enforcement systems can

manage the scale of content that is uploaded to YouTube (over 500 hours of content every minute),

while still digging into the nuances that determine whether a piece of content is violative.



Some examples of how YouTube uses automated processes for content moderation include: