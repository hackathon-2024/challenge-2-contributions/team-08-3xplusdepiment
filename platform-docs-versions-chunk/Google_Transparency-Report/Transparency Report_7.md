platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Courts and government agencies in the EU regularly request that we remove information from Google

services (Removal Orders). These requests are routed to the appropriate team(s) within Google who

review these requests closely to determine if information should be removed because it may violate a

law or our product policies. In addition, speci c Member State laws allow government agencies in the EU

to request user information for civil, administrative, criminal, and national security purposes (User Data

Disclosure Orders). Each request is carefully reviewed to make sure it satis es applicable laws. No

Removal Orders or User Data Disclosure Orders conforming to the requirements of Articles 9 and 10 of

the DSA were received during the reporting period.



Information about other requests from government authorities around the world are published in our

Government Requests for Content Removal Transparency Report and our Government Requests for

User Information Transparency Report2.