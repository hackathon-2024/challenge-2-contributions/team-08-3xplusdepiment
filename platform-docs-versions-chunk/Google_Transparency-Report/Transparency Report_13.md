platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

2.5 Median time needed to take action on content identi ed in Article 16

notices

Article 15(1), point (b)



Table 2.5.1 re ects the median time, in days, needed to take action on content identi ed in Article 16

notices for each service.



EU DSA Report • 6

Table 2.5.1: Median time to take action on Article 16 notices, by service



Service Median time to take action (days)



Maps 6



Play <1



Shopping 3



YouTube <1



Multi-Services1 <1



Notes:



1 Notices relating to advertisements that may appear across multiple Google services, including VLOPs, are

included under Multi-Services.



EU DSA Report • 7

Section 3: Content moderation engaged in at Google’s

own initiative



3.1 Content Moderation at Google’s own initiative

Article 15(1), point (c)