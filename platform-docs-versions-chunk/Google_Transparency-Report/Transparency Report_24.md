platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

4 Google uses a variety of automated tools to provide a secure environment for users including Safe Browsing

technology. This technology examines billions of URLs per day to identify malware and phishing sites and notify

users and webmasters so they can protect themselves from harm. The number of URLs added to Safe Browsing

block lists are not included in the metrics above.



5 Removal actions from Google Search do not remove content from publishers’ sites, but only prevent the content

from being included in search results.



6 All Data Defect items were related to “Things to Do” search results.



EU DSA Report • 11

Table 3.1.1.b: Own initiative actions taken on advertisements presented on

Google Search, by type of illegal content or violation of terms and conditions



Table 3.1.1.c: Own initiative actions taken on Google Maps, by type of illegal

content or violation of terms and conditions