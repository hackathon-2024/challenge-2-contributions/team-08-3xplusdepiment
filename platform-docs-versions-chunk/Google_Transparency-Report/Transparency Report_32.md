platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Host Level Actions 1,255 0 N/A1



URL Level Removals 1,370,062 30,507,164 N/A1



URL Level Filtering 61,847,459 0 N/A1



Incident Level Actions 335 1,835 N/A1



Partner Feed Item Level Actions 3,390 0 N/A1



Ads on Search 1,630,713 514,798 N/A1



Maps 1,935,749 16,397 0



Ads on Maps 61,529 10,247 N/A1



Play 461,502 1,059 897



Ads on Play 1,812 336 N/A1



EU DSA Report • 18

Service



Number of measures taken on



Violative content

that was

self-detected



Violative content

that was detected

externally



Violative content

where detection

method was

unknown



Shopping2 181,823,192 32,125 N/A1



YouTube3 424,790 7,147 33,459



Ads on YouTube 642,445 165,568 N/A1



Multi-Services N/A1 N/A1 120,048



Notes:



1 N/A indicates that this is not an applicable outcome for this service.



2 Shopping metrics re ect content moderation actions taken on both unpaid content (e.g., free listings) and

advertisements.