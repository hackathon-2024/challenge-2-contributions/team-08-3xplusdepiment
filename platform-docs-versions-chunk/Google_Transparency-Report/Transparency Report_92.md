platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

● Metrics presented in this report generally re ect our e orts and resources to moderate

potentially illegal content and policy-violative content in the EU. However, most, but not all, of

policy-violating content on Google Search and its features is moderated globally. Most content

delisted from Google Search on legal grounds is content subject to copyright removal

noti cations, which are also processed globally. However, there are classes of delistings based

on local law or local court orders that a ect only certain country services, based on variance in

laws between countries. Finally, in some cases, EEA metrics have been voluntarily provided in

this report.



● Numbers reported may  uctuate between successive reports due to various reasons, including

service level changes or enhancements, changes in the number of users on a service, external

events and di erences in reporting periods. Therefore, report-by-report comparisons may not

accurately re ect time-based improvements in our processes.