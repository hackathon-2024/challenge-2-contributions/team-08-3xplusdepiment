platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Article 24(1), point (b)



To protect users from signi cant harm and unlawful activity, Google suspends user accounts when we

detect egregious content (e.g., child abuse) or repeated violations of our services’ policies. Suspended

user accounts are unable to access Google products and, depending on the suspension reason, may not

contribute to Google pla orms or engage in speci c Google processes (e.g., submission of complaints

through dedicated complaint channels).



6.1 Number of suspensions for Manifestly Illegal Content imposed

pursuant to Article 23

Article 24(1), point (b)