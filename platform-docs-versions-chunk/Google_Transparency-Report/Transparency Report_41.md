platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Google also uses automation to process some legal notices. The vast majority of notices are copyright

removal requests, largely from submi ers with a well-established track record of submi ing valid

requests, allowing Google to be relatively con dent in automating this processing. During the reporting

period, \>99.99% of all fully automated removal decisions on Web Search that impacted users based in

the EU were unchanged, while <0.01% were reinstated as a result of a counter notice.



Automated Tools used to combat Child Sexual Abuse Material (CSAM)

Google takes its responsibility to  ght child sexual abuse and exploitation online very seriously. We do

this by comba ing CSAM across Google’s products and by detecting instances of abuse and enforcing

robust policies. We also partner with non-governmental organisations (NGOs) and others in industry to

share proprietary technology and drive the industry forward.