platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Legal standards vary greatly by country/region. Content that violates a speci c law in one country/region

may be legal in others. Typically, Google removes or restricts access to content only in the

country/region where it is deemed to be illegal. However, when content is found to violate Google’s

content or product policies or Terms of Service, Google may remove or restrict access globally.



When a legal notice is reviewed and the content violates our content policies, action may be taken on

policy grounds. If the content does not violate our policies, Google may take action on legal grounds, in

line with local laws (see Table 2.3.1 for breakdown by service). As a legal notice may contain one or more

URLs for review, multiple actions may be taken as a result of a single notice received.



EU DSA Report • 5



Type of alleged

illegal content



Number of Article 16 notices



Maps Play Shopping YouTube Multi-

Services1



Violent Extremism 0 0 0 180 0



Other Legal 2 1 0 848 32