platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Table 3.1.1.d: Own initiative actions taken on advertisements presented on

Google Maps, by type of illegal content or violation of terms and conditions



Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken



Healthcare and Medicine 4,072



Intellectual Property Infringements 10,252



Pornography / Sexualised Content 264



Scams and/or Fraud 9,214



Scope of Pla orm Service 44,063



Unsafe and/or Illegal Products 3,911



Total 71,776



Table 3.1.1.e: Own initiative actions taken on Google Play, by type of illegal

content or violation of terms and conditions



Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken



Data Defect 638



Data Protection and Privacy Violations 12,633



Illegal / Harmful Speech 1,083



Inappropriate and Unhelpful 89,110



Negative E ects on Civic Discourse / Elections 85



Non-consensual Behaviour 18