platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Depending on the severity of the detected violation and involvement of legal enforcement authorities,

users may receive a warning and/or remedial instructions to remove the violating content before their

account is suspended. During the reporting period, there were 1,975 Google-wide account-level

suspensions of EU users who posted manifestly illegal content across Google services, but not

necessarily limited to VLOPs.



6.2 Number of suspensions for Manifestly Unfounded Notices imposed

pursuant to Article 23

Article 24(1), point (b)