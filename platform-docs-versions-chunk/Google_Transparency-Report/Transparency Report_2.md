platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

In accordance with Articles 15, 24, and 42 of the DSA, Google is publishing biannual transparency

reports for its services designated by the European Commission as a Very Large Online Search Engine

(VLOSE) or a Very Large Online Pla orm (VLOP): Google Search, Google Maps, Google Play, Shopping

and YouTube.



This report describes Google’s e orts and resources to moderate content on the services listed above

in the EU during the period from 28 August 2023 to 10 September 20231.



Overview

Since Google was founded, our mission has been to organise the world’s information and make it

universally accessible and useful. When it comes to the information and content on our pla orms, we

take seriously our responsibility to safeguard the people and businesses using our products, and do so

with clear and transparent policies and processes.