platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 27

2. Identifying copyright-protected content: Content ID, YouTube's automated content

identi cation system, identi es copyright-protected content on YouTube. Videos uploaded to

YouTube are scanned against a database of audio and visual reference  les submi ed to

YouTube by copyright owners. A Content ID claim is automatically generated on behalf of a

copyright owner when an uploaded video matches another video or audio reference  le (in

whole or in part) in YouTube's Content ID system. Depending on the copyright owner's Content

ID se ings, Content ID claims can:

● Block a video from being viewed in one or more territories;

● Enable revenue-sharing with the copyright owner based on the video’s earnings;

● Provide the video’s viewership statistics to the copyright owner.



YouTube only grants Content ID to copyright owners who meet speci c criteria. More

information about How Content ID works is available here.