platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Additionally, Google uses the corpus of already human-reviewed and removed content to train machine

learning technology to  ag new content that might also violate product and service policies. Using

machine learning technology trained by human decisions means the enforcement systems adapt and

get smarter over time.



This section describes how Google uses automated tools, o en supplemented with human review, for

content moderation, along with the indicators of accuracy of any fully automated tools. While we report

fully automated tools primarily on a language-agnostic basis, where applicable and feasible for this

reporting period, the indicators of accuracy are broken down by language.



3.2.1 Automated tools that a ect multiple services