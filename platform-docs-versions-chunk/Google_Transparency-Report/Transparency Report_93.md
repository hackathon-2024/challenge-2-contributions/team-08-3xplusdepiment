platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

● Services designated as VLOSE and VLOPs di er in various ways, including content type on the

service, underlying content moderation systems and number of users on a service, which means

that in some cases, metrics may not be directly comparable.



EU DSA Report • 42