platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU Digital Services

Act (EU DSA)

Biannual

VLOSE/VLOP

Transparency Report



The European Union (EU) Digital Services Act (DSA) came into force on 16

November 2022. We welcome the DSA's goals of making the internet even more

safe, transparent and accountable, while ensuring that everyone in the EU

continues to bene t from the open web. Google is commi ed to promoting

transparency for the users of our pla orms.



Published: 27 October 2023

EU DSA Biannual VLOSE/VLOP Transparency Report



The European Union (EU) Digital Services Act (DSA) came into force on 16 November 2022. Google has

long been aligned with the broad goals of the DSA and has devoted signi cant resources into tailoring

our programs to meet its speci c requirements. We welcome the DSA's goals of making the internet

even safer, more transparent and more accountable, while ensuring that everyone in the EU continues to

bene t from the open web.