platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

To support information and content quality on our products and services, we take a wide range of

enforcement actions to maintain a trusted experience for all. Enforcement actions di er from service to

service.



EU DSA Report • 8

3.1.1 Number of measures taken at Google’s own initiative that a ect the

availability, visibility and accessibility of information provided by recipients

of the service, broken down by type of illegal content or violation of terms

and conditions

Article 15(1), point (c)



Google considers 'measures' as actions taken on moderated videos, URLs, listings, accounts and other

content types which are of a policy-violative nature, or which are delisted as a result of applicable law.

Tables 3.1.1.a through 3.1.1.j re ect the number of measures taken at Google’s own initiative that a ect

the availability, visibility and accessibility of information provided by recipients of each service, broken

down by the type of alleged illegal content or policy violation.