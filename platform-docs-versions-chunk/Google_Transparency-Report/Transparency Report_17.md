platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

For each of these services except Shopping, we separately present the metrics relating to

advertisements impressed on those services. The majority of the measures that Shopping takes happen

before the content is shown publicly, and the actions may apply to both unpaid content (e.g., free

listings) and advertisements. As such, Shopping cannot readily distinguish between unpaid content and

advertisements in these metrics, therefore they are combined. In addition, for non-Shopping content,

content moderation actions on advertisements that are taken before the advertisement is surfaced on a

VLOSE or VLOP are not included in this report.