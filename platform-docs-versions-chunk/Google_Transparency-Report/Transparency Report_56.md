platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

User reviews of Google Play apps also go through automated review processes to determine if the user

review violates the user comment posting policies (e.g., contains hate speech, sexually explicit content,

spam, etc.). This automated model’s precision at the time of launch was \>90%, globally, and is

monitored. Should there be a performance outlier, the rule is re-evaluated and adjusted as needed.



EU DSA Report • 26

3.2.5 Shopping



Products and merchants go through in-depth safety reviews before they can list on Google. Thanks to

features such as the Shopping Graph (Shopping’s data set of the world’s products and sellers),

Shopping’s systems can quickly review whether a business is legitimate, and whether the products and

other content follow Shopping’s policies. This automated ve ing process has helped to more e ciently

and accurately review a massive amount of products.