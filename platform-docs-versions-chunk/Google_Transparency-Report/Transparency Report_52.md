platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

To detect this policy-violating content, Maps’ machine-learning algorithms scan contributions for signals

of suspicious user activity. The policy-violating content is either removed by automated models or

 agged for further review by trained operators and analysts who conduct content evaluations that

might be di cult for algorithms to perform alone.



To protect users from  nding inappropriate content, Maps deploys many other protections, such as

suspending UGC for speci c places, geographic areas and categories of places. These measures may

be deployed reactively to counteract a spike in content that violates our policies, or proactively if Maps

believes that these measures are necessary to prevent content that violates our policies. Maps may also

restrict feature access or suspend Google accounts that violate our policies. Removing content,

rejecting edits or restricting feature access may include preventing uploaded content from being

displayed to other users.