platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

In this report, we outline and provide metrics contemplated by the DSA regarding our e orts and

resources to moderate potentially illegal content and policy-violative content in the EU. We are

commi ed to improving and augmenting future iterations with further insights about our continued

e orts to combat violative content on our pla orms.



EU DSA Report • 2

Section 1: Article 9 and 10 Orders from Member States’

authorities

Article 15(1), point (a)