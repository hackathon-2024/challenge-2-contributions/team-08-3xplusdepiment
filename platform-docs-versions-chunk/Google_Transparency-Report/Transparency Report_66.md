platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Quali cations and linguistic expertise

Quali cations for Google employees who work on sensitive content may include role related knowledge

in the content ma er, professional experience in content moderation or sensitive work ows, linguistic

expertise and computer pro ciency. The linguistic expertise required varies depending on the speci c

work ow of a product or service, the type of content, and languages that content is available in. Some

products or services require native pro ciency in global supported languages, others may use

translation tools if necessary, and some videos or images do not require any language pro ciency in

order to review. Some Google employees who work on sensitive content are also subject ma er

specialists skilled in specialty areas, such as child sexual abuse material or violent extremism.