platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 1

distribution of harmful and illegal content before it reaches users; detect and evaluate potentially

violative content; and respond to bad actors and abusive content in an appropriate way. Second, through

our ranking and recommendation systems, we deliver reliable information to users, as well as provide

tools to help users evaluate content themselves, giving them added context and con dence in what

they  nd on our products and services, and across the internet. Third, we partner to create a safer

internet and scale our impact, collaborating with experts, governments, and organisations to inform our

tools and share our technologies.



Helpful, safe online environments do not just happen — they are designed. At Google, we aim to balance

access to information with protecting users and society, while providing information and content users

can trust.