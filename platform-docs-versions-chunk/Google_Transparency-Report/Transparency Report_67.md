platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Onboarding and training

Google employees that work on sensitive content teams are o ered subject ma er speci c training on

a variety of topics. Employees working in sensitive content are required to complete a training on the

Psychological Impact of Sensitive Content Review at the point of onboarding, and managers are

required to complete an additional training on Supporting Teams who Work with Sensitive Content.

Additional optional training opportunities include those on self-compassion, emotional agility, and

subject ma er speci c training to provide a deeper dive into the unique challenges faced by each team.

The training is generally conducted via e-learning with opportunities for live facilitated training.