platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 29

Wellbeing support

Google is commi ed to supporting the wellness of its employees that work with sensitive content

through comprehensive programs and resources. Google strives for safe and healthy working

conditions for all employees exposed to sensitive content and is commi ed to ensuring they have the

highest standard of support. Google has invested signi cantly in these teams by: