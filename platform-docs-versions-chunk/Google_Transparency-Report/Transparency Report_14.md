platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Across all products and services, we set clear policies for what is and is not acceptable on our pla orms.

These policies aim to ensure a safe and positive experience for our users and observe a high standard of

quality and reliability for advertisers, publishers, and content creators alike.



Content policies establish the rules of the road for what content can be created, uploaded, sent, shared,

and monetised. These policies are used to guide content moderation and enforcement actions on our

products. Community Guidelines play an important role in maintaining a positive experience for

everyone on our pla orms no ma er where they are in the world.



User data and developer policies provide rules for how developers interact with our products and

services. They also describe the privacy and security requirements for handling user data to include the

full spectrum of developer actions, like requesting, obtaining, using, and sharing data.