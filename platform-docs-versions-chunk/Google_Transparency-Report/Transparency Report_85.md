platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

4 Within the “initial decision overturned” category, the majority of overturned decisions for YouTube are for

monetary payment restrictions. This is an expected outcome intended to protect users, creators, and

advertisers, enforced using YouTube’s Advertiser-Friendly Content Guidelines. Monetising creators (those in

the YouTube Partner Program) have easy access to a timely and user-friendly internal complaint handling

system and are encouraged to use it if they believe YouTube’s systems made a mistake. For information

speci cally about YouTube’s Community Guidelines enforcement and appeals, please see YouTube’s



EU DSA Report • 36

Community Guidelines Transparency Report (note: data in YouTube’s Community Guidelines Transparency

Report are not directly comparable with the data presented in this DSA Transparency Report).