platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

1 Shopping metrics re ect complaints about content moderation actions taken on both unpaid content (e.g.,

free listings) and advertisements.



2 The Multi-Services category includes complaints about Google account-level terminations, in addition to

complaints relating to actions taken in response to Article 16 notices that relate to advertisements appearing

on one or more Google services, which may include a VLOSE or VLOP.



EU DSA Report • 35

4.3 Number of complaints, broken down by outcome of complaint

Article 15(1), point (d)