platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

9 N/A indicates that this is not an applicable category for YouTube.



10 Content moderators who review non-language content (e.g., an image) are included in the ‘Agnostic’

category.



EU DSA Report • 32

Content Moderation at Google: an Illustrative Case Study



Max is a Google employee who works in sensitive content and specialises in violent extremism as a

Content Specialist\*. Max graduated from the University of Amsterdam with a degree in

Communications. He has three years of experience working in content moderation on sensitive

work ows. He is  uent in English, Spanish, Dutch and French.