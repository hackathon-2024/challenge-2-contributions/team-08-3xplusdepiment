platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Shopping’s automated systems are always monitoring for violating activity. Some examples of

automated content moderation processes used include:

● policy checks for harmful, regulated, or illegal content (e.g., weapons, recreational and

prescription drugs, tobacco products);

● product image checks for policy violations such as graphic overlays or nudity;

● product data quality checks;

● landing page checks; and

● checks for recalled products such as those listed in the Rapid Exchange of Information System

(RAPEX) or Organisation for Economic Co-operation and Development (OECD) public

databases.



During the reporting period, <0.01% of all automated content moderation actions on Shopping were

appealed by content or account owners based in the EU and consequently <0.01% of all original content

moderation actions were overturned. Of the relatively few original content moderations that were

appealed and subsequently closed within the reporting period, 83% were overturned.



3.2.6 YouTube