platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

● Domain Level Actions: Number of internet domains taken action on due to policy violations.

● Host Level Actions: Number of internet hostnames (or variants with common pre xes such as

www) taken action on due to policy violations.

● URL Level Removals: Number of individual URLs removed due to legal or policy violations.

● URL Level Filtering: Number of times individual URLs were  ltered algorithmically from Discover

feeds based on Google’s content policies.

● Incident Level Actions: Number of incidents originating from various reporting channels, which

were actioned due to policy violations.

● Partner Feed Item Level Actions: Number of entities (URLs or images) taken action on in

response to partner feeds providing “Things to Do” results that appear on Google Search.