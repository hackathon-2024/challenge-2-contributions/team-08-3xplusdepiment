platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

In order to detect and report CSAM, we may use a combination of cu ing-edge technology, including

machine learning classi ers (to identify unknown CSAM) and hash-matching technology, as well as

trained specialist teams. Hash-matching technology creates a “hash”, or unique digital  ngerprint, for an

image or a video so it can be compared with hashes of known CSAM. When Google  nds CSAM, our

services remove it, report it to the National Center for Missing and Exploited Children (NCMEC), and

take action, which may include disabling the account.