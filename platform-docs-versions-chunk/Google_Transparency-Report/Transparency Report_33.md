platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

3 Consistent with Table 3.1.1.h, YouTube also removed 469,889 videos as the result of their associated channel’s

termination. There is no detection method associated with these actions, therefore these are excluded from

the table above.



3.1.3 Number of measures taken at Google’s own initiative that a ect the

availability, visibility and accessibility of information provided by the

recipients of the service, broken down by type of restriction applied

Article 15(1), point (c)



Table 3.1.3 provides the number of measures taken on violative content, broken down by service and the

type of restriction applied. The type of restrictions include:



(i) restrictions of visibility of content;

(ii) demonetisation;

(iii) partial service-level suspension; and

(iv) service-speci c or Google-wide account-level termination.