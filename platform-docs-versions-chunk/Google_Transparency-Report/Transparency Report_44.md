platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Google scales its impact by collaborating with NCMEC and partnering with NGOs and industry

coalitions to help grow and contribute to a joint understanding of the evolving nature of child sexual

abuse and exploitation. One of the ways Google contributes is by creating and sharing free tools to help

other organisations prioritise potential CSAM images for human review. For example, Google’s Child

Safety Toolkit consists of two APIs. The  rst is CSAI Match, an API developed by YouTube that partners

can use to automatically detect known videos of CSAM so they can  ag for review, con rm, report, and

act on it. The second is Google’s Content Safety API that helps partners classify and prioritise novel

potentially abusive images and videos for review. Detection of never-before-seen CSAM helps the child



EU DSA Report • 22

safety ecosystem by identifying child victims in need of safeguarding and contributing to the list of

known digital  ngerprints to grow our abilities to detect known CSAM.