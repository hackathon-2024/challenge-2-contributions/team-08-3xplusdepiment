platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Unsafe and/or Illegal Products Incident Level Actions 2



Violence

URL Level Filtering 19,615,160



Incident Level Actions 14



Other

Domain Level Actions 23



Incident Level Actions 2,020



Notes:



1 Google Search is out-of-scope for Art 15(1)(b). However, actions taken on a legal basis in response to legal

notices received about Search content are reported under Art 15(1)(c) for completeness. Policy violations may

apply to some but not all Search products and features.