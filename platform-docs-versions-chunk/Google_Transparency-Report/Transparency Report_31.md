platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 17

3.1.2 Number of measures taken at Google’s own initiative that a ect the

availability, visibility and accessibility of information provided by recipients

of the service, broken down by detection method

Article 15(1), point (c)



Table 3.1.2 re ects the number of measures taken on violative content, broken down by service and

detection method, which can be:

(i) Self-detection, where Google employees, algorithms, or contractors  ag potentially illegal or

policy violating content;

(ii) External detection in response to an external noti cation (e.g., user policy  ags or legal

complaints); or

(iii) Unknown due to system limitations.



Table 3.1.2: Number of measures taken at Google’s own initiative, by service and

detection method



Service



Number of measures taken on



Violative content

that was

self-detected



Violative content

that was detected

externally



Violative content

where detection

method was

unknown



Search



Domain Level Actions 1,638,528 0 N/A1