platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Research and technological innovation

In addition to gathering feedback directly from workers and soliciting professional input and advice,

Google is commi ed to driving industry-leading research and technological innovation in the  eld of

content moderation. For instance, Google published a research paper in 2019 indicating that “grayscale

transformations” (i.e., where an image was converted to black and white) reduced the emotional impact

of reviewing violent and extremist content. Based on these  ndings, Google built grayscaling into review

tools, giving each reviewer an option to use this feature when performing reviews, based on their own

preference.



3.3.1 Human resources evaluating content across the o cial EU Member

State languages

Article 42(2)