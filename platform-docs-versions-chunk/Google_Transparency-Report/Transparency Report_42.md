platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>





Built-in protections help prevent Google products from showing abusive content and deter bad actors.

For example, Google deploys safety by design principles to deter users from seeking out CSAM on

Google Search. It is our policy to block search results that lead to child sexual abuse imagery or material

that appears to sexually victimise, endanger or otherwise exploit children. We are constantly updating

our algorithms to combat these evolving threats. We apply extra protections to searches that we

recognise as seeking CSAM content. We  lter out explicit sexual results if the search query seems to be

seeking CSAM. For queries seeking adult explicit content, Google Search won’t return imagery that

includes children, to break the association between children and sexual content. In many countries,

users who enter queries clearly related to CSAM are shown a prominent warning that child sexual abuse

imagery is illegal, with information on how to report this content to trusted organisations. When these

warnings are shown, we have found that users are less likely to continue looking for this material.