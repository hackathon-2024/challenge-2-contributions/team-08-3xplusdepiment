platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

When reviewing ad content or advertiser accounts to determine whether they violate our policies,

Google takes various information into consideration when making a decision, including the content of

the creative (e.g. ad text, keywords, and any images and video) as well as the associated ad destination.

Google also considers account information (e.g., past history of policy violations) and other information

provided through reporting mechanisms (where applicable) in our investigation.



During the reporting period, <2% of Google’s fully automated enforcement decisions on ads placed by

advertisers in the EU were overturned a er subsequently undergoing human review.



3.2.2 Google Search