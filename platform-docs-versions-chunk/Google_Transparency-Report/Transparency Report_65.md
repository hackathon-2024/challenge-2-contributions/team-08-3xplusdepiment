platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Google strives to create workplaces and economic opportunities that work for employees, as well as

vendors, temporary sta , and independent contractors. While Google does not employ all of the

individuals who contribute to content moderation, Google is commi ed to ensuring that work on

Google products is conducted in environments that treat all workers with respect and dignity, ensure

safe working conditions, and conduct responsible, ethical operations. For that reason, Google seeks out

suppliers that embrace its values, commitment to human rights, and that support a safe working

environment. Suppliers must operate in accordance with our Supplier Code of Conduct, and comply

with all applicable labour protection laws, including those related to privacy, safety, health, and wages.

Google also provides Wellness Standards that promote healthy working conditions for provisioned

extended workforce members performing sensitive content moderation.