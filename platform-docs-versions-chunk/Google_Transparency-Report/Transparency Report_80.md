platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Shopping1 89,749



YouTube 37,446



Ads on YouTube 75,426



Multi-Services2 50,535



Notes:



1 Shopping metrics re ect complaints relating to both unpaid content (e.g., free listings) and advertisements.



EU DSA Report • 34

2 The Multi-Services category includes complaints about Google account-level terminations, in addition to

complaints relating to actions taken in response to Article 16 notices that relate to advertisements appearing

on one or more Google services, which may include a VLOSE or VLOP.



4.2 Number of complaints, broken down by complaint reason

Article 15(1), point (d)