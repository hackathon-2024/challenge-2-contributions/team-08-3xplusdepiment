platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

In order to safeguard against content actions that could potentially contribute to or exacerbate adverse

impacts due to allowing or removing content, Google utilises international human rights standards to

guide policy and enforcement decision-making, considering how content could adversely impact the

rights of an individual, community, or society as a whole, or further the understanding of social, political,

cultural, civic, and economic a airs. As an example of public interest-informed content moderation,

Google carves out exceptions to enforcement guidelines for material that is Educational, Documentary,

Scienti c, and/or Artistic (EDSA). Content that falls under those exceptions are crucial to understanding

the world and to chronicling history, whether it is documenting wars and revolutions, or artistic

expression that may include nudity. Consequently, Google takes great care in helping reviewers

understand the EDSA exceptions when reviewing  agged content.