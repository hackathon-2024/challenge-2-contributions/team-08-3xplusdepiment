platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Monetised product guidelines are the policies and standards related to products Google earns revenue

from and cover what can or cannot be monetised. These policies empower and protect users while

promoting a thriving digital ecosystem that is safe and conducive to innovation and growth.



Content moderation actions taken at Google’s “own initiative” are considered to be actions taken on

content shown to or  agged by those in the EU because the content violates our policies, or where the

content is illegal but action is not taken in response to an Article 9 order or Article 16 notice. These can

encompass both proactive and reactive enforcement actions. Proactive enforcement takes place when

Google employees, algorithms, or contractors  ag potentially policy-violating content. Reactive

enforcement takes place in response to external noti cations, such as user policy  ags or legal

complaints.