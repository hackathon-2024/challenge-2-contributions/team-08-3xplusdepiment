platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Sometimes, we make mistakes in our decisions to enforce our policies, which may result in the

unwarranted removal of content from or access to our services. To address that risk, where appropriate,

we make it clear to creators that we have taken action on their content and provide them the

opportunity to contest that decision through designated complaint-handling systems and give us

clari cations. In addition, under the DSA, EU users can submit complaints about an action that Google

did not take in response to a notice/ ag that they previously submi ed.



4.1 Number of complaints received

Article 15(1), point (d)



Table 4.1.1 re ects the number of content moderation complaints received from creators and users

located in EU Member States during the reporting period, broken down by service.



Table 4.1.1: Number of complaints received, by service



Service Number of complaints received



Search 642



Ads on Search 179,151



Maps 130,694



Ads on Maps 3,873



Play 2,357



Ads on Play 2,932