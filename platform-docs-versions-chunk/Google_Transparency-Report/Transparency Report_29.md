platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken



Ad Friendly Guidelines Violation 33,459



Age Restricted 325,290



Channel-level Termination Video Removals1 469,889



Child Safety 40,249



Harassment / Cyberbullying 3,933



Harmful / Dangerous 13,288



Hateful / Abusive 4,522



Misinformation 2,474



Nudity / Sexual 6,320



Promotion of Violence and Violent Extremism 2,232



Violent / Graphic 2,958



Other 30,671



Total 935,285



Notes:



1 This re ects the number of videos removed from the YouTube pla orm when the associated YouTube channel

was terminated.



Table 3.1.1.i: Own initiative actions taken on advertisements presented on

YouTube, by type of illegal content or violation of terms and conditions



Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken



Healthcare and Medicine 71,155



Intellectual Property Infringements 163,576



Pornography / Sexualised Content 2,298