platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

In addition to using automated processes related to CSAM discussed above, Google Search uses

automated measures to detect racy, commercial, violent, and profane content globally on its ‘Discover’

feature. Precision of each automated process during the reporting period (28 August 2023 to 10

September 2023) was as follows:



● Detection of violative racy content, globally: 85%;

● Detection of violative commercial content, globally: 73%;

● Detection of violative violent content, globally: 90%; and

● Detection of violative profane content, globally: 85%.