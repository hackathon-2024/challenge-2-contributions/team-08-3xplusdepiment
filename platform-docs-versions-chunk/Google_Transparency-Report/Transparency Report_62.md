platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>





YouTube strives to prevent content that violates our policies from being widely viewed—or viewed at

all—before it is removed. As the overwhelming majority of violative content is detected by automated

systems, YouTube’s Violative View Rate (VVR) is a good indication of how well our automated systems

are protecting our community. VVR is an estimate of the proportion of video views that violate our

Community Guidelines in a given quarter (excluding spam). In order to calculate VVR, we take a sample

of the views on YouTube and send the sampled videos for review. Once we receive the decisions from

reviewers about which videos in the sample are violative, we aggregate these decisions in order to arrive

at our estimate. In Q2 2023, VVR was 0.09-0.10% globally. This means that out of every 10,000 views on

YouTube in Q2, only 9-10 came from violative content. Additional information about the VVR

methodology is available in the YouTube Community Guidelines enforcement transparency report and a

third-party statistical assessment commissioned by Google.