platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Mixed

result2

Issue

resolved Other



Maps 30,670 63,610 N/A 28 0



Ads on Maps 2,521 1,296 0 0 0



Play 1,044 88 N/A 515 0



Ads on Play 2,378 226 0 0 0



Shopping3 15,209 61,117 0 3,468 0



YouTube 11,217 23,2344 N/A 1,420 472



Ads on YouTube 40,627 32,129 0 0 0



Multi-Services5 30,283 3,864 N/A 5,299 0



Notes:



1 Not all complaints can be resolved during the reporting period, therefore the total number of complaint

outcomes above will be less than the total number of complaints received (Table 4.1.1).



2 The ‘mixed result’ outcome only applies to complaints about Ads presented on each service; 0 outcomes fell

into this category for this reporting period.



3 Shopping metrics re ect complaints about content moderation actions taken on both unpaid content (e.g.,

free listings) and advertisements.