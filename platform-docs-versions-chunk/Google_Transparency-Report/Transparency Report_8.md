platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

2 Information in these reports is voluntarily provided and not necessarily directly comparable with information

presented in this mandated DSA report, due to di erences in methodologies.



EU DSA Report • 3

Section 2: Notices received through notice and action

mechanisms



Article 15(1), point (b)



Google’s content and product policies apply wherever you are in the world, but we also have processes

in place to remove or restrict access to content based on local laws. Users, Trusted Flaggers (as de ned

by Article 22) and other entities can report content that they would like to be removed from Google's

services under applicable laws. Action is taken on content that is deemed to violate applicable laws or

Google policies.



2.1 Number of notices submi ed in accordance with Article 16, broken

down by type of alleged illegal content concerned

Article 15(1), point (b)