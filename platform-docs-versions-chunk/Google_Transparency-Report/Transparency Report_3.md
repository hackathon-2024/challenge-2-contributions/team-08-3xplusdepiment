platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

As such, our product, policy, and enforcement decisions are guided by a set of principles which enable

us to preserve freedom of expression, while curbing the spread of content that is damaging to users and

society.



1. We value openness and accessibility: We lean towards keeping content accessible by

providing access to an open and diverse information ecosystem.

2. We respect user choice: If users search for content that is not illegal or prohibited by our

policies, they should be able to  nd it.

3. We build for everyone: Our services are used around the world by users from di erent

cultures, languages, and backgrounds, and at di erent stages in their lives. We take the diversity

of our users into account in policy development and policy enforcement decisions.



These principles are addressed in three key ways, providing our users with access to trustworthy

information and content.