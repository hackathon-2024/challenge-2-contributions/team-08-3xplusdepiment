platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Account level restrictions may be imposed as a result of multiple legal or policy violations across one or

more services. Where possible, these restrictions are a ributed to the service associated with the  nal

violation that led to the restriction being imposed.



EU DSA Report • 19

Table 3.1.3: Number of measures taken at Google’s own initiative, by service and

type of restriction applied



Service



Number of measures taken



Restrictions

of the

visibility of

content



Demonetisation



Partial

service

level

suspension



Account

level

termination¹



Search



Domain Level Actions 1,638,528 N/A3 N/A4 N/A5



Host Level Actions 1,255 N/A3 N/A4 N/A5



URL Level Removals 31,877,226 N/A3 N/A4 N/A5



URL Level Filtering 61,847,459 N/A3 N/A4 N/A5



Incident Level Actions 2,170 N/A3 N/A4 N/A5



Partner Feed Item Level Actions 3,390 N/A3 N/A4 N/A5



Ads on Search 2,137,206 N/A3 N/A4 8,305



Maps 1,950,211 N/A3 1,935 N/A5



Ads on Maps 71,688 N/A3 N/A4 88



Play 461,409 N/A3 572 1,477