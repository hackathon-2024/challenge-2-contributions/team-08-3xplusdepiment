platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 16

Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken



Scams and/or Fraud 204,616



Scope of Pla orm Service 338,683



Unsafe and/or Illegal Products 27,685



Total 808,013



Table 3.1.1.j: Own initiative actions taken on Multiple Services, by type of illegal

content or violation of terms and conditions



Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken1



Protection of Minors 2,079



Scams and/or Fraud 2,757



Spam2 115,212



Total 120,048



Notes:



1 These measures re ect Google-wide account-level terminations (i.e., termination of access to all Google

products and services).



2 This metric does not refer to any possible actions taken to combat webspam in Google Search.