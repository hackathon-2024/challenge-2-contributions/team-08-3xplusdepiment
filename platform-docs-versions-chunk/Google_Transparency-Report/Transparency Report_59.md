platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

1. Flagging, removing, or restricting inappropriate content: YouTube uses smart detection

technology to detect content that may violate YouTube’s policies and sends it for human review.

In some cases, that same technology automatically takes an action, which could include

removing or restricting content (e.g., age-restrict content not suitable for all audiences), limiting

content’s monetisation eligibility, or applying a strike to a channel.