platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Table 2.1.1 re ects the number of notices submi ed by EU-based users and other entities in accordance

with Article 16 during the reporting period, broken down by type of alleged illegal content and service.



Table 2.1.1: Number of Article 16 notices submi ed, by type of alleged illegal

content and service



EU DSA Report • 4



Type of alleged

illegal content



Number of Article 16 notices



Maps Play Shopping YouTube Multi-

Services1



Child Sexual Abuse

and Exploitation 4 0 0 235 1



Circumvention 0 0 0 6 0



Copyright 36 79 17 36,568 112



Counterfeit 0 1 6 429 7



Defamation 14,627 6 4 1,768 2



Hate and

Harassment 2 0 0 767 0



Privacy 235 3 1 636 0



Trademark 0 56 1 653 171

2.2 Number of Article 16 notices submi ed by Trusted Flaggers

Article 15(1), point (b)