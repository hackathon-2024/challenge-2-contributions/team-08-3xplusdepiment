platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Notes:

1 Content can be posted by users globally or reviewed by content moderators located globally. A single content

moderator can be assigned content posted in several di erent languages for review. In some cases and where

appropriate, translation tools may be used to assist in the review process. Accordingly, these metrics do not

necessarily re ect the number of content moderators who speak each EU Member State language.



2 With the exception of Google Maps, metrics relate to content posted in o cial EU languages. The number of

content moderators who completed reviews of content posted in each language should not be aggregated as

this may not re ect the total number of unique content moderators available to conduct reviews.



3 For Google Maps, the metrics re ect the number of content moderators whose primary language of review is

an EU language and who reviewed at least one piece of content during the period from 28 August 2023 to 10

September 2023.