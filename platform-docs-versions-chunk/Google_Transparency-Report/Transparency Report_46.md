platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Google uses a combination of automated and human evaluation to detect and remove ads which violate

our policies and are harmful to users and the overall ecosystem. Our enforcement technologies may use

automated evaluation, modelled on human reviewers’ decisions, to help protect our users and keep our

ad pla orms safe. The policy-violating content is either removed by automated means or, where a more

nuanced determination is required, it is  agged for further review by trained operators and analysts who

conduct content evaluations that might be di cult for algorithms to perform alone, for example

because an understanding of the context of the ad is required. The results of these manual reviews are

then used to help build training data to further improve our machine learning models.