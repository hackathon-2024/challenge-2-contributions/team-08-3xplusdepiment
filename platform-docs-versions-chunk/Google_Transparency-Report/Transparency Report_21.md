platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Illegal / Harmful Speech

URL Level Removals 1



Incident Level Actions 1



Inappropriate and Unhelpful Incident Level Actions 5



Intellectual Property

Infringements



Host Level Actions 1,255



URL Level Removals 30,456,401



Multiple Policy Violations

URL Level Filtering 4,508,249



Partner Feed Item Level Actions 24



Non-consensual Behaviour URL Level Removals 4,929



Online Bullying / Intimidation Incident Level Actions 6



Pornography / Sexualised

Content



URL Level Removals 30,084



URL Level Filtering 32,887,155



Incident Level Actions 19



Protection of Minors URL Level Removals 1,378,633



Risk for Public Security URL Level Removals 2



Scope of Pla orm Service

URL Level Filtering 4,836,895



Incident Level Actions 98



EU DSA Report • 10

Type of illegal content or

violation of terms and

conditions

Granularity Number of own initiative

measures taken



Partner Feed Item Level Actions 167



Spam Domain Level Actions 1,638,505