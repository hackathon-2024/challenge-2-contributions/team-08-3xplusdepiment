platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

3.3 Human Resources involved in Content Moderation

Article 42(2), points (a) and (b)



Human reviewers or content moderators play a key role in content moderation at Google. Although

technology has become very helpful in identifying some kinds of problematic content (e.g.,  nding

objects and pa erns quickly and at scale in images, video, and audio), humans are able to apply a more



EU DSA Report • 28

nuanced approach to assessing content. For example, algorithms cannot always tell the di erence

between terrorist propaganda and human rights footage or hate speech and provocative comedy.