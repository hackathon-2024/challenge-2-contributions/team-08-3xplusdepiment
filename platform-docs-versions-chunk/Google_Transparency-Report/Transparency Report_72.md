platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 30

Member State language between 1 January 2023 and 30 June 2023. The remaining metrics re ect the

number of content moderators who completed at least one review of content posted in an o cial EU

language during the period from 28 August 2023 to 10 September 2023. The metrics in Table 3.3.1 do

not necessarily re ect the language in which the content was ultimately reviewed; and with the

exception of Google Maps, they do not represent the number of content moderators hired to review in

each o cial EU language and should not be aggregated as this may not re ect the total number of

unique content moderators available to conduct reviews.



Table 3.3.1: Human resources evaluating content across the o cial EU Member

State languages, by service1, 2



Member

State

Language



Human resources evaluating content



Maps3 Play4, 5 Shopping4\. 5 Multi-

Services4, 5, 6 YouTube5, 7



Bulgarian 0 15 16 1,478 9



Croatian 0 25 17 1,279 24



Czech 0 69 156 1,830 31