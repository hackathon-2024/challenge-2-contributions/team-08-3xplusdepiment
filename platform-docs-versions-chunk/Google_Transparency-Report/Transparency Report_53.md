platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 24

Google Maps is reporting a single accuracy metric for each of the 26 o cial European Economic Area

(EEA) Member State languages, as well as an overall accuracy metric across all automated content

moderation decisions that is language-agnostic.



For each metric, accuracy is computed based on human evaluation of a random sample of all user

contributions, across data types and content types (e.g., reviews, media, facts, etc.) between 1 March

2023 and 31 August 2023. The accuracy for that slice is then de ned as the percentage of correct

decisions made by the automated system, assuming the human evaluation is the ground truth.



The accuracy of all automated content moderation decisions a ecting EEA users on Google Maps

between 1 March 2023 and 31 August 2023 was 90% (95% con dence interval: 84% to 94%). Accuracy

by EEA Member State language is provided in Table 3.2.3.



Table 3.2.3: Accuracy of automated measures on Google Maps, by EEA Member

State language