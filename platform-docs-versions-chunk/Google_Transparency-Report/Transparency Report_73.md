platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Danish 0 57 128 1,649 9



Dutch 0 131 188 2,419 24



English 2,797 2,290 632 6,402 15,142



Estonian 0 9 12 741 7



Finnish 0 38 94 1,290 15



French 2 396 236 3,222 176



German 3 302 204 3,350 231



Greek 0 24 104 1,545 28



Hungarian 0 37 123 1,398 25



Irish 0 0 0 7 0



Italian 2 184 175 2,651 91



Latvian 0 4 13 704 11



Lithuanian 0 26 14 951 11



EU DSA Report • 31

Member

State

Language



Human resources evaluating content



Maps3 Play4, 5 Shopping4\. 5 Multi-

Services4, 5, 6 YouTube5, 7



Maltese 0 0 0 22 0



Polish 0 169 151 2,558 99



Portuguese 3 328 196 3,784 464



Romanian 0 35 117 2,067 34



Slovak 0 26 100 1,234 5



Slovene 0 11 12 904 15



Spanish 2 520 218 4,227 507



Swedish 0 68 137 1,765 16



Unknown8 0 2,555 861 7,112 N/A9



Agnostic10 0 0 61 213 N/A9