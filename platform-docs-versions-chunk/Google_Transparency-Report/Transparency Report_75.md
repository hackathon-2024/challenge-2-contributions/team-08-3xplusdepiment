platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

4 For Play, Shopping and Multi-Services, the metrics re ect the number of content moderators who completed

at least one review of content posted in an o cial EU language during the reporting period (28 August 2023 to

10 September 2023).



5 Metrics do not represent the number of content moderators hired to review in each o cial EU language.



6 Content moderators who review content that may have appeared across multiple Google services, but not

necessarily a VLOP, are included under Multi-Services.



7 YouTube metrics cover the period from 1 January 2023 to 30 June 2023 and re ect the number of content

moderators who reviewed at least 10 videos posted in an EU language.



8 Content moderators who review content posted in a less common language (e.g., Breton, Basque, Occitan,

Catalan, or Corsican) or in multiple languages, or where there are limitations in reporting the content language,

are included in the ‘Unknown’ category.