platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Max is a valuable asset to the Google team, and he is dedicated to keeping the pla orm safe

through reviewing policy escalations. The work he does is essential to keeping digital pla orms safe

for everyone. However, some of the content he reviews can be emotionally taxing. During Max’s

onboarding training, he learned about the resources available to him, such as access to counselling,

limiting his exposure to content, physical well-being activities and quiet rooms, and post-exit

support should he decide to leave his position at Google.



\*While Max is a hypothetical content moderator, his training and experience are typical of many policy escalation specialists

at Google.



EU DSA Report • 33

Section 4: Complaints received through internal

complaint handling systems (i.e., appeals)

Article 15(1), point (d)