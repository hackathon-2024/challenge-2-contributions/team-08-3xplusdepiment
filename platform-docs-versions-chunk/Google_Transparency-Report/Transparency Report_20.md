platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Google services are wide-ranging and di er in their user bases, content hosted, services provided, and

expectations for enforcement. Where feasible, the high-level categories identi ed by the European

Commission for its DSA Transparency Database containing statements of reasons are used to group and

report policy enforcement actions. However, some policies do not fully align with these high-level

categories, and are thus reported using additional categories.



EU DSA Report • 9

Table 3.1.1.a: Own initiative actions taken on Google Search, by type of illegal

content or violation of terms and conditions1, 2, 3, 4, 5



Type of illegal content or

violation of terms and

conditions

Granularity Number of own initiative

measures taken



Data Defect6 Partner Feed Item Level Actions 3,199



Data Protection and Privacy

Violations URL Level Removals 7,176



Foreign Information

Manipulation and Interference Incident Level Actions 2



Healthcare and Medicine Incident Level Actions 3