platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

5 N/A indicates that account level termination is not an applicable enforcement action for this service.



6 Shopping metrics re ect content moderation actions taken on both unpaid content (e.g., free listings) and

advertisements.



3.2 Google’s use of automated tools

Article 15(1), point (e); Article 42(2), point (c)