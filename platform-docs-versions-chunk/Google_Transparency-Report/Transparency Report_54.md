platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Member State Language % Accuracy (95% Con dence Interval)



Bulgarian 88% (81% - 92%)



Croatian 92% (86% - 96%)



Czech 96% (92% - 99%)



Danish 92% (86% - 96%)



Dutch 94% (89% - 97%)



English 88% (81% - 92%)



Estonian 91% (85% - 95%)



Finnish 95% (90% - 98%)



French 96% (91% - 98%)



German 91% (85% - 95%)



Greek 90% (84% - 94%)



Hungarian 93% (87% - 96%)



Icelandic 89% (83% - 93%)



Irish 85% (78% - 90%)



Italian 96% (91% - 98%)



EU DSA Report • 25

Member State Language % Accuracy (95% Con dence Interval)



Latvian 87% (80% - 92%)



Lithuanian 89% (83% - 93%)



Maltese 85% (78% - 90%)



Norwegian 87% (80% - 92%)



Polish 87% (80% - 92%)



Portuguese 91% (84% - 95%)



Romanian 84% (77% - 89%)



Slovak 91% (85% - 95%)



Slovene 90% (84% - 94%)



Spanish 95% (90% - 98%)



Swedish 91% (85% - 95%)



3.2.4 Google Play