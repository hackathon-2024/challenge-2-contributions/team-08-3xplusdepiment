platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

● Providing access to on- and o -site counselling for workers who need it, dedicated wellness

spaces, on-site specialist counsellor support in certain Google o ces, and 24/7 phone support;

● Limiting content exposure for those focusing on sensitive content by providing guidance on

daily review time limits;

● Providing peer-led peer support groups and optional listening sessions if teams experience

escalations or speci c events that are particularly impac ul;

● Providing physical and mental wellbeing activities (e.g., gym space, workout classes,

mindfulness app access, educational sessions on a variety of topics);

● Providing post-exit mental health support, including counselling services, for one year a er an

employee who was regularly exposed to sensitive content and situations as part of their core

role exits their position at Google.