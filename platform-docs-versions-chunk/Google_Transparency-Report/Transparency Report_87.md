platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

1 Shopping metrics re ect content moderation actions taken on both unpaid content (e.g., free listings) and

advertisements.



2 The Multi-Services category includes complaints about Google account-level terminations, in addition to

complaints relating to actions taken in response to Article 16 notices that relate to advertisements appearing

on one or more Google services, which may include a VLOSE or VLOP.



EU DSA Report • 37

Section 5: Out-of-court dispute se lements



Article 24(1), point (a)



Out-of-court dispute se lement bodies are independent bodies with the means and expertise to

consider the issues brought to them. The DSA requires that each Member State certify out-of-court

se lement bodies to handle eligible disputes. No out-of-court se lement bodies have been certi ed at

this time and thus no out-of-court disputes were submi ed or resolved during the reporting period.



EU DSA Report • 38

Section 6: Article 23 Suspensions imposed to protect

against misuse