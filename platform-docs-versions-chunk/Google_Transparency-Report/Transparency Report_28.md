platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken



Animal Welfare 127,285



Data Defect 159,004,187



Healthcare and Medicine 146,112



Illegal / Harmful Speech 306



Intellectual Property Infringements 32,119



Negative E ects on Civic Discourse / Elections 295



Online Bullying / Intimidation 1



Pornography / Sexualised Content 6,890,349



Protection of Minors 1,438



Risk for Public Security 4,000



Scams and/or Fraud 41,725



Scope of Pla orm Service 14,072,178



Spam 40



Unsafe and/or Illegal Products 1,534,894



Violence 1



Other 387



Total 181,855,317



Notes:



1 Shopping metrics re ect content moderation actions taken on both unpaid content (e.g., free listings) and

advertisements.



EU DSA Report • 15

Table 3.1.1.h: Own initiative actions taken on YouTube, by type of illegal content or

violation of terms and conditions