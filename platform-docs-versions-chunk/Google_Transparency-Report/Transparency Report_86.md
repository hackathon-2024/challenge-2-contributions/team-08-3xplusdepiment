platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

5 The Multi-Services category includes complaints about Google account-level terminations, in addition to

complaints relating to actions taken in response to Article 16 notices that relate to advertisements appearing

on one or more Google services, which may include a VLOSE or VLOP.



4.4 Median time needed to action a complaint

Article 15(1), point (d)



Google works to provide complaint outcomes to users within a reasonable timeframe. The types of

complaints vary widely, with some requiring a longer review period due to varying degrees of

complexity or external factors (e.g., legally prescribed wait times). Table 4.4.1 re ects the median time, in

days, needed to action a complaint for each service.



Table 4.4.1: Median time needed to action a complaint, by service



Service Median time to action a complaint (days)



Maps 3



Ads on Maps <1



Play <1



Ads on Play <1



Shopping1 <1



YouTube <1



Ads on YouTube <1



Multi-Services2 <1



Notes: