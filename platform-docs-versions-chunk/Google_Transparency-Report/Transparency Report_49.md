platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

To ensure our algorithms meet high standards of relevance and quality, Google Search has a rigorous

process that involves both live tests and thousands of trained external Search Quality Raters from

around the world. Raters do not determine the ranking of an individual, speci c page or website, but

they help to benchmark the quality of Google Search’s results so that Google Search can meet a high



EU DSA Report • 23

bar for users all around the world. Under the Google Search Quality Rater Guidelines, raters are

instructed to assign the lowest rating to pages that are potentially harmful to users or speci ed groups,

misleading, untrustworthy, and spammy.



Google Search is providing precision metrics for the automated processes outlined below. Precision

metrics included in this section are de ned as the ratio of true positive instances (i.e., correct

automated decisions) as a proportion of both true and false positives.