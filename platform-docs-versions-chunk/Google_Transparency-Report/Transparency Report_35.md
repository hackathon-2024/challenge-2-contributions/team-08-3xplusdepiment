platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Ads on Play 2,134 N/A3 N/A4 14



Shopping6 181,844,046 N/A3 N/A4 11,271



YouTube 871,465 33,973 45 29,802



Ads on YouTube 804,859 N/A3 N/A4 3,154



Multi-Services N/A2 N/A3 N/A4 120,048



Notes:



1 Service-speci c account level terminations, where users are prevented from using the account for the

service’s main purpose, are re ected in the numbers for each service. The number of Google-wide

account-level terminations, where users can no longer log into any Google products or services is re ected in

Multi-Services.



2 N/A indicates that restrictions of the visibility of content is not an applicable enforcement action for

Multi-Services.



EU DSA Report • 20

3 N/A indicates that demonetisation, by itself, is not an applicable enforcement action for this service. However

in some cases, a di erent enforcement action (e.g., suspension) may prevent features from being monetised.



4 N/A indicates that partial service level suspension is not an applicable enforcement action for this service.