platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Table 4.2.1 re ects the number of complaints, broken down by service and by the complaint reason (i.e.,

on the basis that either action was taken against the content or account, or Google did not action the

request to remove content or disable access). For complaints on the basis of action taken, the appellant

is likely to be the content or account owner whereas for complaints on the basis of non-action taken, the

complainant is likely to be the individual or entity who originally  agged the content as potentially

violative.



Table 4.2.1: Number of complaints received, by service and complaint reason



Service



Number of complaints received on



Basis of action taken against

the content or account

Basis that the request to remove content

or disable access was not actioned



Maps 130,651 43



Ads on Maps 3,873 0



Play 2,356 1



Ads on Play 2,932 0



Shopping1 89,714 35



YouTube 37,216 230



Ads on YouTube 75,124 302



Multi-Services2 50,520 15



Notes: