platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

2 For Google Search, only some of these actions result in complete removal from search results (e.g.

“non-consensual behaviour” includes delistings under Google’s policies relating to highly personal information.)

Others apply only to certain Search features, such as Discover, Knowledge Graph or Featured Snippets, where

prominently surfacing content might cause undue surprise to users (e.g., “violence”). Others involve the

application of a ranking signal, for example, applying a demotion to domains that receive a high volume of valid

copyright removal notices.



3 Most, but not all, of policy-violating content on Google Search and its features is moderated globally. Most

content delisted from Google Search on legal grounds is content subject to copyright removal noti cations,

which are also processed globally. However, there are classes of delistings based on local law or local court

orders that a ect only certain country services, based on variance in laws between countries.