platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

EU DSA Report • 13

Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken



Online Bullying / Intimidation 12



Pornography / Sexualised Content 340



Protection of Minors 615



Risk for Public Security 9



Scams and/or Fraud 1,229



Scope of Pla orm Service 4,565



Spam 352,931



Violence 5



Other 185



Total 463,458



Table 3.1.1.f: Own initiative actions taken on advertisements presented on Google

Play, by type of illegal content or violation of terms and conditions



Type of illegal content or violation of terms and

conditions

Number of own initiative measures

taken



Healthcare and Medicine 55



Intellectual Property Infringements 4



Scams and/or Fraud 328



Scope of Pla orm Service 1,430



Unsafe and/or Illegal Products 331



Total 2,148



EU DSA Report • 14

Table 3.1.1.g: Own initiative actions taken on Shopping (unpaid content and

advertisements)1, by type of illegal content or violation of terms and conditions