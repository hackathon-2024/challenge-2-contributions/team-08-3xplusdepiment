platform: Google
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Transparency-Report/Transparency Report.md
url: <EMPTY>

Total 14,906 146 29 42,090 325



Notes:



1 Notices relating to advertisements that may have appeared across multiple Google services, including VLOPs,

are included under Multi-Services.

Table 2.3.1: Number of actions taken in response to Article 16 notices, by

service and basis of the action1



Service Actions taken on legal grounds Actions taken on policy grounds



Maps 25,077 1,191



Play 36 57



Shopping 14 161



YouTube 32,522 5



Multi-Services2 107 96



Notes:



1 More than one action can be taken on an Article 16 notice.



2 Notices relating to advertisements that may appear across multiple Google services, including VLOPs, are

included under Multi-Services.



2.4 Number of Article 16 notices processed by automated means

Article 15(1), point (b)



During the reporting period, YouTube processed 20,157 Article 16 notices by automated means (i.e.,

with no human involvement). Article 16 notices are not processed by automated means for any of the

other VLOPs.