platform: Apple
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_DSA-Transparency-Report/DSA Transparency Report.md
url: https://www.apple.com/legal/dsa/transparency/eu/app-store/2310/


## Section 1: Orders Received from EU Member States[2](#two)

This section covers orders issued by EU Member States’ judicial or administrative authorities to act against illegal content in accordance with Article 9 of the DSA. These orders can be submitted through Content Reports ([ContentReports.apple.com](http://contentreports.apple.com/)) — the App Store notice and action mechanism developed in compliance with Article 16 — as well as through existing communication channels.[3](#three) The section also covers orders issued by EU Member States’ judicial or administrative authorities to provide information as defined by Article 10 of the DSA.

**Total number of EU Member State orders to act against illegal content: 1**

**Table 1.1:** EU Member State orders to act against illegal content categorised by the EU Member State issuing the order

|     | **Count of EU Member State orders** |
| --- | --- |
| Hungary | 1   |

**Table 1.2:** EU Member State orders to act against illegal content categorised by type of illegal content concerned

|     | **Count of EU Member State orders** |
| --- | --- |
| Provides or facilitates an illegal service | 1   |

EU Member State orders to act against illegal content — median time to confirm receipt: 0 days[4](#four)

EU Member State orders to act against illegal content — median time to give effect to the order: 5 days

**Total number of EU Member State orders to provide information: 8**

**Table 1.3:** EU Member State orders to provide information categorised by the EU Member State issuing the order

|     | **Count of EU Member State orders** |
| --- | --- |
| France | 3   |
| Germany | 1   |
| Hungary | 1   |
| Italy | 1   |
| Netherlands | 1   |
| Sweden | 1   |

EU Member State orders to provide information — median time to confirm receipt: 0 days

EU Member State orders to provide information — median time to give effect to the order: 3 days