platform: Apple
topic: DSA-Transparency-Report
subtopic: Developer Terms
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_DSA-Transparency-Report/Developer Terms.md
url: <EMPTY>


## Section 7: App Store Recipients of the Service

**Table 7.1:** Average monthly recipients of the App Store categorised by EU Member State

|     | **Count of monthly recipients of the App Store** |
| --- | --- | --- |
| Austria | 3 million |
| Belgium | 4 million |
| Bulgaria | Under 1 million |
| Croatia | Under 1 million |
| Cyprus | Under 1 million |
| Czechia | 2 million |
| Denmark | 4 million |
| Estonia | Under 1 million |
| Finland | 2 million |
| France | 24 million |
| Germany | 28 million |
| Greece | 2 million |
| Hungary | 2 million |     |
| Ireland | 2 million |
| Italy | 14 million |
| Latvia | Under 1 million |
| Lithuania | Under 1 million |
| Luxembourg | Under 1 million |
| Malta | Under 1 million |
| Netherlands | 8 million |
| Poland | 5 million |
| Portugal | 2 million |
| Romania | 2 million |
| Slovakia | Under 1 million |
| Slovenia | Under 1 million |
| Spain | 11 million |
| Sweden | 6 million |

1. Data in the report comes from EU Member State storefronts only.
2. For the purposes of this report, an “order” is a valid request from a Member State judicial or administrative authority in accordance with specific mandatory powers under the laws of the Member State in question, seeking production of information relating to the use of App Store services by one or more specific recipients of the service, or requiring action to be taken in respect of specific items of illegal content, regardless of whether it constitutes an order legally binding on Apple Distribution International Limited. This reporting is without prejudice to the legal position of Apple Distribution International Limited with regard to the binding nature of any such order, under applicable law.
3. For more information on accessing the Content Reports portal, please see Section 2.
4. All lengths of time are listed in calendar days.
5. This table displays the category of illegal content selected by the notifier. The selected category may not accurately reflect the content concerned.
6. Intellectual property rights notices are generally settled by the rights’ claimants themselves without requiring an action from the App Store.
7. Under DSA Article 22(5), the European Commission will publish in a publicly available database information regarding entities awarded the status of trusted flaggers by the Digital Services Coordinators; these Digital Services Coordinators are designated by the EU Member States in accordance with DSA Article 22(2). To date, no such information has been published.
8. For more information on the content moderation actions that the App Store takes, see Table 3.2. Some actions on reported notices may not be included in these figures because they were pending at the time that data was collected. Additionally, some notices are not related to the reporting of illegal content and are therefore not actionable.
9. The App Store terms and conditions — including the App Store Review Guidelines, the Apple Developer Program License Agreement (DPLA) and Apple Media Services (AMS) Terms and Conditions — prohibit illegal content on the App Store, as well as the use of the App Store for illegal purposes. As such, this figure may include actions taken against content that might also be incompatible with applicable laws.
10. Apple Search Ads is a separate business from the App Store, and it’s included in this report because its business involves selling advertising media on the App Store.
11. Table 3.1 lists human resources who are proficient in multiple languages under each applicable language.
12. This category includes ratings and reviews detected and removed solely through automated means.
13. This category includes terminations of App Store developers, AMS customers and Apple Search Ads advertisers.
14. This category includes ratings and reviews removed before and after publication.
15. This category includes apps that were live on the App Store when the developer account was terminated.
16. This category includes apps that were suppressed on App Store charts and in App Store search. It also includes apps blocked from being transferred to other developers.
17. This category includes user accounts that are restricted from leaving ratings or reviews on the App Store.
18. This removal is the result of ongoing cleanup for outdated apps.
19. The App Store has multiple complaint-handling systems from which data is included in this section. These systems cover complaints related to Content Reports notices, review removals, App Review Board (ARB) cases, platform-to-business (P2B) cases and advertisement restrictions. For more information on redress options, please see [apple.com/legal/dsa/en/redress-options.html](https://www.apple.com/legal/dsa/en/redress-options.html).
20. The App Store will seek to provide users filing complaints with additional flexibility to select a basis for complaint for future reports.
21. In accordance with Article 21(8) of the DSA, the European Commission will publish a list of certified out-of-court dispute settlement bodies certified by Digital Services Coordinators under DSA Article 21(3). To date, no such list has been published.