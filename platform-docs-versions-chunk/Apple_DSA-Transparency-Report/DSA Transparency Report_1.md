platform: Apple
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_DSA-Transparency-Report/DSA Transparency Report.md
url: https://www.apple.com/legal/dsa/transparency/eu/app-store/2310/

# DSA Transparency Report

## App Store

## October 2023

In accordance with Articles 15, 24 and 42 of the EU’s Digital Services Act (DSA), this transparency report provides information on orders and notices of illegal content received by the App Store and content moderation that the App Store has undertaken on its own initiative.[1](#one) This first report covers the reporting period between 27 August 2023 and 27 September 2023. Subsequent reports will cover successive six-month periods.