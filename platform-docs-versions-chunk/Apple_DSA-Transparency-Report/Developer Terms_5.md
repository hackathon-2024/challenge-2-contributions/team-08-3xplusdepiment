platform: Apple
topic: DSA-Transparency-Report
subtopic: Developer Terms
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_DSA-Transparency-Report/Developer Terms.md
url: <EMPTY>


## Section 4: Complaints[19](#nineteen)

**Total complaints received: 147**

Median time to take decisions: 11 days

**Table 4.1:** Complaints received categorised by the basis for the complaint[20](#twenty)

|     | **Count of complaints received** |
| --- | --- |
| Procedural complaints | 0   |
| Substantive complaints on the illegality or incompatibility | 147 |
| Restriction imposed is not diligent, objective, or proportionate | 0   |

**Table 4.2:** Complaints received categorised by decision taken

|     | **Count of complaints received** |
| --- | --- |
| Decision upheld | 53  |
| Decision reversed | 34  |
| Decision pending | 60  |

**Section 5: Out-of-Court Disputes**

Number of disputes submitted to out-of-court dispute settlement bodies referred to in Article 21: 0[21](#twentyone)

**Section 6: Suspensions for Misuse of the Service**

DSA Article 23(1) provides for the suspension of users who “frequently provide manifestly illegal content.” DSA Article 23(2) provides for the suspension of users who “frequently submit notices or complaints that are manifestly unfounded.”

Under its existing content moderation practices, and in accordance with its terms and conditions, the App Store will terminate — rather than merely suspend — the accounts of any user or developer who frequently provides manifestly illegal content in the form of apps, user reviews of published apps or other forms of illegal content. The App Store may suspend or terminate users who frequently submit Content Reports notices or related complaints that are manifestly unfounded.

**Total number of suspensions for provision of manifestly illegal content: 0**

**Total number of suspensions for submission of manifestly unfounded notices: 0**

**Total number of suspensions for submission of manifestly unfounded complaints: 0**