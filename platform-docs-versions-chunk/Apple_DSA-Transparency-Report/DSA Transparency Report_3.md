platform: Apple
topic: DSA-Transparency-Report
subtopic: DSA Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_DSA-Transparency-Report/DSA Transparency Report.md
url: https://www.apple.com/legal/dsa/transparency/eu/app-store/2310/


## Section 2: Notices Received Through Notice and Action Mechanism

The App Store notice and action mechanism in accordance with Article 16 is available at [ContentReports.apple.com](http://contentreports.apple.com/) or through the [Report a Problem tool](http://reportaproblem.apple.com/) and can be accessed by any individual or entity with an EU IP address.

**Total number of notices: 615**

**Table 2.1:** Notices categorised by type of alleged illegal content concerned[5](#five)

|     | **Count of notices** |
| --- | --- |
| Violates intellectual property rights[6](#six) | 547 |
| Violates consumer protection or privacy law | 19  |
| Provides or facilitates an illegal service | 17  |
| Child sexual abuse material | 8   |
| Violates advertising law | 4   |
| Illegal hate speech | 3   |
| Incites terrorism or violence | 0   |
| Other | 17  |

Notices submitted by trusted flaggers: 0[7](#seven)

Notices processed using automated means: 0

**Total number of notices on which the App Store took an action: 14[8](#eight)**

* Action taken on the basis of the law: 0
* Action taken on the basis of terms and conditions: 14[9](#nine)

Median time to take action: 8 days