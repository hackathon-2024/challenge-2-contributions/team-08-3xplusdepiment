platform: Apple
topic: DSA-Transparency-Report
subtopic: Developer Terms
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Apple_DSA-Transparency-Report/Developer Terms.md
url: <EMPTY>


## Section 3: App Store–Initiated Content Moderation

The App Store moderates three types of content for compliance with applicable law and terms and conditions: live apps that are published on the App Store, ads that are live on the App Store,[10](#ten) and user ratings and reviews and public developer responses (both before and after publication).

**Content moderation relating to published apps**

Before they’re published on the App Store — that is, before becoming subject to any content moderation decisions — all apps and app updates are subject to two levels of ex ante review by the App Review team (App Review): automated review and human review. After apps are published, the App Store continues to monitor them and can and does take action, including app takedowns, if it identifies apps that do not comply with applicable law, with the [App Store Review Guidelines](https://developer.apple.com/app-store/review/guidelines/) or with other applicable terms and conditions — namely the [Apple Developer Agreement](https://developer.apple.com/support/downloads/terms/apple-developer-agreement/Apple-Developer-Agreement-20230605-English.pdf) or the [Apple Developer Program License Agreement](https://developer.apple.com/support/terms/apple-developer-program-license-agreement/) (DPLA). The App Store may also take action if alerted to concerns about an app by third parties, including via Report a Problem or Content Reports.

Automated tools are used to assist App Review specialists in their ongoing monitoring of apps published on the App Store. This includes automated tools that detect malware or bait-and-switch apps that change their functionality after approval by App Review. It also includes automated tools that scan user reviews of published apps to identify concerns raised by consumers that may indicate that apps contain content incompatible with applicable law or terms and conditions. These tools are continually trained and enhanced to address new and emerging threats and to factor in learning from human-based decision-making. Any apps flagged as potentially problematic by these automated tools are escalated to human App Review specialists. These specialists then determine whether the apps violate App Store terms and conditions and, if so, what action to take — for example, app removal. No such actions are taken solely on the basis of automated tools.

App Review supports all official EU Member State languages ([see Table 3.1](#table3)). Each specialist receives language- and region-specific training that covers cultural and sensitivity issues as they relate to enforcing the App Store Review Guidelines. Specialists also participate in regular discussions on new issues or trends that arise in their particular regions.

All App Review specialists charged with the continuing review of apps published on the App Store receive comprehensive training when they join the App Review team. This training is supplemented by ongoing training to ensure that specialists remain informed of new and emerging threats and issues. They have access to senior App Review specialists for guidance and can escalate issues internally, including to the App Review Board.

**Content moderation relating to published ads**

Before they’re published on the App Store, ads are reviewed to ensure that they comply with [Apple Search Ads Advertising Policies](https://searchads.apple.com/policies). The App Store will also moderate ads after they’re published if it becomes aware that they’re in breach of the Apple Search Ads Advertising Policies.

Some automated tools are used to assist with moderating ads, but final decisions regarding content moderation are taken by humans.

Personnel charged with moderating published Apple Search Ads content receive comprehensive training when they join the team. This training is supplemented by ongoing training to ensure that personnel remain informed of new and emerging threats and issues.

**Content moderation relating to user ratings and reviews as well as developer responses**

Customers provide ratings and reviews on the App Store to give feedback on their experience with an app and to help others decide which apps they’d like to try. App developers can respond to user reviews regarding their apps. Some submitted ratings and reviews aren’t published on the App Store because automated tools are applied before they’re published to prevent reviews or responses that contain certain types of content — such as spam, fake reviews or profanity — from ever being published on the App Store. Nonetheless, we include these ratings and reviews in the relevant tables that follow.

All user ratings and reviews must comply with the Submissions Guidelines in the [Apple Media Services (AMS) Terms and Conditions](https://www.apple.com/legal/internet-services/itunes/). Ratings and reviews that do not comply with these terms and conditions can be removed from the App Store. All developer responses must comply with the Developer Code of Conduct in the App Store Review Guidelines, as well as with the DPLA. Responses that do not comply can also be removed from the App Store.

We use a combination of automated and human review to moderate this content. Any reviews that may violate the law or App Store terms and conditions are evaluated by human moderators. Automated tools are used to flag reviews with potential concerns for human moderators to consider; but because decisions are taken by human reviewers rather than by automated means, considerations of the accuracy or error rate of such automated tools do not apply. Human reviewers sometimes detect or are alerted by developers and customers to published reviews that contain new patterns of illegal content or content that doesn’t comply with the AMS Terms and Conditions. In these circumstances, human reviewers may run automated queries to detect other published reviews that contain such content.

The personnel responsible for moderating user ratings and reviews and developer responses both before and after publication on the App Store receive comprehensive training at onboarding. This training is supplemented by ongoing training to ensure that personnel remain informed of new and emerging threats and issues.

**Total number of human resources dedicated to content moderation: 607**

**Table 3.1: Human resources dedicated to content moderation categorised by supported language[11](#eleven)**

In accordance with Article 42(2)(a) of the DSA, the App Store must specify the human resources that it dedicates to content moderation, broken down by each official language of the EU Member States. The App Store does not maintain separate content moderation teams per EU Member State language, so it has detailed the number of content moderation human resources broken down by proficiency in EU Member State languages.

|     | **Count of dedicated human resources** |
| --- | --- |
| English | 607 |
| Spanish | 42  |
| Portuguese | 34  |
| German | 32  |
| French | 29  |
| Italian | 22  |
| Swedish | 14  |
| Danish | 13  |
| Polish | 10  |
| Slovak | 10  |
| Dutch | 9   |
| Bulgarian | 8   |
| Czech | 8   |
| Greek | 8   |
| Croatian | 7   |
| Hungarian | 6   |
| Romanian | 6   |
| Estonian | 5   |
| Finnish | 5   |
| Slovenian | 5   |
| Maltese | 4   |
| Latvian | 3   |
| Lithuanian | 3   |
| Irish | 2   |

**Total number of content moderation measures taken:** **1,000,804**

Number of content moderation measures taken detected solely using automated means: 221,255[12](#twelve)

**Table 3.2:** Content moderation measures taken categorised by type of restriction applied

|     | **Count of content moderation measures taken** |
| --- | --- |
| Accounts terminated[13](#thirteen) | 512,914 |
| Ratings or reviews removed[14](#fourteen) | 480,719 |
| Apps removed[15](#fifteen) | 7,129 |
| Apps restricted[16](#sixteen) | 27  |
| Ads removed | 10  |
| Accounts restricted[17](#seventeen) | 5   |

**Table 3.3:** Content moderation measures taken categorised by type of illegal content or violation of terms and conditions

|     | **Count of content moderation measures taken** |
| --- | --- |
| AMS Terms Section K — Prohibited Use of Service | 993,429 |
| App Store Review Guideline 4.0 — Design[18](#eighteen) | 5,456 |
| Apple DPLA Section 3.2(f) — Fraud | 1,756 |
| App Store Review Guideline 4.3 — Spam | 38  |
| App Store Review Guideline 5.6.3 — Discovery Fraud | 27  |
| App Store Review Guideline 3 — Business | 21  |
| App Store Review Guideline 4.1 — Copycats | 14  |
| App Store Review Guideline 2.3.1 — Accurate Metadata | 9   |
| App Store Review Guideline 5.6 — Developer Code of Conduct | 9   |
| App Store Review Guideline 2.1 — App Completeness | 5   |
| App Store Review Guideline 1.1 — Objectionable Content | 5   |
| Advertising Policies 3.4 — Adult Content | 4   |
| App Store Review Guideline 3.1.2 — Subscriptions | 4   |
| App Store Review Guideline 3.1.1 — In-App Purchases | 3   |
| App Store Review Guideline 5 — Legal | 3   |
| App Store Review Guideline 5.2.3 — Intellectual Property — Audio/Video Downloading | 3   |
| Advertising Policies 4.2 — Sensitive Content or Imagery | 2   |
| App Store Review Guideline 2.3 — Accurate Metadata | 2   |
| App Store Review Guideline 4.2 — Minimum Functionality | 2   |
| App Store Review Guideline 4.8 — Sign in with Apple | 2   |
| Advertising Policies 1 — Advertiser Responsibilities | 1   |
| Advertising Policies 3.5 — Controlled Substances | 1   |
| Advertising Policies 4.4 — Real Money Gambling | 1   |
| Advertising Policies 4.6 — Dating Services/Match Making | 1   |
| App Store Review Guideline 2.3.7 — Accurate Metadata | 1   |
| App Store Review Guideline 2.5.18 — Software Requirements | 1   |
| App Store Review Guideline 3.2.2 — Unacceptable Business Model | 1   |
| App Store Review Guideline 5.1.1 — Data Collection and Storage | 1   |
| App Store Review Guideline 5.2.1 — Intellectual Property — Generally | 1   |
| App Store Review Guideline 5.2.2 — Intellectual Property — Third-Party Sites/Services | 1   |