platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/content_publishing_limit

# IG User Content Publishing Limit

Represents an [IG User's](https://developers.facebook.com/docs/instagram-api/reference/ig-user) current [content publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) usage.

## Creating

This operation is not supported.

## Reading

**`GET /{ig-user-id}/content_publishing_limit`**

Get the number of times an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has published and [IG Container](https://developers.facebook.com/docs/instagram-api/reference/ig-container) within a given time period. Refer to the [Content Publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) guide for complete publishing steps.