platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/content-publishing

### Public Server

We cURL media used in publishing attempts so the media must be hosted on a publicly accessible server at the time of the attempt.

### Page Publishing Authorization

Instagram Professional accounts connected to a [Page](https://developers.facebook.com/docs/instagram-api/overview#pages) that requires [Page Publishing Authorization](https://www.facebook.com/business/m/one-sheeters/page-publishing-authorization) (PPA) cannot be published to until PPA has been completed.

It's possible that an app user may be able to perform [Tasks](https://developers.facebook.com/docs/instagram-api/overview#tasks) on a Page that initially does not require PPA but later requires it. In this scenario, the app user would not be able to publish content to their Instagram Professional account until completing PPA. Since there's no way for you to determine if an app user's Page requires PPA, we recommend that you advise app users to preemptively complete PPA.