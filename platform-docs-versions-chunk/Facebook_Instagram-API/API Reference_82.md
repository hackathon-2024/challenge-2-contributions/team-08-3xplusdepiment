platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-media/product_tags

## Reading

**`GET /{ig-media-id}/product_tags`**

Get a collection of product tags on an [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media). See the [Product Tagging](https://developers.facebook.com/docs/instagram-api/guides/product-tagging) guide for complete product tagging steps.

### Limitations

* Instagram Creator accounts are not supported.
* Stories, Instagram TV, Reels, Live, and Mentions are not supported.