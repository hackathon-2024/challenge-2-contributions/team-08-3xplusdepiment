platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-media/insights

### Sample Post Metric Request

curl -i -X GET \\
 "https://graph.facebook.com/`v19.0`/17932174733377207/insights?metric=profile\_activity&breakdown=action\_type&access\_token=EAAOc..."