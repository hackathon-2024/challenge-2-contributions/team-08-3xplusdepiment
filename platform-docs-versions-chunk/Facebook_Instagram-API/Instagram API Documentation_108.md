platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## November 9, 2022

#### Instagram Webhooks

_Applies to all versions._

The `ad_id` and `ad_title` will be returned in the `media` object of the `comments` field's `value` object when a person comments on a [boosted Instagram post or Instagram ads post](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F1067656009937668&h=AT1YcsU9P6E_0nZ8fYZ8siJgm-mgyXiwrdAfR8GKeD-Rbk6iqLUbgVoeQFhN1IvvTw9yJG8olJTvT7VBsxw3nmZoSCQV0-xhtx3P2ssIUUSQHYumHVGdwGEgAlCmV_G8k_RnUKfPJSpA1Fzww_EWPw).

[](#)

## October 31st

#### Reels – Product Tags

_Applies to all versions._

Instagram Product Tagging API for Reels is made available. You can tag up to 30 products when publishing a reel.

[](#)