platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

## Pages

[Instagram Professional accounts](#instagram-professional-accounts) must be connected to a [Facebook Page](https://www.facebook.com/business/pages) before their data can be accessed through the API. Once connected, any Facebook User who is able to perform [Tasks](#tasks) on that Page can grant your app an access token, which can then be used in API requests.

Our [Add or change the Facebook Page connected to your Instagram professional account](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F570895513091465&h=AT2HEcHWcyPZeVpermcRPxII_5_Lhg6_FK-6BEqM4qrHoWAkEjFaED9m1alRYYvTeMPMBTMQPFaRHYHfWZ4F09OZu9_wyzZyWTLazkBksP4SUS0AJMcyxEvAZn9AGgJC59nRwARq6k8yByM6W5zV2Q) help article explains how to connect to a Facebook Page to an Instagram Professional account.

[](#)