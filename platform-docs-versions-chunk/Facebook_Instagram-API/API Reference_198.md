platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/tags

# Tags

Represents a collection of [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects in which an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has been tagged by another Instagram user.

## Creating

This operation is not supported.

## Reading

**`GET /{ig-user-id}/tags`**

Returns a list of [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects in which an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has been tagged by another Instagram user.

### Limitations

Private [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects will not be returned.