platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/mentions

# IG User Mentions

This edge allows you to create an [IG Comment](https://developers.facebook.com/docs/instagram-api/reference/ig-comment) on an [IG Comment](https://developers.facebook.com/docs/instagram-api/reference/ig-comment) or captioned [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) object that an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has been @mentioned in by another Instagram user.

## Creating