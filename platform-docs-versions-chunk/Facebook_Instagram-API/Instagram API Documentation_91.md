platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/webhooks

The following content is from the [Webhooks product documentation](https://developers.facebook.com/docs/graph-api/webhooks). Please refer to the Webhooks documentation if you are unfamiliar with Webhooks.

# Set Up Webhooks for Instagram

Webhooks for [Instagram](https://developers.facebook.com/docs/instagram-api) allow you to receive real-time notifications whenever someone comments on the Media objects of your app users; [@mentions](https://developers.facebook.com/docs/pages/mentions) your app users; or when Stories of your app users expire.

[](#)