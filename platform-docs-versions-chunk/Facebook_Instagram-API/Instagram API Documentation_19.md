platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/getting-started

# Getting Started

This document explains how to successfully call the Instagram Graph API with your app and get an Instagram Business or Creator Account's media objects. It assumes you are familiar with the [Graph API](https://developers.facebook.com/docs/graph-api) and [Facebook Login](https://developers.facebook.com/docs/facebook-login), and know how to perform REST API calls. If you do not have an app yet, you can use the [Graph API Explorer](https://developers.facebook.com/tools/explorer) instead and skip steps 1 and 2.