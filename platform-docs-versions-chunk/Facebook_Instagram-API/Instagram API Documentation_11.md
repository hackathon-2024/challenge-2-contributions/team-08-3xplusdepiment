platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

## Collaborators

The [Instagram Collab](https://www.facebook.com/help/instagram/291200585956732) feature allows Instagram app users to co-author content (i.e. publish media) with other accounts (collaborators).

With a few exceptions, data on or about co-authored media can only be accessed through the API by the user who published the media; collaborators are unable to access this data via the API. The only exceptions are when searching for top performing media or recently published media that has been tagged with a specific hashtag. See [Hashtag Search](https://developers.facebook.com/docs/instagram-api/guides/hashtag-search).

[](#)