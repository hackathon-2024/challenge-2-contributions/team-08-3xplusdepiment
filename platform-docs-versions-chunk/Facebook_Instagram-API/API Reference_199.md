platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/tags


### Requirements

| Type | Description |
| --- | --- |
| [Access Tokens](https://developers.facebook.com/docs/facebook-login/access-tokens) | [User](https://developers.facebook.com/docs/facebook-login/access-tokens#usertokens) |
| [Features](https://developers.facebook.com/docs/apps/review/feature) | Not applicable. |
| [Permissions](https://developers.facebook.com/docs/apps/review/login-permissions) | [`instagram_basic`](https://developers.facebook.com/docs/facebook-login/permissions#reference-instagram_basic)  <br>[`instagram_manage_comments`](https://developers.facebook.com/docs/facebook-login/permissions#reference-instagram_manage_comments)  <br>[`pages_read_engagement`](https://developers.facebook.com/docs/pages/overview#permissions)<br><br>[`pages_show_list`](https://developers.facebook.com/docs/pages/overview#permissions)<br><br>  <br><br>If the token is from a User whose Page role was granted via the Business Manager, one of the following permissions is also required: [`ads_management`](https://developers.facebook.com/docs/facebook-login/permissions#reference-ads_management), [`business_management`](https://developers.facebook.com/docs/facebook-login/permissions#reference-business_management), or [`pages_read_engagement`](https://developers.facebook.com/docs/pages/overview#permissions). |
| [Tasks](https://developers.facebook.com/docs/instagram-api/overview#tasks) | The app user must be able to perform appropriate Tasks on the Page based on the Permissions requested by the app. |