platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/available_catalogs

# IG User Available Catalogs

Represents a collection of product catalogs in an [IG User's](https://developers.facebook.com/docs/instagram-api/reference/ig-user) [Instagram Shop](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F1187859655048322%2F&h=AT3KWWxxQZxTFzhzHRN-CBdYGsgRoi1bS8IBr_VaLjDVvMsnC_SpqUaV7emScGimHxdxbqFoORZ4LDINRxxoU4WkGgL_Bz_d1Af3w3_3_I18keHlmeDClFY3MyE6fyZHPCOtqiAZDq9SnWEW). See [Product Tagging](https://developers.facebook.com/docs/instagram-api/guides/product-tagging) guide for complete usage details.

## Creating

This operation is not supported.

## Reading

**`GET /{ig-user-id}/available_catalogs`**

Get the product catalog in an [IG User's](https://developers.facebook.com/docs/instagram-api/reference/ig-user) [Instagram Shop](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F1187859655048322%2F&h=AT2qZXlkFBlByDsdqWKpf0xUIH-alER1PjspjAsRix9BBDlrs41VP5mBhOr6W8RB5fU3FEtVUG4qB9ZkUtNhCSHmKyedpy9mA1Bx9EPM-7rYV_U91T2iI10yWOwZfNHGrN7uJH7YiR9KnTdR).