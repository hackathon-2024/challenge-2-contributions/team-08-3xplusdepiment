platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/insights

### Sample Interaction Metric Request

curl -i -X GET \\
  "https://graph.facebook.com/`v19.0`/17841405822304914/insights?metric=reach&period=day&breakdown=media\_product\_type&metric\_type=total\_value&since=1658991600&access\_token=EAAOc..."