platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## March 16, 2021

IGTV media is [now supported in v10.0+](https://developers.facebook.com/blog/post/2021/03/15/igtv-media-mmetrics-instagram-graph-api/). This applies to all endpoints except those used for content publishing and webhooks. To support this change, new `media_product_type` and `video_title` fields have been added to the [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) node. IGTV media must have been shared to Instagram at the time of publish (**Post a Preview** or **Share Preview** to Feed enabled) in order to be accessible via the API.

[](#)

## Januray 26, 2021

The Content Publishing beta has ended and all developers can now publish media on Instagram Professional accounts. Refer to the [Content Publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) guide for usage details.

[](#)