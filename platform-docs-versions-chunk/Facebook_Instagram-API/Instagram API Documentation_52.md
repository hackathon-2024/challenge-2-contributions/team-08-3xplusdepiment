platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/comment-moderation


## Endpoints

The API consists of the following endpoints. Refer to each endpoint's reference documentation for parameter and permission requirements.

* [`GET /{ig-media-id}/comments`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/comments#reading) — Get comments on an IG Media.
* [`GET /{ig-comment-id}/replies`](https://developers.facebook.com/docs/instagram-api/reference/ig-comment/replies#read) — Get replies on an IG Comment.
* [`POST /{ig-comment-id}/replies`](https://developers.facebook.com/docs/instagram-api/reference/ig-comment/replies#create) — Reply to an IG Comment.
* [`POST /{ig-comment-id}`](https://developers.facebook.com/docs/instagram-api/reference/ig-comment#update) — Hide/unhide an IG Comment.
* [`POST /{ig-media-id}`](https://developers.facebook.com/docs/instagram-api/reference/ig-media#update) — Disable/enable comments on an IG Media.
* [`DELETE /{ig-comment-id}`](https://developers.facebook.com/docs/instagram-api/reference/ig-comment#delete) — Delete an IG Comment.

[](#)