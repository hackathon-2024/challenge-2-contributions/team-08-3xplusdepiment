platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/product-tagging

## Limitations

* All [content publishing limitations](https://developers.facebook.com/docs/instagram-api/guides/content-publishing#limitations) apply to product tagging.
* Product tagging is not supported for Stories and Live.
* Product tagging is not supported for Instagram Creator accounts.
* Accounts are limited to 25 tagged media posts within a 24 hour period. Carousel albums count as a single post.

[](#)