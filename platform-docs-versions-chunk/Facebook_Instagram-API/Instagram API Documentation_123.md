platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## November 10, 2020

* **IG User Insights** — The [`follower_count`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/insights) values now align more closely with their corresponding values displayed in the Instagram app. In addition, [`follower_count`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/insights) now returns a maximum of 30 days of data instead of 2 years. This change applies to v9.0+ and will apply to all versions May 9, 2021.

[](#)