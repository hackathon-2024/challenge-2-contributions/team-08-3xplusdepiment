platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

# Overview

The Instagram Graph API is a collection of Facebook Graph API endpoints that allow apps to access data in [Instagram Professional accounts](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F502981923235522&h=AT0frtnvT6ee8vwu7kSGN3CZSVokxbtHqlIBTs8nAX0lKVX-1TVDC1MVl1xVubinZmr8hSGgfz2LftZk6WBMhAJtggfZRUGig8ojJ-vncYTte_DBQGSqmNh70QcAA_FOFiuwG0IxQKpiHAH2DS08gA) (both Business and Creator accounts). If you are unfamiliar with the Facebook Graph API, please read our [Graph API documentation](https://developers.facebook.com/docs/graph-api/) before proceeding.

## Base URL

All endpoints can be accessed via the `graph.facebook.com` host.

[](#)