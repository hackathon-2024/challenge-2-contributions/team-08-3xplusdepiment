platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/getting-started

## Before You Start

You will need access to the following:

* An [Instagram Business Account](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F502981923235522&h=AT3WpEyPfxmyxB3Cl2zk58Ev0lYrWEwKD_lq9izP3RDglw8JfU-sWdwTWiz8aOSmJD7nrcvsjHHIfiA5UZAw7iSHLprrUqxMu2m4PIG2v5_L1fekchwA3ye0yZ6Xm8qXgk8ENIAXCA_NEs1JSpn1QQ) or [Instagram Creator Account](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F1158274571010880&h=AT2ifRHWu2A0LcdNqvwshZdzpujkaIVj8lx4C_ffFac59Zf6Ts3W5R4gkmXlCuuvuTqESFFsyU1K6zHcJN6_WKSEeFtEMCOV4ZZjI3yxK_234-8qoZU_Xa0bebpT5JhG86Qlrl07-DgXOiNYt7OVjQ)
* A [Facebook Page connected to that account](https://developers.facebook.com/docs/instagram-api/overview#pages)
* A Facebook Developer account that can perform [Tasks on that Page](https://developers.facebook.com/docs/instagram-api/overview#tasks)
* A [registered Facebook App](https://developers.facebook.com/docs/development/register) with **Basic** settings configured

[](#)