platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/business-discovery

# Business Discovery

You can use the Instagram Graph API to get basic metadata and metrics about other Instagram Business and Creator Accounts.

## Limitations

Data about age-gated Instagram Business Accounts will not be returned.

[](#)

## Endpoints

The API consists of the following endpoints. Refer to the endpoint's reference documentation for parameter and permission requirements.

* [`GET /{ig-user-id}/business_discovery`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/business_discovery)

[](#)

## Examples