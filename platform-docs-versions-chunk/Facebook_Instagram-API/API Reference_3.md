platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference

## Root Edges

| Root Edge | Actions |
| --- | --- |
| [ig\_hashtag\_search](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag-search) | Gets the ID of an [IG Hashtag](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag). |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)