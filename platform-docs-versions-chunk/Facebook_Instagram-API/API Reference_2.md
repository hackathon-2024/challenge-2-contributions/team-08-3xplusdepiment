platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference

## Nodes

| Node | Actions |
| --- | --- |
| [IG Comment](https://developers.facebook.com/docs/instagram-api/reference/ig-comment/) | Represents an Instagram comment. |
| [IG Container](https://developers.facebook.com/docs/instagram-api/reference/ig-container/) | Represents a media container for publishing an Instagram post. |
| [IG Hashtag](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag/) | Represents an Instagram hashtag. |
| [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media/) | Represents an Instagram photo, video, story, or album. |
| [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user/) | Represents an Instagram Business Account or Instagram Creator Account. |
| [Page](https://developers.facebook.com/docs/instagram-api/reference/page/) | Represents a Facebook Page. |