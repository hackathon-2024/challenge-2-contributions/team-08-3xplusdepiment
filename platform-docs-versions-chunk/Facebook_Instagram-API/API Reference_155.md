platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/media

### Story Image Specifications

* Format: JPEG
* File size: 8 MB maximum.
* Aspect ratio: We recommended 9:16 to avoid cropping or blank space
* Color Space: sRGB. Images using other color spaces will have their color spaces converted to sRGB

### Story Video Specifications

* Container: MOV or MP4 (MPEG-4 Part 14), no edit lists, moov atom at the front of the file.
    
* Audio codec: AAC, 48khz sample rate maximum, 1 or 2 channels (mono or stereo).
    
* Video codec: HEVC or H264, progressive scan, closed GOP, 4:2:0 chroma subsampling.
    
* Frame rate: 23-60 FPS.
    
* Picture size:
    
    * Maximum columns (horizontal pixels): 1920
        
    * Required aspect ratio is between 0.1:1 and 10:1 but we recommend 9:16 to avoid cropping or blank space
        
    
* Video bitrate: VBR, 25Mbps maximum
    
* Audio bitrate: 128kbps
    
* Duration: 60 seconds maximum, 3 seconds minimum
    
* File size: 100MB maximum