platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/comment-moderation

# Comment Moderation

You can use the Instagram Graph API to get comments, reply to comments, delete comments, hide/unhide comments, and disable/enable comments on IG Media owned by your app users.

You can use the [Instagram Messaging API](https://developers.facebook.com/docs/messenger-platform/instagram) to send private replies (direct messages) to users who have commented on your app users' live video IG Media. Refer to the Instagram Messaging's [private replies](https://developers.facebook.com/docs/messenger-platform/instagram/features/private-replies) documentation to learn how.