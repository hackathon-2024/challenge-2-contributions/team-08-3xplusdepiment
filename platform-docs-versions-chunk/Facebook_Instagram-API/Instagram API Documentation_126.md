platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## May 9, 2019

* **Webhooks** — The `story_insights` field now requires the `instagram_manage_insights` permission instead of `instagram_manage_comments`.

[](#)

## October 31, 2018

* **Hashtag Search API** — You can now search for media tagged with specific hashtags by using our new [Hashtag Search API](https://developers.facebook.com/docs/instagram-api/guides/hashtag-search). `#spooky`!

[](#)

## October 23, 2018

* `/{ig-media-id}/comments` edge — `GET` requests made using API version 3.1 or older will have results returned in chronological order. Requests made using version 3.2+ will have results returned in reverse chronological order.

[](#)

## June 7, 2018

* `/{ig-media-id}` node — You can now use field expansion to get the `permalink` field on media objects.

[](#)