platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-media/product_tags

### Response

An object indicating success or failure.

{
  "success": {success}
}

#### Response Contents

| Property | Value |
| --- | --- |
| `success` | Returns `true` if able to delete the specified product tags on the [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media), otherwise returns `false`. |