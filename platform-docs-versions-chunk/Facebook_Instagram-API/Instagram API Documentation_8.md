platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

## Authentication

App user authentication is handled through access tokens. Instagram Professional accounts are accessed indirectly through Facebook accounts, so all API requests must include your app users's Facebook [User access token](https://developers.facebook.com/docs/facebook-login/access-tokens/#usertokens). You can get tokens from app users by implementing [Facebook Login](https://developers.facebook.com/docs/facebook-login). Note that Facebook Login does not support Instagram credentials so app users must sign in using a Facebook account.

[](#)