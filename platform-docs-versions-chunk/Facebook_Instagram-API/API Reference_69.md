platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-media/insights


### Metrics

#### Post Metrics

The following metrics are available on image and video IG Media published as a Post. Album carousels and IGTV are not supported.

| Metric | Breakdown | Description |
| --- | --- | --- |
| `comments` | n/a | The number of comments on your post. |
| `follows` | n/a | The number of accounts that started following you. |
| `likes` | n/a | The number of likes on your post. |
| `profile_activity` | `action_type` | The number of actions people take when they visit your profile after engaging with your post. |
| `profile_visits` | n/a | The number of times your profile was visited. |
| `shares` | n/a | The number of shares of your post. |
| `total_interactions` | n/a | The number of likes, saves, comments and shares on your post minus the number of unlikes, unsaves and deleted comments. |

#### Story Metrics

The following metrics are available on IG Media published as a Story.

| Metric | Breakdown | Description |
| --- | --- | --- |
| `follows` | n/a | This is how many accounts started following you. |
| `navigation` | `story_navigation_action_type` | This is the total number of actions taken from your story. These are made up of metrics like exited, forward, back and next story. |
| `profile_activity` | `action_type` | The number of actions people take when they visit your profile after engaging with your story. |
| `profile_visits` | n/a | The number of times your profile was visited. |
| `shares` | n/a | The number of shares of your story. |
| `total_interactions` | n/a | The number of replies and shares for your story. |