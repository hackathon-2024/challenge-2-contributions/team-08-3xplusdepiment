platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-container

### Edges

There are no edges on this node.

### Response

A JSON-formatted object containing default and requested [fields](#fields).

{
  "{field}":"{value}",
  ...
}

### Sample Request

curl -X GET \\
  'https://graph.facebook.com/17889615691921648?fields=status\_code&access\_token=IGQVJ...'

### Sample Response

{
  "status\_code": "FINISHED",
  "id": "17889615691921648"
}

## Updating

This operation is not supported.

## Deleting

This operation is not supported.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)