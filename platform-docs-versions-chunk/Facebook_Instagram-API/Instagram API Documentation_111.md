platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## June 20, 2022

#### Product Tagging

_Applies to all versions._

You can now create and manage [Instagram Shopping Product Tags](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F2022466637835789&h=AT0b-aO40TpF89A-pC4_zRtTRKu9MbNBV7a575cTLR3ui4zdxTGcpn5LxfHmdW_COqh5laXPM5qzFLLcBhIW9N3d8U3uznHs6fl0W0L3UoP_mCd0WAhO-F_vsK5dAJAyiPi76tMgBgOvq0XWwEQv3Q) on an Instagram Business's published media. Refer to the [Product Tagging](https://developers.facebook.com/docs/instagram-api/guides/product-tagging) guide to learn how.

[](#)