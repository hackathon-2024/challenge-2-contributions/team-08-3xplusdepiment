platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/live_media

### Response

A JSON-formatted object containing the data you requested.

{
  "data": \[\],
  "paging": {}
}

#### Response Contents

| Property | Value |
| --- | --- |
| `data` | An array of [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) on an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user). |
| `paging` | An object containing [paging](https://developers.facebook.com/docs/graph-api/using-graph-api#paging) cursors and next/previous data set retrievial URLs. |