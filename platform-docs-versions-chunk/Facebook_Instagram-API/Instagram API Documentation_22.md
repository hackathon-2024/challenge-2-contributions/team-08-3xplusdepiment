platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/getting-started

## 2\. Implement Facebook Login

Follow our [Facebook Login documentation](https://developers.facebook.com/docs/facebook-login) for your platform and implement Facebook Login into your app. Set up your implementation to request these permissions:

* [`instagram_basic`](https://developers.facebook.com/docs/apps/review/login-permissions#instagram-basic)
* [`pages_show_list`](https://developers.facebook.com/docs/apps/review/login-permissions#pages-show-list)

[](#)