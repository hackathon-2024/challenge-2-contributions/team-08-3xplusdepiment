platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

### Private Apps

If our reviewers are unable to test your app because it is behind a private intranet, has no user interface, or has not implemented Facebook Login, you may only request approval for these Permissions:

* [instagram\_basic](https://developers.facebook.com/docs/permissions/reference/instagram_basic)
* [instagram\_manage\_comments](https://developers.facebook.com/docs/permissions/reference/instagram_manage_comments)

[](#)

## Business Verification

You must complete [Business Verification](https://developers.facebook.com/docs/apps#business-verification) if your app will be used by app users who do not have a Role on the app itself, or a Role in a Business that has claimed the app.

[](#)