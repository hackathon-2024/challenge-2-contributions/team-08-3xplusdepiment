platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api

## Common Uses

* [Getting](https://developers.facebook.com/docs/instagram-api/reference/ig-user/media#get-media) and [managing](https://developers.facebook.com/docs/instagram-api/reference/ig-media) published photos, videos, and stories
* [Getting basic data about other Instagram Business and Creator accounts](https://developers.facebook.com/docs/instagram-api/guides/business-discovery)
* [Moderating comments and their replies](https://developers.facebook.com/docs/instagram-api/guides/comment-moderation)
* [Measuring media and profile interaction](https://developers.facebook.com/docs/instagram-api/guides/insights)
* [Discovering hashtagged media](https://developers.facebook.com/docs/instagram-api/guides/hashtag-search)
* [Discovering @mentions](https://developers.facebook.com/docs/instagram-api/guides/mentions)
* [Publishing photos and videos](https://developers.facebook.com/docs/instagram-api/guides/content-publishing)

[](#)