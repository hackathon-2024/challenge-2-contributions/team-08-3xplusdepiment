platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/page

# Page

Represents a Facebook Page.

This node allows you to:

* get the [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) connected to a Facebook Page.

## Creating

This operation is not supported.

## Reading