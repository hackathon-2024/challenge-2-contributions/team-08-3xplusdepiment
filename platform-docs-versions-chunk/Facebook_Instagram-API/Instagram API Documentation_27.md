platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/getting-started


## Next Steps

* Develop your app further so it can successfully use any other endpoints it needs, and keep track of the permissions each endpoint requires
    * If you plan to implement [Instagram Messaging from Messenger Platform](https://developers.facebook.com/docs/messenger-platform//instagram) you will need additional permissions
* Complete the [App Review](https://developers.facebook.com/docs/instagram-api/overview#app-review) process and request approval for all of the permissions your app will need so your app users can grant them while your app is in [Live Mode](https://developers.facebook.com/docs/development/build-and-test/app-modes#live-mode)
* Switch your app to Live Mode and market it to potential users

Once your app is in Live Mode, any Facebook User who you've made your app available to can access an Instagram Business or Creator Account's data, as long as they have a Facebook User account that can perform Tasks on the Page connected to that Instagram Business or Creator Account.

[](#)

[](#)