platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/catalog_product_search

## Reading

**`GET /{ig-user-id}/catalog_product_search`**

Get a collection of products that match a given search string within the targeted [IG User's](https://developers.facebook.com/docs/instagram-api/reference/ig-user) [Instagram Shop](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F1187859655048322%2F&h=AT3OYSE-L429wpO86s5LNdyZoRfShVAcuNbqKVqCXV9SRpUBxkK-KVtTOTVXeibcWD4hFaLLUBCwcKdTJVHatjHDRdbxsx6pMSNKoe8HwKb1tgLCeynvormXwjAhxyYjgMlM_Gibf_25x1Ds) catalog.