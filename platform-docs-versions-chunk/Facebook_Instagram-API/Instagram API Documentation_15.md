platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

## App Review

Your app must complete [App Review](https://developers.facebook.com/docs/app-review) before it can be used by [app users](#app-users) who do not have a [Role](https://developers.facebook.com/docs/apps#roles) on your app or a [Role](https://www.facebook.com/business/help/623924618023072?id=2190812977867143) on a Business that has claimed your app. If your app will only be used by app users who have a Role on your app or Business, you do not need to complete App Review.

Your App Review submission does not need to include any [Facebook test user](https://developers.facebook.com/docs/apps/test-users) credentials if you have implemented [Facebook Login](https://developers.facebook.com/docs/facebook-login) and your app is publicly available. However, if our reviewers need to sign into a non-Facebook account in order to trigger your implementation of Facebook Login, you must include the non-Facebook account credentials in your submission.