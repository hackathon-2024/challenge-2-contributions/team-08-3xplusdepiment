platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/content-publishing

## Product Tags

You can publish both single media posts and carousel posts tagged with [Instagram Shopping](https://www.facebook.com/help/instagram/1187859655048322) products. Refer to the [Product Tagging](https://developers.facebook.com/docs/instagram-api/guides/product-tagging) guide to learn how.

[](#)