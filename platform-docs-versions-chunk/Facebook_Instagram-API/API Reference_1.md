platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference

# Reference

The Instagram API consists of nodes (objects), edges (collections) on those nodes, and fields (object properties). Nodes and Root Edges (edges that are not on a node) are listed below.