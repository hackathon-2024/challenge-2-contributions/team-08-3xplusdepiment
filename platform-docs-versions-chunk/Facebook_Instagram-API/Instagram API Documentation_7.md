platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

## App Users

Instagram Professional accounts are accessed indirectly through Facebook accounts so your app users must have a Facebook account and use it when signing into your app. In addition, the Facebook account must be able to perform admin-equivalent [Tasks](#tasks) on a Facebook Page that has been [connected to the Instagram account](#pages) they are trying to access.

These requirements apply to all app users, even those who have a Role on your app or a Role on a Business that has claimed your app.

[](#)