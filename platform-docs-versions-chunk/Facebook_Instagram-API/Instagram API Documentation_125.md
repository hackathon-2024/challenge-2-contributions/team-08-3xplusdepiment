platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## August 13, 2019

* **Business Discovery** — The [Business Discovery API](https://developers.facebook.com/docs/instagram-api/reference/ig-user/business_discovery) can now be used to get data about other Instagram Creator accounts.

[](#)

## May 22, 2019

* **Instagram Creator Accounts** — The API now supports [Instagram Creator Accounts](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F1158274571010880&h=AT2S36AftXAsJ8guNBtEYTbFxy-mXG7TA2jqUwRQukmO6RBYo12zprJqrWHbxrl5ht6NyEfEByOqtNTrvsVdLRNnuAJgDaxXiskVNRHOD2e3261Ps2RdoxxmGAVRAN4yd1SbLANFCPvyr6xP3-9mbA), with two exceptions. (1) The [Content Publishing API](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) cannot be used by Instagram Creators, and (2) the [Business Discovery API](https://developers.facebook.com/docs/instagram-api/guides/business-discovery) can be used by Creators but can only target Businesses.

[](#)