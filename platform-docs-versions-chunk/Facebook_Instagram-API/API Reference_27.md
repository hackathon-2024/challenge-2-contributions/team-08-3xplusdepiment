platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag

# IG Hashtag

Represents an Instagram hashtag.

## Creating

This operation is not supported.

## Reading

**`GET /{ig-hashtag-id}`**

Returns [Fields](#fields) and [Edges](#edges) on an IG Hashtag.

### Limitations

You can query a maximum of 30 unique hashtags [within a 7 day period](https://developers.facebook.com/docs/instagram-api/reference/ig-user/recently_searched_hashtags).