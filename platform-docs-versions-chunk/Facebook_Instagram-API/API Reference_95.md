platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user


### Edges

| Edge | Description |
| --- | --- |
| [`business_discovery`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/business_discovery) | Get data about other Instagram Business or Instagram Creator [IG Users](https://developers.facebook.com/docs/instagram-api/reference/ig-user). |
| [`content_publishing_limit`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/content_publishing_limit) | Represents an [IG User's](https://developers.facebook.com/docs/instagram-api/reference/ig-user) current [content publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) usage. |
| [`insights`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/insights) | Represents social interaction metrics on an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user). |
| [`live_media`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/live_media) | Represents a collection of live video [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) on an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user). |
| [`media`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/media) | Represents a collection of [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) on an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user). |
| [`media_publish`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/media_publish) | Publish an [IG Container](https://developers.facebook.com/docs/instagram-api/reference/ig-container) on an Instagram Business [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user). |
| [`mentions`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/mentions) | Create an [IG Comment](https://developers.facebook.com/docs/instagram-api/reference/ig-comment) on an IG Comment or captioned [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) that an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has been @mentioned in by another Instagram user. |
| [`mentioned_comment`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/mentioned_comment) | Get data on an [IG Comment](https://developers.facebook.com/docs/instagram-api/reference/ig-comment) in which an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has been @mentioned by another Instagram user. |
| [`mentioned_media`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/mentioned_media) | Get data on an [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) in which an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has been @mentioned in a caption by another Instagram user. |
| [`recently_searched_hashtags`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/recently_searched_hashtags) | Get [IG Hashtags](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag) that an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has searched for within the last 7 days. |
| [`stories`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/stories) | Represents a collection of story [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects on an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user). |
| [`tags`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/tags) | Represents a collection of [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) in which an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has been tagged by another Instagram user. |