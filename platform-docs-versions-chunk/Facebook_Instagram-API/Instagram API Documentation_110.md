platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## June 27, 2022

#### Legacy Instagram API Documentation

_Applies to all versions._

The [Legacy Instagram API developer documentation](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.instagram.com%2Fdeveloper%2F&h=AT1MhAuKx9huSVMkrWm8UHWeqmvk8dx9sH8ePS0yu1f03acKWyXFs0F0GDRe8JVXxWnjUVFQ8mCP41B-2tOAILizVzIDrdCsCsrETxVenvnOgcGKka_ca7ra5o1Dox8TktMEzAGp2FNxZbhsHiRtrw) has been removed and now redirects to the [Instagram Platform](https://developers.facebook.com/docs/instagram) developer documentation.

[](#)