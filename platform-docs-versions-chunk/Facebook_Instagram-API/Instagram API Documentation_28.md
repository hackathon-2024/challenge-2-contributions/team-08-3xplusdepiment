platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides

# Guides

## Business Discovery

The [Business Discovery](https://developers.facebook.com/docs/instagram-api/guides/business-discovery) guide explains how to get basic metadata and metrics about other Instagram Business [IG Users](https://developers.facebook.com/docs/instagram-api/reference/ig-user).

[](#)

## Comment Moderation

The [Comment Moderation](https://developers.facebook.com/docs/instagram-api/guides/comment-moderation) guide explains how to reply to comments, delete comments, hide/unhide comments, and disable/enable comments on [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects owned by [IG Users](https://developers.facebook.com/docs/instagram-api/reference/ig-user).

[](#)