platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/product-tagging


## Endpoints

* [`GET /{ig-user-id}`](https://developers.facebook.com/docs/instagram-api/reference/ig-user#read) — Check the app user's tagging eligibility.
* [`GET /{ig-user-id}/available_catalogs`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/available_catalogs#reading) — Get a list of the app user's product catalogs.
* [`GET /{ig-user-id}/catalog_product_search`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/catalog_product_search#reading) — Get a list of tag eligible products in the app user's catalog.
* [`POST /{ig-user-id}/media`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/media#creating) — Create a tagged media container (step 1 of publishing process).
* [`POST /{ig-user-id}/media_publish`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/media_publish) — Publish a tagged media container (step 2 of publishing process).
* [`GET /{ig-media-id}/product_tags`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/product_tags#reading) — Get tags on published IG Media.
* [`DELETE /{ig-media-id}/product_tags`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/product_tags#deleting) — Delete tags on published IG Media.
* [`POST /{ig-media-id}/product_tags`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/product_tags#creating) — Create or update tags on published IG Media.
* [`GET /{ig-user-id}/product_appeal`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/product_appeal#reading) — Get product appeal information.
* [`POST /{ig-user-id}/product_appeal`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/product_appeal#creating) — Appeal a product rejection.
* [`GET /{ig-media-id}/children`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/children#read) — Get a list of child IG Media in a carousel IG Media.

[](#)