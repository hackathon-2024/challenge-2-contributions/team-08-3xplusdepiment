platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/content-publishing

# Content Publishing

You can use the Instagram Graph API to publish single images, videos, reels (i.e., single media posts), or posts containing multiple images and videos (carousel posts) on Instagram Professional accounts.

Beginning July 1, 2023, all single feed videos published through the Instagram Content Publishing API will be shared as reels.

## Requirements

### Access Tokens

All requests must include the app user's [User](https://developers.facebook.com/docs/facebook-login/access-tokens/#usertokens) access token.