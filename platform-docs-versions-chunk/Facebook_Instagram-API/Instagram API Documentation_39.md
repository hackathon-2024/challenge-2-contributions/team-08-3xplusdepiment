platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/content-publishing

### Limitations

* JPEG is the only image format supported. Extended JPEG formats such as MPO and JPS are not supported.
* Shopping tags are not supported.
* Branded content tags are not supported.
* Filters are not supported.
* Publishing to Instagram TV is not supported.

For additional limitations, refer to each [endpoint's](#endpoints) reference.