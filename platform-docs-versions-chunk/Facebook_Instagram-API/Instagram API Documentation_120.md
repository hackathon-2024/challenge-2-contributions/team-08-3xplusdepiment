platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## April 9, 2021

* The `status` field on an [IG Container](https://developers.facebook.com/docs/instagram-api/reference/ig-container) now returns an [error subcode](https://developers.facebook.com/docs/instagram-api/reference/error-codes) if the container's `error_code` field value is `ERROR`.
* The [IG Media Insights](https://developers.facebook.com/docs/instagram-api/reference/ig-media/insights) `video_views` metric now supports albums and will return the sum of `video_views` on all videos in the album instead of `0`.

[](#)