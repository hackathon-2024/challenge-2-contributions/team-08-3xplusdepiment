platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/business_discovery

### Sample Response

{
  "business\_discovery": {
    "media": {
      "data": \[
        {
          "comments\_count": 50,
          "like\_count": 5837,
          "id": "17858843269216389"
        },
        {
          "comments\_count": 11,
          "like\_count": 2997,
          "id": "17894036119131554"
        },
        {
          "comments\_count": 28,
          "like\_count": 3643,
          "id": "17894449363137701"
        },
        {
          "comments\_count": 43,
          "like\_count": 4943,
          "id": "17844278716241265"
        },
     \],
   },
   "id": "17841401441775531"
  },
  "id": "17841405976406927"
}

## Updating

This operation is not supported.

## Deleting

This operation is not supported.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)