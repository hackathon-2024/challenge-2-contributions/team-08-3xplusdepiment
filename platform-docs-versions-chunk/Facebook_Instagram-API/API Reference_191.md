platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/product_appeal

### cURL Example

#### Request

curl -i -X GET \\
 "https://graph.facebook.com/`v19.0`/90010177253934/product\_appeal?product\_id=4029274203846188&access\_token=EAAOc..."

#### Response

{
  "data": \[
    {
      "product\_id": 4029274203846188,
      "review\_status": "approved",
      "eligible\_for\_appeal": false
    }
  \]
}

## Updating

This operation is not supported.

## Deleting

This operation is not supported.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)