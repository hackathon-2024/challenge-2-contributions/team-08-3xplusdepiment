platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## March 13, 2018

* **Content Publishing API** — Beta partners can now use the `/{ig-user-id}/media` edge to tag [locations](https://developers.facebook.com/docs/instagram-api/guides/content-publishing#publish-with-locations) and public Instagram [users](https://developers.facebook.com/docs/instagram-api/guides/content-publishing#publish-with-tagged-users) when publishing photos.

[](#)

## March 8, 2018

* **Public fields** — The `timestamp` field on the `/{ig-media-id}` node is now a public field and can be returned via field expansion.

[](#)

## February 22, 2018

* **Public fields** — The `/{ig-user-id}`, `/{ig-comment-id}`, and `/{ig-media-id}` nodes will now return all public fields when accessed through an edge via field expansion. Refer to each node's reference document to see which fields are public.

[](#)

## February 8, 2018

* **Content Publishing API** — Beta partners can now include hashtags when publishing photos via the `/{ig-user-id}/media` edge. `#crazywildebeest` FTW!

[](#)

[](#)