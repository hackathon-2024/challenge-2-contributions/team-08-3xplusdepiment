platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/hashtag-search

## Requirements

In order to use this API, you must undergo [App Review](https://developers.facebook.com/docs/apps/review) and request approval for:

* the [`Instagram Public Content Access`](https://developers.facebook.com/docs/apps/review/feature#reference-INSTAGRAM_PUBLIC_CONTENT_ACCESS) feature
* the [`instagram_basic`](https://developers.facebook.com/docs/facebook-login/permissions#reference-instagram_basic) permission

[](#)