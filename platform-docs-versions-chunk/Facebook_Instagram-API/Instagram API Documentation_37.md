platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/content-publishing


### Permissions

Publishing relies on a combination of the following permissions. The exact combination depends on which [endpoints](#endpoints) your app uses. Refer to our [endpoint](#endpoints) references to determine which permissions each endpoint requires.

* [ads\_management](https://developers.facebook.com/docs/permissions/reference/ads_management)
* [business\_management](https://developers.facebook.com/docs/permissions/reference/business_management)
* [instagram\_basic](https://developers.facebook.com/docs/permissions/reference/instagram_basic)
* [instagram\_content\_publish](https://developers.facebook.com/docs/permissions/reference/instagram_content_publish)
* [pages\_read\_engagement](https://developers.facebook.com/docs/permissions/reference/pages_read_engagement)

If your app will be used by app users who do not have a [role](https://developers.facebook.com/docs/development/build-and-test/app-roles) on your app or a role in a [Business](https://www.facebook.com/business/help/442345745885606?id=180505742745347) that has claimed your app, you must request approval for each permission via [App Review](https://developers.facebook.com/docs/app-review) before non-role app users can grant them to your app.