platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-media


### Edges

Public edges can be returned through field expansion.

| Edge | Description |
| --- | --- |
| [`children`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/children)  <br>Public. | Represents a collection of [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects on an album [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media). |
| [`collaborators`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/collaborators) | Represents a list of users who are added as collaborators on an [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) object. |
| [`comments`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/comments) | Represents a collection of [IG Comments](https://developers.facebook.com/docs/instagram-api/reference/ig-comment) on an [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) object. |
| [`insights`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/insights) | Represents social interaction metrics on an [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) object. |