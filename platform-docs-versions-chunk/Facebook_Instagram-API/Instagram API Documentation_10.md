platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

### Instagram Messaging

If you plan to implement Instagram Messaging from Messenger Platform, you will need to include the `instagram_manage_messages` permission. [Learn more about Instagram Messaging. ![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.2365-6/276034258_1045248339390233_3876773921429146148_n.png?_nc_cat=110&ccb=1-7&_nc_sid=e280be&_nc_ohc=WPfL2rhRbykAX8aThOd&_nc_ht=scontent-cdg4-1.xx&oh=00_AfAeKtIuTWG4LWvhf60z5-EUNIsbizHHpLc-3l8mqFhnLw&oe=65D587B5)](https://developers.facebook.com/docs/messenger-platform/overview#permissions) 

[](#)