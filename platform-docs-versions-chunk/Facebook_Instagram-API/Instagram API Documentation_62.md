platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/hashtag-search


## Endpoints

The Hashtag Search API consists of the following nodes and edges:

* [`GET /ig_hashtag_search`](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag-search#reading) — to get a specific hashtag's node ID
* [`GET /{ig-hashtag-id}`](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag#reading) — to get data about a hashtag
* [`GET /{ig-hashtag-id}/top_media`](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag/top-media#reading) — to get the most popular photos and videos that have a specific hashtag
* [`GET /{ig-hashtag-id}/recent_media`](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag/recent-media#reading) — to get the most recently published photos and videos that have a specific hashtag
* [`GET /{ig-user-id}/recently_searched_hashtags`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/recently_searched_hashtags) — to determine the unique hashtags an Instagram Business or Creator Account has searched for in the current week

Refer to each endpoint's reference documentation for supported fields, parameters, and usage requirements.

[](#)