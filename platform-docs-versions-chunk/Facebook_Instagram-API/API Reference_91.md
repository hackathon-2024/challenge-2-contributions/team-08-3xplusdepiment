platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user

# IG User

Represents an [Instagram Business Account](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F502981923235522&h=AT1aGy3qVayaMUrb6Q-HuR_LkZQRppI1AIbk3SqW44vWWiL6oOQz35htqvHeXievk5QAKwVGPL9hQkD4KngdGyoi22g5YbJLFMBHJ6XuQsl5Fb551hojXsuVyNhgfymdrpimEDa6jL5hEIkh) or an [Instagram Creator Account](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F1158274571010880&h=AT3SUG-IieG_NCTrLSWuHS45SZIZPRFVsym-RtW_1AkqpBle2k9Cz6s-TLFxsBII9CE-V7bhqSRIODusYuXNzndhrAmgJp9B08r4C6YNtRYqvBhO3r4qGqq5eZNhqhteNSAqRHD3kl1GYHTi).

## Creating

This operation is not supported.

## Reading

**`GET /{ig-user-id}`**

Get fields and edges on an Instagram Business or Creator Account.