platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/insights


### Sample Demographic Metric Response

{
  "data": \[
    {
      "name": "engaged\_audience\_demographics",
      "period": "lifetime",
      "title": "Engaged audience demographics",
      "description": "The demographic characteristics of the engaged audience, including countries, cities and gender distribution.",
      "total\_value": {
        "breakdowns": \[
          {
            "dimension\_keys": \[
              "timeframe",
              "country"
            \],
            "results": \[
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "AR"
                \],
                "value": 1
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "RU"
                \],
                "value": 1
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "MA"
                \],
                "value": 1
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "LA"
                \],
                "value": 1
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "IQ"
                \],
                "value": 2
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "MX"
                \],
                "value": 1
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "FR"
                \],
                "value": 1
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "ES"
                \],
                "value": 3
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "NL"
                \],
                "value": 1
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "TR"
                \],
                "value": 1
              },
              {
                "dimension\_values": \[
                  "LAST\_90\_DAYS",
                  "US"
                \],
                "value": 7
              }
            \]
          }
        \]
      },
      "id": "17841401130346306/insights/engaged\_audience\_demographics/lifetime"
    }
  \]
}

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)