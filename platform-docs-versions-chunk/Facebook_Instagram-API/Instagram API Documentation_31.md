platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides

## Webhooks

The [Webhooks](https://developers.facebook.com/docs/instagram-api/guides/webhooks) guide explains how to get real-time notifications whenever Instagram users comment on any of an [IG User's](https://developers.facebook.com/docs/instagram-api/reference/ig-user) [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects, @mention an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) on other Instagram users' [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects, or when an [IG User's](https://developers.facebook.com/docs/instagram-api/reference/ig-user) stories expire.

[](#)

[](#)