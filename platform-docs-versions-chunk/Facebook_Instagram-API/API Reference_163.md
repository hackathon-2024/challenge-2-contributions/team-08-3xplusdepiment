platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/media_publish

# IG User Media Publish

Publish an [IG Container](https://developers.facebook.com/docs/instagram-api/reference/ig-container) on an Instagram Business [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user). Refer to the [Content Publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) guide for complete publishing steps.

## Creating

**`POST /{ig-user-id}/media_publish`**

Publish an [IG Container](https://developers.facebook.com/docs/instagram-api/reference/ig-container) object on an Instagram Business [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user).