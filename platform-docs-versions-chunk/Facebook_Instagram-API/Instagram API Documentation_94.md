platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/webhooks

## Step 1: Create an Endpoint

[Create an endpoint](https://developers.facebook.com/docs/graph-api/webhooks/getting-started) that accepts and processes webhooks. During the [configuration](https://developers.facebook.com/docs/graph-api/webhooks/getting-started#configure-webhooks-product), select the **Instagram Graph API** object, click **Set up**, and subscribe to one or more [Instagram fields](https://developers.facebook.com/docs/graph-api/webhooks/reference/instagram/).