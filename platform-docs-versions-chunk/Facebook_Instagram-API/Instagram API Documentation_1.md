platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api

# Instagram Graph API

The Instagram Graph API allows [Instagram Professionals](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F502981923235522&h=AT3B3sMOiVW_S86mIFvILQZEYOVOs1TQtRK7Phvlthq0dOBrfGlRFKFTxK-nlURn2os-2LPesF0viSz3fJiJUJvwB7Sx1RsTfwsJ1yZwgtZRGtLBD7rjp__fj9e9Wen3VaPEW-oOb7QtzC2wx-Gr9Q) — Businesses and Creators — to use your app to manage their presence on Instagram. The API can be used to get and publish their media, manage and reply to comments on their media, identify media where they have been @mentioned by other Instagram users, find hashtagged media, and get basic metadata and metrics about other Instagram Businesses and Creators.

The API is intended for Instagram Businesses and Creators who need insight into, and full control over, all of their social media interactions. If you are building an app for consumers or you only need to get an app user's basic profile information, photos, and videos, consider the [Instagram Basic Display API](https://developers.facebook.com/docs/instagram-basic-display-api) instead.

The API is built on the Facebook Graph API. If you are unfamiliar with the Facebook Graph API, please read our [Facebook Graph API documentation](https://developers.facebook.com/docs/graph-api/) to learn how it works before proceeding.