platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides

## Mentions

The [Mentions](https://developers.facebook.com/docs/instagram-api/guides/mentions) guide explains how to identify [IG Comments](https://developers.facebook.com/docs/instagram-api/reference/ig-comment) and [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) in which an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has been tagged or @mentioned.

[](#)