platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/recently_searched_hashtags

# Recently Searched Hashtags

This edge allows you to determine the [IG Hashtags](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag) that an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user) has queried for within the last 7 days.