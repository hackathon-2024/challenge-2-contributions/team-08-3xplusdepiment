platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## March 15, 2022

#### Carousel Posts

_Applies to all versions._

You can now use the Instagram API to publish posts containing multiple images and videos ([carousel posts](https://developers.facebook.com/docs/instagram-api/guides/content-publishing#carousel-posts)). Refer to the [Content Publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) guide for complete publishing steps.

If your app has already been approved for [permissions](https://developers.facebook.com/docs/instagram-api/guides/content-publishing#permissions) required for content publishing, it does not need to undergo [App Review](https://developers.facebook.com/docs/app-review) again to take advantage of this functionality.

[](#)