platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/product-tagging

## Carousels

You can publish carousels (albums) containing up to 10 total tagged images, videos, or a mix of the two. To do this, when performing step 1 of 3 of the [carousel posts](https://developers.facebook.com/docs/instagram-api/guides/content-publishing#carousel-posts) publishing process, simply create [tagged media containers](#create-tagged-media-container) for each tagged image or video that you want to appear in the album carousel and continue with the carousel publishing processs as you normally would.

### Get child media in a carousel

To get the IDs of IG Media in an album carousel, use the [IG Media Children](https://developers.facebook.com/docs/instagram-api/reference/ig-media/children) endpoint.

[](#)

[](#)