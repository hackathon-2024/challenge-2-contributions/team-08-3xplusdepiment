platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag-search

# IG Hashtag Search

This root edge allows you to get [IG Hashtag](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag) IDs.

## Creating

This operation is not supported.

## Reading