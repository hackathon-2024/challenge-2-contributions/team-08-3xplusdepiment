platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## May 27, 2022

#### Product Variants

_Applies to all versions._

For partners in the [Product Tagging](https://developers.facebook.com/docs/instagram-api/guides/product-tagging) beta, all [product variants](https://developers.facebook.com/docs/marketing-api/catalog/guides/product-variants) that match a query's search criteria will now be returned when [searching a catalog for products](https://developers.facebook.com/docs/instagram-api/guides/product-tagging#get-eligible-products).

[](#)