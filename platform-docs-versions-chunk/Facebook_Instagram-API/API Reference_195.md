platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/stories

# Stories

Represents a collection of story [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects on an [IG User](https://developers.facebook.com/docs/instagram-api/reference/ig-user).

## Creating

For creating Stories Media, refer to the [Instagram User Media](https://developers.facebook.com/docs/instagram-api/reference/ig-user/media) documentation.

## Reading