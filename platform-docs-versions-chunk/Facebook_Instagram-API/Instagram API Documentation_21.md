platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/getting-started

## 1\. Configure Facebook Login

Add the Facebook Login product to your app in the App Dashboard.

![](https://scontent-cdg4-3.xx.fbcdn.net/v/t39.2365-6/57313411_166141971002334_3488307145718366208_n.png?_nc_cat=104&ccb=1-7&_nc_sid=e280be&_nc_ohc=PjvWWxrQZiUAX-niDZy&_nc_ht=scontent-cdg4-3.xx&oh=00_AfAlAnR-EqxctBkNjQsiKw-AJ5w9hzqIIYxjBdgEdxshjg&oe=65D57EC6)

You can leave all settings on their defaults. If you are implementing Facebook Login manually (which we don't recommend), enter your `redirect_uri` in the **Valid OAuth redirect URIs** field. If you will be using one of our SDKs, you can leave it blank.

[](#)