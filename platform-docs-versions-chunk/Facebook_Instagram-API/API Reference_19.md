platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-container

# IG Container

Represents a media container for publishing an Instagram post. Refer to the [Content Publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) guide for complete publishing steps.

## Creating

This operation is not supported.

## Reading

**`GET /{instagram-container-id}`**

Get [fields](#fields) and [edges](#edges) on an IG Container.