platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-user/catalog_product_search

# IG User Catalog Product Search

Represents products and product variants that match a given search string in an [IG User's](https://developers.facebook.com/docs/instagram-api/reference/ig-user) [Instagram Shop](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F1187859655048322%2F&h=AT1utBW3D6BuJln5O-ks8dCwV6WSlcPPcrXvsigWpO9RviiJdQWoz7lAm1c1zA8tEhQSpEIX-1igArlBn-aDybGzuBlK5OUZYZ8r0DN-nrAtbOCGblIg3j1vvccXxKETtnmYHyM0UfPt6iR-) product catalog. See [Product Tagging](https://developers.facebook.com/docs/instagram-api/guides/product-tagging) guide for complete usage details.

## Creating

This operation is not supported.