platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/mentions


## Endpoints

The API consists of the following endpoints:

* [`GET /{ig-user-id}/tags`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/tags) — to get the media objects in which a Business or Creator Account has been tagged
* [`GET /{ig-user-id}?fields=mentioned_comment`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/mentioned_comment#reading) — to get data about a comment that an Business or Creator Account has been @mentioned in
* [`GET /{ig-user-id}?fields=mentioned_media`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/mentioned_media#reading) — to get data about a media object on which a Business or Creator Account has been @mentioned in a caption
* [`POST /{ig-user-id}/mentions`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/mentions#creating) — to reply to a comment or media object caption that a Business or Creator Account has been @mentioned in by another Instagram user

Refer to each endpoint reference document for usage instructions.

[](#)