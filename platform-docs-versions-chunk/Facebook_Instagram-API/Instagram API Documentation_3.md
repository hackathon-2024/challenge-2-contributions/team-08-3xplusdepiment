platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api

## Limitations

* The API cannot access Instagram consumer accounts (i.e., non-Business or non-Creator Instagram accounts). If you are building an app for consumer users, use the [Instagram Basic Display API](https://developers.facebook.com/docs/instagram-basic-display-api) instead.
* [Content Publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) is available to all Instagram Professional accounts, except Stories, which are only available to business accounts.
* [Ordering results](https://developers.facebook.com/docs/graph-api/using-graph-api#ordering) is not supported
* All endpoints support cursor-based [pagination](https://developers.facebook.com/docs/graph-api/using-graph-api#paging), but the [User Insights](https://developers.facebook.com/docs/instagram-api/reference/ig-user/insights) edge is the only endpoint that supports time-based pagination

[](#)