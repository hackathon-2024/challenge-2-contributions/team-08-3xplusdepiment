platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides

## Content Publishing

The [Content Publishing](https://developers.facebook.com/docs/instagram-api/guides/content-publishing) guide explains how to publish [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects on Instagram Business [IG Users](https://developers.facebook.com/docs/instagram-api/reference/ig-user).

[](#)

## Hashtag Search

The [Hashtag Search](https://developers.facebook.com/docs/instagram-api/guides/hashtag-search) guide explains how to find public photos and videos that have been tagged with specific hashtags.

[](#)

## Insights

The [Insights](https://developers.facebook.com/docs/instagram-api/guides/insights) guide explains how to get social interaction metrics for [IG Users](https://developers.facebook.com/docs/instagram-api/reference/ig-user) and their [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) objects.

[](#)