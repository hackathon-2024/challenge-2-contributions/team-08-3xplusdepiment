platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/guides/insights

## UTC

Timestamps in API responses use UTC with zero offset and are formatted using ISO-8601. For example: `2019-04-05T07:56:32+0000`

[](#)

## Endpoints

The API consists of the following endpoints:

* [`GET /{ig-media-id}/insights`](https://developers.facebook.com/docs/instagram-api/reference/ig-media/insights) — gets metrics on a media object
* [`GET /{ig-user-id}/insights`](https://developers.facebook.com/docs/instagram-api/reference/ig-user/insights) — gets metrics on an Instagram Business Account or Instagram Creator account.

Refer to each endpoint's reference documentation for available metrics, parameters, and permission requirements.

[](#)

## Examples