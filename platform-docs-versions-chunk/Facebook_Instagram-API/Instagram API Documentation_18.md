platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/overview

## Instagram Messaging

Several Instagram Graph API endpoints are used in conjunction with the Messenger Platform endpoints to allow your app users to interact with direct messages sent to their Instagram Professional accounts. Refer to the Messenger Platform's [Instagram Messaging](https://developers.facebook.com/docs/messenger-platform/instagram) documentation to learn how to access messages in Instagram Business accounts.

[](#)

[](#)