platform: Facebook
topic: Instagram-API
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/API Reference.md
url: https://developers.facebook.com/docs/instagram-api/reference/ig-media/insights

# IG Media Insights

Represents social interaction metrics on an [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) object.

## Creating

This operation is not supported.

## Reading

**`GET /{ig-media-id}/insights`**

Get insights data on an [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media) object.