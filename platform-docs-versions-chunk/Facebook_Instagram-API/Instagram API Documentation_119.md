platform: Facebook
topic: Instagram-API
subtopic: Instagram API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Instagram-API/Instagram API Documentation.md
url: https://developers.facebook.com/docs/instagram-api/changelog

## April 14, 2021

Story [IG Media](https://developers.facebook.com/docs/instagram-api/reference/ig-media/insights#metrics) interactions performed by users in Japan are no longer included in some `replies` metric calculations:

* For stories created by users in Japan, the `replies` metric will now return a value of `0`.
* For stories created by users outside Japan, the `replies` metric will return the number of replies, but replies made by users in Japan will not be included in the calculation.

[](#)

## April 12, 2021

Fixed a minor bug with reach [metrics](https://developers.facebook.com/docs/instagram-api/reference/ig-media/insights#metrics) on story IG Media.

[](#)