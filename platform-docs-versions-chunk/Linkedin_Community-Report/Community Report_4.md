platform: Linkedin
topic: Community-Report
subtopic: Community Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Community-Report/Community Report.md
url: https://about.linkedin.com/transparency/community-report


### What we do to catch spam and scams

  
Of the spam or scam content removed in this reporting period, our automated defenses stopped 99.6% and our teams manually removed the rest.  

#### Spam and scams detected and removed

2023: Jan–Jun 2022: Jul–Dec 2022: Jan - Jun 2021: Jul–Dec 2021: Jan-Jun 2020: Jul–Dec 2020: Jan–Jun 2019: Jul–Dec 2019: Jan–Jun##### 2023: January-June

M = millions        K = thousands ##### 2022: July-December

M = millions        K = thousands ##### 2022: January-June

M = millions        K = thousands ##### 2021: July-December

M = millions        K = thousands 

##### 2021: January-June

M = millions        K = thousands

_\*An earlier version of this report reflected that LinkedIn removed 232 thousand spam and scams after member reports during the reporting period of January through June 2021. We have edited the report to accurately reflect that LinkedIn removed 224 thousand spam and scams during such reporting period._

##### 2020: July-December

M = millions        K = thousands ##### 2020: January-June

M = millions        K = thousands ##### 2019: July-December

M = millions        K = thousands ##### 2019: January-June

M = millions        K = thousands