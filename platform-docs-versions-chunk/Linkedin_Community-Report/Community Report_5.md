platform: Linkedin
topic: Community-Report
subtopic: Community Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Community-Report/Community Report.md
url: https://about.linkedin.com/transparency/community-report


## Content violations

  
We do not tolerate any content that violates our [Professional Community Policies](https://www.linkedin.com/legal/professional-community-policies), and take swift action to remove it. We won’t always get it right, so members can [ask us to take a second look](https://www.linkedin.com/help/linkedin/answer/82934).  
  
Similar to our previous reporting periods, we continue to see record-high engagement and conversations from our growing community of over 950 million members. While we saw an overall decrease in violative content across most categories compared to the previous reporting period, we did see an increase in caught harassing and abusive content, attributable to enhanced messaging defenses.

[Learn more about content policy violations](https://www.linkedin.com/legal/professional-community-policies)

#### Content removed

2023: Jan–Jun 2022: Jul–Dec 2022: Jan-Jun 2021: Jul–Dec 2021: Jan-Jun 2020: Jul–Dec 2020: Jan-Jun 2019: Jul–Dec 2019: Jan–Jun

##### 2023: January-June

##### 2022: July-December

##### 2022: January-June

##### 2021: July-December

##### 2021: January-June

_\*An earlier version of this report reflected that LinkedIn removed 147,156 pieces of “harassment or abusive” content during the reporting period of January through June 2021. We have edited the report to accurately reflect that LinkedIn removed 158,988 pieces of content during such reporting period._

##### 2020: July-December

##### 2020: January-June

##### 2019: July-December

##### 2019: January-June