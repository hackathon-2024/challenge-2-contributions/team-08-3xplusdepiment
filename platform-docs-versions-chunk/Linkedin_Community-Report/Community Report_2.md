platform: Linkedin
topic: Community-Report
subtopic: Community Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Community-Report/Community Report.md
url: https://about.linkedin.com/transparency/community-report


### What we do to catch fake accounts

  
Our automated defenses blocked 90.1% of the fake accounts we stopped during the January - June 2023 period, with the remaining 9.9% stopped by our manual investigations and restrictions. 99.7% of the fake accounts were stopped proactively, before a member report. We continue enhancing our defenses to prevent and remove malicious accounts.  

#### Fake accounts detected and removed

2023: Jan-Jun 2022: Jul–Dec 2022: Jan–Jun 2021: Jul–Dec 2021: Jan-Jun 2020: Jul–Dec 2020: Jan-Jun 2019: Jul–Dec 2019: Jan–Jun##### 2023: January-June

            M = millions        K = thousands ##### 2022: July-December

            M = millions        K = thousands ##### 2022: January-June

            M = millions        K = thousands ##### 2021: July-December

            M = millions        K = thousands ##### 2021: January-June

            M = millions        K = thousands ##### 2020: July-December

            M = millions        K = thousands ##### 2020: January-June

            M = millions        K = thousands ##### 2019: July-December

            M = millions        K = thousands ##### 2019: January-June

            M = millions        K = thousands