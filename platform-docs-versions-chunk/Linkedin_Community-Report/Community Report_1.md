platform: Linkedin
topic: Community-Report
subtopic: Community Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Community-Report/Community Report.md
url: https://about.linkedin.com/transparency/community-report

# Community Report

  
This report covers actions we took on content that violated our Professional Community Policies and User Agreement for the six-month period between January 1 and June 30, 2023. It also summarizes requests to remove content based on copyright infringement claims.  

## This report covers:

[Fake accounts](#fake-accounts)

[Spam and scams](#spam-scams)

[Content violations](#content-violations)

[Copyrighted materials](#copyright-removal-requests)

## Fake accounts

  
We are a safe, trusted and professional community where real people share, learn and connect. Our policies prohibit fake profiles and require that members are real people who represent themselves accurately and contribute authentically.