platform: Linkedin
topic: Community-Report
subtopic: Community Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Community-Report/Community Report.md
url: https://about.linkedin.com/transparency/community-report

## Spam and scams

  
Spam and scams are not permitted on LinkedIn. By far the most common type of inappropriate content we take action on is spam or scam content, which includes inappropriate commercial activity and repetitive communications or invitations, often meant for financial gain.