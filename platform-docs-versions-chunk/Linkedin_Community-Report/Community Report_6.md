platform: Linkedin
topic: Community-Report
subtopic: Community Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Community-Report/Community Report.md
url: https://about.linkedin.com/transparency/community-report


## Copyright removal requests

  
We respect the intellectual property rights of others and do not allow copyright infringement. Our [User Agreement](https://www.linkedin.com/legal/user-agreement) requires members to respect others’ rights and follow the law, including gathering any necessary legal permissions before sharing protected content.  

2023 2022 2021 2020 2019 2018

#### Copyright removal requests

##### 2023: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 2,143 | 1,994 | 1,168 | 826 | 59% |

#### Copyright removal requests

##### 2022: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 1,195 | 1,214 | 1,098 | 116 | 90.44% |

#### Copyright removal requests

##### 2022: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 1,052 | 1,067 | 1,012 | 55  | 94.85% |

#### Copyright removal requests

##### 2021: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 872 | 881 | 863 | 18  | 97.95% |

#### Copyright removal requests

##### 2021: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 1,716 | 5,796 | 5,780 | 16  | 99.70% |

#### Copyright removal requests

##### 2020: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 3,876 | 23,269 | 23,260 | 9   | 99.96% |

#### Copyright removal requests

##### 2020: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 7,039 | 125,306 | 125,289 | 17  | 99.99% |

#### Copyright removal requests

##### 2019: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 11,564 | 290,170 | 290,145 | 25  | 99.99% |

#### Copyright removal requests

##### 2019: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 8,845 | 116,164 | 116,127 | 37  | 99.97% |

#### Copyright removal requests

##### 2018: July-December

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 9,333 | 115,310 | 115,284 | 26  | 100% |

#### Copyright removal requests

##### 2018: January-June

Swipe

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| Requests \[1\] | Total infringements reported \[2\] | Reported infringements removed \[3\] | Reported infringements rejected \[4\] | Percentage accepted |
| 4,737 | 37,901 | 37,881 | 20  | 99.94% |

\[1\] Requests from copyright owners claiming infringement of protected works.

  
\[2\] Requests often cite multiple infringements.

\[3\] Infringements removed means action was taken. 

\[4\] Reported infringements not removed.