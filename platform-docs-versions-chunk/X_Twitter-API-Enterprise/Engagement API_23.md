platform: X
topic: Twitter-API-Enterprise
subtopic: Engagement API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Engagement API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/engagement-api/guides/interpreting-metrics

Interpreting the metrics

## Interpreting the metrics

**Note:** You may observe differences between reported data on some of the Twitter web dashboards, and the data reported in the Engagement API. These differences occur because the web dashboards typically only show engagements and/or impressions that occurred within the selected time range. For example, a web dashboard may show engagement on Tweets within the span of a calendar month, while the Engagement API may show engagements that fall beyond the span of that month, but within the time range requested. The Engagement API should be seen as the valid source, in these cases.