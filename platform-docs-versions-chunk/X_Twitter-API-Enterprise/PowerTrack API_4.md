platform: X
topic: Twitter-API-Enterprise
subtopic: PowerTrack API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/PowerTrack API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/powertrack-api/overview


### Available operators

The PowerTrack API currently supports the following operators:  

* keyword
* emoji
* "exact phrase match"
* "keyword1 keyword2"~N
* contains:
* from:
* to:
* url:
* url\_title:
* url\_description:
* url\_contains:
* has:links
* sample:
* #
* point\_radius:\[lon lat radius\]
* bounding\_box:\[west\_long south\_lat east\_long north\_lat\]
* @
* $
* bio:
* bio\_name:
* retweets\_of:
* lang:
* bio\_location:
* statuses\_count:
* followers\_count:
* friends\_count:
* listed\_count:
* is:verified
* source:
* place:
* place\_country:
* has:geo
* has:mentions
* has:hashtags
* has:images
* has:videos
* has:media
* has:symbols
* is:retweet
* is:reply
* is:quote
* retweets\_of\_status\_id:
* in\_reply\_to\_status\_id:
* has:profile\_geo
* profile\_point\_radius:\[long lat radius\]
* profile\_bounding\_box:\[west\_long south\_lat east\_long north\_lat\]
* profile\_country:
* profile\_region:
* profile\_locality:
* profile\_subregion:

For more details, please see the [Getting started with enterprise rules](https://developer.twitter.com/en/docs/twitter-api/enterprise/rules-and-filtering/enterprise-operators) guide.