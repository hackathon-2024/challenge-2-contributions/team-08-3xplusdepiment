platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/location

Location object

## Location Object

Location obejcts can exist within the actor obejct set on the Twitter account level or within the profileLocations object of the [gnip object](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/gnip.html). Location objects have a place object type and can have a name, address, or geo coordinates. Location objects are similar to [Geo](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/geo.html) in native enriched format.