platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/usage

### Daily

The Daily Usage page provides daily deduplicated counts for each day in the current month, broken down by product. Counts are updated every 24 hours at 00:00 UTC.

### Rules

Rule limits are based on your contracted PowerTrack rule package and are applied across all streams of a given "type". Counts are updated whenever a new rule is added or deleted.

  
  

### Next Steps:

* [Review account tab](https://developer.twitter.com/en/docs/twitter-api/enterprise/console/account.html)
* [Review the Usage API documentation](https://developer.twitter.com/en/docs/twitter-api/enterprise/usage-api/overview.html)