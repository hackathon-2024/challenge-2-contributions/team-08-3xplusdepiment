platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/product

### Overview

Clicking on one of the streams in the main dashboard will take you to an overview page for that stream.

For streaming delivery products, this page includes the following:

1. A volume chart of the number of activities being delivered to you through each specific stream connection 
    
2. Details (connection ID and IP address) on currently active connections on the stream 
    
3. A log of recent connection, disconnection, and rule-update events for your stream
    

Note that the scale of the chart may be adjusted with the links in the top-right corner. The visibility of individual connections and disconnections can be toggled by clicking the appropriate key in the legend directly below the chart.