platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/overview


## Overview

The enterprise console (at console.gnip.com) is available to enterprise data customers with contracted enterprise API access. If you are not currently an enterprise data customer but would like more information, you can learn more about [enterprise data](https://developer.twitter.com/en/products/twitter-api/enterprise.html) here.  

This is an overview of Twitter’s enterprise console dashboard. The sections that are covered pertain to each specific area of the console for product details, usage, and general account management. If you have any questions regarding your specific account, please contact your designated account manager, or review our [technical documentation for enterprise products](https://developer.twitter.com/en/docs/twitter-api/enterprise.html).

As part of the enterprise trial and onboarding process with your account manager, enterprise customers will receive a login to [console.gnip.com](http://console.gnip.com/), which is the user interface where enterprise product access can be reviewed and configured. Initial access to the console for customer admin is set up by Twitter as part of the enterprise onboarding process, and admins can then add additional users. The enterprise console allows you to manage and access more details related to your enterprise products. 

The following video provides an overview of the various portions of the console.gnip.com dashboard. 

**Disclaimer: Please note that some of the features shown in the video, including certain stream enrichments and non-Twitter data source products, may no longer be available.**

 Your browser does not support the <code>video</code> element.