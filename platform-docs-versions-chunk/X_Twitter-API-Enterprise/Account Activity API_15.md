platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/guides/getting-started-with-webhooks

### 2\. Get Account Activity API access

After creating a Twitter app, the next step is applying for Account Activity API access. 

Those needing enterprise level access to more than 250 account subscriptions and 3+ webhooks will need to submit an application at the following link. If you can satisfy your use case with less access than this, you may want to check out [Account Activity API premium](https://developer.twitter.com/en/docs/twitter-api/premium/account-activity-api/overview). 

[Apply for Enterprise access](https://developer.twitter.com/en/enterprise-application.html)