platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/overview

Introduction

Interested in learning more about how the enterprise data formats map to the Twitter API v2 format?

Check out our comparison guides:

* [Native Enriched compared to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/native-enriched-to-v2)
* [Activity Streams compared to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/activity-streams-to-v2)