platform: X
topic: Twitter-API-Enterprise
subtopic: Search API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Search API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/guides/changelog

### Next steps

* [Learn more about the Full-Archive Search API](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/search-api/overview)
* [Learn more about Historical PowerTrack and its metadata and filtering timeline](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/historical-powertrack-api/guides/hpt-timeline)
* [Choosing between Historical PowerTrack and Full-Archive Search APIs](https://developer.twitter.com/content/developer-twitter/en/docs/tutorials/choosing-historical-api)