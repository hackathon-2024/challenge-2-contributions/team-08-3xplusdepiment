platform: X
topic: Twitter-API-Enterprise
subtopic: Decahose API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Decahose API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/decahose-api/overview/streaming-likes

Streaming likes

Enterprise

# Streaming likes

_This is an enterprise API available within our managed access levels only. To use this API, you must first set up an account with our enterprise sales team. [Learn more](https://developer.twitter.com/en/products/twitter-api/enterprise)_

Likes enable insight into who likes Tweets and delivers accurate counts of likes. Gnip’s Firehose and Decahose can deliver public likes related to the Tweets delivered via Gnip. This yields real-time public engagement and audience metrics associated with a Tweet.