platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/activity

Activity object

Interested in learning more about how the Activity Streams data format maps to the Twitter API v2 format?

Check out our comparison guide: [Activity Streams compared to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/activity-streams-to-v2)

Please note: It is highly recommended to use the [Enriched Native](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/tweet.html) format for enterprise data APIs. 

* The Enriched Native format includes all new metadata since 2017, such as [poll metadata](https://developer.twitter.com/en/docs/twitter-api/enterprise/enrichments/overview/poll-metadata.html), and additional metrics such as reply\_count and quote\_count.
    
* Activity Streams format has not been updated with new metadata or enrichments since the [character update](https://blog.twitter.com/official/en_us/topics/product/2017/Giving-you-more-characters-to-express-yourself.html) in 2017.