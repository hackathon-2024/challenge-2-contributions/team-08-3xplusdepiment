platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/overview

Overview

This endpoint has been updated to include Tweet edit metadata. Learn more about these metadata on the ["Edit Tweets" fundamentals page](https://developer.twitter.com/en/docs/twitter-api/enterprise/edit-tweets). 

This endpoint is often used with the Direct Messages endpoints. We have launched new [v2 Direct Messages endpoints](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/introduction). Note that the Enterprise and Premium Account Activity APIs support v2 one-to-one messages, but do not yet support group conversations.     

Overview