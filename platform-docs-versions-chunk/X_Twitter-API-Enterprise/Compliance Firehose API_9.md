platform: X
topic: Twitter-API-Enterprise
subtopic: Compliance Firehose API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Compliance Firehose API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/compliance-firehose-api/guides/honoring-user-intent

Honoring user intent on Twitter

We believe that respecting the privacy and intent of Twitter users is critically important to the long term health of one of the largest public, real-time information platforms in the world. Twitter puts privacy controls in the hands of its users, giving individuals the ability to control their own Twitter experience. As business consumers of Twitter data, we have a collective responsibility to honor the privacy and actions of end users in order to maintain this environment of trust and respect.  

There are a variety of things that can happen to Tweets and User accounts that impact how they are displayed on the platform. The actions that affect privacy and intent are defined at both the Status (Tweet) and User levels. These actions include: