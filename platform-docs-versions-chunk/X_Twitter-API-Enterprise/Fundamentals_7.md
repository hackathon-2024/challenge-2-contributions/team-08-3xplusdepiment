platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/product

### Connections

The Connections page provides details on recent connection events on your stream. This includes the start and end times for each connection (in 24 hour UTC), the duration of each connection, the IP of the server that made the connection, a unique connection ID for reference purposes, and the current connection status. The status corresponds to the most recent event for the specified connection – i.e. Client Connected, or a disconnect, with the type of disconnect specified.   For more details on connection debugging, visit the [disconnections explained guide](https://developer.twitter.com/en/docs/twitter-api/enterprise/powertrack-api/guides.html).