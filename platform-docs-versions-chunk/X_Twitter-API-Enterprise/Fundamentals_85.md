platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/actor


###   
Examples:

      `      "actor": {         "objectType": "person",         "id": "id:twitter.com:2244994945",         "link": "http://www.twitter.com/TwitterDev",         "displayName": "Twitter Dev",         "postedTime": "2013-12-14T04:35:55.036Z",         "image": "https://pbs.twimg.com/profile_images/1283786620521652229/lEODkLTh_normal.jpg",         "summary": "The voice of the #TwitterDev team and your official source for updates, news, and events, related to the #TwitterAPI.",         "friendsCount": 2039,         "followersCount": 512197,         "listedCount": 1662,         "statusesCount": 3632,         "twitterTimeZone": null,         "verified": true,         "utcOffset": null,         "preferredUsername": "TwitterDev",         "languages": [],         "links": [           {             "href": "https://developer.twitter.com/en/community",             "rel": "me"           }         ],         "location": {           "objectType": "place",           "displayName": "127.0.0.1"         },         "favoritesCount": 2147       }`
    

      `"actor": {     "objectType": "person",     "id": "id:twitter.com:6253282",     "link": "http://www.twitter.com/TwitterAPI",     "displayName": "Twitter API",     "postedTime": "2007-05-23T06:01:13.000Z",     "image": "https://pbs.twimg.com/profile_images/942858479592554497/BbazLO9L_normal.jpg",     "summary": "Tweets about changes and service issues. Follow @TwitterDev for more.",     "friendsCount": 39,     "followersCount": 6054164,     "listedCount": 12285,     "statusesCount": 3689,     "twitterTimeZone": null,     "verified": true,     "utcOffset": null,     "preferredUsername": "TwitterAPI",     "languages": [            ],     "links": [       {         "href": "https://developer.twitter.com",         "rel": "me"       }     ],     "favoritesCount": 4   }`