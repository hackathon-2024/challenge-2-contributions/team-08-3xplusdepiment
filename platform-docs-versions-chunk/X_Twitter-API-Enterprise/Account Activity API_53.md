platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/migration/us-ss-migration-guide

### The Account Activity dashboard (sample Account Activity API application)

We've created a sample app to make testing the Account Activity API a little quicker:   

* Download the Account Activity Dashboard sample application [here](https://github.com/twitterdev/Account-Activity-dashboard) (it uses Node.js)
* Follow the instructions on the README to install and launch the app
* Once the application has been launched, you can use the UI to easily set up your webhook and create a new subscription