platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/entities

### JSON example

      `{   "id": 6253282,   "id_str": "6253282",   "name": "Twitter API",   "screen_name": "twitterapi",   "location": "San Francisco, CA",   "description": "The Real Twitter API. I tweet about API changes, service issues and happily answer questions about Twitter and our API. Don't get an answer? It's on my website.",   "url": "http:\/\/t.co\/78pYTvWfJd",   "entities": {     "url": {       "urls": [         {           "url": "http:\/\/t.co\/78pYTvWfJd",           "expanded_url": "http:\/\/dev.twitter.com",           "display_url": "dev.twitter.com",           "indices": [             0,             22           ]         }       ]     },     "description": {       "urls": [                ]     }   } }`