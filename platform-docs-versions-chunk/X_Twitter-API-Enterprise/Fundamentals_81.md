platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/activity

### Next Steps

Explore the other nested objects of this format:

* Review actor object
* Review location object
* Review twitter\_entities object
* See migration guide from Activity Streams to Twitter API v2 format