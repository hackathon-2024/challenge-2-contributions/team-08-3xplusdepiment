platform: X
topic: Twitter-API-Enterprise
subtopic: Compliance Firehose API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Compliance Firehose API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/compliance-firehose-api/guides/honoring-user-intent


### User

| Action | Description |
| --- | --- |
| Protect Account | A Twitter user can protect or unprotect their account at any time. Protected accounts require manual user approval of every person who is allowed to view their account's Tweets.   <br>For more information, see [About Public and Protected Tweets](https://support.twitter.com/articles/14016-about-public-and-protected-tweets). |
| Delete Account | A Twitter user can decide to delete their account and all associated status messages at any time. Twitter retains the account information for 30 days after deletion in case the user decides to undelete and effectively reactivate their account. |
| Scrub Geo | A Twitter user can remove all location data from past Tweets at any time. This known as “scrub geo”. |
| Suspend Account | Twitter retains the right to suspend accounts that are in violation of the Twitter Rules or if an account is suspected to have been hacked or compromised. Account suspensions can only be reversed (unsuspend) by Twitter. |
| Withhold Account | Twitter retains the right to reactively withhold access to certain content in a specific country from time to time. A withheld account can only be made unwithheld by Twitter.   <br>For more information, see [Country Withheld Content](https://support.twitter.com/articles/20169222-country-withheld-content). |