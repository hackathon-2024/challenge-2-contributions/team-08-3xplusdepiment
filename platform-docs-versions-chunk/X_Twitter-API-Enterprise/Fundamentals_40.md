platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/geo

### Derived locations

|     |     |     |
| --- | --- | --- |
| Field | Type | Description |
| derived | locations object | Derived location from the profile geo enrichement  <br>  <br>    **"derived": {**<br><br>      **"locations": \[**<br><br>        **{**<br><br>          **"country":** "United Kingdom"**,**<br><br>          **"country\_code":** "GB"**,**<br><br>          **"locality":** "Yorkshire"**,**<br><br>          **"region":** "England"**,**<br><br>          **"full\_name":** "Yorkshire, England, United Kingdom"**,**<br><br>          **"geo": {**<br><br>            **"coordinates": \[**<br><br>              \-1.5**,**<br><br>              54<br><br>            **\],**<br><br>            **"type":** "point"<br><br>          **}**<br><br>        **}**<br><br>      **\]**<br><br>    **}** |