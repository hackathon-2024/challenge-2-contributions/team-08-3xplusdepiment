platform: X
topic: Twitter-API-Enterprise
subtopic: Search API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Search API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/overview

Overview

**Please note:**

We have released a new version of [search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search) and [Tweet counts](https://developer.twitter.com/en/docs/twitter-api/tweets/counts) in [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api). While we have not announced a deprecation timeline for this API yet, we do encourage you to start to experiment and [review what's new](https://developer.twitter.com/en/docs/twitter-api/migrate) with Twitter API v2. 

These endpoints have been updated to include Tweet edit metadata. Learn more about these metadata on the ["Edit Tweets" fundamentals page](https://developer.twitter.com/en/docs/twitter-api/enterprise/edit-tweets).