platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/user


###   
Example user object:

      `"user": { 		"id": 2244994945, 		"id_str": "2244994945", 		"name": "Twitter Dev", 		"screen_name": "TwitterDev", 		"location": "127.0.0.1", 		"url": "https://developer.twitter.com/en/community", 		"description": "The voice of the #TwitterDev team and your official source for updates, news, and events, related to the #TwitterAPI.", 		"translator_type": "regular", 		"protected": false, 		"verified": true, 		"followers_count": 512292, 		"friends_count": 2038, 		"listed_count": 1666, 		"favourites_count": 2147, 		"statuses_count": 3634, 		"created_at": "Sat Dec 14 04:35:55 +0000 2013", 		"utc_offset": null, 		"time_zone": null, 		"geo_enabled": true, 		"lang": null, 		"contributors_enabled": false, 		"is_translator": false, 		"profile_background_color": "FFFFFF", 		"profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png", 		"profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png", 		"profile_background_tile": false, 		"profile_link_color": "0084B4", 		"profile_sidebar_border_color": "FFFFFF", 		"profile_sidebar_fill_color": "DDEEF6", 		"profile_text_color": "333333", 		"profile_use_background_image": false, 		"profile_image_url": "http://pbs.twimg.com/profile_images/1283786620521652229/lEODkLTh_normal.jpg", 		"profile_image_url_https": "https://pbs.twimg.com/profile_images/1283786620521652229/lEODkLTh_normal.jpg", 		"profile_banner_url": "https://pbs.twimg.com/profile_banners/2244994945/1594913664", 		"default_profile": false, 		"default_profile_image": false, 		"following": null, 		"follow_request_sent": null, 		"notifications": null 	}`