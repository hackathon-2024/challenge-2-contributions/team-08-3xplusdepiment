platform: X
topic: Twitter-API-Enterprise
subtopic: Search API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Search API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/api-reference/enterprise-search

Enterprise search APIs

enterprise-search

# Enterprise search APIs

Jump to on this page

[Introduction](#Introduction)

[Methods](#Methods)

[Authentication](#Authentication)

[Request/response behavior](#RequestResponseBehavior)

[Pagination](#Pagination)

[Data endpoint](#DataEndpoint)

[Data request parameters](#DataParameters)

[Example data requests and responses](#DataRequestExamples)

[Counts endpoint](#CountsEndpoint)

[Counts request parameters](#CountsParameters)

[Example counts requests and responses](#CountsRequestExamples)

[HTTP response codes](#HTTPCodes)