platform: X
topic: Twitter-API-Enterprise
subtopic: Search API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Search API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/guides/changelog

Full-archive search - Metadata and filtering timeline

## Full-Archive Search metadata timeline

That article discusses how the historical changes of the full-archive roadmap affects creating the filters needed to find your historical signal of interest. This article and a complementary [article about Historical PowerTrack](https://developer.twitter.com/en/docs/twitter-api/enterprise/historical-powertrack-api/guides/hpt-timeline), will serve as a ‘compare and contrast’ discussion of the two Twitter historical products.