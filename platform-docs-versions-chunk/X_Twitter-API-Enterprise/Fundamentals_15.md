platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/account

### Usage Thresholds

Configure volume thresholds for your products. These will initiate email alerts for the users who have configured those notifications in their profiles, both for the warning threshold and critical threshold for each product.

  

Please note that these thresholds are evaluated once per day at 19:30 (UTC), and are not evaluated in real-time.

  
  

### Next Steps:

* [Review enterprise data formats](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/overview.html)
* [Explore the Twitter API v2 developer portal](https://developer.twitter.com/en/docs/developer-portal/overview.html)
* [See enterprise support resources](https://developer.twitter.com/en/support/twitter-api.html#item6)