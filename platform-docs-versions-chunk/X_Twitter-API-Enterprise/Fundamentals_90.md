platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/location

## Examples

      `"location": {     "objectType": "place",     "displayName": "Kansas, USA",     "name": "Kansas",     "country_code": "United States",     "twitter_country_code": "US",     "twitter_place_type": "admin",     "link": "https://api.twitter.com/1.1/geo/id/27c45d804c777999.json",     "geo": {       "type": "Polygon",       "coordinates": [         [           [             -102.051769,             36.99311           ],           [             -102.051769,             40.003282           ],           [             -94.588081,             40.003282           ],           [             -94.588081,             36.99311           ]         ]       ]     }`
    

      `    "location": {       "objectType": "place",       "displayName": "California, USA"     }`