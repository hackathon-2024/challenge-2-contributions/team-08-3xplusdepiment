platform: X
topic: Twitter-API-Enterprise
subtopic: Decahose API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Decahose API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/decahose-api/overview/streaming-likes

## Decahose

* For the 10% sample Tweets delivered in the Decahose, stream includes 100% of the applicable public likes
* **Partitions:** 2
* **URL Structure**
    * https://gnip-stream.twitter.com/stream/sample10-likes/accounts/<accountName>/publishers/twitter/<streamLabel>.json?partition=1