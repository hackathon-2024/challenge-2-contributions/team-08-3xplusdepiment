platform: X
topic: Twitter-API-Enterprise
subtopic: PowerTrack API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/PowerTrack API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/powertrack-api/overview


# Overview

[Enterprise](https://developer.twitter.com/en/products/twitter-api/enterprise)

_This is an enterprise API available within our managed access levels only. To use this API, you must first set up an account with our enterprise sales team. [Learn more](https://developer.twitter.com/en/products/twitter-api/enterprise)_  

_You can view all of the Twitter API filtered stream offerings [HERE](https://developer.twitter.com/en/docs/twitter-api/filtered-stream-overview)._

The PowerTrack API provides customers with the ability to filter the full Twitter firehose, and only receive the data that they or their customers are interested in. This is accomplished by applying the PowerTrack filtering language - see [Rules and filtering](https://developer.twitter.com/en/docs/twitter-api/enterprise/rules-and-filtering/overview.html) - to match Tweets based on a wide variety of attributes, including user attributes, geo-location, language, and many others. Using PowerTrack rules to filter Tweet ensures that customers receive all of the data, and _only_ the data they need for your app.