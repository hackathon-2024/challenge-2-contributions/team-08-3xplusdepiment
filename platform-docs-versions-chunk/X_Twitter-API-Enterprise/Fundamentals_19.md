platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/overview


### Object comparison per data format 

Whatever your Twitter use case, understanding what these JSON-encoded Tweet objects and attributes _represent_ is critical to successfully finding your data signals of interest. To help in that effort, there are a set of pages dedicated to each object in each data format_._

Reflecting the JSON hierarchy above, here are links to each of these objects:

| Native Enriched | Activity Streams |
| --- | --- |
| [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/tweet) Tweet object | [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/tweet) Activity object |
| [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/user) User object | [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/user) Actor object |
| [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/entities) Entities object | [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/entities) Twitter entities object |
| [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/extended-entities) Extended entities object | [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/extended-entities) Twitter extended entitites object |
| [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/geo) Geo object | [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/geo) Location object |
| n/a | [Link](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/gnip.html) Gnip object |