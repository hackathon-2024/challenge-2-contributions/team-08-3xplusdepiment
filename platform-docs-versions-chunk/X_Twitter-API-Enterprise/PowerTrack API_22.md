platform: X
topic: Twitter-API-Enterprise
subtopic: PowerTrack API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/PowerTrack API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/powertrack-api/guides/rule-limits

Rule limits

## Rule limits

Twitter will now begin to enforce long-held contractual limits for the number of rules that a customer is able to add to their stream by enforcing rule limits on PowerTrack. While these limits have always been observed, we are now making it easier for customers to know where their limits stand and how close they are to their cap. Functionality has been added to our console that will allow you to observe your current rule count for each product and stream. This information can be found on the right hand side of a product page just under the activity counter (see below).

  

This can also be found under the rules section of the usage tab (see below).