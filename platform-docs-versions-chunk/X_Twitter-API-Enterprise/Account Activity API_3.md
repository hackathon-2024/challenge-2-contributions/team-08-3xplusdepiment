platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/overview

### Feature summary

| Tier | Pricing | Number of unique subscriptions | Number of webhooks | Reliability and Activity Recovery |
| --- | --- | --- | --- | --- |
| Enterprise | [Contact sales](https://developer.twitter.com/content/developer-twitter/en/enterprise-application) | Up to 500+ | 3+  | [Retries](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/activity-retries) and [Replay](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/activity-replay-api) |