platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/activity


### Long object

Activity streams format of the extended\_tweet

      `{   "id": "tag:search.twitter.com,2005:1050118621198921728",   "objectType": "activity",   "verb": "post",   "postedTime": "2018-10-10T20:19:24.000Z",   "generator": {     "displayName": "Twitter Web Client",     "link": "http://twitter.com"   },   "provider": {     "objectType": "service",     "displayName": "Twitter",     "link": "http://www.twitter.com"   },   "link": "http://twitter.com/TwitterAPI/statuses/1050118621198921728",   "body": "To make room for more expression, we will now count all emojis as equal—including those with gender‍‍‍ ‍‍and skin t… https://t.co/MkGjXf9aXm",   "long_object": {     "body": "To make room for more expression, we will now count all emojis as equal—including those with gender‍‍‍ ‍‍and skin tone modifiers 👍🏻👍🏽👍🏿. This is now reflected in Twitter-Text, our Open Source library. \n\nUsing Twitter-Text? See the forum post for detail: https://t.co/Nx1XZmRCXA",     "display_text_range": [       0,       277     ],     "twitter_entities": {see twitter_entities object},   "actor": {see actor object},   "object": {     "objectType": "note",     "id": "object:search.twitter.com,2005:1050118621198921728",     "summary": "To make room for more expression, we will now count all emojis as equal—including those with gender‍‍‍ ‍‍and skin t… https://t.co/MkGjXf9aXm",     "link": "http://twitter.com/TwitterAPI/statuses/1050118621198921728",     "postedTime": "2018-10-10T20:19:24.000Z"   },   "favoritesCount": 298,   "twitter_entities": {see twitter_entities object},   "twitter_lang": "en",   "retweetCount": 153,   "gnip": {see gnip object},   "twitter_filter_level": "low" }`