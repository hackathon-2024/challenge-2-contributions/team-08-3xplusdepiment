platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/migration/us-ss-migration-guide


### Available Activities

| Message Type | Details |
| --- | --- |
| [tweet\_create\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#tweet_create_events) | Tweet status payload when any of the following actions are taken by or to the subscription user: Tweets, Retweets, Replies, @mentions, QuoteTweets |
| [favorite\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#favorite_events) | Favorite (like) event status with the user and target. |
| [follow\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#follow_events) | Follow event with the user and target. |
| [block\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#block_events) | Block event with the user and target. |
| [mute\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#mute_events) | Mute event with the user and target. |
| [direct\_message\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#direct_message_events) | Direct message status with the user and target. |
| [direct\_message\_indicate\_typing\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#direct_message_indicate_typing_events) | Direct message typing event with the user and target. |
| [direct\_message\_mark\_read\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#direct_message_mark_read_events) | Direct message read event with the user and target. |