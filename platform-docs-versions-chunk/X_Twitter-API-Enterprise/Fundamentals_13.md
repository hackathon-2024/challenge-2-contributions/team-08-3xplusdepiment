platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/account

Account tab

## Account tab

### My Profile

View details about your individual user profile, and change your password here. Additionally, you may configure account usage threshold notices specific to your account.  API status notices are available through the status page at https://api.twitterstat.us.