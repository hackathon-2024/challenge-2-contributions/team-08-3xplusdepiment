platform: X
topic: Twitter-API-Enterprise
subtopic: Engagement API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Engagement API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/engagement-api/api-reference/post-insights-engagement

POST insights/engagement

post-insights-engagement

# POST insights/engagement

Jump to on this page

[Methods](#Methods)

[Authentication](#Authentication)

[POST /insights/engagement/totals](#Totals)

[POST /insights/engagement/28hr](#api-28hr)

[POST /insights/engagement/historical](#Historical)

[Response Codes](#ResponseCodes)

[Error Messages](#ErrorMessages)

## Methods [¶](#methods- "Permalink to this headline")

| Method | Description |
| --- | --- |
| [POST /insights/engagement/totals](#Totals) | Retrieve total impressions and engagements for a collection of Tweets. |
| [POST /insights/engagement/historical](#Historical) | Retrieve impressions and engagements for a collection of Tweets for a period up to 4 weeks in duration, back to September 1, 2014. |
| [POST /insights/engagement/28hr](#api-28hr) | Retrieve impressions and engagements for a collection of Tweets for the past 28 hours. |