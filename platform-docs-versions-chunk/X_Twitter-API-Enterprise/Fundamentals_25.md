platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/tweet

### Deprecated Attributes

|     |     |     |
| --- | --- | --- |
| Field | Type | Description |
| geo | Object | **Deprecated.** _Nullable._ Use the `coordinates` field instead. This deprecated attribute has its coordinates formatted as _\[lat, long\]_, while all other Tweet geo is formatted as _\[long, lat\]_. |