platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects


## Available Activities

| Message Type | Details |
| --- | --- |
| [tweet\_create\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#tweet_create_events) | Tweet status payload when any of the following actions are taken by or to the subscription user: Tweets, Retweets, Replies, @mentions, QuoteTweets, Retweet of Quote Tweets. |
| [favorite\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#favorite_events) | Favorite (like) event status with the user and target. |
| [follow\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#follow_events) | Follow event with the user and target. |
| [unfollow\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#unfollow_events) | Unfollow event with the user and target. |
| [block\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#block_events) | Block event with the user and target. |
| [unblock\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#unblock_events) | Unblock event with the user and target. |
| [mute\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#mute_events) | Mute event with the user and target. |
| [unmute\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#unmute_events) | Unmute event with the user and target. |
| [user\_event](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#user_event) | Revoke events sent when a user removes application authorization and subscription is automatically deleted. |
| [direct\_message\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#direct_message_events) | Direct message status with the user and target when a direct message is sent or received. |
| [direct\_message\_indicate\_typing\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#direct_message_indicate_typing_events) | Direct message typing event with the user and target. |
| [direct\_message\_mark\_read\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#direct_message_mark_read_events) | Direct message read event with the user and target. |
| [tweet\_delete\_events](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/account-activity-data-objects#tweet_delete_events) | Notice of deleted Tweets to make it easier to maintain compliance. |