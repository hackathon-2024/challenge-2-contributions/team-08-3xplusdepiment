platform: X
topic: Twitter-API-Enterprise
subtopic: Compliance Firehose API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Compliance Firehose API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/compliance-firehose-api/overview

Overview

**Please note**

We have released a new compliance tool to Twitter API v2 called [batch compliance](https://developer.twitter.com/en/docs/twitter-api/compliance/batch-compliance). This new tool allows you to upload large datasets of Tweet or user IDs to retrieve their compliance status in order to determine what data requires action in order to bring your datasets into compliance.

In addtion, both the batch compliance and the compliance firehose have been updated to support Tweet edits. For the compliance firehose, a new 'tweet\_edit' event was added. See the [Compliance Data Objects](https://developer.twitter.com/en/docs/twitter-api/enterprise/compliance-firehose-api/guides/compliance-data-objects) documentation for more details. Learn more about how Edit Tweet metadata works on the [Edit Tweets fundamentals](https://developer.twitter.com/en/docs/twitter-api/edit-tweets) page.