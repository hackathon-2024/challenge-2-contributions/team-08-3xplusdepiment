platform: X
topic: Twitter-API-Enterprise
subtopic: Decahose API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Decahose API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/decahose-api/overview/streaming-likes

## Like Delete / “Unlike” payload

      `{      "delete":{         "favorite":{            "tweet_id":696615514970279937,          "tweet_id_str":"696615514970279937",          "user_id":2510287578,          "user_id_str":"2510287578"       },       "timestamp_ms":"1480437031205"    } }`