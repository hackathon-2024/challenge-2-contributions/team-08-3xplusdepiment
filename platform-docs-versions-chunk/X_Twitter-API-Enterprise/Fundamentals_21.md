platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/tweet

Tweet object

Interested in learning more about how the Native Enriched data format maps to the Twitter API v2 format?

Check out our comparison guide: [Native Enriched compared to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/native-enriched-to-v2)