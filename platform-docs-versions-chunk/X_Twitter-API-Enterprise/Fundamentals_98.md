platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/gnip

### Example:

      `  "gnip": {     "matching_rules": [       {         "tag": null       }     ],     "urls": [       {         "url": "https://t.co/Nx1XZmRCXA",         "expanded_url": "https://twittercommunity.com/t/new-update-to-the-twitter-text-library-emoji-character-count/114607",         "expanded_status": 200,         "expanded_url_title": null,         "expanded_url_description": null       }     ]   }`