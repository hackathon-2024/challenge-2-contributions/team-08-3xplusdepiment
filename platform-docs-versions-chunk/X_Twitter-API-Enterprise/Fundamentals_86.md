platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/actor

### Next Steps

Explore the other sub-objects that a Tweet contains:

* [Tweet object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object.html)
* [Entities object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object.html)
* [Extended Entities object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object.html)
* [Tweet geo objects and data dictionaries](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/geo-objects.html)