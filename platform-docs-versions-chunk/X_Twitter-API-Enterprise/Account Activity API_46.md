platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/migration/introduction

## Next steps

Review our [User Streams and Site Streams migration guide](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/migration/us-ss-migration-guide)

Review our [Direct Message API migration guide](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/migration/direct-message-migration)

Learn more about the [Account Activity API](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/overview)