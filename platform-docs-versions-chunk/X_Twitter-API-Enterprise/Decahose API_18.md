platform: X
topic: Twitter-API-Enterprise
subtopic: Decahose API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Decahose API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/decahose-api/api-reference/decahose

## Replay API  [¶](#replay-api- "Permalink to this headline")

The Replay API is an important complement to realtime Volume streams. Replay is a data recovery tool that provides streaming access to a rolling window of recent Twitter historical data.