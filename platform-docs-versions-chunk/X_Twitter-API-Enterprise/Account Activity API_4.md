platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/overview


## Next steps

* [Apply for access and get started with webhooks.](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/getting-started-with-webhooks)
* Check out our [API references](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/api-reference) to find the right endpoint for the job.
* Follow this step-by-step [tutorial on how to get started with the Account Activity API](https://developer.twitter.com/content/developer-twitter/en/docs/tutorials/getting-started-with-the-account-activity-api).
* Follow this [step-by-step tutorial on how to build a customer engagement application](https://developer.twitter.com/content/developer-twitter/en/docs/tutorials/customer-engagement-application-playbook) on Twitter.  
      
    
* Have questions? Running into errors?
    * Read our [Frequently asked questions](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/faq) or [Error Troubleshooting guide](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/faq#troubleshooting).  
          
        
* Explore our sample code:  
    * [Enterprise Account Activity API dashboard](https://github.com/twitterdev/account-activity-dashboard-enterprise), a node web app that displays webhook events using the enterprise tier of the Account Activity API and includes [Replay](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/account-activity-api/guides/activity-replay-api) functionality.
    * The [SnowBot chatbot](https://github.com/twitterdev/SnowBotDev), a Ruby web app built on the enterprise Account Activity and Direct Message APIs.