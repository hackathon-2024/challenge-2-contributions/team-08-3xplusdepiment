platform: X
topic: Twitter-API-Enterprise
subtopic: Engagement API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Engagement API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/engagement-api/overview

### Engagement groupings

Groupings enable custom organization of the returned engagement metrics. You can include a maximum of 3 groupings per request. You can choose to group the metrics by one or more of the following values:

_All three endpoints support:_

* tweet.id
* engagement.type  
     

_The `/28hr` and `/historical` can provide time-series metrics, and thus support:_

* engagement.day
* engagement.hour  
      
    

To learn more about grouping, please visit the [Engagement API Grouping](https://developer.twitter.com/content/developer-twitter/en/docs/metrics/get-tweet-engagement/guides/grouping-results) page within the Guides section.