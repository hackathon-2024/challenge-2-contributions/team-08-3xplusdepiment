platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/product

### API Help

The API Help page provides the API endpoint URLs for your stream, as well as the Rules API endpoint for the stream, where applicable. In addition, it includes sample curl commands and instructions on how to connect to the stream endpoint, and how to programmatically add, delete, and list rules from your stream's Rules API endpoint.

  

  
  

### Rules

The Rules tab is available for PowerTrack streams, and provides a quick way to get started by manually entering plain text rules via a user interface. Note that the interface only supports adding up to 1000 rules via this manual method, and should be only used for initial testing. We recommend managing your rules programmatically via the [Rules API](https://developer.twitter.com/en/docs/twitter-api/enterprise/powertrack-api/api-reference.html) in any production setting.