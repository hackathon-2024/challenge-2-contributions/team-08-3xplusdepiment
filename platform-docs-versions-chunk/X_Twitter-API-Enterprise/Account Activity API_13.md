platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/guides/getting-started-with-webhooks

Getting started with webhooks

**Enterprise**

## Getting started with webhooks

The Account Activity API is a webhook-based API that sends account events to a web app you develop, deploy and host. 

There are several 'plumbing' details that need attention before you can start receiving webhook events in your event consumer application. As described below, you will need to create a Twitter app, obtain Account Activity API access, and develop a web app that consumes webhook events.