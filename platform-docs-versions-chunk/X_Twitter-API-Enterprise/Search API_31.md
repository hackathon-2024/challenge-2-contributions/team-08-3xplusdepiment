platform: X
topic: Twitter-API-Enterprise
subtopic: Search API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Search API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/api-reference/enterprise-search

## Authentication [¶](#authentication- "Permalink to this headline")

All requests to the enterprise search APIs must use HTTP _Basic Authentication_, constructed from a valid email address and password combination used to log into your account at [https://console.gnip.com](https://console.gnip.com/). Credentials must be passed as the _Authorization_ header for each request.