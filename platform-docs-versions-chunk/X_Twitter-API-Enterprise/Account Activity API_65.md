platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/direct-messages/sending-and-receiving/guides/direct-message-migration.html

## Next Steps

*     Download our Direct Message Migration Guide (below)

Direct Messages[Direct Message - Migration Guide
--------------------------------](https://cdn.cms-twdigitalassets.com/content/dam/developer-twitter/pdfs-and-files/DM%20-%20Migration%20Guide.pdf)

[Download PDF](https://cdn.cms-twdigitalassets.com/content/dam/developer-twitter/pdfs-and-files/DM%20-%20Migration%20Guide.pdf)