platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/tweet-timeline

## Next steps

Now that we’ve explored the timeline of when key Twitter features were introduced and learned how these metadata changes affect filtering at a high-level, the next step is to get into the many product-specific details:

* [Learn more about the Historical PowerTrack API and its metadata and filtering timeline](https://developer.twitter.com/en/docs/tweets/batch-historical/guides/hpt-timeline)
* [Learn more about the Full-Archive Search API and its metadata and filtering timeline](https://developer.twitter.com/en/docs/tweets/search/guides/fas-timeline)
* [Choosing between Historical PowerTrack and Full-Archive Search APIs](https://developer.twitter.com/en/docs/tutorials/choosing-historical-api)