platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/user


###   
No longer supported (deprecated) attributes

| Field | Type | Description |
| --- | --- | --- |
| utc\_offset | null | Value will be set to null. Still available via [GET account/settings](https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-settings) |
| time\_zone | null | Value will be set to null. Still available via [GET account/settings](https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-settings) as tzinfo\_name |
| lang | null | Value will be set to null. Still available via [GET account/settings](https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-settings) as language |
| geo\_enabled | null | Value will be set to null.  Still available via [GET account/settings](https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-settings). This field must be true for the current user to attach geographic data when using [POST statuses / update](https://developer.twitter.com/en/docs/tweets/post-and-engage/guides/post-tweet-geo-guide) |
| following | null | Value will be set to null. Still available via [GET friendships/lookup](https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-friendships-lookup) |
| follow\_request\_sent | null | Value will be set to null. Still available via [GET friendships/lookup](https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-friendships-lookup) |
| has\_extended\_profile | null | **Deprecated**. Value will be set to null. |
| notifications | null | **Deprecated**. Value will be set to null. |
| profile\_location | null | **Deprecated**. Value will be set to null. |
| contributors\_enabled | null | **Deprecated**. Value will be set to null. |
| profile\_image\_url | null | **Deprecated**. Value will be set to null. NOTE: Profile images are only available using the profile\_image\_url\_https field. |
| profile\_background\_color | null | **Deprecated**. Value will be set to null. |
| profile\_background\_image\_url | null | **Deprecated**. Value will be set to null. |
| profile\_background\_image\_url\_https | null | **Deprecated**. Value will be set to null. |
| profile\_background\_tile | null | **Deprecated**. Value will be set to null. |
| profile\_link\_color | null | **Deprecated**. Value will be set to null. |
| profile\_sidebar\_border\_color | null | **Deprecated**. Value will be set to null. |
| profile\_sidebar\_fill\_color | null | **Deprecated**. Value will be set to null. |
| profile\_text\_color | null | **Deprecated**. Value will be set to null. |
| profile\_use\_background\_image | null | **Deprecated**. Value will be set to null. |
| is\_translator | null | **Deprecated**. Value will be set to null. |
| is\_translation\_enabled | null | **Deprecated**. Value will be set to null. |
| translator\_type | null | **Deprecated**. Value will be set to null. |