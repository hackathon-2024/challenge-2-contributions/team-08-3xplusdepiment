platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/direct-messages/sending-and-receiving/guides/direct-message-migration.html

### New features

The new Direct Message API endpoints support a number of new capabilities and provide improved access to previous Direct Messages. New features include:

* Support for media attachments (image, GIF, and video).
* Ability to prompt users for structured replies with a predefined options list.
* Up to 30 days of access to past Direct Messages.

For a full list of new Direct Message features and additional new API endpoints refer to the [technical documentation](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/sending-and-receiving/overview).