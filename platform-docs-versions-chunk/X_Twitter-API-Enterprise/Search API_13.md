platform: X
topic: Twitter-API-Enterprise
subtopic: Search API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Search API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/quick-start/enterprise-30-day

### Referenced articles

* [Introduction to Tweet objects](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/data-dictionary/overview)
* [Premium search operators](https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/guides/enterprise-operators)
* [Tweet objects and payloads](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects) 

## Next Steps

* [Discover more about the counts endpoint](https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/api-reference/enterprise-search)  
    
* [Join the conversation on Twitter community forums](https://twittercommunity.com/)