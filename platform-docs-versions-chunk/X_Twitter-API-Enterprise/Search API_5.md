platform: X
topic: Twitter-API-Enterprise
subtopic: Search API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Search API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/overview

### Data availability / important date

When using the Full-Archive search API, keep in mind that the Twitter platform has continued to evolve since 2006. As new features were added, the underlying JSON objects have had new metadata added to it. For that reason it is important to understand when Tweet attributes were added that search operators match on. Below are some of the more fundamental 'born on' dates of important groups of metadata. To learn more about when Tweet attributes were first introduced, see [this guide](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/enterprise/search-api/guides/changelog).  

* First Tweet: 3/21/2006
* First Native Retweets: 11/6/2009
* First Geo-tagged Tweets: 11/19/2009
* URLs first indexed for filtering: 8/27/2011
* Enhanced URL expansion metadata (website titles and descriptions): 12/1/2014
* Profile Geo enrichment metadata and filtering: 2/17/2015