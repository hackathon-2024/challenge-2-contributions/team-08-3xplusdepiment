platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/account

### Account Settings

You may add, remove, and edit users, and configure email notifications for individual users and usage threshold alerts.

  

Please note that there are three types of users – Account Admin, User, and Email Only.  

* Account admins are allowed to create/delete/edit other users, and can use basic authentication with username (email) and password to connect to the enterprise APIs
    
* Users cannot create or modify other users, but can use basic authentication with username (email) and password to connect to the enterprise APIs
    
* Email Only users do not have access to the dashboard, are not authorized to connect to the enterprise APIs  and only receive notifications, if they are configured to receive them in the Notifications section.