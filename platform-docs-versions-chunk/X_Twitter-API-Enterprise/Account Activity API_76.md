platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/api-reference/aaa-enterprise

### Response[¶](#response "Permalink to this headline")

_HTTP 204 OK_

    { }

### Error Messages[¶](#error-messages "Permalink to this headline")

| Code | Message |
| --- | --- |
| 34  | Webhook does not exist or is associated with a different twitter application. |
| 214 | Webhook URL does not meet the requirements. |
| 214 | Webhook URL does not meet the requirements. Invalid CRC token or json response format. |
| 214 | High latency on CRC GET request. Your webhook should respond in less than 3 seconds. |
| 214 | Non-200 response code during CRC GET request (i.e. 404, 500, etc). |