platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/twitter-entities

## Next steps

Explore other Tweet JSON objects and data dictionaries:

* [Extended Entities object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object)
* [Tweet object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object)
* [User object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object)
* [Tweet geo objects and data dictionaries](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/geo-objects)