platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/console/product


## Products tab

Upon logging into your account at [console.gnip.com](http://console.gnip.com/), you will land on the products tab of the dashboard.  This page includes an overview of the enterprise products currently available on your account.

For products using streaming delivery like PowerTrack or Decahose, this page lists the product name, and stream label,  the number of current active connections, the number of rules currently active for each (where applicable), and the raw count of activities (for example, Tweets) delivered in the most recent 24 hours.

For products with REST delivery, like Search API, this page lists the product name,labels (also shown as "streams"), current activities (for example, Tweets) returned through these endpoints, and a few different request counts per endpoint.

Note that the [Usage API](https://developer.twitter.com/en/docs/twitter-api/enterprise/usage-api/overview.html) delivers much of this data programatically.

Specific details per stream are available by clicking the name, or the settings button.