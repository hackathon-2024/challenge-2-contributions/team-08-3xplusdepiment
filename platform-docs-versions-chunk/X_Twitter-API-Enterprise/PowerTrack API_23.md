platform: X
topic: Twitter-API-Enterprise
subtopic: PowerTrack API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/PowerTrack API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/powertrack-api/guides/rule-limits

## What If I Hit My Cap?

If you attempt to upload more rules to your stream that you are contractually allowed, you will receive the following message:

_“Request exceeds account’s Rule Limit. Delete rules or contact your account manager to proceed.”_

If you encounter this error message while you have an open connection, your stream will not be disrupted. In order to add more rules once you hit your cap, you will either need to delete rules from your stream or reach out to your account manager to increase your contractual limit.