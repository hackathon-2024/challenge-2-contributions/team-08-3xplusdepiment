platform: X
topic: Twitter-API-Enterprise
subtopic: Usage API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Usage API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/usage-api/overview

## Supported APIs

Below is a list of the APIs currently supported by the Usage API:

* PowerTrack API enterprise
* 30-Day Search API enterprise
* Full-Archive Search API enterprise
* Historical PowerTrack enterprise