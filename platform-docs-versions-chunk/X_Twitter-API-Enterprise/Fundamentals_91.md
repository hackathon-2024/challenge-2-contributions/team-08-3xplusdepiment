platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/activity-streams-objects/location

## Next steps

Explore other Tweet JSON objects and data dictionaries:

* [Tweet object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object)
* [User object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object)
* [Entities object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/entities-object)
* [Extended Entitites object and data dictionary](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object)

Read more about Tweets and their location metadata:

* [Introduction to Tweet geospatial metadata](https://developer.twitter.com/en/docs/tutorials/tweet-geo-metadata) 
* [Filtering Twitter data by location](https://developer.twitter.com/en/docs/tutorials/filtering-tweets-by-location)