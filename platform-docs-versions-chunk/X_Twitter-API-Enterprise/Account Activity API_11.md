platform: X
topic: Twitter-API-Enterprise
subtopic: Account Activity API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Account Activity API.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/account-activity-api/quick-start/a-visual-walkthrough-of-the-account-activity-api

A video walkthrough of the Account Activity API

## A video walkthrough of the Account Activity API

In this video walkthrough, you will learn about the capabilities of the premium and enterprise tiers of the Account Activity API.

By the end of this video, you will learn about the following capabilities.

* Registering a webhook
* Adding a user subscription
* Removing a user subscription
* Receiving account activities
* Replay account activities

> Tired of reading? We’ve got you covered. Learn about the capabilities of the Account Activity API in this video walkthrough with [@tonyv00](https://twitter.com/tonyv00?ref_src=twsrc%5Etfw) from our DevRel team. 🍿 ⬇️ [pic.twitter.com/LdHy4aLu0i](https://t.co/LdHy4aLu0i)
> 
> — Twitter Dev (@TwitterDev) [December 9, 2019](https://twitter.com/TwitterDev/status/1204084171334832128?ref_src=twsrc%5Etfw)

Next steps