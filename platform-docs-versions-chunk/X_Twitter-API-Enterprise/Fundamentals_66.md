platform: X
topic: Twitter-API-Enterprise
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Enterprise/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/extended-entities

### Next Steps

Explore the other sub-objects that a Tweet contains:

* [Tweet object and data dictionary](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/tweet)
* [User object and data dictionary](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/user)
* [Entities object and data dictionary](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/entities)
* [Tweet geo objects and data dictionaries](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary/native-enriched-objects/geo)