platform: Wikipedia
topic: DSA-Statistics
subtopic: DSA-Statistics
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Wikipedia_DSA-Statistics/DSA-Statistics.md
url: https://foundation.wikimedia.org/wiki/Legal:EU_DSA_Userbase_Statistics

## References

1. ↑ [1.0](#cite_ref-Cisco_1-0) [1.1](#cite_ref-Cisco_1-1) [1.2](#cite_ref-Cisco_1-2) [Cisco Annual Internet Report (2018–2023) White Paper](https://www.cisco.com/c/en/us/solutions/collateral/executive-perspectives/annual-internet-report/white-paper-c11-741490.html). 9 March 2020