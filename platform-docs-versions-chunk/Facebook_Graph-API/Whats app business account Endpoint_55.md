platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/phone_numbers/

### Requirements

| Type | Description |
| --- | --- |
| [Access Tokens](https://developers.facebook.com/docs/facebook-login/guides/access-tokens) | [User](https://developers.facebook.com/docs/facebook-login/access-tokens#usertokens) or [System User](https://www.facebook.com/business/help/503306463479099). |
| [Permissions](https://developers.facebook.com/docs/permissions/reference) | [whatsapp\_business\_management](https://developers.facebook.com/docs/permissions/reference/whatsapp_business_management)  <br>[whatsapp\_business\_messaging](https://developers.facebook.com/docs/permissions/reference/whatsapp_business_messaging) |