platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/guides/secure-requests

## Login Security

There are a large number of settings you can change to improve the security of your app. Please see our [Login Security](https://developers.facebook.com/docs/facebook-login/security/) documentation for a checklist of things you can do.

It's also worth looking at our [access token](https://developers.facebook.com/docs/facebook-login/access-tokens/) documentation which covers various architectures and the security trade-offs that you should consider.

## Server Allow List

We also enable you to restrict some of your API calls to come from a list of servers that you have allowed to make calls. Learn more in our [login security](https://developers.facebook.com/docs/facebook-login/security#surfacearea) documentation.