platform: Facebook
topic: Graph-API
subtopic: Page Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page/

## Creating

This is only available to select developers. Please contact your Facebook Partner for more information.

You can make a POST request to `take_thread_control` edge from the following paths:

* [`/{page_id}/take_thread_control`](https://developers.facebook.com/docs/graph-api/reference/page/take_thread_control/)

When posting to this edge, a [Page](https://developers.facebook.com/docs/graph-api/reference/page/) will be created.