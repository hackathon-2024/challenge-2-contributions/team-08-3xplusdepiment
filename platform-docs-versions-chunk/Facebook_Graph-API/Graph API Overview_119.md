platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/advanced/api-upgrade

## Learn How a Change Affects Your App

The API Upgrade Tool displays a customized list of changes that impact an app when upgrading to a specified target version. This allows you to view all relevant changes between the source and target versions.

Step 1. In the [Upgrade tool](https://developers.facebook.com/tools/api_versioning/), select your app from the dropdown menu or type in the name of the app.

The dropdown menu only lists up to ten apps. To view more apps than those listed, use the search bar in the dropdown menu.

Step 2. Use the dropdown menus to the right to select the version you would like to **Upgrade from** and the version you would like to **Upgrade to**.