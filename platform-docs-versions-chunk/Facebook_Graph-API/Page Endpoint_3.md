platform: Facebook
topic: Graph-API
subtopic: Page Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page/

### Example

curl -i -X GET "https://graph.facebook.com/PAGE-ID?access\_token=ACCESS-TOKEN"

### Parameters

| Parameter | Description |
| --- | --- |
| `account_linking_token`<br><br>UTF-8 encoded string | Short lived account linking token (5 mins expiry) to get the PSID for a user-page pair |