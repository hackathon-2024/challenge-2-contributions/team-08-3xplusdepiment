platform: Facebook
topic: Graph-API
subtopic: Collaborative ads directory Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Collaborative ads directory Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/collaborative-ads-directory/

### Parameters

This endpoint doesn't have any parameters.

### Fields

| Field | Description |
| --- | --- |
| `collaborative_ads_merchants`<br><br>[list<Business>](https://developers.facebook.com/docs/marketing-api/reference/business/) | List of publicly viewable merchants that use collaborative ads |

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can't perform this operation on this endpoint.

## Deleting

You can't perform this operation on this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)