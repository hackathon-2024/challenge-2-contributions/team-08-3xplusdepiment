platform: Facebook
topic: Graph-API
subtopic: App Link Host Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/App Link Host Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app-link-host

### Permissions

* An app access token is required to update app link hosts belonging to that app.
    

### Fields

| Name | Description | Type |
| --- | --- | --- |
| `name` | A custom name for this app link host. | `string` |

### Response

Returns `true` successful, otherwise an error message.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)