platform: Facebook
topic: Graph-API
subtopic: Photo Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Photo Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/photo/

### Edges

| Edge | Description |
| --- | --- |
| [`insights`](https://developers.facebook.com/docs/graph-api/reference/photo/insights/) | Insights data |
| [`likes`](https://developers.facebook.com/docs/graph-api/reference/photo/likes/) | People who like this |
| [`picture`](https://developers.facebook.com/docs/graph-api/reference/photo/picture/) | Link to the 100px wide representation of this photo |
| [`sponsor_tags`](https://developers.facebook.com/docs/graph-api/reference/photo/sponsor_tags/) | Sponsor pages tagged in the photo. |