platform: Facebook
topic: Graph-API
subtopic: Group Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/group/members

### Permissions

A User access token of an Admin of the Group with the following permissions:

* `user_managed_groups`
    

### Fields

A list of [User objects](https://developers.facebook.com/docs/graph-api/reference/user/) and one additional field:

| Name | Description | Type |
| --- | --- | --- |
| `administrator` | Whether or not the person is a group admin. | `boolean` |

## Creating

This operation is not supported.

## Updating

This operation is not supported.

## Deleting

This operation is not supported.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)