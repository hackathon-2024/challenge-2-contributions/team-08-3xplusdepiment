platform: Facebook
topic: Graph-API
subtopic: Permissions Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Permissions Reference.md
url: https://developers.facebook.com/docs/permissions

## Learn More

* [Access Tokens](https://developers.facebook.com/docs/facebook-login/guides/access-tokens)
* [Maintaining Data Access](https://developers.facebook.com/docs/development/maintaining-data-access)
* [Secure Requests](https://developers.facebook.com/docs/graph-api/guides/secure-requests)
* [Terms and Policies](https://developers.facebook.com/docs/development/terms-and-policies)

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)