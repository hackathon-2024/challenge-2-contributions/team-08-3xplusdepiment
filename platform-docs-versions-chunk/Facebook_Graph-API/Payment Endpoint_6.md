platform: Facebook
topic: Graph-API
subtopic: Payment Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Payment Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/payment/dispute

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/payment/dispute)

# [`/{payment-id}`](https://developers.facebook.com/docs/graph-api/reference/payment/)`/dispute`

Used to settle any payment disputes.

## Reading

You can't read this edge, use the `disputes` field on the parent Payment object.