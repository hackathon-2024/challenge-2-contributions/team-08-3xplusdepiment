platform: Facebook
topic: Graph-API
subtopic: Milestone Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Milestone Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/milestone

### Permissions

To view publicly shared milestones of any Page, you will need:

* A valid app or user access token
    
* The [Public Page Content Access feature](https://developers.facebook.com/docs/pages/overview-1#features)
    

To view milestones of Pages you manage, you will need:

* A Page access token
    
* The [`pages_read_engagement` permission](https://developers.facebook.com/permissions)