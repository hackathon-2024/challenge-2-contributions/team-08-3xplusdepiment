platform: Facebook
topic: Graph-API
subtopic: Group Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/group/albums

### Requirements

| Type of Requirement | Description |
| --- | --- |
| [App Review](https://developers.facebook.com/docs/apps/review) | Your app must be approved for the following login permissions and features: (Click to expand) |
| Login permissions | `publish_to_groups` |
| Features | [Groups API](https://developers.facebook.com/docs/groups-api/) |
| [App Installation](https://developers.facebook.com/docs/groups-api#app-installation) | The app must be installed on the Group. |
| [Tokens](https://developers.facebook.com/docs/facebook-login/access-tokens) | A User access token for a user who is a member of the Group where the app is installed. |
| [Permissions](https://developers.facebook.com/docs/facebook-login/permissions/) | The user must grant your app this permission:<br><br>`publish_to_groups` |