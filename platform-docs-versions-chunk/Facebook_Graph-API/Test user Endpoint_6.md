platform: Facebook
topic: Graph-API
subtopic: Test user Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Test user Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/test-user/friends

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/test-user/friends)

# [`/{test-user-id}`](https://developers.facebook.com/docs/graph-api/reference/test-user)`/friends`

The friends of a test user. This is [identical to the `/{user-id}/friends` edge](https://developers.facebook.com/docs/graph-api/reference/user/friends/) aside from the [publishing](#publishing) operation explained below.

### Related Guides

* [Managing Test Accounts using the App Dashboard](https://developers.facebook.com/docs/apps/test-users#managetool)
    

## Publishing

You can't perform this operation on this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)