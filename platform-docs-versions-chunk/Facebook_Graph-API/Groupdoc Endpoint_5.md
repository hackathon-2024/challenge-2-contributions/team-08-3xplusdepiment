platform: Facebook
topic: Graph-API
subtopic: Groupdoc Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Groupdoc Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/groupdoc

## Publishing

You cannot create docs via the Graph API.

## Deleting

You cannot delete docs via the Graph API.

## Updating

You cannot update docs via the Graph API.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)