platform: Facebook
topic: Graph-API
subtopic: Comment Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Comment Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/comment

## Publishing

You can publish comments by using the [`/comments`](https://developers.facebook.com/docs/graph-api/reference/object/comments/) edge when it is present on a node.