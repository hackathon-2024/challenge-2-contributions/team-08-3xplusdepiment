platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/


### Edges

| Edge | Description |
| --- | --- |
| [`conversation_analytics`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/conversation_analytics/) | Analytics data of the WhatsApp Business Account with conversation based pricing |
| [`dcc_config`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/dcc_config/) | Returns a list of DCC configs |
| [`message_template_previews`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/message_template_previews/) | Retrieves a preview of a message template based on the provided configuration |
| [`message_templates`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/message_templates/) | Message templates that belong to the WhatsApp Business Account |
| [`phone_numbers`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/phone_numbers/) | The phone numbers that belong to the WhatsApp Business Account |
| [`product_catalogs`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/product_catalogs/) | product\_catalogs |
| [`subscribed_apps`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/subscribed_apps/) | List of apps that are subscribed to webhooks updates for this WABA |
| [`template_performance_metrics`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/template_performance_metrics/) | template\_performance\_metrics |