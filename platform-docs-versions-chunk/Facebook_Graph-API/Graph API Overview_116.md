platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/changelog


## Available Graph API Versions

| Version | Date Introduced | Available Until |
| --- | --- | --- |
| [v19.0](https://developers.facebook.com/docs/graph-api/changelog/version19.0#graph-api) | January 23, 2023 | TBD |
| [v18.0](https://developers.facebook.com/docs/graph-api/changelog/version18.0#graph-api) | September 12, 2023 | TBD |
| [v17.0](https://developers.facebook.com/docs/graph-api/changelog/version17.0#graph-api) | May 23, 2023 | TBD |
| [v16.0](https://developers.facebook.com/docs/graph-api/changelog/version16.0#graph-api) | February 2, 2023 | TBD |
| [v15.0](https://developers.facebook.com/docs/graph-api/changelog/version15.0#graph-api) | September 15, 2022 | TBD |
| [v14.0](https://developers.facebook.com/docs/graph-api/changelog/version14.0#graph-api) | May 25, 2022 | September 17, 2024 |
| [v13.0](https://developers.facebook.com/docs/graph-api/changelog/version13.0#graph-api) | February 8, 2022 | May 28, 2024 |
| [v12.0](https://developers.facebook.com/docs/graph-api/changelog/version12.0#graph-api) | September 14, 2021 | February 8, 2024 |