platform: Facebook
topic: Graph-API
subtopic: Object Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Object Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/object/reactions

### Requirements

**Marketing Apps**

* `ads_management`
* `pages_read_engagement`
* `pages_show_list`

**Page Management Apps**

* `pages_show_list`