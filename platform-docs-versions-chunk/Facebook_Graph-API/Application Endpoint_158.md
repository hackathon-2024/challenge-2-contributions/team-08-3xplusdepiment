platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app/translations

### Permissions

* An app access token is required to add new translation strings for that app.
    

### Fields

| Name | Description | Type |
| --- | --- | --- |
| Vector | Vector | Vector |

#### Response

If successful, you will receive a plain response of the number of strings that were added, otherwise an error message.