platform: Facebook
topic: Graph-API
subtopic: Video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video/video_insights/

### Limitations

* Insights for Videos on [Users](https://developers.facebook.com/docs/graph-api/reference/user) or [Groups](https://developers.facebook.com/docs/graph-api/reference/group) are not available.
* Data is only available for the past 2 years.
* A crossposted Video will have a unique video ID for each Page it was posted to.