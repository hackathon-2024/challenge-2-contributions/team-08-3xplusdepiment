platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/overview

## Facebook APIs, SDKs, and Platforms

Connect interfaces and develop across platforms using Facebook's various [APIs, SDKs, and platforms](https://developers.facebook.com/docs#apis-and-sdks).

## Next Steps

[**Get Started with Graph API**](https://developers.facebook.com/docs/graph-api/get-started) – Let's explore the Facebook Social Graph using the Graph Explorer tool and run a couple requests to get data.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)