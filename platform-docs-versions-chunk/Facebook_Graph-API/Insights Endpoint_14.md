platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights

### Page Reactions

The "like" reaction counts include both "like" and "care" reactions.

| Metric Name | Description | Values for \`period\` |
| --- | --- | --- |
| `page_actions_post_reactions_like_total*` | Daily total post "like" reactions of a page. | day, week, days\_28 |
| `page_actions_post_reactions_love_total*` | Daily total post "love" reactions of a page. | day, week, days\_28 |
| `page_actions_post_reactions_wow_total*` | Daily total post "wow" reactions of a page. | day, week, days\_28 |
| `page_actions_post_reactions_haha_total*` | Daily total post "haha" reactions of a page. | day, week, days\_28 |
| `page_actions_post_reactions_sorry_total*` | Daily total post "sorry" reactions of a page. | day, week, days\_28 |
| `page_actions_post_reactions_anger_total*` | Daily total post "anger" reactions of a page. | day, week, days\_28 |
| `page_actions_post_reactions_total` | Daily total post reactions of a page by type. | day |