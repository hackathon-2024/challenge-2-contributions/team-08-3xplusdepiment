platform: Facebook
topic: Graph-API
subtopic: Page Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page/


### Edges

| Edge | Description |
| --- | --- |
| [`ab_tests`](https://developers.facebook.com/docs/graph-api/reference/page/ab_tests/) | ab\_tests |
| [`ads_posts`](https://developers.facebook.com/docs/graph-api/reference/page/ads_posts/) | The ad posts for this Page |
| [`agencies`](https://developers.facebook.com/docs/graph-api/reference/page/agencies/) | Businesses that have agency permissions on the Page |
| [`albums`](https://developers.facebook.com/docs/graph-api/reference/page/albums/) | Photo albums for this Page. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`ar_experience`](https://developers.facebook.com/docs/graph-api/reference/page/ar_experience/) | Page that owns a container |
| [`assigned_users`](https://developers.facebook.com/docs/graph-api/reference/page/assigned_users/) | Users assigned to this Page. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`audio_media_copyrights`](https://developers.facebook.com/docs/graph-api/reference/page/audio_media_copyrights/) | The music copyrights owned by this page (using alacorn) |
| [`blocked`](https://developers.facebook.com/docs/graph-api/reference/page/blocked/) | User or Page Profiles blocked from this Page |
| [`businessprojects`](https://developers.facebook.com/docs/graph-api/reference/page/businessprojects/) | Business projects |
| [`call_to_actions`](https://developers.facebook.com/docs/graph-api/reference/page/call_to_actions/)[](#) | The call-to-action created by this Page. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`canvas_elements`](https://developers.facebook.com/docs/graph-api/reference/page/canvas_elements/)[](#) | The canvas elements associated with this page |
| [`canvases`](https://developers.facebook.com/docs/graph-api/reference/page/canvases/)[](#) | The canvas documents associated with this page |
| [`chat_plugin`](https://developers.facebook.com/docs/graph-api/reference/page/chat_plugin/) | customization configuration values of the Page's corresponding Chat Plugin |
| [`commerce_orders`](https://developers.facebook.com/docs/graph-api/reference/page/commerce_orders/) | The commerce orders of this Page |
| [`conversations`](https://developers.facebook.com/docs/graph-api/reference/page/conversations/) | This Page's conversations |
| [`crosspost_whitelisted_pages`](https://developers.facebook.com/docs/graph-api/reference/page/crosspost_whitelisted_pages/)[](#) | Pages that are allowed to crosspost |
| [`custom_labels`](https://developers.facebook.com/docs/graph-api/reference/page/custom_labels/)[](#) | custom\_labels |
| [`custom_user_settings`](https://developers.facebook.com/docs/graph-api/reference/page/custom_user_settings/) | Custom user settings for a page |
| [`fantasy_games`](https://developers.facebook.com/docs/graph-api/reference/page/fantasy_games/) | Fantasy Games sponsored by this page |
| [`feed`](https://developers.facebook.com/docs/graph-api/reference/page/feed/) | This Page's feed. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`global_brand_children`](https://developers.facebook.com/docs/graph-api/reference/page/global_brand_children/) | Children Pages of a Global Pages root Page. Both default and root Page can return children Pages. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`groups`](https://developers.facebook.com/docs/graph-api/reference/page/groups/) | groups |
| [`image_copyrights`](https://developers.facebook.com/docs/graph-api/reference/page/image_copyrights/) | Image copyrights from this page |
| [`insights`](https://developers.facebook.com/docs/graph-api/reference/page/insights/) | This Page's Insights data |
| [`instagram_accounts`](https://developers.facebook.com/docs/graph-api/reference/page/instagram_accounts/) | Linked Instagram accounts for this Page |
| [`leadgen_forms`](https://developers.facebook.com/docs/graph-api/reference/page/leadgen_forms/) | A library of lead generation forms created for this page. |
| [`likes`](https://developers.facebook.com/docs/graph-api/reference/page/likes/) | The Pages that this Page has liked. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). For [New Page Experience](https://developers.facebook.com/docs/pages/reference) Pages, this field will return `followers_count`.<br><br>[Core](https://developers.facebook.com/docs/apps/versions/#coreextended) |
| [`live_videos`](https://developers.facebook.com/docs/graph-api/reference/page/live_videos/) | Live videos on this Page. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`locations`](https://developers.facebook.com/docs/graph-api/reference/page/locations/) | The location Pages that are children of this Page. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). To manage a child Page's location use the [`/{page-id}/locations` endpoint](https://developers.facebook.com/docs/graph-api/reference/page/locations#updatepost). |
| [`media_fingerprints`](https://developers.facebook.com/docs/graph-api/reference/page/media_fingerprints/)[](#) | Media fingerprints from this page |
| [`messaging_feature_review`](https://developers.facebook.com/docs/graph-api/reference/page/messaging_feature_review/) | Feature status of the page that has been granted through feature review that show up in the page settings |
| [`messenger_lead_forms`](https://developers.facebook.com/docs/graph-api/reference/page/messenger_lead_forms/) | messenger\_lead\_forms |
| [`messenger_profile`](https://developers.facebook.com/docs/graph-api/reference/page/messenger_profile/) | SELF\_EXPLANATORY |
| [`page_backed_instagram_accounts`](https://developers.facebook.com/docs/graph-api/reference/page/page_backed_instagram_accounts/) | Gets the [Page Backed Instagram Account](https://developers.facebook.com/docs/marketing-api/guides/instagramads/#pbia) (an [InstagramUser](https://developers.facebook.com/docs/graph-api/reference/instagram-user/)) associated with this Page. |
| [`personas`](https://developers.facebook.com/docs/graph-api/reference/page/personas/) | Messenger Platform Bot personas for the Page |
| [`photos`](https://developers.facebook.com/docs/graph-api/reference/page/photos/) | This Page's Photos. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`picture`](https://developers.facebook.com/docs/graph-api/reference/page/picture/) | This Page's profile picture<br><br>[Core](https://developers.facebook.com/docs/apps/versions/#coreextended) |
| [`posts`](https://developers.facebook.com/docs/graph-api/reference/page/feed/) | This Page's own Posts, a derivative of the `/feed` edge. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`product_catalogs`](https://developers.facebook.com/docs/graph-api/reference/page/product_catalogs/) | Product catalogs owned by this page |
| [`published_posts`](https://developers.facebook.com/docs/graph-api/reference/page/published_posts/) | All published posts by this page |
| [`ratings`](https://developers.facebook.com/docs/graph-api/reference/page/ratings/) | Open Graph ratings given to this Page |
| [`roles`](https://developers.facebook.com/docs/graph-api/reference/page/roles/)[](#) | The Page's Admins |
| [`scheduled_posts`](https://developers.facebook.com/docs/graph-api/reference/page/scheduled_posts/) | All posts that are scheduled to a future date by a page |
| [`secondary_receivers`](https://developers.facebook.com/docs/graph-api/reference/page/secondary_receivers/) | Secondary Receivers for a page |
| [`settings`](https://developers.facebook.com/docs/graph-api/reference/page/settings/) | Controllable settings for this page |
| [`shop_setup_status`](https://developers.facebook.com/docs/graph-api/reference/page/shop_setup_status/) | Shows the shop setup status |
| [`subscribed_apps`](https://developers.facebook.com/docs/graph-api/reference/page/subscribed_apps/) | Applications that have real time update subscriptions for this Page. Note that we will only return information about the current app |
| [`tabs`](https://developers.facebook.com/docs/graph-api/reference/page/tabs/) | This Page's tabs and the apps in them. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`tagged`](https://developers.facebook.com/docs/graph-api/reference/page/tagged/) | The Photos, Videos, and Posts in which the Page has been tagged. A derivative of `/feeds`. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`thread_owner`](https://developers.facebook.com/docs/graph-api/reference/page/thread_owner/) | App which owns a thread for Handover Protocol |
| [`threads`](https://developers.facebook.com/docs/graph-api/reference/thread/) | Deprecated. Use conversations instead |
| [`video_copyright_rules`](https://developers.facebook.com/docs/graph-api/reference/page/video_copyright_rules/) | Video copyright rules from this page |
| [`video_lists`](https://developers.facebook.com/docs/graph-api/reference/page/video_lists/)[](#) | Video Playlists for this Page |
| [`videos`](https://developers.facebook.com/docs/graph-api/reference/page/videos/) | Videos for this Page. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |
| [`visitor_posts`](https://developers.facebook.com/docs/graph-api/reference/page/visitor_posts/) | Shows all public Posts published by Page visitors on the Page. Can be read with [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature/#reference-PAGES_ACCESS). |