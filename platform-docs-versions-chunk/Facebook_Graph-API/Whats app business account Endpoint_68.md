platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/upsert_message_templates/

# Whats App Business Account Upsert Message Templates

## Reading

You can't perform this operation on this endpoint.

## Creating

You can make a POST request to `upsert_message_templates` edge from the following paths:

* [`/{whats_app_business_account_id}/upsert_message_templates`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/upsert_message_templates/)

When posting to this edge, no Graph object will be created.