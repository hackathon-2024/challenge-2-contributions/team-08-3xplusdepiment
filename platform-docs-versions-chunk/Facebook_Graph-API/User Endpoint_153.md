platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/videos/

### Return Type

Struct {

`id`: numeric string,

`upload_session_id`: numeric string,

`video_id`: numeric string,

`start_offset`: numeric string,

`end_offset`: numeric string,

`success`: bool,

`skip_upload`: bool,

`upload_domain`: string,

`region_hint`: string,

`xpv_asset_id`: numeric string,

`is_xpv_single_prod`: bool,

`transcode_bit_rate_bps`: numeric string,

`transcode_dimension`: numeric string,

`should_expand_to_transcode_dimension`: bool,

`action_id`: string,

`gop_size_seconds`: numeric string,

`target_video_codec`: string,

`target_hdr`: string,

`maximum_frame_rate`: numeric string,

}