platform: Facebook
topic: Graph-API
subtopic: Media fingerprint Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Media fingerprint Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/media-fingerprint/

# Media Fingerprint

## Reading

Media fingerprint

### New Page Experience

This endpoint is supported for [New Page Experience](https://developers.facebook.com/docs/pages/new-pages-experience/).