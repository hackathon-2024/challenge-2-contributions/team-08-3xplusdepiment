platform: Facebook
topic: Graph-API
subtopic: Object Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Object Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/object/sharedposts

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/object/sharedposts)

# `/{object-id}/sharedposts`

This reference describes the `/sharedposts` edge that is common to multiple Graph API nodes. The structure and operations are the same for each node. The following objects have a `sharedposts` edge:

* PagePost
    
* Post
    
* User