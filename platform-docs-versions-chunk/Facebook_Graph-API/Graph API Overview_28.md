platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/results

## Next Steps

Now that you are more familiar with the Graph API visit our [Graph Explorer Tool Guide](https://developers.facebook.com/docs/graph-api/explorer) to explore the Graph without writing code, [Common Uses](https://developers.facebook.com/docs/graph-api/using-graph-api/common-scenarios) to view the most common tasks performed, and [the SDKs available](https://developers.facebook.com/docs/graph-api/using-graph-api/using-with-sdks).

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)