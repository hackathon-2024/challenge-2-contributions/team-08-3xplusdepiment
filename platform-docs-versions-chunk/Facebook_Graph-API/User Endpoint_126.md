platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/picture/

# User Picture

A User's profile image.

Querying a User ID (UID) now requires an access token. Refer to the [requirements](#requirements) table to determine which token type to include in UID-based requests.

## Reading

Get the app user's profile image.