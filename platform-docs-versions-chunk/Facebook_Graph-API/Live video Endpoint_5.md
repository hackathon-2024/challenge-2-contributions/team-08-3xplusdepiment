platform: Facebook
topic: Graph-API
subtopic: Live video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Live video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/live-video/


### Edges

| Edge | Description |
| --- | --- |
| [`blocked_users`](https://developers.facebook.com/docs/graph-api/reference/live-video/blocked_users/) | The users who are blocked from commenting on the live video |
| [`comments`](https://developers.facebook.com/docs/graph-api/reference/live-video/comments/) | Comments made on this |
| [`crosspost_shared_pages`](https://developers.facebook.com/docs/graph-api/reference/live-video/crosspost_shared_pages/) | Pages which are allowed to crosspost this live video. This field is only available on the original live video. |
| [`crossposted_broadcasts`](https://developers.facebook.com/docs/graph-api/reference/live-video/crossposted_broadcasts/) | Live videos which have been crossposted from this live video. This field is only available on the original live video. |
| [`errors`](https://developers.facebook.com/docs/graph-api/reference/live-video/errors/) | Errors that occurred during the live stream |
| [`polls`](https://developers.facebook.com/docs/graph-api/reference/live-video/polls/) | Polls configured for this broadcast |
| [`reactions`](https://developers.facebook.com/docs/graph-api/reference/live-video/reactions/)[](#) | People who reacted on this |