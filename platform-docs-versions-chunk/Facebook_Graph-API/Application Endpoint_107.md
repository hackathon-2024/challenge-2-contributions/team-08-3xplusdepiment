platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/page_activities/

# Application Page Activities

## Reading

You can't perform this operation on this endpoint.

## Creating

You can make a POST request to `page_activities` edge from the following paths:

* [`/{application_id}/page_activities`](https://developers.facebook.com/docs/graph-api/reference/application/page_activities/)

When posting to this edge, an [Application](https://developers.facebook.com/docs/graph-api/reference/application/) will be created.