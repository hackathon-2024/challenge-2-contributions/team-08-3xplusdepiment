platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/businesses/


### Parameters

| Parameter | Description |
| --- | --- |
| `child_business_external_id`<br><br>string | child\_business\_external\_id |
| `email`<br><br>string | The business email of the business admin |
| `name`<br><br>string | Username<br><br>Required |
| `primary_page`<br><br>page ID | Primary Page ID |
| `sales_rep_email`<br><br>string | Sales Rep email address |
| `survey_business_type`<br><br>enum {AGENCY, ADVERTISER, APP\_DEVELOPER, PUBLISHER} | Business Type |
| `survey_num_assets`<br><br>int64 | Number of Assets in the business |
| `survey_num_people`<br><br>int64 | Number of People that will work on the business |
| `timezone_id`<br><br>int64 | Timezone ID |
| `vertical`<br><br>enum {NOT\_SET, ADVERTISING, AUTOMOTIVE, CONSUMER\_PACKAGED\_GOODS, ECOMMERCE, EDUCATION, ENERGY\_AND\_UTILITIES, ENTERTAINMENT\_AND\_MEDIA, FINANCIAL\_SERVICES, GAMING, GOVERNMENT\_AND\_POLITICS, MARKETING, ORGANIZATIONS\_AND\_ASSOCIATIONS, PROFESSIONAL\_SERVICES, RETAIL, TECHNOLOGY, TELECOM, TRAVEL, NON\_PROFIT, RESTAURANT, HEALTH, LUXURY, OTHER} | Vertical ID<br><br>Required |