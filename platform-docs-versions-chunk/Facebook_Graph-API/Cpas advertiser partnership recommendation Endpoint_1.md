platform: Facebook
topic: Graph-API
subtopic: Cpas advertiser partnership recommendation Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Cpas advertiser partnership recommendation Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/cpas-advertiser-partnership-recommendation/

# CPASAdvertiser Partnership Recommendation

## Reading

Returns a recommendation of a single retailer for a specific brand. This endpoint returns a retailer-brand pair and an advertiser who can advertise on behalf of the producer.

This endpoint is mainly for Facebook’s Marketing Partners using [Collaborative Ads](https://developers.facebook.com/docs/marketing-api/collaborative-ads).