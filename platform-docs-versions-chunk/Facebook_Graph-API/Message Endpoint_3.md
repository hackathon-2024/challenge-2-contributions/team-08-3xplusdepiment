platform: Facebook
topic: Graph-API
subtopic: Message Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Message Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/message

### Requirements

| Type | Description |
| --- | --- |
| [App Review](https://developers.facebook.com/docs/apps/review) | Required to access data for people who do not have a role on your app, such as a developer, tester or admin |
| Features | Not applicable |
| Tokens | A [Page access token](https://developers.facebook.com/docs/facebook-login/access-tokens/#pagetokens) from a person who can perform the [`MODERATE` or `MESSAGING` task](https://developers.facebook.com/docs/pages/overview-1#tasks) on the Page being queried or linked to the Instagram Professional account. |
| Permissions | The [`pages_messaging` and `pages_manage_metadata` permissions](https://developers.facebook.com/docs/permissions/reference)<br><br>The [`instagram_basic` and `instagram_manage_messaging` permissions](https://developers.facebook.com/docs/permissions/reference) are also required for Instagram Messaging |