platform: Facebook
topic: Graph-API
subtopic: Live video input stream Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Live video input stream Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/live-video-input-stream/

### Parameters

| Parameter | Description |
| --- | --- |
| `target_token`<br><br>string | A target token recently returned by the speed test API |