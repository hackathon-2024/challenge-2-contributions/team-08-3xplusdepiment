platform: Facebook
topic: Graph-API
subtopic: Media fingerprint Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Media fingerprint Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/media-fingerprint/

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can update a [MediaFingerprint](https://developers.facebook.com/docs/graph-api/reference/media-fingerprint/) by making a POST request to [`/{media_fingerprint_id}`](https://developers.facebook.com/docs/graph-api/reference/media-fingerprint/).