platform: Facebook
topic: Graph-API
subtopic: Video poll Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video poll Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video-poll/

# Video Poll

## Reading

Embedded video poll

### Feature Permissions

| Name | Description |
| --- | --- |
| Live video API | This [feature permission](https://developers.facebook.com/docs/videos/live-video-api/) may be required. |