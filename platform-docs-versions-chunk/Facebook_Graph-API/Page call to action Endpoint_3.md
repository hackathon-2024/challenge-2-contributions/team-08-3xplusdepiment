platform: Facebook
topic: Graph-API
subtopic: Page call to action Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page call to action Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page-call-to-action/

### Requirements

| Type | Description |
| --- | --- |
| [App Review](https://developers.facebook.com/docs/apps/review) | Your app must be be approved for the following login permissions and features. |
| [Login permissions](https://developers.facebook.com/docs/facebook-login/permissions) | None |
| [Features](https://developers.facebook.com/docs/apps/review/feature) | [Page Public Content Access](https://developers.facebook.com/docs/apps/review/feature#reference-PAGES_ACCESS) |
| [Tokens](https://developers.facebook.com/docs/facebook-login/access-tokens) | A User access token for a User who has a role on the Page |
| [Permissions](https://developers.facebook.com/docs/facebook-login/permissions) | None |

### Parameters

This endpoint doesn't have any parameters.