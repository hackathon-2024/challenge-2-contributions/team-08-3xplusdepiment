platform: Facebook
topic: Graph-API
subtopic: Group Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/group/photos

# Group Photos

## Reading

You can't perform this operation on this endpoint.

## Creating

Animated photos are not supported, and a photo must be less than 10MB in size.

You can make a POST request to `photos` edge from the following paths:

* [`/{group_id}/photos`](https://developers.facebook.com/docs/graph-api/reference/group/photos/)

When posting to this edge, a [Photo](https://developers.facebook.com/docs/graph-api/reference/photo/) will be created.