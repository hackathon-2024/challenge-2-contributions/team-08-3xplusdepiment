platform: Facebook
topic: Graph-API
subtopic: Group Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/group


## Edges

| Name | Description |
| --- | --- |
| [`/admins`](https://developers.facebook.com/docs/graph-api/reference/group/admins) | _This edge was deprecated on April 4th, 2018._ |
| [`/albums`](https://developers.facebook.com/docs/graph-api/reference/group/albums) | Photo albums owned by the Group. |
| [`/docs`](https://developers.facebook.com/docs/graph-api/reference/group/docs) | Documents owned by the Group. |
| [`/events`](https://developers.facebook.com/docs/graph-api/reference/group/events) | Events owned by the Group. |
| [`/feed`](https://developers.facebook.com/docs/graph-api/reference/group/feed) | Feed of Posts owned by the Group. |
| [`/files`](https://developers.facebook.com/docs/graph-api/reference/group/files) | Files owned by the Group. |
| [`/live_videos`](https://developers.facebook.com/docs/graph-api/reference/live-video/) | Live Videos owned by the Group. |
| [`/members`](https://developers.facebook.com/docs/graph-api/reference/group/members/) | _This edge was deprecated on April 4th, 2018._ |
| [`/photos`](https://developers.facebook.com/docs/graph-api/reference/group/photos) | Photos owned by the Group. |
| [`/videos`](https://developers.facebook.com/docs/graph-api/reference/group/videos) | Videos owned by the Group. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)