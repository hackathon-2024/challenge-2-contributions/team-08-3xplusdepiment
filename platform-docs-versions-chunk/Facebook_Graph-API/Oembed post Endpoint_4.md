platform: Facebook
topic: Graph-API
subtopic: Oembed post Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Oembed post Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/oembed-post/


### Fields

| Field | Description |
| --- | --- |
| `author_name`<br><br>string | The name of the Facebook user that owns the post.<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `author_url`<br><br>string | A URL for the author/owner of the post.<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `height`<br><br>int32 | The height in pixels required to display the HTML.<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `html`<br><br>string | The HTML used to display the post.<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `provider_name`<br><br>string | Name of the provider (Facebook).<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `provider_url`<br><br>string | URL of the provider (Facebook).<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `type`<br><br>string | The oEmbed resource type. See https://oembed.com/.<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `version`<br><br>string | Always 1.0. See https://oembed.com/<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `width`<br><br>int32 | The width in pixels required to display the HTML.<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |