platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/get-started

# Get Started

This guide explains how to get started with receiving data from the Facebook Social Graph.

## Before You Start

You will need:

* [Register as a Meta Developer](https://developers.facebook.com/docs/development/register)
    
* A [Meta App](https://developers.facebook.com/docs/development/create-an-app) – This app will be for testing so there is no need to involve your app code when creating this Meta App.
    
* The [Graph API Explorer tool](https://developers.facebook.com/tools/explorer) open in a separate browser window
    
* A brief understanding of the structure of the Meta's social graph from our [Graph API Overview](https://developers.facebook.com/docs/graph-api/overview#nodes) guide
    

## Your First Request