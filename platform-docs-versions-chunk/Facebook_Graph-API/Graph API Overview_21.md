platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/overview/access-levels

## Changing Access Levels

App administrators can change access levels for individual permissions and features. Restoring Advanced Access to permissions and features does not require [re-review](https://developers.facebook.com/docs/app-review), but changing from Advanced to Standard will invalidate/deactivate any permission/feature for any app users who do not have a role on your app.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)