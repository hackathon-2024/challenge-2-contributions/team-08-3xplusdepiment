platform: Facebook
topic: Graph-API
subtopic: Event Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Event Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/event/photos

### Response

If successful:

| Name | Description | Type |
| --- | --- | --- |
| `id` | The newly created photo ID | `string` |

## Deleting

You can't delete using this edge, however you can [delete each photo using the /{photo-id} node](https://developers.facebook.com/docs/reference/api/photo/).

## Updating

You can't update using this edge.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)