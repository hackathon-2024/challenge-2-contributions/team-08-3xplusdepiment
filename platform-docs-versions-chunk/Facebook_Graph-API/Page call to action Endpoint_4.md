platform: Facebook
topic: Graph-API
subtopic: Page call to action Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page call to action Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page-call-to-action/


### Fields

| Field | Description |
| --- | --- |
| `id`<br><br>numeric string | ID of the call-to-action<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `android_app`<br><br>[Application](https://developers.facebook.com/docs/graph-api/reference/application/) | App that stores the destination info on Android |
| `android_deeplink`<br><br>string | Destination deeplink for the call-to-action on Android |
| `android_destination_type`<br><br>enum | Destination type for the call-to-action on Android |
| `android_package_name`<br><br>string | Destination app for the call-to-action on Android |
| `android_url`<br><br>string | Destination url for the call-to-action on Android |
| `created_time`<br><br>datetime | Time when the call-to-action was created |
| `email_address`<br><br>string | Email address that can be contacted by a user |
| `from`<br><br>[Page](https://developers.facebook.com/docs/graph-api/reference/page/) | Page that owns the call-to-action |
| `intl_number_with_plus`<br><br>string | International phone number with plus that can be called by a phone |
| `iphone_app`<br><br>[Application](https://developers.facebook.com/docs/graph-api/reference/application/) | App that stores the destination info on iPhone |
| `iphone_deeplink`<br><br>string | Destination deeplink for the call-to-action on iPhone |
| `iphone_destination_type`<br><br>enum | Destination type for the call-to-action on iPhone |
| `iphone_url`<br><br>string | Destination url for the call-to-action on iPhone |
| `status`<br><br>enum | Current running status of this action<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `type`<br><br>enum | The type of action<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `updated_time`<br><br>datetime | Time when the call-to-action was last updated |
| `web_destination_type`<br><br>enum | Destination type for the call-to-action on desktop |
| `web_url`<br><br>string | Destination url for the call-to-action on desktop |