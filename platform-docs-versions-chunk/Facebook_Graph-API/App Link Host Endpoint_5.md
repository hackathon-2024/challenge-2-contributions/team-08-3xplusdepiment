platform: Facebook
topic: Graph-API
subtopic: App Link Host Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/App Link Host Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app-link-host

### Permissions

* An app access token is required to delete app link hosts belonging to that app.
    

### Response

Returns `true` successful, otherwise an error message.