platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/phone_numbers/

# Whats App Business Account Phone Numbers

Represents a collection of business phone numbers associated with a WhatsApp Business Account (WABA).

To find the ID of a WhatsApp Business Account, go to [**Business Manager**](https://business.facebook.com/) > **Business Settings** > **Accounts** > **WhatsApp Business Accounts**. Find the account you want to use and click on it. A panel opens, with information about the account, including the ID.

  

For more information on how to use the API, see [WhatsApp Business Management API](https://developers.facebook.com/docs/whatsapp/business-account-management-api).

## Reading

Get a list of phone numbers associated with a WhatsApp Business Account.