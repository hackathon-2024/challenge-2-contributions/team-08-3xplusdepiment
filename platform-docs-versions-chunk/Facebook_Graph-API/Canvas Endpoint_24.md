platform: Facebook
topic: Graph-API
subtopic: Canvas Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Canvas Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/canvas-product-list/

# Canvas Product List

**Only E-commerce and travel hotel vertical catalogs are currently supported.**

## Reading

A product list inside the canvas

  

Select a product catalog and then manually provide the product ID, name and color variations to promote in a collection's ad creative. Use this option if you or your advertiser does not want to set-up a product set from a catalog feed. This option makes creating of ads from product catalog simpler. **Note that we do not save selected products as a product set for later reuse.**

Use this option to select which item colors you want to show in an ad and control the order products appear. **Because this is a manual ordering, we do not dynamically rank or display products based on popularity or relevancy to each viewer.**