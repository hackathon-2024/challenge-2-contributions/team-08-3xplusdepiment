platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/accounts/

# Application Accounts

Represents a collection of [test users](https://developers.facebook.com/docs/development/build-and-test/test-users) on an app.

## Reading

Get a list of [test users](https://developers.facebook.com/docs/development/build-and-test/test-users) on an app.

### Requirements

| Type | Requirement |
| --- | --- |
| [Access Tokens](https://developers.facebook.com/docs/facebook-login/access-tokens/) | [App](https://developers.facebook.com/docs/facebook-login/access-tokens/#apptokens) |
| [Permissions](https://developers.facebook.com/docs/permissions/reference) | None |