platform: Facebook
topic: Graph-API
subtopic: Post Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Post Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/post/

### Limitations

The following fields for `/page/feed`, `/page/posts`, `/pageposts`, and `/page/published_posts` were deprecated in v3.3:

* `caption`
* `description`
* `link`
* `name`
* `object_id`
* `source`
* `type`