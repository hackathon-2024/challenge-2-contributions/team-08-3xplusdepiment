platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/guides/explorer

# Graph API Explorer Guide

|     |     |
| --- | --- |
| The Graph API Explorer tool allows you to construct and perform Graph API queries and see their responses for any apps on which you have an admin, developer, or tester role. | [Open the Graph API Explorer tool](https://developers.facebook.com/tools/explorer) |

## Common Uses

* Quickly generate access tokens
* Get code samples for your queries
* Generate debug information to include in support requests
* Test API queries with your production app's settings including permissions, features, and settings for your use cases
* Test API queries with your test or development app using permission and features on test users or test data

## Requirements

* A [Facebook Developer Account](https://developers.facebook.com/apps)
    
* An app for which you have a role, such as an [admin, developer, or tester role](https://developers.facebook.com/docs/apps#roles)
    

## Components