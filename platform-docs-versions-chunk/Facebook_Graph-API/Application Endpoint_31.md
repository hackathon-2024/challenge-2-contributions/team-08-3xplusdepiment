platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/activities/

# Application Activities

Application activities are events from your app.

## Reading

You can't perform this operation on this endpoint.

## Creating

You can use a user access token or app access token to log events to this endpoint.

You can make a POST request to `activities` edge from the following paths:

* [`/{application_id}/activities`](https://developers.facebook.com/docs/graph-api/reference/application/activities/)

When posting to this edge, no Graph object will be created.