platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/app_installed_groups/

### Requirements

| Type of Requirement | Description |
| --- | --- |
| [App Review](https://developers.facebook.com/docs/apps/review) | Your app must be approved for the [Groups API](https://developers.facebook.com/docs/groups-api/) feature. |
| [App Installation](https://developers.facebook.com/docs/groups-api#app-installation) | The app must be installed on the Group. |
| [Tokens](https://developers.facebook.com/docs/facebook-login/access-tokens) | An AppToken access token. See [App Access Tokens](https://developers.facebook.com/docs/facebook-login/access-tokens/#apptokens). |

### Parameters

| Parameter | Description |
| --- | --- |
| `group_id`<br><br>group\_id ID | Id of the group to check if app is installed in it or not |