platform: Facebook
topic: Graph-API
subtopic: Url Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Url Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/url

### Requirements

| Type | Description |
| --- | --- |
| [Access Tokens](#) | Any access token can be used to make this request. |
| [Features](#) | Not applicable. |
| [Page Tasks](#) | Not applicable. |
| [Permissions](#) | Not applicable. |