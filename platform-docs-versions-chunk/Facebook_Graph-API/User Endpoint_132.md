platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/user/posts

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/user/posts)

# User Posts

Represents a collection of [Posts](https://developers.facebook.com/docs/graph-api/reference/post) on a [User](https://developers.facebook.com/docs/graph-api/reference/user).