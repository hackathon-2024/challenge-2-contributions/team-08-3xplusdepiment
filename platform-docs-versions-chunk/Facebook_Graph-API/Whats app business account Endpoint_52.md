platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/phone_numbers/

### Sample Response 2

{   
   "id" : "106853218861309", 
   "is\_official\_business\_account" : true,
   "display\_phone\_number" : "+1 555-555-5555",      
   "verified\_name" : "Jaspers Market"
}

### Parameters

This endpoint doesn't have any parameters.