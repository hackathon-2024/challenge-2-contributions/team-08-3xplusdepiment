platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app/translations

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/app/translations)

# [`/{app-id}`](https://developers.facebook.com/docs/reference/api/application/)`/translations`

The strings from this app that were translated using our [translations tools](https://developers.facebook.com/docs/internationalization).