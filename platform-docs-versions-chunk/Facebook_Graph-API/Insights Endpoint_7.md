platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights


### Page CTA Clicks

| Metric Name | Description | Values for \`period\` |
| --- | --- | --- |
| `page_total_actions` | The number of clicks on your Page's contact info and call-to-action button. | day, week, days\_28 |
| `page_cta_clicks_logged_in_total` | Total number of clicks on the Page CTA button by people who are logged in to Facebook. | day, week, days\_28 |
| `page_cta_clicks_logged_in_unique` | Unique number of clicks on the Page CTA button by people who are logged in to Facebook. | day, week, days\_28 |
| `page_cta_clicks_by_site_logged_in_unique` | Number of people who are logged in to Facebook and clicked on the CTA button, broken down by www, mobile, api or other. | day, week, days\_28 |
| `page_cta_clicks_by_age_gender_logged_in_unique` | Number of people who are logged in to Facebook and clicked the Page CTA button, broken down by age and gender group. | day, week, days\_28 |
| `page_cta_clicks_logged_in_by_country_unique` | Number of people who are logged in to Facebook and clicked the Page CTA button, broken down by country. | day, week |
| `page_cta_clicks_logged_in_by_city_unique` | Number of people who are logged in to Facebook and clicked the Page CTA button, broken down by city. | day, week |
| `page_call_phone_clicks_logged_in_unique` | Number of people who logged into Facebook and clicked the Call Now button. | day, week, days\_28 |
| `page_call_phone_clicks_by_age_gender_logged_in_unique` | Number of people who logged in to Facebook and clicked the Call Now button, broken down by age and gender group. | day, week, days\_28 |
| `page_call_phone_clicks_logged_in_by_country_unique` | Number of people who logged into Facebook and clicked the Call Now button, broken down by countries. | day, week |
| `page_call_phone_clicks_logged_in_by_city_unique` | Number of people who logged into Facebook and clicked the Call Now button, broken down by city. | day, week |
| `page_call_phone_clicks_by_site_logged_in_unique` | The number of people who clicked your Page's phone number or Call now button while they were logged into Facebook, broken down by the type of device they used. | day, week, days\_28 |
| `page_get_directions_clicks_logged_in_unique` | Number of people who logged in to Facebook and clicked the Get Directions button. | day, week, days\_28 |
| `page_get_directions_clicks_by_age_gender_logged_in_unique` | Number of people who logged into Facebook and clicked the Get Directions button, broken down by age and gender group. | day, week, days\_28 |
| `page_get_directions_clicks_logged_in_by_country_unique` | Number of people who logged in to Facebook and clicked the Get Directions button, broken down by country. | day, week |
| `page_get_directions_clicks_logged_in_by_city_unique` | Number of people who logged in to Facebook and clicked the Get Directions button, broken down by city. | day, week |
| `page_get_directions_clicks_by_site_logged_in_unique` | Number of people who logged in to Facebook and clicked the Get Directions button, broken down by www, mobile, api or other. | day, week, days\_28 |
| `page_website_clicks_logged_in_unique*` | Number of people who logged in to Facebook and clicked the goto website CTA button. | day, week, days\_28 |
| `page_website_clicks_by_age_gender_logged_in_unique` | Number of people who logged into Facebook and clicked the goto website CTA button, broken down by age and gender group. | day, week, days\_28 |
| `page_website_clicks_logged_in_by_country_unique` | Number of people who logged in to Facebook and clicked the goto website CTA button, broken down by country. | day, week |
| `page_website_clicks_logged_in_by_city_unique` | Number of people who logged in to Facebook and clicked the goto website CTA button, broken down by city. | day, week |
| `page_website_clicks_by_site_logged_in_unique` | Number of people who logged in to Facebook and clicked the Page CTA button, broken down by www, mobile, api and other. | day, week, days\_28 |