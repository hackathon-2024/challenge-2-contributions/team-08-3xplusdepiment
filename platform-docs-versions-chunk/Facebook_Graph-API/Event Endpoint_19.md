platform: Facebook
topic: Graph-API
subtopic: Event Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Event Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/event/picture

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/event/picture)

# [`/{event-id}`](https://developers.facebook.com/docs/graph-api/reference/event/)`/picture`

An event's cover photo with profile picture dimensions.