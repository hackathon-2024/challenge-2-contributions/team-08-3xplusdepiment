platform: Facebook
topic: Graph-API
subtopic: Profile Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Profile Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/profile

## Fields

When something has a profile, Graph API returns the fields for that object.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)