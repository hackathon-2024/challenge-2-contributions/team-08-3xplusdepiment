platform: Facebook
topic: Graph-API
subtopic: Request Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Request Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/request

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/request)

# Request `/{request-id}`

An individual game request received by someone, sent by an app or by another person.

### Related Guides

* [Game Requests](https://developers.facebook.com/docs/games/requests/)
    
* [`/{user-id}/apprequests`](https://developers.facebook.com/docs/graph-api/reference/user/apprequests/)