platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/live_videos/

# User Live Videos

Represents a collection of [LiveVideos](https://developers.facebook.com/docs/graph-api/reference/live-video/) on a [User](https://developers.facebook.com/docs/graph-api/reference/user/).

## Reading

Get a collection of [LiveVideos](https://developers.facebook.com/docs/graph-api/reference/live-video/) on a [User](https://developers.facebook.com/docs/graph-api/reference/user/).

### New Page Experience

This endpoint is supported for [New Page Experience](https://developers.facebook.com/docs/pages/new-pages-experience/).