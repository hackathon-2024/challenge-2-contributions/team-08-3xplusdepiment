platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/overview/access-levels

## Advanced Access

[Permissions](https://developers.facebook.com/docs/permissions/reference) with Advanced Access can be requested from any app user, and [features](https://developers.facebook.com/docs/apps/features-reference) with Advanced Access are active for all app users. However, [Business Verification](https://developers.facebook.com/docs/development/release/business-verification) is required to get Advanced Access. In some cases additional [App Review](https://developers.facebook.com/docs/app-review) on an individual permission and feature basis might be required.