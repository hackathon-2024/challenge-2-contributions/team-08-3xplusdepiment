platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/advanced/api-upgrade

## Learn More

* [Graph API Changelog](https://developers.facebook.com/docs/graph-api/changelog) – Learn about the latest version changes.
* [Versioning](https://developers.facebook.com/docs/graph-api/guides/versioning) – Learn all about Graph API versioning, requests to different versions, and more.
* [Test Apps](https://developers.facebook.com/docs/development/build-and-test/test-apps) – Learn how to create test apps to test changes to your app before public release.
* [Test Users](https://developers.facebook.com/docs/development/build-and-test/test-users) – Learn how to create test users to test changes to your app before public release.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)