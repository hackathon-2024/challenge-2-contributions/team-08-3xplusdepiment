platform: Facebook
topic: Graph-API
subtopic: Event Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Event Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/event/

### Edges

| Edge | Description |
| --- | --- |
| [`roles`](https://developers.facebook.com/docs/graph-api/reference/event/roles/)[](#) | List of profiles having roles on the event. Requires an access token of an Admin of the Event |
| [`ticket_tiers`](https://developers.facebook.com/docs/graph-api/reference/event/ticket_tiers/)[](#) | List of ticket tiers. Requires an access token of an Admin of the Event |

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |
| 200 | Permissions error |
| 458 | The session is invalid because the application is not installed |
| 190 | Invalid OAuth 2.0 Access Token |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can't perform this operation on this endpoint.