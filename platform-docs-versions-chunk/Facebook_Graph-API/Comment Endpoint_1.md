platform: Facebook
topic: Graph-API
subtopic: Comment Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Comment Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/comment

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/comment)

# Comment `/{comment-id}`

A `comment` can be made on various types of content on Facebook. Most Graph API nodes have a `/comments` edge that lists all the comments on that object. The `/{comment-id}` node returns a single `comment`.

## Reading

### New Page Experience

This API is supported for New Page Experience.