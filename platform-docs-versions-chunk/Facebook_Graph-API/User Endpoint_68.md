platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/businesses/

## Creating

You can make a POST request to `businesses` edge from the following paths:

* [`/{user_id}/businesses`](https://developers.facebook.com/docs/graph-api/reference/user/businesses/)

When posting to this edge, a [Business](https://developers.facebook.com/docs/marketing-api/reference/business/) will be created.