platform: Facebook
topic: Graph-API
subtopic: Link Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Link Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/link

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/link)

# Link `/{link-id}`

A link shared on Facebook.