platform: Facebook
topic: Graph-API
subtopic: Permissions Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Permissions Reference.md
url: https://developers.facebook.com/docs/permissions

# Permissions Reference for Meta Technologies APIs

Permissions are a form of granular, user-granted Graph API authorization. Before your app can use an endpoint to access an app user's data, the app user must grant your app all permissions required by that endpoint.

Starting on or after October 27, 2023, if your app requests permission to use an endpoint to access an app user’s data, you may need to complete [data handling questions](https://developers.facebook.com/docs/development/release/data-handling-questions/questions-preview). See this [blog post](https://developers.facebook.com/blog/post/2023/04/05/data-handling-questions/) and [FAQs](https://developers.facebook.com/docs/development/release/data-handling-questions/faqs) for more information.

Beginning September 5, some developers may also be required to answer data handling questions during their annual [Data Use Checkup](https://developers.facebook.com/docs/development/maintaining-data-access/data-use-checkup).

You may also use any permission granted to your app to request analytics insights to improve your app and for marketing or advertising purposes, through the use of aggregated and de-identified or anonymized information (provided such data cannot be re-identified).