platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app/roles

### Requirements

* An app access token for the app is required.
    

### Fields

| Name | Description | Type |
| --- | --- | --- |
| `app_id` | The ID of the app being requested. | `string` |
| `user` | The ID of the user who has a role in the app. | `string` |
| `role` | The name of the role that this person has. | `enum{administrators, developers, testers, insights users}` |

## Publishing

You can't perform this operation on this endpoint.

## Updating

You can't perform this operation on this endpoint.

## Deleting

You can't perform this operation on this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)