platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights


### Video Ad Breaks

| Metric Name | Description | Values for period |
| --- | --- | --- |
| `page_daily_video_ad_break_ad_impressions_by_crosspost_status` | The total number of times an ad was shown during ad breaks in crossposted videos. | day |
| `page_daily_video_ad_break_cpm_by_crosspost_status` | The average amount paid by advertisers for 1,000 of impressions of their ads in a crossposted videos. This is a gross number and includes the amount paid to Facebook. | day |
| `page_daily_video_ad_break_earnings_by_crosspost_status` | An estimate of the amount you earned from ad breaks in a crossposted videos, based on the number of impressions and CPM of ads shown. Actual payments may differ if there are content ownership claims or other adjustments. | day |
| `post_video_ad_break_ad_impressions` | The total number of times an ad was shown during ad breaks in your videos. | day, lifetime |
| `post_video_ad_break_earnings` | An estimate of the amount you earned from ad breaks in your videos, based on the number of impressions and CPM of ads shown. Actual payments may differ if there are content ownership claims or other adjustments. | day, lifetime |
| `post_video_ad_break_ad_cpm` | The average amount paid by advertisers for 1,000 impressions of their ads in your videos. This number also includes the amount paid to Facebook. | day, lifetime |