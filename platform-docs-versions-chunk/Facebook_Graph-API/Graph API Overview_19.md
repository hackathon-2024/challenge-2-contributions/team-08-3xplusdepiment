platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/overview/access-levels

## Data Use Checkup

Apps that have Advanced Access for a permission or feature must complete [Data Use Checkup](https://developers.facebook.com/docs/development/maintaining-data-access/data-use-checkup/), which is an annual process to certify that the app accesses Facebook APIs, products, and data in compliance with our [Platform Terms](https://developers.facebook.com/terms) and [Developer Policies](https://developers.facebook.com/devpolicy).