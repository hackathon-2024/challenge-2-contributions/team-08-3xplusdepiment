platform: Facebook
topic: Graph-API
subtopic: Album Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Album Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/album/

# Album

Represents an [Album](https://www.facebook.com/help/www/490693151131920/).

## Reading

Get [Fields](#fields) and [Edges](#edges) on an Album.