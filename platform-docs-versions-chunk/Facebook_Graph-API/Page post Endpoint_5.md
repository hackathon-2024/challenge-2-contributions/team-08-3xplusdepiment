platform: Facebook
topic: Graph-API
subtopic: Page post Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page post Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page-post/


### Edges

| Edge | Description |
| --- | --- |
| [`attachments`](https://developers.facebook.com/docs/graph-api/reference/page-post/attachments/)[](#) | Any attachments that are associated with the story |
| [`comments`](https://developers.facebook.com/docs/graph-api/reference/page-post/comments/) | Comments made on this |
| [`dynamic_posts`](https://developers.facebook.com/docs/graph-api/reference/page-post/dynamic_posts/) | All dynamic ad creatives |
| [`insights`](https://developers.facebook.com/docs/graph-api/reference/page-post/insights/) | Insights for this post (only for Pages). |
| [`likes`](https://developers.facebook.com/docs/graph-api/reference/page-post/likes/) | People who like this |
| [`reactions`](https://developers.facebook.com/docs/graph-api/reference/page-post/reactions/)[](#) | People who reacted on this |
| [`sharedposts`](https://developers.facebook.com/docs/graph-api/reference/page-post/sharedposts/) | Shared posts |
| [`sponsor_tags`](https://developers.facebook.com/docs/graph-api/reference/page-post/sponsor_tags/) | An array sponsor pages tagged in the post |
| [`to`](https://developers.facebook.com/docs/graph-api/reference/page-post/to/) | Profiles mentioned or targeted in this post. |