platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights


### Page Post Impressions

| Metric Name | Description | Values for \`period\` |
| --- | --- | --- |
| `post_impressions*` | The number of times your Page's post entered a person's screen. Posts include statuses, photos, links, videos and more. | lifetime |
| `post_impressions_unique*` | The number of people who had your Page's post enter their screen. Posts include statuses, photos, links, videos and more. This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_paid*` | The number of times your Page content was on screen, attributed to your ads. | lifetime |
| `post_impressions_paid_unique*` | The number of Accounts Center accounts that saw your post at least once, attributed to your ads. This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_fan*` | The number of times your Page content was on screen for accounts that followed or liked your Page. | lifetime |
| `post_impressions_fan_unique*` | The number of [Accounts Center accounts](https://www.facebook.com/business/help/283579896000936) that followed or liked your Page. This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_fan_paid*` | The number of times your Page content was on screen, for accounts that followed or liked your Page through an ad. | lifetime |
| `post_impressions_fan_paid_unique*` | The number of [Accounts Center accounts](https://www.facebook.com/business/help/283579896000936) that followed or liked your Page, [attributed to your ads](https://www.facebook.com/business/help/458681590974355). This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_organic*` | The number of times your Page content was on screen through organic distribution. | lifetime |
| `post_impressions_organic_unique*` | The number of people who had your Page's post enter their screen through unpaid distribution. This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_viral*` | The number of times your Page content was on screen with social information attached. Social information is shown on Feed after someone interacts with your Page, post or story. This metric is [in development](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_viral_unique*` | The number of people who had your Page's post enter their screen with social information attached. As a form of organic distribution, social information displays when a person's friend interacted with you Page or post. This includes when someone's friend likes or follows your Page, engages with a post, shares a photo of your Page and checks into your Page. This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling) and [in development](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_nonviral*` | The number of times your Page content was on screen, excluding when your content was shown with social information attached. Social information is shown on Feed after someone interacts with your Page, post or story. This metric is [in development](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_nonviral_unique*` | This metric counts reach from the organic or paid distribution of your Page content, excluding when your content was shown with social information attached. Social information is shown on Feed after someone interacts with your Page, post or story. Reach is only counted once if it occurs from both organic and paid distribution. This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling) and [in development](https://www.facebook.com/business/help/metrics-labeling). | lifetime |
| `post_impressions_by_story_type*` | The number of times your Page content was on screen, grouped by [Page Story type](https://developers.facebook.com/docs/graph-api/reference/v18.0/insights#page-story-types). | lifetime |
| `post_impressions_by_story_type_unique*` | This metric counts reach from the organic or paid distribution of your Page content, grouped by [Page Story type](https://developers.facebook.com/docs/graph-api/reference/v18.0/insights#page-story-types). Reach is only counted once if it occurs from both organic and paid distribution. This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling). | lifetime |