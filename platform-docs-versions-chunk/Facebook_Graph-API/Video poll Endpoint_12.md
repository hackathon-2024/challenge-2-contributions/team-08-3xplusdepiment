platform: Facebook
topic: Graph-API
subtopic: Video poll Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video poll Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video-poll-option/

### Parameters

This endpoint doesn't have any parameters.

### Fields

| Field | Description |
| --- | --- |
| `id`<br><br>numeric string | Poll option ID<br><br>[Core](https://developers.facebook.com/docs/apps/versions/#coreextended) |
| `is_correct`<br><br>bool | True if this answer is considered correct, otherwise false |
| `order`<br><br>int32 | Options appear in increasing numerical order within a poll |
| `text`<br><br>string | Text to display to the user for this option<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `total_votes`<br><br>int32 | Total number of votes for this option |

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can't perform this operation on this endpoint.