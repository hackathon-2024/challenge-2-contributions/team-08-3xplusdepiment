platform: Facebook
topic: Graph-API
subtopic: Video poll Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video poll Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video-poll-option/

# Video Poll Option

## Reading

Represents a single poll option that may be selected by the user

### Feature Permissions

| Name | Description |
| --- | --- |
| `Live Video API` | This is a required [feature permission](https://developers.facebook.com/docs/apps/review/feature/) |