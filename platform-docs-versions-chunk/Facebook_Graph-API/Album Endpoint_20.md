platform: Facebook
topic: Graph-API
subtopic: Album Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Album Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/album/photos/

# Album Photos

Represents a collection [Photos](https://developers.facebook.com/docs/graph-api/reference/photo) and [Videos](https://developers.facebook.com/docs/graph-api/reference/video) on an [Album](https://developers.facebook.com/docs/graph-api/reference/album).

## Reading

Get a list of [Photos](https://developers.facebook.com/docs/graph-api/reference/photo) on an [Album](https://developers.facebook.com/docs/graph-api/reference/album).

### New Page Experience

This endpoint is supported for [New Page Experience](https://developers.facebook.com/docs/pages/new-pages-experience/).