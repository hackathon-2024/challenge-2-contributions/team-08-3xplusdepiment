platform: Facebook
topic: Graph-API
subtopic: Oembed page Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Oembed page Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/oembed-page/

### Parameters

| Parameter | Description |
| --- | --- |
| `adapt_container_width`<br><br>boolean | Default value: `true`<br><br>Try to fit inside the container width. |
| `hide_cover`<br><br>boolean | Default value: `false`<br><br>Hide cover photo in the header. |
| `maxheight`<br><br>int64 | Maximum height of returned media. |
| `maxwidth`<br><br>int64 | Maximum width of returned media. |
| `omitscript`<br><br>boolean | Default value: `false`<br><br>If set to true, the returned embed HTML code will not include any javascript. |
| `sdklocale`<br><br>string | sdklocale |
| `show_facepile`<br><br>boolean | Default value: `true`<br><br>Show profile photos when friends like this. |
| `show_posts`<br><br>boolean | Default value: `true`<br><br>show\_posts |
| `small_header`<br><br>boolean | Default value: `false`<br><br>Use the small header instead. |
| `url`<br><br>URI | The page's url.<br><br>Required |