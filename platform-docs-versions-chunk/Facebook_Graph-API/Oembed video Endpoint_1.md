platform: Facebook
topic: Graph-API
subtopic: Oembed video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Oembed video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/oembed-video/

# Oembed Video

## Reading

OembedVideo