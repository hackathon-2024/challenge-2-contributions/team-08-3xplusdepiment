platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/advanced/api-upgrade

### Read the Results

The tool displays the number of changes that need to be made to update your app to the selected version. If your app makes API calls that will not be affected by a newer version no data will be returned.

Methods are color coded by the version affecting the call. Hover over the bar chart to see how many changes are in each version. The dates associated with each version are when the changes will be enforced for all apps.

The table shows the type of change (deprecation, new feature or change), which methods are affected, the number of calls made in the last 7 days, and the percentage of API calls affected by that specific change.