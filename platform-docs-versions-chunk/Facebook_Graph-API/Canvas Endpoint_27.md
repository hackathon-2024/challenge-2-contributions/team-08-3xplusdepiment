platform: Facebook
topic: Graph-API
subtopic: Canvas Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Canvas Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/canvas-product-list/

### Fields

| Field | Description |
| --- | --- |
| `id`<br><br>numeric string | The id of the element |
| `bottom_padding`<br><br>numeric string | The padding below the element |
| `element_group_key`<br><br>string | The element group key to bundle multiple elements in editing |
| `element_type`<br><br>enum | The type of the element<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `item_description`<br><br>string | A token to represent which field from the product to show in the product description |
| `item_headline`<br><br>string | A token to represent which field from the product to show in the product headline |
| `name`<br><br>string | The name of the element<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `product_id_list`<br><br>list<integer> | A list of product ids inside the canvas |
| `top_padding`<br><br>numeric string | The padding above the element |