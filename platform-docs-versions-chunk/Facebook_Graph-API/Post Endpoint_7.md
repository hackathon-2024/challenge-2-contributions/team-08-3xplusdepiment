platform: Facebook
topic: Graph-API
subtopic: Post Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Post Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/post/


### Edges

| Edge | Description |
| --- | --- |
| [`attachments`](https://developers.facebook.com/docs/graph-api/reference/post/attachments/)[](#) | Any attachments that are associated with the story |
| [`comments`](https://developers.facebook.com/docs/graph-api/reference/post/comments/) | Comments made on this |
| [`dynamic_posts`](https://developers.facebook.com/docs/graph-api/reference/post/dynamic_posts/) | All dynamic ad creatives |
| [`insights`](https://developers.facebook.com/docs/graph-api/reference/post/insights/) | Insights for this post (only for Pages). |
| [`reactions`](https://developers.facebook.com/docs/graph-api/reference/post/reactions/)[](#) | People who reacted on this |
| [`sharedposts`](https://developers.facebook.com/docs/graph-api/reference/post/sharedposts/) | Shared posts |
| [`sponsor_tags`](https://developers.facebook.com/docs/graph-api/reference/post/sponsor_tags/) | An array sponsor pages tagged in the post |
| [`to`](https://developers.facebook.com/docs/graph-api/reference/post/to/) | Profiles mentioned or targeted in this post. |