platform: Facebook
topic: Graph-API
subtopic: Canvas Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Canvas Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/canvas/

### Edges

| Edge | Description |
| --- | --- |
| [`preview`](https://developers.facebook.com/docs/graph-api/reference/canvas/preview/) | Get preview HTML embed element |
| [`previews`](https://developers.facebook.com/docs/graph-api/reference/canvas/previews/)[](#) | Get preview notifications for the canvas |

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |
| 80001 | There have been too many calls to this Page account. Wait a bit and try again. For more info, please refer to https://developers.facebook.com/docs/graph-api/overview/rate-limiting. |
| 368 | The action attempted has been deemed abusive or is otherwise disallowed |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can update a [Canvas](https://developers.facebook.com/docs/graph-api/reference/canvas/) by making a POST request to [`/{canvas_id}`](https://developers.facebook.com/docs/graph-api/reference/canvas/).