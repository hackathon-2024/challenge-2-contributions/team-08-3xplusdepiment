platform: Facebook
topic: Graph-API
subtopic: Post Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Post Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/post/

# Post

An individual post in a profile's feed. The profile could be a user, page, app, or group.

## Reading

A Facebook Feed story

### New Page Experience

This endpoint is supported for [New Page Experience](https://developers.facebook.com/docs/pages/new-pages-experience/).

### Feature Permissions

| Name | Description |
| --- | --- |
| Page Public Content Access | This [feature permission](https://developers.facebook.com/docs/apps/review/feature/) may be required. |