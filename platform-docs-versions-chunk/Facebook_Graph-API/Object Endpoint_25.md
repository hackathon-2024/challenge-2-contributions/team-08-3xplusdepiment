platform: Facebook
topic: Graph-API
subtopic: Object Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Object Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/object/reactions

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/object/reactions)

# Object Reactions

This reference describes the `/reactions` edge that is common to multiple Graph API nodes. The structure and operations are the same for each node. The following objects have a `/reactions` edge:

|     |     |
| --- | --- |
| * [Comment](https://developers.facebook.com/docs/graph-api/reference/comment)<br>* [PagePost](https://developers.facebook.com/docs/graph-api/reference/pagepost)<br>* [Post](https://developers.facebook.com/docs/graph-api/reference/post) |     |

## Reading

Get reactions on an object.

View [Insights](https://developers.facebook.com/docs/graph-api/reference/insights#availmetrics) for more information on Page and Post reactions.

### New Page Experience

This endpoint is supported for New Page Experience.