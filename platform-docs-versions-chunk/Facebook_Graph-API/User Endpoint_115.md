platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/user/outbox

# Graph API Reference [`/{user-id}`](https://developers.facebook.com/docs/graph-api/reference/user/)`/outbox`

A person's Facebook Messages outbox.