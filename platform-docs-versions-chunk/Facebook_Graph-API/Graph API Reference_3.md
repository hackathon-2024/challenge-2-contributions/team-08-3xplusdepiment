platform: Facebook
topic: Graph-API
subtopic: Graph API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Reference.md
url: https://developers.facebook.com/docs/graph-api/reference

### Graph API Root Edges

Root edges are edges that can be queried directly. They allow you to access collections of nodes that are not on a parent node.

| Node | Description |
| --- | --- |
| [`Ads Archive`](https://developers.facebook.com/docs/graph-api/reference/ads_archive/) | Returns archived ads based on your search. |
| [`Branded Content Search`](https://developers.facebook.com/docs/graph-api/reference/branded_content_search/) | Returns branded content based on your search. By default we only return content that is currently available on Facebook or Instagram, and content that was created on or after August 17, 2023. |
| [`Debug Token`](https://developers.facebook.com/docs/graph-api/reference/debug_token/) | Metadata about a particular access token |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)