platform: Facebook
topic: Graph-API
subtopic: Payment Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Payment Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/payment

## Publishing

You can create payments by using [Facebook Payments](https://developers.facebook.com/docs/payments).

## Deleting

You cannot delete payments using this API.

## Updating

You cannot update payments using this edge.

## Edges

| Name | Description |
| --- | --- |
| [/dispute](https://developers.facebook.com/docs/graph-api/reference/payment/dispute) | Updates the dispute status of a payment. |
| [/refunds](https://developers.facebook.com/docs/graph-api/reference/payment/refunds) | Refunds a payment. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)