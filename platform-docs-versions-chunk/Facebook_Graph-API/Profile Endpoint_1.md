platform: Facebook
topic: Graph-API
subtopic: Profile Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Profile Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/profile

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/profile)

# Profile `/{profile-id}`

A profile can be a:

* [User](https://developers.facebook.com/docs/graph-api/reference/user)
    
* [Page](https://developers.facebook.com/docs/graph-api/reference/page)
    
* [Group](https://developers.facebook.com/docs/graph-api/reference/group)
    
* [Event](https://developers.facebook.com/docs/graph-api/reference/event)
    
* [Application](https://developers.facebook.com/docs/graph-api/reference/app)
    

The profile object is used within the Graph API to refer to the generic type that includes all of these other objects.

The individual reference docs for each profile type should be used instead.