platform: Facebook
topic: Graph-API
subtopic: Event Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Event Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/event/photos

### Permissions

* A user access token is required.
    

### Fields

An array of [Photo objects](https://developers.facebook.com/docs/graph-api/reference/photo).