platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/changelog

# Changelog

|     |     |
| --- | --- |
| The latest Graph API version is: | `v19.0` |

The Graph API and Marketing API changelogs document [versioned](#versioned) and [out-of-cycle](#outofcycle) changes, respective to the API.