platform: Facebook
topic: Graph-API
subtopic: Page call to action Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page call to action Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page-call-to-action/

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can update a [PageCallToAction](https://developers.facebook.com/docs/graph-api/reference/page-call-to-action/) by making a POST request to [`/{page_call_to_action_id}`](https://developers.facebook.com/docs/graph-api/reference/page-call-to-action/).