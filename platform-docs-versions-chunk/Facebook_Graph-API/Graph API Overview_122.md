platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/advanced/api-upgrade

## Implement a New Version

In the App Dashboard **Settings > Advanced**, scroll to the **Upgrade API Version** section.

#### Upgrading Developers and Admins

This upgrades all developers and admins of an app to the next available version. This allows you to test changes with a small subset of real users before releasing the new version to the public.

#### Upgrading All Calls

This upgrades all calls made by an app to the next available version. Upgrading early is useful since it preserves the option of going back to the original version in case of unforeseen bugs or issues.