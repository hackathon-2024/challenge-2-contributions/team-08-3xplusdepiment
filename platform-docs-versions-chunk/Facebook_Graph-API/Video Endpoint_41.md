platform: Facebook
topic: Graph-API
subtopic: Video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video/thumbnails/

# Video Thumbnails

Represents a collection of [VideoThumbnails ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/graph-api/reference/video-thumbnail) on a [Video ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/graph-api/reference/video) 

## Reading

Get a list of [VideoThumbnails](https://developers.facebook.com/docs/graph-api/reference/video-thumbnail/) on a [Video](https://developers.facebook.com/docs/graph-api/reference/video/).