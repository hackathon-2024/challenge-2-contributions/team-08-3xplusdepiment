platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api


# Graph API

|     |     |
| --- | --- |
| **The latest version is**: | `v19.0` |

The Graph API is the primary way for apps to read and write to the Facebook social graph. All of our SDKs and products interact with the Graph API in some way, and our other APIs are extensions of the Graph API, so understanding how the Graph API works is crucial.

If you are unfamiliar with the Graph API, we recommend that you start with these documents:

1. [Overview](https://developers.facebook.com/docs/graph-api/overview) – Learn how the Graph API is structured, and how it works.
2. [Get Started](https://developers.facebook.com/docs/graph-api/get-started) – Explore the Graph API endpoints using the Graph API Explorer tool and run your first request.
3. [Batch requests](https://developers.facebook.com/docs/graph-api/batch-requests) – Learn how to send multiple API requests in one call.
4. [Debug requests](https://developers.facebook.com/docs/graph-api/guides/debugging) – Learn how to debug API requests.
5. [Handle errors](https://developers.facebook.com/docs/graph-api/guides/error-handling) – Learn how to handle common errors encountered when using the Graph API.
6. [Field expansion](https://developers.facebook.com/docs/graph-api/guides/field-expansion) – Learn how to limit the number of objects returned in a request and perform nested requests.
7. [Secure requests](https://developers.facebook.com/docs/graph-api/guides/secure-requests) – Learn how to make secure requests to the Graph API.
8. [Resumable Uploads API](https://developers.facebook.com/docs/graph-api/guides/upload) – Learn how to upload files to the Graph API.

#### [Reference](https://developers.facebook.com/docs/graph-api/reference)

Learn how to read our reference documents so you can easily find what you're looking for.