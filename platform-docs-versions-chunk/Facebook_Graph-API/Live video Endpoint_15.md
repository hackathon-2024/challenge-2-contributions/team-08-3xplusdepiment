platform: Facebook
topic: Graph-API
subtopic: Live video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Live video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/live-video/

## Updating

You can update a [LiveVideo](https://developers.facebook.com/docs/graph-api/reference/live-video/) by making a POST request to [`/{live_video_id}`](https://developers.facebook.com/docs/graph-api/reference/live-video/).