platform: Facebook
topic: Graph-API
subtopic: Page upcoming change Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page upcoming change Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page-upcoming-change/

# Page Upcoming Change

## Reading

Notification of page upcoming changes.