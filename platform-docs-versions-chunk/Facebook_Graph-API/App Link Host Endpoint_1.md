platform: Facebook
topic: Graph-API
subtopic: App Link Host Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/App Link Host Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app-link-host

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/app-link-host)

# App Link Host `/app-link-host`

An individual app link host object created by an app. An app link host is a wrapper for a group of different app links.

Please see our main [App Links](https://developers.facebook.com/docs/applinks) documentation to learn more.