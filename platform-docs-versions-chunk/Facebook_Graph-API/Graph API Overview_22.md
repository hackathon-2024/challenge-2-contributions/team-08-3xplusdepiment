platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/guides/our-sdks

# The Facebook SDKs

Facebook SDKs have built-in methods and objects to get data in and out of the Meta social graph.