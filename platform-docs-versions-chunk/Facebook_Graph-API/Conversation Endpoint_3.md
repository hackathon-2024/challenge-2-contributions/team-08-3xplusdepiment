platform: Facebook
topic: Graph-API
subtopic: Conversation Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Conversation Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/conversation

### Permissions

* A Page access token requested by a person who can perform the [`MESSAGING` task](https://developers.facebook.com/docs/pages/overview-1#tasks) on the Page being queried
    
* The [`pages_messaging` permission](https://developers.facebook.com/docs/apps/review/login-permissions#reference-pages_messaging)
    
* The [`pages_manage_metadata` permission](https://developers.facebook.com/docs/apps/review/login-permissions#reference-pages_manage_metadata)
    

For Instagram Messaging, you will also need:

* The [`instagram_basic` permission](https://developers.facebook.com/docs/apps/review/login-permissions#reference-instagram_basic)
    
* The [`instagram_manage_messages` permission](https://developers.facebook.com/docs/apps/review/login-permissions#reference-instagram_manage_messages)