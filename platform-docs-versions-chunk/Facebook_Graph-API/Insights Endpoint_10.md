platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights


### Page Posts

| Metric Name | Description | Values for \`period\` |
| --- | --- | --- |
| `page_posts_impressions*` | The number of times your Page's posts entered a person's screen. Posts include statuses, photos, links, videos and more. | day, week, days\_28 |
| `page_posts_impressions_unique*` | The number of people who had any of your Page's posts enter their screen. Posts include statuses, photos, links, videos and more. | day, week, days\_28 |
| `page_posts_impressions_paid*` | The number of times your Facebook Page and Page content was on screen, attributed to your ads. | day, week, days\_28 |
| `page_posts_impressions_paid_unique*` | The number of Accounts Center accounts that saw any posts from your Page at least once, attributed to your ads. This metric is [estimated](https://www.facebook.com/business/help/metrics-labeling). | day, week, days\_28 |
| `page_posts_impressions_organic*` | The number of times your Page content was on screen through organic distribution. | day, week, days\_28 |
| `page_posts_impressions_organic_unique*` | The number of people who had any of your Page's posts enter their screen through unpaid distribution. | day, week, days\_28 |
| `page_posts_served_impressions_organic_unique` | The number of people who were served your Page's posts in their Feed whether it entered their screen or not. Posts include statuses, photos, links, videos and more. | day, week, days\_28 |
| `page_posts_impressions_viral*` | The number of times your Page's posts entered a person's screen with social information attached. Social information displays when a person's friend interacted with you Page or post. This includes when someone's friend likes or follows your Page, engages with a post, shares a photo of your Page and checks into your Page. | day, week, days\_28 |
| `page_posts_impressions_viral_unique*` | The number of people who had any of your Page's posts enter their screen with social information attached. As a form of organic distribution, social information displays when a person's friend interacted with you Page or post. This includes when someone's friend likes or follows your Page, engages with a post, shares a photo of your Page and checks into your Page. | day, week, days\_28 |
| `page_posts_impressions_nonviral*` | The number of times your Page's posts entered a person's screen. This does not include content created about your Page with social information attached. Social information displays when a person's friend interacted with you Page or post. This includes when someone's friend likes or follows your Page, engages with a post, shares a photo of your Page and checks into your Page. | day, week, days\_28 |
| `page_posts_impressions_nonviral_unique*` | The number of people who had any posts by your Page enter their screen. This does not include content created about your Page with social information attached. As a form of organic distribution, social information displays when a person's friend interacted with you Page or post. This includes when someone's friend likes or follows your Page, engages with a post, shares a photo of your Page and checks into your Page. | day, week, days\_28 |
| `page_posts_impressions_frequency_distribution` | The number of people who saw your Page posts, broken down by how many times people saw your posts. | day, week, days\_28 |