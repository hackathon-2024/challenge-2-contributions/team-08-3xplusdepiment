platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/user/payment_transactions

### Permissions

* An app access token is required to view any transactions made using that app.
    

### Fields

An array of [Payment objects](https://developers.facebook.com/docs/reference/api/payment/).

## Publishing

You can't publish using this edge. Please see the [Payments docs for details on how to integrate this into your ap](https://developers.facebook.com/docs/payments/).

## Deleting

You can't delete using this edge.

## Updating

You can't update using this edge.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)