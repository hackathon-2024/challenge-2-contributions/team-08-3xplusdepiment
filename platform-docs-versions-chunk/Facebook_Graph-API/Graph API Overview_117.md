platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/changelog

## Available Marketing API Versions

| Version | Date Introduced | Available Until |
| --- | --- | --- |
| [v19.0](https://developers.facebook.com/docs/marketing-api/marketing-api-changelog/version19.0) | January 23, 2024 | TBD |
| [v18.0](https://developers.facebook.com/docs/marketing-api/marketing-api-changelog/version18.0) | September 12, 2023 | August 13, 2024 |
| [v17.0](https://developers.facebook.com/docs/graph-api/changelog/version17.0#marketing-api) | May 23, 2023 | May 14, 2024 |
| [v16.0](https://developers.facebook.com/docs/graph-api/changelog/version16.0#marketing-api) | February 2, 2023 | February 6, 2024 |

## Out-Of-Cycle Changes

* [2023 out-of-cycle changes](https://developers.facebook.com/docs/graph-api/changelog/non-versioned-changes/nvc-2023)

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)