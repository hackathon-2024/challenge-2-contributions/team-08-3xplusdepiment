platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/albums/

# User Albums

Albums for a Facebook User.

## Reading

UserAlbums

### Permissions

* A user access token is required with the `user_photos` permission to view a person's albums.
    

This API can only be used to read albums of the person who created the access token. It can't be used to read the albums of friends or non-friends.