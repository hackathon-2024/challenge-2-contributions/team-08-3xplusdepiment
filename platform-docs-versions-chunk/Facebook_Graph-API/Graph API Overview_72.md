platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/get-started


## Learn More

You can use the Graph API Explorer to test any request for Users, Pages, Groups, and more. Visit the [reference](https://developers.facebook.com/docs/graph-api/reference) for each node or edge to determine the permission and access token type required.

|     |     |
| --- | --- |
| * [Access Token](https://developers.facebook.com/docs/facebook-login/access-tokens)<br>* [Facebook Login](https://developers.facebook.com/docs/facebook-login)<br>* [Facebook SDKs](https://developers.facebook.com/docs#apis-and-sdks) | * [Graph API References](https://developers.facebook.com/docs/graph-api/reference)<br>* [Graph API Explorer Guide](https://developers.facebook.com/docs/graph-api/guides/explorer)<br>* [Login Security](https://developers.facebook.com/docs/facebook-login/security)<br>* [Permissions Reference](https://developers.facebook.com/docs/permissions/reference) |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)