platform: Facebook
topic: Graph-API
subtopic: Whats app business hsm Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business hsm Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-hsm/

# WhatsApp Message Template

Represents a specific [message template](https://developers.facebook.com/docs/whatsapp/overview/messages#message-templates). Make the API call to the message template ID.

  

To find a message template ID, call ``https://graph.facebook.com/`v19.0`/{whatsapp-business-account-ID}/message_templates``.

  

For more information on how to use the API, see [WhatsApp Business Management API](https://developers.facebook.com/docs/whatsapp/business-account-management-api).

## Reading

Retrieves information about the message template