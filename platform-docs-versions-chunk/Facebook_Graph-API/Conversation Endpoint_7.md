platform: Facebook
topic: Graph-API
subtopic: Conversation Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Conversation Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/conversation/messages

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/conversation/messages)

# Graph API Reference [`/{conversation-id}`](https://developers.facebook.com/docs/graph-api/reference/conversation/)`/messages`

The messages in a conversation between a person and a Facebook Page or Instagram Professional account.