platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights

### Page Post Reactions

The "like" reaction counts include both "like" and "care" reactions.

| Metric Name | Description | Values for \`period\` |
| --- | --- | --- |
| `post_reactions_like_total` | Total "like" reactions of a post. | lifetime |
| `post_reactions_love_total` | Total "love" reactions of a post. | lifetime |
| `post_reactions_wow_total` | Total "wow" reactions of a post. | lifetime |
| `post_reactions_haha_total` | Total "haha" reactions of a post. | lifetime |
| `post_reactions_sorry_total` | Total "sad" reactions of a post. | lifetime |
| `post_reactions_anger_total` | Total "anger" reactions of a post. | lifetime |
| `post_reactions_by_type_total` | Total post reactions by type. | lifetime |