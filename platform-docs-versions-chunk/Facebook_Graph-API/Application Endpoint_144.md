platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app/subscriptions

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/app/subscriptions)

# [`/{app-id}`](https://developers.facebook.com/docs/reference/api/application/)`/subscriptions`

This edge allows you to configure [webhooks](https://developers.facebook.com/docs/graph-api/webhooks/) subscriptions on an app.