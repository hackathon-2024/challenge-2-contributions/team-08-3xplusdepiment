platform: Facebook
topic: Graph-API
subtopic: Request Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Request Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/request

### Permissions

* A user access token is required if you are using only the Request object ID. The request must also have been sent to the person whose access token you are using.
    
* An app access token can be used when you are using the concatenated Request object ID and user ID string.
    

### Fields

No fields are needed to delete.

### Response

If successful:

{
  "success": true
}

Otherwise a relevant error message will be returned.

## Updating

You can't update using this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)