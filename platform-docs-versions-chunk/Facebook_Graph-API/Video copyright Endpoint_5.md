platform: Facebook
topic: Graph-API
subtopic: Video copyright Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video copyright Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video-copyright/

### Edges

| Edge | Description |
| --- | --- |
| [`update_records`](https://developers.facebook.com/docs/graph-api/reference/video-copyright/update_records/) | Update records of the copyright |

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can update a [VideoCopyright](https://developers.facebook.com/docs/graph-api/reference/video-copyright/) by making a POST request to [`/{video_copyright_id}`](https://developers.facebook.com/docs/graph-api/reference/video-copyright/).