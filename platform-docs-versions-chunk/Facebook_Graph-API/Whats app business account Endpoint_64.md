platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/template_performance_metrics/

# Whats App Business Account Template Performance Metrics

## Reading

Get a performance metrics on a WhatsApp template.

### Limitations

Businesses, WhatsApp Businesses, and business phone numbers originating in the EU or Japan are not supported.