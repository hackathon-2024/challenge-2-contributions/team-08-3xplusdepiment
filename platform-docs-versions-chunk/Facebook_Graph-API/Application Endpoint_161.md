platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/uploads/

# Application Uploads

## Reading

You can't perform this operation on this endpoint.

## Creating

Create an upload session.

Upload session IDs returned by this endpoint can be used to upload large files, get the status of an upload session, or resume an interrupted session. See the [Resumable Upload API](https://developers.facebook.com/docs/graph-api/guides/upload) document for complete usage instructions.