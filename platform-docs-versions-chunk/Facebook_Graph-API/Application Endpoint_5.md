platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/


### Edges

| Edge | Description |
| --- | --- |
| [`accounts`](https://developers.facebook.com/docs/graph-api/reference/application/accounts/) | Test User accounts associated with the app |
| [`ad_placement_groups`](https://developers.facebook.com/docs/graph-api/reference/application/ad_placement_groups/) | Ad placement groups for publishing ads on this app |
| [`adnetworkanalytics_results`](https://developers.facebook.com/docs/graph-api/reference/application/adnetworkanalytics_results/) | Obtain the results of an async Audience Network query for this publisher entity |
| [`aem_attribution`](https://developers.facebook.com/docs/graph-api/reference/application/aem_attribution/) | aem\_attribution |
| [`aem_conversion_configs`](https://developers.facebook.com/docs/graph-api/reference/application/aem_conversion_configs/) | The aggregated event measurement conversion value configs for this app |
| [`aem_conversion_filter`](https://developers.facebook.com/docs/graph-api/reference/application/aem_conversion_filter/) | Boolean for if product\_set\_id \[fb\_content\_id\] belongs to a certain catalog \[catalog\_id\] |
| [`agencies`](https://developers.facebook.com/docs/graph-api/reference/application/agencies/) | The businesses which are not owner but can advertise for this app |
| [`app_event_types`](https://developers.facebook.com/docs/graph-api/reference/application/app_event_types/) | Info about App Events logged for the app |
| [`app_installed_groups`](https://developers.facebook.com/docs/graph-api/reference/application/app_installed_groups/) | List of facebook groups the app is installed in |
| [`appassets`](https://developers.facebook.com/docs/graph-api/reference/application/appassets/) | appassets |
| [`button_auto_detection_device_selection`](https://developers.facebook.com/docs/graph-api/reference/application/button_auto_detection_device_selection/) | Whether to turn on auto device sampling. |
| [`cloudbridge_settings`](https://developers.facebook.com/docs/graph-api/reference/application/cloudbridge_settings/) | cloudbridge\_settings |
| [`da_checks`](https://developers.facebook.com/docs/graph-api/reference/application/da_checks/) | A list of results after running Dynamic Ads checks on this app. |
| [`events`](https://developers.facebook.com/docs/graph-api/reference/application/events/) | Events |
| [`ios_skadnetwork_conversion_config`](https://developers.facebook.com/docs/graph-api/reference/application/ios_skadnetwork_conversion_config/) | ios\_skadnetwork\_conversion\_config |
| [`mobile_sdk_gk`](https://developers.facebook.com/docs/graph-api/reference/application/mobile_sdk_gk/) | Gatekeeper for Mobile SDK |
| [`model_asset`](https://developers.facebook.com/docs/graph-api/reference/application/model_asset/) | model\_asset |
| [`monetized_digital_store_objects`](https://developers.facebook.com/docs/graph-api/reference/application/monetized_digital_store_objects/) | List of digital store objects for this app monetized via Audience Network |
| [`object_types`](https://developers.facebook.com/docs/graph-api/reference/application/object_types/) | Open Graph Object types associated with this app |
| [`objects`](https://developers.facebook.com/docs/graph-api/reference/application/objects/) | Open Graph objects |
| [`permissions`](https://developers.facebook.com/docs/graph-api/reference/application/permissions/) | The status of permissions that are have been submitted for Login Review |
| [`products`](https://developers.facebook.com/docs/graph-api/reference/application/products/) | In-app-purchaseable products associated with this app |
| [`purchases`](https://developers.facebook.com/docs/graph-api/reference/application/purchases/) | In-app-purchaseable products of this app owned by the user |
| [`roles`](https://developers.facebook.com/docs/graph-api/reference/app/roles/) | The developer roles defined for this app |
| [`subscribed_domains`](https://developers.facebook.com/docs/graph-api/reference/application/subscribed_domains/) | subscribed\_domains |
| [`subscribed_domains_phishing`](https://developers.facebook.com/docs/graph-api/reference/application/subscribed_domains_phishing/) | subscribed\_domains\_phishing |