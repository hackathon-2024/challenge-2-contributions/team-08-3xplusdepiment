platform: Facebook
topic: Graph-API
subtopic: Image copyright Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Image copyright Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/image-copyright/

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Creating

You can make a POST request to `image_copyrights` edge from the following paths:

* [`/{page_id}/image_copyrights`](https://developers.facebook.com/docs/graph-api/reference/page/image_copyrights/)

When posting to this edge, an [ImageCopyright](https://developers.facebook.com/docs/graph-api/reference/image-copyright/) will be created.