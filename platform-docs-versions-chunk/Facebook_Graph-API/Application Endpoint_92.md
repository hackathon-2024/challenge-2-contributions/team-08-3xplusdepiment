platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/mmp_auditing/

# Application Mmp Auditing

## Reading

You can't perform this operation on this endpoint.

## Creating

You can make a POST request to `mmp_auditing` edge from the following paths:

* [`/{application_id}/mmp_auditing`](https://developers.facebook.com/docs/graph-api/reference/application/mmp_auditing/)

When posting to this edge, an [Application](https://developers.facebook.com/docs/graph-api/reference/application/) will be created.