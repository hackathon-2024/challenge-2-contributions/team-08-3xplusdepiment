platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/

### Default Public Profile Fields

The [`public_profile`](https://developers.facebook.com/docs/permissions/reference/public_profile) permission allows apps to read the following fields:

* `id`
* `first_name`
* `last_name`
* `middle_name`
* `name`
* `name_format`
* `picture`
* `short_name`

### Parameters

This endpoint doesn't have any parameters.