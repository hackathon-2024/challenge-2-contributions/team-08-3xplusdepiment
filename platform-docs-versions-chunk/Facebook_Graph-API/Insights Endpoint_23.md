platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights

## See Also

* View all your insights in the [Insights Dashboard](https://www.facebook.com/insights).
* Visit the [Insights Results Reference Guide](https://developers.facebook.com/docs/graph-api/reference/insights_result) for more information.
* Visit the [App Insights Reference Guide](https://developers.facebook.com/docs/graph-api/reference/app/app_insights) reference for App Insights.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)