platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/accounts/

### Limitations

* You can only create a Page as a [test user](https://developers.facebook.com/docs/apps/test-users) or if your app has been allowlisted by your Facebook representative.
    

* * *

You can make a POST request to `accounts` edge from the following paths:

* [`/{user_id}/accounts`](https://developers.facebook.com/docs/graph-api/reference/user/accounts/)

When posting to this edge, no Graph object will be created.