platform: Facebook
topic: Graph-API
subtopic: Conversation Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Conversation Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/conversation/messages

### Permissions

* A Page access token requested by a person who can perform the `MESSAGING` task on the Page
    
* The `[pages_messaging](https://developers.facebook.com/docs/apps/review/login-permissions#reference-pages_messaging)` permission
    
* For Instagram Messaging, the `instagram_manage_messages` permission is also required
    

### Fields

Returns an array of [Message objects](https://developers.facebook.com/docs/graph-api/reference/message) with additional fields:

| Name | Description | Type |
| --- | --- | --- |
| Vector | Vector |

## Send a message

You cannot send a message to this enpoint. Use [Send API](https://developers.facebook.com/docs/messenger-platform/reference/send-api/) instead.

## Delete

You cannot delete this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)