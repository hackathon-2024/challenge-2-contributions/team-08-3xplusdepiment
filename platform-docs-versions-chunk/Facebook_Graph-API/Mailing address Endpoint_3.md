platform: Facebook
topic: Graph-API
subtopic: Mailing address Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Mailing address Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/mailing-address/

### Parameters

This endpoint doesn't have any parameters.