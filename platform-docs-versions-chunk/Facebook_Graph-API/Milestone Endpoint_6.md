platform: Facebook
topic: Graph-API
subtopic: Milestone Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Milestone Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/milestone

### Fields

No fields necessary to delete.

### Response

If successful:

{
  "success": true
}

Otherwise a relevant error message will be returned.

## Updating

### Permissions

You can't perform this operation on this endpoint.

## Edges

| Name | Description |
| --- | --- |
| [`/likes`](https://developers.facebook.com/docs/graph-api/reference/object/likes) | A list of the likes. |
| [`/comments`](https://developers.facebook.com/docs/graph-api/reference/object/comments) | A list of the comments. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)