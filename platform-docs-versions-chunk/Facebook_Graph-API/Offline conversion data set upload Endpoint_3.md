platform: Facebook
topic: Graph-API
subtopic: Offline conversion data set upload Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Offline conversion data set upload Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/offline-conversion-data-set-upload/

### Parameters

This endpoint doesn't have any parameters.