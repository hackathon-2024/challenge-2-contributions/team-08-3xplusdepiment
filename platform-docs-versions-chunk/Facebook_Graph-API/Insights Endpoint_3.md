platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights

### Requirements

| Type | Description |
| --- | --- |
| [Access Tokens](https://developers.facebook.com/docs/pages/access-tokens) | A Page access token requested by a person who can perform the ANALYZE task on the Page. |
| [Features](https://developers.facebook.com/docs/pages/overview#features) | Not applicable. |
| [Permissions](https://developers.facebook.com/docs/pages/overview#permissions) | `read_insights`, `pages_read_engagement` |
| [Page Tasks](https://developers.facebook.com/docs/pages/overview#tasks) | [ANALYZE](https://developers.facebook.com/docs/pages/overview#tasks) |