platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/changelog/versions


## Marketing API

| Version | Release Date | Expiration Date |
| --- | --- | --- |
| [v19.0](https://developers.facebook.com/docs/marketing-api/marketing-api-changelog/version19.0) | January 23, 2024 | TBD |
| [v18.0](https://developers.facebook.com/docs/marketing-api/marketing-api-changelog/version18.0) | September 12, 2023 | August 13, 2024 |
| [v17.0](https://developers.facebook.com/docs/graph-api/changelog/version17.0#marketing-api) | May 23, 2023 | May 14, 2024 |
| [v16.0](https://developers.facebook.com/docs/graph-api/changelog/version16.0#marketing-api) | February 2, 2023 | February 6, 2024 |
| [v15.0](https://developers.facebook.com/docs/graph-api/changelog/version15.0#marketing-api) | September 15, 2022 | September 20, 2023 |
| [v14.0](https://developers.facebook.com/docs/graph-api/changelog/version14.0#marketing-api) | May 25, 2022 | September 20, 2023 |
| [v13.0](https://developers.facebook.com/docs/graph-api/changelog/version13.0#marketing-api) | February 8, 2022 | January 25, 2023 |
| [v12.0](https://developers.facebook.com/docs/graph-api/changelog/version12.0#marketing-api) | September 14, 2021 | August 9, 2022 |
| [v11.0](https://developers.facebook.com/docs/graph-api/changelog/version11.0#marketing-api) | June 8, 2021 | February 23, 2022 |
| [v10.0](https://developers.facebook.com/docs/graph-api/changelog/version10.0#marketing-api) | February 23, 2021 | October 4, 2021 |
| [v9.0](https://developers.facebook.com/docs/graph-api/changelog/version9.0#marketing-api) | November 10, 2020 | August 25, 2021 |
| [v8.0](https://developers.facebook.com/docs/graph-api/changelog/version8.0#marketing-api) | August 4, 2020 | May 4, 2021 |
| [v7.0](https://developers.facebook.com/docs/graph-api/changelog/version7.0#marketing-api) | May 5, 2020 | March 3, 2021 |
| [v6.0](https://developers.facebook.com/docs/graph-api/changelog/version6.0#marketing-api) | February 3, 2020 | Extended to February 8, 2021 |
| [v5.0](https://developers.facebook.com/docs/graph-api/changelog/version5.0#marketing-api) | October 29, 2019 | September 28, 2020 |
| [v4.0](https://developers.facebook.com/docs/graph-api/changelog/version4.0) | July 29, 2019 | March 31, 2020 |
| [v3.3](https://developers.facebook.com/docs/graph-api/changelog/version3.3) | April 30, 2019 | January 13, 2020 |
| [v3.2](https://developers.facebook.com/docs/graph-api/changelog/version3.2) | October 23, 2018 | August 13, 2019 |
| [v3.1](https://developers.facebook.com/docs/graph-api/changelog/version3.1) | July 26, 2018 | May 14, 2019 |
| [v3.0](https://developers.facebook.com/docs/graph-api/changelog/version3.0) | May 1, 2018 | February 1, 2019 |
| [v2.12](https://developers.facebook.com/docs/graph-api/changelog/version2.12) | January 30, 2018 | August 7, 2018 |
| [v2.11](https://developers.facebook.com/docs/graph-api/changelog/version2.11) | November 7, 2017 | August 7, 2018 |
| [v2.10](https://developers.facebook.com/docs/graph-api/changelog/version2.10) | July 18, 2017 | May 8, 2018 |
| [v2.9](https://developers.facebook.com/docs/graph-api/changelog/version2.9) | April 18, 2017 | November 6, 2017 |
| [v2.8](https://developers.facebook.com/docs/graph-api/changelog/version2.8) | October 5, 2016 | July 26, 2017 |
| [v2.7](https://developers.facebook.com/docs/graph-api/changelog/version2.7) | July 13, 2016 | April 25, 2017 |
| [v2.6](https://developers.facebook.com/docs/graph-api/changelog/version2.6) | April 12, 2016 | October 5, 2016 |
| [v2.5](https://developers.facebook.com/docs/graph-api/changelog/version2.5) | October 7, 2015 | July 13, 2016 |
| [v2.4](https://developers.facebook.com/docs/graph-api/changelog/version2.4) | July 8, 2015 | April 11, 2016 |
| [v2.3](https://developers.facebook.com/docs/graph-api/changelog/version2.3) | March 25, 2015 | October 8, 2015 |
| [v2.2](https://developers.facebook.com/docs/graph-api/changelog/version2.2) | October 30, 2014 | July 8, 2015 |
| [v2.1](https://developers.facebook.com/docs/graph-api/changelog/version2.1) | October 1, 2014 | March 11, 2015 |
| v2.0 | October 1, 2014 | March 11, 2015 |
| v1.0 | October 1, 2014 | March 11, 2015 |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)