platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/

Facebook App

# 

A Facebook app.

## Reading

Get information about a Facebook app.

### Permissions

* An app access token can be used to view all fields for an app.