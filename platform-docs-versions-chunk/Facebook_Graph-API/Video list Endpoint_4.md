platform: Facebook
topic: Graph-API
subtopic: Video list Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video list Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video-list/

### Edges

| Edge | Description |
| --- | --- |
| [`videos`](https://developers.facebook.com/docs/graph-api/reference/video-list/videos/)[](#) | Videos in the playlist |

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can't perform this operation on this endpoint.

## Deleting

You can't perform this operation on this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)