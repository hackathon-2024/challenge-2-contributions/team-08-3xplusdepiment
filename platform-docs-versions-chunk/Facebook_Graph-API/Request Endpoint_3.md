platform: Facebook
topic: Graph-API
subtopic: Request Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Request Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/request

### Permissions

* A user access token is required if you are requesting using only the Request object ID, and want to know about the recipient of the request. The request must have been sent to the person whose access token you are using.
    
* An app access token can be used when you are requesting using the concatenated Request object ID and user ID string, or when you are only using the request object ID, but do not need to know recipient info. See the [Requests docs](https://developers.facebook.com/docs/howtos/requests/) for more info on this ID.