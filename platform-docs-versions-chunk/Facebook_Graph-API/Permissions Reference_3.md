platform: Facebook
topic: Graph-API
subtopic: Permissions Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Permissions Reference.md
url: https://developers.facebook.com/docs/permissions


### Ways to ask for a permission

When users log onto your app, they receive a request to grant the permissions your app has requested. Users can grant or deny the requested permissions or any subset of them.

* [Facebook Login ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/facebook-login) 
    
* [Facebook Login for Business ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/facebook-login/facebook-login-for-business/) 
    
* [Business Login for Instagram ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/instagram/business-login-for-instagram) 
    
* [Meta Business Manager ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/business-manager-api) 
    

If your app does not use a permission for 90 days, usually due to user inactivity, the app user must regrant your app that permission.