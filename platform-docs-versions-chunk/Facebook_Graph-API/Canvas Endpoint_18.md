platform: Facebook
topic: Graph-API
subtopic: Canvas Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Canvas Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/canvas-carousel/

### Fields

| Field | Description |
| --- | --- |
| `id`<br><br>numeric string | The id of the element |
| `bottom_padding`<br><br>numeric string | The padding below the element |
| `child_elements`<br><br>list<CanvasPhoto\|CanvasDynamicPhoto> | The child elements of the carousel |
| `element_group_key`<br><br>string | The element group key to bundle multiple elements in editing |
| `element_type`<br><br>enum | The type of the element<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `name`<br><br>string | The name of the element<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `style`<br><br>enum | The presentation style of the carousel |
| `top_padding`<br><br>numeric string | The padding above the element |

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can't perform this operation on this endpoint.