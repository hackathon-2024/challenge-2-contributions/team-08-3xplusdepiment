platform: Facebook
topic: Graph-API
subtopic: Canvas Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Canvas Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/canvas-text/


### Fields

| Field | Description |
| --- | --- |
| `id`<br><br>numeric string | The id of the element |
| `background_color`<br><br>string | The color of the background for the element |
| `bottom_padding`<br><br>numeric string | The padding below the element |
| `element_group_key`<br><br>string | The element group key to bundle multiple elements in editing |
| `element_type`<br><br>enum | The type of the element<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `font_family`<br><br>string | The font family |
| `font_size`<br><br>numeric string | The size of the font for the text |
| `line_height`<br><br>numeric string | The line height of the text |
| `name`<br><br>string | The name of the element<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `rich_text`<br><br>[CanvasRichText](https://developers.facebook.com/docs/graph-api/reference/canvas-rich-text/) | The text content of the element |
| `text_alignment`<br><br>enum | The alignment of the text |
| `text_color`<br><br>string | The color of the text |
| `top_padding`<br><br>numeric string | The padding above the element |