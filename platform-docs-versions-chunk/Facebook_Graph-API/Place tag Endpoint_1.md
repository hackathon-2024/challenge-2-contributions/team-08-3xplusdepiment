platform: Facebook
topic: Graph-API
subtopic: Place tag Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Place tag Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/place-tag/

# `/{place-tag-id}`

An instance of a person being tagged at a place in a photo, video, post, status or link.