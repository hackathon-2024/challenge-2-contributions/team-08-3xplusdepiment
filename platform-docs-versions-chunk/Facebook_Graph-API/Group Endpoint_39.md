platform: Facebook
topic: Graph-API
subtopic: Group Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/group/opted_in_members

### Permissions

* A User or a Page access token of an admin of the Group.
    
* The app must be installed in the Group.
    

### Fields

Returns a list of [User](https://developers.facebook.com/docs/graph-api/reference/user/) nodes.

## Publishing

This operation is not supported.

## Deleting

This operation is not supported.

## Updating

This operation is not supported.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)