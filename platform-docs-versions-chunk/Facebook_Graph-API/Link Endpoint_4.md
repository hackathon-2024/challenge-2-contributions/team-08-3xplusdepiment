platform: Facebook
topic: Graph-API
subtopic: Link Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Link Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/link

## Publishing

Please use the [Sharing documentation](https://developers.facebook.com/docs/sharing/) to publish.

## Updating

You can't update a link using the Graph API.

## Edges

| Name | Description |
| --- | --- |
| [`/likes`](https://developers.facebook.com/docs/graph-api/reference/object/likes) | People who like this link. |
| [`/comments`](https://developers.facebook.com/docs/graph-api/reference/object/comments) | Comments on this link. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)