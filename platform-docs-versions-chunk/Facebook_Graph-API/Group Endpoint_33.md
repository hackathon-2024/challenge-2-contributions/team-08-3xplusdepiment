platform: Facebook
topic: Graph-API
subtopic: Group Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/group/files

### Fields

| Name | Description | Type |
| --- | --- | --- |
| `download_link` | URL to download the file. | `string` |
| `group` | The Group the file is uploaded to (the same Group as in the request). | [`Group`](https://developers.facebook.com/docs/graph-api/reference/group) |
| `id` | The ID of the file. Note that you cannot request each file individually by its ID, only through this edge. | `string` |
| `message` | The text included with the file post. | `string` |
| `updated_time` | The last time the file was changed. | `datetime` |

## Publishing

This operation is not supported.

## Deleting

This operation is not supported.

## Updating

This operation is not supported.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)