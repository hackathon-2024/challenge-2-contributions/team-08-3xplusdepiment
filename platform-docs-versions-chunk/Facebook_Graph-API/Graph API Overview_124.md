platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/changelog/versions

# Versions

The changelogs listed below document changes introduced in new Graph API and Marketing API versions. To learn more about versions, please see our [Platform Versioning](https://developers.facebook.com/docs/apps/versions) document.

Changes introduced outside of a version release are documented in our [Out-Of-Cycle Changes](https://developers.facebook.com/docs/graph-api/changelog/out-of-cycle-changes) document.