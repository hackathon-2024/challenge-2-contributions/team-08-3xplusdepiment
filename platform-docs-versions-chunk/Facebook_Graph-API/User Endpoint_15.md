platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/

### Error Codes

| Error | Description |
| --- | --- |
| 3919 | There was an unexpected technical issue. Please try again. |
| 100 | Invalid parameter |
| 457 | The session has an invalid origin |

You can dissociate a [User](https://developers.facebook.com/docs/graph-api/reference/user/) from a [CustomAudience](https://developers.facebook.com/docs/marketing-api/reference/custom-audience/) by making a DELETE request to [`/{custom_audience_id}/users`](https://developers.facebook.com/docs/marketing-api/reference/custom-audience/users/).