platform: Facebook
topic: Graph-API
subtopic: Branded Content Search Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Branded Content Search Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/branded_content_search/

# Branded Content Search

## Reading

Returns branded content based on your search. By default we only return content that is currently available on Facebook or Instagram, and content that was created on or after August 17, 2023.