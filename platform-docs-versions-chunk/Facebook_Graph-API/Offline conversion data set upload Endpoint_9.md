platform: Facebook
topic: Graph-API
subtopic: Offline conversion data set upload Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Offline conversion data set upload Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/offline-conversion-data-set-upload/pull_sessions/

# Offline Conversion Data Set Upload Pull Sessions

## Reading

OfflineConversionDataSetUploadPullSessions

Starting September 14, 2021, this endpoint will throw an error for version 12.0+ calls made by apps that lack the endpoint's required permissions. This change will apply to all versions on December 13, 2021.