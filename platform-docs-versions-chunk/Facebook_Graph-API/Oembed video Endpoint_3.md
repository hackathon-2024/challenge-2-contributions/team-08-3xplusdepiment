platform: Facebook
topic: Graph-API
subtopic: Oembed video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Oembed video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/oembed-video/

### Parameters

| Parameter | Description |
| --- | --- |
| `maxwidth`<br><br>int64 | maxwidth |
| `omitscript`<br><br>boolean | Default value: `false`<br><br>omitscript |
| `sdklocale`<br><br>string | sdklocale |
| `url`<br><br>URI | url<br><br>Required |
| `useiframe`<br><br>boolean | Default value: `false`<br><br>useiframe |