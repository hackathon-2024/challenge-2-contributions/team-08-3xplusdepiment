platform: Facebook
topic: Graph-API
subtopic: Video copyright Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video copyright Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video-copyright/update_records/

# Video Copyright Update Records

## Reading

Update history records for on video reference files

Starting September 14, 2021, this endpoint will throw an error for version 12.0+ calls made by apps that lack the endpoint's required permissions. This change will apply to all versions on December 13, 2021.