platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/user/outbox

### Permissions

* A user access token with `read_mailbox` permission is required to view that person's outbox.

### Fields

An array of [Thread objects](https://developers.facebook.com/docs/reference/api/thread/).

## Publishing

You can't publish using this edge.

## Deleting

You can't delete using this edge.

## Updating

You can't update using this edge.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)