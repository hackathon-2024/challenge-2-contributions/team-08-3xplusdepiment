platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/advanced/api-upgrade

### Limitations

* You must be an admin or developer of the app to view the app in the tool.
* No data will be returned if your app has not made any, or too few, API calls from the **Update from** version.
* Call volumes may appear incorrect. API call logging is sampled and aggregated over the previous week. It is compared with the call volume to estimate how many of your calls could be affected by a given version change.

**Note:** Not all changes may affect each API call. Use your best judgment on whether a particular change needs to be handled by your app. Be sure to test your API calls in the newer version to ensure it works properly.