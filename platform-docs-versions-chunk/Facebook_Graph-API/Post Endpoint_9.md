platform: Facebook
topic: Graph-API
subtopic: Post Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Post Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/post/

## Updating

You can publish posts by using the [`/{user-id}/feed`](https://developers.facebook.com/docs/graph-api/reference/user/feed), [`/{page-id}/feed`](https://developers.facebook.com/docs/graph-api/reference/page/feed), [`/{event-id}/feed`](https://developers.facebook.com/docs/graph-api/reference/event/feed), or [`/{group-id}/feed`](https://developers.facebook.com/docs/graph-api/reference/group/feed) edges.

When creating a Post for a Page if you use a user access token the post will be in the voice of the user that posted it. If you use a page access token, the post will be in the voice of the page.

You can't perform this operation on this endpoint.

## Deleting

You can't perform this operation on this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)