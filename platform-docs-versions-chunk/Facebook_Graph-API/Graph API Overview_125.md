platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/changelog/versions


## Graph API

| Version | Release Date | Expiration Date |
| --- | --- | --- |
| [v19.0](https://developers.facebook.com/docs/graph-api/changelog/version19.0#graph-api) | January 23, 2023 | TBD |
| [v18.0](https://developers.facebook.com/docs/graph-api/changelog/version18.0#graph-api) | September 12, 2023 | TBD |
| [v17.0](https://developers.facebook.com/docs/graph-api/changelog/version17.0#graph-api) | May 23, 2023 | TBD |
| [v16.0](https://developers.facebook.com/docs/graph-api/changelog/version16.0#graph-api) | February 2, 2023 | TBD |
| [v15.0](https://developers.facebook.com/docs/graph-api/changelog/version15.0#graph-api) | September 15, 2022 | TBD |
| [v14.0](https://developers.facebook.com/docs/graph-api/changelog/version14.0#graph-api) | May 25, 2022 | September 17, 2024 |
| [v13.0](https://developers.facebook.com/docs/graph-api/changelog/version13.0#graph-api) | February 8, 2022 | May 28, 2024 |
| [v12.0](https://developers.facebook.com/docs/graph-api/changelog/version12.0#graph-api) | September 14, 2021 | February 8, 2024 |
| [v11.0](https://developers.facebook.com/docs/graph-api/changelog/version11.0#graph-api) | June 8, 2021 | September 14, 2023 |
| [v10.0](https://developers.facebook.com/docs/graph-api/changelog/version10.0#graph-api) | February 23, 2021 | June 8, 2023 |
| [v9.0](https://developers.facebook.com/docs/graph-api/changelog/version9.0#graph-api) | November 10, 2020 | February 23, 2023 |
| [v8.0](https://developers.facebook.com/docs/graph-api/changelog/version8.0#graph-api) | August 4, 2020 | November 1, 2022 |
| [v7.0](https://developers.facebook.com/docs/graph-api/changelog/version7.0#graph-api) | May 5, 2020 | August 4, 2022 |
| [v6.0](https://developers.facebook.com/docs/graph-api/changelog/version6.0) | February 3, 2020 | May 5, 2022 |
| [v5.0](https://developers.facebook.com/docs/graph-api/changelog/version5.0) | October 29, 2019 | February 3, 2022 |
| [v4.0](https://developers.facebook.com/docs/graph-api/changelog/version4.0) | July 29, 2019 | November 2, 2021 |
| [v3.3](https://developers.facebook.com/docs/graph-api/changelog/version3.3) | April 30, 2019 | August 3, 2021 |
| [v3.2](https://developers.facebook.com/docs/graph-api/changelog/version3.2) | October 23, 2018 | May 4, 2021 |
| [v3.1](https://developers.facebook.com/docs/graph-api/changelog/version3.1) | July 26, 2018 | October 27, 2020 |
| [v3.0](https://developers.facebook.com/docs/graph-api/changelog/version3.0) | May 1, 2018 | July 28, 2020 |
| [v2.12](https://developers.facebook.com/docs/graph-api/changelog/version2.12) | January 30, 2018 | May 5, 2020 |
| [v2.11](https://developers.facebook.com/docs/graph-api/changelog/version2.11) | November 7, 2017 | January 28, 2020 |
| [v2.10](https://developers.facebook.com/docs/graph-api/changelog/version2.10) | July 18, 2017 | November 7, 2019 |
| [v2.9](https://developers.facebook.com/docs/graph-api/changelog/version2.9) | April 18, 2017 | July 22, 2019 |
| [v2.8](https://developers.facebook.com/docs/graph-api/changelog/version2.8) | October 5, 2016 | April 18, 2019 |
| [v2.7](https://developers.facebook.com/docs/graph-api/changelog/version2.7) | July 13, 2016 | October 5, 2018 |
| [v2.6](https://developers.facebook.com/docs/graph-api/changelog/version2.6) | April 12, 2016 | July 13, 2018 |
| [v2.5](https://developers.facebook.com/docs/graph-api/changelog/version2.5) | October 7, 2015 | April 12, 2018 |
| [v2.4](https://developers.facebook.com/docs/graph-api/changelog/version2.4) | July 8, 2015 | October 9, 2017 |
| [v2.3](https://developers.facebook.com/docs/graph-api/changelog/version2.3) | March 25, 2015 | July 10, 2017 |
| [v2.2](https://developers.facebook.com/docs/graph-api/changelog/version2.2) | October 30, 2014 | March 27, 2017 |
| [v2.1](https://developers.facebook.com/docs/graph-api/changelog/version2.1) | August 7, 2014 | October 31, 2016 |
| v2.0 | April 30, 2014 | August 8, 2016 |
| v1.0 | April 21, 2010 | April 30, 2015 |