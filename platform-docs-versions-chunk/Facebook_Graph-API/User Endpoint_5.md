platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/


### Edges

| Edge | Description |
| --- | --- |
| [`accounts`](https://developers.facebook.com/docs/graph-api/reference/user/accounts/) | Pages the User has a role on. |
| [`ad_studies`](https://developers.facebook.com/docs/graph-api/reference/user/ad_studies/) | Ad studies that this User's can view. |
| [`albums`](https://developers.facebook.com/docs/graph-api/reference/user/albums/) | The photo albums this person has created |
| [`apprequestformerrecipients`](https://developers.facebook.com/docs/graph-api/reference/user/apprequestformerrecipients/) | App requests |
| [`apprequests`](https://developers.facebook.com/docs/graph-api/reference/user/apprequests/) | This person's pending requests from an app<br><br>[Core](https://developers.facebook.com/docs/apps/versions/#coreextended) |
| [`assigned_business_asset_groups`](https://developers.facebook.com/docs/graph-api/reference/user/assigned_business_asset_groups/) | Business asset groups that are assign to this business scoped user |
| [`assigned_pages`](https://developers.facebook.com/docs/graph-api/reference/user/assigned_pages/) | Pages that are assigned to this business scoped user |
| [`assigned_product_catalogs`](https://developers.facebook.com/docs/graph-api/reference/user/assigned_product_catalogs/) | Product catalogs that are assigned to this business scoped user |
| [`business_users`](https://developers.facebook.com/docs/graph-api/reference/user/business_users/) | Business users corresponding to the user |
| [`businesses`](https://developers.facebook.com/docs/graph-api/reference/user/businesses/) | Businesses associated with the user |
| [`conversations`](https://developers.facebook.com/docs/graph-api/reference/user/conversations/) | Facebook Messenger conversation |
| [`custom_labels`](https://developers.facebook.com/docs/graph-api/reference/user/custom_labels/)[](#) | custom\_labels |
| [`feed`](https://developers.facebook.com/docs/graph-api/reference/user/feed/) | The posts and links published by this person or others on their profile |
| [`ids_for_apps`](https://developers.facebook.com/docs/graph-api/reference/user/ids_for_apps/)[](#) | Businesses can claim ownership of multiple apps using Business Manager. This edge returns the list of IDs that this user has in any of those other apps |
| [`ids_for_business`](https://developers.facebook.com/docs/graph-api/reference/user/ids_for_business/)[](#) | Businesses can claim ownership of multiple apps using Business Manager. This edge returns the list of IDs that this user has in any of those other apps |
| [`ids_for_pages`](https://developers.facebook.com/docs/graph-api/reference/user/ids_for_pages/)[](#) | Businesses can claim ownership of apps and pages using Business Manager. This edge returns the list of IDs that this user has in any of the pages owned by this business |
| [`likes`](https://developers.facebook.com/docs/graph-api/reference/user/likes/) | All the Pages this person has liked |
| [`live_videos`](https://developers.facebook.com/docs/graph-api/reference/user/live_videos/) | Live videos from this person |
| [`music`](https://developers.facebook.com/docs/graph-api/reference/user/music/) | Music this person likes |
| [`payment.subscriptions`](https://developers.facebook.com/docs/graph-api/reference/user/payment.subscriptions/) | Payment subscriptions |
| [`permissions`](https://developers.facebook.com/docs/graph-api/reference/user/permissions/) | The permissions that the person has granted this app<br><br>[Core](https://developers.facebook.com/docs/apps/versions/#coreextended) |
| [`photos`](https://developers.facebook.com/docs/graph-api/reference/user/photos/) | Photos the person is tagged in or has uploaded |
| [`picture`](https://developers.facebook.com/docs/graph-api/reference/user/picture/) | The person's profile picture<br><br>[Core](https://developers.facebook.com/docs/apps/versions/#coreextended) |
| [`rich_media_documents`](https://developers.facebook.com/docs/graph-api/reference/user/rich_media_documents/) | A list of rich media documents belonging to Pages that the user has advertiser permissions on |
| [`videos`](https://developers.facebook.com/docs/graph-api/reference/user/videos/) | Videos the person is tagged in or uploaded |