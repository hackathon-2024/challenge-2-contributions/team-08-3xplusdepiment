platform: Facebook
topic: Graph-API
subtopic: Debug token Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Debug token Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/debug_token

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/debug_token)

# Debug-Token `/debug_token`

This endpoint returns metadata about a given access token. This includes data such as the user for which the token was issued, whether the token is still valid, when it expires, and what permissions the app has for the given user.

This may be used to programatically debug issues with large sets of access tokens.