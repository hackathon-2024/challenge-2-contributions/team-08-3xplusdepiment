platform: Facebook
topic: Graph-API
subtopic: Group message Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group message Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/group-message/insights/

# Group Message Insights

## Reading

PostInsights

### New Page Experience

This endpoint is supported for [New Page Experience](https://developers.facebook.com/docs/pages/new-pages-experience/).