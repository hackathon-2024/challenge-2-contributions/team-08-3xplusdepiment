platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/overview/access-levels


## Standard Access

[Permissions](https://developers.facebook.com/docs/permissions/reference) with Standard Access can only be requested from app users who have a [role](https://developers.facebook.com/docs/development/build-and-test/app-roles) on the requesting app. Similarly, [features](https://developers.facebook.com/docs/apps/features-reference) with Standard Access are only active for app users who have a role on the app.

[Business](https://developers.facebook.com/docs/development/create-an-app/app-dashboard/app-types#business), [Consumer](https://developers.facebook.com/docs/development/create-an-app/app-dashboard/app-types#consumer), and [Gaming](https://developers.facebook.com/docs/development/create-an-app/app-dashboard/app-types#gaming-services) apps are automatically approved for Standard Access for all permissions and features available to their app type.

Standard Access is intended for apps that will only be used by people who have roles on them, or used during app development, when testing API endpoints that the calling app has not been approved for.