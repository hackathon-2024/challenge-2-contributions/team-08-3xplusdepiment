platform: Facebook
topic: Graph-API
subtopic: Insights Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Insights Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/insights


### Page Views

| Metric Name | Description | Values for \`period\` |
| --- | --- | --- |
| `page_views_total*` | The number of times a Page's profile has been viewed by logged in and logged out people. | day, week, days\_28 |
| `page_views_logout` | The number of times a Page has been viewed by people not logged into Facebook. | day |
| `page_views_logged_in_total*` | The number of times a Page's profile has been viewed by people logged in to Facebook. | day, week, days\_28 |
| `page_views_logged_in_unique*` | The number of people logged in to Facebook who have viewed the Page profile. | day, week, days\_28 |
| `page_views_external_referrals` | Top referrering external domains sending traffic to your Page. | day |
| `page_views_by_profile_tab_total` | The number of people who have viewed each Page profile tab. | day, week, days\_28 |
| `page_views_by_profile_tab_logged_in_unique` | The number of people logged into Facebook who have viewed your Page, broken down by each tab. | day, week, days\_28 |
| `page_views_by_internal_referer_logged_in_unique` | The number of people logged into Facebook who have viewed your Page, broken down by the internal referer within Facebook. | day, week, days\_28 |
| `page_views_by_site_logged_in_unique` | The number of people logged into Facebook who have viewed your Page, broken down by the type of device. | day, week, days\_28 |
| `page_views_by_age_gender_logged_in_unique` | The number of people logged into Facebook who have viewed your Page, broken down by gender group. | day, week, days\_28 |
| `page_views_by_referers_logged_in_unique` | Logged-in Page visit counts (unique users) by referral source. | day, week |