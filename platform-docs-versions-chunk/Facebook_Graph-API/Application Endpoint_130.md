platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app/scores

# [`/{app-id}`](https://developers.facebook.com/docs/graph-api/reference/app/)`/scores`

Scores from this Facebook game for a user and their friends.