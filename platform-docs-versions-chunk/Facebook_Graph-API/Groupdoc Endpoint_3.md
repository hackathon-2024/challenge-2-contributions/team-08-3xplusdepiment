platform: Facebook
topic: Graph-API
subtopic: Groupdoc Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Groupdoc Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/groupdoc

### Permissions

* Any valid access token if the group is public (i.e. the group's privacy setting is `OPEN`)