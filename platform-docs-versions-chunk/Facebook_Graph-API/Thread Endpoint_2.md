platform: Facebook
topic: Graph-API
subtopic: Thread Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Thread Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/thread

### Permissions

* A Page access token requested by a person who can perform the [`MODERATE` task](https://developers.facebook.com/docs/pages/overview/permissions-features#tasks) on the Page.
    
* The [`pages_messaging` permission](https://developers.facebook.com/docs/permissions/reference/pages_messaging)
    
* The [`pages_manage_metadata` permission](https://developers.facebook.com/docs/permissions/reference/pages_manage_metadata)
    
* The [`pages_show_list` permission](https://developers.facebook.com/docs/permissions/reference/pages_show_list)