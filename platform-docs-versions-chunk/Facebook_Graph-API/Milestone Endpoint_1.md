platform: Facebook
topic: Graph-API
subtopic: Milestone Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Milestone Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/milestone

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/milestone)

# Milestone `/{milestone-id}`

This represents a milestone on a Facebook Page.

### Related Guides

* [Adding Milestones to Pages using the Graph API](https://developers.facebook.com/docs/graph-api/reference/page/milestones/)