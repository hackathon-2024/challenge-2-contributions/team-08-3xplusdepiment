platform: Facebook
topic: Graph-API
subtopic: Mailing address Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Mailing address Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/mailing-address/


### Fields

| Field | Description |
| --- | --- |
| `id`<br><br>numeric string | The mailing address ID |
| `city`<br><br>string | Address city name<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `city_page`<br><br>[Page](https://developers.facebook.com/docs/graph-api/reference/page/) | Page representing the address city |
| `country`<br><br>string | Country of the address<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `postal_code`<br><br>string | Postal code of the address<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `region`<br><br>string | Region or state of the address<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `street1`<br><br>string | Street address<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `street2`<br><br>string | Second part of the street address - apt, suite, etc<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |