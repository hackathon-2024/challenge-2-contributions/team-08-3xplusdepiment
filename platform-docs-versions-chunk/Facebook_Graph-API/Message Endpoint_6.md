platform: Facebook
topic: Graph-API
subtopic: Message Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Message Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/message

## Edges

| Name | Description |
| --- | --- |
| [`/attachments`](https://developers.facebook.com/docs/graph-api/reference/message/attachments) | Files attached to a message. |
| [`/shares`](https://developers.facebook.com/docs/graph-api/reference/message/shares/) | Shared items, including links, photos, videos, stickers and products. |

## Create

You can't perform this operation on this endpoint.

## Update

You can't perform this operation on this endpoint.

## Delete

You can't perform this operation on this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)