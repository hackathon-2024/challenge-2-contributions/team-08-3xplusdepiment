platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/changelog

### Versioned Changes

Versioned changes are changes introduced with the release of a new API [version](https://developers.facebook.com/docs/apps/versions). Versioned changes typically apply to the newest version immediately and often will apply to other versions at a future date. The changelog accompanying each release indicates which changes apply to the current release and which changes apply to other versions.

Refer to our [Upgrade Guide](https://developers.facebook.com/docs/apps/upgrading) to learn how to upgrade to a new API version.

### Out-Of-Cycle Changes

Out-of-cycle changes are changes introduced outside of our normal, versioned release schedule and typically do not apply to a specific version. Instead, out-of-cycle changes usually apply to all API versions immediately. Because of this, out-of-cycle changes are introduced infrequently.