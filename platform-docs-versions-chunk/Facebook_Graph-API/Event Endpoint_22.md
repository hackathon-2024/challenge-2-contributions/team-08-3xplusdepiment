platform: Facebook
topic: Graph-API
subtopic: Event Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Event Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/event/picture

### Fields

| Parameter | Description | Type |
| --- | --- | --- |
| `url` | The URL of the profile photo. Only returned when `redirect` is `false`. | `string` |
| `is_silhouette` | Indicates if the photo hasn't been customised and is the default icon. Only returned when `redirect` is `false`. | `boolean` |

## Publishing

You can't publish an event cover photo using the Graph API.

## Deleting

You can't delete an event cover photo using the Graph API.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)