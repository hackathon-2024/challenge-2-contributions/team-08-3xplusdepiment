platform: Facebook
topic: Graph-API
subtopic: Group Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/group/opted_in_members

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/group/opted_in_members)

# [`/{group-id}`](https://developers.facebook.com/docs/graph-api/reference/group/)`/opted_in_members`

This edge allows you to get a list of Users on the Group who have chosen to share their publicly available profile information with apps installed on the Group.

## Creating

This operation is not supported.