platform: Facebook
topic: Graph-API
subtopic: Whats app business hsm Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business hsm Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-hsm/compare/

### Sample Response

{
  "data": \[
    {
      "metric": "BLOCK\_RATE",
      "type": "RELATIVE",
      "order\_by\_relative\_metric": \[
        "1533406637136032",
        "5289179717853347"
      \]
    },
    {
      "metric": "MESSAGE\_SENDS",
      "type": "NUMBER\_VALUES",
      "number\_values": \[
        {
          "key": "5289179717853347",
          "value": 1273
        },
        {
          "key": "1533406637136032",
          "value": 1042
        }
      \]
    },
    {
      "metric": "TOP\_BLOCK\_REASON",
      "type": "STRING\_VALUES",
      "string\_values": \[
        {
          "key": "5289179717853347",
          "value": "UNKNOWN\_BLOCK\_REASON"
        },
        {
          "key": "1533406637136032",
          "value": "UNKNOWN\_BLOCK\_REASON"
        }
      \]
    }
  \]
}