platform: Facebook
topic: Graph-API
subtopic: Commerce merchant settings Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Commerce merchant settings Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/


### Edges

| Edge | Description |
| --- | --- |
| [`commerce_orders`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/commerce_orders/) | Orders for this merchant |
| [`commerce_payouts`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/commerce_payouts/) | The commerce payouts of this Page |
| [`order_management_apps`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/order_management_apps/) | App ID that is authorized to manage this shop |
| [`product_catalogs`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/product_catalogs/) | Product catalogs attached to this merchant |
| [`returns`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/returns/)[](#) | Order Returns for this Merchant |
| [`seller_issues`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/seller_issues/) | seller\_issues |
| [`setup_status`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/setup_status/) | Onboarding status for this merchant |
| [`shops`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/shops/) | The shops associated with the merchant. |
| [`tax_settings`](https://developers.facebook.com/docs/graph-api/reference/commerce-merchant-settings/tax_settings/) | Tax settings including information about fulfillment locations |