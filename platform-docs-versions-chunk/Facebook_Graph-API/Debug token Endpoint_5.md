platform: Facebook
topic: Graph-API
subtopic: Debug token Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Debug token Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/debug_token

## Publishing and Deleting

You cannot perform these actions on this edge.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)