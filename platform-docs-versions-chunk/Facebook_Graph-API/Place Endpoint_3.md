platform: Facebook
topic: Graph-API
subtopic: Place Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Place Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/place/

### Parameters

This endpoint doesn't have any parameters.

### Fields

| Field | Description |
| --- | --- |
| `id`<br><br>numeric string | ID  |
| `location`<br><br>[Location](https://developers.facebook.com/docs/graph-api/reference/location/) | Location of Place<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `name`<br><br>string | Name<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `overall_rating`[](#)<br><br>float | Overall Rating of Place, on a 5-star scale. 0 means not enough data to get a combined rating. |

## Creating

You can't perform this operation on this endpoint.

## Updating

You can't perform this operation on this endpoint.