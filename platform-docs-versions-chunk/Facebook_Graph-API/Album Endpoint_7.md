platform: Facebook
topic: Graph-API
subtopic: Album Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Album Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/album/

### Edges

| Edge | Description |
| --- | --- |
| [`comments`](https://developers.facebook.com/docs/graph-api/reference/album/comments/) | Represents a collection of [Comments](https://developers.facebook.com/docs/graph-api/reference/comment) on the Album. |
| [`likes`](https://developers.facebook.com/docs/graph-api/reference/album/likes/) | A collection of [Profiles](https://developers.facebook.com/docs/graph-api/reference/profile) who like the Album. |
| [`photos`](https://developers.facebook.com/docs/graph-api/reference/album/photos/) | A collection of [Photos](https://developers.facebook.com/docs/graph-api/reference/photo) in the Album |
| [`picture`](https://developers.facebook.com/docs/graph-api/reference/album/picture/) | A link to the Album cover photo. |