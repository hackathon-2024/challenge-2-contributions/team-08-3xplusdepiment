platform: Facebook
topic: Graph-API
subtopic: Whats app business hsm Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business hsm Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-hsm/compare/

# Whats App Message Template Compare

Compare template send counts and block ratios. See [Template Comparison](https://developers.facebook.com/docs/whatsapp/business-management-api/message-templates/template-comparison).

## Reading

Compare the WhatsApp message template's send count and block ratio to another WhatsApp message template's send count and block ratio.

### Requirements

| Type | Description |
| --- | --- |
| [Access Tokens](https://developers.facebook.com/docs/facebook-login/guides/access-tokens) | [User](https://developers.facebook.com/docs/whatsapp/business-management-api/get-started#user-access-tokens) or [System User](https://developers.facebook.com/docs/whatsapp/business-management-api/get-started#user-access-tokens) |
| [Permissions](https://developers.facebook.com/docs/permissions/reference) | [business\_management](https://developers.facebook.com/docs/permissions/reference/business_management) |