platform: Facebook
topic: Graph-API
subtopic: Permissions Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Permissions Reference.md
url: https://developers.facebook.com/docs/permissions


### Requirements

* [App Review ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/app-review) is required for **all permissions** except for `email` and `public_profile` if your app needs access to data that you do not own or manage
    
* [Business Verification ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/development/release/business-verification) is required for all apps making requests for [Advanced Access ![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/310307727_3347317042262105_1088877051262827250_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=6zzb9-5bY8QAX_nY52g&_nc_ht=scontent-cdg4-2.xx&oh=00_AfBRGSoDbMfUe1dC6xxUblU-wz3raGSpfpjPYKA-ck1AaA&oe=65D572A2)](https://developers.facebook.com/docs/graph-api/overview/access-levels/#advanced-access) 
    
* Only select permissions that your app needs to function as intended. Selecting unneeded permissions is a common reason for rejection during app review