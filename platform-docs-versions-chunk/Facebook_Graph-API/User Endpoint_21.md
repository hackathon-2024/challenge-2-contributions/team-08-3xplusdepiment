platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/accounts/

### Parameters

| Parameter | Description |
| --- | --- |
| `is_place`[](#)<br><br>boolean | If specified,filter pages based on whetherthey are places or not |
| `is_promotable`[](#)<br><br>boolean | If specified, filter pages based on whether they can be promoted or not |