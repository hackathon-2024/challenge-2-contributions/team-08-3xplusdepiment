platform: Facebook
topic: Graph-API
subtopic: Group message Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Group message Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/group-message/

### Edges

| Edge | Description |
| --- | --- |
| [`attachments`](https://developers.facebook.com/docs/graph-api/reference/group-message/attachments/)[](#) | attachments |
| [`comments`](https://developers.facebook.com/docs/graph-api/reference/group-message/comments/) | comments |
| [`dynamic_posts`](https://developers.facebook.com/docs/graph-api/reference/group-message/dynamic_posts/) | dynamic\_posts |
| [`insights`](https://developers.facebook.com/docs/graph-api/reference/group-message/insights/) | insights |
| [`reactions`](https://developers.facebook.com/docs/graph-api/reference/group-message/reactions/)[](#) | reactions |
| [`sharedposts`](https://developers.facebook.com/docs/graph-api/reference/group-message/sharedposts/) | sharedposts |
| [`sponsor_tags`](https://developers.facebook.com/docs/graph-api/reference/group-message/sponsor_tags/) | sponsor\_tags |
| [`to`](https://developers.facebook.com/docs/graph-api/reference/group-message/to/) | to  |