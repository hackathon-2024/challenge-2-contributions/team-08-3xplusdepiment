platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/notifications/

# User Notifications

## Reading

You can't perform this operation on this endpoint.

## Creating

You can make a POST request to `notifications` edge from the following paths:

* [`/{user_id}/notifications`](https://developers.facebook.com/docs/graph-api/reference/user/notifications/)

When posting to this edge, no Graph object will be created.