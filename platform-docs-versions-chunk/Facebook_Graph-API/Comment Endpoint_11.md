platform: Facebook
topic: Graph-API
subtopic: Comment Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Comment Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/comment

## Edges

| Property Name | Description | Type |
| --- | --- | --- |
| [`/comments`](https://developers.facebook.com/docs/graph-api/reference/object/comments) | Comments that reply to this comment. | `Edge<Comment>` |
| [`/likes`](https://developers.facebook.com/docs/graph-api/reference/object/likes) | People who like this comment. | `Edge<Profile>` |
| [`/reactions`](https://developers.facebook.com/docs/graph-api/reference/object/reactions) | People who have reacted to this post. | `Edge<Reaction>` |
| [`/private_replies`](https://developers.facebook.com/docs/graph-api/reference/object/private_replies) | Used to send private message reply to this comment (Page viewers only). | `Edge<Message>` |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)