platform: Facebook
topic: Graph-API
subtopic: Debug token Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Debug token Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/debug_token

### Permissions

* An [app access token](https://developers.facebook.com/docs/facebook-login/guides/access-tokens/#apptokens) or an app developer's [user access token](https://developers.facebook.com/docs/facebook-login/guides/access-tokens/#usertokens) for the app associated with the `input_token` beiing inspected is required to access this endpoint.
    

### Parameters

| Name | Description | Type |
| --- | --- | --- |
| `input_token` | The Access Token that is being inspected. This parameter must be specified. | `string` |