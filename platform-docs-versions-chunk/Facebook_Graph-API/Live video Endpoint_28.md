platform: Facebook
topic: Graph-API
subtopic: Live video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Live video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/live-video/crossposted_broadcasts/

# Live Video Crossposted Broadcasts

## Reading

Live videos which have been crossposted from this live video. This field is only available on the original live video.