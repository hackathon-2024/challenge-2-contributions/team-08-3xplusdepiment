platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/mobile_sdk_gk/

### Parameters

| Parameter | Description |
| --- | --- |
| `device_id`<br><br>string | Device ID |
| `extinfo`<br><br>Object | Extra Information |
| `0`<br><br>string | extinfo version<br><br>Required |
| `1`<br><br>string | app package name |
| `2`<br><br>string | short version (int or string) |
| `3`<br><br>string | long version |
| `4`<br><br>string | OS version |
| `5`<br><br>string | device model name |
| `6`<br><br>string | locale |
| `7`<br><br>string | timezone abbreviation |
| `8`<br><br>string | carrier |
| `9`<br><br>int64 | screen width |
| `10`<br><br>int64 | screen height |
| `11`<br><br>string | screen density (float decimal , or .) |
| `12`<br><br>int64 | CPU cores |
| `13`<br><br>int64 | external storage size in GB |
| `14`<br><br>int64 | free space on external storage in GB |
| `15`<br><br>string | device timezone |
| `platform`<br><br>enum {ANDROID, IOS} | SDK Platform<br><br>Required |
| `sdk_version`<br><br>string | SDK version<br><br>Required |