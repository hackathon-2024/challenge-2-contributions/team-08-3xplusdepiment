platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/user/picture/

### Requirements

| Type | Requirement |
| --- | --- |
| [Access Tokens](https://developers.facebook.com/docs/facebook-login/access-tokens) | If querying an App-Scoped User ID:<br><br>* None<br><br>If querying a User ID:<br><br>* [User](https://developers.facebook.com/docs/facebook-login/access-tokens#usertokens) or [Page](https://developers.facebook.com/docs/facebook-login/access-tokens#pagetokens) access token for Facebook Login authenticated requests<br>* [App](https://developers.facebook.com/docs/facebook-login/access-tokens#apptokens) access token for server-side requests<br>* [Client](https://developers.facebook.com/docs/facebook-login/access-tokens#clienttokens) access token for mobile or web client-side requests<br><br>If querying a Page-Scoped User ID:<br><br>* [Page](https://developers.facebook.com/docs/facebook-login/access-tokens#pagetokens) |
| [Permissions](https://developers.facebook.com/docs/permissions/reference) | None |