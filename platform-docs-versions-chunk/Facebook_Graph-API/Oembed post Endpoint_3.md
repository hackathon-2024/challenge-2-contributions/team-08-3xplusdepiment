platform: Facebook
topic: Graph-API
subtopic: Oembed post Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Oembed post Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/oembed-post/

### Parameters

| Parameter | Description |
| --- | --- |
| `maxwidth`<br><br>int64 | Maximum width of returned post. |
| `omitscript`<br><br>boolean | Default value: `false`<br><br>If set to true, the returned embed HTML code will not include any javascript. |
| `sdklocale`<br><br>string | sdklocale |
| `url`<br><br>URI | The post's URL.<br><br>Required |
| `useiframe`<br><br>boolean | Default value: `false`<br><br>useiframe |