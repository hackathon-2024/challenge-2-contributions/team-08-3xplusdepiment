platform: Facebook
topic: Graph-API
subtopic: Place topic Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Place topic Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/place-topic/

# Place Topic

## Reading

The category of a place Page

### New Page Experience

This endpoint is supported for [New Page Experience](https://developers.facebook.com/docs/pages/new-pages-experience/).

### Feature Permissions

| Name | Description |
| --- | --- |
| Page Public Content Access | This [feature permission](https://developers.facebook.com/docs/apps/review/feature/) may be required. |