platform: Facebook
topic: Graph-API
subtopic: User Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/User Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/user/scores

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/user/scores)

As of April 4, 2018, this endpoint only returns an empty data set. Please see the [changelog](https://developers.facebook.com/docs/graph-api/changelog/breaking-changes/#4-4-2018) for more information.

# [`/{user-id}`](https://developers.facebook.com/docs/graph-api/reference/user/)`/scores`

The [scores](https://developers.facebook.com/docs/score/) this person has received from Facebook Games that they've played.