platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/message_template_previews/

# Whats App Business Account Message Template Previews

Represents a collection of WhatsApp authentication template previews. See [Authentication Templates](https://developers.facebook.com/docs/whatsapp/business-management-api/authentication-templates) for additional information about authentication templates.

## Reading

Get previews of [authentication template](https://developers.facebook.com/docs/whatsapp/business-management-api/authentication-templates) text in various language, based on parameters include in the request.

### Requirements

| Type | Description |
| --- | --- |
| Access Tokens | [User](https://developers.facebook.com/docs/whatsapp/business-management-api/get-started#user-access-tokens) or [System User](https://developers.facebook.com/docs/whatsapp/business-management-api/get-started#system-user-access-tokens) |
| Permissions | [whatsapp\_business\_management](https://developers.facebook.com/docs/permissions/reference/whatsapp_business_management) |