platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/message_templates/

# Whats App Business Account Message Templates

Represents a collection of templates on a [WhatsApp Business Account](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/). See [Templates](https://developers.facebook.com/docs/whatsapp/business-management-api/message-templates).

## Reading

Get a list of templates on a WhatsApp Business Account.

### Requirements

| Type | Description |
| --- | --- |
| Access Tokens | [User](https://developers.facebook.com/docs/whatsapp/business-management-api/get-started#user-access-tokens) or [System User](https://developers.facebook.com/docs/whatsapp/business-management-api/get-started#system-user-access-tokens) |
| Permissions | [whatsapp\_business\_management](https://developers.facebook.com/docs/permissions/reference/whatsapp_business_management) |