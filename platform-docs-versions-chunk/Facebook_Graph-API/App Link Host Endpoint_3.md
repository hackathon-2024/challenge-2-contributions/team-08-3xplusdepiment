platform: Facebook
topic: Graph-API
subtopic: App Link Host Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/App Link Host Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app-link-host

### Permissions

* Any valid access token is required.
    

### Response

| Name | Description | Type |
| --- | --- | --- |
| `name` | A custom name for this app link host. | `string` |
| `canonical_url` | The App Link URL. | `string` |

## Publishing

Apps can create app link hosts using the [`/app/app_link_hosts` edge](https://developers.facebook.com/docs/graph-api/reference/app/app_link_hosts).