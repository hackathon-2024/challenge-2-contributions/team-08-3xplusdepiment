platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app/staticresources

### Permissions

* A user access token for an admin of the app.
    

### Fields

| Name | Description | Type |
| --- | --- | --- |
| `usage_stats` | Static resource URLs with the fractions of page loads that used each resource. | `array` |
| `prefetched_resources` | Static resource URLs that are currently being flushed early for users who are using the app. | `array` |
| `https` | An object containing `usage_stats` and `prefetched_resources` again, only showing resources accessed via HTTPS. | `object` |

## Publishing

You can't publish using this edge.

## Deleting

You can't delete using this edge.

## Related Topics

* [Resource Prefetching via Facebook SDK for JavaScript](https://developers.facebook.com/docs/reference/javascript/FB.Canvas.Prefetcher.addStaticResource)
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)