platform: Facebook
topic: Graph-API
subtopic: Page Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Page Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/page/

# Page

Represents a [Facebook Page](https://www.facebook.com/help/282489752085908/).

Over the coming months, all classic Pages will be migrated to the New Pages Experience. Use the `has_transitioned_to_new_page_experience` Page field to determine if a Page has been migrated. After all Pages have been migrated, the classic Pages experience will no longer be available.

Refer to our [Pages API Guides](https://developers.facebook.com/docs/pages) for additional usage information.

## Reading

Get information about a Facebook Page.