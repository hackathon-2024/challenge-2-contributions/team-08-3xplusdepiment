platform: Facebook
topic: Graph-API
subtopic: Object Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Object Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/object/likes

### Permissions

* A Page access token requested by a person who can perform the [`MODERATE` task](https://developers.facebook.com/docs/pages/overview-1#tasks) on the Page
    
* The `pages_manage_engagement` permission
    

### Limitations

* A User or Page can only delete their own `likes`.
    
* The object must have already been liked.
    
* Deleting a Page review like is not supported.
    

### Fields

There are no fields for this endpoint.

### Response

On success, your app will receive the following response:

{
  "success": true
}

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)