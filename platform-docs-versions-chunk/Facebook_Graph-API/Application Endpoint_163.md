platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/application/uploads/

### Parameters

| Parameter | Description |
| --- | --- |
| `file_length`<br><br>int64 | The file length in bytes |
| `file_name`<br><br>RegexParam | The name of the file to be uploaded |
| `file_type`<br><br>RegexParam | The MIME type of the file to be uploaded |
| `session_type`<br><br>enum {attachment} | Default value: `attachment`<br><br>The type of upload session that is being requested by the app |

### Return Type

Struct {

`id`: string,

}

### Error Codes

| Error | Description |
| --- | --- |
| 100 | Invalid parameter |

## Updating

You can't perform this operation on this endpoint.

## Deleting

You can't perform this operation on this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)