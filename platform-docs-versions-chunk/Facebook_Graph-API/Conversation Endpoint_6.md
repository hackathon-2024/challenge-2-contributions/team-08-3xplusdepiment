platform: Facebook
topic: Graph-API
subtopic: Conversation Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Conversation Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/conversation

## Publishing

You can't publish using this edge.

Use the [Messenger Platform](https://developers.facebook.com/docs/messenger-platform/reference/send-api/) to send [Templates](https://developers.facebook.com/docs/messenger-platform/send-messages/templates), [Quick Replies](https://developers.facebook.com/docs/messenger-platform/send-messages/quick-replies), and more.

## Deleting

You can't delete using this edge.

## Updating

You can't update using this edge.

## Edges

| Name | Description | Used to Publish |
| --- | --- | --- |
| [`/messages`](https://developers.facebook.com/docs/graph-api/reference/conversation/messages/) | List of all messages in the conversation | Replies (by Pages only) |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)