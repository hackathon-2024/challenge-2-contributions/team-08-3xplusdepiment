platform: Facebook
topic: Graph-API
subtopic: Whats app business account Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Whats app business account Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/phone_numbers/

### Sample Response

{
  "id": "POST-ID"
}

You can make a POST request to `phone_numbers` edge from the following paths:

* [`/{whats_app_business_account_id}/phone_numbers`](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account/phone_numbers/)

When posting to this edge, a [WhatsAppBusinessPhoneNumber](https://developers.facebook.com/docs/graph-api/reference/whats-app-business-account-to-number-current-status/) will be created.