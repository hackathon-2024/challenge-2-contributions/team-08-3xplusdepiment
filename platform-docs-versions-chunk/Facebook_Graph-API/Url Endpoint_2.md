platform: Facebook
topic: Graph-API
subtopic: Url Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Url Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/url

### Parameters

Include the following query string parameters to augment the request.

| Parameter | Description |
| --- | --- |
| `access_token`<br><br>Required<br><br>_string_ | An [access token](#). |
| `fields`<br><br>_string_ | A comma-separated list of fields you want to request. |
| `id`<br><br>_string_ | The url to be shared. |
| `scopes`<br><br>_string_ | A comma-separated list of scopes. |