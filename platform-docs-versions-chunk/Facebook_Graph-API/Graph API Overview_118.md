platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/advanced/api-upgrade

# Upgrade to the Latest Graph API Version

This guide describes how to prepare your app to test different versions of the Graph API and to upgrade to the latest version.

The [API Upgrade Tool](https://developers.facebook.com/tools/api_versioning/) shows the API calls from your app that may be affected by changes in newer versions of the API. You will be able to quickly see which changes you need to make to upgrade from your current version to a newer version.