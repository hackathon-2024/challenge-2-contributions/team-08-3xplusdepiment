platform: Facebook
topic: Graph-API
subtopic: Application Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Application Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/app/translations

### Permissions

* An app access token is required to delete translation strings from that app.
    

### Fields

| Name | Description | Type |
| --- | --- | --- |
| `native_hashes` | An array of hashes for each translation string. The hash is a unique identifier for each string, and can be retrieved using the [`translation` FQL table](https://developers.facebook.com/docs/reference/fql/translation/). | `string[]` |

#### Response

If successful, you will receive a plain response of the number of strings that were deleted, otherwise an error message.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)