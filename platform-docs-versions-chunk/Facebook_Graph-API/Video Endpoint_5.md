platform: Facebook
topic: Graph-API
subtopic: Video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video/captions/

### Error Codes

| Error | Description |
| --- | --- |
| 482 | The captions files you selected contain locales that had been applied to video, please remove and try again. |
| 387 | There was a problem uploading your captions file. Please try again. |
| 386 | You uploaded a .SRT file with an incorrect file name. Please use this format: filename.en\_US.srt |
| 100 | Invalid parameter |
| 385 | The captions file you selected is in a format that we don't support. |

## Deleting

You can't perform this operation on this endpoint.

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)