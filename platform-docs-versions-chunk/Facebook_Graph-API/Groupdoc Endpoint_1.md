platform: Facebook
topic: Graph-API
subtopic: Groupdoc Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Groupdoc Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/groupdoc

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/groupdoc)

# Group Doc `/{group-doc-id}`

Represents a doc within a Facebook group. The `/{group-doc-id}` node returns a single doc.