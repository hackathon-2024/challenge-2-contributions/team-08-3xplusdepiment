platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/guides/versioning


### Version Schedules

Each version is guaranteed to operate for at least two years. **A version will no longer be usable two years after the date that the subsequent version is released.** For example, if API version v2.3 is released on March 25th, 2015 and API version v2.4 is released August 7th, 2015 then v2.3 would expire on August 7th, 2017, two years after the release of v2.4.

For APIs, once a version is no longer usable, any calls made to it will be defaulted to the next oldest, usable version. Here is a timeline example:

![](https://scontent-cdg4-3.xx.fbcdn.net/v/t39.2178-6/10333112_605697729514600_969655318_n.png?_nc_cat=106&ccb=1-7&_nc_ohc=g4MfdZw2Nv4AX-19p7a&_nc_ht=scontent-cdg4-3.xx&stp=dst-emg0_q75&ur=34156e&_nc_sid=a284aa&oh=00_AfCTBccsGHCK1XQIzBxCQA4WII8Y4QMl2Wje54eRSMDpfA&oe=65C0160B)

For SDKs, a version will always remain available as it is a downloadable package. However, the SDK may rely upon APIs or methods which no longer work, so you should assume an end-of-life SDK is no longer functional.

You can find specific information about our version timelines, changes, and release dates on our [changelog page](https://developers.facebook.com/docs/graph-api/changelog).