platform: Facebook
topic: Graph-API
subtopic: Games iap product Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Games iap product Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/games-iap-product/

### Parameters

This endpoint doesn't have any parameters.