platform: Facebook
topic: Graph-API
subtopic: Graph API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Graph API Overview.md
url: https://developers.facebook.com/docs/graph-api/get-started

### Step 1: Open the Graph API Explorer tool

[Open the Graph API Explorer in a new browser window.](https://developers.facebook.com/tools/explorer) This allows you to execute the examples as you read this tutorial.

The explorer loads with a default query with the `GET` method, the lastest version of the Graph API, the `/me` node and the `id` and `name` fields in the [Query String Field](https://developers.facebook.com/docs/graph-api/guides/explorer#query-string-field), and your Facebook App.

![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/232068365_563091814813799_6070357364579520404_n.png?_nc_cat=100&ccb=1-7&_nc_sid=e280be&_nc_ohc=odTlaHkpANQAX-44sG0&_nc_ht=scontent-cdg4-2.xx&oh=00_AfAaMKWtKHxR9Z8lZ4Si-LdypbLzhQdcpgITsy3VCH1Ecg&oe=65D568A4)