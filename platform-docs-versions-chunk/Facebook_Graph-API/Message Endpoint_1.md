platform: Facebook
topic: Graph-API
subtopic: Message Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Message Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/message

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/message)

# Message

An individual message in a Messenger or Instagram Messaging conversation.

To get the message ID use the [conversation endpoint](https://developers.facebook.com/docs/graph-api/reference/conversation) or [Webhooks](https://developers.facebook.com/docs/graph-api/webhooks/) to retrieve the individual message IDs.