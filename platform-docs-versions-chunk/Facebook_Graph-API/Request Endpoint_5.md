platform: Facebook
topic: Graph-API
subtopic: Request Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Request Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/request

## Publishing

You can't publish using this endpoint.

Requests are published via the [Game Request Dialog](https://developers.facebook.com/docs/games/requests). If your app is a **Game** you can publish app requests using the [`/{user-id}/apprequests` edge](https://developers.facebook.com/docs/graph-api/reference/user/apprequests/).