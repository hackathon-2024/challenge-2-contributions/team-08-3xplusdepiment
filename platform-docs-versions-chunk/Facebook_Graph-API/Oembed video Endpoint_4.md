platform: Facebook
topic: Graph-API
subtopic: Oembed video Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Oembed video Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/oembed-video/


### Fields

| Field | Description |
| --- | --- |
| `author_name`<br><br>string | author\_name<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `author_url`<br><br>string | author\_url<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `height`<br><br>int32 | height<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `html`<br><br>string | html<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `provider_name`<br><br>string | provider\_name<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `provider_url`<br><br>string | provider\_url<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `type`<br><br>string | type<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `version`<br><br>string | version<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `width`<br><br>int32 | width<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |