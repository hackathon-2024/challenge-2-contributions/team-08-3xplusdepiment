platform: Facebook
topic: Graph-API
subtopic: Video list Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Video list Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/video-list/

### Parameters

This endpoint doesn't have any parameters.

### Fields

| Field | Description |
| --- | --- |
| `id`<br><br>numeric string | Video playlist ID<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `creation_time`<br><br>datetime | The time when the playlist was created |
| `description`<br><br>string | Description of the playlist |
| `last_modified`<br><br>datetime | The time when the contents of the playlist was last changed |
| `owner`<br><br>User\|Page | Owner of the playlist |
| `season_number`<br><br>int32 | Number of Season which this episode belongs to |
| `thumbnail`<br><br>string | Thumbnail of the playlist |
| `title`<br><br>string | Title of the playlist<br><br>[Default](https://developers.facebook.com/docs/graph-api/using-graph-api/#fields) |
| `videos_count`<br><br>int32 | Number of Videos in the playlist |