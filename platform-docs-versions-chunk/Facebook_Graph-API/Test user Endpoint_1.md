platform: Facebook
topic: Graph-API
subtopic: Test user Endpoint
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Graph-API/Test user Endpoint.md
url: https://developers.facebook.com/docs/graph-api/reference/v18.0/test-user

This document refers to an outdated version of Graph API. Please [use the latest version.](https://developers.facebook.com/docs/graph-api/reference/v19.0/test-user)

# Test User /{test-user-id}

A test user associated with a Facebook app. Test users are created and associated using the [`/{app-id}/accounts/test-users`](https://developers.facebook.com/docs/graph-api/reference/app/accounts/test-users) edge or [in the App Dashboard](https://developers.facebook.com/docs/apps/test-users#managetool).

### Related Guides

* [Managing Test Accounts using the App Dashboard](https://developers.facebook.com/docs/apps/test-users#managetool)
    

## Reading

Permissions and fields for read operations on this node are [identical to those of the regular `/{user-id}` node](https://developers.facebook.com/docs/graph-api/reference/user#read).