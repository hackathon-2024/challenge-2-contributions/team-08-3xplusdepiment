platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### Integrate with Cloud

* **[Twitter API Toolkit for Google Cloud: Recent Search](https://developer.twitter.com/en/docs/tutorials/developer-guide--twitter-api-toolkit-for-google-cloud)** - Process, analyze and visualize Tweets using Google Cloud and Recent Search Twitter API v2
* **[Twitter API Toolkit for Google Cloud: Filtered Stream](https://developer.twitter.com/en/docs/tutorials/developer-guide--twitter-api-toolkit-for-google-cloud1)** - Listen to Tweets in real-time, detect and monitor trends using Google Cloud and the Filtered Stream API v2