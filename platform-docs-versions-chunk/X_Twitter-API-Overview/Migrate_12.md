platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/whats-new

###   
Track threaded conversations

We have introduced a new Tweet field to help you identify which conversation thread that Tweet belongs to. In addition to this, we launched a new filter operator that matches Tweets that share a common conversation ID, and is available for search Tweets, Tweet counts, and filtered stream. 

A conversation ID is the Tweet ID of the Tweet that started the conversation. 

Learn more about [conversation tracking](https://developer.twitter.com/en/docs/twitter-api/conversation-id).

[Ready to migrate?](https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate)