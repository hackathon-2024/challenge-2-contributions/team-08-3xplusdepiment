platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### Swift

**Clients**

**\--**

**SDKs / Libraries**

**[Swifter](https://github.com/mattdonnelly/Swifter)**

by [@mattdonnelly](https://github.com/mattdonnelly)

  

**Tools**

**\--**

### TypeScript

**Clients**

**\--**

**SDKs / Libraries**

**[twitter-api-ts](https://github.com/OliverJAsh/twitter-api-ts)**

by [@OliverJAsh](https://twitter.com/oliverjash)

**Tools**

**[twitter-d](https://github.com/abraham/twitter-d)**

TypeScript definitions for v1.1 Twitter API objects, by [@Abraham](https://twitter.com/Abraham)