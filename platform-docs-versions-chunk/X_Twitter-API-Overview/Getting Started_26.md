platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/make-your-first-request

### Step 3. Review the response

Once you’ve made a successful request, you will receive a payload with metadata related to the request. 

If you used an endpoint that utilizes a GET HTTP method, you will receive metadata related to the resource (Tweet, user, List, Space, etc) that you made the request to in JSON format. Review the different fields that returned and see if you can map the information that you requested to the content on Twitter.

If you used an endpoint that utilizes a POST, PUT, or DELETE HTTP method, you performed an action on Twitter. Go to Twitter.com or the mobile app and see if you can track down that action.