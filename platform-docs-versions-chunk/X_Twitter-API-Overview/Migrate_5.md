platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/whats-new


## New functionality

X API v2 also includes new features that will help you find more value with the X API. A lot of what is new has been driven by your feedback, and includes certain features that were reserved for enterprise customers previously. 

Some of the improvements to the API include:

* [A consistent design across endpoints](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/consistency)
* [The ability specify which fields and objects return in the response payload](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/data-dictionary/using-fields-and-expansions)
* [New and more detailed data objects](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/data-dictionary/introduction)
* [Receive and filter data with new contextual information powered by Tweet annotations](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/annotations/overview)
* [Access to new metrics](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/metrics)
* [Easily identify and filter for conversations that belong to a reply thread](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/conversation-id)
* [Advanced functionality and increased access to data for academic researchers](https://developer.twitter.com/content/developer-twitter/en/products/twitter-api/academic-research)
* [Recovery and redundancy functionality for streaming endpoints](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/filtered-stream/integrate/recovery-and-redundancy-features)
* [Easily return counts of Tweets that match a query](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/counts/introduction)
* [Support for Edit Tweets](https://developer.twitter.com/en/docs/twitter-api/edit-tweets)
* High confidence spam filtering
* Shortened URLs are fully unwound for more effective filtering and analysis 
* Simplified JSON response objects by removing deprecated fields and modernizing labels
* Return of 100% of matching public and available Tweets in search queries
* Streaming "rules" so you can make changes without dropping connections
* More expressive query language for search Tweets, Tweet counts, and filtered stream
* OpenAPI spec to build new libraries & more transparently track changes  
      
    

Before you dive in, let’s cover some of the fundamental changes we’ve made to the way you will access and consume data from the endpoints.