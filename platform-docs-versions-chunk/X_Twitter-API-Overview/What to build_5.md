platform: X
topic: Twitter-API-Overview
subtopic: What to build
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/What to build.md
url: https://developer.twitter.com/en/docs/twitter-api/what-to-build

### Improve community experiences

As new communities grow and convene on Twitter, we want developers to use their expertise to help serve the unique needs of these communities. Twitter is home of the global conversation, and we want everyone to be able to participate regardless of their language, timezone, cultural differences, or other localized needs. Additionally, we want growing communities around topics like finance or gaming to have tools built for their unique experiences and needs.