platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api

## Next steps

You can [sign up](https://developer.twitter.com/en/portal/products/basic) for the Twitter API, or review our [Getting access to the Twitter API](https://developer.twitter.com/en1/docs/twitter-api/getting-started/getting-access-to-the-twitter-api) guide for more details.

We’ve also put together a guide describing [how to make your first request](https://developer.twitter.com/en/docs/twitter-api/getting-started/make-your-first-request) to the Twitter API. 

If you have any feedback on these getting started resources, we’d love to hear from you!  
Please fill out the brief survey at the bottom of each page to help us improve.