platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

v1.1 tools and libraries

  
## Official v1.1 tools and libraries

The Twitter teams maintain a set of official libraries and SDKs, listed here. 

We also include a list of [community-supported libraries](#community-libraries) lower on this page. 

### JavaScript / Node.js

**Clients**

**\--**

**SDKs / Libraries**

**\--**

**Tools**

[**Autohook**](https://www.npmjs.com/package/twitter-autohook)

Get started with the Premium v1.1 Account Activity API

### Python

**Clients**

**[search-tweets-python](https://github.com/twitterdev/search-tweets-python)**

A client supporting v2, Premium v1.1 and Enterprise search

**SDKs / Libraries**

**\--**

**Tools**

**\--**

### Ruby

**Clients**

**[search-tweets-ruby](https://github.com/twitterdev/search-tweets-ruby)**

A client supporting v2, Premium v1.1 and Enterprise search

**SDKs / Libraries**

**\--**

**Tools**

**\--**