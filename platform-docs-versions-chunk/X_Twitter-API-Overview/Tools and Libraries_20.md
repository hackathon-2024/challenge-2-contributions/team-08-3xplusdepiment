platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### Clojure

**Clients**

**\--**

**SDKs / Libraries**

**[twitter-api](https://github.com/adamwynne/twitter-api)**

by [@adamjwynne](https://twitter.com/adamjwynne) / [@StreamScience](https://twitter.com/streamscience)

**[twttr](https://github.com/chbrown/twttr)**

by [@chbrown](https://github.com/chbrown/)

**Tools**

**\--**

### ColdFusion

**Clients**

**\--**

**SDKs / Libraries**

**[MonkehTweets](https://github.com/coldfumonkeh/monkehTweets)**

by [@coldfumonkeh](https://twitter.com/coldfumonkeh)

**Tools**

**\--**

### Dart

**Clients**

**\--**

**SDKs / Libraries**

**[dartTwitterAPI](https://github.com/stefanuros/dartTwitterAPI)**

by [@stefanuros](https://github.com/stefanuros)

**[twitter.dart](https://github.com/sh4869/twitter.dart)**

by [@sh4869sh](https://twitter.com/sh4869sh)

**[dart\_twitter\_api](https://github.com/robertodoering/twitter_api)**

by [@robertodoering](https://github.com/robertodoering)

**Tools**

**\--**