platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/whats-new


### V2 endpoints

You can see a full list of v2 endpoints and their pre-v2 equivalent via the following guide:

[X API Endpoint Map](https://developer.twitter.com/en/docs/twitter-api/migrate/twitter-api-endpoint-map)

  
While most of the endpoints in X API v2 are replacements, we have introduced several new endpoints. Here are several examples of new endpoints that we’ve released to v2:

* [Spaces endpoints](https://developer.twitter.com/en/docs/twitter-api/spaces/overview) to help people get more out of X Spaces, and to allow developers to help shape the future of audio conversations.
* [Hide replies](https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies), which allows you to build tools that help limit the impact of abusive, distracting, or misleading replies at scale – a crucial piece to improving the health of the public conversation on X, and ensuring brands and people feel comfortable starting conversations.
* New Lists endpoints that allow you to [pin and unpin Lists](https://developer.twitter.com/en/docs/twitter-api/lists/pinned-lists/introduction), or look up someone’s pinned Lists
* New [batch compliance endpoints](https://developer.twitter.com/en/docs/twitter-api/compliance/batch-compliance/introduction) that allow you to ensure your stored user and Tweet data is in compliance.