platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate

### Authentication

With the new Twitter API, you’ll use two different authentication patterns, [OAuth 1.0a User Context](https://developer.twitter.com/en/docs/authentication/oauth-1-0a) and [OAuth 2.0 Bearer Token](https://developer.twitter.com/en/docs/authentication/oauth-2-0), to access different endpoints. Each serves a different purpose when making requests to the endpoints: 

* OAuth 1.0a User Context is required when making a request on behalf of a Twitter user
* OAuth 2.0 Bearer token is required to make requests on behalf of your developer App