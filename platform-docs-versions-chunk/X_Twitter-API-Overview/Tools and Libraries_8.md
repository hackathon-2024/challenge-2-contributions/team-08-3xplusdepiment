platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### C# / .NET

* [**CoreTweet**](https://github.com/CoreTweet/CoreTweet) Yet Another .NET Twitter Library
* [**LinqToTwitter**](https://github.com/JoeMayo/LinqToTwitter) LINQ Provider for the Twitter API
* [**SocialOpinion**](https://github.com/jamiemaguiredotnet/SocialOpinion-Public) APIs written in C# that connect to the Twitter API
* [**Tweetinvi**](https://github.com/linvi/tweetinvi) an intuitive Twitter C# library
* [**TwitterSharp**](https://github.com/Xwilarg/TwitterSharp) C# wrapper around Twitter API V2

### Dart / Flutter

* [**twitter-api-v2**](https://github.com/twitter-dart/twitter-api-v2) a lightweight wrapper library for Twitter API v2.0 written in Dart and Flutter