platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/whats-new

What's new

What is the X API v2?  
## 

The X API v2 is now the primary X API, and is where product investment and innovation are focused. We’ve partnered with developers to build the next generation of the X API to better serve our diverse community of developers. Based on developer feedback, we’ve re-built the API to better serve a broader collection of needs, introduced new features and endpoints, and improved upon the developer experience.  

The X API v2 is now the primary X API, and is where product investment and innovation are focused. Over the past few years, we partnered with developers and re-built the API to better serve a broader collection of needs, introduce new features and endpoints, and improve upon the developer experience. We are committed to continuing to build an open developer platform, and are excited to see what you build with the X API v2.