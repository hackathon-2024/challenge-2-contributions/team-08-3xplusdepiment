platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### PHP

**Clients**

**\--**

**SDKs / Libraries**

**[twitter-labs](https://github.com/spatie/twitter-labs)**

by [@spatie\_be](https://twitter.com/spatie_be) (Labs)

**[TwitterOAuth](https://twitteroauth.com/)**

by [@Abraham](https://twitter.com/Abraham)

**[codebird-php](https://github.com/jublo/codebird-php)**

by [@jublo](https://twitter.com/jublo)

**[Twitter-API-PHP](https://github.com/J7mbo/twitter-api-php)**

by [@j7mbo](https://twitter.com/J7mbo/)

**[phirehose](https://github.com/fennb/phirehose)**

by [@fennb](https://twitter.com/fennb)

**[twitter-streaming-api](https://github.com/spatie/twitter-streaming-api)**

by [@spatie\_be](https://twitter.com/spatie_be)

**Tools**

**\--**

### PowerShell

**Clients**

**\--**

**SDKs / Libraries**

**\--**

**Tools**

**[PSTwitterAPI](https://github.com/mkellerman/PSTwitterAPI)**

by [@mkellerman](https://twitter.com/)