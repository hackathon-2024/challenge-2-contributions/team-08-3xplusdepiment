platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### Lua

**Clients**

**\--**

**SDKs / Libraries**

**[lua-twitter](https://github.com/leafo/lua-twitter)**

by [@moonscript](https://twitter.com/moonscript)

**Tools**

**\--**

### Objective-C

**Clients**

**\--**

**SDKs / Libraries**

**[STTwitter](https://github.com/nst/STTwitter)**

by [@nst021](https://twitter.com/nst021)

**Tools**

**\--**

### Perl

**Clients**

**\--**

**SDKs / Libraries**

**[Twitter::API](https://github.com/semifor/Twitter-API)**

by [@semifor](https://twitter.com/semifor)

**Tools**

**\--**