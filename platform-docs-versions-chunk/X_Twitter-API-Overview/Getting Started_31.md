platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/important-resources

### Keep up-to-date with the latest releases

We frequently release updates to the platform to unlock new Twitter functionality via the API. In addition to this, we aim to release at least one new major version bump per year, which will include some breaking changes. 

To receive the latest updates, please make sure to subscribe to our [forum’s Announcements category](https://twittercommunity.com/c/announcements/22), follow [@TwitterAPI](https://twitter.com/twitterapi) and [@TwitterDev](https://twitter.com/twitterdev) on Twitter and turn on notifications, and review our different resources via our [stay informed](https://developer.twitter.com/en/stay-informed) page. 

In addition to this, we have built out a new [migration hub](https://developer.twitter.com/en/docs/twitter-api/migrate) to help guide you through the process of updating to the latest version of the API.