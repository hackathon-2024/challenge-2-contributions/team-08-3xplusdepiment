platform: X
topic: Twitter-API-Overview
subtopic: What to build
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/What to build.md
url: https://developer.twitter.com/en/docs/twitter-api/what-to-build


### Impact the greater good

We’ve seen so many ways that developers have used Twitter to help make the world a better place. From groundbreaking research, to helpful bots, to other non-commercial innovation, developers never cease to amaze us. Below are just a few of the resources to help you get started: 

Relevant endpoints:

* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/introduction)
* [Conversation ID](https://developer.twitter.com/en/docs/twitter-api/conversation-id)
* [Tweet annotations](https://developer.twitter.com/en/docs/twitter-api/annotations/overview)
* [Search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search/introduction)
* [Tweet counts](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/introduction)
* [Filtered stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/introduction)
* [Sampled stream](https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/introduction)  
     

Success stories: 

* [UC Davis and Max Planck Institute](https://developer.twitter.com/en/community/success-stories/uc-davis-max-planck-institute)
* [Penn Medicine CDH](https://developer.twitter.com/en/community/success-stories/penn)
* [HateLab](https://developer.twitter.com/en/community/success-stories/hatelab)

  
Explore [different access levels](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api)