platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/make-your-first-request


### Step 4. Adjust your request using parameters

Each endpoint has a different set of parameters that you can use to alter your request. For example, you can request additional metadata fields when using GET endpoints with the fields and expansions parameters. You can also experiment with a variety of different filtering tools with endpoints such as [search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search), [Tweet counts](https://developer.twitter.com/en/docs/twitter-api/tweets/counts), and [filtered stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream) to help narrow down the data you receive to just those Tweets that you are interested in. 

You can find a full list of the request parameters and fields in the API Reference for the endpoint that you are working with, and a host of other helpful integration information in our integration guides and fundamentals pages. 

You can learn more about all of the educational materials we’ve made available to you via our [Important resources](https://developer.twitter.com/en/docs/twitter-api/getting-started/important-resources) guide.