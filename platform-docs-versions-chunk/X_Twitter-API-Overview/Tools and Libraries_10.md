platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### Java

* [**twittered**](https://github.com/redouane59/twittered) Twitter API client for Java developers
* [**twitter4j-v2**](https://github.com/takke/twitter4j-v2) a simple wrapper for Twitter API v2 that is designed to be used with Twitter4J
* [**twitter-compliance**](https://github.com/UCL/twitter-compliance) multi-module Jakarta EE application for syncing compliance events from Twitter
* [**JTW**](https://github.com/uakihir0/jtw) Twitter V2 API client library for Java