platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate

Ready to migrate?

## Ready to migrate?

In order to use v2 endpoints, you will need the following things:

* [A developer account](https://developer.twitter.com/en/portal/petition/essential/basic-info)
* [A developer App](https://developer.twitter.com/en/apps) created within a [Project](https://developer.twitter.com/en/docs/projects)
* [Keys and tokens](https://developer.twitter.com/en/docs/authentication) from that Project’s developer App  
      
    

Please note the importance of using keys and tokens from an App within a Project. If you are using keys and tokens from an App outside of a Project, you will not be able to make a request to v2 endpoints.

Once you have a developer account, you can set up all of the above in the [developer portal](https://developer.twitter.com/en/portal).