platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/important-resources

Important resources

## Learn about what's possible

Our use cases, solutions, and product pages that are accessible from the top navigation are a great way to get a high-level overview of what’s possible on the platform. 

We have also built out several [tutorials](https://developer.twitter.com/en/docs/twitter-api/tutorials) that help you identify how to use one or many different platform tools to satisfy a variety of different use cases. 

At the lowest-level of our documentation lives our endpoint and tool docs, which all include introductory content that provides a brief overview of that tool’s functionality and typical use case. For example, here is our Twitter API v2 endpoint section for [Search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search).