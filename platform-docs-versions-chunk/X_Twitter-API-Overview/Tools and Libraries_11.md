platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### JavaScript (Node.JS) / TypeScript

* [**node-twitter-api-v2**](https://github.com/PLhery/node-twitter-api-v2) strongly typed, full-featured, light, versatile yet powerful Twitter API client for Node.js
* [**twitter.js**](https://github.com/twitterjs/twitter.js) an object-oriented Node.js and TypeScript library for interacting with Twitter API v2
* [**twitter-types**](https://github.com/twitterjs/twitter-types) type definitions for the Twitter API
* [**twitter-v2**](https://github.com/HunterLarco/twitter-v2) An asynchronous client library for the Twitter APIs
* [**tweet-json-to-html**](https://github.com/wdl/tweet-json-to-html) converts Twitter API v2 Tweet JSON objects into HTML format