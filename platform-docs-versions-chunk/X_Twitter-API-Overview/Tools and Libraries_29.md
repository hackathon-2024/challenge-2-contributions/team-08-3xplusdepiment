platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### Rust

**Clients**

**\--**

**SDKs / Libraries**

**[egg-mode](https://github.com/QuietMisdreavus/twitter-rs)**

by [@QuietMisdreavus](https://twitter.com/QuietMisdreavus)

**[twitter-api-rs](https://github.com/gifnksm/twitter-api-rs)**

by [@gifnksm](https://twitter.com/gifnksm)

**[twitter-stream-rs](https://github.com/tesaguri/twitter-stream-rs)**

by [@teseguri](https://github.com/tesaguri)

**Tools**

**\--**

### Scala

**Clients**

**\--**

**SDKs / Libraries**

[**twitter4s**](https://github.com/DanielaSfregola/twitter4s)

by [@danielesfregola](https://twitter.com/danielasfregola)

**Tools**

**\--**

### Shell script (Bash)

**Clients**

[**tweet.sh**](https://github.com/piroor/tweet.sh)

A simple Twitter client by [@piroor](https://github.com/piroor). Requires additional command-line tools.

**SDKs / Libraries**

**\--**

**Tools**

**\--**