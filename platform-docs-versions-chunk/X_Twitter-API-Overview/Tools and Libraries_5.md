platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### v1.1 to v2 migration tools

* [**Migrate v1.1 search query to v2**](https://developer.twitter.com/apitools/migrate/query) - Convert your standard v1.1 search queries into the v2 search query format.
* [**Migrate v1.1 data format to v2**](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/migrate/data-formats/visual-data-format-migration-tool) - Visually explore the differences between the v1.1 and v2 data formats.

### Command-line interface (CLI)

* [**twurl**](https://github.com/twitter/twurl) - Interact with the Twitter API, including OAuth authentication, via your command-line interface. Requires a Ruby runtime. twurl does not currently work with OAuth 2.0 Authorization Code with PKCE.