platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api


## Twitter API platform resources

In the API design space, a resource is an entity with associated data, relationships to other resources, and a set of methods that operate on it. For example, a Tweet is a resource that you can create, delete, or retrieve using a variety of different tools, such as historically searching for them, or retrieving them in real-time. 

The Twitter API provides access to create, delete, receive, or adjust a variety of different resources on the platform including the following:

| Resource | Description |
| --- | --- |
| **Tweets** | Tap into millions of Tweets to understand the public conversation, or create your own to engage with the conversation.<br><br>Current availability:<br><br>* Twitter API v2<br>* Enterprise<br>* Premium v1.1<br>* Standard v1.1 |
| **Users** | Manage or look up Twitter users to analyze networks, understand your audience, or foster positive online relationships.<br><br>Current availability:<br><br>* Twitter API v2<br>* Enterprise<br>* Premium v1.1<br>* Standard v1.1 |
| **Spaces** | Look up and search Twitter Spaces and their attendees to help people find interesting and relevant audio conversations.<br><br>Current availability:<br><br>* Twitter API v2 |
| **Direct Messages** | Send and receive Direct Messages to triage customer issues, send welcome messages, or create positive human interaction.<br><br>Current availability:<br><br>* Standard v1.1 |
| **Lists** | Curate and manage lists of accounts to keep a pulse on industry experts, powerful voices, or organize who you follow.<br><br>Current availability:<br><br>* Twitter API v2<br>* Standard v1.1 |
| **Trends** | Identify geographic trends first to pinpoint industry movement, discover hot topics, or stay ahead of the latest fad.<br><br>Current availability:<br><br>* Standard v1.1 |
| **Media** | Upload media objects to share your creative energy, create interactive experiences, or build accessibility tools.<br><br>Current availability:<br><br>* Standard v1.1 |
| **Places** | Search for places to understand what’s happening in your neighborhood and around the world.<br><br>Current availability:<br><br>* Standard v1.1 |