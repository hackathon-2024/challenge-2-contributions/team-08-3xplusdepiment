platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api

## Twitter API v2

Twitter API v2 is ready for prime time! We recommend that the majority of developers start to think about migrating to v2 of the API, and for any new users to get started with v2. Why migrate?

[New and more detailed data objects](https://developer.twitter.com/en/docs/twitter-api/data-dictionary)

[New parameters to request objects and fields](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/using-fields-and-expansions)

[Advanced metrics](https://developer.twitter.com/en/docs/twitter-api/metrics)

[Receive and filter data with contextual Tweet annotations](https://developer.twitter.com/en/docs/twitter-api/annotations)

[Filter on and identify which Tweets belong to a reply thread](https://developer.twitter.com/en/docs/twitter-api/conversation-id)

[And much more...](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api#What%27s)

## Access Levels