platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/make-your-first-request


## Make your first request to the Twitter API

This guide will walk you through some steps that you could follow to make your first request. This is a great resource to help you once you’ve signed up for a Twitter account. 

If you are interested in using code samples, more technical guides, or a graphical tool like Postman, please consider using the following guides to make your first request:

* [Step-by-step guide to making your first request to the Twitter API](https://developer.twitter.com/content/developer-twitter/en/docs/tutorials/step-by-step-guide-to-making-your-first-request-to-the-twitter-api-v2)
* [A quick start guide to making your first request to the post Tweet endpoint](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/manage-tweets/quick-start)
* [Getting started with Postman](https://developer.twitter.com/content/developer-twitter/en/docs/tutorials/postman-getting-started) - best for beginners
* [Twitter API v2 code samples](https://github.com/twitterdev/Twitter-API-v2-sample-code)
* [Tweet lookup API reference](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference)  
      
    

This guide assumes that you have collected your [API key and secret](https://developer.twitter.com/en/docs/authentication/oauth-1-0a/api-key-and-secret), [user Access Token and Secret](https://developer.twitter.com/en/docs/authentication/oauth-1-0a/obtaining-user-access-tokens), [App Access Token](https://developer.twitter.com/en/docs/authentication/oauth-2-0/bearer-tokens), and have stored them in a secure location. You can learn how to do this by following the steps in the [getting access to the Twitter API](https://developer.twitter.com/en/docs/twitter-api/getting-started/getting-access-to-the-twitter-api) guide.