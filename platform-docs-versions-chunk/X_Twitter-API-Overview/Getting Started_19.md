platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/getting-access-to-the-twitter-api

Getting access to the Twitter API

## How to get access to the Twitter API

### Step one: Sign up for a developer account

Signing up for a developer account is quick and easy! Just click on the button below, answer a few questions, and you can start exploring and building on the Twitter API v2 using [Basic access](https://developer.twitter.com/en/portal/products/basic).

Next you will create a Project and an associated developer App during the onboarding process, which will provide you a set of credentials that you will use to authenticate all requests to the API. 

[Sign up](https://developer.twitter.com/en/portal/products/basic)