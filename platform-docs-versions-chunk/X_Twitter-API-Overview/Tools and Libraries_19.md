platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### (ASP, C#, VB)

**Clients**

**[ASPTwitter](http://www.timacheson.com/Blog/2013/jun/asptwitter)**

by [@timacheson](https://twitter.com/timacheson)

**SDKs / Libraries**

**[LINQ2Twitter](https://github.com/JoeMayo/LinqToTwitter)**

by [@JoeMayo](https://twitter.com/JoeMayo) (v2)

**[SocialOpinion](https://github.com/jamiemaguiredotnet/SocialOpinion-Public)**

by [@jamie\_maguire1](https://twitter.com/jamie_maguire1) (v2)

**[Tweetinvi](https://github.com/linvi/tweetinvi)**

by [@TweetinviApi](https://twitter.com/TweetinviApi) (v2)

**[CoreTweet](https://coretweet.github.io/)**

by [@CoreTweet](https://github.com/CoreTweet/) (v2)

**Tools**

**\--**

### C++

**Clients**

**\--**

**SDKs / Libraries**

**[Twitter-API-C-Library](https://github.com/a-n-t-h-o-n-y/Twitter-API-C-Library)**

by [@a-n-t-h-o-n-y](https://github.com/a-n-t-h-o-n-y)

**Tools**

**\--**