platform: X
topic: Twitter-API-Overview
subtopic: What to build
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/What to build.md
url: https://developer.twitter.com/en/docs/twitter-api/what-to-build

### Curate and recommend content

Developers have always come up with creative ways to connect people on Twitter to content they’re interested in. Whether it’s apps, bots, or other tools that help people have more curated or customized experiences, this is an opportunity for developers to connect people to the content they care most about. Get started: 

Relevant endpoints:

* [Spaces](https://developer.twitter.com/en/docs/twitter-api/spaces/overview)
* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/introduction)
* [Lists](https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/introduction)  
     

Sample apps:

* [Spaces Search](https://developer.twitter.com/en/community/success-stories/spaces-search)