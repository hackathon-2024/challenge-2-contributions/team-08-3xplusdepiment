platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### Go

**Clients**

**[twty](https://github.com/mattn/twty)**

A command-line client tool written in Go, by [@mattn](https://github.com/mattn)

**SDKs / Libraries**

**[go-twitter](https://github.com/g8rswimmer/go-twitter)**

by [@g8rswimmer](https://github.com/g8rswimmer) (v2)

**[twittergo](https://github.com/kurrik/twittergo)**

by [@kurrik](https://twitter.com/kurrik)

**[go-twitter](https://github.com/dghubble/go-twitter)**

by [@dghubble](https://twitter.com/dghubble)

**[Anaconda](https://github.com/ChimeraCoder/anaconda)**

by [@chimeracoder](https://twitter.com/chimeracoder)

**Tools**

**[tw-oob-oauth-cli](https://github.com/smaeda-ks/tw-oob-oauth-cli)**

A small PIN-based OAuth tool to retrieve tokens, by [@smaeda-ks](https://github.com/smaeda-ks)