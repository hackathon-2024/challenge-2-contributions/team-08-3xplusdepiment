platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api

### Filter on and identify which Tweets contain different topics

When using search Tweets or filtered stream, you can now filter by topic using our entity and context operators. We’ve also provided these topics within the Tweet payload to help with analysis. 

[Learn more about Tweet annotations >](https://developer.twitter.com/en/docs/twitter-api/annotations)  
 

### FIlter on and identify which Tweets belong to a reply thread

Make it easier to identify a Tweet as part of a conversation thread when using search Tweets, filtered stream, and Tweet lookup. We've also added the ability to determine whether conversation reply settings have been set for a Tweet with the Tweet field reply\_settings.

[Learn more about conversation tracking >](https://developer.twitter.com/en/docs/twitter-api/conversation-id)