platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api

## Migrate to Twitter API v2

Interested in migrating your current integration to Twitter API v2? Check out our migration hub for resources that will help you understand what is different between v2 and previous versions, including the data formats. You can also access migration guides for each endpoint listed in the new v2 endpoint sections.  
 

[Learn more](https://developer.twitter.com/en/docs/twitter-api/migrate)

[Twitter API endpoint map](https://developer.twitter.com/en/docs/twitter-api/migrate/twitter-api-endpoint-map)