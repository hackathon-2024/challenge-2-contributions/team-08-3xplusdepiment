platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/whats-new

## Why migrate?

The X API v2 is built with a modern and more sustainable foundation and includes both improved replacement endpoints for the standard v1.1, and enterprise products, but also net-new functionality. We strongly encourage customers of legacy APIs (v1.1, and enterprise) to begin to migrate to v2 as we do intend to deprecate them eventually. Use the X API to listen to and analyze the public conversation, engage with people on X, and innovate.  

In this section, we will discuss the endpoints and functionality.