platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/important-resources

### Troubleshoot an issue

If you are running into an error or have a question, please check out our [support hub](https://developer.twitter.com/en/support/twitter-api). There, you will find troubleshooting tips, answers to frequently asked questions and other useful resources.