platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### Kotlin

* [**KTweet**](https://github.com/ChromasIV/KTweet) a Kotlin library that allows you to consume the Twitter API v2.
* [**Tweedle**](https://github.com/tyczj/Tweedle) a Kotlin-based Android library around the Twitter v2 API
* [**TwitterApiKit**](https://github.com/kojofosu/TwitterApiKit) saves you time creating data objects to access Twitter's API v2. This library is supported on Java, Kotlin, and Android