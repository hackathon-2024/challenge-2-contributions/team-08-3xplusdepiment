platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### R

**Clients**

**\--**

**SDKs / Libraries**

**[rtweet](https://docs.ropensci.org/rtweet/)**

by [@kearnymw](https://twitter.com/kearneymw). Includes Premium v1.1 search support.

**[streamr](https://github.com/pablobarbera/streamR)**

by [@p\_barbera](https://twitter.com/p_barbera)

**Tools**

**[rtweetXtras](https://github.com/Arf9999/rtweetXtras)**

Helper functions for rTweet, by [@Arf9999](https://github.com/Arf9999)

**[tweetrmd](https://github.com/gadenbuie/tweetrmd)**

Easily embed Tweets into R Markdown, by [@gadenbuie](https://github.com/gadenbuie)

### Ruby

**Clients**

**[t](https://github.com/sferik/t)**

A command-line tool by [@sferik](https://twitter.com/sferik)

**SDKs / Libraries**

[**twitter-labs-api**](https://github.com/tomholford/twitter-labs-api)

by [@tomholford](https://twitter.com/tomholford) (supports Labs)

[**twitter**](https://github.com/sferik/twitter)

by [@sferik](https://twitter.com/sferik)

**Tools**

**\--**