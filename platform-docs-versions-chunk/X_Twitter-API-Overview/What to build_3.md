platform: X
topic: Twitter-API-Overview
subtopic: What to build
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/What to build.md
url: https://developer.twitter.com/en/docs/twitter-api/what-to-build


### Enable creation and personal expression

Twitter is where people come to share their perspective, ideas, and passions. It’s the convergence of individuals and brands, founders and athletes, celebrities and CEOs. Developers have an opportunity to build creative solutions that allow people on Twitter to expand their reach, express themselves in new ways, and connect with like minded communities. As creators and innovators themselves, developers can also build things that broaden how people experience Twitter like helpful bots, gamification, or cross-posted content. Below are a few ideas to seed your innovation: 

Relevant endpoints:

* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/introduction)
* [Spaces](https://developer.twitter.com/en/docs/twitter-api/spaces/overview)
* [Retweets](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/introduction)  
     

Sample apps:

* [Spaces Reach](https://github.com/twitterdev/spaces-reach)
* [FactualCat Twitter bot](https://github.com/twitterdev/FactualCat-Twitter-Bot/)