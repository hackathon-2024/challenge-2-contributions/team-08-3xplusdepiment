platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate

## What’s next?

Those of you who have used the platform for some time will notice that many of the new endpoints are aligned with existing [standard v1.1](https://developer.twitter.com/en/docs/twitter-api/v1), and [enterprise](https://developer.twitter.com/en/docs/twitter-api/enterprise) endpoints. Indeed, we intend for these to replace all three versions in the future. 

We’ve put together a table to help you understand how the [Twitter API endpoints map](https://developer.twitter.com/en/docs/twitter-api/migrate/twitter-api-endpoint-map) to previous versions.

If you’d like to see what’s coming next, please visit our [product roadmap](https://trello.com/b/myf7rKwV/twitter-developer-platform-roadmap).

We also have a [changelog](https://developer.twitter.com/en/updates/changelog) that you can check out to understand what we have already released.