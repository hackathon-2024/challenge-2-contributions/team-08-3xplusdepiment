platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate

### What should we build next?

As we build out additional capabilities of the Twitter API v2 we want to continue to hear from you. We welcome and encourage [feedback](https://twitterdevfeedback.uservoice.com/) from you. 

Take a look at the ideas that have already been submitted, show your support for those that correlate with your needs and provide feedback as well!