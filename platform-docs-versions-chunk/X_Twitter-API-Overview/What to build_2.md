platform: X
topic: Twitter-API-Overview
subtopic: What to build
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/What to build.md
url: https://developer.twitter.com/en/docs/twitter-api/what-to-build


### Moderate conversations for health and safety

We want everyone to feel comfortable, safe, and excited to participate in the conversation on Twitter. We’ve worked hard to offer tools that give people more control over their experience, like Tweet reply settings, blocks, and mutes. However, there will always be more work to do, and we want to encourage you to build complementary or additive solutions that help improve the health and safety of the platform. Check out some of our resources below to help you get started:

Relevant endpoints: 

* [Follows](https://developer.twitter.com/en/docs/twitter-api/users/follows/introduction)
* [Blocks](https://developer.twitter.com/en/docs/twitter-api/users/blocks/introduction)
* [Mutes](https://developer.twitter.com/en/docs/twitter-api/users/mutes/introduction)
* [Hide replies](https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/introduction)
* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/introduction) (Tweet reply settings)  
     

Sample apps:  

* [Hide replies with Perspective API](https://glitch.com/edit/#!/twitter-hide-replies-perspective-api)
* [Hide replies with Tweet Annotations](https://glitch.com/edit/#!/twitter-hide-replies-annotations-v2)  
     

Success stories: 

* [Block Party](https://developer.twitter.com/en/community/success-stories/tracy-chou-block-party)