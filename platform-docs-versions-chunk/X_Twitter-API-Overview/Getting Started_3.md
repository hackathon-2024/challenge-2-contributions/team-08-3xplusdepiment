platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api

### **Free**  

For write-only use cases and testing the Twitter API

* Low rate-limit access to v2 tweet posting and media upload endpoints
    
* 1,500 Tweets per month - posting limit at the app level
    

* 1 Project

* 1 App per Project
* 1 Environment (Development/ Production/ Staging)

* Login with Twitter
* Access to Ads API

* Cost: Free
    

### **Basic**  

For hobbyists or prototypes

* Low-rate limit access to suite of v2 endpoints
    
* 3,000 Tweets per month - posting limit at the user level
    
* 50,000 Tweets per month - posting limit at the app level
    
* 10,000/month Tweets read-limit rate cap
    
* 1 Project
* 2 Apps per Project with unique Environment (Development/ Production/ Staging)
* Login with Twitter
* Access to Ads API
* Cost: $100 per month