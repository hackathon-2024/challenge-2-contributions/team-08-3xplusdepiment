platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/make-your-first-request

### Step 1. Identify which endpoint you would like to use

The Twitter API allows you to perform a variety of different actions using code that you might be able to perform on the Twitter website or mobile application. 

We have a complete list of the endpoints that are available via the API listed within our [API Reference Index](https://developer.twitter.com/en/docs/api-reference-index), but we recommend sticking to one of the following for simplicity’s sake:

* [Manage Tweets > Post a Tweet](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets)
* [Search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search)
* [User lookup](https://developer.twitter.com/en/docs/twitter-api/users/lookup)