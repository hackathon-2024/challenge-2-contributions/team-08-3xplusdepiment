platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### R

* [**academictwitteR**](https://github.com/cjbarrie/academictwitteR) R package to query the Twitter Academic Research Product Track v2 API endpoint
* [**RTwitterV2**](https://github.com/MaelKubli/RTwitterV2) R functions for Twitter's v2 API

### Ruby

* [**omniauth-twitter2**](https://github.com/unasuke/omniauth-twitter2) OmniAuth strategy for authenticating with Twitter OAuth2
* [**tweetkit**](https://github.com/julianfssen/tweetkit) Twitter v2 API client for Ruby
* [**twitter\_oauth2**](https://github.com/nov/twitter_oauth2) Twitter OAuth 2.0 Client Library in Ruby

### Rust

* [**twitter-v2**](https://github.com/jpopesculian/twitter-v2-rs) Rust bindings for Twitter API v2

### Swift

* [**Twift**](https://github.com/daneden/Twift/) An async Swift library for the Twitter v2 API
* [**TwitterAPIKit**](https://github.com/mironal/TwitterAPIKit) Swift library for the Twitter API v1 and v2