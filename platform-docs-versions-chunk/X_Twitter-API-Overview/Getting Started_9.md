platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api

About the Twitter API

## About the Twitter API

The Twitter API can be used to programmatically retrieve and analyze Twitter data, as well as build for the conversation on Twitter.

Over the years, the Twitter API has grown by adding additional levels of access for developers and academic researchers to be able to scale their access to enhance and research the public conversation. 

Recently, we released the Twitter API v2. The Twitter API v2 includes a modern foundation, new and advanced features, and quick onboarding to [Basic access](https://developer.twitter.com/en/portal/products/basic).   

The following three tabs explain the different versions and access levels of the Twitter API, what’s new with v2, and which Twitter resources you can retrieve, create, destroy, and adjust using the API.   

  

* [Access levels and versions](#item0)
* [What's new with v2](#item1)
* [Twitter API resources](#item2)

Access levels and versions

What's new with v2

Twitter API resources