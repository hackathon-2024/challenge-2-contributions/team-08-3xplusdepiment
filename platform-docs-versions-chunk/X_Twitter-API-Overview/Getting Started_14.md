platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api

### New parameters to help you retrieve just those objects and fields that you want

We’ve added fields and expansions parameters to our data endpoints that allow you to request related objects and fields beyond those fields that return by default.

[Learn how to use fields and expansions >](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/using-fields-and-expansions)  
 

### Advanced metrics

More easily understand the performance of Tweets, users, media, and polls from directly within your payload by requesting both public and private metrics including impressions, video views, user profile, and URL clicks, some of which are separated into an organic and promoted context.

[Learn more about metrics >](https://developer.twitter.com/en/docs/twitter-api/metrics)