platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api

Twitter API

The Twitter API enables programmatic access to Twitter in unique and advanced ways. Tap into core elements of Twitter like: Tweets, Direct Messages, Spaces, Lists, users, and more.  
 

[Subscribe to Pro access](https://developer.twitter.com/en/portal/products/basic)

[API access levels and versions](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api)

[Try a live request](https://oauth-playground.glitch.me/?id=createTweet&compose=1)