platform: X
topic: Twitter-API-Overview
subtopic: What to build
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/What to build.md
url: https://developer.twitter.com/en/docs/twitter-api/what-to-build

What to build

## Introduction

Our developer community has unique skills, experiences, and perspectives that can fill gaps, solve problems, and seed new innovation on Twitter well beyond what we at Twitter can do on our own. We encourage you to build tools and products that make Twitter better, healthier, and extend the public conversation. In particular, we want to see innovation in the following categories:

* [Moderate conversations for health and safety](#moderate)
* [Enable creation and personal expression](#enable)
* [Measure and analyze what’s happening](#measure)
* [Improve community experiences](#improve)
* [Curate and recommend content](#curate)
* [Impact the greater good](#impact)