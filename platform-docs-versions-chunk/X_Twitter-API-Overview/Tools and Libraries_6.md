platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### Additional tools

* [**twemoji**](https://twemoji.twitter.com/) - Twitter’s free, open source emoji character set, including a JavaScript library for display.
* [**twitter-text**](https://github.com/twitter/twitter-text) - A collection of libraries to standardize parsing and tokenization of Tweet text. Available for Java, JavaScript (Node.JS), Objective-C & Ruby. [Learn more about counting characters in Tweets](https://developer-staging.twitter.com/en/docs/counting-characters).

Looking for even more code? You can find examples from our team on our [GitHub](https://github.com/twitterdev), and on [Glitch](https://glitch.com/@twitter).