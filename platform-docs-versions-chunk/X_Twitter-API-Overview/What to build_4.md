platform: X
topic: Twitter-API-Overview
subtopic: What to build
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/What to build.md
url: https://developer.twitter.com/en/docs/twitter-api/what-to-build


### Measure and analyze “what’s happening”

Many people come to Twitter because of the opportunity it presents to reach new or existing audiences, amplify their voice, or have an impact. Developers can help these creators, artists, businesses, and local advocates measure, analyze, or understand the impact of their content. Additionally, developers can build new solutions that help people derive insights from the public conversation about trends, their audience, and so much more. We’ve put together a few resources below to help you get started: 

Relevant endpoints/functionality: 

* [Object model](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet) (Tweets, Users, Spaces, etc)
* [Advanced metrics](https://developer.twitter.com/en/docs/twitter-api/metrics)
* [Tweet annotations](https://developer.twitter.com/en/docs/twitter-api/annotations/overview)
* [Search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search/introduction)
* [Tweet counts](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/introduction)
* [Filtered stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/introduction)
* [Sampled stream](https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/introduction)  
     

Tutorials:

* [Listen for important events](https://developer.twitter.com/en/docs/tutorials/listen-for-important-events)
* [Analyze past conversations](https://developer.twitter.com/en/docs/tutorials/analyze-past-conversations)
* [Analyze the sentiment of your own Tweets](https://developer.twitter.com/en/docs/tutorials/how-to-analyze-the-sentiment-of-your-own-tweets)  
     

Success stories:   

* [Audience](https://developer.twitter.com/en/community/success-stories/audiense)
* [TweepsMap](https://developer.twitter.com/en/community/success-stories/samir-al-battran-tweeps-map)