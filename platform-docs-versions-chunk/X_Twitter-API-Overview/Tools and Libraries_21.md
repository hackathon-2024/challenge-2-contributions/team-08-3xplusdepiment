platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v1

### Elixir

**Clients**

**\--**

**SDKs / Libraries**

**[extwitter](https://github.com/parroty/extwitter)**

by [@parroty](https://github.com/parroty)

**Tools**

**\--**

### Erlang

**Clients**

**\--**

**SDKs / Libraries**

**[ErlyBird](https://github.com/igb/ErlyBird)**

by [@igb](https://twitter.com/igb)

**[yatael](https://github.com/tgrk/yatael)**

by [@tajgur](https://twitter.com/tajgur)

**Tools**

**\--**