platform: X
topic: Twitter-API-Overview
subtopic: Tools and Libraries
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Tools and Libraries.md
url: https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2

### PHP

* [**bird-elephant**](https://github.com/danieldevine/bird-elephant) PHP client library for the Twitter API v2 endpoints
* [**twifer**](https://github.com/ferrysyahrinal/twifer) Simple PHP Library for Twitter API Standard v1.1 & Twitter API v2
* [**twitter-api-v2-php**](https://github.com/noweh/twitter-api-v2-php) PHP package providing easy and fast access to Twitter API V2
* [**twitteroauth**](https://github.com/abraham/twitteroauth) PHP library for use with the Twitter API
* [**twitter-ultimate-php**](https://github.com/utxo-one/twitter-ultimate-php) PHP Wrapper for the Twitter v2 API
* [**Twitter Stream API**](https://github.com/redwebcreation/twitter-stream-api) consume the Twitter Stream API v2 in real-time

### PowerShell

* [**BluebirdPS**](https://github.com/thedavecarroll/BluebirdPS) a Twitter Automation Client for PowerShell 7. Tweet, Retweet, send Direct Messages, manage lists, and more