platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/overview

Overview

## Overview

The latest version of the Twitter API v2 is a big deal. As such, we’ve broken this migration section into a few partitions:

|     |     |
| --- | --- |
| **[What’s new with Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/migrate/whats-new)** | Learn about the new endpoints and functionality that we’ve released to Twitter API v2. |
| **[Ready to migrate?](https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate)** | Get started with your migration with a set of guides and instructions. |
| **[Data format migration guide](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats)** | Learn how to rework your data parsers that previously worked with the standard v1.1, and enterprise data formats. |
| **[Twitter API endpoint map](https://developer.twitter.com/en/docs/twitter-api/migrate/twitter-api-endpoint-map)** | See how standard v1.1, and enterprise endpoints map to the new Twitter API v2 endpoints. |