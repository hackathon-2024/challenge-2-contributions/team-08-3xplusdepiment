platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate


### Migrating to the new data format

As you move from v1.1 or enterprise to v2, it is important to understand that the format the data are delivered in has changed significantly. We have added new fields, modified the sequence of fields, and in some cases removed elements altogether. 

To learn more about these changes, we are developing a series of guides that will help you map out the pre-v2 data format fields to the newer fields, and describe how to request these new fields. 

You can learn more by visiting our [data formats migration](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats) section of this migration hub, or by visiting our specific data format guides:  

* [Native format to Twitter API v2 (standard v1.1)](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/standard-v1-1-to-v2) 
* [Native Enriched to Twitter API v2 (enterprise)](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/native-enriched-to-v2)
* [Activity Streams to Twitter API v2 (enterprise)](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/activity-streams-to-v2)