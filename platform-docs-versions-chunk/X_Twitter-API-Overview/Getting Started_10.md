platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api


## Twitter API access levels and versions

While the Twitter API v2 is the primary Twitter API, the platform currently supports previous versions (v1.1, Gnip 2.0) as well. We recommend that all users start with v2 as this is where all future innovation will happen. 

The Twitter API v2 includes a few access levels to help you scale your usage on the platform. In general, new accounts can quickly sign up for Basic access. Should you want additional access, you may choose to apply for Enterprise access.   
 

|     | **Free** | **Basic** | **Pro** | **Enterprise** |
| --- | --- | --- | --- | --- |
| **Getting access** | [Get Started](https://developer.twitter.com/en/portal/products/free) | [Get Started](https://developer.twitter.com/en/portal/products/basic) | [Get Started](https://developer.twitter.com/en/portal/products/basic) | [Get Started](https://docs.google.com/forms/d/e/1FAIpQLScO3bczKWO2jFHyVZJUSEGfdyfFaqt2MvmOfl_aJp0KxMqtDA/viewform) |
| **Price** | Free | $100/month | $5000/month |
| **Access to Twitter API v2** | ✔️ (Only Tweet creation) | ✔️  | ✔️  |
| **Access to standard v1.1** | ✔️(Only Media Upload, Help, Rate Limit, and Login with Twitter) | ✔️(Only Media Upload, Help, Rate Limit, and Login with Twitter) | ✔️(Only Media Upload, Help, Rate Limit, and Login with Twitter) |
| **Project limits** | 1 Project | 1 Project | 1 Project |
| **App limits** | 1 App per Project | 2 Apps per Project | 3 Apps per Project |
| **Tweet caps - Post** | 1,500 | 3,000 | 300,000 |
| **Tweet caps - Pull** | ❌   | 10,000 | 1,000,000 |
| **Filteres stream API** | ❌   | ❌   | ✔️  |
| **Access to full-archive search** | ❌   | ❌   | ✔️  |
| **Access to Ads API** | ✔️  | ✔️  | ✔️  |