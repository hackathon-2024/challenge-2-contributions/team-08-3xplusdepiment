platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api

## What's new with v2

The Twitter API v2 represents the largest upgrade of the Twitter API since 2012. With it comes a host of new and advanced features, as well as fast and free access to the API. 

Some of the features that are available with v2 include the following:  
  

### New endpoints

We have released a set of net-new endpoints to Twitter API v2. You can see a full list of v2 endpoints, including those that are new, on our Twitter API endpoint map guide.

[Visit the Twitter API endpoint map >](https://developer.twitter.com/en/docs/twitter-api/migrate/twitter-api-endpoint-map)

###   
New and more detailed data objects

We've modernized our data objects with a variety off new improvements that will enable you to more easily navigate and parse data.

[](https://developer.twitter.com/en1/docs/twitter-api/data-dictionary)[Visit the data dictionary >](https://developer.twitter.com/en/docs/twitter-api/data-dictionary)