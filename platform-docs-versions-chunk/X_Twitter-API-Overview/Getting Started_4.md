platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api

### **Pro**  

For startups scaling their business

* Rate-limited access to suite of v2 endpoints, including search and  filtered stream
    
* 1,000,000 Tweets per month - GET at the app level
    
* 300,000 Tweets per month - posting limit at the app level
    
* 1 Project
* 3 Apps per Project with unique Environment (Development/ Production/ Staging)

* Login with Twitter
* Access to Ads API

* $5,000 per month
    

### **Enterprise**  

For businesses and scaled commercial projects

* Commercial-level access that meets your and your customer's specific needs
    
* Managed services by a dedicated account team
    
* Complete streams: replay, engagement metrics, backfill, and more features
    
* Cost: Monthly subscription tiers
    

[More on v2 access levels](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api#v2-access-leve)