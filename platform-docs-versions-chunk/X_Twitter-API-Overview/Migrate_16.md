platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate


### Migrating to updated endpoints

As you start to explore the new Twitter v2 endpoints, we’ve built a series of detailed migration guides to help you compare and contrast each updated endpoints' capabilities compared to older versions:

* Tweets
* [Tweets lookup](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/migrate)
* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/migrate)
[](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/migrate)* [](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/migrate)[](https://developer.twitter.com/en/docs/twitter-api/tweets/search/migrate)[Timelines](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/migrate)
* [Search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search/migrate)
* [Tweet counts](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/migrate)
* [Filtered stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/migrate)
* [Sampled stream](https://developer.twitter.com/en/docs/twitter-api/tweets/sampled-stream/migrate)
* [Retweets](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate)
* [Likes](https://developer.twitter.com/en/docs/twitter-api/tweets/likes/migrate)
* [Hide replies](https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/migrate)
* Users
* [Users lookup](https://developer.twitter.com/en/docs/twitter-api/users/lookup/migrate)
* [Follows](https://developer.twitter.com/en/docs/twitter-api/users/follows/migrate)
* [Blocks](https://developer.twitter.com/en/docs/twitter-api/users/blocks/migrate)
* [Mutes](https://developer.twitter.com/en/docs/twitter-api/users/mutes/migrate)            
* Lists
* [List lookup](https://developer.twitter.com/en/docs/twitter-api/lists/list-lookup/migrate)
* [Manage Lists](https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/migrate)
* [List Tweet lookup](https://developer.twitter.com/en/docs/twitter-api/lists/list-tweets/migrate)
* [List members](https://developer.twitter.com/en/docs/twitter-api/lists/list-members/migrate)
* [List follows](https://developer.twitter.com/en/docs/twitter-api/users/follows/migrate)