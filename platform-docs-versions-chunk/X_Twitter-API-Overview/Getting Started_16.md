platform: X
topic: Twitter-API-Overview
subtopic: Getting Started
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Getting Started.md
url: https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api

### And so much more...

* High confidence spam filtering
* Shortened URLs are fully unwound for easier URL analysis
* Simplified JSON response objects by removing deprecated fields and modernizing labels
* Recovery and redundancy functionality for our streaming endpoints
* Return of 100% of matching public and available Tweets in search queries
* Streaming "rules" so you can make changes without dropping connections
* More expressive query language for filtered stream and search
* OpenAPI spec to build new libraries & more transparently track changes
* API support for new features and endpoints more quickly as our platform evolves to meet the needs of developers, researchers, businesses, and people using Twitter