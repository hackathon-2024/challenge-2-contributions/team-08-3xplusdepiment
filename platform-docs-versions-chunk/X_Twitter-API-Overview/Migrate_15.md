platform: X
topic: Twitter-API-Overview
subtopic: Migrate
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-Overview/Migrate.md
url: https://developer.twitter.com/en/docs/twitter-api/migrate/ready-to-migrate

### Tools and Code 

To help you get started and familiarize yourself with the new endpoints and capabilities we have a few options to jump start your work:

First, we have a Twitter [Postman collection](https://t.co/twitter-api-postman) that allows you to use the Postman client to make requests of and connect to the individual endpoints. This is a low friction way to test authentication and experiment with the endpoints. It’s important to note the Postman client is best for RESTful endpoints, but you can copy requests to streaming endpoints from the tool and paste them into your command line tool.

If you want to dig deeper, we’ve also provided a list of both Twitter supported and third party libraries in Ruby, Python, Node, Java, and many more. For additional context, take a look at our [tools and libraries page](https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries).