platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/sample-applications

### Next Steps

If you are interested in exploring the sample application for Marketing, see [Marketing Sample Application](https://learn.microsoft.com/en-us/linkedin/marketing/sample-apps-lms).