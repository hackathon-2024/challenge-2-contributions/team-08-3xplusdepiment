platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/postman-getting-started

### Step 8 - Testing

Finally, send a request within the Use Cases folder. Ensure the correct environment is selected and that if any environment or collection level variables are being used in the request, ensure they are set. For example, in the screenshot below, the request uses the `sponsoredaccount_id` variable from the `campaign-management-env` environment.

Learn more about Postman variables in [Postman's online documentation](https://learning.postman.com/docs/sending-requests/variables/#variable-scopes)

Note that some requests dynamically set variables via a script that runs post request execution. You will know if a script is set to run for a request if there is a green dot next to the Tests tab.

To see an example sample response, view the saved example.