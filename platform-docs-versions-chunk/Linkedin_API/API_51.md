platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/error-handling?context=linkedin%2Fcontext

## 500 Internal Server Error

A `500 Internal Server Error` indicates that LinkedIn is experiencing an internal error. If you continue to receive server errors, record the following details and report it to your partner technical support channel or [https://developer.linkedin.com/support](https://developer.linkedin.com/support):

* Request: `url`, `method`, `header`. For example, `access_token`, `body`.
* Response: `header`. For example, `x-li-uuid`, `x-li-fabric`, `x-li-request-id`, `body`.
* Your application configuration. For example, Client ID.