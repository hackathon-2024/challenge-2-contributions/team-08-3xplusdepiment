platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/postman-getting-started

# Generate an Access Token - Getting Started with Postman

## Summary

The full process your application will need to implement for 3-legged tokens is described in [Authorization Code Flow](https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow) and 2-legged tokens is described in [Client Credentials Flow](https://learn.microsoft.com/en-us/linkedin/shared/authentication/client-credentials-flow). The steps outlined below describe the process for using LinkedIn's Public Postman workspaces to generate OAuth tokens for testing. For any specific examples, we will use the Marketing Solutions workspace, but all steps should easily apply to all workspaces. These steps assume you have already created a [free Postman account](https://www.postman.com/postman-account/).