platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts?context=linkedin%2Fcontext

# Concepts

LinkedIn's v2 REST APIs contain a number of concepts that help apply standard RESTful principles at scale, model data consistently, and ultimately provide a well-structured end-to-end developer experience.

    host: api.linkedin.com
    basePath: /v2
    scheme: https
    

Note

The `/V2` basePath is used for _most_ LinkedIn API calls.  
The [_authentication procedure_](https://learn.microsoft.com/en-us/linkedin/shared/authentication/authentication) uses host: `www.linkedin.com`

* [Data Formats](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/data-formats?context=linkedin/context)
* [Decoration](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/decoration?context=linkedin/context)
* [Errors](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/error-handling?context=linkedin/context)
* [Methods](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/methods?context=linkedin/context)
* [Pagination](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/pagination?context=linkedin/context)
* [Projections](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/projections?context=linkedin/context)
* [Protocol Version](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/protocol-version?context=linkedin/context)
* [Rate Limits](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/rate-limits?context=linkedin/context)
* [URNs](https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/urns?context=linkedin/context)