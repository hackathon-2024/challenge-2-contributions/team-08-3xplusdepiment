platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/token-introspection?context=linkedin%2Fcontext&tabs=http

# Token Introspection

## Introduction

The token inspector tool enables developers to check the Time to Live (TTL) and status (active/expired) for all tokens (including Enterprise tokens.) For [Authorization Code Flow](https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow) (3-legged OAuth) tokens, permission scopes will be displayed. You can fetch access token data using the `/introspectToken` endpoint or the [Token Inspector Tool](https://www.linkedin.com/developers/tools/oauth/token-inspector) in the UI.