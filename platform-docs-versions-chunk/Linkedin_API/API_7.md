platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/getting-access?context=linkedin%2Fcontext

## Compliance (Closed)

The following permissions used for Compliance integrations are listed for reference purposes only. Access is closed and may not be requested.

| Product/Program | Permission | Description |
| --- | --- | --- |
| Compliance | r\_compliance | **Member Auth**: Retrieve activities for compliance monitoring and archiving |
| Compliance | w\_compliance | **Member Auth**: Manage and delete data for compliance. |