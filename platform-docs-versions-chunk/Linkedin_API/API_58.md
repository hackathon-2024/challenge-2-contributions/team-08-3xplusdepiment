platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/pagination?context=linkedin%2Fcontext

### End of the Dataset

You have reached the end of the dataset when your response contains fewer elements in the `entities` block of the response than your `count` parameter request.