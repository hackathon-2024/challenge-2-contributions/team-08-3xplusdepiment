platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/getting-access?context=linkedin%2Fcontext


## Talent

Developers seeking to build talent related integrations through one of the programs listed below can [apply here](https://business.linkedin.com/talent-solutions/ats-partners/partner-application). We recommend familiarizing yourself with the types of partner integrations available before applying by visiting [here](https://business.linkedin.com/talent-solutions/ats-partners#all) and [here](https://business.linkedin.com/talent-solutions/talent-hub/integrations#all).

* [Recruiter System Connect (RSC)](https://learn.microsoft.com/en-us/linkedin/talent/recruiter-system-connect)
* [Apply Connect](https://learn.microsoft.com/en-us/linkedin/talent/apply-connect)
* [Talent Hub](https://learn.microsoft.com/en-us/linkedin/talent/talent-hub-integrations)
* [Apply with LinkedIn](https://learn.microsoft.com/en-us/linkedin/talent/apply-with-linkedin/apply-with-linkedin)
* [Premium Job Posting](https://learn.microsoft.com/en-us/linkedin/talent/premium-job-posting)
* [Easy Apply](https://learn.microsoft.com/en-us/linkedin/talent/easy-apply)