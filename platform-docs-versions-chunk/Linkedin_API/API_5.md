platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/getting-access?context=linkedin%2Fcontext

## Sales

Developers seeking to build sales related integration using one of the permissions below must be approved as a Sales Navigator Application Platform (SNAP) partner. [Apply here](https://business.linkedin.com/sales-solutions/partners/become-a-partner) to be a SNAP partner.

| Product/Program | Permission | Description |
| --- | --- | --- |
| Sales Navigator Application Platform(SNAP) | r\_sales\_nav\_analytics | **Member Auth**: Enables access to Sales Navigator Analytics retrieval. |
| Sales Navigator Application Platform(SNAP) | r\_sales\_nav\_display | **Member Auth**: Display Services permission for Sales Navigator. |
| Sales Navigator Application Platform(SNAP) | r\_sales\_nav\_validation | **Application Auth**: Access Sales Navigator endpoints for CRM data validation. |
| Sales Navigator Application Platform(SNAP) | r\_sales\_nav\_profiles | **Application Auth**: Access Sales Navigator endpoints that present matched, publicly available member profile information. |