platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/authentication?context=linkedin%2Fcontext

## Application Authorization (2-legged OAuth Client Credential Flow)

Application Authorization or using 2-Legged OAuth grants permissions to your application to access protected LinkedIn resources. If you are accessing APIs that are not member specific, use this flow. Not all APIs support Application Authorization. For example, Marketing APIs you must use Member Authorization explained above. For step-by-step instructions on how to implement 2-legged OAuth, see [**Client Credential Flow (2-legged OAuth)**](https://learn.microsoft.com/en-us/linkedin/shared/authentication/client-credentials-flow?context=linkedin/context) page.

Note

Always request the minimal permission scopes necessary for your use case.