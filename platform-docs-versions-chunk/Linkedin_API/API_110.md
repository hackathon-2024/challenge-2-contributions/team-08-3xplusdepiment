platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/api-guide/best-practices/application-development?context=linkedin%2Fcontext

### Errors

A `500 Internal Server Error` indicates that LinkedIn is experiencing an internal error. If you continue to receive server errors, record the following details:

* Request: `url`, `method`, `header`, e.g., `access_token`, `body`
* Response: `header`, e.g., `x-li-uuid`, `x-li-fabric`, `x-li-request-id`, `body`
* Your application configuration, e.g., `client_id`

If you continue to receive errors, reach out to your partner technical support channel, or view our [Developer Support Knowledge](https://developer.linkedin.com/support).