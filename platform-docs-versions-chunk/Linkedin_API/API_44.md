platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/data-formats?context=linkedin%2Fcontext

# Data Formats

## Input

When you create or update existing entities, specify all input in JSON format and use the following HTTP Content Type:

`Content-Type: application/json`

XML input is not supported.

## Output

The output is returned in JSON format as the following HTTP Content Type:

`Content-Type: application/json`

XML output is not supported.