platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/sample-applications

## Start the OAuth sample application

**To start the server:**

1. Access the server folder.
2. Open the terminal/command prompt and run the following command to install dependencies: `mvn install`
3. Execute the following command to run the spring-boot server: `mvn spring-boot:run`

Note

The server runs on `http://localhost:8080/`

**To start the client:**

1. Access the client folder.
2. Open the terminal/command prompt and run the following command to install dependencies: `mvn install`
3. Execute the following command to run the spring-boot server: `mvn spring-boot:run`

> **Note**: The client runs on `http://localhost:8989/`

## Sample Application Demo

This demo shows an overall execution of the LinkedIn OAuth Sample Application. The demo contains clickable areas on the screen, which you can use to navigate.

Note

You can reset the demo by refreshing the page.