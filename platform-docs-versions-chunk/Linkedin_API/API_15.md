platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?context=linkedin%2Fcontext&tabs=HTTPS1

### Prerequisites

* A LinkedIn Developer application to [create a new application](https://www.linkedin.com/developer/apps/new?csrfToken=ajax%3A8674117952230020505) or [select your existing application](https://www.linkedin.com/developers/apps)
* Prior authorization access granted for at least one 3-legged OAuth permission.

The permission request workflow is outlined in the [Getting Access](https://learn.microsoft.com/en-us/linkedin/shared/authentication/getting-access) section.