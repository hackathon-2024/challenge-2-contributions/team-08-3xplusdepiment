platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/client-credentials-flow?context=linkedin%2Fcontext

## Step 1: Get Client ID and Client Secret

* Getting started? [Create a new application](https://www.linkedin.com/developers/) on the Developer Portal.
* Existing application? Go to [My apps](https://www.linkedin.com/developers/apps) to modify your app settings.

Each application is assigned a unique _Client ID_ (Consumer key/API key) and _Client Secret_. Please make a note of these values as they will be integrated into your application config files. Your _Client Secret_ protects your application's security so be sure to keep it secure!

Warning

Do not share your _Client Secret_ value with anyone, and **do not** pass it in the URL when making API calls, or URI query-string parameters, or post in support forums, chat, etc.