platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/getting-access?context=linkedin%2Fcontext

## Open Permissions (Consumer)

The following permissions are available to all developers, and may be added via self-service through the LinkedIn [Developer Portal](https://www.linkedin.com/developers/), under the Products tab on your application page. LinkedIn products can be enabled after creating a new application.

| Product/Program | Permission | Description |
| --- | --- | --- |
| Sign in with LinkedIn using OpenID Connect | profile | **Member Auth**: Retrieve authenticated member's name, headline, and photo. |
| Sign in with LinkedIn using OpenID Connect | email | **Member Auth**: Retrieve authenticated member's primary email address. |
| Share on LinkedIn | w\_member\_social | **Member Auth**: Post, comment and like posts on behalf of an authenticated member. |

## Learning

Developers seeking to build a learning related integration should refer to the [Request API Access](https://learn.microsoft.com/en-us/linkedin/learning/getting-started/request-access) page within the LinkedIn Learning API space.