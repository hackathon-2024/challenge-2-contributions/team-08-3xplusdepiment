platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/api-guide/concepts/query-tunneling?context=linkedin%2Fcontext

## Query Tunneling

To support these requirements, we’re introducing the concept of query tunneling. This feature allows you to modify your existing requests with a custom header to easily resolve offending requests.