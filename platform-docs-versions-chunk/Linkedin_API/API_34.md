platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/programmatic-refresh-tokens?context=linkedin%2Fcontext

# Refresh Tokens with OAuth 2.0

LinkedIn supports programmatic refresh tokens for all approved Marketing Developer Platform (MDP) partners.