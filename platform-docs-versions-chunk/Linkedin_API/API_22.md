platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/client-credentials-flow?context=linkedin%2Fcontext

# Client Credential Flow (2-legged OAuth)

If your application needs to access APIs that are not member specific, use the Client Credential Flow. Your application **cannot** access these APIs by default.

Learn more:

* [LinkedIn Developer Enterprise products](https://www.linkedin.com/developers/products) and permission requests.
* [LinkedIn Developers Platform](https://www.linkedin.com/help/linkedin/topics/6400/6401/112189) knowledge base.

Important

2-legged OAuth authentication is not available for [Marketing APIs](https://learn.microsoft.com/en-us/linkedin/marketing/getting-started)

Note

**Generate a Token Manually Using the Developer Portal**  
The LinkedIn Developer Portal has a token generator for manually creating tokens. Visit the [LinkedIn Developer Portal Token Generator](https://www.linkedin.com/developers/tools/oauth/token-generator) or follow the steps outlined in [Developer Portal Tools.](https://learn.microsoft.com/en-us/linkedin/shared/authentication/developer-portal-tools)