platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/postman-getting-started

### Step 3 - Fork Collections and Environments

Navigate to LinkedIn's public Postman workspaces:

[](https://www.postman.com/linkedin-developer-apis?tab=workspaces)

Choose a workspace and fork the collections and relevant environments of interest. Each collection will have an environment it should be used with. For example, if you were to navigate to the LinkedIn Marketing Solutions workspace, the Campaign Management collection should be used with the `campaign-management-env` environment.

#### Fork a Postman Collection

Fork a collection:

Fork an environment:

### Step 4 - Fill in Environment Variables

Fill in the Client ID and Client Secret environment variables before moving onto the next step. Don't forget to save your changes!