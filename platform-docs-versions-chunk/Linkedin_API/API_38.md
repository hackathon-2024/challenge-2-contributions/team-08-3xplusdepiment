platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/developer-portal-tools?context=linkedin/context

# Developer Portal Tools

The LinkedIn Developer Portal Token Generator Tool allows a quick and easy method for generating an access token to make authenticated API calls.