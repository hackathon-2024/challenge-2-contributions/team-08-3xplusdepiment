platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/api-guide/best-practices/application-development?context=linkedin%2Fcontext


## Authentication

* Whenever possible, **remind the member that they are logged in** to your application by displaying their name, portrait, and/or account settings somewhere on the page.
* **Avoid having to log in multiple times**. When a member is integrated for multiple permissions, combine the permissions into a single request rather than asking the member to reauthenticate and grant consent each time.
* **Avoid generating an access token for each API call.Cache the member's access token** after they grant your application access, and do not re-authenticate the member unless they log out or the access token expires.
* Make sure you **allow the member to log out**, and when they do log out, ensure you destroy their access token and refresh token, as applicable.
* Validate the member access token using [Token Introspection](https://learn.microsoft.com/en-us/linkedin/shared/authentication/token-introspection) or by calling any API before making the access token call.
* Whenever the access token gets expired, make use of the refresh token, if applicable, to exchange for a new access token, unless the refresh token has also expired or been revoked.
* Reintroduce the member into the authentication flow only when both the access token and the refresh token have expired or been revoked.
* If you authorize the member through the JS SDK, do not send the member through the REST authorization flow. If you do, users will have to re-authorize your application. You can exchange the JS SDK token for an OAuth 2.0 REST access token if you want to make REST calls. Otherwise, use the JS SDK token to make calls with the JS SDK.

If a member authorizes your application through the REST workflow, it does not mean they are automatically logged in to the linkedin.com website. You should not assume that the member has access to resources that are on the LinkedIn website while using your application.