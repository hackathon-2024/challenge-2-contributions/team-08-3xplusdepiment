platform: Linkedin
topic: API
subtopic: API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_API/API.md
url: https://learn.microsoft.com/en-us/linkedin/shared/authentication/getting-access?context=linkedin%2Fcontext

# Getting Access to LinkedIn APIs

The LinkedIn API uses [OAuth 2.0](http://oauth.net/2/) for user authorization and API authentication. Applications must be authorized and authenticated before they can fetch data from LinkedIn or get access to member data. This page summarizes the available permissions and partner programs available for accessing LinkedIn APIs. Most permissions and partner programs require explicit approval from LinkedIn. [Open Permissions](#open-permissions-consumer) are the only permissions that are available to all developers without special approval.

All permissions listed below are used in either [Member Authentication Flow](https://learn.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow) (3-legged) or [Application Authentication Flow](https://learn.microsoft.com/en-us/linkedin/shared/authentication/client-credentials-flow) (2-legged). More about these permission types can be found in [Authenticating with OAuth 2.0 Overview](https://learn.microsoft.com/en-us/linkedin/shared/authentication/authentication).