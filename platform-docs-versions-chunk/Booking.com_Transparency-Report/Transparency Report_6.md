platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Approx. 21

million

N/A N/A N/A N/A N/A N/A N/A



7 Comprises items moderated by our human moderators and our core ML models.



6 Calculated on the basis of the notices processed as at 30 September 2023.



5 Includes notices of illegal listings in the EU only.



4

28 August 2023 - 30 September 2023 (inclusive)



Number of

items detected

solely using

automated

means8



868,637 N/A N/A N/A N/A N/A N/A N/A



Number of

restrictions

imposed9



159,817 151,719 0 0 28 8,069 0 1



Moderation divided by category



Animal

welfare

339 339 0 0 0 0 0 0



Data

protection and

privacy

violations



64,49410 64,494 0 0 0 0 0 0



Illegal or

harmful

speech



357 357 0 0 0 0 0 0



Intellectual

property

269 269 0 0 0 0 0 0



10 Includes instances of travellers or partners erroneously sharing personal data about other persons, including but not limited to sensitive or

special-category personal data.