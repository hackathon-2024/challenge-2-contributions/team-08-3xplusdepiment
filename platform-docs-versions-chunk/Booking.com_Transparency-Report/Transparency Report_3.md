platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

\*As reflected on Booking.com's DSA webpage, the MAR information is an estimate and is based on the data available to

Booking.com at this time, and the limited guidance in the DSA. This estimate is required to be published under the DSA and should

not be used for any other purpose. The methodologies used to estimate average monthly recipients as defined in the DSA require

significant judgement and design inputs, are subject to data and other limitations, and inherently are subject to statistical variances

and uncertainties. This estimate may be revised as Booking.com refines its approach and in response to the publication of

methodology by the European Commission.

\*\*For ease of reference we have grouped under “Other” all other EU Member States where Booking.com MAR estimates represent

less than 4% of the total.



2

28 August 2023 - 30 September 2023 (inclusive)



Section 2 - Information on number of governmental orders