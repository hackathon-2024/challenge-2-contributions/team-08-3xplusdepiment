platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Summary of the use made of automated means for the purpose

of content moderation

Booking.com maintains two core ML models in 43 different

languages to detect inappropriate content. We handle the bulk

of content moderated through automation, one core model for

text content and the other core model for images.



In addition to our core ML models, we utilise human and

machine intelligence to monitor offerings on our platform and

to safeguard its integrity against fraudulent actors. For this

purpose, we maintain four ancillary ML models: three for

inauthentic listings and one for inauthentic reviews.



Qualitative description of the automated means The core ML models Booking.com applies are content classifier

models which are tuned to identify context. The ancillary ML

models use numerous different data points and fraud indicators

to detect inauthentic listings and inauthentic reviews.