platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Meaningful and comprehensible information regarding the

applied detection method

Our core ML algorithms swiftly review various types of content,

such as guest reviews, partner responses, photos and more.

Most content is approved within seconds and goes live on



15 Comprises resources fully dedicated to content moderation activities and does not include all resources in our Fraud and Trust and Safety

teams whose scope is larger than content moderation.



10

28 August 2023 - 30 September 2023 (inclusive)



Booking.com. Anything not approved by our core ML algorithms

is sent to our moderators for further review when potential

guideline violations are detected. Approved content is

accessible on our platform and apps, while policy-violating

content won't be published. Our decision can be appealed or

the content can be resubmitted.



Use made of automated means