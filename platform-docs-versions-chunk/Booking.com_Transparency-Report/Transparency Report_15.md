platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Support given to human resources dedicated to content

moderation

Internal employees can reach out to the employee assistance

program that provides counselling services, practical

information and digital content to support employees’ mental,

physical, social and financial well-being. All moderators have

followed a course about dealing with distressing content,

designed to increase awareness, learn about prevention of

potential issues and to understand what support is available.

Our external partner contracted for moderation has a consistent

approach towards employee safety utilising SGS audits and



12

28 August 2023 - 30 September 2023 (inclusive)



participating with renown partners.



13