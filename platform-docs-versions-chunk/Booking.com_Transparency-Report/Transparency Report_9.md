platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Substantive complaints on the illegality or incompatibility 3192



Number of decision reversals 635



Section 7 - Use of automated means for content moderation and human resources



Metric Total number Internal complaints Own-initiative Notice and

Action

mechanism

(NAM)



Languages of the MS of

the EU



NAM

total

Trusted

flagger



Accuracy rate

of the items

processed

solely by

automated

means13



99.122% N/A 99.122% N/A N/A English - 99.7

Estonian - 98.9

Finnish - 99.2

French - 99.9

German - 99.9

Greek - 99.7

Hungarian - 97.8

Irish - N/A

Italian - 99.5

Latvian - 98.4

Lithuanian - 98.9

Maltese - N/A



13 The accuracy rate is calculated for our core ML models only, and excludes our ancillary ML models.



8

28 August 2023 - 30 September 2023 (inclusive)



Polish - 99.9

Portuguese - 99.7

Romanian - 94.7

Slovak - 99.7

Slovenian - N/A

Spanish - 99.9

Swedish - 99.7



Accuracy rate

of the items

processed

partly by

automated

means



N/A N/A N/A N/A N/A N/A