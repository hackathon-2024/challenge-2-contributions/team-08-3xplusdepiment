platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Specification of the precise purposes to apply automated

means

The core ML models are designed to detect both illegal content

and content that violates Booking’s content policies. Illegal and

violating content identified by the core ML models is sent to our

content moderators for human review. Content moderators will

make the final decision. The ancillary ML models are designed



11

28 August 2023 - 30 September 2023 (inclusive)



and used to detect inauthentic listings and inauthentic reviews

at scale.



Safeguards applied to the use of automated means We perform constant random sampling of the automatically

approved items and send them to moderators to ensure that

the quality of ML automated content approvals is within the

acceptable range.



Specific elements of the human resources dedicated to content moderation