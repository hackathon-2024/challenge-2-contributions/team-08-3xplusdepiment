platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Our long-held values as well as our guidelines and terms and conditions for all users of our platform - travellers and supply

partners - are designed to foster safe and welcoming travel experiences for all. To maintain that environment and ensure the safety

of our travellers and supply partners, we take action upon content that violates the law, our Content Guidelines or Terms and

Conditions.



As required under the Digital Services Act (DSA), this report provides insights into the content moderation activities that we

engaged in during the reporting period, including the volume and nature of content removed from our platform and removal

requests received from public authorities and users.



1

28 August 2023 - 30 September 2023 (inclusive)



Section 1 - EU Member State MAR information from 1 February 2023 up to and including 31 July 2023



Figure 1 - EU Member State MAR Information\*