platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

11 Comprises content uploaded by travellers or partners which violates our content guidelines as it contains content of a sexual nature.



6

28 August 2023 - 30 September 2023 (inclusive)



platform

service



Section 5 - Suspensions imposed on repeated offenders



Metric Total number



Number of suspensions enacted for the provision of manifestly

illegal content

0



Number of suspensions enacted for the provision of manifestly

unfounded notices

0



Number of suspensions enacted for the provision of manifestly

unfounded complaints

0



Section 6 - Out of court dispute settlement bodies and internal complaints mechanism



Metric Number



Out-of-court dispute settlement bodies



Number of decisions submitted to out-of-court dispute

settlement bodies

0



Internal complaints mechanism



Number of complaints submitted to the internal-complaints

mechanism

3192



7

28 August 2023 - 30 September 2023 (inclusive)



Procedural complaints 0