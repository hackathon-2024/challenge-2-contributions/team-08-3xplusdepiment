platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Error rate of

the

automated

means

applied14



0.878% N/A 0.878% N/A N/A N/A



Human resources dedicated to content moderation



Number of

internal

moderators

employed by



22 7 22 2 2 English 22

Dutch 3

German 1

Italian 3



14 The error rate is calculated for our core ML models only, and excludes our ancillary ML models.



9

28 August 2023 - 30 September 2023 (inclusive)



the provider15 Spanish 5

French 2

Greek 1



Number of

external

moderators

contracted by

the provider



13 0 13 0 0 English 13

Romanian 13

German 2

French 2

Spanish 1



Section 8 - Statement on content moderation



Information about content moderation



Own initiative moderation



Summary of the content moderation engaged in at our own

initiative

Booking.com aims to reflect the most up-to-date reviews from

our travellers. That’s why we have a fast turnaround on our

content moderation, using both moderation by automated

Machine Learning (ML) models and manual review.