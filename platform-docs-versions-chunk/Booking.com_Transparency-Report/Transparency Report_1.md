platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Digital Services Act



27 October 2023 - Transparency Report prepared by Booking.com

B.V. under the Digital Services Act

28 August 2023 - 30 September 2023 (inclusive)



About this report



At Booking.com, our mission is to make it easier for everyone to experience the world. We believe that travel can bring out the best

in humanity. Travel promotes a better understanding of different cultures and ways of life. We also believe in and work towards

making travel a force for good in the world - one that enriches people’s lives through a range of experiences, big and small. As a

travel platform, it is at the core of our activities to facilitate travel experiences centred on our customers and underpinned by our

values.