platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

3 Calculated on the basis of the orders processed as at 30 September 2023.



2 Includes illegal listings orders received.



1 Includes all other Member States where no data requests or orders were received.



3

28 August 2023 - 30 September 2023 (inclusive)



Section 3 - Information on number of notices



Metric Total number Notice and Action mechanism (NAM)



NAM total Trusted Flagger



Number of notices received5 15 15 0



Median time to take action on the basis of

the notice6

6.8 days 6.8 days N/A



Number of actions taken on the basis of

the law

0 0 N/A



Number of actions taken on the basis of

the terms and conditions of service

9 9 N/A



Section 4 - Information on own-initiative content moderation



Own-initiative

content

moderation



Total

number

Visibility restriction Monetary

restriction

Provision of the service Account restriction



Removal Other Suspension Termination Suspension Termination



Number of

items

moderated7