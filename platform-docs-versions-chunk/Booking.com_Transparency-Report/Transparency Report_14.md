platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

Qualifications of the human resources dedicated to content

moderation

1 senior manager, 3 team leads, 2 project managers, 4

associates, 22 content moderators, 13 vendor content

moderators.



Training given to human resources dedicated to content

moderation

When new policies are launched or a new content moderator is

onboarded, training decks and videos are provided to introduce

the new content policies. Content moderators spend on

average approximately 6 hours monthly receiving training,

reviewing content guidelines and policy clarifications, reviewing

their errors and asking questions. Frequently asked questions

are compiled, and grey areas are clarified on a regular basis.