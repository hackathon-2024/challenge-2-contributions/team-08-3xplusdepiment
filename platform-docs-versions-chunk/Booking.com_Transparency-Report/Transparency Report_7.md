platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

9 Comprises restrictions on content applied in the EU, and restrictions on EU listings and EU accounts related to the provision of information that

is illegal or incompatible with our terms and conditions.



8 Comprises items detected solely using our core ML models.



5

28 August 2023 - 30 September 2023 (inclusive)



infringements



Negative

effects on civic

discourse or

elections



0 0 0 0 0 0 0 0



Non-consensu

al behaviour

0 0 0 0 0 0 0 0



Pornography

or sexualised

content11



155 155 0 0 1 0 0 1



Protection of

minors

0 0 0 0 0 0 0 0



Risk for public

security

0 0 0 0 0 0 0 0



Scams and/or

fraud12

39,617 31,548 0 0 0 8,069 0 0



Unsafe and/or

illegal

products



61 34 0 0 27 0 0 0



Violence 4 4 0 0 0 0 0 0



Scope of 54,520 54,520 0 0 0 0 0 0



12 Includes restrictions imposed on inauthentic EU listings and inauthentic user reviews.