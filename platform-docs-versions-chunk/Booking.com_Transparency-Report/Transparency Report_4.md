platform: Booking.com
topic: Transparency-Report
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Booking.com_Transparency-Report/Transparency Report.md
url: <EMPTY>

The section below provides insight into the volume of government data or removal requests we received during the reporting

period, categorised by type of illegal content.



Metric Total number Member States of the European Union



BE DE ES FR HR IT NL PT Other1



Information on number of orders



Number of data requests received 10 1 2 2 1 1 1 2 0 0



Number of illegal content orders

received2

4 0 0 2 0 0 1 0 1 0



Median time to inform the authority of

the receipt of the order

0 (instantly upon

submitting)

0 0 0 0 0 0 0 0 N/A



Median time to give effect to the

order3

10 days N/A N/A N/A N/A N/A N/A N/A N/A N/A



Moderation divided by category



Unsafe and/or illegal products4 14 1 2 4 1 1 2 2 1 0



4 Under the DSA, we are required to categorise orders by the type of illegal content concerned. All orders received concern regulatory

compliance issues which fall under the category of ‘Unsafe and/or illegal products’.