platform: Youtube
topic: Data-API
subtopic: DSA-Statistics
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Youtube_Data-API/DSA-Statistics.md
url: https://developers.google.com/youtube/v3/docs

### MembershipsLevels

A `**membershipsLevel**` resource identifies a pricing level for the creator that authorized the API request.

For more information about this resource, see its [resource representation](https://developers.google.com/youtube/v3/docs/membershipsLevels#resource) and list of [properties](https://developers.google.com/youtube/v3/docs/membershipsLevels#properties).

| Method | HTTP request | Description |
| --- | --- | --- |
| URIs relative to `https://www.googleapis.com/youtube/v3` |     |     |
| `[list](https://developers.google.com/youtube/v3/docs/membershipsLevels/list)` | `GET /membershipsLevels` | Returns a collection of zero or more `**membershipsLevel**` resources owned by the channel that authorized the API request. Levels are returned in implicit display order. |