platform: Youtube
topic: Data-API
subtopic: DSA-Statistics
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Youtube_Data-API/DSA-Statistics.md
url: https://developers.google.com/youtube/v3/docs

### VideoCategories

A `**videoCategory**` resource identifies a category that has been or could be associated with uploaded videos.

For more information about this resource, see its [resource representation](https://developers.google.com/youtube/v3/docs/videoCategories#resource) and list of [properties](https://developers.google.com/youtube/v3/docs/videoCategories#properties).

| Method | HTTP request | Description |
| --- | --- | --- |
| URIs relative to `https://www.googleapis.com/youtube/v3` |     |     |
| `[list](https://developers.google.com/youtube/v3/docs/videoCategories/list)` | `GET /videoCategories` | Returns a list of categories that can be associated with YouTube videos. |