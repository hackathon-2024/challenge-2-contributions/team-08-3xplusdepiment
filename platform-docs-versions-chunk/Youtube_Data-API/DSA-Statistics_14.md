platform: Youtube
topic: Data-API
subtopic: DSA-Statistics
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Youtube_Data-API/DSA-Statistics.md
url: https://developers.google.com/youtube/v3/docs

### Members

A `**member**` resource represents a channel member for a YouTube channel. A member provides recurring monetary support to a creator and receives special benefits. For example, members are able to chat when the creator turns on members-only mode for a chat.

For more information about this resource, see its [resource representation](https://developers.google.com/youtube/v3/docs/members#resource) and list of [properties](https://developers.google.com/youtube/v3/docs/members#properties).

| Method | HTTP request | Description |
| --- | --- | --- |
| URIs relative to `https://www.googleapis.com/youtube/v3` |     |     |
| `[list](https://developers.google.com/youtube/v3/docs/members/list)` | `GET /members` | Lists members (formerly known as "sponsors") for a channel. The API request must be authorized by the channel owner. |