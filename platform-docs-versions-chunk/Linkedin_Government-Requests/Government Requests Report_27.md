platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### By country and action taken

#### 2019: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Canada | 1   | 1   | 100% |
| China | 3   | 3   | 100% |
| Germany | 1   | 1   | 100% |
| Saudi Arabia | 1   | 1   | 100% |
| Turkey | 2   | 2   | 100% |
| **Totals** | **8** | **8** | **100%** |

### By country and action taken

#### 2018: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Turkey | 1   | 1   | 100% |
| China | 4   | 2   | 50% |
| India | 1   | 1   | 100% |
| **Totals** | **6** | **4** | **67%** |