platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


## Breakdown of U.S. government requests for data

#### 2023: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 386 |
| Accounts subject to requests | 2,053 |
| Requests for which LinkedIn provided some data | 83% |
| Subpoenas \[2\] | 76% |
| Search warrants \[3\] | 6%  |
| Court orders \[4\] | 17% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2022: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 391 |
| Accounts subject to requests | 2,636 |
| Requests for which LinkedIn provided some data | 80% |
| Subpoenas \[2\] | 77% |
| Search warrants \[3\] | 7%  |
| Court orders \[4\] | 15% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2021: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 302 |
| Accounts subject to requests | 1,190 |
| Requests for which LinkedIn provided some data | 83% |
| Subpoenas \[2\] | 73% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 18% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2021: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 362 |
| Accounts subject to requests | 2,349 |
| Requests for which LinkedIn provided some data | 83% |
| Subpoenas \[2\] | 70% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 21% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2020: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 364 |
| Accounts subject to requests | 1,420 |
| Requests for which LinkedIn provided some data | 76% |
| Subpoenas \[2\] | 66% |
| Search warrants \[3\] | 11% |
| Court orders \[4\] | 22% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2020: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 377 |
| Accounts subject to requests | 2,459 |
| Requests for which LinkedIn provided some data | 78% |
| Subpoenas \[2\] | 63% |
| Search warrants \[3\] | 11% |
| Court orders \[4\] | 25% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2019: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 349 |
| Accounts subject to requests | 1,488 |
| Requests for which LinkedIn provided some data | 82% |
| Subpoenas \[2\] | 68% |
| Search warrants \[3\] | 12% |
| Court orders \[4\] | 20% |
| Other \[5\] | 0%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] |     |

#### 2019: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 314 |
| Accounts subject to requests | 1,170 |
| Requests for which LinkedIn provided some data | 82% |
| Subpoenas \[2\] | 72% |
| Search warrants \[3\] | 10% |
| Court orders \[4\] | 17% |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 250-499 |

#### 2018: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 203 |
| Accounts subject to requests | 1,069 |
| Requests for which LinkedIn provided some data | 52% |
| Subpoenas \[2\] | 71% |
| Search warrants \[3\] | 11% |
| Court orders \[4\] | 16% |
| Other \[5\] | 2%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2018: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 196 |
| Accounts subject to requests | 1,077 |
| Requests for which LinkedIn provided some data | 37% |
| Subpoenas \[2\] | 70% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 19% |
| Other \[5\] | 3%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2017: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 191 |
| Accounts subject to requests | 602 |
| Requests for which LinkedIn provided some data | 69% |
| Subpoenas \[2\] | 62% |
| Search warrants \[3\] | 8%  |
| Court orders \[4\] | 26% |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2017: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 187 |
| Accounts subject to requests | 594 |
| Requests for which LinkedIn provided some data | 71% |
| Subpoenas \[2\] | 63% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 26% |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2016: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 135 |
| Accounts subject to requests | 345 |
| Requests for which LinkedIn provided some data | 74% |
| Subpoenas \[2\] | 71% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 18% |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2016: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 128 |
| Accounts subject to requests | 291 |
| Requests for which LinkedIn provided some data | 68% |
| Subpoenas \[2\] | 66% |
| Search warrants \[3\] | 14% |
| Court orders \[4\] | 17% |
| Other \[5\] | 2%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2015: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 126 |
| Accounts subject to requests | 226 |
| Requests for which LinkedIn provided some data | 72% |
| Subpoenas \[2\] | 72% |
| Search warrants \[3\] | 11% |
| Court orders \[4\] | 16% |
| Other \[5\] | 2%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2015: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 99  |
| Accounts subject to requests | 141 |
| Requests for which LinkedIn provided some data | 78% |
| Subpoenas \[2\] | 83% |
| Search warrants \[3\] | 10% |
| Court orders \[4\] | 7%  |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2014: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 84  |
| Accounts subject to requests | 202 |
| Requests for which LinkedIn provided some data | 70% |
| Subpoenas \[2\] | 86% |
| Search warrants \[3\] | 12% |
| Court orders \[4\] | 2%  |
| Other \[5\] | 0%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2014: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 85  |
| Accounts subject to requests | 1,069 |
| Requests for which LinkedIn provided some data | 84% |
| Subpoenas \[2\] | 82% |
| Search warrants \[3\] | 13% |
| Court orders \[4\] | 4%  |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

\[1\] U.S. Government Requests for Member Data include all requests received by LinkedIn from the U.S. government except for national security-related requests, such as National Security Letters and requests under the U.S. Foreign Intelligence Surveillance Act (FISA), if any.  
  

\[2\] Subpoenas may be issued for information that is reasonably relevant to the general subject matter of a pending investigation. They are typically pre-signed by a court clerk and are issued by prosecutors without the involvement of a judge.  
  

\[3\] Search warrants require the government to demonstrate "probable cause" and are generally issued by a judge. The standard applicable to a search warrant is higher than that applicable to a subpoena.  

\[4\] Court orders vary depending on the circumstances and the issuing court and jurisdiction. Many of the court orders LinkedIn receives are issued pursuant to 18 U.S.C. § 2703(d), a provision of the Electronic Communications Privacy Act (ECPA). To obtain such an order, the government must demonstrate specific and articulable facts showing that there are reasonable grounds to believe that the information sought is relevant and material to an ongoing investigation. This standard is higher than that applicable to subpoenas but lower than that applicable to search warrants.

\[5\] The "Other" category includes requests that do not fall within any of the above categories. Examples include emergency requests. As indicated in footnote 1, the category does not include national security-related requests.  

  
\[6\] In our Government Requests Reports prior to July 2019, we reported on National Security Requests, which could include National Security Letters and FISA requests. In the effort to provide more transparency to our members, we now report NLSs and FISA requests separately. Because we are required by law to delay the release of FISA data by 6 months, reported FISA request metrics correspond to the previous reporting period (for example, data for the July-December 2019 period is reported in our January-June 2020 report).