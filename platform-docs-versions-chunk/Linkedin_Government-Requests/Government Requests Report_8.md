platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


### Requests by country and accounts subject to those requests

#### 2020: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 4   | 4   | 0%  | 0   |
| Australia | 4   | 5   | 50% | 2   |
| Belgium | 2   | 4   | 100% | 4   |
| Brazil | 4   | 4   | 50% | 2   |
| Canada | 1   | 1   | 100% | 1   |
| China | 2   | 2   | 0%  | 0   |
| Columbia | 2   | 2   | 0%  | 0   |
| Czech Republic | 1   | 1   | 0%  | 0   |
| Dominican Republic | 1   | 1   | 0%  | 0   |
| Estonia | 2   | 2   | 50% | 1   |
| France | 26  | 33  | 46% | 14  |
| Georgia | 1   | 1   | 0%  | 0   |
| Germany | 50  | 82  | 50% | 38  |
| India | 29  | 32  | 0%  | 0   |
| Italy | 7   | 8   | 57% | 5   |
| Monaco | 1   | 1   | 100% | 1   |
| Netherlands | 16  | 33  | 69% | 19  |
| Norway | 1   | 1   | 100% | 1   |
| Pakistan | 1   | 1   | 0%  | 0   |
| Peru | 1   | 1   | 0%  | 0   |
| Poland | 2   | 2   | 0%  | 0   |
| Portugal | 2   | 2   | 0%  | 0   |
| Serbia | 2   | 3   | 0%  | 0   |
| Singapore | 3   | 3   | 0%  | 0   |
| Slovenia | 2   | 8   | 50% | 2   |
| Spain | 8   | 9   | 38% | 3   |
| Sweden | 1   | 1   | 100% | 1   |
| Switzerland | 3   | 3   | 67% | 2   |
| United Kingdom | 5   | 5   | 40% | 1   |
| United States | 364 | 1,420 | 76% | 702 |
| **Total** | **548** | **1,675** | **63%** | **799** |