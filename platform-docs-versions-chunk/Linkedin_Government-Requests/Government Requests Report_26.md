platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### By country and action taken

#### 2020: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| US  | 1   | 1   | 100% |
| Turkey | 2   | 2   | 100% |
| China | 18  | 16  | 89% |
| **Totals** | **21** | **19** | **90%** |

### By country and action taken

#### 2019: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Brazil | 1   | 1   | 100% |
| China | 14  | 11  | 79% |
| Greece | 1   | 1   | 100% |
| India | 1   | 1   | 100% |
| Poland | 1   | 1   | 100% |
| Russia | 2   | 2   | 100% |
| Switzerland | 1   | 1   | 100% |
| Turkey | 2   | 2   | 100% |
| **Totals** | **23** | **20** | **87%** |