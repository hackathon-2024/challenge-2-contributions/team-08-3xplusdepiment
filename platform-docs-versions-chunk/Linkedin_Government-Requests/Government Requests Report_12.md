platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Requests by country and accounts subject to those requests

#### 2018: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 10  | 13  | 31% | 4   |
| Canada | 3   | 3   | 67% | 2   |
| China | 2   | 2   | 100% | 2   |
| France | 11  | 14  | 29% | 4   |
| Germany | 3   | 4   | 75% | 3   |
| India | 7   | 9   | 0%  | 0   |
| Ireland | 1   | 1   | 100% | 1   |
| Italy | 2   | 2   | 0%  | 0   |
| Spain | 4   | 4   | 0%  | 0   |
| United Kingdom | 1   | 1   | 0%  | 0   |
| United States | 203 | 1,069 | 52% | 553 |
| **Total** | **247** | **1,122** | **41%** | **569** |