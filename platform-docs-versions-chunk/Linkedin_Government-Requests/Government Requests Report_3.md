platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


### Requests by country and accounts subject to those requests

#### 2023: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 2   | 2   | 0%  | 0   |
| Australia | 2   | 3   | 50% | 1   |
| Austria | 6   | 7   | 0%  | 0   |
| Bangladesh | 1   | 1   | 0%  | 0   |
| Belgium | 4   | 5   | 100% | 4   |
| Bolivia | 1   | 2   | 0%  | 0   |
| Brazil | 18  | 20  | 44% | 8   |
| Canada | 8   | 10  | 75% | 8   |
| Chile | 1   | 1   | 0%  | 0   |
| China | 3   | 0   | 0%  | 0   |
| Columbia | 2   | 2   | 0%  | 0   |
| Czech Republic | 1   | 1   | 100% | 1   |
| Estonia | 2   | 2   | 0%  | 0   |
| Finland | 1   | 2   | 0%  | 0   |
| France | 63  | 68  | 57% | 39  |
| Germany | 65  | 102 | 82% | 64  |
| Greece | 3   | 3   | 33% | 1   |
| Hungary | 1   | 1   | 100% | 1   |
| India | 81  | 116 | 21% | 19  |
| Ireland | 12  | 12  | 58% | 7   |
| Italy | 103 | 250 | 43% | 88  |
| Malta | 1   | 1   | 0%  | 0   |
| Mexico | 7   | 8   | 0%  | 0   |
| Netherlands | 21  | 54  | 95% | 52  |
| Poland | 8   | 21  | 38% | 3   |
| Portugal | 2   | 2   | 50% | 1   |
| Romania | 1   | 4   | 0%  | 0   |
| Singapore | 2   | 2   | 0%  | 0   |
| Spain | 16  | 17  | 44% | 7   |
| Switzerland | 2   | 2   | 0%  | 0   |
| Taiwan | 1   | 1   | 0%  | 0   |
| United Kingdom | 17  | 22  | 53% | 10  |
| United States | 386 | 2053 | 83% | 1306 |
| Venezuela | 1   | 1   | 0%  | 0   |
| **Total** | **845** | **2798** | **64%** | **1620** |