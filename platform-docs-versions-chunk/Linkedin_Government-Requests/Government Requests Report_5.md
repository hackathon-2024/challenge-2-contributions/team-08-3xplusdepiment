platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


### Requests by country and accounts subject to those requests

#### 2022: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 3   | 3   | 0%  | 0   |
| Australia | 1   | 1   | 0%  | 0   |
| Austria | 2   | 2   | 0%  | 0   |
| Belgium | 6   | 13  | 100% | 10  |
| Brazil | 12  | 35  | 50% | 7   |
| Canada | 5   | 7   | 80% | 6   |
| China | 6   | 6   | 33% | 2   |
| Costa Rica | 1   | 5   | 0%  | 0   |
| Denmark | 2   | 3   | 100% | 2   |
| Finland | 1   | 1   | 0%  | 0   |
| France | 22  | 35  | 73% | 24  |
| Georgia | 1   | 1   | 0%  | 0   |
| Germany | 68  | 81  | 75% | 52  |
| Hungary | 2   | 3   | 50% | 1   |
| India | 64  | 84  | 8%  | 5   |
| Ireland | 5   | 5   | 20% | 1   |
| Italy | 11  | 23  | 73% | 14  |
| Jordan | 1   | 2   | 0%  | 0   |
| Malta | 3   | 3   | 0%  | 0   |
| Mexico | 2   | 2   | 0%  | 0   |
| Monaco | 1   | 2   | 100% | 1   |
| Nepal | 1   | 1   | 0%  | 0   |
| Netherlands | 10  | 75  | 40% | 9   |
| New Zealand | 1   | 1   | 0%  | 0   |
| Peru | 1   | 1   | 0%  | 0   |
| Poland | 3   | 3   | 33% | 1   |
| Portugal | 2   | 4   | 50% | 1   |
| Romania | 1   | 1   | 0%  | 0   |
| Singapore | 1   | 1   | 0%  | 0   |
| Spain | 11  | 12  | 36% | 5   |
| Switzerland | 5   | 5   | 20% | 1   |
| United Arab Emirates | 1   | 1   | 0%  | 0   |
| United Kingdom | 21  | 24  | 43% | 11  |
| United States | 327 | 1114 | 81% | 572 |
| **Total** | **604** | **1560** | **64%** | **725** |