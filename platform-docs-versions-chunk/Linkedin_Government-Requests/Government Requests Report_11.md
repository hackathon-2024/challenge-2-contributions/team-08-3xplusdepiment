platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Requests by country and accounts subject to those requests

#### 2019: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 8   | 12  | 88% | 9   |
| Canada | 2   | 2   | 100% | 2   |
| China | 1   | 1   | 0%  | 0   |
| Dominican Republic | 1   | 1   | 0%  | 0   |
| France | 6   | 9   | 67% | 4   |
| Germany | 9   | 14  | 44% | 4   |
| India | 1   | 10  | 0%  | 0   |
| Italy | 1   | 1   | 0%  | 0   |
| Netherlands | 2   | 2   | 50% | 1   |
| Poland | 1   | 1   | 100% | 1   |
| Portugal | 2   | 2   | 0%  | 0   |
| Spain | 1   | 1   | 0%  | 0   |
| Switzerland | 2   | 2   | 50% | 1   |
| UK  | 3   | 5   | 0%  | 0   |
| US  | 314 | 1,170 | 82% | 639 |
| **Total** | **362** | **1,233** | **77%** | **662** |