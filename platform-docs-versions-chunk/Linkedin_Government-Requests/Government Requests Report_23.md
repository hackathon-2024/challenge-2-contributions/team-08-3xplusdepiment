platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

## Government requests for content removal

### By country and action taken

#### 2023: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Australia | 2   | 1   | 50% |
| Bahrain | 1   | 0   | 0%  |
| Bangladesh | 1   | 1   | 100% |
| Bolivia | 1   | 1   | 100% |
| Brazil | 4   | 4   | 100% |
| China | 5   | 4   | 80% |
| Ghana | 1   | 1   | 100% |
| India | 13  | 12  | 92% |
| Indonesia | 8   | 8   | 100% |
| Qatar | 1   | 0   | 0%  |
| Singapore | 1   | 1   | 100% |
| Turkey | 5   | 5   | 100% |
| United Kingdom | 1   | 1   | 100% |
| United States | 5   | 5   | 100% |
| **Totals** | **49** | **44** | **90%** |