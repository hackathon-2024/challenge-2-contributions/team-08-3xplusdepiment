platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Government requests for data

#### 2014: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts impacted (LinkedIn provided some data) \[2\] |
| Brazil | 3   | 3   | 33% | 1   |
| France | 3   | 3   | 0%  | 0   |
| Germany | 3   | 3   | 0%  | 0   |
| India | 5   | 5   | 0%  | 0   |
| Ireland | 1   | 1   | 100% | 1   |
| Singapore | 1   | 1   | 0%  | 0   |
| United States | 84  | 202 | 70% | 76  |
| **Total** | **100** | **218** | **60%** | **78** |