platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### By country and action taken

#### 2021: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| China | 12  | 12  | 100% |
| India | 5   | 4   | 80% |
| Turkey | 10  | 10  | 100% |
| **Totals** | **27** | **26** | **96%** |

### By country and action taken

#### 2021: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| China | 31  | 30  | 97% |
| India | 7   | 4   | 57% |
| Turkey | 24  | 23  | 96% |
| **Totals** | **62** | **57** | **92%** |

### By country and action taken

#### 2020: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Turkey | 5   | 4   | 80% |
| China | 24  | 22  | 92% |
| **Totals** | **29** | **26** | **89%** |