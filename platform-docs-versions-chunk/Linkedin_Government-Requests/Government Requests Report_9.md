platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


### Requests by country and accounts subject to those requests

#### 2020: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Belgium | 1   | 1   | 0%  | 0   |
| Brazil | 5   | 5   | 80% | 4   |
| Canada | 1   | 1   | 100% | 1   |
| Columbia | 1   | 1   | 0%  | 0   |
| Finland | 1   | 1   | 0%  | 0   |
| France | 13  | 16  | 62% | 10  |
| Germany | 62  | 92  | 74% | 63  |
| Greece | 2   | 2   | 0%  | 0   |
| India | 11  | 14  | 0%  | 0   |
| Italy | 3   | 4   | 100% | 4   |
| Malta | 2   | 2   | 0%  | 0   |
| Netherlands | 7   | 11  | 86% | 9   |
| Poland | 1   | 7   | 0%  | 0   |
| Portugal | 1   | 1   | 0%  | 0   |
| Serbia | 1   | 1   | 0%  | 0   |
| Singapore | 3   | 5   | 33% | 2   |
| Slovenia | 1   | 2   | 0%  | 0   |
| Spain | 4   | 4   | 25% | 1   |
| Switzerland | 2   | 2   | 0%  | 0   |
| United Kingdom | 10  | 11  | 30% | 3   |
| United States | 377 | 2,459 | 78% | 1,862 |
| **Total** | **509** | **2,642** | **72%** | **1,959** |