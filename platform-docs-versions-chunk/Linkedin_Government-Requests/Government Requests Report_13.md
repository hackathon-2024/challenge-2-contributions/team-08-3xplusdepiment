platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Requests by country and accounts subject to those requests

#### 2018: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Australia | 2   | 2   | 0%  | 0   |
| Brazil | 6   | 8   | 63% | 5   |
| Canada | 3   | 3   | 0%  | 0   |
| France | 2   | 10  | 10% | 1   |
| Germany | 12  | 15  | 73% | 11  |
| India | 5   | 5   | 0%  | 0   |
| Italy | 1   | 1   | 100% | 1   |
| Qatar | 1   | 1   | 0%  | 0   |
| Singapore | 2   | 2   | 0%  | 0   |
| United Kingdom | 2   | 2   | 100% | 2   |
| United States | 196 | 1,077 | 37% | 399 |
| **Total** | **232** | **1,126** | **35%** | **419** |