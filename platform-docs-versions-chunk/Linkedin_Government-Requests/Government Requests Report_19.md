platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Government requests for data

#### 2015: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts impacted (LinkedIn provided some data) \[2\] |
| India | 4   | 4   | 0%  | 0   |
| Bulgaria | 2   | 2   | 50% | 1   |
| Canada | 2   | 2   | 100% | 2   |
| France | 2   | 2   | 0%  | 0   |
| Spain | 1   | 8   | 0%  | 0   |
| United Kingdom | 2   | 2   | 0%  | 0   |
| United States | 99  | 141 | 78% | 113 |
| **Total** | **112** | **161** | **74%** | **116** |