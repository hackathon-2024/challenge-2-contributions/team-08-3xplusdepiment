platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

## This report covers:

[Global requests for data](#government-requests-data)

[U.S. government requests for data](#us-government-requests)

[Global content removal requests](#content-removal-requests)

## Government requests for data: global

### Requests in the U.S. and globally

### Requests in the U.S. and globally

### Requests in the U.S. and globally

### Requests in the U.S. and globally

### Requests in the U.S. and globally

## Requests by country