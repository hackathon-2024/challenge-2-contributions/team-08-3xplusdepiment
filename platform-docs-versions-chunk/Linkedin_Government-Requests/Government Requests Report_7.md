platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


### Requests by country and accounts subject to those requests

#### 2021: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 1   | 1   | 0%  | 0   |
| Belgium | 4   | 4   | 0%  | 4   |
| Brazil | 3   | 6   | 0%  | 0   |
| Canada | 4   | 4   | 100% | 4   |
| Chile | 2   | 2   | 0%  | 0   |
| China | 5   | 5   | 80% | 4   |
| Colombia | 1   | 1   | 0%  | 0   |
| France | 56  | 74  | 73% | 47  |
| Germany | 62  | 70  | 77% | 52  |
| Greece | 1   | 1   | 100% | 1   |
| Hungary | 1   | 1   | 100% | 1   |
| India | 30  | 37  | 0%  | 0   |
| Ireland | 6   | 6   | 33% | 2   |
| Italy | 23  | 36  | 61% | 17  |
| Mexico | 2   | 2   | 0%  | 0   |
| Monaco | 1   | 1   | 100% | 1   |
| Netherlands | 21  | 28  | 76% | 18  |
| Nigeria | 1   | 1   | 0%  | 0   |
| Pakistan | 1   | 1   | 0%  | 0   |
| Peru | 3   | 3   | 100% | 3   |
| Poland | 3   | 3   | 0%  | 0   |
| Portugal | 4   | 4   | 0%  | 0   |
| Singapore | 1   | 1   | 0%  | 0   |
| Slovenia | 2   | 2   | 0%  | 0   |
| South Korea | 1   | 1   | 0%  | 0   |
| Spain | 9   | 10  | 33% | 3   |
| Sweden | 1   | 1   | 100% | 1   |
| Switzerland | 2   | 2   | 50% | 1   |
| United Kingdom | 12  | 15  | 58% | 9   |
| United States | 362 | 2349 | 83% | 901 |
| **Total** | **625** | **2672** | **72%** | **1069** |