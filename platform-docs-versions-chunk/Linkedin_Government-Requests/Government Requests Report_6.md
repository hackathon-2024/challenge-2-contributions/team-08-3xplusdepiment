platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


### Requests by country and accounts subject to those requests

#### 2021: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 4   | 4   | 0%  | 0   |
| Australia | 1   | 1   | 0%  | 0   |
| Belgium | 8   | 9   | 50% | 5   |
| Brazil\* | 3   | 6   | 100% | 6   |
| Canada | 1   | 1   | 0%  | 0   |
| China | 4   | 4   | 50% | 2   |
| Czech Republic | 1   | 1   | 100% | 1   |
| France | 37  | 43  | 65% | 27  |
| Germany | 79  | 92  | 77% | 69  |
| Greece | 3   | 3   | 33% | 1   |
| Hungary | 1   | 1   | 0%  | 0   |
| India | 46  | 49  | 0%  | 0   |
| Ireland | 1   | 1   | 0%  | 0   |
| Italy | 8   | 9   | 0%  | 0   |
| Malta | 1   | 1   | 0%  | 0   |
| Mexico | 1   | 1   | 0%  | 0   |
| Netherlands | 11  | 23  | 82% | 12  |
| New Zealand | 1   | 1   | 100% | 1   |
| Poland | 5   | 5   | 20% | 1   |
| Singapore | 1   | 1   | 0%  | 0   |
| Spain | 12  | 12  | 33% | 4   |
| Switzerland | 6   | 6   | 67% | 4   |
| United Kingdom | 15  | 20  | 47% | 8   |
| United States | 302 | 1190 | 83% | 676 |
| **Total** | **552** | **1484** | **67%** | **817** |

_\*An earlier version of this report inadvertently omitted the data shown in the table above for Brazilian requests made to LinkedIn during the reporting period of July through December 2021._