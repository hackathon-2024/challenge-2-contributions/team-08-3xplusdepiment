platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Government requests for data

#### 2016: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 3   | 4   | 33% | 1   |
| Canada | 1   | 1   | 100% | 1   |
| France | 2   | 2   | 50% | 1   |
| Germany | 2   | 2   | 0%  | 0   |
| India | 6   | 7   | 0%  | 0   |
| Poland | 1   | 1   | 0%  | 0   |
| Spain | 1   | 1   | 0%  | 0   |
| Turkey | 1   | 1   | 0%  | 0   |
| United States | 128 | 291 | 68% | 150 |
| **Total** | **145** | **310** | **62%** | **153** |