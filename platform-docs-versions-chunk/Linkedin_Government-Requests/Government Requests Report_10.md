platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Requests by country and accounts subject to those requests

#### 2019: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 11  | 11  | 82% | 9   |
| Canada | 1   | 1   | 0%  | 0   |
| China | 1   | 1   | 0%  | 0   |
| France | 8   | 8   | 88% | 7   |
| Germany | 33  | 81  | 85% | 71  |
| India | 9   | 11  | 0%  | 0   |
| Ireland | 1   | 4   | 0%  | 0   |
| Malta | 1   | 1   | 0%  | 0   |
| Monaco | 1   | 1   | 100% | 1   |
| Netherlands | 5   | 5   | 80% | 4   |
| Spain | 6   | 7   | 17% | 1   |
| UK  | 4   | 4   | 0%  | 0   |
| US  | 349 | 1,488 | 82% | 819 |
| **Total** | **430** | **1,623** | **78%** | **912** |