platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


### Requests by country and accounts subject to those requests

#### 2022: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 1   | 23  | 0%  | 0   |
| Austria | 1   | 1   | 100% | 1   |
| Belgium | 2   | 2   | 100% | 2   |
| Brazil | 22  | 34  | 59% | 13  |
| Canada | 3   | 3   | 33% | 1   |
| China | 2   | 4   | 50% | 1   |
| Czech Republic | 1   | 1   | 100% | 1   |
| France | 21  | 23  | 71% | 15  |
| Germany | 54  | 63  | 78% | 48  |
| India | 65  | 134 | 26% | 21  |
| Ireland | 1   | 1   | 0%  | 0   |
| Italy | 34  | 34  | 44% | 15  |
| Jordan | 2   | 2   | 0%  | 0   |
| Netherlands | 5   | 50  | 100% | 48  |
| Pakistan | 1   | 1   | 0%  | 0   |
| Poland | 1   | 1   | 0%  | 0   |
| Portugal | 1   | 3   | 0%  | 0   |
| Romania | 1   | 1   | 0%  | 0   |
| Spain | 10  | 15  | 70% | 12  |
| Switzerland | 2   | 3   | 0%  | 0   |
| Turkey | 1   | 1   | 100% | 1   |
| United Kingdom | 27  | 45  | 44% | 17  |
| United States | 391 | 2636 | 80% | 949 |
| Venezuela | 1   | 1   | 0%  | 0   |
| **Total** | **649** | **3081** | **69%** | **1144** |