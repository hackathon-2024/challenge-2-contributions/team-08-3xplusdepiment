platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report


### Government requests for data

#### 2014: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts impacted (LinkedIn provided some data) \[2\] |
| Argentina | 2   | 2   | 0%  | 0   |
| Australia | 2   | 2   | 50% | 1   |
| Brazil | 1   | 1   | 100% | 1   |
| Canada | 4   | 51  | 35% | 45  |
| France | 4   | 4   | 0%  | 0   |
| Germany | 1   | 1   | 0%  | 0   |
| Hong Kong | 1   | 1   | 0%  | 0   |
| India | 4   | 4   | 25% | 1   |
| Spain | 1   | 8   | 0%  | 0   |
| United Kingdom | 1   | 1   | 100% | 1   |
| United States | 85  | 1,069 | 65% | 101 |
| **Total** | **116** | **1,144** | **52%** | **150** |

\[1\] This column was previously labeled "Accounts Impacted", but we changed the name to clarify that it reflects the number of accounts subject to the data requests, and not the number of accounts for which some responsive data was in fact provided.

  
\[2\] We started reporting the number of accounts for which at least some data was provided in response to government requests in our January-June 2014 Transparency Report.