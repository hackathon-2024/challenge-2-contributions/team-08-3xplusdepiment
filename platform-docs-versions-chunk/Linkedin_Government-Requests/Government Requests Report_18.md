platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Government requests for data

#### 2015: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts impacted (LinkedIn provided some data) \[2\] |
| Brazil | 1   | 1   | 0%  | 0   |
| China | 1   | 1   | 0%  | 0   |
| France | 1   | 1   | 0%  | 0   |
| Germany | 2   | 2   | 0%  | 0   |
| India | 5   | 5   | 0%  | 0   |
| Liechtenstein | 1   | 1   | 0%  | 0   |
| United Kingdom | 1   | 1   | 0%  | 0   |
| United States | 127 | 226 | 72% | 134 |
| **Total** | **139** | **238** | **66%** | **134** |