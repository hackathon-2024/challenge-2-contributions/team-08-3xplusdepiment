platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

# Government Requests Report

  
This report discusses how we respond to government requests for member data and removal of content. As part of our commitment to a safe, trusted, and professional environment, we provide transparency about government requests for member data and content removal we receive. We believe in free expression and try to minimize the impact of laws that negatively affect constructive, relevant speech.

We respect the laws that apply to us in the countries where we operate. In the U.S., we respond to several types of legal process at the federal, state, and local levels. Most often, we’re responding to search warrants and subpoenas. Outside the U.S., we ask the government that’s requesting information to issue its request properly, for example through a Mutual Legal Assistance Treaty or a form of international process known as a letter rogatory. Sometimes, we’ll make an exception in an emergency. For more information, see our [Law Enforcement Data Request Guidelines](https://www.linkedin.com/help/linkedin/answer/16880/linkedin-law-enforcement-data-request-guidelines?lang=en).  
  
We never provide information to a government to fulfill a member data request without first trying to notify the individual member, except when applicable law prohibits us from doing so. We may make exceptions in certain emergencies.  
  
This report covers the six-month period between January 1 and June 30, 2023.