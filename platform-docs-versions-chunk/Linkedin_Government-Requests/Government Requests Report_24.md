platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### By country and action taken

#### 2022: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Australia | 2   | 2   | 100% |
| Canada | 1   | 1   | 100% |
| India | 6   | 4   | 67% |
| Indonesia | 4   | 4   | 100% |
| Turkey | 10  | 9   | 90% |
| **Totals** | **23** | **20** | **87%** |

### By country and action taken

#### 2022: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Australia | 1   | 1   | 100% |
| Brazil | 1   | 1   | 100% |
| France | 1   | 1   | 100% |
| Germany | 1   | 1   | 100% |
| Ghana | 1   | 0   | 0%  |
| India | 12  | 10  | 83% |
| Monaco | 1   | 0   | 0%  |
| Turkey | 8   | 8   | 100% |
| UK  | 1   | 1   | 100% |
| **Totals** | **27** | **23** | **85%** |