platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### Requests by country and accounts subject to those requests

#### 2017: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 3   | 3   | 33% | 1   |
| China | 1   | 1   | 100% | 1   |
| Czech Republic | 1   | 1   | 0%  | 0   |
| France | 1   | 1   | 0%  | 0   |
| Germany | 2   | 2   | 50% | 1   |
| India | 3   | 3   | 0%  | 0   |
| Ireland | 1   | 1   | 100% | 1   |
| Italy | 1   | 1   | 100% | 1   |
| Netherlands | 1   | 1   | 0%  | 0   |
| Poland | 1   | 1   | 100% | 1   |
| Spain | 3   | 3   | 67% | 2   |
| United Kingdom | 2   | 2   | 0%  | 0   |
| United States | 187 | 594 | 71% | 229 |
| **Total** | **207** | **614** | **68%** | **237** |