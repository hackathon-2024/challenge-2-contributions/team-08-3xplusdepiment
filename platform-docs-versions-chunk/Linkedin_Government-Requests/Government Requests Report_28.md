platform: Linkedin
topic: Government-Requests
subtopic: Government Requests Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Linkedin_Government-Requests/Government Requests Report.md
url: https://about.linkedin.com/transparency/government-requests-report

### By country and action taken

#### 2018: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Canada | 1   | 1   | 100% |
| China | 9   | 9   | 100% |
| India | 2   | 1   | 50% |
| Indonesia | 1   | 1   | 100% |
| United Kingdom | 1   | 1   | 100% |
| United States | 1   | 1   | 100% |
| **Totals** | **15** | **14** | **93.33%** |

• We started reporting on content-related requests in our January 1-June 30, 2018 Transparency Report.

  
• Government Requests for Content Removal are requests received by LinkedIn from governments seeking the removal of content, including reporting violations of our Terms of Service or violations of local law.