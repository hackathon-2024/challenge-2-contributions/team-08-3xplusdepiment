platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/libraries

## Featured samples for this API

Often, the easiest way to learn how to use an API can be to look at sample code. The table above provides links to some basic samples for each of the languages shown. Currently, there are no additional featured samples available for the Custom Search JSON API.