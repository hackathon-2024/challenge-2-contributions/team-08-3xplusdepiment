platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/overview

## Pricing

Custom Search JSON API provides 100 search queries per day for free. If you need more, you may sign up for [billing](https://cloud.google.com/billing/docs/how-to/manage-billing-account) in the API Console. Additional requests cost $5 per 1000 queries, up to 10k queries per day.