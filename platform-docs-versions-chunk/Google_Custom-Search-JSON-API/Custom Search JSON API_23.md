platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/site_restricted_api

## Making a request

Making a request to Custom Search Site Restricted JSON API is similar to making a request to Custom Search JSON API; however, the URI is different. The format for the Custom Search Site Restricted JSON API is

https://www.googleapis.com/customsearch/v1/siterestrict?\[parameters\]

The `[parameters]` are the same as the [Custom Search JSON API](https://developers.google.com/custom-search/v1/using_rest) parameters

## Pricing

Custom Search Site Restricted JSON API requests cost $5 per 1000 queries and there is no daily query limit. You may sign up for [billing](https://cloud.google.com/billing/docs/how-to/manage-billing-account) in the API Console.