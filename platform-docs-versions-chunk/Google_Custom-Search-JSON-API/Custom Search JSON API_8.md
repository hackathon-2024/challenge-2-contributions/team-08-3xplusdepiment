platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/introduction

## Try it

To play around and see what the API can do, without writing any code, visit the ["Try this API" tool](https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list?apix=true).

For a full description of parameters visit the [cse.list reference](https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list).

To learn how to use the API via HTTP requests, continue to [Using REST](https://developers.google.com/custom-search/v1/using_rest).