platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/overview

The Custom Search JSON API lets you develop websites and applications to retrieve and display search results from Programmable Search Engine programmatically. With this API, you can use RESTful requests to get either **web search** or **image search** results in JSON format.

## Data format

Custom Search JSON API can return results in [JSON](https://developers.google.com/custom-search/docs/glossary#json) data format.

## Related documents

The Custom Search JSON API uses the [OpenSearch 1.1 Specification](https://github.com/dewitt/opensearch/blob/master/opensearch-1-1-draft-6.md).

## Prerequisites