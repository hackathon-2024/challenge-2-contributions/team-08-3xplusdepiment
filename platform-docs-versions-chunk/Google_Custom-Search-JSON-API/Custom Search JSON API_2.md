platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/overview

### Search engine ID

Before using the Custom Search JSON API you will first need to create and configure your Programmable Search Engine. If you have not already created a Programmable Search Engine, you can start by visiting the [Programmable Search Engine control panel](https://programmablesearchengine.google.com/controlpanel/all).

Follow the [tutorial](https://developers.google.com/custom-search/docs/tutorial/creatingcse) to learn more about different configuration options.

After you have created a Programmable Search Engine, visit the [help center](https://support.google.com/programmable-search/answer/2649143) to learn how to locate your Search engine ID.

### API key

Custom Search JSON API requires the use of an API key. Get a Key