platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/using_rest

## Response data

If the request succeeds, the server responds with a `200 OK` HTTP status code and the response data in JSON format. You can look up the response data structure in the [reference](https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list#response).

The response data is a JSON object that includes three types of properties:

* Metadata describing the requested search (and, possibly, related search requests)
* Metadata describing the Programmable Search Engine
* Search results

For a detailed description of each property, see the [reference](https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list#response).