platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/overview

## Monitoring

Basic monitoring for the Custom Search JSON API is available through [Cloud Platform Console's API Dashboard](https://console.cloud.google.com/apis/dashboard). For more advanced monitoring [Google Cloud's Operations suite](https://cloud.google.com/products/operations) (formerly Stackdriver) is available.

With Google Cloud Operations you can create [custom dashboards](https://cloud.google.com/monitoring/charts), [set up alerts](https://cloud.google.com/monitoring/alerts), and [access metrics data programmatically](https://cloud.google.com/monitoring/api/v3). To access Custom Search JSON API usage data in Google Cloud Operations, select "Resource type: Consumed API" and filter on "service = 'customsearch.googleapis.com'" in the Query Builder.

See [Monitoring Your API Usage](https://cloud.google.com/apis/docs/monitoring) for a discussion of the different monitoring and alerting capabilities provided by the API Dashboard and the Google Cloud Operations suite.