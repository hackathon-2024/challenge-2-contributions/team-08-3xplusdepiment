platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/site_restricted_api

**Note:** The Custom Search Site Restricted JSON API endpoints will cease to serve traffic on December 18, 2024.

All Custom Search Site Restricted JSON API customers must begin their transition to [Google Cloud's Vertex AI Search](https://cloud.google.com/enterprise-search). Detailed transition guidance can be found [here](https://cloud.google.com/generative-ai-app-builder/docs/migrate-from-cse). If you experience issues, you can contact us [here](https://cloud.google.com/generative-ai-app-builder/docs/support).

This document describes how to use the Custom Search Site Restricted JSON API.