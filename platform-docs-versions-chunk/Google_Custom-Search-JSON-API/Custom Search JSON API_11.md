platform: Google
topic: Custom-Search-JSON-API
subtopic: Custom Search JSON API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Google_Custom-Search-JSON-API/Custom Search JSON API.md
url: https://developers.google.com/custom-search/v1/using_rest

## Query parameters

There are two types of parameters that you can pass in your request:

* API-specific parameters - define properties of your search, like the search expression, number of results, language etc.
* Standard query parameters - define technical aspects of your request, like the API key.

All parameter values need to be URL encoded.

### API-specific query parameters

Request parameters that apply specifically to the Custom Search JSON API and define your search request are summarized in the [reference](https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list#request).

### Standard query parameters

Query parameters that apply to all Custom Search JSON API operations are documented at [System Parameters](https://cloud.google.com/apis/docs/system-parameters).