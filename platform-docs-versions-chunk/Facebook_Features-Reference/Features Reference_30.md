platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/page-public-metadata-access

## Allowed Usage

* Analyze engagement with public Pages by viewing Like and follower counts.
    
* Aggregate public-facing "about" Page information from multiple, disparate pages.
    

## Common Endpoints

[/page](https://developers.facebook.com/docs/graph-api/reference/page)