platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/instagram-public-content-access

## Allowed Usage

* Discover content associated with its current campaign.
    
* Provide customer support.
    
* Identify entrants to its contests, competitions, or sweepstakes.
    
* Understand public sentiment around brand.
    
* Understand and manage their audience, develop their content strategy and obtain digital rights.
    

## Common Endpoints

* [/ig-hashtag-search](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag-search)
    
* [/ig-hashtag](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag)
    
* [/ig-hashtag/recent-media](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag/recent-media)
    
* [/ig-hashtag/top-media](https://developers.facebook.com/docs/instagram-api/reference/ig-hashtag/top-media)