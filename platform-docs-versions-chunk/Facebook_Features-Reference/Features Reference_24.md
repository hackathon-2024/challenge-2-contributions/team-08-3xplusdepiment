platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/page-mentioning

# Page Mentioning

The **Page Mentioning** feature allows your app mention any Facebook Page when publishing posts on the Pages managed by your app. To use Page Mentioning, your app needs to have been granted the **manage\_pages** and **publish\_pages** permissions. The allowed usage for this feature is to to let people use your app to publish Page posts that mention other Pages or to mention Pages relevant to the content in your page post. You may also use this permission to request analytics insights to improve your app and for marketing or advertising purposes, through the use of aggregated and de-identified or anonymized information (provided such data cannot be re-identified).

## Allowed Usage

* Allow people to use your app to publish Page posts that mention other Pages.
    
* Mention Pages relevant to the content in your page post.