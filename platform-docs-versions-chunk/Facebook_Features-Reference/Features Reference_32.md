platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/threat-exchange

# Threat Exchange

_Requires [App Review](https://developers.facebook.com/docs/app-review)._

The **ThreatExchange** feature allows your app to share threat data among a select group of vetted industry partners. The allowed usage for this feature is to share threat data with a specific group of partners to achieve their security goals. You may also use this feature to request analytics insights to improve your app and for marketing or advertising purposes, through the use of aggregated and de-identified or anonymized information (provided such data cannot be re-identified).

## Allowed Usage

* Share threat data with a specific group of partners to achieve their security goals.