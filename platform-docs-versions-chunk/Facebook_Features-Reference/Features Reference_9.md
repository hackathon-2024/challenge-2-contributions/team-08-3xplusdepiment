platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/business-asset-user-profile-access

# Business Asset User Profile Access

_Requires [App Review](https://developers.facebook.com/docs/app-review)._

The **Business Asset User Profile Access** feature allows your app to read the User Fields for users engaging with your business assets such as id, ids\_for\_business, name, and picture. The allowed usage for this feature is to read one or more of the User Fields in a business app experience. You may also use this feature to request analytics insights to improve your app and for marketing or advertising purposes, through the use of aggregated and de-identified or anonymized information (provided such data cannot be re-identified).

## Common Endpoints

[/User](https://developers.facebook.com/docs/graph-api/reference/user#fields)

## Allowed Usage

You can use this feature if your app uses one or more of the User Fields in its business app experience.