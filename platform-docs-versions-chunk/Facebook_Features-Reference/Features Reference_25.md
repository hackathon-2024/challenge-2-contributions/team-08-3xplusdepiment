platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/page-mentioning

## Common Endpoints

[/page/feed](https://developers.facebook.com/docs/graph-api/reference/page/feed)  
[/page-post](https://developers.facebook.com/docs/graph-api/reference/page-post)  
[/page-post/comments](https://developers.facebook.com/docs/graph-api/reference/object/comments)