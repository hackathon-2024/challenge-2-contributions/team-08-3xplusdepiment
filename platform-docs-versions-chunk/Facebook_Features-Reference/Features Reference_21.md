platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/oembed-read

# oEmbed Read

_Requires [App Review](https://developers.facebook.com/docs/app-review)._

The **oEmbed Read** feature allows your app to get embed HTML and basic metadata for public Facebook and Instagram pages, posts, and videos. The allowed usage for this feature is to provide front-end views of Facebook and Instagram pages, posts, and videos. You may also use this permission to request analytics insights to improve your app and for marketing or advertising purposes, through the use of aggregated and de-identified or anonymized information (provided such data cannot be re-identified).

## Allowed Usage

* Provide front-end views of Facebook and Instagram pages, posts, and videos.