platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/groups-api

Deprecated for v19.0. Will be removed for all version April 22, 2024.

# Groups API

_Requires [App Review](https://developers.facebook.com/docs/app-review)._

The **Groups API** feature allows your app to access content in a Facebook Group. The allowed usage for this feature is to help people manage the posts and content published to their group or to get information about content posted to their Group. It can also be used to let people publish content from your app to their Facebook Group. This feature is often used with the **publish\_to\_group** and **groups\_access\_member\_info** permissions. You may also use this permission to request analytics insights to improve your app and for marketing or advertising purposes, through the use of aggregated and de-identified or anonymized information (provided such data cannot be re-identified).