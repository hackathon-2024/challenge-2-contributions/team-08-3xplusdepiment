platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/human-agent

# Human Agent

The **Human Agent** feature allows your app to have a human agent respond to user messages using the **human\_agent** tag within 7 days of a user's message. The allowed usage for this feature is to provide human agent support in cases where a user’s issue cannot be resolved in the standard messaging window. Examples include when the business is closed for the weekend, or if the issue requires more than 24 hours to resolve.

## Allowed Usage

* Provide human agent support in cases where a user’s issue cannot be resolved in the standard messaging window.
    

## Common Endpoints

[/page/messages](https://developers.facebook.com/docs/graph-api/reference/page/messages)