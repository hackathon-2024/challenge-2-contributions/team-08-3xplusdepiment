platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/live-video-api

# Live Video API

_Requires [App Review](https://developers.facebook.com/docs/app-review)._

The **Live Video API** feature allows an app to manage live videos to Pages, Groups and User timelines when combined with the correct matching permission.

## Allowed Usage

* App users can publish live video content from your app to Facebook.
    

## Common Endpoints

[/group/live\_videos](https://developers.facebook.com/docs/graph-api/reference/group/live_videos)  
[/live-video](https://developers.facebook.com/docs/graph-api/reference/live-video)  
[/page/live\_videos](https://developers.facebook.com/docs/graph-api/reference/page/live_videos)  
[/user/live\_videos](https://developers.facebook.com/docs/graph-api/reference/user/live_videos)