platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/groups-api

## Allowed Usage

* Help people manage the posts and content published to their Group.
    
* Help people get information about content posted to their Group.
    
* Let people publish content from your app to their Facebook Group.
    
* Help people get aggregated insights about activity happening in their Group.
    

## Common Endpoints

[/group](https://developers.facebook.com/docs/graph-api/reference/group)