platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/page-public-content-access

# Page Public Content Access

The **Page Public Content Access** feature allows an app access to the Pages Search API and to read public data for Pages for which you lack the **pages\_read\_engagement** permission and the **pages\_read\_user\_content** permission. Readable data includes business metadata, public comments and posts. The allowed usage for this feature is to analyze and/or display posts and engagement on Pages.

## Allowed Usage

* Analyze and/or display posts and engagement on Pages.
    

## Common Endpoints

[/page/feed](https://developers.facebook.com/docs/graph-api/reference/page/feed)  
[/page-post](https://developers.facebook.com/docs/graph-api/reference/page-post)  
[/page-post/comments](https://developers.facebook.com/docs/graph-api/reference/object/comments)