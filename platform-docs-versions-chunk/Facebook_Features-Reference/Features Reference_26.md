platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/page-mentioning


## Additional Details

* This permission or feature requires successful completion of the App Review process before your app can access live data. [Learn More](https://developers.facebook.com/docs/app-review)
    
* This permission or feature is only available with business verification. You may also need to sign additional contracts before your app can access data. [Learn More Here](https://developers.facebook.com/docs/development/release/business-verification)
    
* To use Page Mentioning, your app needs to have been granted the [`pages_read_engagement`](https://developers.facebook.com/docs/permissions/reference/pages_read_engagement) and [`pages_manage_posts`](https://developers.facebook.com/docs/permissions/reference/pages_manage_posts) permissions.
    
* If your app has its own user authentication system, please include a working username and password in your review instructions so our team can easily reproduce your page mentioning functionality.
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)