platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/ads-management-standard-access

## Allowed Usage

* Enable an unlimited number of ad accounts and lower rate limiting.
    
* To read ads reports for ad accounts you own or have been granted access to by the ad account owner, request **Ads Management Standard Access**, along with the **ads\_read** permission.
    
* To read and manage ads for ad accounts you own or have been granted access to by the ad account owner, request **Ads Management Standard Access**, along with the **ads\_management** permission.
    
* To pull ads reports from a set of clients, and to read and manage ads from another set of clients, request **Ads Management Standard Access**, along with both **ads\_read** and **ads\_management** permissions.
    

## Common Endpoints

[/adaccount](https://developers.facebook.com/docs/graph-api/reference/adaccount)  
[/adaccount/adcreatives/](https://developers.facebook.com/docs/graph-api/reference/adaccount/adcreatives/)  
[/adaccount/ads](https://developers.facebook.com/docs/graph-api/reference/adaccount/ads)