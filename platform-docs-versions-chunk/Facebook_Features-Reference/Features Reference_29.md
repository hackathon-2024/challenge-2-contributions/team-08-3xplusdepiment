platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/page-public-metadata-access

# Page Public Metadata Access

_Requires [App Review](https://developers.facebook.com/docs/app-review)._

The **Page Public Metadata Access** allows your app access to the [Pages Search API](https://developers.facebook.com/docs/pages/searching) and to read public data for Pages for which you lack the [pages\_read\_engagement permission](https://developers.facebook.com/docs/permissions/reference/pages_read_engagement) and the [pages\_read\_user\_content permission](https://developers.facebook.com/docs/permissions/reference/pages_read_user_content). The allowed usage for this feature is to analyze engagement with public Pages by viewing Like and follower counts, or aggregate public-facing **About** Page information from multiple, disparate pages. You may also use this permission to request analytics insights to improve your app and for marketing or advertising purposes, through the use of aggregated and de-identified or anonymized information (provided such data cannot be re-identified).