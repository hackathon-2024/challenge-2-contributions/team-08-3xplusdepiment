platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/oembed-read

## Common Endpoints

* [/oembed\_page](https://developers.facebook.com/docs/graph-api/reference/oembed-page/)
    
* [/oembed\_post](https://developers.facebook.com/docs/graph-api/reference/oembed-post/)
    
* [/oembed\_video](https://developers.facebook.com/docs/graph-api/reference/oembed-video/)
    
* [/instagram\_oembed](https://developers.facebook.com/docs/graph-api/reference/instagram-oembed/)