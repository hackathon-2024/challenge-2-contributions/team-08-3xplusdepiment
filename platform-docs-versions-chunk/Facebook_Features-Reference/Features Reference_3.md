platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference


## Learn More

Learn more about developing with Meta with the following guides:

|     |     |     |
| --- | --- | --- |
| * [Access Levels](https://developers.facebook.com/docs/graph-api/overview/access-levels/#development-mode-and-live-mode)<br>* [App Review](https://developers.facebook.com/docs/app-review)<br>* [App Roles](https://developers.facebook.com/docs/development/build-and-test/app-roles) | * [App Types](https://developers.facebook.com/docs/development/create-an-app#app-type)<br>* [Business Roles](https://www.facebook.com/business/help/442345745885606?id=180505742745347)<br>* [Development Mode](https://developers.facebook.com/docs/development/build-and-test/app-modes) | * [Live Mode](https://developers.facebook.com/docs/development/build-and-test/app-modes)<br>* [Permissions Reference](https://developers.facebook.com/docs/permissions/reference) |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)