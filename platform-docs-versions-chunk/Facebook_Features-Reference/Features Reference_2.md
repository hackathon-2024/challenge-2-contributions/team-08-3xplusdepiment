platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference

## Deprecated Features

| Feature | Deprecation Date |
| --- | --- |
| All Mutual Friends API | October 24th, 2018 |
| Profile Expression Kit | September 30th, 2018 |
| Optimized Sharing to Messenger | August 1st, 2018 |
| Taggable Friends | April 4th, 2018 |