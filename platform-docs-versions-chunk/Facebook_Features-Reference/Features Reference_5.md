platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/ad-targeting-data-access

## Additional Details

* This permission or feature requires successful completion of the App Review process before your app can access live data. [Learn More](https://developers.facebook.com/docs/app-review)
    
* This permission or feature requires that you complete [business verification](https://developers.facebook.com/docs/development/release/business-verification) in addition to App Review.
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)