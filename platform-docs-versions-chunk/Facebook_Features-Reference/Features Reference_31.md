platform: Facebook
topic: Features-Reference
subtopic: Features Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Features-Reference/Features Reference.md
url: https://developers.facebook.com/docs/features-reference/page-public-metadata-access


## Additional Details

* This permission or feature requires successful completion of the App Review process before your app can access live data. [Learn More](https://developers.facebook.com/docs/app-review)
    
* This permission or feature is only available with business verification. You may also need to sign additional contracts before your app can access data. [Learn More Here](https://developers.facebook.com/docs/development/release/business-verification)
    
* If your app also needs to read the [Page Feed](https://developers.facebook.com/docs/graph-api/reference/page/feed) edge, or [Comments](https://developers.facebook.com/docs/graph-api/reference/comment) on a Page's [Posts](https://developers.facebook.com/docs/graph-api/reference/post), request the [Page Public Content Access](#page-public-content-access) feature instead.
    
* This feature is superseded by the Page Public Content Access (PPCA) feature. If your App Review submission includes PPCA, or your app has already been approved for PPCA, you cannot request this permission.
    
* If your app also needs to create, update, or delete data on a Page, request the [`pages_read_engagement`](https://developers.facebook.com/docs/permission/reference/pages_read_engagement) permission instead.
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)