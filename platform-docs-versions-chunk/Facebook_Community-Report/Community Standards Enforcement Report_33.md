platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/bullying-and-harassment/facebook/

## appealed content

How much of the content we actioned for bullying and harassment did people appeal?

How we calculate it

Appealed content counts the number of pieces of content actioned which were submitted for another review during the reporting period.

[Read about this data](https://transparency.fb.com/policies/improving/appealed-content-metric/)