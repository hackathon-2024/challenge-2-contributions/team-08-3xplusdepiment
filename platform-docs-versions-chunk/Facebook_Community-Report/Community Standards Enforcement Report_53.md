platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/fake-accounts/facebook/

# Fake Accounts

Our goal is to remove as many fake accounts on Facebook as we can. These include accounts created with malicious intent to violate our policies and personal profiles created to represent a business, organization or non-human entity, such as a pet.

We prioritize enforcement against fake accounts that seek to cause harm. Many of these accounts are used in spam campaigns and are financially motivated.

We expect the number of accounts we action to vary over time due to the unpredictable nature of adversarial account creation. Our detection technology helps us block millions of attempts to create fake accounts every day and detect millions more, often within minutes after creation. We do not include blocked attempts in the metrics we report here.

[Read the policy details](https://transparency.fb.com/policies/community-standards/account-integrity-and-authentic-identity/)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)

## Recent trends