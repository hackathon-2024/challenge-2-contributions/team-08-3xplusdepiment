platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/spam/facebook/

[Home](https://transparency.fb.com/)

[Data](https://transparency.fb.com/reports/)

[Community Standards Enforcement Report](https://transparency.fb.com/reports/community-standards-enforcement/)

# Spam

We do not allow spam on Facebook. Spam is a broad term used to describe content that is designed to be shared in deceptive and annoying ways or attempts to mislead users to drive engagement. Spam spreads in a number of ways: It can be automated (published by bots or scripts) or coordinated (when an actor uses multiple accounts to spread deceptive content).

Spammers aim to build audiences to inflate their content's distribution and reach, typically for financial gain. The tactics spammers use, and our ability to detect them, drive the amount of content we take action on as well as our proactive rate.

[Read the policy details](https://transparency.fb.com/policies/community-standards/spam/)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)

## Recent trends