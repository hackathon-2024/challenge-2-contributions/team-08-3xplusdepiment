platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/bullying-and-harassment/facebook/


# Bullying and Harassment

We do not tolerate bullying and harassment on Facebook and Instagram. Because we recognize bullying can be especially harmful for minors, our policies provide heightened protections for them. We want to allow for open and vital discussion of people who are in the news or who have a large public audience, so we do permit more open or critical discourse towards public figures than private individuals.

Because bullying and harassment is highly personal by nature, using technology to proactively detect these behaviors can be more challenging than other types of violations. That's why we also rely on people to report this behavior to us so we can identify and remove it. When measuring prevalence in this area, the metric captures only bullying and harassment where a deeper understanding of context or meaning is not necessary to determine if it violates our policy. We continue to invest in our proactive detection technology to ensure we are tackling the problem and protecting our community.

[Read the policy details](https://transparency.fb.com/policies/community-standards/bullying-harassment)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)