platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Content actioned decreased from 1.1 million in Q2 2023 to 750K in Q3 2023, returning to Q2 levels after a spike in reported viral links in April.

[](https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/facebook/#content-actioned)

## Q3 2023

129K

## 

Content Actioned on Dangerous Organizations and Individuals: Organized Hate

## 

Content actioned decreased from 215K in Q2 2023 to 129K in Q3 2023, returning to pre-Q3 levels after a spike in reported viral links in April.

[](https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/instagram/#content-actioned)

## Q3 2023

77.2%

## 

Proactive Rate on Dangerous Organizations and Individuals: Organized Hate