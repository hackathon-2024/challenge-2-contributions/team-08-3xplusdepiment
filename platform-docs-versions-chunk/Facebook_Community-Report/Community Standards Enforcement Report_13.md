platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Appealed content increased from 625K in Q2 2023 to 977K in Q3 2023, as we increased our appeals period to adhere to Digital Services Act.

[](https://transparency.fb.com/reports/community-standards-enforcement/hate-speech/instagram/#appealed-content)

## Q3 2023

87.7K

## 

Restored Content on Hate Speech

## 

Restored content decreased from 3.92 million in Q2 2023 to 87.7K in Q3 2023, returning to pre-Q2 levels following an increase in enforcement on non-violating content due to a bug in our proactive detection technology that was later fixed and the content was restored. This impacted both platforms in Q2.

[](https://transparency.fb.com/reports/community-standards-enforcement/hate-speech/instagram/#restored-content)

## Q3 2023

290K

## 

Appealed Content on Restricted Goods and Services: Drugs