platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/graphic-violence/facebook/

[Home](https://transparency.fb.com/)

[Data](https://transparency.fb.com/reports/)

[Community Standards Enforcement Report](https://transparency.fb.com/reports/community-standards-enforcement/)

# Violent and Graphic Content

We remove content that glorifies violence or celebrates the suffering or humiliation of others on Facebook and Instagram. We do allow people to share some graphic content to raise awareness about current events and issues. In these cases, we may hide the content from people under 18 and cover it with a warning for those over 18, so people are aware it is graphic or violent before they choose to view it.

[Read the policy details](https://transparency.fb.com/policies/community-standards/violent-graphic-content/)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)

## Recent trends

## Q3 2023

9 million

## 

Content Actioned on Violent and Graphic Content