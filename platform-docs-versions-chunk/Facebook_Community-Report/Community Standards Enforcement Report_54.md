platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/fake-accounts/facebook/

## Q3 2023

827 million

## 

Accounts Actioned on Fake Accounts

## 

Accounts actioned increased from 676 million in Q2 2023 to 827 million in Q3 2023. Fluctuations in enforcement metrics for fake accounts are expected due to the highly adversarial nature of this space.

[](https://transparency.fb.com/reports/community-standards-enforcement/fake-accounts/facebook/#content-actioned)

## prevalence

How prevalent were fake account violations?

We estimate that fake accounts represented approximately 4-5% of our worldwide monthly active users (MAU) on Facebook during Q3 2023.

## accounts actioned

How many fake accounts did we take action on?

How we calculate it

Accounts actioned is the total number of accounts that Facebook took action on for fake accounts. It includes both accounts we actioned after someone reported them, and accounts that we found proactively.

[Read about this data](https://transparency.fb.com/policies/improving/content-actioned-metric/)