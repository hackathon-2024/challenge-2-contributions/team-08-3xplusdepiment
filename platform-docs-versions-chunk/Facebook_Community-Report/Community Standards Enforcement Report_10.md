platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Restored content decreased from 643K in Q2 2023 to 23.7K in Q3 2023, returning to pre-Q2 levels after an increase in enforcement on non-violating content due to a bug in our proactive detection technology that was later fixed and the content was restored in Q2.

[](https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/instagram/#restored-content)

## Q3 2023

827 million

## 

Accounts Actioned on Fake Accounts

## 

Accounts actioned increased from 676 million in Q2 2023 to 827 million in Q3 2023. Fluctuations in enforcement metrics for fake accounts are expected due to the highly adversarial nature of this space.

[](https://transparency.fb.com/reports/community-standards-enforcement/fake-accounts/facebook/#content-actioned)

## Q3 2023

9.6 million

## 

Content Actioned on Hate Speech