platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/adult-nudity-and-sexual-activity/facebook/

## proactive rate

Of the violating content we actioned for adult nudity and sexual activity, how much did we find and action before people reported it?

Found and actioned by us

Reported by users

How we calculate it

Proactive rate is the number of pieces of content acted on that we found and actioned before people using Facebook reported them, divided by the total number of pieces of content we took action on.

[Read about this data](https://transparency.fb.com/policies/improving/proactive-rate-metric/)

Correcting mistakes

People can appeal our decisions, unless there are extreme safety concerns. We restore content we incorrectly removed or when circumstances change. Restores can happen from appeals or when we identify issues ourselves.