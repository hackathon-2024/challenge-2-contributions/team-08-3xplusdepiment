platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/facebook/

[Home](https://transparency.fb.com/)

[Data](https://transparency.fb.com/reports/)

[Community Standards Enforcement Report](https://transparency.fb.com/reports/community-standards-enforcement/)