platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

[Home](https://transparency.fb.com/)

[Data](https://transparency.fb.com/reports/)

[Community Standards Enforcement Report](https://transparency.fb.com/reports/community-standards-enforcement/)

# Community Standards Enforcement Report

We want Facebook and Instagram to be places where people have a voice. To create conditions where everyone feels comfortable expressing themselves, we must also protect their safety, privacy, dignity and authenticity. This is why we have the [Facebook Community Standards](https://transparency.fb.com/policies/community-standards/) and [Instagram Community Guidelines](https://www.facebook.com/help/instagram/477434105621119), which define what is and is not allowed in our community.

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)