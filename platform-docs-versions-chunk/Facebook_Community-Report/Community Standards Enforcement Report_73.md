platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/spam/facebook/

## 

Restored content decreased from 129 million in Q2 2023 to 16.5 million in Q3 2023, due to updates to our proactive detection technology to improve accuracy.

[](https://transparency.fb.com/reports/community-standards-enforcement/spam/facebook/#restored-content)

## prevalence

How prevalent were spam violations?

We cannot estimate this metric right now. We are working on new methods to measure the prevalence of spam on Facebook. Our existing methods for measuring prevalence, which rely on people to manually review samples of content, do not fully capture this type of highly adversarial violation, which includes deceptive behavior as well as content. Spammy behavior cannot always be detected by reviewing the content alone. We are working on ways to review and classify spammers' behavior to build a comprehensive picture.