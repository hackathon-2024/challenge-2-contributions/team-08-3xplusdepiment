platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## Q3 2023

16.5 million

## 

Restored Content on Spam

## 

Restored content decreased from 129 million in Q2 2023 to 16.5 million in Q3 2023, due to updates to our proactive detection technology to improve accuracy.

[](https://transparency.fb.com/reports/community-standards-enforcement/spam/facebook/#restored-content)

## Q3 2023

401K

## 

Appealed Content on Suicide and Self-Injury

## 

Appealed content increased from 236K in Q2 2023 to 401K in Q3 2023, as we increased our appeals period to adhere to the Digital Services Act.

[](https://transparency.fb.com/reports/community-standards-enforcement/suicide-and-self-injury/instagram/#appealed-content)

## Q3 2023

9 million

## 

Content Actioned on Violent and Graphic Content

## 

Content actioned decreased from 13.8 million in Q2 2023 to 9.0 million in Q3 2023, due to changes made to our enforcement systems.

[](https://transparency.fb.com/reports/community-standards-enforcement/graphic-violence/facebook/#content-actioned)

## Q3 2023

98.2%