platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/facebook/

# Dangerous Organizations: Terrorism and Organized Hate

We do not allow organizations or individuals that proclaim a violent mission, or are engaging in violence, to have a presence on Facebook and Instagram. We do not allow content that praises, supports or represents individuals or groups engaging in terrorist activity or organized hate.

This report does not include data on other dangerous organizations prohibited from having a presence on Facebook and Instagram, including those engaging in mass or multiple murder, human trafficking or organized criminal activity. [Learn more about our latest efforts enforcing our dangerous organizations policy.](https://about.fb.com/news/2020/05/combating-hate-and-dangerous-organizations)

[Read the policy details](https://transparency.fb.com/policies/community-standards/dangerous-individuals-organizations)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)

## Recent trends

## Q3 2023

750K