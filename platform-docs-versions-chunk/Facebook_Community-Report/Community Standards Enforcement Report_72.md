platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/spam/facebook/

## Q3 2023

413 million

## 

Content Actioned on Spam

## 

Content actioned decreased from 1.1 billion in Q2 2023 to 413 million in Q3 2023, due to a decrease in enforcement due to a bug in our proactive detection technology that was later fixed in August. Fluctuations in enforcement metrics for spam are expected due to the highly adversarial nature of this space.

[](https://transparency.fb.com/reports/community-standards-enforcement/spam/facebook/#content-actioned)

## Q3 2023

98.2%

## 

Proactive Rate on Spam

## 

Proactive rate increased from 95.3% in Q2 2023 to 98.2% in Q3 2023, due to an update in our calculation to the proactive rate.

[](https://transparency.fb.com/reports/community-standards-enforcement/spam/facebook/#proactive-rate)

## Q3 2023

16.5 million

## 

Restored Content on Spam