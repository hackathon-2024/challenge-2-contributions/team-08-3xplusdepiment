platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/adult-nudity-and-sexual-activity/facebook/

# Adult Nudity and Sexual Activity

We restrict the display of adult nudity and sexual activity on Facebook and Instagram. We make some exceptions when it is clear the content is being shared in the context of a protest, for educational or medical reasons or a similar reason. On the other hand, we default to removing sexual imagery to prevent non-consensual or underage content from being shared.

This report does not include metrics related to our separate policy on the [promotion of sexual assault, violence or exploitation](https://transparency.fb.com/policies/community-standards/sexual-exploitation-adults/).

[Read the policy details](https://transparency.fb.com/policies/community-standards/adult-nudity-sexual-activity)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)

## Recent trends

## Q3 2023

35.1 million

## 

Content Actioned on Adult Nudity and Sexual Activity