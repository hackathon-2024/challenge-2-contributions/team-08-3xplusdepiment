platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Content Actioned on Child Endangerment: Sexual Exploitation

## 

Content actioned increased from 7.2 million in Q2 2023 to 16.9 million in Q3 2023, due to a large takedown of coordinated behavior violating our policies.

[](https://transparency.fb.com/reports/community-standards-enforcement/child-nudity-and-sexual-exploitation/facebook/#content-actioned)

## Q3 2023

99%

## 

Proactive Rate on Child Endangerment: Sexual Exploitation

## 

Proactive rate increased from 96.9% in Q2 2023 to 99% in Q3 2023, due to an increase in proactive detection technology mentioned in content actioned.

[](https://transparency.fb.com/reports/community-standards-enforcement/child-nudity-and-sexual-exploitation/facebook/#proactive-rate)

## Q3 2023

267K

## 

Appealed Content on Child Endangerment: Sexual Exploitation