platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/bullying-and-harassment/facebook/

## Recent trends

## Q3 2023

87.8%

## 

Proactive Rate on Bullying and Harassment

## 

Proactive rate increased from 65.8% in Q2 2023 to 87.8% in Q3 2023, due to an update in our calculation to the proactive rate.

[](https://transparency.fb.com/reports/community-standards-enforcement/bullying-and-harassment/facebook/#proactive-rate)

## prevalence

How prevalent were bullying and harassment violations?

How we calculate it

Prevalence is the estimated number of views that showed violating content, divided by the estimated number of total content views on Facebook.

[Read about this data](https://transparency.fb.com/policies/improving/prevalence-metric/)