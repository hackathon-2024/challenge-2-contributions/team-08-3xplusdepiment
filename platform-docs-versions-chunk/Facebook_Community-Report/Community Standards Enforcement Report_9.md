platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Restored content decreased from 952K in Q2 2023 to 99K in Q3 2023, returning to pre-Q2 levels after an increase in enforcement on non-violating content due to a bug in our proactive detection technology that was later fixed and the content was restored in Q2.

[](https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/facebook/#restored-content)

## Q3 2023

831K

## 

Content Actioned on Dangerous Organizations and Individuals: Terrorism

## 

Content actioned decreased from 2 million in Q2 2023 to 831K in Q3 2023, due to a decline in content that violated our policies.

[](https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/instagram/#content-actioned)

## Q3 2023

23.7K

## 

Restored Content on Dangerous Organizations and Individuals: Terrorism