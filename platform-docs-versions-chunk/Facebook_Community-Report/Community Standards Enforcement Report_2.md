platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/


## Q3 2023 report

We publish the Community Standards Enforcement Report on a quarterly basis to more effectively track our progress and demonstrate our continued commitment to making Facebook and Instagram safe and inclusive.

#### What's new

In this November 2023 quarterly report, we share updated metrics for the reporting period from July to September 2023, detailing our progress on content that violates our policies.

#### Facebook and Instagram policies

Facebook and Instagram share content policies. Content that is considered violating on Facebook is also considered violating on Instagram. Throughout this report, we link to our Community Standards, which include the most comprehensive descriptions of these policies.

As we improve our methodologies, measure violations in more languages or across new parts of Facebook and Instagram and update our policies, the way we define and measure enforcement may change. As a result, historical comparisons may be imperfect.

#### Learn more

* [**Review EY’s independent, third-party assessment of our Community Standards Enforcement Report from the fourth quarter of 2021.**](https://about.fb.com/news/2022/05/community-standards-enforcement-report-assessment-results/)
    
* [**Review a report from independent academic experts on their findings and recommendations on our data transparency efforts**](https://law.yale.edu/yls-today/news/facebook-data-transparency-advisory-group-releases-final-report?fbclid=IwAR2xMZr5GdD1GaNpjsXR3_yeeIR4H9iFASfrni5HKcJVAO5oWA52bvwcZxU).
    

[14

Policies on Facebook](https://transparency.fb.com/data/community-standards-enforcement/adult-nudity-and-sexual-activity/facebook)

[12

Policies on Instagram](https://transparency.fb.com/data/community-standards-enforcement/adult-nudity-and-sexual-activity/instagram)

[Read our post about this report


---------------------------------](https://transparency.fb.com/integrity-reports-q3-2023)