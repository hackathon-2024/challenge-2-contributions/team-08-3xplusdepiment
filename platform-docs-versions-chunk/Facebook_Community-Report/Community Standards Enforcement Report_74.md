platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/spam/facebook/

## content actioned

How much spam content did we take action on?

How we calculate it

Content actioned is the total number of pieces of content that Facebook took action on for spam. It includes both content we actioned after someone reported it, and content that we found proactively.

[Read about this data](https://transparency.fb.com/policies/improving/content-actioned-metric/)