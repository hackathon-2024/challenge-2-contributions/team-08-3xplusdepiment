platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/hate-speech/facebook/

# Hate Speech

We do not allow hate speech on Facebook and Instagram. We define hate speech as violent or dehumanizing speech, statements of inferiority, calls for exclusion or segregation based on protected characteristics or slurs. These characteristics include race, ethnicity, national origin, religious affiliation, sexual orientation, caste, sex, gender, gender identity and serious disability or disease.

When the intent is clear, we may allow people to share someone else's hate speech content to raise awareness or discuss whether the speech is appropriate to use, to use slurs self-referentially in an effort to reclaim the term or for other similar reasons.

[Read the policy details](https://transparency.fb.com/policies/community-standards/hate-speech/)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)

## Recent trends

## Q3 2023

9.6 million

## 

Content Actioned on Hate Speech