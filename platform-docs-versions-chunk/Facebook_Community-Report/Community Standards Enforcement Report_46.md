platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/facebook/

## 

Restored content decreased from 952K in Q2 2023 to 99K in Q3 2023, returning to pre-Q2 levels after an increase in enforcement on non-violating content due to a bug in our proactive detection technology that was later fixed and the content was restored in Q2.

[](https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/facebook/#restored-content)

## prevalence

How prevalent were dangerous organizations violations?