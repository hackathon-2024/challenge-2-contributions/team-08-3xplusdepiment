platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Proactive Rate on Violent and Graphic Content

## 

Proactive rate increased from 97.5% in Q2 2023 to 98.2% in Q3 2023, due to an update in our calculation to the proactive rate.

[](https://transparency.fb.com/reports/community-standards-enforcement/graphic-violence/facebook/#proactive-rate)

## Q3 2023

4.1 million

## 

Content Actioned on Violent and Graphic Content

## 

Content actioned decreased from 6.2 million in Q2 2023 to 4.1 million in Q3 2023, due to changes made to our enforcement systems.

[](https://transparency.fb.com/reports/community-standards-enforcement/graphic-violence/instagram/#content-actioned)

## Q3 2023

8.6 million

## 

Content Actioned on Violence and Incitement

## 

Content actioned decreased from 10.6 million in Q2 2023 to 8.6 million in Q3 2023, due to a decrease in content that violated our policies.

[](https://transparency.fb.com/reports/community-standards-enforcement/violence-incitement/facebook/#content-actioned)

## Q3 2023

97.3%