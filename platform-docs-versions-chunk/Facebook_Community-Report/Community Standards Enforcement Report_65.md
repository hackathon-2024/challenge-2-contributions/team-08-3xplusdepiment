platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/regulated-goods/facebook/


# Restricted Goods and Services: Drugs and Firearms

We do not allow private individuals, manufacturers or retailers to buy, sell or trade non-medical drugs, pharmaceutical drugs and marijuana on Facebook and Instagram. We also do not allow private individuals to buy, sell, give, exchange or transfer firearms, including firearm parts or ammunition. While drugs and firearms are regulated by different legal restrictions around the world, we enforce these standards consistently across Instagram due to the borderless nature of our community.

This report does not include data on other goods prohibited from being sold on Facebook or Instagram, including human organs, animals and their parts, or on our enforcement of our separate [Commerce Policies](https://www.facebook.com/policies_center/commerce) or [Advertising Policies](https://business.facebook.com/policies/ads).

[Read the policy details](https://transparency.fb.com/policies/community-standards/regulated-goods/)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)