platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## DEEP DIVE

[Prevalence](https://transparency.fb.com/policies/improving/prevalence-metric/)

[Content actioned](https://transparency.fb.com/policies/improving/content-actioned-metric/)

[Proactive rate](https://transparency.fb.com/policies/improving/proactive-rate-metric/)

[Appealed content](https://transparency.fb.com/policies/improving/appealed-content-metric/)

[Restored content](https://transparency.fb.com/policies/improving/restored-content-metric/)

[Getting better at measurement](https://transparency.fb.com/policies/improving/getting-better-at-measurement/)

[Corrections and adjustments](https://transparency.fb.com/policies/improving/corrections-adjustments/)