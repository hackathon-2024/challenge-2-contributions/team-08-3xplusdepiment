platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/adult-nudity-and-sexual-activity/facebook/


## restored content

How much actioned content for adult nudity and sexual activity was later restored?

Restored without appeal

Restored after appeal

Total

How we calculate it

Restored content is the number of pieces of content that we restored during the reporting period after previously actioning it. We restore content both when it is appealed and when we discover issues ourselves.

[Read about this data](https://transparency.fb.com/policies/improving/restored-content-metric/)

NOTE:

Due to a temporary reduction in our review capacity as a result of COVID-19, we could not always offer people the option to appeal. We still gave people the option to tell us they disagreed with our decision, which helped us review many of these instances and restore content when appropriate. Starting in Q2 2022, we [updated our methodology](https://transparency.fb.com/policies/improving/corrections-adjustments/) for how we count appeals to include all instances where content was submitted for additional review, including after people told us that they disagreed with our decision.

How we calculate it

Prevalence is the estimated number of views that showed violating content, divided by the estimated number of total content views on Facebook.

How we calculate it

Content actioned is the total number of pieces of content that Facebook took action on for adult nudity and sexual activity. It includes both content we actioned after someone reported it, and content that we found proactively.

How we calculate it

Proactive rate is the number of pieces of content acted on that we found and actioned before people using Facebook reported them, divided by the total number of pieces of content we took action on.

How we calculate it

Appealed content counts the number of pieces of content actioned which were submitted for another review during the reporting period.

How we calculate it

Restored content is the number of pieces of content that we restored during the reporting period after previously actioning it. We restore content both when it is appealed and when we discover issues ourselves.