platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/suicide-and-self-injury/facebook/

[Home](https://transparency.fb.com/)

[Data](https://transparency.fb.com/reports/)

[Community Standards Enforcement Report](https://transparency.fb.com/reports/community-standards-enforcement/)

# Suicide and Self-Injury

We remove content that encourages suicide or self-injury on Facebook and Instagram. Self-injury is defined as the intentional and direct injuring of the body, including self-mutilation and eating disorders. We also remove content that identifies and negatively targets victims or survivors of self-injury or suicide.

We do allow people to discuss suicide and self-injury because we want Facebook and Instagram to be spaces where people can raise awareness about these issues and seek support.

[Read the policy details](https://transparency.fb.com/policies/community-standards/suicide-self-injury/)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)