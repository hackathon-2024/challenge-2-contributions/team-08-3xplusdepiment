platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/child-nudity-and-sexual-exploitation/facebook/

## appealed content

How much of the content we actioned for child endangerment did people appeal?

Child Nudity and Sexual Exploitation

Child Nudity and Physical Abuse

Child Sexual Exploitation

How we calculate it

Appealed content counts the number of pieces of content actioned which were submitted for another review during the reporting period.

[Read about this data](https://transparency.fb.com/policies/improving/appealed-content-metric/)