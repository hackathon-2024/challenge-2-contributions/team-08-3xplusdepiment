platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/child-nudity-and-sexual-exploitation/facebook/


# Child Endangerment: Nudity and Physical Abuse and Sexual Exploitation

We do not allow content that endangers children, such as content that contains nudity or physical abuse or content that sexually exploits children on Facebook and Instagram. When we find this type of violating content, we remove it, regardless of the context or the person's motivation for sharing it. We may also disable the account of the person who shared it, unless it appears the intent was not malicious (for example, to spread awareness of child exploitation).

We report apparent child exploitation to the [National Center for Missing and Exploited Children (NCMEC)](https://www.missingkids.org/home), a nonprofit that refers cases to law enforcement globally, in compliance with US law. We choose to remove content depicting non-sexualized child nudity to reduce the potential for abuse of the content by others.

[Read the policy details](https://transparency.fb.com/policies/community-standards/child-sexual-exploitation-abuse-nudity)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)