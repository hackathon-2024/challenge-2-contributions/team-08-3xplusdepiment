platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Restored content decreased from 6.92 million in Q2 2023 to 313K in Q3 2023, returning to pre-Q2 levels following an increase in enforcement on non-violating content due to a bug in our proactive detection technology that was later fixed and the content was restored. This impacted both platforms in Q2.

[](https://transparency.fb.com/reports/community-standards-enforcement/hate-speech/facebook/#restored-content)

## Q3 2023

7 million

## 

Content Actioned on Hate Speech

## 

Content actioned decreased from 9.8 million in Q2 2023 to 7.0 million in Q3 2023, returning to pre-Q2 levels following an increase in enforcement on non-violating content due to a bug in our proactive detection technology that was later fixed and the content was restored. This impacted both platforms in Q2.

[](https://transparency.fb.com/reports/community-standards-enforcement/hate-speech/instagram/#content-actioned)

## Q3 2023

977K

## 

Appealed Content on Hate Speech