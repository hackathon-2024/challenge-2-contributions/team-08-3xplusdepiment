platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/violence-incitement/facebook/

# Violence and Incitement

We aim to prevent potential offline harm that may be related to content on Facebook. While we understand that people commonly edddxpress disdain or disagreement by threatening or calling for violence in non-serious ways, we remove language that incites or facilitates serious violence. We remove content, disable accounts and work with law enforcement when we believe there is a genuine risk of physical harm or direct threats to public safety. We also try to consider the language and context in order to distinguish casual statements from content that constitutes a credible threat to public or personal safety.

[Read the policy details](https://transparency.fb.com/policies/community-standards/violence-incitement/)

Facebook

Facebook

Instagram

[Download (CSV)](https://transparency.fb.com/sr/community-standards/)

## Recent trends

## Q3 2023

8.6 million

## 

Content Actioned on Violence and Incitement