platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/graphic-violence/facebook/

## 

Content actioned decreased from 13.8 million in Q2 2023 to 9.0 million in Q3 2023, due to changes made to our enforcement systems.

[](https://transparency.fb.com/reports/community-standards-enforcement/graphic-violence/facebook/#content-actioned)

## Q3 2023

98.2%

## 

Proactive Rate on Violent and Graphic Content

## 

Proactive rate increased from 97.5% in Q2 2023 to 98.2% in Q3 2023, due to an update in our calculation to the proactive rate.

[](https://transparency.fb.com/reports/community-standards-enforcement/graphic-violence/facebook/#proactive-rate)

## prevalence

How prevalent were violent and graphic content violations?

How we calculate it

Prevalence is the estimated number of views that showed violating content, divided by the estimated number of total content views on Facebook.

[Read about this data](https://transparency.fb.com/policies/improving/prevalence-metric/)