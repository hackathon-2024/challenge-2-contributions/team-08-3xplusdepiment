platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## VIOLENCE AND CRIMINAL BEHAVIOR

[Dangerous Organizations: Terrorism and Organized Hate](https://transparency.fb.com/data/community-standards-enforcement/dangerous-organizations/)

[Restricted Goods and Services: Drugs and Firearms](https://transparency.fb.com/data/community-standards-enforcement/regulated-goods/)

[Violence and Incitement](https://transparency.fb.com/data/community-standards-enforcement/violence-incitement/)

## SAFETY

[Suicide and Self-Injury](https://transparency.fb.com/data/community-standards-enforcement/suicide-and-self-injury/)

[Child Endangerment: Nudity and Physical Abuse and Sexual Exploitation](https://transparency.fb.com/data/community-standards-enforcement/child-nudity-and-sexual-exploitation/)

[Bullying and Harassment](https://transparency.fb.com/data/community-standards-enforcement/bullying-and-harassment/)