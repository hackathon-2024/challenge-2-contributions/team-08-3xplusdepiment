platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## OBJECTIONABLE CONTENT

[Hate Speech](https://transparency.fb.com/data/community-standards-enforcement/hate-speech/)

[Violent and Graphic Content](https://transparency.fb.com/data/community-standards-enforcement/graphic-violence/)

[Adult Nudity and Sexual Activity](https://transparency.fb.com/data/community-standards-enforcement/adult-nudity-and-sexual-activity/)

## INTEGRITY AND AUTHENTICITY

[Fake Accounts](https://transparency.fb.com/data/community-standards-enforcement/fake-accounts/)

[Spam](https://transparency.fb.com/data/community-standards-enforcement/spam/)

## Learn about our measurements