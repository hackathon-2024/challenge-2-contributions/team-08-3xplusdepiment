platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/fake-accounts/facebook/

## proactive rate

Of the violating accounts we actioned, how many did we find and action before people reported them?

Found and actioned by us

Reported by users

How we calculate it

Proactive rate is the number of pieces of content acted on that we found and actioned before people using Facebook reported them, divided by the total number of pieces of content we took action on.

[Read about this data](https://transparency.fb.com/policies/improving/proactive-rate-metric/)

How we calculate it

Accounts actioned is the total number of accounts that Facebook took action on for fake accounts. It includes both accounts we actioned after someone reported them, and accounts that we found proactively.

How we calculate it

Proactive rate is the number of pieces of content acted on that we found and actioned before people using Facebook reported them, divided by the total number of pieces of content we took action on.