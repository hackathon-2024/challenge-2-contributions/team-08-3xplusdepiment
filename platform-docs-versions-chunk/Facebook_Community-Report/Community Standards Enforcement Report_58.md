platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/hate-speech/facebook/

## 

Content actioned decreased from 18 million in Q2 2023 to 9.6 million in Q3 2023, returning to pre-Q2 levels following an increase in enforcement on non-violating content due to a bug in our proactive detection technology that was later fixed and the content was restored. This impacted both platforms in Q2.

[](https://transparency.fb.com/reports/community-standards-enforcement/hate-speech/facebook/#content-actioned)

## Q3 2023

94.8%

## 

Proactive Rate on Hate Speech

## 

Proactive rate increased from 88.8% in Q2 2023 to 94.8% in Q3 2023, due to an update in our calculation to the proactive rate.

[](https://transparency.fb.com/reports/community-standards-enforcement/hate-speech/facebook/#proactive-rate)

## Q3 2023

313K

## 

Restored Content on Hate Speech