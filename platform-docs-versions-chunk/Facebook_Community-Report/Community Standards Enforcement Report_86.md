platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/violence-incitement/facebook/

## 

Content actioned decreased from 10.6 million in Q2 2023 to 8.6 million in Q3 2023, due to a decrease in content that violated our policies.

[](https://transparency.fb.com/reports/community-standards-enforcement/violence-incitement/facebook/#content-actioned)

## Q3 2023

97.3%

## 

Proactive Rate on Violence and Incitement

## 

Proactive rate increased from 85.1% in Q2 2023 to 97.3% in Q3 2023, due to an update in our calculation to the proactive rate.

[](https://transparency.fb.com/reports/community-standards-enforcement/violence-incitement/facebook/#proactive-rate)

## Prevalence

How prevalent were violence and incitement violations?

How we calculate it

Prevalence is the estimated number of views that showed violating content, divided by the estimated number of total content views on Facebook.

[Read about this data](https://transparency.fb.com/policies/improving/prevalence-metric/)