platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Appealed content increased from 1.0 million in Q2 2023 to 1.6 million in Q3 2023, as we increased our appeals period to adhere to the Digital Services Act.

[](https://transparency.fb.com/reports/community-standards-enforcement/adult-nudity-and-sexual-activity/instagram/#appealed-content)

## Q3 2023

87.8%

## 

Proactive Rate on Bullying and Harassment

## 

Proactive rate increased from 65.8% in Q2 2023 to 87.8% in Q3 2023, due to an update in our calculation to the proactive rate.

[](https://transparency.fb.com/reports/community-standards-enforcement/bullying-and-harassment/facebook/#proactive-rate)

## Q3 2023

1.6 million

## 

Appealed Content on Bullying and Harassment

## 

Appealed content increased from 847K in Q2 2023 to 1.6 million in Q3 2023, as we increased our appeals period to adhere to the Digital Services Act.

[](https://transparency.fb.com/reports/community-standards-enforcement/bullying-and-harassment/instagram/#appealed-content)

## Q3 2023

16.9 million