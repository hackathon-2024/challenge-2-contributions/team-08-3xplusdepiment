platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## Recent trends

## Q3 2023

35.1 million

## 

Content Actioned on Adult Nudity and Sexual Activity

## 

Content actioned decreased from 51.2 million in Q2 2023 to 35.1 million in Q3 2023, returning to pre-Q2 levels after an increase in enforcement on non-violating content due to a bug in our proactive detection technology that was later fixed and the content was restored.

[](https://transparency.fb.com/reports/community-standards-enforcement/adult-nudity-and-sexual-activity/facebook/#content-actioned)

## Q3 2023

92.4%

## 

Proactive Rate on Adult Nudity and Sexual Activity

## 

Proactive rate decreased from 93.8% in Q2 2023 to 92.4% in Q3 2023, due to a bug in our proactive detection technology.

[](https://transparency.fb.com/reports/community-standards-enforcement/adult-nudity-and-sexual-activity/facebook/#proactive-rate)

## Q3 2023

1.6 million

## 

Appealed Content on Adult Nudity and Sexual Activity