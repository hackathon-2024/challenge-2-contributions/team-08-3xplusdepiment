platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/facebook/

## Terrorism

Views of violating content that contains terrorism are very infrequent, and we remove much of this content before people see it. As a result, many times we do not find enough violating samples to precisely estimate prevalence.

In Q3 2023, this was true for violations of our policies on terrorism, suicide and self-injury and restricted goods and services on Facebook and Instagram. In these cases, we can estimate an upper limit of how often someone would see content that violates these policies.

In Q3 2023, the upper limit was 0.05% for violations of our policy for terrorism on Facebook. This means that out of every 10,000 views of content on Facebook, we estimate no more than 5 of those views contained content that violated the policy.

## Organized Hate

We cannot estimate prevalence for organized hate right now. We will continue to expand prevalence measurement to more areas as we confirm accuracy and meaningful data.