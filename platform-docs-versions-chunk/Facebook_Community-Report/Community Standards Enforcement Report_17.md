platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Proactive Rate on Violence and Incitement

## 

Proactive rate increased from 85.1% in Q2 2023 to 97.3% in Q3 2023, due to an update in our calculation to the proactive rate.

[](https://transparency.fb.com/reports/community-standards-enforcement/violence-incitement/facebook/#proactive-rate)

## Q3 2023

1.2 million

## 

Appealed Content on Violence and Incitement

## 

Appealed content increased from 663K in Q2 2023 to 1.2 million in Q3 2023, as we increased our appeals period to adhere to the Digital Services Act.

[](https://transparency.fb.com/reports/community-standards-enforcement/violence-incitement/instagram/#appealed-content)

## Data by policy area