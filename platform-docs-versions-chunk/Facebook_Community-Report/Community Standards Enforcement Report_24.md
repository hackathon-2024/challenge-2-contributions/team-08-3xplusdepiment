platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/adult-nudity-and-sexual-activity/facebook/

## prevalence

How prevalent were adult nudity and sexual activity violations?

How we calculate it

Prevalence is the estimated number of views that showed violating content, divided by the estimated number of total content views on Facebook.

[Read about this data](https://transparency.fb.com/policies/improving/prevalence-metric/)

## content actioned

How much adult nudity and sexual activity content did we take action on?

How we calculate it

Content actioned is the total number of pieces of content that Facebook took action on for adult nudity and sexual activity. It includes both content we actioned after someone reported it, and content that we found proactively.

[Read about this data](https://transparency.fb.com/policies/improving/content-actioned-metric/)