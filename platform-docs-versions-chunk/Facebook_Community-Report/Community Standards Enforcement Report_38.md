platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/child-nudity-and-sexual-exploitation/facebook/

## 

Appealed content increased from 147K in Q2 2023 to 267K in Q3 2023, due to an increase in proactive detection technology taking down content violating our policies.

[](https://transparency.fb.com/reports/community-standards-enforcement/child-nudity-and-sexual-exploitation/facebook/#appealed-content)

## Q3 2023

204K

## 

Restored Content on Child Endangerment: Sexual Exploitation

## 

Restored content increased from 80K in Q2 2023 to 204K in Q3 2023, due to adjustments in our proactive detection technology.

[](https://transparency.fb.com/reports/community-standards-enforcement/child-nudity-and-sexual-exploitation/facebook/#restored-content)

## prevalence

How prevalent were child endangerment violations?

We cannot estimate prevalence for child endangerment right now. We will continue to expand prevalence measurement to more areas as we confirm accuracy and meaningful data.