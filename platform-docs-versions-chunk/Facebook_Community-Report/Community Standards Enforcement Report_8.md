platform: Facebook
topic: Community-Report
subtopic: Community Standards Enforcement Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Community-Report/Community Standards Enforcement Report.md
url: https://transparency.fb.com/reports/community-standards-enforcement/

## 

Proactive rate decreased from 84.5% in Q2 2023 to 77.2% in Q3 2023, due to a decrease in proactive actions taken by our media-matching technology.

[](https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/instagram/#proactive-rate)

## Q3 2023

8.2 million

## 

Content Actioned on Dangerous Organizations and Individuals: Terrorism

## 

Content actioned decreased from 13.6 million in Q2 2023 to 8.2 million in Q3 2023, due to a decrease in content that violated our policies.

[](https://transparency.fb.com/reports/community-standards-enforcement/dangerous-organizations/facebook/#content-actioned)

## Q3 2023

99K

## 

Restored Content on Dangerous Organizations and Individuals: Terrorism