platform: Facebook
topic: Fort-Pages-API-Reference
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Pages-API-Reference/API Reference.md
url: https://developers.facebook.com/docs/fort-pages-api/reference/get_engagement_counts


## Available Facebook and Instagram data

Both tools use the same database of near real-time public content from Facebook and Instagram. Details about the content, such as the post owner and the number of reactions and shares, are also available. Information and content from the public entities listed below are available; however, certain data that could potentially identify users (such as tags) is redacted or omitted.

* Facebook Page
* Facebook group
* Facebook event
* Facebook post
* Instagram business, creator, or personal account
* Instagram post

Note that public Instagram accounts include professional accounts for businesses and creators. They also include a subset of personal accounts that have privacy [set to public](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F517073653436611&h=AT3gR10NU2l81IDJINp1o_xgs6FADTHQqDElLAHFyzt8c9QI6jUzEgspLJhZ2fUSfm0EpypmE3Wlk8f_QGloHrEPITJJemMOwAwpGBLMBPoNdB20mybv1rbWkCv_aiYIEKu73b651M51NbKk) and have either a verified badge or 50,000 or more followers. A [verified badge](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F733907830039577%3Fhelpref%3Dfaq_content&h=AT3K2qOn77EAW5pNvvSl8KEWTD-KkpRtdRYSyjOK4filxMFA1Y0QXW1iUL18Gt1Y4Q6M1U0mru0fngQj6HF7Ch9KX_4L5SSLu6jKR158ABCr0Uy0t3gx9d2ZT6rSb94pqda9C5MkIE3U_9Bk) in this context refers to accounts confirmed as authentic and not those with a paid Meta Verified subscription.

**Post view data**

The number of times the post was onscreen, which can shed light on questions relating to exposure and popularity of the content.

**Geographical data**

Content Library surfaces content from most countries and territories. Content from the following countries and territories is currently excluded: Australia, Belarus, China, Crimea, Cuba, Hong Kong, Iran, Iraq, North Korea, Russia, South Korea, Syria, Togo, Ukraine (Luhansk and Donetsk regions) and Venezuela. This list of excluded countries and territories is subject to change.