platform: Facebook
topic: Fort-Pages-API-Reference
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Pages-API-Reference/API Reference.md
url: https://developers.facebook.com/docs/fort-pages-api/reference/get_post_counts

# Meta Content Library and API

**Version v2.0**

Welcome to Meta Content Library and API. This page provides a broad-level overview of these products and acquaints you with the documentation resources that are available. To learn about obtaining access, see [Get Access](https://developers.facebook.com/docs/content-library-api/get-access).

* **Content Library**: a web-based tool that allows researchers to explore and understand data across Facebook and Instagram by offering a comprehensive visual and searchable collection of publicly accessible content.
    
* **Content Library API**: an API for querying and analyzing Meta's full public content archive. The API supports data analysis in Python and R in Meta's Researcher Platform, a secure digital cleanroom. VPN connection is required for Researcher Platform. The API can also be used in an approved third-party cleanroom. Each third-party cleanroom environment has its own user interface, the documentation of which is outside the scope of Meta Content Library and API documentation.
    

Content Library and API are controlled-access environments and do not allow data to be exported to a researcher’s own machine for analysis. You can perform deeper analysis of the public content from the library by using Content Library API in Researcher Platform or in an approved third-party cleanroom environment.