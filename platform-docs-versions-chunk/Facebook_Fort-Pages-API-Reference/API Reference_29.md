platform: Facebook
topic: Fort-Pages-API-Reference
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Pages-API-Reference/API Reference.md
url: https://developers.facebook.com/docs/fort-pages-api/reference/get_post_counts

## Content Library API highlights

**Endpoints and data fields**

With six dedicated endpoints, Content Library API searches over 100 data fields across Instagram accounts and posts, and Facebook Pages, posts, groups and events.

**Search indexing and results**

The API has powerful search capabilities and can return up to 100,000 results per query.

**Asynchronous search**

In addition to synchronous search, you can submit asynchronous queries in Content Library API. This means a search can run in the background while you submit other queries or work on other tasks.

**Documentation resources**

* [Content Library API documentation.](https://developers.facebook.com/docs/content-library-api)
    
* [Quick links](https://developers.facebook.com/docs/content-library-and-api/quick-links)
    
    provides quick access to key topics of interest to Content Library and API users.