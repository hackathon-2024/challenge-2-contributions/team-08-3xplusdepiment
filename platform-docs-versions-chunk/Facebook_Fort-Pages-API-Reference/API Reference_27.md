platform: Facebook
topic: Fort-Pages-API-Reference
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Pages-API-Reference/API Reference.md
url: https://developers.facebook.com/docs/fort-pages-api/reference/get_post_counts


## Available Facebook and Instagram data

Both tools use the same database of near real-time public content from Facebook and Instagram. Details about the content, such as the post owner and the number of reactions and shares, are also available. Information and content from the public entities listed below are available; however, certain data that could potentially identify users (such as tags) is redacted or omitted.

* Facebook Page
* Facebook group
* Facebook event
* Facebook post
* Instagram business, creator, or personal account
* Instagram post

Note that public Instagram accounts include professional accounts for businesses and creators. They also include a subset of personal accounts that have privacy [set to public](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F517073653436611&h=AT1akwfcjnkk5bSU2LpecZzM3U1Nv6MmEqWaanrS-mHnjOFBXFqvqX4w5-PpoIVpAK1np5ubNBKpcrOFK8l21qrsFAHJvyntA8UJ4BxLbgOIv2WWOQmQAHQSbEc8t8-uCok2INcmhgRKhTVd) and have either a verified badge or 50,000 or more followers. A [verified badge](https://l.facebook.com/l.php?u=https%3A%2F%2Fhelp.instagram.com%2F733907830039577%3Fhelpref%3Dfaq_content&h=AT2TCq7ViZcCpWaCMthmvGc_lsoVOZ614UI-TWNJDhMCtaNwshV_kXejH8hhfLbF7kb_tHTI6UozJvULivEYp6ZY995MpMIPhyc3RD15-L0YprF91dSy7sOAYEzwWfHbjk-3yUbax8QgVItV) in this context refers to accounts confirmed as authentic and not those with a paid Meta Verified subscription.

**Post view data**

The number of times the post was onscreen, which can shed light on questions relating to exposure and popularity of the content.

**Geographical data**

Content Library surfaces content from most countries and territories. Content from the following countries and territories is currently excluded: Australia, Belarus, China, Crimea, Cuba, Hong Kong, Iran, Iraq, North Korea, Russia, South Korea, Syria, Togo, Ukraine (Luhansk and Donetsk regions) and Venezuela. This list of excluded countries and territories is subject to change.