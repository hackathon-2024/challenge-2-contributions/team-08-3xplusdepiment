platform: Facebook
topic: Fort-Pages-API-Reference
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Pages-API-Reference/API Reference.md
url: https://developers.facebook.com/docs/fort-pages-api/reference/search_pages


## Researcher Platform highlights

Researcher Platform is a virtual data cleanroom that provides secure access to Facebook and Instagram datasets and tools designed for researchers.

**Free computation**

Computation and data analysis on Researcher Platform are free. The platform supports researchers collaborating on terabyte- to petabyte-scale datasets.

**JupyterHub environment**

Researcher Platform runs a modified, access-controlled version of JupyterHub—an open source tool that supports R, Python, SQL and a range of standard statistical packages. It offers both CPU and GPU servers.

**Research outputs**

You can export your research outputs under agreed-upon terms and conditions. Research outputs can consist of code, figures, tables, graphs and statistics.

**Documentation resources**

* [Researcher Platform documentation](https://developers.facebook.com/docs/researcher-platform).

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)