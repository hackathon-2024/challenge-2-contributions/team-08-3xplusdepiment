platform: Facebook
topic: Fort-Pages-API-Reference
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_Fort-Pages-API-Reference/API Reference.md
url: https://developers.facebook.com/docs/fort-pages-api/reference/get_follower_counts


## Content Library highlights

**Searching and filtering**

Searching all public posts across Facebook or Instagram is easy with comprehensive sorting and filtering options. Post results can be filtered by language, view count, media type, content producer and more. Search results can be shown in card view or table view.

**Multimedia**

Photos, videos and reels are available for dynamic search, exploration and analysis.

**Producer lists**

You can apply custom producer lists to a search query to surface public content from specific creators on Facebook or Instagram.

**Get API Code tool**

You can generate an API query in Python or R directly from your search query. The code can be pasted into the Content Library API in Researcher Platform to retrieve search results and perform deeper analysis.

**Trends in posts created**

Researchers can explore a graph showing a normalized trend of how often content matching their search keywords was posted on Facebook and Instagram within their chosen date range.

**Documentation resources**

* The [Content Library "About" tab](https://www.facebook.com/transparency-tools/content-library/dataset/1119037145491882/about/)
    
    has comprehensive documentation right in the UI.
    
* [Content Library documentation](https://developers.facebook.com/docs/content-library)
    
    has the same information, but does not require access to the Content Library UI.
    
* [Quick links](https://developers.facebook.com/docs/content-library-and-api/quick-links)
    
    provides quick access to key topics of interest to Content Library and API users.