platform: TikTok
topic: Research-API
subtopic: Research API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Documentation.md
url: https://developers.tiktok.com/doc/research-api-codebook/


### **Users**

#### **Unit of Analysis**

User information of all TikTok users that have set their account to public and are aged 18 and over.

#### **Scope**

The public details of a public user who is aged 18 and over can be accessed via this particular API.

#### **Details**

1. **Following Count** : This is the number of people that a public user follows.
2. **Likes Count** : This is the total number of likes accumulated by the user.
3. **Video Count** : This is the total number of videos that the user has posted on his/her TikTok account.
4. **Bio Description** : This is the description in the bio of the user. If the user does not have a description, this will be returned blank.
5. **Display Name** : This is the user's display name that is found under the user name.
6. **Follower Count** : This is the total number of followers that follow the user.
7. **Avatar****URL** : This is the URL of the user's profile picture.
8. **Is Verified** : This returns the information on whether the user has been verified. All verified users will have "blue tick" next to their user name. If the user has a blue tick, this variable will return a "true" in the response.