platform: TikTok
topic: Research-API
subtopic: Research API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Reference.md
url: https://developers.tiktok.com/doc/research-api-specs-query-videos/

# Response

|     |     |     |
| --- | --- | --- |
| **Key** | **Type** | **Example** |
| data | QueryVideoResponseData | {<br><br>"videos": \[...\],<br><br>"cursor": 100,<br><br>"has\_more": true,<br><br>"search\_id": ""<br><br>} |
| error | ErrorStruct | Error object |

## Data Structures