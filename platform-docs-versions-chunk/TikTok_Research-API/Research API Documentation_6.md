platform: TikTok
topic: Research-API
subtopic: Research API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Documentation.md
url: https://developers.tiktok.com/doc/research-api-get-started/

### field\_name

The following are the `field_name` values:

* `keyword`
* `create_date`
* `username`
* `region_code`
* `video_id`
* `hashtag_name`
* `music_id`
* `effect_id`
* `video_length`

### operation

The following are the `operation` values:

`IN`: Tests if an expression matches any value in a list of values.`EQ`: Tests if an expression matches the specified value.`GT`: Tests if an expression is strictly greater than the specified value.`GTE`: Tests if an expression is greater than or equal to the specified value.`LT`: Tests if an expression is strictly less than the specified value.`LTE`: Tests if an expression is less than or equal to the specified value.

### AND, OR or NOT

Conditions are grouped by the following boolean operators:

`AND`: Displays a record if all the conditions separated by `AND` are `TRUE`.`OR`: Displays a record if any of the conditions separated by `OR` is `TRUE`.`NOT`: Displays a record if all the conditions separated by `NOT` are `FALSE`.