platform: TikTok
topic: Research-API
subtopic: Research API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Documentation.md
url: https://developers.tiktok.com/doc/research-api-codebook/


### **Comments**

#### **Unit of Analysis**

A comment OR a reply to a comment posted for a public video on TikTok.

#### **Scope**

The information provided here includes text extracted from comments and a serial number (i.e. comment IDs) that help identify original comments posted on a video and any replies to comments. To protect the privacy of our users, other information is removed.

#### **Details**

1. **Create Time** : This is the time when the comment was posted on a video.
2. **ID** : This is the unique comment ID for a comment posted on a video.
3. **Like Count** : The total number of likes for a comment under a video, created by users by clicking the “Heart” icon.
4. **Parent Comment ID** : This is the unique ID of the parent comment when the user responds to another user's comment. If the comment was directly entered for a video, this ID is the same as the Video ID.
5. **Text** : This is the actual text of the comment entered on a video. To protect the privacy of our users, other information is removed.
6. **Video\_ID** : This is the video ID for which the comment was entered.