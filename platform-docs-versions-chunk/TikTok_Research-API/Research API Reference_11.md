platform: TikTok
topic: Research-API
subtopic: Research API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Reference.md
url: https://developers.tiktok.com/doc/research-api-specs-query-user-info/

### Example

    {
        "data": {
            "bio_description": "my_bio",
            "is_verified": false,
            "likes_count": 27155089,
            "video_count": 44,
            "avatar_url": "https://some_cdn.com/my_avatar",
            "follower_count": 232,
            "following_count": 45,
            "display_name": "my nick name"
        },
        "error": {
            "code": "ok",
            "message": "",
            "log_id": "202207280326050102231031430C7E754E",
        }
    }