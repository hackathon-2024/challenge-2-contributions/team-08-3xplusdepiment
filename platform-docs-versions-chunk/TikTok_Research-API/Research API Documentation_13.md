platform: TikTok
topic: Research-API
subtopic: Research API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Documentation.md
url: https://developers.tiktok.com/doc/research-api-codebook/

# **Codebook**

## **Introduction**

TikTok supports independent research about our platform. Through our Research API, academic researchers from non-profit universities in the U.S. And Europe can apply to study public data about TikTok content and accounts. This document describes the Research API functionality provided to researchers by TikTok.

## **Eligibility and Data Access**

To check your eligibility and determine the process to get access to this data, check [our product page](https://developers.tiktok.com/products/research-api/).

## **Content Made Available for Research**