platform: TikTok
topic: Research-API
subtopic: Research API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Documentation.md
url: https://developers.tiktok.com/doc/research-api-faq/

# Frequently Asked Questions

1. **What data is available as part of the Research** **API****?**

1. More information on the Research API can be found [here](https://developers.tiktok.com/products/research-api/). Once approved for access to the Research API, researchers can access public data on:

1. **Accounts**: This includes user profiles, and other data such as the number of likes of creator's content, followers and following count.
2. **Content**: This includes comments, captions, subtitles, and number of comments, shares, likes, and favorites that a video receives.

3. For more info on the data returned via our APIs, check out the [Research API codebook](https://developers.tiktok.com/doc/research-api-codebook/)

3. **How do I know if I qualify for Research** **API** **access?**

1. Applicants must fulfill the following criteria to qualify for access:

1. Have demonstrable academic experience and expertise in the research area specified in the application
2. Have no conflicts of interest with respect to using the services
3. Be employed by a non-profit academic institution in the U.S. or Europe
4. Be able to provide a clearly defined research proposal
5. Be committed to only using data for non-commercial purposes, as set out in our [Research API Terms of Service](https://www.tiktok.com/legal/page/global/terms-of-service-research-api/en)

5. **How do I apply for Research** **API** **access?**

1. If you meet the eligibility criteria outlined above and have prepared a research proposal, click [here](https://developers.tiktok.com/application/research-api) to apply.

7. **Can I work with a group of researchers to collaborate on a research topic? How do I ensure we all get access to the Research** **API** **in order to collaborate on our approved project?**

1. We now support lab-level access to the Research API. The application form should be submitted by the project's principal researcher. Once the application is approved, the principal researcher can log in and view the approved "Organization" on the TikTok for Developers (TT4D) home page. Here, the principal researcher will have the ability to manage access, including adding and removing collaborators.
2. The principal researcher can add up to 9 collaborators to work together on a research topic. It is preferable that the principal researchers submit the details of all anticipated collaborators during the application process. TikTok will review any collaborators that are invited to join an approved research project prior to granting them access. Further, it is the responsibility of the principal researcher to remove and revoke access to collaborators who are no longer working on the approved research project.
3. If the collaborators are from a different university, an application should be submitted with a list of collaborators for each university.
4. The collaborators need to ensure that they have setup their TT4D account in advance prior to getting an invite.

9. **Where is the Research** **API** **available?**

1. The Research API is currently available to qualifying researchers located in the United States and Europe.

11. **When will the Research** **API** **be available in my country?**

1. TikTok is actively working to expand eligibility to additional regions. Please check [this page](https://developers.tiktok.com/products/research-api/) for updates.

13. **I am a researcher working for a non-profit organization, in civil society, or at an** **NGO****. Can I apply for access?**

1. At present, eligibility is restricted to those employed by not-for-profit academic institutions in the United States, and Europe. We are actively working to expand eligibility globally as well as for the civil society sector. Please check [this page](https://developers.tiktok.com/products/research-api/) for updates.

15. **I am a creator, advertiser, or commercial user. Am I eligible for access to the Research** **API****?**

1. At present, eligibility is restricted to those employed by not-for-profit academic institutions in the United States, and Europe who meet our eligibility criteria. [Click here](https://developers.tiktok.com/) to learn more about API access for non-academic purposes.

17. **What are the daily quota limits? Can I request an increase in the quota limit?**

1. Currently, the daily limit is set at 1000 requests per day, allowing you to obtain up to 100,000 records per day across our APIs. (Video and Comments API can return 100 records per request). The daily quota gets reset at 12 AM UTC.
2. If you believe a quota limit increase is necessary for your research, please email us at Research-API@tiktok.com. We can't grant exceptions, but we're eager to better understand use cases from the research community to evaluate and take your requests into account for the future.

19. **I have a developer account with** **TikTok****. Can I start using the Research** **API****?**

1. Your developer account alone is not sufficient to grant you access to the Research API. In order to access the Research API, you will need to meet our eligibility requirements, submit an application, and be approved for a specific research project.