platform: TikTok
topic: Research-API
subtopic: Research API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Documentation.md
url: https://developers.tiktok.com/doc/research-api-codebook/


### **Videos**

#### **Unit of Analysis**

A public TikTok video posted by a public creator (who is aged 18 and over), who wants to expose his/her videos to all users of TikTok

#### **Scope**

All the videos from TikTok that are:

1. made public by a creator who is aged 18 and over;
2. AND, are posted in the regions of US, Europe and Rest of the World;
3. AND do not belong to Canada.

#### **Details**

1. **Region Code** : This describes the code of the country where the video was created. [Click here](https://developers.tiktok.com/doc/research-api-specs-query-videos/) to learn more about the region codes returned.
2. **User Name** : This is the user name of the creator.
3. **Video Description** : This is the description of the video.
4. **View Count** : This is the total number of views for a video on TikTok.
5. **Comment Count** : This is the total number of comments posted on a video.
6. **Create Time** : This is the time when the video was created.
7. **Video ID** : This is a unique video ID for each video posted on TikTok. This is a number that can be used to reconstruct the URL link to access the video.
8. **Like Count** : The total number of likes on a TikTok video, created by users by clicking the “Heart” icon.
9. **Share Count** : The total number of times a TikTok video has been shared by clicking the "Share" button with the video.