platform: TikTok
topic: Research-API
subtopic: Research API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Reference.md
url: https://developers.tiktok.com/doc/research-api-specs-query-videos/

## Query Parameters

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Key** | **Type** | **Description** | **Example** | **Required** |
| fields | string | The requested fields. Choose from the Video Object's fields. | Complete list:<br><br>id,video\_description,create\_time, region\_code,share\_count,view\_count,like\_count,comment\_count, music\_id,hashtag\_names, username,effect\_ids,playlist\_id,voice\_to\_text | Yes |