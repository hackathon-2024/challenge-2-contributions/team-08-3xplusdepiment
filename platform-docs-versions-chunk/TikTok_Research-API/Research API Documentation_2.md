platform: TikTok
topic: Research-API
subtopic: Research API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Documentation.md
url: https://developers.tiktok.com/doc/research-api-get-started/

# Getting Started

This guide will show you how to use the Research API. Learn how to use the Research API to query video data and fetch public TikTok account data in the following use case example.

# View your client registration

Once your application is approved, a research client will be generated for your project. You can view your approved research projects [here](https://developers.tiktok.com/research/). Select a project from the list to see the research client details.

The provided **Client key** and **Client secret** are required to connect to the Research API endpoints. The client key and secret are hidden by default but can be displayed by clicking the **Display** button (eye icon).

The client secret is a credential used to authenticate your connection to TikTok's APIs. Do not share this with anyone!