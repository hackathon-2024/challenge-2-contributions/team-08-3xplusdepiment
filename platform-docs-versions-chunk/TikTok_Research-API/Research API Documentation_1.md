platform: TikTok
topic: Research-API
subtopic: Research API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/TikTok_Research-API/Research API Documentation.md
url: https://developers.tiktok.com/doc/about-research-api/

# About Research API

TikTok's Research API allows independent and academic researchers who work for a non-profit institution to access certain data, including the following information about public account owners who are aged 18 and over.

* **Videos** - Videos configured to "Everyone" for who can watch, and for such videos, the total number of likes, total number of comments, voice-to-text, subtitles, the time of creation and video length.
* **Comments -** comment text, and total number of likes, replies, and the time the comment was posted.
* **Accounts -** bios, profile pictures, the total number of followers and the number of people who are followed by the account.

This Research API supports research in areas such as misinformation, disinformation, violent extremism, social trends, and community building. In order to obtain access to TikTok's Research API, researchers must submit an application, be approved, and adhere to our [Community Guidelines](https://www.tiktok.com/community-guidelines), [TikTok Research API Services Terms of Service](https://www.tiktok.com/legal/page/global/terms-of-service-research-api/en), as well as be approved by their research institution's ethics committee.

Additional information about TikTok's Research API can be found on [TikTok for Developers](https://developers.tiktok.com/products/research-api/).