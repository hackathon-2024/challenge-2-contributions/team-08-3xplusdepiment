platform: Snapchat
topic: Transparency-World
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency-World/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency

### 

Appels

À partir de ce rapport, nous commençons à signaler le nombre d’appels lancés par des utilisateurs dont les comptes ont été verrouillés pour violation de nos politiques. Nous rétablissons uniquement les comptes qui, selon nos modérateurs, ont été injustement verrouillés. Pendant cette période, nous signalons les appels relatifs au contenu lié aux drogues.  Dans notre prochain rapport, nous nous publierons plus de données relatives aux appels découlant d’autres violations de nos politiques.

| Nombre total d’appels soumis | Nombre total de comptes rétablis | % rétablis | Délai de réponse (en minutes médianes) |
| --- | --- | --- | --- |
| 36,682 | 1,249 | 3.40% | 1,008 |