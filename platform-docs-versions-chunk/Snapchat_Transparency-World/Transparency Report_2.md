platform: Snapchat
topic: Transparency-World
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency-World/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency


### 

Aperçu du contenu et des comptes en infraction

Du 1er janvier au 30 juin 2023, Snap a fait imposer 6 216 118 contenus dans le monde entier qui ont violé nos politiques.

Pendant ladite période, nous avons enregistré un taux de vues non conformes (TVNC) de 0,03 %, ce qui signifie que sur 10 000 vues de contenu Snap et de Story sur Snapchat, trois contenaient du contenu qui violait nos politiques.

| Nombre total de signalements sur le contenu et les comptes | Nombre total de contenu sanctionné | Nombre total de comptes uniques sanctionnés |
| --- | --- | --- |
| 17,267,985 | 6,216,118 | 3,687,082 |

| Raison | Signalements sur le contenu et les comptes | Contenu sanctionné | % de contenu total sanctionné | Comptes uniques sanctionnés | Délai de réponse (en minutes médianes) |
| --- | --- | --- | --- | --- | --- |
| Contenu à caractère sexuel | 5,843,879 | 3,270,318 | 52.6% | 1,833,194 | 6   |
| Harcèlement et intimidation | 6,645,969 | 920,070 | 14.8% | 778,417 | 13  |
| Menaces et violence | 615,011 | 94,556 | 1.5% | 73,981 | 21  |
| Automutilation et suicide | 122,369 | 24,621 | 0.4% | 22,637 | 21  |
| Fausses informations | 387,242 | 238 | 0.0% | 209 | 12  |
| Usurpation d'identité | 521,168 | 12,379 | 0.2% | 12,314 | 2   |
| Pourriel | 1,924,768 | 1,380,341 | 22.2% | 941,653 | <1  |
| Drogues | 642,421 | 297,554 | 4.8% | 203,939 | 28  |
| Armes | 81,223 | 16,622 | 0.3% | 12,203 | 20  |
| Autres marchandises réglementées | 216,562 | 141,514 | 2.3% | 103,157 | 30  |
| Discours haineux | 267,373 | 57,905 | 0.9% | 49,975 | 32  |

\*L'application correcte et cohérente de la législation contre les fausses informations est un processus dynamique qui nécessite un contexte actualisé et de la diligence.  Comme nous nous efforçons d'améliorer continuellement la précision de l'application de la loi de nos agents dans cette catégorie, nous avons choisi, depuis le premier semestre 2022, de signaler des chiffres dans les catégories « Contenu appliqué » et « Comptes uniques appliqués » qui sont estimés sur la base d'un examen rigoureux d'assurance qualité d'une partie statistiquement significative de l'application de la loi sur les fausses informations.  Plus précisément, nous échantillonnons une partie statistiquement significative des cas d'application de la législation sur les fausses informations dans chaque pays et contrôlons la qualité des décisions d'application.  Nous utilisons ensuite ces contrôles de qualité pour obtenir des évaluations de modération avec un intervalle de confiance de 95 % (marge d'erreur de +/- 5 %), que nous utilisons pour calculer le nombre de modération pour fausses informations signalées dans le rapport sur la transparence.