platform: Snapchat
topic: Transparency-World
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency-World/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency

### 

Contenu portant sur l'automutilation et le suicide

La santé mentale et le bien-être des Snapchatters nous tiennent à cœur, ce qui a influencé - et continue d'influencer - nos décisions visant à concevoir Snapchat différemment. En tant que plateforme conçue pour les communications entre de véritables amis, nous pensons que Snapchat peut jouer un rôle unique en permettant aux amis de s'entraider dans les moments difficiles.

Lorsque notre équipe Trust & Safety reconnaît un Snapchatter en détresse, elle a la possibilité de transmettre des ressources de prévention et d'assistance en matière d'automutilation, et d'informer le personnel d'intervention d'urgence, le cas échéant. Les ressources que nous partageons sont disponibles sur notre [liste mondiale de ressources en matière de sécurité](https://values.snap.com/fr-FR/safety/safety-resources), et ces ressources sont accessibles à tous les Snapchatters.

| Nombre total de fois où les ressources pour les suicides ont été partagées. |
| --- |
| 24,278 |