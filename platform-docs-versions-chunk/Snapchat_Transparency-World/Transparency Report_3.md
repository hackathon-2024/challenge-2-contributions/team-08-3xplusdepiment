platform: Snapchat
topic: Transparency-World
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency-World/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency


### 

Analyse des violations de contenus et de comptes

Nos taux globaux de signalement et d’application sont restés assez similaires à ceux des six mois précédents, à quelques exceptions près dans les catégories clés. Nous avons observé une diminution d’environ 3 % du nombre total de contenu, de rapports et de modération sur ce cycle.

Les catégories les plus importantes étaient le harcèlement et l’intimidation, le spam, les armes et les fausses informations. Le harcèlement et l’intimidation ont augmenté d'environ 56 % comparé au nombre total de signalements, puis une augmentation de près de 39 % du contenu et des impositions sur le compte unique. Ces augmentations des exécutions ont été couplées à une diminution d’environ 46 % du délai d’exécution, ce qui souligne l’efficacité opérationnelle de notre équipe pour ce type de contenu. De même, nous avons observé une augmentation d’environ 65 % du nombre total de signalements de spam, une augmentation de près de 110 % des modérations de contenu et une augmentation de près de 80 % des comptes uniques imposés à la loi et nos équipes ont également réduit le délai de traitement de près de 80 %. Notre catégorie d’armes a vu une diminution d’environ 13 % du nombre total de signalements, une diminution d’environ 51 % des modérations de contenu et une réduction d’environ 53 % des comptes uniques imposés. Enfin, notre catégorie de fausses informations a vu une augmentation d’environ 14 % du nombre total de signalements, mais une diminution d’environ 78 % des modérations de contenu et une diminution d’environ 74 % des comptes uniques imposés. Cela peut être attribué à la poursuite du processus d’assurance de la qualité (QA) et au recours aux ressources que nous appliquons aux rapports de fausses informations pour nous assurer que nos équipes de modération identifient les fausses informations et agissent avec précision sur la plateforme.

Dans l’ensemble, bien que nous ayons observé des chiffres globalement similaires à ceux de la dernière période, nous pensons qu’il est important de poursuivre l’amélioration des outils de notre communauté pour signaler activement et précisément les violations potentielles telles qu’elles apparaissent sur la plateforme.