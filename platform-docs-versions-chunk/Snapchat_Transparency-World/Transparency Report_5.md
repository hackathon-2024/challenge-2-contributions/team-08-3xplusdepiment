platform: Snapchat
topic: Transparency-World
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency-World/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency

### 

Contenu d'extrémistes terroristes et violents

Pendant la période de rapport (1er janvier 2023 au 30 juin 2023), nous avons supprimé 18 comptes pour violation de notre politique d’interdiction du contenu terroriste et extrémiste violent.

Chez Snap, nous supprimons les contenus terroristes et extrémistes violents signalés sur des canaux multiples. Nous encourageons les utilisateurs à signaler les contenus terroristes et extrémistes violents via notre menu de signalement dans l'application, et nous travaillons en étroite collaboration avec les forces de l'ordre pour traiter les contenus terroristes et extrémistes violents susceptibles d'apparaître sur Snap.

| Nombre total de suppressions de comptes |
| --- |
| 18  |