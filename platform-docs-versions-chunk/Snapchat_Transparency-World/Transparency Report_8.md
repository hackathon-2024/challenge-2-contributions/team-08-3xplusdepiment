platform: Snapchat
topic: Transparency-World
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency-World/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency


### 

Modération des publicités

Snap s’engage à veiller à ce que toutes les publicités soient conformes aux politiques de nos plateformes. Nous croyons en une approche responsable et respectueuse de la publicité, en créant une expérience sûre et agréable pour tous nos utilisateurs. Ci-dessous, nous avons inclus des informations sur notre modération publicitaire. Notez que les publicités sur Snapchat peuvent être supprimées pour diverses raisons telles que décrites dans les [politiques publicitaires de Snap](https://snap.com/en-US/ad-policies?lang=fr-FR), y compris les contenus trompeurs, les contenus adulte, les contenus violents ou inquiétants, les discours de haine et la violation de la propriété intellectuelle. En outre, vous pouvez maintenant trouver la [Galerie des publicités](https://adsgallery.snap.com/?lang=fr-FR) de Snapchat dans la barre de navigation de ce rapport sur la transparence. 

| Nombre total de publicités signalées | Nombre total de publicités supprimées |
| --- | --- |
| 186,344 | 5,145 |