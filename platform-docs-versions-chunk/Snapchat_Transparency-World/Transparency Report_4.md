platform: Snapchat
topic: Transparency-World
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency-World/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency


### 

La lutte contre l'exploitation et les abus sexuels à l'encontre des enfants

L'exploitation sexuelle de tout membre de notre communauté, en particulier des mineurs, est illégale, odieuse et interdite en vertu de nos Règles communautaires. La prévention, la détection et l'éradication des images d'exploitation et d'abus sexuels sur des enfants (CSEAI) sur notre plateforme est une priorité absolue pour Snap, et nous développons continuellement nos capacités à combattre ces types de crimes comme beaucoup d'autres.

Nous utilisons des outils de détection de technologie active tels que le hachage robuste de PhotoDNA et l'appariement d'imagerie pédopornographique de Google, pour identifier les images et vidéos illégales connues d'abus sexuels sur des enfants, respectivement, et les signaler au centre américain pour les enfants disparus et exploités (NCMEC), comme l'exige la loi. Le NCMEC ensuite, à son tour, collabore avec les forces de l'ordre nationales ou internationales, selon les besoins.

Au cours du premier semestre 2023, nous avons détecté et traité de manière proactive 98 % des violations liées à l'exploitation et aux abus sexuels concernant des enfants signalées ici, soit une augmentation de 4 % par rapport à notre rapport précédent.

| Raison | Nombre total de contenu sanctionné | Nombre total de suppressions de comptes | Nombre total de soumissions au NCMEC\* |
| --- | --- | --- | --- |
| CSEAI | 548,509 | 228,897 | 292 489 |

\*\*Notez que chaque envoi au NCMEC peut contenir plusieurs éléments de contenu. Le total des pièces individuelles de média soumises au NCMEC est égal au total du contenu sanctionné.