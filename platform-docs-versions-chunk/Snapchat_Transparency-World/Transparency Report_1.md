platform: Snapchat
topic: Transparency-World
subtopic: Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Snapchat_Transparency-World/Transparency Report.md
url: https://values.snap.com/fr-FR/privacy/transparency

Rapport de transparence

1er janvier 2023 - 30 juin 2023

Publication :

25 octobre 2023

Mise à jour :

25 octobre 2023

Pour donner un aperçu des efforts de Snap en matière de sécurité ainsi que de la nature et du volume du contenu signalé sur notre plateforme, nous publions des rapports sur la transparence deux fois par an. Nous entendons poursuivre la publication de ces rapports dans l'objectif de les rendre plus complets et plus documentés, afin que les nombreuses parties prenantes qui s'intéressent de près à nos pratiques en matière de modération de contenu et d'application de la loi, ainsi qu'au bien-être de notre communauté puissent en profiter.  

Le présent rapport de transparence couvre le premier semestre 2021 (1er janvier - 30 juin). Comme pour nos précédents rapports, nous partageons les données sur le nombre total de contenus dans l'application, les signalements au niveau des comptes que nous avons reçus et sanctionnés dans des catégories spécifiques de violations des politiques, la manière dont nous avons répondu aux demandes des forces de l'ordre et des gouvernements, et nos différentes sanctions ventilées par pays. et nos mesures d’exécution ventilées par pays.  
  
Dans le cadre de notre engagement permanent à améliorer nos rapports sur la transparence, nous introduisons quelques nouveaux éléments avec cette publication. Nous avons ajouté des points de données supplémentaires sur nos pratiques publicitaires et notre modération, ainsi que sur le contenu et les appels sur le compte. Pour se conformer à la loi de l’UE sur les services numériques, nous avons également ajouté de nouvelles informations contextuelles sur nos opérations dans les états membres de l’UE, telles que le nombre de modérateurs de contenu et d’utilisateurs actifs mensuels (MAU) dans la région. Une grande partie de ces informations se trouve dans le rapport et sur la page de notre [Centre de transparence](https://values.snap.com/fr-FR/privacy/transparency/european-union) dédiée à l'Union européenne.

Enfin, nous avons mis à jour notre [Glossaire](https://values.snap.com/fr-FR/privacy/transparency/glossary)en y ajoutant des liens vers nos « Explications sur les règles communautaires », qui offrent un contexte supplémentaire sur la politique de notre plateforme et sur nos efforts opérationnels.

Pour plus d'informations sur nos politiques de lutte contre les contenus préjudiciables en ligne et sur nos plans visant à améliorer nos pratiques de signalement, veuillez consulter notre récent [blog sécurité et impact](https://snap.com/en-US/safety-and-impact?lang=fr-FR) sur ce rapport de transparence.

Pour trouver des ressources supplémentaires en matière de sécurité et de vie privée sur Snapchat, cliquez sur notre onglet [À propos du rapport sur la transparence](https://www.snap.com/en-US/privacy/transparency/about?lang=fr-FR) au bas de la page.

Veuillez noter que la version la plus récente de ce rapport de transparence est disponible en anglais (en-US).