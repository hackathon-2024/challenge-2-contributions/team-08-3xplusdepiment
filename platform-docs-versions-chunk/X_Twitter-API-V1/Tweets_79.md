platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/overview

Overview

**Please note:**  

We've recently released the following endpoints within the [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api). 

|     |     |     |
| --- | --- | --- |
| **v1.1 endpoints** | **Corresponding v2 endpoints** |     |
| [GET statuses/user\_timeline](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-user_timeline.html) | [User Tweet timeline](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/introduction) | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/migrate) |
| [GET statuses/user\_mentions](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-mentions_timeline.html) | [User mention timeline](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/introduction) | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/migrate) |

Please use the migration guides to see what has changed between the standard v1.1 and v2 versions.

**Important note:**  

This endpoint has been updated to include Tweet edit metadata. Learn more about these metadata on the ["Edit Tweets" fundamentals page](https://developer.twitter.com/en/docs/twitter-api/v1/edit-tweets).