platform: X
topic: Twitter-API-V1
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/geo

### Tweet with Twitter Place

    {
      "geo": null,
      "coordinates": null,
      "place": {
        "id": "07d9db48bc083000",
        "url": "https://api.twitter.com/1.1/geo/id/07d9db48bc083000.json",
        "place_type": "poi",
        "name": "McIntosh Lake",
        "full_name": "McIntosh Lake",
        "country_code": "US",
        "country": "United States",
        "bounding_box": {
          "type": "Polygon",
          "coordinates": [
            [
              [
                -105.14544,
                40.192138
              ],
              [
                -105.14544,
                40.192138
              ],
              [
                -105.14544,
                40.192138
              ],
              [
                -105.14544,
                40.192138
              ]
            ]
          ]
        },
        "attributes": {
          
        }
      }
    }