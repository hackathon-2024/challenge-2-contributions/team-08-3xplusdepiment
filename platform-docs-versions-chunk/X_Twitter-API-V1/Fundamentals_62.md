platform: X
topic: Twitter-API-V1
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/rate-limits

Rate limits: Standard v1.1

This page describes the rate limits for standard v1.1 endpoints. 

We also have a rate limits page for [premium v1.1](https://developer.twitter.com/en/docs/twitter-api/premium/rate-limits), [enterprise](https://developer.twitter.com/en/docs/twitter-api/enterprise/rate-limits), and [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/rate-limits).