platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/custom-profiles/api-reference/new-profile

## Example Response[¶](#example-response "Permalink to this headline")

    {
      "custom_profile": {
        "id": "100001",
        "created_timestamp": "1479767168196",
        "name": "Jon C, Partner Engineer",
        "avatar": {
            "media": {
               "url": "https://pbs.twimg.com/media/Cr7HZpvVYAAYZIX.jpg"
           }
        }
    }