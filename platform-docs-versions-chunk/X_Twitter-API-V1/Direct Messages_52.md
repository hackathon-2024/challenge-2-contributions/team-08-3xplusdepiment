platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/welcome-messages/api-reference/new-welcome-message

## Example Response[¶](#example-response "Permalink to this headline")

    {
      "welcome_message" : {
        "id": "844385345234",
        "created_timestamp": "1470182274821",
        "message_data": {
          "text": "Welcome!",
          "attachment": {
            "type": "media",
            "media": {
              ...
            }
          }
        }
      }
      "name": "simple_welcome-message 01"
    }