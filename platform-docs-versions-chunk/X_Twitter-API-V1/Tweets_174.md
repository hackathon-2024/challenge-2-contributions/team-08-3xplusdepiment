platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/curate-a-collection/api-reference/post-collections-entries-curate

## Failure:[¶](#failure- "Permalink to this headline")

    {"objects":{},"response":{"errors":[{"reason":"duplicate","change":{"op":"add","tweet_id":"390897780949925889"}},{"reason":"duplicate","change":{"op":"add","tweet_id":"390853164611555329"}},{"reason":"duplicate","change":{"op":"add","tweet_id":"390892747810295808"}},{"reason":"duplicate","change":{"op":"add","tweet_id":"390898463090561024"}}]}}