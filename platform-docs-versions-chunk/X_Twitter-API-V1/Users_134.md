platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/create-manage-lists/api-reference/get-lists-memberships

## Example Request[¶](#example-request "Permalink to this headline")

`GET https://api.twitter.com/1.1/lists/memberships.json?screen_name=twitter&cursor=-1`