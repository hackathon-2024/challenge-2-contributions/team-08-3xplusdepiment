platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/customer-feedback/api-reference/events

## Response Values[¶](#response-values "Permalink to this headline")

|     |     |
| --- | --- |
| **events** | An array of events. |
| **event\_type** | Possible values: feedback.created, feedback.updated |
| **next\_cursor** | Values are unique to a given from\_time/to\_time range. |