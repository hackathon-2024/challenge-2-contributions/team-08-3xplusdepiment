platform: X
topic: Twitter-API-V1
subtopic: Trends
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Trends.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/trends/locations-with-trending-topics/api-reference/get-trends-closest

## Example Response[¶](#example-response "Permalink to this headline")

    [
      {
        "country": "Australia",
        "countryCode": "AU",
        "name": "Australia",
        "parentid": 1,
        "placeType": {
          "code": 12,
          "name": "Country"
        },
        "url": "http://where.yahooapis.com/v1/place/23424748",
        "woeid": 23424748
      }
    ]