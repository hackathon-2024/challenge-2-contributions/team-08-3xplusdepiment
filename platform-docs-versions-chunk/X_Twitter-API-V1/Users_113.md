platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/post-friendships-create

## Example Response[¶](#example-response "Permalink to this headline")

    {user-object,
      "status": {tweet-object}
    }

For more detail, see the [user-object definition](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object) and the [tweet-object definition](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object).