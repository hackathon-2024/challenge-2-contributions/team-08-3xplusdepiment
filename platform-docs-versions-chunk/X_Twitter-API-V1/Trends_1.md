platform: X
topic: Twitter-API-V1
subtopic: Trends
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Trends.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/trends/trends-for-location/overview

Overview

An API to return the [trending topics](https://support.twitter.com/articles/101125) near a specific latitude, longitude location.