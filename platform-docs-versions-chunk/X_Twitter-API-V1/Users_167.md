platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/create-manage-lists/api-reference/post-lists-members-create

## Example Request[¶](#example-request "Permalink to this headline")

`POST https://api.twitter.com/1.1/lists/members/create.json?slug=team&owner_screen_name=twitter&screen_name=kurrik`

## Example Response[¶](#example-response "Permalink to this headline")