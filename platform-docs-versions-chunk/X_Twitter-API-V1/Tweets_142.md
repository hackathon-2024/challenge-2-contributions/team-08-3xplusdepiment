platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/curate-a-collection/overview

Overview

## Collections

A collection is an editable group of Tweets hand-selected by a Twitter user or programmatically managed via collection APIs. Each collection is public and has its own page on twitter.com, making it easy to share and embed in your website and apps.  
 

### Create and edit a collection using TweetDeck

[TweetDeck](https://tweetdeck.twitter.com/) supports adding Tweets to a collection by simply dragging a Tweet into a collection column.

  

### View a collection on Twitter.com

Each collection has a public URL on Twitter.com. Share a collection with others by including it in a Tweet, email, or other share method.

Example: [https://twitter.com/NYTNow/timelines/576828964162965504](https://twitter.com/NYTNow/timelines/576828964162965504)