platform: X
topic: Twitter-API-V1
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/entities

Entities object

## Twitter entities  

Jump to on this page

[Introduction](#intro)

[Entities object](#entitiesobject)

  - [Hashtag object](#hashtags)

  - [Media object](#media)  

  - [Media size object](#media-size)  

  - [URL object](#urls)

  - [User mention object](#mentions)

  - [Symbol object](#symbols)

  - [Poll object](#polls)

[Retweet and Quote Tweet details](#retweets-quotes)

[Entities in user objects](#entities-user)

[Entities in Direct Messages](#entities-dm)

[Next Steps](#next)