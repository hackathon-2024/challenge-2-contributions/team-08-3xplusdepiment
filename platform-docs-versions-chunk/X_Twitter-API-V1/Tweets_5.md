platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/search/guides/standard-operators

Using standard search

**Please note:**  

We launched a [new version of the standard Search Tweets endpoint](https://developer.twitter.com/en/docs/twitter-api/tweets/search/introduction) as part of [Twitter API v2: Early Access](https://developer.twitter.com/en/docs/twitter-api/early-access). If you are currently using any of these endpoints, you can use our [migration materials](https://developer.twitter.com/en/docs/twitter-api/tweets/search/migrate/standard-to-twitter-api-v2) to start working with the newer endpoint.

To see all of Twitter's search endpoint offerings, please visit our [search overview](https://developer.twitter.com/en/docs/twitter-api/search-overview).