platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/welcome-messages/api-reference/get-welcome-message-rule

## Example Response[¶](#example-response "Permalink to this headline")

    {
      "welcome_message_rule" : {
        "id": "9910934913490319",
        "created_timestamp": "1470182394258",
        "welcome_message_id": "844385345234"
      }
    }