platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-followers-ids


## Example Response[¶](#example-response "Permalink to this headline")

    {
        "ids": [
            455974794,
            947576438684872705,
            850839346009780224,
            958850376630910976,
            889483959943536640,
            966094285119606784,
            1020583045,
            948604640811212801,
            967155179614240768,
            554514802,
            14873932,
            963916668731904000,
            970763391181746178,
            966091392631140358,
            .
            .
            .
            5000 ids later,
            .
            .
            .
            813143846,
            958604886735716353,
            402873729,
            958603486551330817,
            913076424897994753,
            820967329068707840,
            958593574932762624,
            958589381102665728,
            958573223737724929,
            889474485694410752
        ],
        "next_cursor": 1591087837626119954,
        "next_cursor_str": "1591087837626119954",
        "previous_cursor": 0,
        "previous_cursor_str": "0"
    }