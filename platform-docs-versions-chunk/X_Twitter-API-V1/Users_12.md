platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/manage-account-settings/api-reference/post-account-remove_profile_banner

POST account/remove\_profile\_banner

post-account-remove\_profile\_banner

# POST account/remove\_profile\_banner

Removes the uploaded profile banner for the authenticating user. Returns HTTP 200 upon success.

## Resource URL[¶](#resource-url "Permalink to this headline")

`https://api.twitter.com/1.1/account/remove_profile_banner.json`

## Resource Information[¶](#resource-information "Permalink to this headline")

|     |     |
| --- | --- |
| Response formats | JSON |
| Requires authentication? | Yes (user context only) |
| Rate limited? | Yes |

## Parameters[¶](#parameters "Permalink to this headline")

None

## Example Request[¶](#example-request "Permalink to this headline")

`POST https://api.twitter.com/1.1/account/remove_profile_banner.json`

## Example Response[¶](#example-response "Permalink to this headline")