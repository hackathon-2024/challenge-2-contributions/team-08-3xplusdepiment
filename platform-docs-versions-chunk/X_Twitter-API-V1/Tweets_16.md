platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/overview

### Terminology

**Tweet/Status** - when a status message is shared on Twitter. Also see [Introduction to Tweet JSON](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/intro-to-tweet-json.html)

**Retweet** - when a Tweet is re-shared by another specific user. Also see [Introduction to Tweet JSON](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/intro-to-tweet-json.html)  

**Like** - when a Tweet recieves a 'heart' from a specific user, formerly known as favo(u)rite or 'star'