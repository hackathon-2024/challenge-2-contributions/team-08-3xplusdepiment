platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/message-attachments/overview

### Retrieving Direct Message media

For media in Direct Messages `media_url` is the same https URL as `media_url_https` and must be accessed by signing a request with the user’s access token using OAuth 1.0A.

It is not possible to directly embed these images in a web page.

[Media in Direct Messages retrieval guide](https://developer.twitter.com/en/docs/direct-messages/message-attachments/guides/retrieving-media)