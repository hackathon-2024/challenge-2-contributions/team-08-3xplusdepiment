platform: X
topic: Twitter-API-V1
subtopic: Media
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Media.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/api-reference/post-media-subtitles-create

# Request

Requests should be HTTP POST with a JSON content body, and Content-Type `application/json; charset=UTF-8`

**Note:** The domain for this endpoint is **upload.twitter.com**