platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/filter-realtime/overview

Overview

**Please note:**  

We recently released [filtered stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream), a set of [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api) endpoints that has similar functionality to this one. The new version is now ready for production and serves adequate access for the majority of developers on our platform.

This endpoint is now deprecated and has NOT been updated to include Tweet edit metadata. Learn more about these metadata on the ["Edit Tweets" fundamentals page](https://developer.twitter.com/en/docs/twitter-api/v1/edit-tweets).   

We have deprecated the delivery of compliance messages through this endpoint. Apps created after April 29, 2022 cannot access this endpoint. This endpoint will stop delivering compliance messages beginning October 31, 2022. [Learn more about this change](https://twittercommunity.com/t/deprecation-announcement-removing-compliance-messages-from-statuses-filter-and-retiring-statuses-sample-from-the-twitter-api-v1-1/170500) and see our [migration resources](https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/migrate).

Developers in need of higher levels of access can explore our [enterprise PowerTrack API](https://developer.twitter.com/en/docs/twitter-api/enterprise/powertrack-api/overview). You can see an overview of our filtered stream offerings on our [filtered stream overview](https://developer.twitter.com/en/docs/twitter-api/filtered-stream-overview) page, and see what's new with v2 by visiting the [migration guide](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/migrate).