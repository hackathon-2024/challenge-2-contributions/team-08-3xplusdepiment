platform: X
topic: Twitter-API-V1
subtopic: Media
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Media.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/api-reference/post-media-upload

## Example Response[¶](#example-response "Permalink to this headline")

    {
      "media_id": 710511363345354753,
      "media_id_string": "710511363345354753",
      "media_key": "3_710511363345354753",
      "size": 11065,
      "expires_after_secs": 86400,
      "image": {
        "image_type": "image/jpeg",
        "w": 800,
        "h": 320
      }
    }