platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/sending-and-receiving/overview

Overview

Please note:

We've recently released the following endpoints within the [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api).

| v1.1 endpoints | Corresponding v2 endpoints |     |
| --- | --- | --- |
| [GET direct\_messages/events/list](https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/sending-and-receiving/api-reference/list-events) | Direct Messages [lookup](https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/introduction) | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/migrate) |
| [POST direct\_mesages/events/new](https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/sending-and-receiving/api-reference/new-event) | [Manage](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/introduction) Direct Messages | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/migrate) |

Please use the migration guides to see what has changed between the standard v1.1 and v2 versions.

Sending message events