platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-mentions_timeline

## Example Request[¶](#example-request "Permalink to this headline")

`GET https://api.twitter.com/1.1/statuses/mentions_timeline.json?count=2&since_id=14927799`