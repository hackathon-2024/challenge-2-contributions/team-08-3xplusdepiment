platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/user-profile-images-and-banners

User profile images and banners

Profile images[](#profile-images "Permalink to this headline")  
## 

Profile images (also known as avatars) are an important component of a Twitter account’s expression of identity. Use [POST account/update\_profile\_image](https://developer.twitter.com/content/developer-twitter/en/docs/accounts-and-users/manage-account-settings/api-reference/post-account-update_profile_image) to upload a profile image on behalf of a user.