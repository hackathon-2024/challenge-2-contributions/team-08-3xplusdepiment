platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/guides/working-with-timelines

Working with timelines

**Please note:**  

We launched a [new version of the standard v1.1 statuses/user\_timeline and statuses/mentions\_timeline endpoints](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/introduction) as part of [Twitter API v2: Early Access](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/early-access). If you are currently using either of these endpoints, you can use our [migration materials](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/timelines/migrate/standard-to-twitter-api-v2) to start working with the newer endpoint.

Working with timelines