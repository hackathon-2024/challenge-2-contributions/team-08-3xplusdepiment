platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-friends-ids

## Example Response[¶](#example-response "Permalink to this headline")

    {
      "previous_cursor": 0,
      "ids": [
        657693,
        183709371,
        7588892,
        38895958,
        22891211,
        9019482,
        14488353,
        11750202,
        12249,
        22915745,
        1249881,
        14927800,
        1523501,
        22548447,
        15062340,
        133031077,
        17874544,
        777925,
        4265731,
        27674040,
        26123649,
        9576402,
        821958,
        7852612,
        819797,
        1401881,
        8285392,
        9160152,
        795649,
        3191321,
        783214
      ],
      "previous_cursor_str": "0",
      "next_cursor": 0,
      "next_cursor_str": "0"
    }