platform: X
topic: Twitter-API-V1
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/user

## 

In general these `user` metadata values are relatively constant. Some fields never change, such as the user's `id` (provided as a string `id_str`) and when the account was created. Other metadata can occasionally change, such as the `screen_name`, display`name`, `description`, `location`, and other profile details. Some metadata frequently changes, such as the number of Tweets the account has posted `statuses_count` and its number of followers `followers_count`.