platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/overview

Overview

**Please note**  

We've released the following endpoints within the [Twitter API v2](https://developer.twitter.com/en/docs/twitt/content/developer-twitter/en/docs/twitter-api/getting-started/about-twitter-apier-api/early-access) . 

|     |     |     |
| --- | --- | --- |
| **v1.1 endpoints** | **Corresponding v2 endpoints** |     |
| [GET users/lookup](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-users-lookup)  <br>[GET users/show](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-users-show) | [User lookup](https://developer.twitter.com/en/docs/twitter-api/users/lookup/introduction) | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/users/lookup/migrate) |
| [GET followers/ids](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-followers-ids)  <br>[GET followers/list](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-followers-list)  <br>[GET friends/ids](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-friends-ids)  <br>[GET friends/list](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-friends-list)  <br>[POST friendships/create](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/post-friendships-create)[POST friendships/destroy](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/post-friendships-destroy) | [Follows](https://developer.twitter.com/en/docs/twitter-api/users/follows/introduction) | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/users/follows/migrate) |

Please use the migration guides to see what has changed between the standard v1.1 and v2 versions.