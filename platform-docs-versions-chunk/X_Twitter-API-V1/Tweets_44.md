platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/get-statuses-lookup

## Example Request[¶](#example-request "Permalink to this headline")

`GET https://api.twitter.com/1.1/statuses/lookup.json?id=20,1050118621198921728`