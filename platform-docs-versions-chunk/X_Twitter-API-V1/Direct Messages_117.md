platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/customer-feedback/api-reference/create

## Example Result[¶](#example-result "Permalink to this headline")

    {
      "created_at":"SatDec1517:58:20+00002015",
      "updated_at":"SatDec1517:59:22+00002015",
      "id":"123456789",
      "type":"nps",
      "test":false,
      "dm_id":"8989898989",
      "from_user_id":"1212121212121",
      "to_user_id":"343434343434",
      "privacy_url":"https://my­business.domain/privacy",
      "external_id":"ticket_5555",
      "question_variant_id":"3",
      "display_name":"MyBusinessName"
    }