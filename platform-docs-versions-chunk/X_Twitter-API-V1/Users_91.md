platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-friendships-lookup


## Example Response[¶](#example-response "Permalink to this headline")

    [
      {
        "name": "andy piper (pipes)",
        "screen_name": "andypiper",
        "id": 786491,
        "id_str": "786491",
        "connections": [
          "following"
        ]
      },
      {
        "name": "λ🥑. 🍞",
        "screen_name": "binary_aaron",
        "id": 165837734,
        "id_str": "165837734",
        "connections": [
          "following",
          "followed_by"
        ]
      },
      {
        "name": "Twitter Dev",
        "screen_name": "TwitterDev",
        "id": 2244994945,
        "id_str": "2244994945",
        "connections": [
          "following"
        ]
      },
      {
        "name": "Emily Sheehan 🏕",
        "screen_name": "happycamper",
        "id": 63046977,
        "id_str": "63046977",
        "connections": [
          "none"
        ]
      },
      {
        "name": "Harrison Test",
        "screen_name": "Harris_0ff",
        "id": 4337869213,
        "id_str": "4337869213",
        "connections": [
          "following",
          "following_requested",
          "followed_by"
        ]
      }
    ]