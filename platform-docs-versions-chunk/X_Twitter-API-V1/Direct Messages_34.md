platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/welcome-messages/guides/deeplinking-to-welcome-message

## 

By simply including the Direct Message deeplink URL at the end of a Tweet, your can append a "Send a private message" button to the bottom of the Tweet. Read more about using Direct Message deeplinks in Tweets in [this blog post](https://blog.twitter.com/marketing/en_us/a/2016/best-practices-using-direct-messages-for-customer-service-0.html).

Using a [Direct Message Card](https://blog.twitter.com/2017/drive-discovery-of-bots-other-personalized-customer-experiences-in-direct-messages), businesses can capture people’s attention with engaging image or video creatives, and include up to four fully customizable call-to-action buttons. Each call-to-action button can deeplink to a unique Welcome Message. The Direct Message Card is currently available in limited beta to Twitter advertisers. Contact your Twitter representative for more information.  

Deeplinking from a Website