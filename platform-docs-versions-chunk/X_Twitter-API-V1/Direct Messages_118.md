platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/customer-feedback/api-reference/create

## NPS Question Variants[¶](#nps-question-variants "Permalink to this headline")

| ID  | Text |
| --- | --- |
| 0   | What is your overall satisfaction with <display\_name>? |
| 1   | How satisfied are you with <display\_name>? |
| 2   | Overall, how satisfied were you with your recent <display\_name> experience? |
| 3   | How would you rate the overall experience with <display\_name>? |
| 4   | How would you rate your overall experience with <display\_name>? |
| 5   | How would you rate your experience so far with <display\_name>? |