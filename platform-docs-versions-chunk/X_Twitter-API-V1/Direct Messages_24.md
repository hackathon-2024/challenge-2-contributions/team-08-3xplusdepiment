platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/sending-and-receiving/api-reference/get-event

## Example Response[¶](#example-response "Permalink to this headline")

    {
      "event": 
        "id": "110", 
        "created_timestamp": "5300",
        "type": "message_create",
        "message_create": {
          ...
        }
      }
    }