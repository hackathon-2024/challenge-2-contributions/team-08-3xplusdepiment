platform: X
topic: Twitter-API-V1
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/geo

Geo objects

# Tweet Location Metadata

Jump to on this page

[Introduction](#intro)

[Place object](#place)

[Coordinates object](#coordinates)

[Tweet with Twitter Place](#tweet-place)

[Tweet with exact location](#tweet-exact)

[Next steps](#next)