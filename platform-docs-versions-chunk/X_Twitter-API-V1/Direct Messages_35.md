platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/welcome-messages/guides/deeplinking-to-welcome-message

## 

Direct Message deeplinks may also be used to deeplink from a website or other external source like a mobile app. For more details on deeplinking to Welcome Messages from a website, see [Message Button](https://dev.twitter.com/web/message-button) documentation.