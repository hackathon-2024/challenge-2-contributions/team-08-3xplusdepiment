platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/customer-feedback/api-reference/show

## Example Result[¶](#example-result "Permalink to this headline")

    {
      "created_at": "SatDec1517:58:20+00002015",
      "updated_at": "SatDec1517:59:22+00002015",
      "id": "123456789"
      "text": "Thankyouforbeingaloyalcustomer!",
      "media_id_str": 12345,
      "response": {
        "score": {
          "created_at": "SatDec1518:59:22+00002015",
          "value": 1
        },
        "text": {
          "created_at": "SatDec1518:59:52+00002015",
          "value": "I<3thisbiz"
        }
      }
    }

Note

Response object will only be present if data is available.