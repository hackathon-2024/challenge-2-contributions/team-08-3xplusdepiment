platform: X
topic: Twitter-API-V1
subtopic: Media
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Media.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/overview

Overview

A media object represents a single photo, video or animated GIF. Media objects are used by many endpoints within the Twitter API, and may be included in Tweets, Direct Messages, user profiles, advertising creatives and elsewhere. Each media object may have multiple display or playback variants, with different resolutions or formats.  
 

## Media types & size restrictions

Size restrictions for uploading via API   

* Image 5 MB
* GIF 15 MB
* Video 512 MB (when using media\_category=amplify)