platform: X
topic: Twitter-API-V1
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/overview

## Next steps

Review the standard v1.1 objects to better understand each field:

* [Tweet object](https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/tweet)
* [User object](https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/user)
* [Entities object](https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/entities)
* [Extended entities object](https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/extended-entities)
* [Geo object](https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/geo)