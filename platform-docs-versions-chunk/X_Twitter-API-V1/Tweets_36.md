platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/get-statuses-oembed

## Resource Information[¶](#resource-information "Permalink to this headline")

|     |     |
| --- | --- |
| **Resource URL** | `https://publish.twitter.com/oembed` |
| **Response formats** | JSON |
| **Requires authentication?** | No  |
| **Rate limited?** | No  |