platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/manage-account-settings/api-reference/post-account-update_profile

POST account/update\_profile

post-account-update\_profile

# POST account/update\_profile

Sets some values that users are able to set under the "Account" tab of their settings page. Only the parameters specified will be updated.

## Resource URL[¶](#resource-url "Permalink to this headline")

`https://api.twitter.com/1.1/account/update_profile.json`

## Resource Information[¶](#resource-information "Permalink to this headline")

|     |     |
| --- | --- |
| Response formats | JSON |
| Requires authentication? | Yes (user context only) |
| Rate limited? | Yes |