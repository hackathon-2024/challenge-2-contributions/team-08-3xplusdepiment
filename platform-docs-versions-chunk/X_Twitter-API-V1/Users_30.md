platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/manage-account-settings/api-reference/get-saved_searches-show-id

## Example Response[¶](#example-response "Permalink to this headline")

    {
      "created_at": "Fri Nov 04 18:46:41 +0000 2011", 
      "id": 62353170, 
      "id_str": "62353170", 
      "name": "@anywhere", 
      "position": null, 
      "query": "@anywhere"
    }