platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/api-features

Direct Message API features

Please note:

We've recently released the following endpoints within the [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api).

| v1.1 endpoints | Corresponding v2 endpoints |     |
| --- | --- | --- |
| [GET direct\_messages/events/list](https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/sending-and-receiving/api-reference/list-events) | Direct Messages [lookup](https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/introduction) | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/migrate) |
| [POST direct\_mesages/events/new](https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/sending-and-receiving/api-reference/new-event) | [Manage](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/introduction) Direct Messages | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/migrate) |

Please use the migration guides to see what has changed between the standard v1.1 and v2 versions.

These API features enable developers to build better-personalized customer experiences at scale as well as other innovative interactions. To help create more engaging customer service, marketing, and user engagement experiences in Direct Messages we’re providing developers access to endpoints to start conversations with a welcome message, publish messages with quick replies and media, and more.

* [Sending and receiving events](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/sending-and-receiving "Sending and receiving events")Send and receive Direct Messages.
* [Welcome Messages](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/welcome-messages "Welcome Messages")Create messages that display for specific scenarios.
* [Message Attachments](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/message-attachments "Message Attachments")Attach videos, images and GIFs.
* [Quick Replies](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/quick-replies "Quick Replies")Prompt users for structured replies with a menu of options.
* [Buttons](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/buttons "Buttons")Add buttons to link to websites, deep link to apps or other parts of Twitter.
* [Conversation management](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/conversation-management "Conversation management")Properties to help manage the conversation between multiple applications.
* [Custom profiles](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/custom-profiles "Custom profiles")Display a custom profile image and name in a Direct Message.
* [Customer feedback cards](https://developer.twitter.com/content/developer-twitter/en/docs/direct-messages/customer-feedback "Customer feedback cards")Prompt users for NPS and CSAT feedback.