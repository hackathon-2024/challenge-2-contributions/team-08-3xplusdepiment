platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/overview

## Overview

The following API endpoints can be used to programmatically follow users, search for users, and get user information:

|     |     |     |
| --- | --- | --- |
| **Friends and followers** | **POST friendships** | **Get user info** |
| * GET followers/ids<br>* GET followers/list<br>* GET friends/ids<br>* GET friends/list<br>* GET friendships/incoming<br>* GET friendships/lookup<br>* GET friendships/no\_retweets/ids<br>* GET friendships/outgoing<br>* GET friendships/show | * POST friendships/create<br>* POST friendships/destroy<br>* POST friendships/update | * GET users/lookup<br>* GET users/search<br>* GET users/show |

For more details, please see the individual endpoint information within the [API reference](https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference) section.