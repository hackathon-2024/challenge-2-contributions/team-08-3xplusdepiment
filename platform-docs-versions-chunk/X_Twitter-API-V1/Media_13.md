platform: X
topic: Twitter-API-V1
subtopic: Media
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Media.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/uploading-media/chunked-media-upload

## 

For issues with the Media APIs:

* Browse the [Media API category](https://twittercommunity.com/c/twitter-api/media-apis/) in the developer forums.