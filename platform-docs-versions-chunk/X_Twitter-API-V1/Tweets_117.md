platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/filter-realtime/guides/connecting

## Authentication

The following authentication methods are supported by the Streaming APIs:

|     |     |     |
| --- | --- | --- |
| Auth Type | Supported APIs | Description |
| [OAuth](https://developer.twitter.com/en/docs/basics/authentication/overview/oauth.html) | * Track API Stream | Requests must be authorized [according to the OAuth specification](https://dev.twitter.com/oauth/overview/authorizing-requests) . |
| [Basic auth](https://developer.twitter.com/en/docs/basics/authentication/overview/basic-auth.html) | * PowerTrack API<br>* Decahose stream | Requests must use of HTTP Basic Authentication, constructed from a valid email address and password combination. |