platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/curate-a-collection/overview/about_collections

## Accounts with protected Tweets and Collections

Some accounts on Twitter have enabled [a setting that “protects” the Tweets they create](https://support.twitter.com/articles/14016) for an approved audience of followers. Users with protected accounts can still use Collections, but with the following caveats:

* Protected accounts can create Collections but the Collections they create will be public.
* Public users can switch to a protected state, but their Collections will remain public.
* Any user can retrieve/discover Collections belonging to any other user, regardless of the Collection owner’s protected account status.
* Tweets created by users with protected accounts cannot be included in Collections.