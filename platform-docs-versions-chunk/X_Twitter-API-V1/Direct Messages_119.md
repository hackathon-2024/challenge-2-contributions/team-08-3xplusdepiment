platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/customer-feedback/api-reference/create


## CSAT Question Variants[¶](#csat-question-variants "Permalink to this headline")

| ID  | Text |
| --- | --- |
| 0   | What is your overall satisfaction with <display\_name>? |
| 1   | How satisfied are you with <display\_name>? |
| 2   | Overall, how satisfied were you with your recent <display\_name> experience? |
| 3   | How would you rate the overall experience with <display\_name>? |
| 4   | How would you rate your overall experience with <display\_name>? |
| 5   | How would you rate your experience so far with <display\_name>? |
| 6   | How would you rate your experience on Twitter with <display\_name>? |
| 7   | Were you satisfied with your recent experience with <display\_name>? |
| 8   | How well does <display\_name> meet your expectations? |
| 9   | How would you rate your guest experience with <display\_name>? |
| 10  | How would you rate your service experience with <display\_name>? |
| 11  | How would you rate your recent service experience with <display\_name>? |
| 12  | How would you rate the service you received from <display\_name>? |
| 13  | Were you satisfied with the result of your interaction with <display\_name>? |
| 14  | How would you rate the ability to resolve your issue with <display\_name>? |
| 15  | How would you rate the response time from <display\_name>? |
| 16  | How would you rate the speed of service from <display\_name>? |
| 17  | How would you rate the time to resolution with <display\_name>? |
| 18  | How would you rate the time to resolve your issue with <display\_name>? |
| 19  | How would you rate the speed of resolution with <display\_name>? |
| 20  | How would you rate the <display\_name> advisor's expertise? |
| 21  | How satisfied were you with the <display\_name> agent who helped you? |
| 22  | How satisfied were you with the <display\_name> specialist who helped you? |
| 23  | How satisfied were you with the <display\_name> representative who helped you? |
| 24  | How would you rate your recent banking experience with <display\_name>? |
| 25  | How would you rate the overall event experience at <display\_name>? |
| 26  | How would you rate your bill pay experience with <display\_name>? |
| 27  | How would you rate your purchase experience with <display\_name>? |
| 28  | How would you rate your shopping experience with <display\_name>? |
| 29  | How would you rate your delivery experience with <display\_name>? |
| 30  | How would you rate your rental experience with <display\_name>? |
| 31  | How would you rate your recent <display\_name> store visit? |
| 32  | How would you rate your recent <display\_name> hotel stay? |
| 33  | How would you rate your recent flight with <display\_name>? |
| 34  | How would you rate your recent ride with <display\_name>? |
| 35  | How would you rate your recent trip with <display\_name>? |
| 36  | How would you rate your recent visit to <display\_name>? |
| 37  | How would you rate your recent meal at <display\_name>? |