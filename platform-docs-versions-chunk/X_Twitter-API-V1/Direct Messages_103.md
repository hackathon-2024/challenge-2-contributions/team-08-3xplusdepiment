platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/custom-profiles/api-reference/get-profile-list

GET custom\_profiles/list

get-profile-list

# GET custom\_profiles/list

Retrieves all custom profiles for the authenticated account. Default page size is 20.

## Resource URL[¶](#resource-url "Permalink to this headline")

`https://api.twitter.com/1.1/custom_profiles/list.json`

## Resource Information[¶](#resource-information "Permalink to this headline")

|     |     |
| --- | --- |
| Response formats | JSON |
| Requires authentication? | Yes (user context only) |
| Rate limited? | Yes |
| Requests / 24 hour window (user auth) | Yes (180 / 15 min) |