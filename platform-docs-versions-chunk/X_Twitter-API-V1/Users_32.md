platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/manage-account-settings/api-reference/post-saved_searches-create

## Example Response[¶](#example-response "Permalink to this headline")

    {
      "created_at": "Fri Aug 24 22:08:58 +0000 2012", 
      "id": 158598597, 
      "id_str": "158598597", 
      "name": "sandwiches", 
      "position": null, 
      "query": "sandwiches"
    }