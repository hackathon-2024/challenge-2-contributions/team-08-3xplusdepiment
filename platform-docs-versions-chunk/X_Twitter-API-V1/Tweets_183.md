platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/curate-a-collection/api-reference/post-collections-update


## Example Response[¶](#example-response "Permalink to this headline")

    {
      "response": {
        "timeline_id": "custom-390882285743898624"
      },
      "objects": {
        "users": {
          "119476949": {
            "profile_sidebar_fill_color": "DDEEF6",
            "profile_sidebar_border_color": "C0DEED",
            "profile_background_tile": true,
            "name": "OAuth Dancer",
            "profile_image_url": "http://a0.twimg.com/profile_images/730275945/oauth-dancer_normal.jpg",
            "created_at": "Wed Mar 03 19:37:35 +0000 2010",
            "location": "San Francisco, CA",
            "follow_request_sent": null,
            "profile_link_color": "0084B4",
            "is_translator": false,
            "id_str": "119476949",
            "default_profile": false,
            "contributors_enabled": false,
            "favourites_count": 0,
            "url": "http://bit.ly/oauth-dancer",
            "profile_image_url_https": "https://si0.twimg.com/profile_images/730275945/oauth-dancer_normal.jpg",
            "utc_offset": null,
            "id": 119476949,
            "profile_use_background_image": true,
            "listed_count": 0,
            "profile_text_color": "333333",
            "lang": "en",
            "followers_count": 0,
            "protected": false,
            "notifications": null,
            "profile_background_image_url_https": "https://si0.twimg.com/profile_background_images/80151733/oauth-dance.png",
            "profile_background_color": "C0DEED",
            "verified": false,
            "geo_enabled": true,
            "time_zone": null,
            "description": "",
            "default_profile_image": false,
            "profile_background_image_url": "http://a0.twimg.com/profile_background_images/80151733/oauth-dance.png",
            "statuses_count": 0,
            "friends_count": 0,
            "following": null,
            "screen_name": "oauth_dancer",
            "counts": {
              "lists": {
                "owned": null,
                "subscribed": null
              },
              "saved_searches": 0
            }
          }
        },
        "timelines": {
          "custom-390882285743898624": {
            "name": "Subtweets",
            "user_id": "119476949"
          }
        }
      }
    }