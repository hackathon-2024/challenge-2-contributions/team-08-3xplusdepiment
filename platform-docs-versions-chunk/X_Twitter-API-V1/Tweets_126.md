platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/filter-realtime/guides/basic-stream-parameters

Standard stream parameters

**Please note:**  

We launched a [new version of the POST statuses/filter endpoint](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/introduction) as part of [Twitter API v2: Early Access](https://developer.twitter.com/en/docs/twitter-api/early-access). If you are currently using this endpoint, you can use our [migration materials](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/migrate/standard-to-twitter-api-v2) to start working with the newer version.

To see all of Twitter's filtered stream endpoint offerings, please visit our [overview](https://developer.twitter.com/en/docs/twitter-api/filtered-stream-overview).