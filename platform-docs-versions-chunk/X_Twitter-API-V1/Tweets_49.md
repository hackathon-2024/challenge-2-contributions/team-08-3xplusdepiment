platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-retweet-id

## Example Response[¶](#example-response "Permalink to this headline")

    {retweet-status-object,
    "user": {retweeting-user-object},
    "retweeted_status": {retweeted-status-object,
      "user": {retweeted-user-object}
    }
    }