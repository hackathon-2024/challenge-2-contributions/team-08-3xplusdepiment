platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/message-attachments/overview

Overview

## Overview

Media may be attached and retrieved for Direct Messages through authenticated calls with app-user authorization.

Direct Messages with an image will contain a media object with relevant details, check our Twitter data objects dictionary for more details [here](https://developer.twitter.com/content/developer-twitter/en/docs/tweets/data-dictionary/overview/entities-object#media).  
 

### Uploading and attaching media (images, GIFs, video) to a Direct Message.

[Media for Direct Messages upload & attachment guide](https://developer.twitter.com/en/docs/direct-messages/message-attachments/guides/attaching-media)