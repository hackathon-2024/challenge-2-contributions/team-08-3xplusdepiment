platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/manage-account-settings/api-reference/get-users-profile_banner


## Example Response[¶](#example-response "Permalink to this headline")

    {
      "sizes": {
        "ipad": {
          "h": 313,
          "w": 626,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/ipad"
        },
        "ipad_retina": {
          "h": 626,
          "w": 1252,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/ipad_retina"
        },
        "web": {
          "h": 260,
          "w": 520,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/web"
        },
        "web_retina": {
          "h": 520,
          "w": 1040,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/web_retina"
        },
        "mobile": {
          "h": 160,
          "w": 320,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/mobile"
        },
        "mobile_retina": {
          "h": 320,
          "w": 640,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/mobile_retina"
        },
        "300x100": {
          "h": 100,
          "w": 300,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/300x100"
        },
        "600x200": {
          "h": 200,
          "w": 600,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/600x200"
        },
        "1500x500": {
          "h": 500,
          "w": 1500,
          "url": "https://pbs.twimg.com/profile_banners/6253282/1347394302/1500x500"
        }
      }
    }