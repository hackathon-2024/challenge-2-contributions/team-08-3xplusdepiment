platform: X
topic: Twitter-API-V1
subtopic: Media
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Media.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/api-reference/post-media-upload

## Request[¶](#request "Permalink to this headline")

Requests should be either `multipart/form-data` or `application/x-www-form-urlencoded` POST formats.

**Note:** The domain for this endpoint is **upload.twitter.com**

## Resource URL[¶](#resource-url "Permalink to this headline")

`https://upload.twitter.com/1.1/media/upload.json`

## Resource Information[¶](#resource-information "Permalink to this headline")

|     |     |
| --- | --- |
| Response formats | JSON |
| Requires authentication? | Yes (user context only) |
| Rate limited? | Yes |