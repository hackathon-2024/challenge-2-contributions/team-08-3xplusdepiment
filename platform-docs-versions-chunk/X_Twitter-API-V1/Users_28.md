platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/manage-account-settings/api-reference/get-saved_searches-list

## Example Response[¶](#example-response "Permalink to this headline")

    [
      {
        "created_at": "Tue Jun 15 09:37:24 +0000 2010",
        "id": 9569704,
        "id_str": "9569704",
        "name": "@twitterapi",
        "position": null,
        "query": "@twitterapi"
      },
      {
        "created_at": "Tue Jun 15 09:38:04 +0000 2010",
        "id": 9569730,
        "id_str": "9569730",
        "name": "@twitter OR twitterapi OR "twitter api" OR "@anywhere"",
        "position": null,
        "query": "@twitter OR twitterapi OR "twitter api" OR "@anywhere""
      }
    ]