platform: X
topic: Twitter-API-V1
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/overview

Introduction

This is the standard v1.1 data dictionary. We also have data dictionaries for [premium v1.1](https://developer.twitter.com/en/docs/twitter-api/premium/data-dictionary), [enterprise](https://developer.twitter.com/en/docs/twitter-api/enterprise/data-dictionary), and [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/data-dictionary). 

If you are migrating from a standard v1.1 endpoint to a v2 endpoint, we've put together a [data format migration guide](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats) that you can use to map standard v1.1 fields to v2 fields. This guide will also let you know which fields and expansions parameters you will need to include in your request to return specific v2 fields.