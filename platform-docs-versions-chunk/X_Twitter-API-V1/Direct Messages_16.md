platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/sending-and-receiving/guides/direct-message-migration

## Next Steps

*     Download our Direct Message Migration Guide (below)

Direct Messages[Direct Message - Migration Guide
--------------------------------](https://cdn.cms-twdigitalassets.com/content/dam/developer-twitter/pdfs-and-files/DM%20-%20Migration%20Guide.pdf)

[Download PDF](https://cdn.cms-twdigitalassets.com/content/dam/developer-twitter/pdfs-and-files/DM%20-%20Migration%20Guide.pdf)