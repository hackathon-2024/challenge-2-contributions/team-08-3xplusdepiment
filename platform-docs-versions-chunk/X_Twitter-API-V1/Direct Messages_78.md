platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/typing-indicator-and-read-receipts/overview

Overview

Send typing indicators or read receipts that are displayed in the Direct Message conversation view to let users know that their messages are being processed.  
  

* [Typing indicators](https://developer.twitter.com/en/docs/direct-messages/typing-indicator-and-read-receipts/api-reference/new-typing-indicator)  
    A visual aniamtion in the Direct Message view notifying the recipient that the user is typing or thinking.
* [Read receipts](https://developer.twitter.com/en/docs/direct-messages/typing-indicator-and-read-receipts/api-reference/new-read-receipt)  
    A subtle visual notification in the Direct Message view notifying the recipient that the user has read or seen the last message.