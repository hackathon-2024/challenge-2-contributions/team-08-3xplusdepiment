platform: X
topic: Twitter-API-V1
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/curate-a-collection/overview/about_collections

## Collections on Twitter.com

Once created, Collections are available to view on twitter.com through a web and mobile-friendly permalink. Collections are meant to be shared with the world! Feel free to Tweet these permalink URLs to share your collection with followers.

Each Collections permalink is indicated in the custom\_timeline\_url field found in Collection timeline object responses.

## Embedded Collections on the Web

Collections are meant to be shared with the world, on and off Twitter. To that end, [embedded timelines](https://dev.twitter.com/web/embedded-timelines) have been expanded to support [embedded collections](https://dev.twitter.com/web/embedded-timelines/collection). Use the [widget configurator](https://twitter.com/settings/widgets/new/custom) to prepare your collections for syndication and receive the simple HTML & JavaScript embed code for your site.

[National Park Tweets](https://twitter.com/TwitterDev/timelines/539487832448843776)