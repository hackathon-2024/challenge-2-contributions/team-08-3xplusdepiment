platform: X
topic: Twitter-API-V1
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/data-dictionary/object-model/extended-entities

Extended entities object

## Twitter extended entities 

Jump to on this page

[Introduction](#intro)

[Extended Entities object](#extended-entities-object)

[Example Tweets and JSON payloads](#example-json)

  - [Tweet with four native photos](#tweet-photos)

  - [Tweet with native video](#tweet-video)

  - [Tweet with an animated GIF](#tweet-gif)

[Next Steps](#next)