platform: X
topic: Twitter-API-V1
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/overview

Overview

**Please note**  

We've released the following endpoints within the [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api). 

|     |     |     |
| --- | --- | --- |
| **v1.1 endpoints** | **Corresponding v2 endpoints** |     |
| [GET blocks/id](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/get-blocks-ids)   <br>[GET blocks/list](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/get-blocks-list)  <br>[POST blocks/create](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/post-blocks-create)   <br>[POST blocks/destroy](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/post-blocks-destroy) | [Blocks](https://developer.twitter.com/en/docs/twitter-api/users/blocks) | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/users/blocks/migrate) |
| [GET mutes/users/id](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/get-mutes-users-ids)   <br>[GET mutes/users/list](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/get-mutes-users-list)  <br>[POST mutes/users/create](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/post-mutes-users-create)   <br>[POST mutes/users/destroy](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/post-mutes-users-destroy) | [Mutes](https://developer.twitter.com/en/docs/twitter-api/users/mutes) | [Migration guide](https://developer.twitter.com/en/docs/twitter-api/users/mutes/migrate) |

Please use the migration guides to see what has changed between the standard v1.1 and v2 versions.

Your app can mute, block and report users for the authenicated user.

For general information on reporting viloations on Twitter see [How to report violations](https://support.twitter.com/articles/15789) in the help center.