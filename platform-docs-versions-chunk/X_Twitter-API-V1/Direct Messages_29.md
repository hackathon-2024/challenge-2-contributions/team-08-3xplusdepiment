platform: X
topic: Twitter-API-V1
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V1/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/v1/direct-messages/welcome-messages/overview

Overview

Welcome Messages provide the ability to display a message to people who are entering a Direct Message conversation. Welcome messages can be customized for different referral paths. For example, users who click on Direct Message links in a Tweet or if a user enters a Direct Message view for the first time with no prior context. Welcome Messages can contain any content that a Direct Message would, including media, Quick Replies, and more.

Welcome Messages can be presented to users in two ways:

* [Deeplinking to a Welcome Message](https://developer.twitter.com/en/docs/direct-messages/welcome-messages/guides/deeplinking-to-welcome-message)  
    Create a URL that can be used anywhere to link a user to a specific message in the Direct Message view.
* [Setting a Default Welcome Message](https://developer.twitter.com/en/docs/direct-messages/welcome-messages/guides/setting-default-welcome-message)  
    Set the default message a user will see when they enter the Direct Message view.