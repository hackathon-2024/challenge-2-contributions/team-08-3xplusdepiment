platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/api-reference/get-tweets-sample-stream

GET /2/tweets/sample/stream

# GET /2/tweets/sample/stream

Streams about 1% of all Tweets in real-time.  
  
If you have [Academic Research access](https://developer.twitter.com/en/products/twitter-api/academic-research), you can connect up to two [redundant connections](https://developer.twitter.com/en/docs/twitter-api/tweets/sampled-stream/integrate/recovery-and-redundancy-features) to maximize your streaming up-time.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets%2Fsample%2Fstream&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets/sample/stream`