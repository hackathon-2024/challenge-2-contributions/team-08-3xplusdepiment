platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/mutes/api-reference/post-users-user_id-muting

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "muting": true   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `muting` | boolean | Indicates whether the user is muting the specified user as a result of this request. |