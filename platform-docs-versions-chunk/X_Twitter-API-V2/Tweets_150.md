platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/migrate/standard-to-twitter-api-v2

Standard v1.1 compared to Twitter API v2

## Standard v1.1 compared to Twitter API v2

If you have been working with the v1.1 [search/tweets](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/search/overview), the goal of this guide is to help you understand the similarities and differences between the standard and Twitter API v2 search Tweets endpoint.

* **Similarities**
    * OAuth 1.0a User Context and OAuth 2.0 App-Only
    * Support for Tweet edit history and metadata. 
* **Differences**
    * Endpoint URLs
    * App and Project requirements
    * Response data format
    * Request parameters
    * New query operators  
        
    * AND / OR operator precedence