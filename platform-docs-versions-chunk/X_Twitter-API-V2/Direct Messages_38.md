platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference/get-dm_conversations-dm_conversation_id-dm_events

GET /2/dm\_conversations/:dm\_conversation\_id/dm\_events

# GET /2/dm\_conversations/:dm\_conversation\_id/dm\_events

Returns a list of Direct Messages within a conversation specified in the `dm_conversation_id` path parameter. Messages are returned in reverse chronological order.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=getDmConversationsIdDmEvents) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fdm_conversations%2F%7Bdm_conversation_id%7D%2Fdm_events&method=get) 

### Endpoint URL

`https://api.twitter.com/2/dm_conversations/:dm_conversation_id/dm_events`