platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/introduction

Introduction

## Introduction

Liking Tweets is one of the core features people use to engage in the public conversation on  Twitter. With endpoints in our Likes lookup endpoint group, you can see a list of accounts that have liked a given Tweet, or which Tweets a given account has liked. You could use these endpoints to understand what kind of content a specified account or group of accounts likes.