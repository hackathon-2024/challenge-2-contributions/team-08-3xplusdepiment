platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/rate-limits

### Twitter API v2 rate limits - Enterprise

To learn more about enterprise access rate limits reach out to your account manager.

**Please note**  

In addition to rate limits, we also have [Tweet caps](https://developer.twitter.com/en/docs/twitter-api/tweet-caps) that limit the number of Tweets that any [Project](https://developer.twitter.com/en/docs/projects) can retrieve from certain endpoints in a given month, which is based on your [access level](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api#v2-access-level).