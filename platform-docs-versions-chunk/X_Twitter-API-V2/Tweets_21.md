platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from the list:

|     |     |
| --- | --- |
| **Retrieve multiple Tweets with a list of IDs** | `[GET /2/tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets)` |
| **Retrieve a single Tweet with an ID** | `[GET /2/tweets/:id](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets-id)` |