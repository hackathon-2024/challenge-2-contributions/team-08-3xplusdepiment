platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/tweet-caps

### Note:

If the same Tweet is returned from multiple queries during the billing cycle, then the Tweet is only counted once against the monthly cap - i.e, the Tweets are deduplicated.

**Dedupe** Counter gets reset on a monthly basis  (at the end of the billing cycle).