platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/rate-limits

## Next steps

[Learn more about Tweet caps](https://developer.twitter.com/en1/docs/twitter-api/tweet-caps "Learn more about Tweet caps")