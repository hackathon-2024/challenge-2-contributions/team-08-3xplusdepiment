platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/integrate


### Including media attachments and referencing Tweets

The Manage Direct Message endpoints all support attaching one piece of media (photo, video, or GIF). Attaching media requires a media ID generated by the v1.1 [Upload media](https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/overview) endpoint. The authenticated user posting the Direct Message must have also uploaded the media. Once uploaded, media is available for 24 hours for including with the message.   

To illustrate how to include media, the following is an example JSON request body:

      `{  "text": "Here's a photo...",  "attachments": [{"media_id": "1583157113245011970}] }`
    

The resulting MessageCreate event will include the following metadata:

      `{     "attachments": {         "media_keys": [             "5_1583157113245011970"         ]     } }`
    

Tweets can also be included by including the Tweet URL in the message text. To illustrate how to include Tweets in messages, the following is an example JSON request body:

      `{  "text": "Here's the Tweet I has talking about: https://twitter.com/SnowBotDev/status/1580559079470145536" }`
    

The resulting MessageCreate event will include the following metadata:

      `{     "referenced_tweets": [         {             "id": "1580559079470145536"         }     ] }`