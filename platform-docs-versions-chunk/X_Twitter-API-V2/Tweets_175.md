platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/introduction

### Recent Tweet counts

The recent Tweet counts endpoint allows you to programmatically retrieve the numerical count of Tweets for a query, over the last seven days. This endpoint is available via to anyone using keys and tokens that are associated with an [App](https://developer.twitter.com/en/docs/apps) within a [Project](https://developer.twitter.com/en/docs/projects) and uses [OAuth 2.0 App-Only](https://developer.twitter.com/content/developer-twitter/en/docs/authentication/oauth-2-0) for authentication.