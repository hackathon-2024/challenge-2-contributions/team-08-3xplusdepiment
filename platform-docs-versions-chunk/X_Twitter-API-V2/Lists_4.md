platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-lookup/quick-start

Quick start

## Getting started with the List lookup endpoint

This quick start guide will help you make your first request to the List lookup endpoint using Postman.

Please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository if you want to see sample code in different languages.

**Note:** For this example, we will make a request to the _List lookup by ID_ endpoint, but you can apply the learnings from this quick start to other lookup requests as well.