platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/data-dictionary/introduction

### Migrating to the Twitter API v2 data format

Interested in learning more about how the Twitter API v2 data format compares to standard, premium, or enterprises' formats? Check out our data format comparison guides:

* [Standard v1.1 Native format to Twitter API v2 format migration guide](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/standard-v1-1-to-v2)
* [Native Enriched to Twitter API v2 format migration guide](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/native-enriched-to-v2)
* [Activity Streams to Twitter API v2 format migration guide](https://developer.twitter.com/en/docs/twitter-api/migrate/data-formats/activity-streams-to-v2)