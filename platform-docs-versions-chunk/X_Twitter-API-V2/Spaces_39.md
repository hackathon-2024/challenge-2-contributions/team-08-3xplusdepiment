platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-id-buyers

### Path parameters

| Name | Type | Description |
| --- | --- | --- |
| `id`  <br> Required | string | Unique identifier of the Space for which you want to request Tweets. |