platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/search/quick-start

## Next steps

[Customize your request using the API Reference](https://developer.twitter.com/en/docs/twitter-api/spaces/search/api-reference "Customize your request using the API Reference")

# Resource URL: https://developer.twitter.com/en/docs/twitter-api/spaces/search/api-reference/get-spaces-search 
GET /2/spaces/search

# GET /2/spaces/search

Return live or scheduled Spaces matching your specified search terms. This endpoint performs a keyword search, meaning that it will return Spaces that are an exact case-insensitive match of the specified search term. The search term will match the original title of the Space.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=searchSpaces&params=%28%27query%21%27Twitter%27%7Ebody%21%28%29%7Epath%21%28%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fspaces%2Fsearch&method=get)