platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference/put-lists-id

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "updated": true   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `updated` | boolean | Indicates whether the List specified in the request has been updated. |