platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/quick-start

## Next steps

[Customize your request using the API Reference](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference "Customize your request using the API Reference")