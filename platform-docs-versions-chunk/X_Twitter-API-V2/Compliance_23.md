platform: X
topic: Twitter-API-V2
subtopic: Compliance
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Compliance.md
url: https://developer.twitter.com/en/docs/twitter-api/compliance/batch-compliance/api-reference/post-compliance-jobs

POST /2/compliance/jobs

# POST /2/compliance/jobs

"Creates a new compliance job for Tweet IDs or user IDs.  
  
A compliance job will contain an ID and a destination URL. The destination URL represents the location that contains the list of IDs consumed by your App.  
  
You can run one batch job at a time."

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fcompliance%2Fjobs&method=post) 

### Endpoint URL

`https://api.twitter.com/2/compliance/jobs`