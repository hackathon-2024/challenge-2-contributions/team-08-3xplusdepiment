platform: X
topic: Twitter-API-V2
subtopic: Compliance
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Compliance.md
url: https://developer.twitter.com/en/docs/twitter-api/compliance/streams/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from the list:

|     |     |
| --- | --- |
| **Connect to Tweet Compliance event stream** | `[GET /2/tweets/compliance/stream](https://developer.twitter.com/en/docs/twitter-api/compliance/streams/api-reference/get-users-compliance-stream)` |
| **Connect to User Compliance event stream** | `[GET /2/users/compliance/stream](https://developer.twitter.com/en/docs/twitter-api/compliance/streams/api-reference/get-tweets-compliance-stream)` |