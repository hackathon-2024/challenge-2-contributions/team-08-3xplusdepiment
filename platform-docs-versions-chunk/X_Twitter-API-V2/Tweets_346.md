platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate/manage-retweets-standard-to-twitter-api-v2

## Next steps

[Check out our quick start guide for Twitter API v2 manage Retweets](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/quick-start/manage-retweets "Check out our quick start guide for Twitter API v2 manage Retweets")

[Review the API reference for the v2 Retweet endpoints](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/api-reference "Review the API reference for the v2 Retweet endpoints")