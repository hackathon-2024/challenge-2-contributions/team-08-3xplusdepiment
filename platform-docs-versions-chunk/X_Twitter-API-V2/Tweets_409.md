platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/migrate/manage-likes-standard-to-twitter-api-v2

## Next steps

[Check out our quick start guide for Twitter API v2 manage Likes](https://developer.twitter.com/en/docs/twitter-api/tweets/likes/quick-start/manage-likes "Check out our quick start guide for Twitter API v2 manage Likes")

[Review the API reference for the v2 Likes endpoints](https://developer.twitter.com/en/docs/twitter-api/tweets/likes/api-reference "Review the API reference for the v2 Likes endpoints")