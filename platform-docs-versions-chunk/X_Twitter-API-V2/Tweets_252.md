platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/migrate/standard-to-twitter-api-v2

Standard v1.1 compared to Twitter API v2

## Standard v1.1 compared to Twitter API v2

If you have been working with the v1.1 [statuses/filter](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/filter-realtime/api-reference/post-statuses-filter) endpoint, this guide can help you understand the similarities and differences between the standard and Twitter API v2 filtered stream endpoints.

* **Similarities**
    * Request parameters and operators
    * Support for Tweet edit history and metadata
* **Differences**
    * Endpoint URLs
    * App and Project requirement
    * Authentication method
    * Rule volume and persistent stream
    * Response data format
    * Request parameters
    * Availability of recovery and redundancy features
    * Query operators