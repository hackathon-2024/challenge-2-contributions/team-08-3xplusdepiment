platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/api-reference/get-users-id-liked_tweets

GET /2/users/:id/liked\_tweets

# GET /2/users/:id/liked\_tweets

Allows you to get information about a user’s liked Tweets.  
  
The Tweets returned by this endpoint count towards the Project-level [Tweet cap](https://developer.twitter.com/en/docs/twitter-api/tweet-caps).

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=usersIdLikedTweets&params=%28%27query%21%28%29%7Ebody%21%27%27%7Epath%21%28%27id%21%272244994945%27%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2F%7Bid%7D%2Fliked_tweets&method=get) 

### Endpoint URL

`https://api.twitter.com/2/users/:id/liked_tweets`