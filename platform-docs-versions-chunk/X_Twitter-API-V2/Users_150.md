platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/mutes/migrate/manage-mutes--standard-v1-1-compared-to-twitter-api-v2

Manage mutes: Standard v1.1 compared to Twitter API v2

## Manage mutes: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [POST mutes/users/create](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/post-mutes-users-create) and [POST mutes/users/destroy](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/post-mutes-users-destroy) endpoints, the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 manage mutes endpoints.

* **Similarities**
    * OAuth 1.0a User Context
* **Differences**
    * Endpoint URLs
    * App and Project requirements
    * HTTP methods
    * Request parameters