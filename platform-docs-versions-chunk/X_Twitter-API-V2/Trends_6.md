platform: X
topic: Twitter-API-V2
subtopic: Trends
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Trends.md
url: https://developer.twitter.com/en/docs/twitter-api/trends/api-reference/get-trends-by-woeid


### Example responses

* [Default Fields](#tab0)

Default Fields

      `{   "data": [     {       "trend_name": "#TEZOSTUESDAY",       "tweet_count": 14869     },     {       "trend_name": "Copenhagen",       "tweet_count": 13005     },     {       "trend_name": "Roses",       "tweet_count": 32193     },     {       "trend_name": "Heroes",       "tweet_count": 69798     },     {       "trend_name": "Cedric",       "tweet_count": 14259     },     {       "trend_name": "#AskSonic",       "tweet_count": 8908     },     {       "trend_name": "Nelson",       "tweet_count": 29841     },     {       "trend_name": "#PSVARS",       "tweet_count": 4915     },     {       "trend_name": "Eddie",       "tweet_count": 34139     },     {       "trend_name": "Saliba",       "tweet_count": 7191     },     {       "trend_name": "Walters",       "tweet_count": 8095     },     {       "trend_name": "Bakayoko",       "tweet_count": 1809     },     {       "trend_name": "Bibby Stockholm",       "tweet_count": 21021     },     {       "trend_name": "Nwaneri",       "tweet_count": 5783     },     {       "trend_name": "Doncaster",       "tweet_count": 3551     },     {       "trend_name": "Kiwior",       "tweet_count": 1535     }   ] }`