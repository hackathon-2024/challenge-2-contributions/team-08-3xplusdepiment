platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/api-reference/delete-users-user_id-likes

### Authentication and rate limits

|     |     |
| --- | --- |
| Authentication methods  <br>supported by this endpoint | [OAuth 1.0a User context](https://developer.twitter.com/en/docs/authentication/oauth-1-0a "This method allows an authorized app to act on behalf of the user, as the user. It is typically used to access or post public information for a specific user, and it us useful when your app needs to be aware of the relationship between a user and what this endpoint returns. Click to learn how to authenticate with user context.") |
| [Rate limit](https://developer.twitter.com/en/docs/rate-limits) | User rate limit: 50 requests per 15-minute window<br><br>User rate limit: 1000 requests per 24-hour window  <br>(Shared with [POST /2/users/:id/likes)](https://developer.twitter.com/en/docs/twitter-api/tweets/likes/api-reference/post-users-user_id-likes) |