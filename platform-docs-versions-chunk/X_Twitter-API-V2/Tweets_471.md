platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/api-reference/get-users-id-bookmarks


### Example responses

* [Default fields](#tab0)
* [Optional fields](#tab1)

Default fields

Optional fields

      `{   "data": [     {       "id": "1362449997430542337",       "text": "Honored to be the first developer to be featured in @TwitterDev's love fest 🥰♥️😍 https://t.co/g8TsPoZsij"     },     {       "id": "1365416026435854338",       "text": "We're so happy for our Official Partner @Brandwatch and their big news. https://t.co/3DwWBNSq0o https://t.co/bDUGbgPkKO"     },     {       "id": "1296487407475462144",       "text": "Check out this feature on @TwitterDev to learn more about how we're mining social media data to make sense of this evolving #publichealth crisis https://t.co/sIFLXRSvEX."     },     {       "id": "1294346980072624128",       "text": "I awake from five years of slumber https://t.co/OEPVyAFcfB"     },     {       "id": "1283153843367206912",       "text": "@wongmjane Wish we could tell you more, but I’m only a teapot 👀"     }   ],   "meta": {     "result_count": 5,     "next_token": "zldjwdz3w6sba13nbs0mbravfipbtqvbiqplg9h0p4k"   } }`
    

      `{   "data": [     {       "created_at": "2021-02-18T17:12:47.000Z",       "source": "Twitter Web App",       "id": "1362449997430542337",       "text": "Honored to be the first developer to be featured in @TwitterDev's love fest 🥰♥️😍 https://t.co/g8TsPoZsij"     },     {       "created_at": "2021-02-26T21:38:43.000Z",       "source": "Twitter Web App",       "id": "1365416026435854338",       "text": "We're so happy for our Official Partner @Brandwatch and their big news. https://t.co/3DwWBNSq0o https://t.co/bDUGbgPkKO"     },     {       "created_at": "2020-08-20T16:41:00.000Z",       "source": "Twitter Web App",       "id": "1296487407475462144",       "text": "Check out this feature on @TwitterDev to learn more about how we're mining social media data to make sense of this evolving #publichealth crisis https://t.co/sIFLXRSvEX."     },     {       "created_at": "2020-08-14T18:55:42.000Z",       "source": "Twitter for Android",       "id": "1294346980072624128",       "text": "I awake from five years of slumber https://t.co/OEPVyAFcfB"     },     {       "created_at": "2020-07-14T21:38:10.000Z",       "source": "Twitter for  iPhone",       "id": "1283153843367206912",       "text": "@wongmjane Wish we could tell you more, but I’m only a teapot 👀"     }   ],   "meta": {     "result_count": 5,     "next_token": "zldjwdz3w6sba13nbs0mbravfipbtqvbiqplg9h0p4k"   } }`