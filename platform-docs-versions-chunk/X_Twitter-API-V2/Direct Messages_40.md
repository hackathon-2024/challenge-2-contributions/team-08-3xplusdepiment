platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference/get-dm_conversations-dm_conversation_id-dm_events

### Path parameters

| Name | Type | Description |
| --- | --- | --- |
| `dm_conversation_id`  <br> Required | string | The `id` of the Direct Message conversation for which events are being retrieved. |