platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/blocks/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from the list.  
 

### Blocks lookup

|     |     |
| --- | --- |
| **Returns a list of users who are blocked by the specified user ID** | `[GET /2/users/:id/blocking](https://developer.twitter.com/en/docs/twitter-api/users/blocks/api-reference/get-users-blocking)` |