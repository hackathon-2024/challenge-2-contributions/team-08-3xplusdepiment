platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/api-reference/get-tweets-id-retweets

GET /2/tweets/:id/retweets

# GET /2/tweets/:id/retweets

Returns the Retweets for a given Tweet ID.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

### Endpoint URL

`https://api.twitter.com/2/tweets/:id/retweets`