platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/rate-limits

### Table of contents

* [Twitter API v2 Free rate limits](#v2-limits-free)  
* [Twitter API v2 Basic rate limits](#v2-limits-basic)  
* [Twitter API v2 Pro rate limits](#v2-limits-pro)  
* [Twitter API Enterprise rate limits](#v2-limits-enterprise)  
* [Rate limits and authentication method](#auth)
* [HTTP headers and response codes](#headers-and-codes)
* [Recovering from rate limits](#recovering)
* [Tips to avoid being rate limited](#tips)