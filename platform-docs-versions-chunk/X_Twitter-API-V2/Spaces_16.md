platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-id

GET /2/spaces/:id

# GET /2/spaces/:id

Returns a variety of information about a single Space specified by the requested ID.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findSpaceById&params=%28%27query%21%28%27space.fields%21%27lang%2Ctitle%2Ccreated_at%27%7Eexpansions%21%27creator_id%27%29%7Ebody%21%28%29%7Epath%21%28%21id%21%271OwxWzlwbrnJQ%27%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fspaces%2F%7Bid%7D&method=get) 

### Endpoint URL

`https://api.twitter.com/2/spaces/:id`