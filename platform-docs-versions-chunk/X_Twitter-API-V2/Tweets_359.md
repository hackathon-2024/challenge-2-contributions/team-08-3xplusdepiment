platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/api-reference/post-users-id-retweets

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "retweeted": true   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `retweeted` | boolean | Indicates whether the user Retweets the specified Tweet as a result of this request. |