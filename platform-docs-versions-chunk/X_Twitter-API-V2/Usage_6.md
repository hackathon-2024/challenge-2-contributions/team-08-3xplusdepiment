platform: X
topic: Twitter-API-V2
subtopic: Usage
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Usage.md
url: https://developer.twitter.com/en/docs/twitter-api/usage/tweets/api-reference/get-usage-tweets

### Query parameters

| Name | Type | Description |
| --- | --- | --- |
| `days`  <br> Optional | number | The number of days for which you need the Tweet usage for. You can get usage for upto 90 days. |
| `usage.fields`  <br> Optional | enum (`daily_client_app_usage`, `daily_project_usage`) | This parameter is used to specify if you want the daily usage returned and at a client app level. |