platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/tweet-caps

### Tweet cap volumes

The Tweet cap volume depends on your access level:

|     |     |
| --- | --- |
| **Free** tier | 1,500 Tweets per month |
| **Basic** tier | 10,000 Tweets per month |
| **Pro** tier | 1,000,000 Tweets per month |
| **Enterprise** tier | 50+ million Tweets per month |

  
You can check your usage towards the monthly Tweet cap by viewing the [main dashboard page](https://developer.twitter.com/content/developer-twitter/en/portal/dashboard) in the [developer portal](https://developer.twitter.com/en/docs/developer-portal). Under the name of your Project, you'll see a status bar that illustrates your current month’s usage in relation to the Tweet cap. You will also see the number of Tweets you pulled this month, the percentage of Tweets used in relation to the cap, and the date your Tweet cap usage resets.