platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/api-reference

API reference

## API reference index

For a complete API reference, please select an endpoint from below.

### Recent Tweet counts

|     |     |
| --- | --- |
| **Receive a count of Tweets that match a query in the last 7 days** | `[GET /2/tweets/counts/recent](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/api-reference/get-tweets-counts-recent)` |

### Full-archive Tweet counts

Only available to those with [](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api#v2-access-level)[Pro and Enterprise access](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api#v2-access-level)

|     |     |
| --- | --- |
| **Receive a count of Tweets that match a query** | `[GET /2/tweets/counts/all](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/api-reference/get-tweets-counts-all)` |