platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/integrate

## Next steps

[Make your first request to a Tweet counts endpoint](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/quick-start "Make your first request to a Tweet counts endpoint")

[See a full list of parameters, fields, and more in our API Reference pages](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/api-reference "See a full list of parameters, fields, and more in our API Reference pages")

[Get support or troubleshoot an error](https://developer.twitter.com/en/support/twitter-api "Get support or troubleshoot an error")