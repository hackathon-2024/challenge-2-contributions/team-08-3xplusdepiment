platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from the list:  
 

### Bookmarks lookup

|     |     |
| --- | --- |
| **Lookup a user's Bookmarks** | [GET /2/users/:id/bookmarks](https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/api-reference/get-users-id-bookmarks) |

### Manage Bookmarks

|     |     |
| --- | --- |
| **Bookmark a Tweet** | `[POST /2/users/:id/bookmarks](https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/api-reference/post-users-id-bookmarks)` |
| **Remove a Bookmark of a Tweet** | `[DELETE /2/users/:id/bookmarks/:tweet_id](https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/api-reference/delete-users-id-bookmarks-tweet_id)` |