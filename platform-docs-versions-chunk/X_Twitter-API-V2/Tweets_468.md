platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/api-reference/get-users-id-bookmarks

### Path parameters

| Name | Type | Description |
| --- | --- | --- |
| `id`  <br> Required | string | User ID of an authenticated user to request bookmarked Tweets for. |