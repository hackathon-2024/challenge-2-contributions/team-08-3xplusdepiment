platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/blocks/migrate

Migrate

## Other migration resources

[Blocks lookup: Standard v1.1 to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/users/blocks/migrate/blocks-lookup-standard-to-twitter-api-v2 "Blocks lookup: Standard v1.1 to Twitter API v2")

[Twitter API migration hub](https://developer.twitter.com/en/docs/twitter-api/migrate "Twitter API migration hub")