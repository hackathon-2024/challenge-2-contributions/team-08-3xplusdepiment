platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/migrate/manage-lists--standard-v1-1-compared-to-twitter-api-v2

## Next steps

[Review the manage Lists API references](https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference "Review the manage Lists  API references")