platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/migrate/standard-to-twitter-api-v2

## Next steps

[Check out our quick start guide for Twitter API v2 users lookup](https://developer.twitter.com/en/docs/twitter-api/users/lookup/quick-start "Check out our quick start guide for Twitter API v2 users lookup")

[Review the API reference for users lookup](https://developer.twitter.com/en/docs/twitter-api/users/lookup/api-reference "Review the API reference for users lookup")

[Check out some sample code for these endpoints](https://github.com/twitterdev/Twitter-API-v2-sample-code "Check out some sample code for these endpoints")