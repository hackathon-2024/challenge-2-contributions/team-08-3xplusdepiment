platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-id-buyers

GET /2/spaces/:id/buyers

# GET /2/spaces/:id/buyers

Returns a list of user who purchased a ticket to the requested Space. You must authenticate the request using the Access Token of the creator of the requested Space.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=spaceBuyers&params=%28%27query%21%28%29%7Ebody%21%27%27%7Epath%21%28%27id%21%271nAKEYqvyoAKL%27%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fspaces%2F%7Bid%7D%2Fbuyers&method=get) 

### Endpoint URL

`https://api.twitter.com/2/spaces/:id/buyers`