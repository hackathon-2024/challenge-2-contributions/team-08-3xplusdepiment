platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/search/quick-start

Quick start

## Getting started with the users search endpoint

This quick start guide will help you make your first request to the users search endpoints using [Postman](https://developer.twitter.com/en/docs/tools-and-libraries/using-postman).

If you would like to see sample code in different languages, please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository.   

**Note**: This endpoint is available only to developers with Pro access. Sign up for Pro access [here](https://developer.twitter.com/en/portal/products/pro).