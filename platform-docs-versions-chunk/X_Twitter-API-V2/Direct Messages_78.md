platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/api-reference/post-dm_conversations

POST /2/dm\_conversations

# POST /2/dm\_conversations

Creates a new group conversation and adds a Direct Message to it on behalf of an authenticated user.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=dmConversationIdCreate) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fdm_conversations&method=post) 

### Endpoint URL

`https://api.twitter.com/2/dm_conversations`