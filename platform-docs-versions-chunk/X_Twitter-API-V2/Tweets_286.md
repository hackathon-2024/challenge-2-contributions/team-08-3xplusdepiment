platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/quick-start/sampled-stream

Sampled stream

## Getting started with the sampled stream endpoints

This quick start guide will help you make your first request to a sampled stream endpoint using a cURL request. cURL is a command-line tool that allows you to make requests with minimal configuration.

If you would like to see sample code in different languages, please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository. The examples there are based on the 1% sampled stream and are easily updated for the 10% sampled stream. See the repository README for more details.   

Note: The steps below are also based on making requests to the 1% sampled stream. If you have Enterprise access and are working with the 10% sampled stream, see the Using the 10% sampled stream section below.