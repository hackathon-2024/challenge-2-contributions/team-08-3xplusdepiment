platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets

GET /2/tweets

# GET /2/tweets

Returns a variety of information about the Tweet specified by the requested ID or list of IDs.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findTweetsById&params=%28%27query%21%28%27expansions%21%27%27*-%7Ebody%21%28%29%7Epath%21%28%29*-*%7Eids%21%27146.37035677698A.43339741184A-.46120540165%27%29.03237A%2C146%01A.-*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets`