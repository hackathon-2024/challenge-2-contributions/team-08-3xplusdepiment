platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/api-reference/put-tweets-id-hidden

PUT /2/tweets/:id/hidden

# PUT /2/tweets/:id/hidden

Hides or unhides a reply to a Tweet.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=hideReplyById&params=%28%27query%21%28%29%7Ebody%21%27%27%7Epath%21%28%27id%21%27%27%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets%2F%7Bid%7D%2Fhidden&method=put) 

### Endpoint URL

`https://api.twitter.com/2/tweets/:id/hidden`