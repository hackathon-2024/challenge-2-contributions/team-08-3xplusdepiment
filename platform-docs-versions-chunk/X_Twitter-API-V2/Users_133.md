platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/mutes/quick-start/mutes-lookup

## Next steps

[Customize your request using the API Reference](https://developer.twitter.com/en/docs/twitter-api/users/mutes/api-reference "Customize your request using the API Reference")

[Reach out to the community for help](https://twittercommunity.com/ "Reach out to the community for help")