platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/versioning

### Deprecation and retirement

First of all, here is our definition of what deprecation and retirement mean to the Twitter API:

* **Deprecation:** The feature is no longer supported by the team. No new functionality will be released around that feature, and if there are any bugs or issues with the product, the chances that we will explore a fix are extremely low. 
* **Retired:** The feature will no longer be accessible.

In most cases, as soon as a new version is released, the previous version will be marked as deprecated. Versions will remain in a deprecated state for a period of time, after which they will be retired. 

Please [stay informed](https://developer.twitter.com/en/updates/stay-informed) to learn more about future deprecations and retirements.