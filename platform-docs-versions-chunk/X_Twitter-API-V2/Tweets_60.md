platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/api-reference/post-tweets

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "id": "1445880548472328192",     "text": "Are you excited for the weekend?"   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `id` | string | The ID of the newly created Tweet. |
| `text` | string | The text of the newly created Tweet. |