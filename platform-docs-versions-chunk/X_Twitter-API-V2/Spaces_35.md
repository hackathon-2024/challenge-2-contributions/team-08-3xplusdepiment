platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-by-creator-ids

### Example responses

* [Default fields](#tab0)
* [Optional fields](#tab1)

Default fields

Optional fields

      `{   "data": [     {       "id": "1DXxyRYNejbKM",       "state": "live"     }   ],   "meta": {     "result_count": 2   } }`
    

      `{   "data": [     {       "host_ids": [         "2244994945"       ],       "id": "1DXxyRYNejbKM",       "state": "live"     },     {       "host_ids": [         "6253282"       ],       "id": "1nAJELYEEPvGL",       "state": "scheduled"     }   ],   "meta": {     "result_count": 2   } }`