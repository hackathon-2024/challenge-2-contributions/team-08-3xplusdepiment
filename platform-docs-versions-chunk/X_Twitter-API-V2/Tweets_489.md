platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/apps

Apps

## Apps using the hide replies endpoint

Here's a collection of noteworthy apps built by developers who integrated the hide replies endpoint. By matching their technology with the Twitter API, they created useful experiences to help all users enhance the public conversation.

  

## Clarabridge

Clarabridge integrates hide replies so that companies can keep their Twitter feed clean and interesting to read. Replies can be hidden manually, and agents can also use Clarabridge’s text analytics to automatically detect profanity and other language that is not in line with brand policy.

[Go to website ▸](https://www.clarabridge.com/ "Go to website ▸")

[Clarabridge on Twitter](https://twitter.com/Clarabridge "Clarabridge on Twitter")