platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces

GET /2/spaces

# GET /2/spaces

Returns details about multiple Spaces. Up to 100 comma-separated Spaces IDs can be looked up using this endpoint.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findSpacesByIds&params=%28%27query%21%28%29~body%21%27%27~path%21%28%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fspaces&method=get) 

### Endpoint URL

`https://api.twitter.com/2/spaces`