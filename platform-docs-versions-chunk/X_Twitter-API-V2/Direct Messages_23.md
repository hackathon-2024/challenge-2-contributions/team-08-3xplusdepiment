platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/migrate/standard-to-twitter-api-v2

## Next steps

[Quick start guide](https://developer.twitter.com/en/docs-vnext/twitter-api/tweets/lookup/quick-start "Quick start guide")

[API reference](https://developer.twitter.com/en/docs-vnext/twitter-api/tweets/lookup/api-reference/get-tweets "API reference")

[Sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code "Sample code")