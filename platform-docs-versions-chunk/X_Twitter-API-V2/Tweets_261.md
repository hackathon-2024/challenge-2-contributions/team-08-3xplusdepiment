platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/api-reference/get-tweets-search-stream-rules

GET /2/tweets/search/stream/rules

# GET /2/tweets/search/stream/rules

Return either a single rule, or a list of rules that have been added to the stream.  
  
If you would like to initiate the stream to receive all Tweets that match these rules in real-time, you will need to use the [GET /tweets/search/stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/api-reference/get-tweets-search-stream) endpoint.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets%2Fsearch%2Fstream%2Frules&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets/search/stream/rules`