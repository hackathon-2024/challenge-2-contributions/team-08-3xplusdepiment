platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/rate-limits

### Twitter API v2 rate limits - Free

The following table lists the rate limits for the Twitter API v2 Free access. These rate limits are also documented on each endpoint's API Reference page and also displayed in the  [developer portal](https://developer.twitter.com/en/docs/developer-portal)'s products section.

|     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- |
| **Endpoint** | **#Requests** | **Window of time** | **Per** | **Part of the Tweet pull cap?** | **Effective 30-day limit** |
| POST\_2\_tweets | 50  | 24 hours | per user | no  | 1,500 |
| 50  | 24 hours | per app | no  | 1,500 |
| DELETE\_2\_tweets\_id | 50  | 24 hours | per user | no  | 1,500 |
| 50  | 24 hours | per app | no  | 1,500 |
| GET\_2\_users\_me | 25  | 24 hours | per user | no  | 750 |