platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-members/quick-start/manage-list-members

Manage List members

**Please note:** This guide assumes you have completed the prerequisites from the [quick start overview](https://developer.twitter.com/en/docs/twitter-api/lists/list-members/quick-start).