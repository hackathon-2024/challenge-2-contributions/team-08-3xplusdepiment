platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/migrate/standard-to-twitter-api-v2

Standard v1.1 compared to Twitter API v2

## Standard v1.1 compared to Twitter API v2

If you have been working with the v1.1 [statuses/sample](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/v1/tweets/sample-realtime/overview/get_statuses_sample) endpoint, the goal of this guide is to help you understand the similarities and differences between the standard and Twitter API v2 sample stream endpoints.

* **Differences**
    * Endpoint URLs
    * App and Project requirements
    * Authentication method
    * Response data format
    * Request parameters
    * Availability of recovery and redundancy features