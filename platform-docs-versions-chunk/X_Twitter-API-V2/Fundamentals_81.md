platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/versioning

### Breaking changes

These changes require developers to change their code to maintain existing functionality of their application.

* Addition of a new required parameter
* Removal of an existing endpoint
* Removal of any field in the response (either required or optional)
* Removal of a query parameter
* Restructuring of the input or output format (for example, making a top-level field a sub-field, or changing the location of errors to be inline)
* Changing the name or data type of an existing input parameter or output value
* Changing the name of a field
* Changing the resource name
* Changing a response code
* Changing error types
* Changes to existing authorization scopes  
    

### Non-breaking changes

* Addition of a new endpoint
* Addition of a new optional parameter
* Addition of a new response field
* Changing text in error messages
* Availability of new scopes
* “Nulling” of fields (changing the value of a to null for privacy/security reasons as an alternative to removing it altogether)