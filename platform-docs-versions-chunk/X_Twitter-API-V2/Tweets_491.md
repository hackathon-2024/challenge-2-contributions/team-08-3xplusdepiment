platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/apps

## Reshuffle

Reshuffle is a platform to connect business applications with flexible scripts. They built an integration script to detect and hide replies that include keywords you define. To try it out, make sure you have a valid app enabled for Hide replies, then follow the instructions in the repo.

[Go to app ▸](https://github.com/reshufflehq/reshuffle "Go to app ▸")

[Reshuffle on Twitter](https://twitter.com/reshufflehq "Reshuffle on Twitter")

  

## Hide unwanted replies

Dara Oladosu, the developer behind the popular app QuotedReplies, built an app that automatically hides replies that meet some of the criteria that he’s determined are more likely to exhibit abusive behavior, including replies that contain certain keywords he’s muted in the past.

[Go to app ▸](https://hideunwantedreplies.com/ "Go to app ▸")

[Dara Oladosu on Twitter](https://twitter.com/dara_tobi "Dara Oladosu on Twitter")