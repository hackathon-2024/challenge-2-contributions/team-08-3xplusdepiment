platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate

Integrate

## How to integrate with the Search Tweets endpoints

This page contains information on several tools and key concepts that you should be aware of as you integrate the recent search or full archive search endpoints into your system. We’ve split the page into the following sections:

* [Helpful tools](#helpful-tools)
* Key concepts
    * [Authentication](#authentication)
    * [Developer portal, Projects, and Apps](#developer-portal)
    * [Rate limits](#rate-limits)
    * [Fields and expansions](#fields-expansions)
    * [Metrics](#metrics)
    * [Building search queries](#queries)
    * [Pagination](#pagination)
    * [Tweet caps](#tweet-caps)   
        
    * [Tweet edits](#tweet-edits)