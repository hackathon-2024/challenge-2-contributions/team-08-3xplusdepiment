platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/annotations/overview


### **Sample Request**

      `curl --location --request GET 'https://api.twitter.com/2/tweets/1212092628029698048?tweet.fields=context_annotations,entities' --header 'Authorization: Bearer $BEARER_TOKEN'`
    

**  
Sample Response**

      `{     "data": {         "context_annotations": [             {                 "domain": {                     "id": "119",                     "name": "Holiday",                     "description": "Holidays like Christmas or Halloween"                 },                 "entity": {                     "id": "1186637514896920576",                     "name": " New Years Eve"                 }             },             {                 "domain": {                     "id": "119",                     "name": "Holiday",                     "description": "Holidays like Christmas or Halloween"                 },                 "entity": {                     "id": "1206982436287963136",                     "name": "Happy New Year: It’s finally 2020 everywhere!",                     "description": "Catch fireworks and other celebrations as people across the globe enter the new year.\nPhoto via @GettyImages "                 }             },             {                 "domain": {                     "id": "45",                     "name": "Brand Vertical",                     "description": "Top level entities that describe a Brands industry"                 }             },             {                 "domain": {                     "id": "46",                     "name": "Brand Category",                     "description": "Categories within Brand Verticals that narrow down the scope of Brands"                 },                 "entity": {                     "id": "781974596752842752",                     "name": "Services"                 }             },             {                 "domain": {                     "id": "47",                     "name": "Brand",                     "description": "Brands and Companies"                 },                 "entity": {                     "id": "10045225402",                     "name": "Twitter"                 }             },             {                 "domain": {                     "id": "119",                     "name": "Holiday",                     "description": "Holidays like Christmas or Halloween"                 },                 "entity": {                     "id": "1206982436287963136",                     "name": "Happy New Year: It’s finally 2020 everywhere!",                     "description": "Catch fireworks and other celebrations as people across the globe enter the new year.\nPhoto via @GettyImages "                 }             }         ],         "entities": {             "annotations": [                 {                     "start": 144,                     "end": 150,                     "probability": 0.626,                     "type": "Product",                     "normalized_text": "Twitter"                 }             ],             "urls": [                 {                     "start": 222,                     "end": 245,                     "url": "https://t.co/yvxdK6aOo2",                     "expanded_url": "https://twitter.com/LovesNandos/status/1211797914437259264/photo/1",                     "display_url": "pic.twitter.com/yvxdK6aOo2"                 }             ]         },         "id": "1212092628029698048",         "text": "We believe the best future version of our API will come from building it with YOU. Here’s to another great year with everyone who builds on the Twitter platform. We can’t wait to continue working with you in the new year. https://t.co/yvxdK6aOo2"     } }`
    

**  
Sample App**

See the [Tweet Entity Extractor on Glitch](https://tweet-entity-extractor.glitch.me/) to easily discover context annotations in Tweets and see how this feature works.