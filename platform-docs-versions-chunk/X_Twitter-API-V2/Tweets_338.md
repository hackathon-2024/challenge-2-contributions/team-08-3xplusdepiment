platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate

## Other migration resources

[Retweets lookup: Standard v1.1 to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate/retweets-lookup-standard-to-twitter-api-v2 "Retweets lookup: Standard v1.1 to Twitter API v2")

[Manage Retweets: Standard v1.1 to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate/manage-retweets-standard-to-twitter-api-v2 "Manage Retweets: Standard v1.1 to Twitter API v2")

[Twitter API migration hub](https://developer.twitter.com/en/docs/twitter-api/migrate "Twitter API migration hub")