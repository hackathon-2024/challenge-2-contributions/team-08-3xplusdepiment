platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/integrate

## Next steps

[API reference](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/api-reference "API reference")