platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/rate-limits


### Twitter API v2 rate limits - Pro

The following table lists the rate limits for the Twitter API v2 Pro access. These rate limits are also documented on each endpoint's API Reference page and also displayed in the  [developer portal](https://developer.twitter.com/en/docs/developer-portal)'s products section.

|     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- |
| **Endpoint** | **#Requests** | **Window of time** | **Per** | **Part of the Tweet pull cap?** | **Effective 30-day limit** |
| DELETE\_2\_lists\_param | 300 | 15 minutes | per user | no  | 864,000 |
| DELETE\_2\_lists\_param\_members\_param | 300 | 15 minutes | per user | no  | 864,000 |
| DELETE\_2\_tweets\_param | 50  | 15 minutes | per user | no  | 144,000 |
| DELETE\_2\_users\_id\_bookmarks\_tweet\_id | 50  | 15 minutes | per user | no  | 144,000 |
| DELETE\_2\_users\_param\_blocking\_param | 50  | 15 minutes | per user | no  | 144,000 |
| DELETE\_2\_users\_param\_followed\_lists\_param | 50  | 15 minutes | per user | no  | 144,000 |
| DELETE\_2\_users\_param\_following\_param | 50  | 15 minutes | per user | no  | 144,000 |
| DELETE\_2\_users\_param\_likes\_param | 50  | 15 minutes | per user | no  | 144,000 |
| 1000 | 24 hours | per user | no  | 30,000 |
| DELETE\_2\_users\_param\_muting\_param | 50  | 15 minutes | per user | no  | 144,000 |
| DELETE\_2\_users\_param\_pinned\_lists\_param | 50  | 15 minutes | per user | no  | 144,000 |
| DELETE\_2\_users\_param\_retweets\_param | 50  | 15 minutes | per user | no  | 144,000 |
| GET\_2\_compliance\_jobs | 150 | 15 minutes | per app | no  | 432,000 |
| GET\_2\_compliance\_jobs\_param | 150 | 15 minutes | per app | no  | 432,000 |
| GET\_2\_dm\_conversations\_param\_dm\_events | 100 | 15 minutes | per user | no  | 288,000 |
| GET\_2\_dm\_conversations\_with\_param\_dm\_events | 100 | 15 minutes | per user | no  | 288,000 |
| GET\_2\_dm\_events | 100 | 15 minutes | per user | no  | 288,000 |
| GET\_2\_lists\_id | 75  | 15 minutes | per user | no  | 216,000 |
| 75  | 15 minutes | per app | no  | 216,000 |
| GET\_2\_lists\_id\_members | 900 | 15 minutes | per user | no  | 2,592,000 |
| 900 | 15 minutes | per app | no  | 2,592,000 |
| GET\_2\_lists\_id\_tweets | 900 | 15 minutes | per user | yes | 2,592,000 |
| 900 | 15 minutes | per app | yes | 2,592,000 |
| GET\_2\_spaces | 300 | 15 minutes | per user | no  | 864,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_spaces\_by\_creator\_ids | 300 | 15 minutes | per user | no  | 864,000 |
| 1   | 1 second | per app | no  | 2,592,000 |
| 1   | 1 second | per user | no  | 2,592,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_spaces\_param | 300 | 15 minutes | per app | no  | 864,000 |
| 300 | 15 minutes | per user | no  | 864,000 |
| GET\_2\_spaces\_param\_buyers | 300 | 15 minutes | per user | no  | 864,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_spaces\_param\_tweets | 300 | 15 minutes | per user | no  | 864,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_spaces\_search | 300 | 15 minutes | per user | no  | 864,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_tweets | 900 | 15 minutes | per user | yes | 2,592,000 |
| 450 | 15 minutes | per app | yes | 1,296,000 |
| GET\_2\_tweets\_counts\_recent | 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_tweets\_param | 900 | 15 minutes | per user | yes | 2,592,000 |
| 450 | 15 minutes | per app | yes | 1,296,000 |
| GET\_2\_tweets\_param\_liking\_users | 75  | 15 minutes | per user | no  | 216,000 |
| 75  | 15 minutes | per app | no  | 216,000 |
| GET\_2\_tweets\_param\_quote\_tweets | 75  | 15 minutes | per user | yes | 216,000 |
| 75  | 15 minutes | per app | yes | 216,000 |
| GET\_2\_tweets\_param\_retweeted\_by | 75  | 15 minutes | per app | yes | 216,000 |
| 75  | 15 minutes | per user | yes | 216,000 |
| GET\_2\_tweets\_search\_recent | 450 | 15 minutes | per app | yes | 1,296,000 |
| 300 | 15 minutes | per user | yes | 864,000 |
| GET\_2\_tweets\_search\_stream | 50  | 15 minutes | per app | yes | 144,000 |
| GET\_2\_tweets\_search\_stream\_rules | 450 | 15 minutes | per app | no  | 1,296,000 |
| GET\_2\_users | 300 | 15 minutes | per app | no  | 864,000 |
| 900 | 15 minutes | per user | no  | 2,592,000 |
| GET\_2\_users\_by | 900 | 15 minutes | per user | no  | 2,592,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_users\_by\_username\_param | 900 | 15 minutes | per user | no  | 2,592,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_users\_by\_username\_param\_followers | 15  | 15 minutes | per user | no  | 43,200 |
| 15  | 15 minutes | per app | no  | 43,200 |
| GET\_2\_users\_by\_username\_param\_mentions | 450 | 15 minutes | per app | no  | 1,296,000 |
| 180 | 15 minutes | per user | no  | 518,400 |
| GET\_2\_users\_by\_username\_param\_tweets | 1500 | 15 minutes | per app | yes | 4,320,000 |
| 900 | 15 minutes | per user | yes | 2,592,000 |
| GET\_2\_users\_id\_bookmarks | 180 | 15 minutes | per user | no  | 518,400 |
| GET\_2\_users\_id\_list\_memberships | 75  | 15 minutes | per app | no  | 216,000 |
| 75  | 15 minutes | per user | no  | 216,000 |
| GET\_2\_users\_id\_owned\_lists | 15  | 15 minutes | per app | no  | 43,200 |
| 15  | 15 minutes | per user | no  | 43,200 |
| GET\_2\_users\_id\_pinned\_lists | 15  | 15 minutes | per user | no  | 43,200 |
| 15  | 15 minutes | per app | no  | 43,200 |
| GET\_2\_users\_me | 75  | 15 minutes | per user | no  | 216,000 |
| GET\_2\_users\_param | 900 | 15 minutes | per user | no  | 2,592,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_users\_param\_blocking | 15  | 15 minutes | per user | no  | 43,200 |
| GET\_2\_users\_param\_following\_spaces | 300 | 15 minutes | per user | no  | 864,000 |
| 300 | 15 minutes | per app | no  | 864,000 |
| GET\_2\_users\_param\_liked\_tweets | 75  | 15 minutes | per app | yes | 216,000 |
| 75  | 15 minutes | per user | yes | 216,000 |
| GET\_2\_users\_param\_mentions | 450 | 15 minutes | per app | yes | 1,296,000 |
| 300 | 15 minutes | per user | yes | 864,000 |
| GET\_2\_users\_param\_muting | 15  | 15 minutes | per user | no  | 43,200 |
| GET\_2\_users\_param\_timelines\_reverse\_chronological | 180 | 15 minutes | per user | no  | 518,400 |
| GET\_2\_users\_param\_tweets | 1500 | 15 minutes | per app | yes | 4,320,000 |
| 900 | 15 minutes | per user | yes | 2,592,000 |
| POST\_2\_compliance\_jobs | 150 | 15 minutes | per app | no  | 432,000 |
| POST\_2\_dm\_conversations | 200 | 15 minutes | per user | no  | 576,000 |
| 2500 | 24 hours | per app | no  | 75,000 |
| 1000 | 24 hours | per user | no  | 30,000 |
| POST\_2\_dm\_conversations\_param\_messages | 2500 | 24 hours | per app | no  | 75,000 |
| 1000 | 24 hours | per user | no  | 30,000 |
| 200 | 15 minutes | per user | no  | 576,000 |
| POST\_2\_dm\_conversations\_with\_param\_messages | 2500 | 24 hours | per app | no  | 75,000 |
| 1000 | 24 hours | per user | no  | 30,000 |
| 200 | 15 minutes | per user | no  | 576,000 |
| POST\_2\_lists | 300 | 15 minutes | per user | no  | 864,000 |
| POST\_2\_lists\_param\_members | 300 | 15 minutes | per user | no  | 864,000 |
| POST\_2\_tweets | 100 | 15 minutes | per user | no  | 288,000 |
| 10000 | 24 hours | per app | no  | 300,000 |
| POST\_2\_tweets\_search\_stream\_rules | 100 | 15 minutes | per user | no  | 288,000 |
| POST\_2\_users\_id\_bookmarks | 50  | 15 minutes | per user | no  | 144,000 |
| POST\_2\_users\_param\_blocking | 50  | 15 minutes | per user | no  | 144,000 |
| POST\_2\_users\_param\_followed\_lists | 50  | 15 minutes | per user | no  | 144,000 |
| POST\_2\_users\_param\_following | 50  | 15 minutes | per user | no  | 144,000 |
| POST\_2\_users\_param\_likes | 1000 | 24 hours | per user | no  | 30,000 |
| 50  | 15 minutes | per user | no  | 144,000 |
| POST\_2\_users\_param\_muting | 50  | 15 minutes | per user | no  | 144,000 |
| POST\_2\_users\_param\_pinned\_lists | 50  | 15 minutes | per user | no  | 144,000 |
| POST\_2\_users\_param\_retweets | 50  | 15 minutes | per user | no  | 144,000 |
| PUT\_2\_lists\_param | 300 | 15 minutes | per user | no  | 864,000 |
| PUT\_2\_tweets\_param\_hidden | 50  | 15 minutes | per user | no  | 144,00 |