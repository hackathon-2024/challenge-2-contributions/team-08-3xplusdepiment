platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/data-dictionary/introduction

Introduction

Across the Twitter API endpoints, there are a variety of objects available to request such as Tweets and users. Each GET endpoint will have a top-level resource and object, such as Tweets in [recent search](https://developer.twitter.com/en/docs/twitter-api/tweets/search/introduction.html) and [filtered stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/introduction.html), and users in [users lookup](https://developer.twitter.com/en/docs/twitter-api/users/lookup/introduction.html).

To see each object and it's included fields, please visit the following:

* [Tweets](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet)
* [Users](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/user)
* [Spaces](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/space)
* [Lists](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/list)
* [Media](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/media)
* [Polls](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/poll)
* [Places](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/place)