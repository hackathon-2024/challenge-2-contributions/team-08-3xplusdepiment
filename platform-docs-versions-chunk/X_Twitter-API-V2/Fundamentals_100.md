platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/tweet-caps

## Next steps

[Learn more about Twitter Apps](https://developer.twitter.com/en/docs/apps "Learn more about Twitter Apps")

[Learn more about Projects](https://developer.twitter.com/en/docs/projects "Learn more about Projects")

[Learn more about Twitter API v2 access levels](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api "Learn more about Twitter API v2 access levels")