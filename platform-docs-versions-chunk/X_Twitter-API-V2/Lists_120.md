platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-members/migrate/manage-list-members-standard-to-twitter-api-v2

Manage List members: Standard v1.1 compared to Twitter API v2

## Manage List members: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [POST lists/members/create](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/create-manage-lists/api-reference/post-lists-members-create) and [POST lists/members/destroy](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/create-manage-lists/api-reference/post-lists-members-destroy) endpoints, the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 manage List members endpoints.

* **Similarities**
    * Authentication
* **Differences**
    * Endpoint URLs
    * App and Project requirements
    * HTTP methods
    * Rate limits
    * Request parameters