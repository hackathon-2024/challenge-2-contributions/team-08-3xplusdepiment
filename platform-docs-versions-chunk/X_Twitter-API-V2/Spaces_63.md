platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/search/quick-start

### Example responses

* [Response](#tab0)

Response

      `{   "data": [     {       "host_ids": [         "2244994945"       ],       "id": "1DXxyRYNejbKM",       "state": "live",       "title": "hello world 👋"     },     {       "host_ids": [         "6253282"       ],       "id": "1nAJELYEEPvGL",       "state": "scheduled",       "title": "Say hello to the Spaces endpoints"     }   ],   "meta": {     "result_count": 2   } }`