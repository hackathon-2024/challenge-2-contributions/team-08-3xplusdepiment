platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/quick-start/recent-search

Recent search quick start

## Getting started with the recent search endpoint

This quick start guide will help you make your first request to the recent search endpoint with a set of specified fields using Postman.

If you would like to see sample code in different languages, please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository.