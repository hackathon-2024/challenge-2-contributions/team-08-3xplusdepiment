platform: X
topic: Twitter-API-V2
subtopic: Usage
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Usage.md
url: https://developer.twitter.com/en/docs/twitter-api/usage/tweets/api-reference

API reference

## API reference index

### Tweets

|     |     |
| --- | --- |
| Get Tweets Usage | [GET /2/usage/tweets](https://developer.twitter.com/en/docs/twitter-api/usage/tweets/api-reference/get-usage-tweets) |