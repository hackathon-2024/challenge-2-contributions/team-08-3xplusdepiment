platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/follows/migrate/manage-follows-standard-to-twitter-api-v2

Manage follows: Standard v1.1 compared to Twitter API v2

## Manage follows: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [POST friendships/create](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/post-friendships-create) and [POST friendships/destroy](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/post-friendships-destroy) endpoints, the goal of this guide is to help you understand the similarities and differences between the standard and Twitter API v2 manage follows endpoints.

* **Similarities**
    * OAuth 1.0a User Context
* **Differences**
    * Endpoint URLs
    * App and Project requirements
    * HTTP methods
    * Request parameters