platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/overview

### Important resources

The following resources will help you get started and integrate with the Spaces endpoints:

* [Getting access to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/getting-started/getting-access-to-the-twitter-api)
* [Spaces data dictionary](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/spaces)
* [Make your first request to a Spaces endpoint](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/quick-start)