platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/follows/api-reference/get-users-id-followers

GET /2/users/:id/followers

# GET /2/users/:id/followers

Returns a list of users who are followers of the specified user ID.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=usersIdFollowers&params=%28%27query%21%28%29%7Ebody%21%27%27%7Epath%21%28%27*%7E**id%21%272244994945%27%29%01*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2F%7Bid%7D%2Ffollowers&method=get) 

### Endpoint URL

`https://api.twitter.com/2/users/:id/followers`