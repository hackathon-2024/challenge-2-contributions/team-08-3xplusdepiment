platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/migrate/enterprise-to-twitter-api-v2

Enterprise compared to Twitter API v2

## Enterprise compared to Twitter API v2

**Similarities**

* Pagination
* Timezone
* Support for Tweet edit history and metadata. 

**Differences**

* Endpoint URLs
* App and Project requirement
* Available time periods
* Response data format
* HTTP methods
* Request time formats
* Request parameters
* Filtering operators