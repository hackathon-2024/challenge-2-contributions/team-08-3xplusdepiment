platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/pinned-lists/quick-start

Quick start

## Getting started with the pinned List endpoint group

This quick overview will help you make your first request to the pinned List endpoints using [Postman](https://developer.twitter.com/en/docs/tools-and-libraries/using-postman).

Please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository if you want to see sample code in different languages.