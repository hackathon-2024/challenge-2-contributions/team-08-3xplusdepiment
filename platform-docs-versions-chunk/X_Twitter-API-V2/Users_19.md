platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/migrate

## Other migration resources

[User lookup: Standard v1.1 to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/users/lookup/migrate/standard-to-twitter-api-v2 "User lookup: Standard v1.1 to Twitter API v2")

[Twitter API migration hub](https://developer.twitter.com/en/docs/twitter-api/migrate "Twitter API migration hub")

[Check out some sample code for these endpoints](https://github.com/twitterdev/Twitter-API-v2-sample-code "Check out some sample code for these endpoints")