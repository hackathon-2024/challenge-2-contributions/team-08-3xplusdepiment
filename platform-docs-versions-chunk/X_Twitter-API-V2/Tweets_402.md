platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/migrate/likes-lookup-standard-to-twitter-api-v21

Likes lookup: Standard v1.1 compared to Twitter API v2

## Likes lookup: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [GET favorites/list](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/get-favorites-list) endpoint, the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 Likes lookup endpoints.

With v2, we’ve also introduced a new liked users endpoint which allows you to get information about a Tweet’s liking users.

* **Similarities**
    * Authentiation
    * Rate limits
* **Differences**
    * Endpoint URLs  
        
    * Request limitations
    * App and Project requirements  
        
    * Response data format
    * Request parameters