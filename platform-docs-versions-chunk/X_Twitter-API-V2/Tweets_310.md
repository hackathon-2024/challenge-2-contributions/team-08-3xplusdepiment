platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/api-reference/get-tweets-sample-stream

### Example responses

* [Default](#tab0)
* [Optional](#tab1)

Default

Optional

      `{   "data": {     "id": "1067094924124872705",     "edit_history_tweet_ids": [       "1067094924124872705"     ],     "text": "Just getting started with Twitter APIs? Find out what you need in order to build an app. Watch this video! https://t.co/Hg8nkfoizN"   } }`
    

      `{   "data": {     "attachments": {       "media_keys": [         "13_1064638969197977600"       ]     },     "id": "1067094924124872705",     "edit_history_tweet_ids": [       "1067094924124872705"     ],     "text": "Just getting started with Twitter APIs? Find out what you need in order to build an app. Watch this video! https://t.co/Hg8nkfoizN"   },   "includes": {     "media": [       {         "duration_ms": 136637,         "media_key": "13_1064638969197977600",         "type": "video"       }     ]   } }`