platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-members/api-reference/delete-lists-id-members-user_id

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "is_member": false   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `is_member` | boolean | Indicates whether the member specified in the request has been removed from the List. |