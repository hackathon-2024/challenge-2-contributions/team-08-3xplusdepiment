platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/quick-start

## Next steps

[API Reference](https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference "API Reference")

[Get support](https://developer.twitter.com/en/support/twitter-api "Get support")

[Sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code "Sample code")