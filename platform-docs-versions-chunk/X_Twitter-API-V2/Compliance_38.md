platform: X
topic: Twitter-API-V2
subtopic: Compliance
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Compliance.md
url: https://developer.twitter.com/en/docs/twitter-api/compliance/streams/integrate/honoring-user-intent

Honoring user intent on Twitter

We believe that respecting the privacy and intent of Twitter users is critically important to the long term health of one of the largest public, real-time information platforms in the world. Twitter puts privacy controls in the hands of its users, giving individuals the ability to control their own Twitter experience. As business consumers of Twitter data, we have a collective responsibility to honor the privacy and actions of end users in order to maintain this environment of trust and respect.  

There are a variety of things that can happen to Tweets and User accounts that impact how they are displayed on the platform. The actions that affect privacy and intent are defined at both the Tweet and User levels. These actions include: