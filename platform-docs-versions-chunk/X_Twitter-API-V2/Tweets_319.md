platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/introduction

### Retweets lookup

With the Retweets lookup endpoint, you can retrieve a list of accounts that have Retweeted a Tweet. For this endpoint, pagination tokens will be provided for paging through large sets of results in batches of up to 100 users. 

You can authenticate these endpoints with either [OAuth 1.0a User Context](https://developer.twitter.com/en/docs/authentication/oauth-1-0a), [OAuth 2.0 App-Only](https://developer.twitter.com/en/docs/authentication/oauth-2-0/application-only), or [OAuth 2.0 Authorization Code with PKCE](https://aem-staging.twitter.biz/content/developer-twitter/en/docs/authentication/oauth-2-0/authorization-code.html).