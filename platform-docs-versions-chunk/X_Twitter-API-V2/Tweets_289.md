platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/quick-start/sampled-stream

## Next steps

[Customize your request using the API Reference](https://developer.twitter.com/en/docs/twitter-api/tweets/sampled-stream/api-reference "Customize your request using the API Reference")

[Learn how to stream Tweets in real-time](https://developer.twitter.com/en/docs/tutorials/track-tweets-in-real-time "Learn how to stream Tweets in real-time")

[Learn how to listen for important events](https://developer.twitter.com/en/docs/tutorials/listen-for-important-events "Learn how to listen for important events")

[Check out some sample code for these endpoints](https://github.com/twitterdev/Twitter-API-v2-sample-code "Check out some sample code for these endpoints")