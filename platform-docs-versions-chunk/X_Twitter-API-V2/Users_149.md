platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/mutes/migrate/mutes-lookup--standard-v1-1-compared-to-twitter-api-v2

## Next steps

[Review the mutes lookup API references](https://developer.twitter.com/en/docs/twitter-api/users/mutes/api-reference "Review the mutes lookup API references")