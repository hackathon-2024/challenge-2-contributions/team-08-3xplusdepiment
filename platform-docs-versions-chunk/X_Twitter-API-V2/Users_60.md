platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/follows/introduction

Introduction

## Introduction

Following users is one of the most foundational actions on Twitter. 

We offer two sets of endpoint groups to help you lookup, create, and delete follow relationships: follows lookup and manage follows.