platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/blocks/migrate/blocks-lookup-standard-to-twitter-api-v2

Blocks lookup: Standard v1.1 compared to Twitter API v2

## Blocks lookup: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [GET blocks/ids](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/get-blocks-ids) and [GET blocks/list](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/get-blocks-list) endpoints, the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 blocks lookup endpoints.

* **Similarities**
    * Authentication
* **Differences**
    * Endpoint URLs  
        
    * Users per request limits
    * App and Project requirements
    * Response data formats
    * Request parameters