platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/api-reference/get-users-by-username-username

GET /2/users/by/username/:username

# GET /2/users/by/username/:username

Returns a variety of information about one or more users specified by their usernames.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findUserByUsername&params=%28%27query%21%28%29%7Ebody%21%27%27%7Epath%21%28%27*%7E**username%21%27TwitterDev%27%29%01*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2Fby%2Fusername%2F%7Busername%7D&method=get) 

### Endpoint URL

`https://api.twitter.com/2/users/by/username/:username`