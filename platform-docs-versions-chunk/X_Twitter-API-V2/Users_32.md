platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/api-reference/get-users

GET /2/users

# GET /2/users

Returns a variety of information about one or more users specified by the requested IDs.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findUsersById&params=%28%27query%21%28%27*%7Ebody%21%28%29%7Epath%21%28%29%7E**ids%21%272244994945%2C6253282%27%29%01*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers&method=get) 

### Endpoint URL

`https://api.twitter.com/2/users`