platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/tweet-caps

Tweet caps

Twitter API v2 has a Tweet consumption cap that limits the number of Tweets that you can receive from a set of endpoints on a monthly basis. Tweet caps apply at the [Project](https://developer.twitter.com/en/docs/projects)\-level, meaning that any requests to the endpoints listed below using the keys and tokens from [developer Apps](https://developer.twitter.com/en/docs/apps) within that Project will count towards the Project Tweet cap.