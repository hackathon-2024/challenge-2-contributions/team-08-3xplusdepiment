platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/quick-start

## Next steps

[Customize your request using the API Reference](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference "Customize your request using the API Reference")

[Learn how to measure Tweet performance](https://developer.twitter.com/en/docs/tutorials/measure-tweet-performance "Learn how to measure Tweet performance")

[Get support](https://developer.twitter.com/en/support/twitter-api "Get support")

[Check out some sample code for these endpoints](https://github.com/twitterdev/Twitter-API-v2-sample-code "Check out some sample code for these endpoints")