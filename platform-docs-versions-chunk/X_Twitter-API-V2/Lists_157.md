platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/pinned-lists/quick-start

## Next steps

Choose any of the following endpoints for a more in-depth guide once, you have completed the prerequisites:

[Pinned List lookup](https://developer.twitter.com/en/docs/twitter-api/lists/pinned-lists/quick-start/pinned-list-lookup "Pinned List lookup")

[Manage Pinned Lists](https://developer.twitter.com/en/docs/twitter-api/lists/pinned-lists/quick-start/manage-pinned-lists "Manage Pinned Lists")