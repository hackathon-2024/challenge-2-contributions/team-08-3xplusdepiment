platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/overview

## What's currently available

|     |     |
| --- | --- |
| **Spaces lookup** | [Lookup by a single Spaces ID](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-id)<br><br>[Lookup using multiple Spaces IDs](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces)<br><br>[Lookup by their creator ID](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-by-creator-ids)<br><br>[Lookup list of user who purchased a ticket](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-id-buyers) |
| **Search Spaces** | [Search for spaces using a keyword](https://developer.twitter.com/en/docs/twitter-api/spaces/search/api-reference/get-spaces-search) |