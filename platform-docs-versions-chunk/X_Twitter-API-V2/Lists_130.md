platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-members/api-reference/post-lists-id-members

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "is_member": true   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `is_member` | boolean | Indicates whether the member specified in the request has been added to the List. |