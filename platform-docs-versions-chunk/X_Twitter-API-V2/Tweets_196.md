platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/migrate

Migrate

## Comparing Twitter API’s Tweet counts endpoints

The v2 Tweet counts endpoint will eventually replace the [enterprise Search API’s counts endpoint](https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api/api-reference/enterprise-search). If you have code, apps, or tools that use an older version of a Tweet counts endpoint and are considering migrating to the newer Twitter API v2 endpoints, then this guide is for you.

This page contains two comparison tables:

* [Recent Tweet counts](#recent)
* [Full-archive Tweet counts](#full-archive)
* [Filtering operator comparison](#operator)