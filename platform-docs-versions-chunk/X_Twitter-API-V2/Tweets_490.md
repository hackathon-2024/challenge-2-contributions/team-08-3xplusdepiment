platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/apps

## Perspective API template app

Perspective is an artificial intelligence trained by Jigsaw (a unit within Alphabet) to detect toxic comments. This app detects a person's incoming replies, and automatically hides a reply based on the “score” that indicates how confident Perspective is that a comment is similar to toxic comments it’s seen in the past. This is an open source template app developed by Twitter. We encourage you to personalize it to build tools for your users.

[Use app ▸](https://quintessential-yoke.glitch.me/ "Use app ▸")

[Jigsaw on Twitter](https://twitter.com/jigsaw "Jigsaw on Twitter")