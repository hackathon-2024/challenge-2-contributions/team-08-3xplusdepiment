platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/mutes/migrate/mutes-lookup--standard-v1-1-compared-to-twitter-api-v2

Mutes lookup: Standard v1.1 compared to Twitter API v2

## Mutes lookup: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [GET mutes/users/ids](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/get-mutes-users-ids) and [GET mutes/users/list](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/v1/accounts-and-users/mute-block-report-users/api-reference/get-mutes-users-list) endpoints, the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 mutes lookup endpoints.

* **Similarities**
    * Authentication
* **Differences**
    * Endpoint URLs  
        
    * Users per request limits
    * App and Project requirements
    * Response data formats
    * Request parameters