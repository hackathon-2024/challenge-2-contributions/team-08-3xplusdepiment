platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/annotations/overview

## Tweet annotation types

### Entities

Entity annotations are programmatically defined entities that are nested within the entities field and are reflected as annotations in the payload. Each annotation has a confidence score and an indication of where in the Tweet text the entities were identified (start and end fields).

The entity annotations can have the following types:

**Person** - Barack Obama, Daniel, or George W. Bush

**Place** - Detroit, Cali, or "San Francisco, California"

**Product** - Mountain Dew, Mozilla Firefox

**Organization** - Chicago White Sox, IBM

**Other** - Diabetes, Super Bowl 50