platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/migrate/manage-likes-standard-to-twitter-api-v2

Manage Likes: Standard v1.1 compared to Twitter API v2

## Manage Likes: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [POST favorites/create](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-favorites-create) and [POST favorites/destroy](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-favorites-destroy) endpoints, the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 manage Likes endpoints.

* **Similarities**
    * OAuth 1.0a User Context
* **Differences**
    * Endpoint URLs and HTTP methods
    * Request limitations
    * App and Project requirements
    * Request parameters