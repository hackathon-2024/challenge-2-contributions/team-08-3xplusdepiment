platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/blocks/introduction

Introduction

## Introduction

Using blocks lookup, you can see who you or an authenticated user has blocked. This can be useful for determining how you can interact with a given account.