platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/quick-start

Quick start

## Getting started with the manage Lists endpoint group

This quick overview will help you make your first request to the manage List endpoints using [Postman](https://developer.twitter.com/en/docs/tools-and-libraries/using-postman).

If you would like to see sample code in different languages, please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository. 

**Note:** For this example, we will make a request to the _Create a List_ endpoint, but you can apply the learnings from this quick start to other manage requests as well.