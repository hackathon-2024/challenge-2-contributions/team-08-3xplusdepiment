platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/quick-start/authenticated-lookup

Authenticated user lookup

## Getting started with the Authenticated User Lookup endpoint

This quick start guide will help you make your first request to the authenticated user lookup endpoint using Postman.

Please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository if you want to see sample code in different languages.