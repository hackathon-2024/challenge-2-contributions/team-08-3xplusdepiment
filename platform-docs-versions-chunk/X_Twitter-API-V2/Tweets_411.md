platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/api-reference/delete-users-user_id-likes

DELETE /2/users/:id/likes/:tweet\_id

# DELETE /2/users/:id/likes/:tweet\_id

Allows a user or authenticated user ID to unlike a Tweet.  
  
The request succeeds with no action when the user sends a request to a user they're not liking the Tweet or have already unliked the Tweet.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

  
  

### Endpoint URL

`https://api.twitter.com/2/users/:id/likes/:tweet_id`