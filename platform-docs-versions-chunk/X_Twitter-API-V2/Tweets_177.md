platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/introduction

## Supporting resources

[Learn how to use Postman to make requests](https://developer.twitter.com/en/docs/tutorials/postman-getting-started "Learn how to use Postman to make requests")

[Troubleshoot an error](https://developer.twitter.com/en/support/twitter-api "Troubleshoot an error")

[Visit the API reference page](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/api-reference "Visit the API reference page ")

## Tutorials

[Getting started with converting JSON objects to CSV](https://developer.twitter.com/en/docs/tutorials/five-ways-to-convert-a-json-object-to-csv "Getting started with converting JSON objects to CSV")

[Learning path: How to build powerful queries](https://developer.twitter.com/en/docs/tutorials/building-powerful-enterprise-filters "Learning path: How to build powerful queries")