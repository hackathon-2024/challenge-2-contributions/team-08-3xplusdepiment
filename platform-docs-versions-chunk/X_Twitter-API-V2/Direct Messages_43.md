platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference/get-dm_conversations-dm_conversation_id-dm_events

### Example responses

* [Default fields](#tab0)
* [Optional fields](#tab1)

Default fields

Optional fields

      `{   "data": [     {       "event_type": "MessageCreate",       "id": "1346889436626259968",       "text": "Hello just you..."     }   ] }`
    

      `{   "data": [     {       "id": "1585321444547837956",       "text": "Another photo https://t.co/J5KotyeIyd",       "event_type": "MessageCreate",       "dm_conversation_id": "1585094756761149440",       "created_at": "2022-10-26T17:24:21.000Z",       "sender_id": "906948460078698496"     }   ] }`