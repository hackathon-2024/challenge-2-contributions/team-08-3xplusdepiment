platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/introduction

## Tutorials

[Stream Tweets in real-time](https://developer.twitter.com/en/docs/tutorials/track-tweets-in-real-time "Stream Tweets in real-time")

[Build a trends dashboard with Twitter API Toolkit](https://developer.twitter.com/en/docs/tutorials/developer-guide--twitter-api-toolkit-for-google-cloud1 "Build a trends dashboard with Twitter API Toolkit")

[Listen for important events](https://developer.twitter.com/en/docs/tutorials/listen-for-important-events "Listen for important events")

[Building an app to stream Tweets in real-time](https://developer.twitter.com/en/docs/tutorials/building-an-app-to-track-tweets "Building an app to stream Tweets in real-time")