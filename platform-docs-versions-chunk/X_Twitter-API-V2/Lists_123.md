platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-members/migrate/manage-list-members-standard-to-twitter-api-v2

## Next steps

[Review the List members API references](https://developer.twitter.com/en/docs/twitter-api/lists/list-members/api-reference "Review the List members API references")