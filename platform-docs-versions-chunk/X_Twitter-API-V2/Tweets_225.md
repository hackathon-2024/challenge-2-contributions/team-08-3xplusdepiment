platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/quick-start


## Next steps

[Customize your request using the API Reference](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/api-reference "Customize your request using the API Reference")

[Learn how to stream Tweets in real-time](https://developer.twitter.com/en/docs/tutorials/track-tweets-in-real-time "Learn how to stream Tweets in real-time")

[Learn how to listen for important events](https://developer.twitter.com/en/docs/tutorials/listen-for-important-events "Learn how to listen for important events")

[Build a trends dashboard with Twitter API Toolkit for Google Cloud](https://developer.twitter.com/en/docs/tutorials/developer-guide--twitter-api-toolkit-for-google-cloud1 "Build a trends dashboard with Twitter API Toolkit for Google Cloud")

[Learn how to build an app that streams real-time Tweets](https://developer.twitter.com/en/docs/tutorials/building-an-app-to-track-tweets "Learn how to build an app that streams real-time Tweets")

[Ask the community for help](https://twittercommunity.com/ "Ask the community for help")

[Check out some sample code for these endpoints](https://github.com/twitterdev/Twitter-API-v2-sample-code "Check out some sample code for these endpoints")