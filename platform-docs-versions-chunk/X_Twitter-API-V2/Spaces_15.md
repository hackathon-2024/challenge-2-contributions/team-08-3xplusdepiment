platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from the list:

### Spaces lookup

|     |     |
| --- | --- |
| **Lookup Space by ID** | `[GET /2/spaces/:id](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-id)` |
| **Lookup multiple Spaces by ID** | `[GET /2/spaces](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces)` |
| **Get users who purchased a ticket to a Space** | `[GET /2/spaces/:id/buyers](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-id-buyers)` |

###   
  
Discover

|     |     |
| --- | --- |
| **Discover Spaces created by user ID** | `[GET /2/spaces/by/creator_ids](https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-by-creator-ids)` |
|     |     |