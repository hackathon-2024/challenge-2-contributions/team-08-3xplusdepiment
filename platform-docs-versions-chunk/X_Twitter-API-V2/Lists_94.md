platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-tweets/api-reference/get-lists-id-tweets

### Example responses

* [Default fields](#tab0)
* [Optional fields](#tab1)

Default fields

Optional fields

      `{   "data": [     {       "id": "1067094924124872705",       "text": "Just getting started with Twitter APIs? Find out what you need in order to build an app. Watch this video! https://t.co/Hg8nkfoizN"     }   ],   "meta": {     "result_count": 1   } }`
    

      `{   "data": {     "author_id": "2244994945",     "created_at": "2018-11-26T16:37:10.000Z",     "text": "Just getting started with Twitter APIs? Find out what you need in order to build an app. Watch this video! https://t.co/Hg8nkfoizN",     "id": "1067094924124872705"   },   "includes": {     "users": [       {         "verified": true,         "username": "TwitterDev",         "id": "2244994945",         "name": "Twitter Dev"       }     ]   },   "meta": {     "result_count": 1   } }`