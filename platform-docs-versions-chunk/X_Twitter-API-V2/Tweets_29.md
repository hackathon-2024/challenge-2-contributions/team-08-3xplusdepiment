platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets-id

GET /2/tweets/:id

# GET /2/tweets/:id

Returns a variety of information about a single Tweet specified by the requested ID.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findTweetById&params=%28%27query%21%28%27tweet*%27%7Eexpansions%21%27attachments.media_keys%27%7Emedia*type%2Cduration_ms%27%29%7Ebody%21%28%29%7Epath%21%28%21id%21%271460323737035677698%27%29%29*.fields%21%27%01*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets%2F%7Bid%7D&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets/:id`