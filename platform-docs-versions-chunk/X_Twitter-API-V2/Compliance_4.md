platform: X
topic: Twitter-API-V2
subtopic: Compliance
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Compliance.md
url: https://developer.twitter.com/en/docs/twitter-api/compliance/batch-compliance/quick-start

Quick start

## Getting started with the batch compliance endpoints

Working with the batch compliance endpoints generally involves 5 steps:

1. Creating a compliance job
2. Preparing the list of Tweet IDs or user IDs
3. Uploading the list of Tweet IDs or user IDs
4. Checking the status of the compliance job
5. Downloading the results

In this section, we will learn how to go through each of these steps using the command line. If you would like to see sample code in different programming languages, please visit our [Twitter API v2 sample code GitHub repository](https://github.com/twitterdev/Twitter-API-v2-sample-code).