platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/migrate/standard-to-twitter-api-v2

Standard v1.1 compared to Twitter API v2

## Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 GET users/show and GET users/lookup, the goal of this guide is to help you understand the similarities and differences between the standard and Twitter API v2 users lookup endpoints.

* **Similarities**
    * OAuth 1.0a User Context
    * Users per request limits
* **Differences**
    * Endpoint URLs
    * App and Project requirements
    * Response data format
    * Request parameters