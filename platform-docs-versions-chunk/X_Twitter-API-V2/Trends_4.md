platform: X
topic: Twitter-API-V2
subtopic: Trends
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Trends.md
url: https://developer.twitter.com/en/docs/twitter-api/trends/api-reference

API reference

## API reference index

### Trends

|     |     |
| --- | --- |
| Trends lookup | [GET /2/trends/by/woeid](https://developer.twitter.com/en/docs/twitter-api/trends/api-reference/get-trends-by-woeid) |