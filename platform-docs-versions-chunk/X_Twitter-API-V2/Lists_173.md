platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/pinned-lists/api-reference/post-users-id-pinned-lists

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "pinned": true   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `pinned` | boolean | Indicates whether the user pinned the specified List as a result of the request. |