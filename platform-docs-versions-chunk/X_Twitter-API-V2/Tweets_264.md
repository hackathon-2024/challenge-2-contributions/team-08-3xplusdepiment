platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/api-reference/get-tweets-search-stream-rules

### Example responses

* [Response](#tab0)

Response

      `{   "data": [     {       "id": "1165037377523306497",       "value": "dog has:images",       "tag": "dog pictures"     },     {       "id": "1165037377523306498",       "value": "cat has:images -grumpy"     }   ],   "meta": {     "sent": "2019-08-29T01:12:10.729Z"   } }`