platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/search/quick-start

Quick start

## Getting started with the search Spaces endpoint

This quick start guide will help you make your first request to the search Spaces endpoint with a set of specified fields using Postman.

If you would like to see sample code in different programming languages, please visit our [Twitter API v2 sample code GitHub repository](https://github.com/twitterdev/Twitter-API-v2-sample-code).