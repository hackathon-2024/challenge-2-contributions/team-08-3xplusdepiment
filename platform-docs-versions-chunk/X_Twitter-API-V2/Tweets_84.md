platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/migrate


## Comparing Twitter API's timelines endpoints

The v2 reverse chronological timeline, user Tweets timeline, and user mention timeline endpoints replace the [v1.1 statuses/home\_timeine,](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-home_timeline) [v1.1 statuses/user\_timeline](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-user_timeline.html), and [v1.1 statuses/mentions\_timeline](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-mentions_timeline.html) endpoints respectively. If you have code, apps, or tools that use an older version of this endpoint and are considering migrating to the newer Twitter API v2 endpoint, then this guide is for you.  For a more in-depth migration guide see [Standard v1.1 migration to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/migrate/standard-to-twitter-api-v2.html).

This page contains three comparison tables:

* Reverse chronological home timeline
* User Tweet timeline
* User mention timeline