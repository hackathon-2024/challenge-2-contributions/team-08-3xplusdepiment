platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/quick-start/bookmarks-lookup

Bookmarks lookup quick start

## Getting started with the Bookmarks lookup endpoint

This quick start guide will help you make your first request to the Bookmarks lookup endpoint using [Postman](https://developer.twitter.com/en/docs/tools-and-libraries/using-postman).  

Please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository if you want to see sample code in different languages.