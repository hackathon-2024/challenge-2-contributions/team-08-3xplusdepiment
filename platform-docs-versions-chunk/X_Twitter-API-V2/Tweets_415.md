platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/api-reference/get-tweets-id-liking_users

GET /2/tweets/:id/liking\_users

# GET /2/tweets/:id/liking\_users

Allows you to get information about a Tweet’s liking users.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=tweetsIdLikingUsers&params=%28%27query%21%28%29%7Ebody%21%27%27%7Epath%21%28%27id%21%271460323737035677698%27%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets%2F%7Bid%7D%2Fliking_users&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets/:id/liking_users`