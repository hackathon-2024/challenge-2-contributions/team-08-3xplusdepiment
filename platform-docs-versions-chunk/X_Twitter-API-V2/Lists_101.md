platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-members/quick-start

## Next steps

Choose any of the following endpoints for a more in-depth guide once, you have completed the prerequisites:

[List members lookup](https://developer.twitter.com/en/docs/twitter-api/lists/list-members/quick-start/list-members-lookup "List members lookup")

[Manage List members](https://developer.twitter.com/en/docs/twitter-api/lists/list-members/quick-start/manage-list-members "Manage List members")