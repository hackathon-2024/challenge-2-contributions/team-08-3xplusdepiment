platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference/delete-lists-id

DELETE /2/lists/:id

# DELETE /2/lists/:id

Enables the authenticated user to delete a List that they own.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=listIdDelete&params=%28%27query%21%28%29%7Ebody%21%27%27%7Epath%21%28%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Flists%2F%7Bid%7D&method=delete) 

### Endpoint URL

`https://api.twitter.com/2/lists/:id`