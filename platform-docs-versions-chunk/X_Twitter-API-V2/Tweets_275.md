platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/api-reference/post-tweets-search-stream-rules

### Query parameters

| Name | Type | Description |
| --- | --- | --- |
| `delete_all`  <br> Optional | boolean | Set to true to delete all existing rules. |
| `dry_run`  <br> Optional | boolean | Set to true to test the syntax of your rule without submitting it. This is useful if you want to check the syntax of a rule before removing one or more of your existing rules. |