platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-members/migrate/list-members-lookup-standard-to-twitter-api-v2

List members lookup: Standard v1.1 compared to Twitter API v2

## List members lookup: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [GET lists/members](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/create-manage-lists/api-reference/get-lists-members) and [GET lists/memberships](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/create-manage-lists/api-reference/get-lists-memberships) endpoints, the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 List member endpoints.

* **Similarities**
    * Authentication methods
* **Differences**
    * Endpoint URLs
    * Rate limits
    * App and Project requirements
    * Data objects per request limits
    * Response data formats
    * Request parameters