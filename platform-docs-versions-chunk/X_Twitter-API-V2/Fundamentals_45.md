platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/fields

## Next step

[Learn how to use fields with expansions](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/using-fields-and-expansions "Learn how to use fields with expansions")

[Review the different data objects available with Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/introduction "Review the different data objects available with Twitter API v2")

[Make your first request with fields and expansions](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/quick-start "Make your first request with fields and expansions")