platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate/manage-retweets-standard-to-twitter-api-v2

Manage Retweets: Standard v1.1 compared to Twitter API v2

## Manage Retweets: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [POST statuses/retweet/:id](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-retweet-id), and [POST statuses/unretweet/:id](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-unretweet-id)  endpoints, the goal of this guide is to help you understand the similarities and differences between the standard and Twitter API v2 Retweets endpoints.

* **Similarities**
    * Authentication
* **Differences**
    * Endpoint URLs and HTTP methods
    * Request limitations
    * App and Project requirements
    * Request parameters