platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/migrate

## Other migration resources

[Tweet lookup: Standard v1.1 to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/migrate/standard-to-twitter-api-v2 "Tweet lookup: Standard v1.1 to Twitter API v2")

[Twitter API migration hub](https://developer.twitter.com/en/docs/twitter-api/migrate "Twitter API migration hub")

[Sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code "Sample code")