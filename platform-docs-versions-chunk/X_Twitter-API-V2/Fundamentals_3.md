platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/data-dictionary/introduction

### Key fields

Other useful fields that you should expect in the Twitter API v2 data format:

* [Metrics](https://developer.twitter.com/en/docs/twitter-api/metrics.html) available in the Tweet, user, Spaces, lists objects
* [Annotation](https://developer.twitter.com/en/docs/twitter-api/annotations.html) topics available in the Tweet payload  
    
* A new [conversation\_id](https://developer.twitter.com/en/docs/twitter-api/conversation-id) field to help you track Tweets included in a conversation
* A new reply\_settings field to help you understand who has the ability to reply to specific Tweets