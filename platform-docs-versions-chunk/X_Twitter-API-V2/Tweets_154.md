platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/migrate/standard-to-twitter-api-v2

## Next steps

[Check out our quick start guide for Twitter API v2 recent search](https://developer.twitter.com/en/docs/twitter-api/tweets/search/quick-start "Check out our quick start guide for Twitter API v2 recent search")

[Review the API reference for recent search](https://developer.twitter.com/en/docs/twitter-api/tweets/search/api-reference "Review the API reference for recent search")

[Check out some sample code for these endpoints](https://github.com/twitterdev/Twitter-API-v2-sample-code "Check out some sample code for these endpoints")

[Step-by-step guide to making your first request to recent search](https://developer.twitter.com/en/docs/tutorials/step-by-step-guide-to-making-your-first-request-to-the-twitter-api-v2 "Step-by-step guide to making your first request to recent search")