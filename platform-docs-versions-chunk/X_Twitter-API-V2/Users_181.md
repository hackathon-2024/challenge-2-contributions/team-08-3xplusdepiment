platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/search/quick-start

### Prerequisites

For you to be able to complete this guide, you will have need to have a set of [keys and tokens](https://developer.twitter.com/en/docs/authentication), which you can generate by following these steps:

1. [Sign up for a developer account](https://developer.twitter.com/en/portal/petition/essential/basic-info).
2. Create a [Project](https://developer.twitter.com/en/docs/projects) and an associated [developer App](https://developer.twitter.com/en/docs/apps) in the developer portal.  
    
3. Navigate to your app's “Keys and tokens” page, and save your credentials in a secure location.