platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/follows/quick-start/manage-follows

Manage follows quick start

## Getting started with the manage follows endpoints

This quick start guide will help you make your first request to the manage follows endpoints using [Postman](https://developer.twitter.com/en/docs/tutorials/postman-getting-started).

If you would like to see sample code in different languages, please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository.