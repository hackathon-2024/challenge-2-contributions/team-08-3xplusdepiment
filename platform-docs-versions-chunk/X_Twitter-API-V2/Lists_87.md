platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-tweets/migrate/list-tweets-lookup-standard-to-twitter-api-v2

## Next steps

[Review the List lookup API references](https://developer.twitter.com/en/docs/twitter-api/lists/list-tweets/api-reference "Review the List lookup API references")