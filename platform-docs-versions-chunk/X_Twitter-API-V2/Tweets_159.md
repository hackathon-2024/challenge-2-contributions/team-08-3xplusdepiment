platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/api-reference

API reference

## API reference index

For a complete API reference, please select an endpoint from below.

### Recent search

|     |     |
| --- | --- |
| **Search for Tweets published in the last 7 days** | `[GET /2/tweets/search/recent](https://developer.twitter.com/en/docs/twitter-api/tweets/search/api-reference/get-tweets-search-recent)` |

### Full-archive search

Only available to those with [Pro and Enterprise access](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api#v2-access-level)

|     |     |
| --- | --- |
| **Search the full archive of Tweets** | `[GET /2/tweets/search/all](https://developer.twitter.com/en/docs/twitter-api/tweets/search/api-reference/get-tweets-search-all)` |