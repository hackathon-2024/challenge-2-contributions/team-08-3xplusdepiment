platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/api-reference/delete-users-id-bookmarks-tweet_id

DELETE /2/users/:id/bookmarks/:tweet\_id

# DELETE /2/users/:id/bookmarks/:tweet\_id

Allows a user or authenticated user ID to remove a Bookmark of a Tweet.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=usersIdBookmarksDelete&params=%28%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2F%7Bid%7D%2Fbookmarks%2F%7Btweet_id%7D&method=delete) 

### Endpoint URL

`https://api.twitter.com/2/users/:id/bookmarks/:tweet_id`