platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/api-reference/get-tweets-counts-all

GET /2/tweets/counts/all

# GET /2/tweets/counts/all

This endpoint is only available to those users who have been approved for [Academic Research access](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api#v2-access-level).  
  
The full-archive Tweet counts endpoint returns the count of Tweets that match your query from the complete history of public Tweets; since the first Tweet was created March 26, 2006.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets%2Fcounts%2Fall&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets/counts/all`