platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/integrate

Integrate

## Integration guide

This page contains information on several tools and critical concepts that you should know as you integrate the manage Bookmarks endpoints into your system. We’ve broken the page into a couple of different sections:

* [Helpful tools](https://developer.twitter.com/en/docs/twitter-api/users/blocks/integrate#helpful)
* Key Concepts
    * [Authentication](https://developer.twitter.com/en/docs/twitter-api/users/blocks/integrate#authentication)
    * [Developer portal, Projects, and Apps](https://developer.twitter.com/en/docs/twitter-api/users/blocks/integrate#portal)
    * [Rate limits](https://developer.twitter.com/en/docs/twitter-api/users/blocks/integrate#limits)