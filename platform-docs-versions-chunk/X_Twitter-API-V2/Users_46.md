platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/api-reference/get-users-by

GET /2/users/by

# GET /2/users/by

Returns a variety of information about one or more users specified by their usernames.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findUsersByUsername&params=%28%27query%21%28%27*%7Ebody%21%28%29%7Epath%21%28%29%7E**usernames%21%27-Dev%2C-%27%29-Twitter%01-*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2Fby&method=get) 

### Endpoint URL

`https://api.twitter.com/2/users/by`