platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/integrate/build-a-rule

### Table of contents

* [Building a rule](#build)
    * [Rule limitations](#limits)
    * [Operator availability](#availability)
    * [Operator types: standalone and conjunction-required](#types)
    * [Boolean operators and grouping](#boolean)
    * [Order of operations](#order-of-operations)
    * [Punctuation, diacritics, and case sensitivity](#punctuation)
    * [Specificity and efficiency](#specificity)
    * [Iteratively building a rule](#iterative)
    * [Adding and removing rules](#adding-removing)
    * [Rule examples](#examples)
* [List of operators](#list)