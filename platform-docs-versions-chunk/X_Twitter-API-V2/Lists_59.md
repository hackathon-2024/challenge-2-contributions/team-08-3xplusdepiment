platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference/post-lists

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "id": "1441162269824405510",     "name": "test v2 create list"   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `id` | number | The id of the newly created List. |
| `name` | string | The name of the newly created List. |