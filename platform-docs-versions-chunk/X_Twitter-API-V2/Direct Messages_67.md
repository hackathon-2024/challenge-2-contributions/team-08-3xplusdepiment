platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from the list:  
  

### Manage Direct Messages

|     |     |     |
| --- | --- | --- |
| **Create a message in a 1-1 conversation with the participant** | [POST /2/dm\_conversations/with/:participant\_id/messages](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/api-reference/post-dm_conversations-with-participant_id-messages) |     |
| **Create a group conversation and add a DM to it** | [POST /2/dm\_conversations/](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/api-reference/post-dm_conversations) |     |
| **Adding a DM to an existing conversation (for both group and 1-1)** | [POST /2/dm\_conversations/:dm\_conversation\_id/messages](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/api-reference/post-dm_conversations-dm_conversation_id-messages) |     |