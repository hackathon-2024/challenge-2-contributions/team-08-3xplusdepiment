platform: X
topic: Twitter-API-V2
subtopic: Compliance
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Compliance.md
url: https://developer.twitter.com/en/docs/twitter-api/compliance/streams/quick-start

Quick start

## Getting started with the compliance stream endpoints

This quick start guide will help you make your first request to the compliance stream endpoints using a cURL request. cURL is a command line tool which allows you to make requests with minimal configuration.

If you would like to see sample code in different programming languages, please visit our [Twitter API v2 sample code GitHub repository](https://github.com/twitterdev/Twitter-API-v2-sample-code).