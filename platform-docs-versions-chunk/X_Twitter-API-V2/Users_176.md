platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/search/api-reference/get-users-search

GET /2/users/search

# GET /2/users/search

The users endpoint returns Users that match a search query. 

[Run in Postman ❯](https://t.co/twitter-api-postman) 

### Endpoint URL

`https://api.twitter.com/2/users/search`  
  

### Authentication and rate limits

|     |     |
| --- | --- |
| Authentication methods  <br>supported by this endpoint | [User Auth](https://developer.twitter.com/en/docs/authentication/oauth-1-0a) |
| [Rate limit](https://developer.twitter.com/en/docs/rate-limits) | User rate limit (User context): 900 requests per 15-minute window per each authenticated user |