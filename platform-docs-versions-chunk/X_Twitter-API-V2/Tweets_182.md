platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/quick-start/full-archive-tweet-counts

Full-archive Tweet counts quick start

## Getting started with the full-archive Tweet counts     endpoint

This quick start guide will help you make your first request to the full-archive Tweet counts endpoint using Postman, a graphical tool that alows you to make HTTP requests.

If you would like to see sample code in different programming languages, please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository.