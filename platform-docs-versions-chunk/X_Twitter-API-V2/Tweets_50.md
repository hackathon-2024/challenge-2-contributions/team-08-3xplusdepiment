platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/migrate/manage-tweets-standard-to-twitter-api-v2

Manage Tweets standard to Twitter API v2

## Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [POST statuses/update](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-update) and [POST statuses/destroy/:id](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-destroy-id) endpoints, the goal of this guide is to help you understand the similarities and differences between the standard and Twitter API v2 manage Tweets endpoints.  

* **Similarities**
    * Authentiation
* **Differences**
    * Endpoint URLs
    * App and Project requirements  
        
    * Request parameters