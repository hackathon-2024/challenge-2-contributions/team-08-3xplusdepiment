platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/integrate/build-a-query

Building queries

## Building queries for Tweet counts

The Tweet counts endpoints accept a single query with a GET request and return a set of historical Tweet counts that match the query.  Queries are made up of operators that are used to match on a variety of Tweet attributes. 

To learn more about how to create high-quality queries, visit the following tutorial:  
[Building high-quality filters for getting Twitter data](https://developer.twitter.com/en/docs/tutorials/building-high-quality-filters)