platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/api-reference/post-users-id-likes

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "liked": true   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `liked` | boolean | Indicates whether the user likes the specified Tweet as a result of this request. |