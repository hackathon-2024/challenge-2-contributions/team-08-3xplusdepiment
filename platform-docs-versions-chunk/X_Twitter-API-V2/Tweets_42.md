platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/quick-start

## Next steps

[API Reference](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/api-reference "API Reference")

[Get support](https://developer.twitter.com/en/support/twitter-api "Get support")

[Sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code "Sample code")