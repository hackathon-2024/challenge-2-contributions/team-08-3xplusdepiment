platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/integrate/handling-disconnections

### Recovering lost data

If you do experience a disconnect, there are some different strategies that you can use to ensure that you receive all of the data that you might have missed. We've documented some key steps that you can take to recover missed data on our integration guide on [recovering data](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/integrate/content/developer-twitter/en/docs/twitter-api/tweets/filtered-stream/integrate/recovering-from-a-disconnection).