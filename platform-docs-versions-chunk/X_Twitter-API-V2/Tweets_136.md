platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-query

### Table of contents

* [Building a query](#build)
* [Query limitations](#limits)
* [Operator availability](#availability)
* [Operator types: standalone and conjunction-required](#types)
* [Boolean operators and grouping](#boolean)
* [Order of operations](#order-of-operations)
* [Punctuation, diacritics, and case sensitivity](#punctuation)
* [Specificity and efficiency](#specificity)
* [Quote Tweet matching behavior](#quote-tweets)
* [Iteratively building a query](#iterative)
* [Adding a query to your request](#adding-a-query)
* [Query examples](#examples)
* [List of operators](#list)