platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/introduction

## Supporting resources

[Learn how to use Postman](https://developer.twitter.com/en/docs/tutorials/postman-getting-started "Learn how to use Postman")

[Troubleshoot an error](https://developer.twitter.com/en/support/twitter-api "Troubleshoot an error")

[API Reference](https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/api-reference "API Reference")