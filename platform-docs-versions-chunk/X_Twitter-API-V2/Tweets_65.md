platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/api-reference/delete-tweets-id

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "deleted": true   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `deleted` | boolean | Indicates whether the specified Tweet has been deleted as a result of this request. The returned value is `true` for a successful delete request. |