platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-lookup/api-reference/get-lists-id

### Example responses

* [Default fields](#tab0)
* [Optional fields](#tab1)

Default fields

Optional fields

      `{   "data": {     "id": "84839422",     "name": "Official Twitter Accounts"   } }`
    

      `{   "data": {     "follower_count": 906,     "id": "84839422",     "name": "Official Twitter Accounts",     "owner_id": "783214"   },   "includes": {     "users": [       {         "id": "783214",         "name": "Twitter",         "username": "Twitter"       }     ]   } }`