platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/api-reference/get-users-me

### Example responses

* [Default fields](#tab0)
* [Optional fields](#tab1)

Default fields

Optional fields

      `{   "data": {     "id": "2244994945",     "name": "TwitterDev",     "username": "Twitter Dev"   } }`
    

      `{   "data": {     "created_at": "2013-12-14T04:35:55.000Z",     "username": "TwitterDev",     "pinned_tweet_id": "1255542774432063488",     "id": "2244994945",     "name": "Twitter Dev"   } }`