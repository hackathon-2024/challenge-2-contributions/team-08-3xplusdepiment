platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/api-reference/post-tweets

POST /2/tweets

# POST /2/tweets

Creates a Tweet on behalf of an authenticated user.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=createTweet&params=%28%27query%21%28%29%7Ebody%21%27%28*text%5C%21*just+setting+up+my+%23TwitterAPI*%29%27%7Epath%21%28%29%29*%5C%27%01*_&body=%27%28*text%5C%21*just+setting+up+my+%23TwitterAPI*%29%27*%5C%27%01*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets&method=post) 

### Endpoint URL

`https://api.twitter.com/2/tweets`