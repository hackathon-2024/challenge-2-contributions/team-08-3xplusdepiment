platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/api-reference/post-dm_conversations-with-participant_id-messages

POST /2/dm\_conversations/with/:participant\_id/messages

# POST /2/dm\_conversations/with/:participant\_id/messages

Creates a one-to-one Direct Message and adds it to the one-to-one conversation. This method either creates a new one-to-one conversation or retrieves the current conversation and adds the Direct Message to it.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=dmConversationWithUserEventIdCreate) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fdm_conversations%2Fwith%2F%7Bparticipant_id%7D%2Fmessages&method=post) 

### Endpoint URL

`https://api.twitter.com/2/dm_conversations/with/:participant_id/messages`