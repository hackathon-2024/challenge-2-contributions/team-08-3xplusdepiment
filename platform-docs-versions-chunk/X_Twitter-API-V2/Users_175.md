platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/search/api-reference

API reference

## API reference index

|     |     |
| --- | --- |
| **Search Users** | `[GET /2/users/search](https://developer.twitter.com/en/docs/twitter-api/users/search/api-reference/get-users-search)` |