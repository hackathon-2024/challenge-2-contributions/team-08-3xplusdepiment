platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/integrate

## Next steps

[Visit the API reference page for these endpoint](https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference "Visit the API reference page for these endpoint")