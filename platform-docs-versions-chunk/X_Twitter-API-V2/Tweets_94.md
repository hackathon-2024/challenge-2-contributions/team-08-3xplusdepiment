platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from below.  
  

### Reverse chronological timeline

|     |     |
| --- | --- |
| **Returns a collection of recent Tweets by you and users you follow** | `[GET /2/users/:id/timelines/reverse_chronological](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/api-reference/get-users-id-reverse-chronological)` |

### User Tweet timeline

|     |     |
| --- | --- |
| **Returns most recent Tweets composed a specified user ID** | `[GET /2/users/:id/tweets](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/timelines/api-reference/get-users-id-tweets)` |

### User mention timeline

|     |     |
| --- | --- |
| **Returns most recent Tweets mentioning a specified user ID** | `[GET /2/users/:id/mentions](https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/api-reference/get-users-id-mentions)` |