platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/likes/migrate

## Other migration resources

[Likes lookup: Standard v1.1 to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/tweets/likes/migrate/likes-lookup-standard-to-twitter-api-v2 "Likes lookup: Standard v1.1 to Twitter API v2")

[Manage Likes: Standard v1.1 to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/tweets/likes/migrate/manage-likes-standard-to-twitter-api-v2 "Manage Likes: Standard v1.1 to Twitter API v2")

[Twitter API migration hub](https://developer.twitter.com/en/docs/twitter-api/migrate "Twitter API migration hub")