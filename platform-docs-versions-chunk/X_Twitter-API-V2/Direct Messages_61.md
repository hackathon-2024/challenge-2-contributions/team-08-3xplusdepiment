platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/integrate


## Helpful tools

Here are some helpful tools we encourage you to explore as you work with the Direct Messages lookup endpoints: 

****Postman**  
**

Postman is a great tool that you can use to test out an endpoint. Each Postman request includes every path and body parameter to help you quickly understand what is available to you. To learn more about our Postman collections, please visit our [Using Postman](https://developer.twitter.com/en/docs/tutorials/postman-getting-started) page. 

****Code samples**  
**

Python sample code for the v2 Direct Messages endpoints is available in our Twitter API v2 sample code GitHub repository. The "Manage-Direct-Messages" folder contains examples for the POST methods, and the "Direct-Messages-lookup" folder contains examples for the GET methods.

****TwitterDev Software Development Kits (SDKs)**  
**

These [libraries](https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/sdks/overview) are being updated for the v2 Direct Messages endpoints and should be ready soon:  

* [Twitter API Java SDK](https://github.com/twitterdev/twitter-api-java-sdk) - Official Java SDK for the Twitter API v2
* [Twitter API TypeScript/JavaScript SDK](https://github.com/twitterdev/twitter-api-typescript-sdk) - Official TS/JS SDK for the Twitter API v2

**Third-party libraries**  

There is a growing number of [third-party libraries](https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2#community-libraries) developed by our community. These libraries are designed to help you get started, and several are expected to support v2 Direct Messages endpoints soon. You can find a library that works with the v2 endpoints by looking for the proper version tag.