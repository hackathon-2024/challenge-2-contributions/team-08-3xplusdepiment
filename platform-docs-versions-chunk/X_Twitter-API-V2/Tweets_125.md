platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/quick-start/recent-search

## Next steps

[Customize your request using the API Reference](https://developer.twitter.com/en/docs/twitter-api/tweets/search/api-reference "Customize your request using the API Reference")

[See a full list of query operators](https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-rule "See a full list of query operators")

[Use sample code for these endpoints](https://github.com/twitterdev/Twitter-API-v2-sample-code "Use sample code for these endpoints")