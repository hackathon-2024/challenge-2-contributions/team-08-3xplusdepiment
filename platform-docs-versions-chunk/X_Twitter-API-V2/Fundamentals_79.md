platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/versioning

Versioning

Along with Twitter API v2, we launched a new versioning strategy that enables developers to better understand when to expect changes to Twitter’s public APIs, and when they will need to migrate to new versions. 

Developers will be notified of deprecations, retirements, changes, and additions to the Twitter API via our [communication channels](https://developer.twitter.com/en/updates/stay-informed) so they can appropriately plan to accommodate these changes in their development roadmap. All changes to the API will be noted in the changelog.

The Twitter API currently has three different versions. We strongly encourage users to utilize Twitter API v2 when planning their integration unless we have not released functionality to v2 that is required by your use case. 

To learn more about each version, please visit the following pages:

* [Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api)
* Twitter API v1.1 ([standard](https://developer.twitter.com/en/docs/twitter-api/v1) and [premium](https://developer.twitter.com/en/docs/twitter-api/premium))
* Gnip 2.0 ([enterprise](https://developer.twitter.com/en/docs/twitter-api/enterprise))