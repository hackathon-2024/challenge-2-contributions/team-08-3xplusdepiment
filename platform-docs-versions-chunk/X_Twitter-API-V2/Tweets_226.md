platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/integrate/build-a-rule

How to build a rule

**Please note:**

If you are moving an app between any [Projects](https://developer.twitter.com/en/docs/projects/overview), any rules you have created on the filtered stream endpoint will be reset. You will need to recreate these rules once it is associated with its new Project. Prior to moving an app that has rules in place on the filtered stream endpoint, you should save a local copy of all rules using the [GET /2/tweets/search/stream/rules endpoint](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/api-reference/get-tweets-search-stream-rules).