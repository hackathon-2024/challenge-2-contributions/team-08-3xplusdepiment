platform: X
topic: Twitter-API-V2
subtopic: Compliance
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Compliance.md
url: https://developer.twitter.com/en/docs/twitter-api/compliance/streams/api-reference/get-users-compliance-stream

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "user_protect": {       "user": {         "id": "906948460078698496"       },       "event_at": "2022-07-01T21:44:57.895Z"     }   } }`