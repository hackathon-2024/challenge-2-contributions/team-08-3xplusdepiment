platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from the list:

|     |     |
| --- | --- |
| **Get all messages in a 1-1 conversation** | [GET /2/dm\_conversations/with/:user\_id/dm\_events](https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference/get-dm_conversations-with-participant_id-dm_events) |
| **Get all messages in a specific conversation (both group and 1-1 conversations)** | [GET /2/dm\_conversations/:dm\_conversation\_id/dm\_events](https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference/get-dm_conversations-dm_conversation_id-dm_events) |
| **Get all messages across a user's DM conversations (both sent and received, group and 1-1 conversations)** | [GET /2/dm\_events](https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference/get-dm_events) |