platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate/retweets-lookup-standard-to-twitter-api-v2

## Next steps

[Review the Retweets lookup API references](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/api-reference "Review the Retweets lookup API references")