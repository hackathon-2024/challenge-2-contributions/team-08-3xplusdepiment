platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/expansions

## Next step

[Learn how to use Fields with Expansions](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/using-fields-and-expansions "Learn how to use Fields with Expansions")

[Review the different data objects available with Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/introduction "Review the different data objects available with Twitter API v2")