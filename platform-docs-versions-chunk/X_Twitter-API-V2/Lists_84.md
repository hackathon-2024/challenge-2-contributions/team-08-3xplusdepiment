platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/list-tweets/migrate/list-tweets-lookup-standard-to-twitter-api-v2

List Tweets lookup: Standard v1.1 compared to Twitter API v2

## List Tweets lookup: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [GET lists/statuses](https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/create-manage-lists/api-reference/get-lists-statuses) endpoint, the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 endpoints.

* **Similarities**
    * Authentication methods
    * Rate limits
* **Differences**
    * Endpoint URLs
    * App and Project requirements
    * Data objects per request limits
    * Response data formats
    * Request parameters