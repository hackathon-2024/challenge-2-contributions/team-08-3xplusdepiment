platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate/retweets-lookup-standard-to-twitter-api-v2

Retweets lookup: Standard v1.1 compared to Twitter API v2

## Retweets lookup: Standard v1.1 compared to Twitter API v2

If you have been working with the standard v1.1 [v1.1 GET statuses/retweets/:id](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/get-statuses-retweets-id), [v1.1 GET statuses/retweets/:ids](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/get-statuses-retweeters-ids), the goal of this guide is to help you understand the similarities and differences between the standard v1.1 and Twitter API v2 Retweets lookup endpoints.

* **Similarities**
    * Authentiation
    * Users per request limits
* **Differences**
    * Endpoint URLs  
        
    * Request limitations
    * App and Project requirements  
        
    * Response data format
    * Request parameters