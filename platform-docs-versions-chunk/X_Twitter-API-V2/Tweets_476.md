platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/quick-start

Quick start

## Getting started with the hide replies endpoint

This quick start guide will help you make your first request to the hide replies endpoint using [Postman](https://developer.twitter.com/en/docs/tools-and-libraries/using-postman).

If you would like to see some code snippets in different languages, please visit the [hide replies API Reference page](https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/api-reference/put-tweets-id-hidden#requests).