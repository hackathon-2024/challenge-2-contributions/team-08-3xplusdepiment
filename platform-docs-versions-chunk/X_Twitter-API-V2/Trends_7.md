platform: X
topic: Twitter-API-V2
subtopic: Trends
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Trends.md
url: https://developer.twitter.com/en/docs/twitter-api/trends/api-reference/get-trends-by-woeid

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `trend_name` | string | The name of the Trend. |
| `tweet_count` | integer | The total number of Tweets that are part of this Trend. |