platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/pinned-lists/api-reference/delete-users-id-pinned-lists-list_id

### Example responses

* [Successful response](#tab0)

Successful response

      `{   "data": {     "pinned": false   } }`
    

### Response fields

| Name | Type | Description |
| --- | --- | --- |
| `pinned` | boolean | Indicates whether the user unpinned the specified List as a result of the request. |