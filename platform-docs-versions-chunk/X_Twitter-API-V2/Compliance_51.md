platform: X
topic: Twitter-API-V2
subtopic: Compliance
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Compliance.md
url: https://developer.twitter.com/en/docs/twitter-api/compliance/streams/api-reference/get-tweets-compliance-stream

GET /2/tweets/compliance/stream

# GET /2/tweets/compliance/stream

Connect to one of four partitiona of the Tweet compliance stream.

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2Tweet%2Fcompliance%2Fstream%2F%7Bid%7D&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets/compliance/stream`  
  

### Authentication and rate limits

|     |     |
| --- | --- |
| Authentication methods  <br>supported by this endpoint | [OAuth 2.0 App-only](https://developer.twitter.com/en/docs/authentication/oauth-2-0/application-only "Use this method to obtain information in the context of an unauthenticated public user. This method is for developers that just need read-only access to public information. Click to learn how to obtain an OAuth 2.0 App Access Token.") |
| [Rate limit](https://developer.twitter.com/en/docs/rate-limits) | App rate limit (Application-only): 100 requests per 15-minute window shared among all users of your app |