platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/migrate

Migrate

## Comparing Twitter API’s Search Tweets endpoints

The v2 Search Tweets endpoint will eventually replace the [standard v1.1 search/tweets](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/v1/tweets/search/overview/standard) endpoint and [enterprise Search API](https://developer.twitter.com/en/docs/twitter-api/enterprise/search-api). If you have code, apps, or tools that use an older version of a Twitter search endpoint and are considering migrating to the newer Twitter API v2 endpoints, then this guide is for you. 

This page contains three comparison tables:

* [Recent search](#recent)
* [Full-archive search](#full-archive)
* [Filtering operator comparison](#operators)