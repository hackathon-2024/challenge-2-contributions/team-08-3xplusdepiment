platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/api-reference/post-users-id-retweets

POST /2/users/:id/retweets

# POST /2/users/:id/retweets

Causes the user ID identified in the path parameter to Retweet the target Tweet.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=usersIdRetweets&params=%28%27query%21%28%29%7Ebody%21%27%28*tweet_id%5C%21*1491487846623956993*%29%27%7Epath%21%28%27id%21%27%27%29%29*%5C%27%01*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2F%7Bid%7D%2Fretweets&method=post) 

### Endpoint URL

`https://api.twitter.com/2/users/:id/retweets`