platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/expansions

### Available expansions for Spaces payloads

| Expansion | Description |
| --- | --- |
| `invited_user_ids` | Returns User objects representing what accounts were invited |
| `speaker_ids` | Returns User objects representing what accounts spoke during a Space |
| `creator_id` | Returns a User object representing what account created the Space |
| `host_ids` | Returns User objects representing what accounts were set up as hosts |
| `topics_ids` | Returns topic descriptions that were set up by the creator |

### Available expansion for Lists payloads

| Expansion | Description |
| --- | --- |
| `owner_id` | Returns a User object representing what account created and maintains the List |