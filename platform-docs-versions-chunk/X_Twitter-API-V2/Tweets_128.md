platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/search/quick-start/full-archive-search

### Prerequisites

The full-archive search endpoint is currently available as part of the Pro and Enterprise access only. In order to use this endpoint, you must upgrade to Pro access or  [apply for Enterprise access level](https://docs.google.com/forms/d/e/1FAIpQLScO3bczKWO2jFHyVZJUSEGfdyfFaqt2MvmOfl_aJp0KxMqtDA/viewform).

In addition to being approved for access, you will need to have a set of [keys and tokens](https://developer.twitter.com/en/docs/authentication) to authenticate your request. You can generate these keys and tokens by following these steps:

* Navigate to your [Project](https://developer.twitter.com/en/docs/projects) with Enterprise or Pro access in the developer portal and make sure you have an associated [developer App](https://developer.twitter.com/en/docs/apps) within that Project.  
    
* Navigate to your App's “Keys and tokens” page to generate the required credentials. Make sure to save all credentials in a secure location.