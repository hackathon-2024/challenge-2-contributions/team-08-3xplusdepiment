platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/tweet-caps


### Limited v2 endpoints

Tweets you receive from any of the following endpoints count towards this monthly Tweet cap:

* [Tweets Lookup](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/introduction)
* [Recent search](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/search)
* [Full-archive search](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/search) 
* [Filtered stream](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/filtered-stream)
* [User Tweet timeline](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/timelines)
* [User mention timeline](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/timelines)
* [Likes lookup - Tweets liked by a user](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/tweets/likes)  
    
* [List Tweets lookup](https://developer.twitter.com/content/developer-twitter/en/docs/twitter-api/lists/list-tweets)