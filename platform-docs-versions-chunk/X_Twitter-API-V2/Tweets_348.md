platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/api-reference/get-tweets-id-retweeted_by

GET /2/tweets/:id/retweeted\_by

# GET /2/tweets/:id/retweeted\_by

Allows you to get information about who has Retweeted a Tweet.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=tweetsIdRetweetingUsers&params=%28%27query%21%28%29%7Ebody%21%27%27%7Epath%21%28%27id%21%271491487846623956993%27%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets%2F%7Bid%7D%2Fretweeted_by&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets/:id/retweeted_by`