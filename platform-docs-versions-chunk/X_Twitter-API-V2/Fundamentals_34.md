platform: X
topic: Twitter-API-V2
subtopic: Fundamentals
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Fundamentals.md
url: https://developer.twitter.com/en/docs/twitter-api/data-dictionary/using-fields-and-expansions

## Next steps

[Try building a query string on your own with the tweets lookup endpoint](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets "Try building a query string on your own with the tweets lookup endpoint")

[Review a full list of the Tweet object fields](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet "Review a full list of the Tweet object fields")

[Review a full list of available expansions](https://developer.twitter.com/en/docs/twitter-api/expansions "Review a full list of available expansions")