platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/bookmarks/api-reference/get-users-id-bookmarks

GET /2/users/:id/bookmarks

# GET /2/users/:id/bookmarks

Allows you to get an authenticated user's 800 most recent bookmarked Tweets.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=getUsersIdBookmarks&params=%28%27id%21%27%27%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2F%7Bid%7D%2Fbookmarks&method=get) 

### Endpoint URL

`https://api.twitter.com/2/users/:id/bookmarks`