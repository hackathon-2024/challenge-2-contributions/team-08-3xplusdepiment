platform: X
topic: Twitter-API-V2
subtopic: Spaces
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Spaces.md
url: https://developer.twitter.com/en/docs/twitter-api/spaces/lookup/api-reference/get-spaces-by-creator-ids

GET /2/spaces/by/creator\_ids

# GET /2/spaces/by/creator\_ids

Returns live or scheduled Spaces created by the specified user IDs. Up to 100 comma-separated IDs can be looked up using this endpoint.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findSpacesByCreatorIds&params=%28%27query%21%28%27user_ids%21%271065249714214457345%2C2328002822%2C2548985366%27%7Espace.fields%21%27%27%7Eexpansions%21%27creator_id%27%29%7Ebody%21%28%29%7Epath%21%28%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fspaces%2Fby%2Fcreator_ids&method=get) 

### Endpoint URL

`https://api.twitter.com/2/spaces/by/creator_ids`