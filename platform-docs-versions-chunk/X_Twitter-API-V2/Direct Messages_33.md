platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/lookup/api-reference/get-dm_conversations-with-participant_id-dm_events

### Path parameters

| Name | Type | Description |
| --- | --- | --- |
| `participant_id`  <br> Required | string | The `participant_id` of the user that the authenicating user is having a 1-1 conversation with. |