platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/quick-start

Quick start

## Getting started with the filtered stream endpoints

This quick start guide will help you make your first request to the filtered stream endpoint group using a cURL request. cURL is a command line tool which allows you to make requests with minimal configuration.

If you would like to see sample code in different languages, please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository.