platform: X
topic: Twitter-API-V2
subtopic: Compliance
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Compliance.md
url: https://developer.twitter.com/en/docs/twitter-api/compliance/batch-compliance/api-reference/get-compliance-jobs-id

### Path parameters

| Name | Type | Description |
| --- | --- | --- |
| `id`  <br> Required | number | The unique identifier for the compliance job you want to retrieve. |