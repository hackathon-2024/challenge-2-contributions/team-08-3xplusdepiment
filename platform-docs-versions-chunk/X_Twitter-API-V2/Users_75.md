platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/follows/migrate/manage-follows-standard-to-twitter-api-v2

## Next steps

[Check out our quick start guide for Twitter API v2 manage follows](https://developer.twitter.com/en/docs/twitter-api/users/follows/quick-start/manage-follows "Check out our quick start guide for Twitter API v2 manage follows")

[Review the API reference for the v2 follows endpoints](https://developer.twitter.com/en/docs/twitter-api/users/follows/api-reference "Review the API reference for the v2 follows endpoints")