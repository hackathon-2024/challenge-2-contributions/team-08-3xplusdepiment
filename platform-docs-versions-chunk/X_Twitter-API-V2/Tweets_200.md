platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/migrate

## Other migration resources

[Twitter API migration hub](https://developer.twitter.com/en/docs/twitter-api/migrate "Twitter API migration hub")

[Check out some sample code for these endpoints](https://github.com/twitterdev/Twitter-API-v2-sample-code "Check out some sample code for these endpoints")

[Tweet counts: Enterprise to Twitter API v2](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/migrate/enterprise-to-twitter-api-v2 "Tweet counts: Enterprise to Twitter API v2")