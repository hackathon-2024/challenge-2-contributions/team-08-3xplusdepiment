platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets-id


### Example responses

* [Default fields](#tab0)
* [Optional fields](#tab1)

Default fields

Optional fields

      `{   "data": {     "id": "1460323737035677698",     "text": "Introducing a new era for the Twitter Developer Platform! nn📣The Twitter API v2 is now the primary API and full of new featuresn⏱Immediate access for most use cases, or apply to get more access for freen📖Removed certain restrictions in the Policynhttps://t.co/Hrm15bkBWJ https://t.co/YFfCDErHsg",     "edit_history_tweet_ids": [       "1460323737035677698"     ]   } }`
    

      `{   "data": {     "attachments": {       "media_keys": [         "7_1460322142680072196"       ]     },     "id": "1460323737035677698",     "text": "Introducing a new era for the Twitter Developer Platform! nn📣The Twitter API v2 is now the primary API and full of new featuresn⏱Immediate access for most use cases, or apply to get more access for freen📖Removed certain restrictions in the Policynhttps://t.co/Hrm15bkBWJ https://t.co/YFfCDErHsg",     "edit_history_tweet_ids": [       "1460323737035677698"     ]   },   "includes": {     "media": [       {         "duration_ms": 11093,         "media_key": "7_1460322142680072196",         "type": "video"       }     ]   } }`