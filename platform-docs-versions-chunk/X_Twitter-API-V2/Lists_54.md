platform: X
topic: Twitter-API-V2
subtopic: Lists
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Lists.md
url: https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference

API reference

## API reference index

For the complete API reference, select an endpoint from the list.  
  

### Manage Lists  

|     |     |
| --- | --- |
| **Creates a new List on behalf of an authenticated user** | `[POST /2/lists](https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference/post-lists)` |
| **Deletes a List the authenticated user owns** | `[DELETE /2/lists/:id](https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference/delete-lists-id)` |
| **Updates the metadata for a List the authenticated user owns** | `[PUT /2/lists/:id](https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/api-reference/put-lists-id)` |