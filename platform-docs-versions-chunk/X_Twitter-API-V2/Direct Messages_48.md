platform: X
topic: Twitter-API-V2
subtopic: Direct Messages
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Direct Messages.md
url: https://developer.twitter.com/en/docs/twitter-api/direct-messages/manage/quick-start

Quick start

## Getting started with the manage Direct Message endpoints

This quick start guide will help you make your first request to the Direct Message endpoints using Postman, a tool for managing and making HTTP requests. To learn more about our Postman collections, please visit our [Using Postman](https://developer.twitter.com/en/docs/tools-and-libraries/using-postman) guide,

Please visit our [Twitter API v2 sample code](https://github.com/twitterdev/Twitter-API-v2-sample-code) GitHub repository if you want to review Python-based examples. In addition, the official [Twitter Developer Platform software development kits (SDKs)](https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/sdks/overview) will be updated to support these Direct Message endpoints.