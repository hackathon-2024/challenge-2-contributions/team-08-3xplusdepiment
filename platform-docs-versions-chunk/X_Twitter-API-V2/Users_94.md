platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/follows/api-reference/get-users-id-following

GET /2/users/:id/following

# GET /2/users/:id/following

Returns a list of users the specified user ID is following.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=usersIdFollowing&params=%28%27query%21%28%29%7Ebody%21%28%29%7Epath%21%28%21id%21%272244994945%27%29%29_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2F%7Bid%7D%2Ffollowing&method=get) 

### Endpoint URL

`https://api.twitter.com/2/users/:id/following`