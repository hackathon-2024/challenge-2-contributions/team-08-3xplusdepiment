platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/mutes/introduction

### Mutes lookup

The mutes lookup endpoint allows you to see which accounts the authenticated user has muted. This endpoint has a rate limit of 15 requests per 15 minutes per user.