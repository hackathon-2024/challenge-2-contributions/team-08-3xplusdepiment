platform: X
topic: Twitter-API-V2
subtopic: Tweets
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Tweets.md
url: https://developer.twitter.com/en/docs/twitter-api/tweets/counts/api-reference/get-tweets-counts-recent

GET /2/tweets/counts/recent

# GET /2/tweets/counts/recent

The recent Tweet counts endpoint returns count of Tweets from the last seven days that match a query.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Ftweets%2Fcounts%2Frecent&method=get) 

### Endpoint URL

`https://api.twitter.com/2/tweets/counts/recent`