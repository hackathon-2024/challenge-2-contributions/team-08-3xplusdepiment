platform: X
topic: Twitter-API-V2
subtopic: Users
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/X_Twitter-API-V2/Users.md
url: https://developer.twitter.com/en/docs/twitter-api/users/lookup/api-reference/get-users-id

GET /2/users/:id

# GET /2/users/:id

Returns a variety of information about a single user specified by the requested ID.

[Run in Postman ❯](https://t.co/twitter-api-postman) 

[Try a live request ❯](https://oauth-playground.glitch.me/?id=findUserById&params=%28%27query%21%28%29%7Ebody%21%28%29%7Epath%21%28%21*%7E**id%21%272244994945%27%29%01*_) 

[Build request with API Explorer ❯](https://developer.twitter.com/apitools/api?endpoint=%2F2%2Fusers%2F%7Bid%7D&method=get) 

### Endpoint URL

`https://api.twitter.com/2/users/:id`