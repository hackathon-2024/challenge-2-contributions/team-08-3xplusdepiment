platform: CrowdTangle
topic: API
subtopic: API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/CrowdTangle_API/API Documentation.md
url: https://github.com/CrowdTangle/API/wiki/Terms-and-Policy


## [](#boring-stuff)Boring Stuff

* Platforms
    * The CrowdTangle API includes data from social media platforms. The platforms' individual policies apply to similar objects from CrowdTangle. For example, relevant data from a Facebook post found through the CrowdTangle API remains bound by the Facebook policies.
* Modification
    * We reserve the right to change this policy at any time without notice or liability to you. Continued use of the API constitutes acceptance of any modifications.
* Termination
    * If you fail to comply with the policy, we have the right to revoke your API access.
    * If you disagree with any parts of the policy, you may terminate this policy by discontinuing use of our API.
* Terms
    * CrowdTangle, Inc ("CrowdTangle", "we", "us", "our").
    * The CrowdTangle publicly available Application Programming Interface as well as the related API Documentation ("API").
    * A person using the API or on behalf of a legal entity with the authority to bind such entity to the policy ("you", "your").
* Responsibility
    * You agree to defend, indemnify and hold harmless CrowdTangle from and against all damages, losses, and expenses of any kind (including reasonable legal fees and costs) related to any claim against us related to your service, actions, content or information.
    * API use remains bound by the terms already agreed to with CrowdTangle, which may be our [general terms](https://www.crowdtangle.com/terms).