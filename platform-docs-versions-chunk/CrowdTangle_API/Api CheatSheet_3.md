platform: CrowdTangle
topic: API
subtopic: Api CheatSheet
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/CrowdTangle_API/Api CheatSheet.md
url: https://help.crowdtangle.com/en/articles/3443476-api-cheat-sheet

# Tools and resources for getting started:

**[Postman](https://www.getpostman.com/) is a free API management software. [This JSON file provides a template for each endpoint](https://ct-staticfiles.s3-us-west-1.amazonaws.com/api/API-Demo-2020.postman_collection.json)** (just right-click and select "Save Link As..." to save the file, and then import into Postman).

**Check out our full [API documentation](https://github.com/CrowdTangle/API/wiki) on GitHub**.

[Watch this training](https://vimeo.com/453763307) to learn more about getting the most out of the CrowdTangle API.

Please note that academic users currently don't have access to Reddit data through our API. Reddit announced changes to their API, which resulted in CrowdTangle losing access and the ability to serve Reddit data.