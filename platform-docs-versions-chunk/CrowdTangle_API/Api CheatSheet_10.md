platform: CrowdTangle
topic: API
subtopic: Api CheatSheet
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/CrowdTangle_API/Api CheatSheet.md
url: https://help.crowdtangle.com/en/articles/3443476-api-cheat-sheet

## /lists

* This endpoint tells you the names and IDs of all your Lists. This allows you to quickly pull List IDs for inclusion or exclusion in queries with other endpoints.
    

## /lists/:listId/accounts

* This endpoint tells you the names of all the Pages or Groups in a given List.  This allows you to quickly pull Account IDs for inclusion or exclusion in queries with other endpoints.
    

   
​**For more information, please see this [broader overview of the API](https://help.crowdtangle.com/en/articles/1189612-crowdtangle-api), and check out our [API documentation](https://github.com/CrowdTangle/API/wiki) on GitHub.**