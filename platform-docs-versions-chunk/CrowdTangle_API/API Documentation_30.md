platform: CrowdTangle
topic: API
subtopic: API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/CrowdTangle_API/API Documentation.md
url: https://github.com/CrowdTangle/API/wiki/Formats

# Formats

### [](#formats)Formats

The CrowdTangle API supports two formats: xml and json. Requesting the different formats is easy: simply add the extension to the specified endpoint. For example:

`https://api.crowdtangle.com/posts.json //=> json`

`https://api.crowdtangle.com/posts.xml //=> xml`