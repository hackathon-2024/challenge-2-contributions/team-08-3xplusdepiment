platform: CrowdTangle
topic: API
subtopic: API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/CrowdTangle_API/API Documentation.md
url: https://github.com/CrowdTangle/API/wiki/Post

### [](#media)Media

The media object represents a piece of media (e.g., video, photo) for a post. It contains the type, source and any additional metadata.

#### [](#properties-2)Properties

| Property | Type | Description |
| --- | --- | --- |
| full | string | The source of the full-sized version of the media. |
| height | int | The height of the media. |
| type | enum (photo or video) | The type of the media. |
| url | string | The source of the media. |
| width | int | The width of the media. |