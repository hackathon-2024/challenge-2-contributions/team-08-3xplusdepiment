platform: CrowdTangle
topic: API
subtopic: Api CheatSheet
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/CrowdTangle_API/Api CheatSheet.md
url: https://help.crowdtangle.com/en/articles/3443476-api-cheat-sheet

API Cheat Sheet

Shortcuts to understanding and using the CrowdTangle API.

**Update July 2023:**

* Reddit announced changes to their API, which resulted in CrowdTangle losing access and the ability to serve Reddit data. You can find more information [here](https://www.redditinc.com/blog/apifacts#:~:text=Our%20API%20pricing%20structure%20and%20why%20it%20matters) and [here](https://www.redditinc.com/blog/2023apiupdates).
    

* * *

The CrowdTangle API corresponds to several functions available through the Dashboard. It’s as if you were directly obtaining a CSV of your List or Saved Search, but in JSON or XML format. Please note that we don't have all filters available in the new Search surface in the API yet, but all the same posts are available.