platform: CrowdTangle
topic: API
subtopic: Api CheatSheet
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/CrowdTangle_API/Api CheatSheet.md
url: https://help.crowdtangle.com/en/articles/3443476-api-cheat-sheet

# Why would you use the CrowdTangle API instead of the UI?

* If you're trying to get **post time-series data and post content information together,** in bulk.
    
* If you're more comfortable using a programming language than clicking around in the UI.
    
* If you want to download a **hosted image link** for any photos that appear in post content.