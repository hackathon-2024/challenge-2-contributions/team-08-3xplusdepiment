platform: CrowdTangle
topic: API
subtopic: API Documentation
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/CrowdTangle_API/API Documentation.md
url: https://github.com/CrowdTangle/API/wiki/Terms-and-Policy

# Terms and Policy

By using the CrowdTangle API, you agree to follow the guidelines set forth by the following policy:

## [](#things-you-care-about)Things You Care About

* Tokens
    * To make calls to the API, you must use your own token(s), which you can find in your dashboard(s).
* Rate Limits
    * Rate limits are set at different levels for different calls, starting as low as 2 per minute per token. We also have internal limits for memory/data/CPU usage and will contact users who go over them.
* Visibility
    * Do not connect front-end widgets directly to our API. Have your back end pull from the API and cache it, and make the front end use your cache.