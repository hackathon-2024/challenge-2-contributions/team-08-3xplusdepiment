platform: Bing
topic: Ad-Library-API
subtopic: Ad Library API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_Ad-Library-API/Ad Library API.md
url: https://learn.microsoft.com/en-us/advertising/guides/ad-library-api?view=bingads-13

## API Authentication

Like the Ad Library UI, the Ad Library API is publicily available and doesn't require any user sign-up or log-in. However, to protect the security and stability of our platform, unauthenticated requests will face stricter limits on how frequently you can call the API and how many entities you can request per call.

To use the Ad Library API with increased limits, you'll need:

* A Microsoft account
* A Microsoft Advertising account
* A developer token

To create a Microsoft Advertising account, go to [https://ads.microsoft.com](https://ads.microsoft.com/). If you're not signed in to your Microsoft account, you'll be redirected to sign in to your account or to sign up for one. After signing in, you'll have the option to **Sign up for a new Microsoft Advertising account**. Select the sign up option and select **Continue**.