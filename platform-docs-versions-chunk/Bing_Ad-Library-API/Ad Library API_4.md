platform: Bing
topic: Ad-Library-API
subtopic: Ad Library API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_Ad-Library-API/Ad Library API.md
url: https://learn.microsoft.com/en-us/advertising/guides/ad-library-api?view=bingads-13


### Value Sets

The following are the value sets used by the API.

| Object | Description |
| --- | --- |
| Country | Defines the values of countries considered by the Ad Library for filtering and reporting ad impressions. |
| TargetType | Defines the types of targeting parameters tracked by the Ad Library. |
| RestrictionReason | Defines the reason an ad has been restricted from serving further or displaying in the Ad Library. |

#### Country

Defines the values of European Union countries and members of the European Economic Area considered by the Ad Library for filtering and reporting ad impressions.

| Name | Value |
| --- | --- |
| Austria | 10  |
| Belgium | 14  |
| Bulgaria | 26  |
| Croatia | 49  |
| Cyprus | 207 |
| Czechia | 51  |
| Denmark | 53  |
| Estonia | 61  |
| Finland | 65  |
| France | 66  |
| Germany | 72  |
| Greece | 76  |
| Hungary | 88  |
| Ireland | 92  |
| Italy | 93  |
| Latvia | 104 |
| Lithuania | 108 |
| Luxembourg | 109 |
| Malta | 115 |
| Netherlands | 129 |
| Poland | 151 |
| Portugal | 152 |
| Romania | 226 |
| Slovakia | 165 |
| Slovenia | 167 |
| Spain | 170 |
| Sweden | 175 |
| Iceland | 89  |
| Liechtenstein | 107 |
| Norway | 139 |

#### TargetType

Defines the types of targeting parameters tracked by the Ad Library.

| Name | Value | Description |
| --- | --- | --- |
| Gender | 1   | Targeting based on gender. |
| Age | 2   | Targeting based on age group. |
| Location | 3   | Targeting based on geographic areas users are located in or searching for. |
| MicrosoftAudiences | 22  | Targeting based on Microsoft-defined audiences including in-market audiences, similar audiences, and LinkedIn profile targeting. |
| AdvertiserAudiences | 25  | Targeting based on Advertiser-defined audiences including custom audiences, customer match lists, and remarketing lists. |