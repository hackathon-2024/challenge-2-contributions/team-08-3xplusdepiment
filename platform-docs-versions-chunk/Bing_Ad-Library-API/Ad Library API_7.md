platform: Bing
topic: Ad-Library-API
subtopic: Ad Library API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_Ad-Library-API/Ad Library API.md
url: https://learn.microsoft.com/en-us/advertising/guides/ad-library-api?view=bingads-13

### Where to use your credentials and developer tokens?

For increased limits, all calls must specify:

* The DeveloperToken header that's set to your developer token.
* The AuthenticationToken header that's set to your access token.

For information about these and other headers that the request and response may contain, see [Headers](https://learn.microsoft.com/en-us/advertising/shopping-content/products-resource.md#headers).