platform: Bing
topic: Ad-Library-API
subtopic: Ad Library API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_Ad-Library-API/Ad Library API.md
url: https://learn.microsoft.com/en-us/advertising/guides/ad-library-api?view=bingads-13

# Ad Library API Overview

* Article
* 09/27/2023
* 2 contributors

Feedback

## In this article

The Ad Library is a public transparency tool that allows you to view all ads shown on Bing.com. You can search for ads by advertiser name and keywords present in the ad copy, and you will see both the ad content and additional ad details. The ad library is part of a compliance effort to ensure we are creating a safe and open internet for our users.

## Ad Library API Resource

### Base URI

The following is the base URI that you append the templates to:

[https://adlibrary.api.bingads.microsoft.com/api/v1/](https://adlibrary.api.bingads.microsoft.com/api/v1/)