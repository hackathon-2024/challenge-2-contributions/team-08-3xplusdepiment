platform: Bing
topic: Ad-Library-API
subtopic: Ad Library API
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Bing_Ad-Library-API/Ad Library API.md
url: https://learn.microsoft.com/en-us/advertising/guides/ad-library-api?view=bingads-13


## Easiest Microsoft Advertising Account Sign-Up Steps

If you are a researcher or regulator who wants to access the Ad Library API, but you don’t already have a Microsoft Advertising Account or wish to advertise on our platform, complete the following steps:

1. Visit [https://ads.microsoft.com/](https://ads.microsoft.com/) and select **Sign up now**. ![Ad Library API Sign-up Step 1](media/ad-library-api-sign-up-step-1.png?view=bingads-13 "Step 1")
    
2. Sign in with an existing Microsoft account or select **Create one!** ![Ad Library API Sign-up Step 2](media/ad-library-api-sign-up-step-2.png?view=bingads-13 "Step 2")
    
3. Use an existing email address or phone number, or select **Get a new email address**. ![Ad Library API Sign-up Step 3](media/ad-library-api-sign-up-step-3.png?view=bingads-13 "Step 3")  
    Enter the above info if you are creating a new email address (email address, password, first/last name, country/region, and birthdate) and solve captcha if necessary.
    
4. To skip through the setup as fast as possible, select **Create new campaign** and then **Switch to expert**. ![Ad Library API Sign-up Step 4a](media/ad-library-api-sign-up-step-4a.png?view=bingads-13 "Step 4a")  
    ![Ad Library API Sign-up Step 4b](media/ad-library-api-sign-up-step-4b.png?view=bingads-13 "Step 4b")
    
5. Select **Skip campaign creation** to set up an account without a campaign. ![Ad Library API Sign-up Step 5](media/ad-library-api-sign-up-step-5.png?view=bingads-13 "Step 5")
    
6. At this point, an account has been created and no further information is needed. ![Ad Library API Sign-up Step 6](media/ad-library-api-sign-up-step-6.png?view=bingads-13 "Step 6")
    

You can now proceed to [Developer Account](https://developers.ads.microsoft.com/Account) to request a token for accessing the API. As part of this, you’ll need to review our [Developer Terms of Use](https://www.microsoft.com/en-us/legal/terms-of-use).