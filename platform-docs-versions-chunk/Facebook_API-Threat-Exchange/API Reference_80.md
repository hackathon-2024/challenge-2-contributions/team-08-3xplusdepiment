platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/reacting

## React Using the UI

2. Search for threat descriptors using [any method of your choice](https://developers.facebook.com/docs/threat-exchange/reference/ui/descriptors#searching); for example, using the tag `testing-reaction-editing`.

5. You can react to threat descriptors owned by other apps (the **View** button), not to those owned by your app (**Edit** button).

  

10. Click **Add Reaction**.

  

15. Select your reactions and click **Save**.

  

20. Dismiss the popup.

  

25. The next image shows being logged in as the owner app. Click **Edit** to view details.

  

For the owner app the reactions are read-only, formatted as a table.