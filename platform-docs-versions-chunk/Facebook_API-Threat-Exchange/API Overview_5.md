platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-started

# Get Started with ThreatExchange

ThreatExchange is a simple-to-use platform that supports signal sharing among predefined groups of members in a secure, privacy-compliant, and automated way. Today, ThreatExchange (aka TX or TE) is used by multiple companies to share signals on a variety of topics intended to prevent real world harm. Some examples of how TX is currently used include sharing malware, phishing scams, and terrorism signals with the goal of helping all participating organizations tackle these problems based on their terms of service.

ThreatExchange is built on these core concepts:

* [Signals (aka ThreatIndicators)](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator/)
* [Opinions about signals (aka ThreatDescriptors)](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptor/)
* [Who can see signals (aka Privacy)](https://developers.facebook.com/docs/threat-exchange/reference/privacy)

These concepts allow a group of ThreatExchange members to share signals, [react to](https://developers.facebook.com/docs/threat-exchange/reference/reacting) and describe signals other members upload, and decide individually on how a signal aligns with their policies.