platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/editing

# Editing existing data

The ThreatExchange API allows for editing existing [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator) objects. As with all Facebook Graph APIs, editing is performed via an HTTP POST request to the object's unique ID URL.

## Editing single threat descriptors using the UI

Using any of various search mechanisms, identify a descriptor you own and click the Edit button:

  

Then, fields are editable as in the Create pop-up: