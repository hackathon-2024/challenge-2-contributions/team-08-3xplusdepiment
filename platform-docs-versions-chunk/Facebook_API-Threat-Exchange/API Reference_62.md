platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/submitting

# Submitting New Data

Visit the [**descriptors-tab page**](https://developers.facebook.com/docs/threat-exchange/reference/ui/descriptors) to see more things you can do with threat descriptors within the ThreatExchange UI including searching, bulk download, and more.

## Creating

Using the Create button you can upload a new descriptor, with tooltips to provide context. Here's an example of submitting a malicious domain using the UI:

Note : If you set a descriptor's privacy to has-whitelist and include no whitelist apps, the owner's app is automatically included. This is a "visible to self" or "storage mode" option.