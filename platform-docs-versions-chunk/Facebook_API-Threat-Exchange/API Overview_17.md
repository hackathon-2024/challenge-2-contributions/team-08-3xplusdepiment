platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-access

### Submit for Review

If you get a message about needing to upload an Android or iOS version of your app (ThreatExchange does not use installable apps), go back to **Settings** -> **Basic**. Ensure that for "platform", you have used "website" with your company's URL.

  

[](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/292590954_1100503220545003_3825931399270878094_n.png?_nc_cat=100&ccb=1-7&_nc_sid=e280be&_nc_ohc=gsYLMH0wdXAAX8XfKBl&_nc_ht=scontent-cdg4-2.xx&oh=00_AfCj-Qn1TM7xhb-hriM6rnylTx1atTIDPSZogumhAMeefg&oe=65D55D57)

  
  

[](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/292621568_452732026275021_2109765007626020073_n.png?_nc_cat=100&ccb=1-7&_nc_sid=e280be&_nc_ohc=--um6kiyTzsAX_0ky53&_nc_ht=scontent-cdg4-2.xx&oh=00_AfDFPIbaEmKmQthkob6gcDzYO0aufUj6dT6AhkTWOl5yvg&oe=65D56179)