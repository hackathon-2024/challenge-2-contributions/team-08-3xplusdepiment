platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/ui

## Find the UI

Navigate to [https://developers.facebook.com/apps](https://developers.facebook.com/apps) and select your app:

  

Then, find the ThreatExchange product within the navbar on the left:

## Add Team Members

1. Navigate to [https://developers.facebook.com/apps](https://developers.facebook.com/apps) and select your app.
2. Select **Roles** > **Roles**.
3. Add teammate roles as **Administrators** or **Developers**. **Note**: Do not add teammates as **Test Users** or **Analytics Users**. These roles have no meaning for ThreatExchange apps.
4. If your organization has a ThreatExchange app ID, but the assigned administrators/developers have since left your organization, please contact us at **threatexchange@meta.com** so that we can reset an admin to be a current employee of your organization. From there, you'll be able to self-service add everyone else in your organization.