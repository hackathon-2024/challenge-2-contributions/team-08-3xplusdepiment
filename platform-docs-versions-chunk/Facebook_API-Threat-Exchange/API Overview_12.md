platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-access

# Get Access

Getting access to ThreatExchange is done through Facebook Developer Applications, and has a few steps:

1. Creating a Facebook account if you do not already have one.
2. Find your company's business account, or creating a new account if your company doesn't have one.
3. Creating a ThreatExchange application connected to your business account and adding the ThreatExchange product.
4. If your business account is not already business verified, you'll need to verify it. This is the longest step and can take 2 or more weeks with multiple submissions.
5. Making a ThreatExchange App Submission. This usually takes 1-2 business days to complete.