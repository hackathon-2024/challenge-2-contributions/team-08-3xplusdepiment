platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/api-structure

## Viewing Individual Objects

You can access a Graph object’s properties with its unique ID, e.g. for a [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator) object:

* [/{threat\_indicator\_id}](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator)
    

You can do the same for all other objects type within ThreatExchange:

* [/{threat\_descriptor\_id}](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptor)