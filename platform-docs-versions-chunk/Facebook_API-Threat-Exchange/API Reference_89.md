platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/integrations

### Splunk add-on

* [Splunk Add-On documentation](https://l.facebook.com/l.php?u=https%3A%2F%2Fdocs.splunk.com%2FDocumentation%2FES%2F4.1.1%2FAddons%2FFBThreatExchange&h=AT3zwrZyzIJPEJhjiVY6bxX17pxksI7kLm-ITnLacMGILuErIvOOwf_HZw4phhoQOG0xbIGcLDTUa65QKuG6n36V4ouZ5dBxKD5rgO1vc6RupFwsg-c4bKoRKXpIUrYbRNmcPLy2DxnWF0ES)