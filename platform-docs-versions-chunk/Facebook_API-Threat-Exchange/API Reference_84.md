platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/submitting-connections

## Use the UI for Bulk Relations

Just as in the [Use the UI](#using-ui) topic, you can assume that multiple descriptors are related to another one.

2. In the next example, do a query for a particular tag (can be any set of descriptors).
3. Click **Bulk relate**.

  

8. Supply the ID of the related-to indicator and click **OK**.