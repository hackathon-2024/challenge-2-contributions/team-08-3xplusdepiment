platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/examples

# API Examples

This page has various API examples in [Python](#python_examples), [Java](#java_examples), [PHP](#php_examples), and using [cURL](#curl_examples).