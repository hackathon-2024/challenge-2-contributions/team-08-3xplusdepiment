platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange

# ThreatExchange

Most threat-intelligence solutions suffer because the data is too hard to standardize and verify. Meta created the ThreatExchange platform so that participating organizations can share threat data using a convenient, structured, and easy-to-use API that provides privacy controls to enable sharing with only desired groups. You can [apply for membership](https://developers.facebook.com/products/threat-exchange) today!

|     |     |
| --- | --- |
| #### [Get Started](https://developers.facebook.com/docs/threat-exchange/getting-started/)<br><br>Learn how ThreatExchange is structured and walk through steps to get started.<br><br>#### [Get Access](https://developers.facebook.com/docs/threat-exchange/getting-access/)<br><br>Learn the steps required to apply to ThreatExchange and how to add the product.<br><br>#### [API Structure](https://developers.facebook.com/docs/threat-exchange/api-structure/)<br><br>Learn how the ThreatExchange API makes use of the Graph API and try a few calls to get started. | #### [Best Practices](https://developers.facebook.com/docs/threat-exchange/best-practices)<br><br>Make the most effective use of the API by following these tips.<br><br>#### [Reference Code on GitHub](https://l.facebook.com/l.php?u=https%3A%2F%2Fgithub.com%2Ffacebook%2FThreatExchange%2F&h=AT1WI6DmdUHssrYlJe13s7TTvKK5v7VRmlUUObUpCk56rsIPC5Og2Fgr3xh1oq4VGltrFm3SS3QqghdysnRWbqI-NxxKKpexIY-knX7K4VRWoIuKp9HaU3hpgnej-Em4zCall7zkhxgoi_Vr)<br><br>Use freely available libraries in JavaScript, PHP, Python, and Ruby to speed up your integration and development.<br><br>#### [Terms and Conditions](https://www.facebook.com/legal/threatexchange_terms/)<br><br>Learn about terms of use. |