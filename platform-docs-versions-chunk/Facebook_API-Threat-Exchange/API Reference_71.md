platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/editing

## Cloning and duplicating

Once you've found a threat descriptor, you may wish to publish a modified copy of it. We use the terms "cloning" for making a copy of your own descriptor (perhaps changing the indicator-text, for example) and "duplicating" for making a copy of someone else's (perhaps changing subjective parameters such as your view of the malicious, the first-active-timestamp, etc.). Regardless, though, Clone and Duplicate both create new threat descriptors owned by you.

Here we search for descriptors visible to us with tag `testing`, then select one to clone.

  

The clone popup is simply a create-descriptor popup -- pre-populated with the cloned-from descriptor's attributes. We can edit whatever we like, then hit OK.

  

Once we hit OK we've got a new descriptor owned by us. We can then go on to duplicate it, if we like.