platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-started

## What Signals are Commonly Shared?

In ThreatExchange, we refer to the signals being shared as [Indicators](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator/). Over 80 types of Indicators can be shared on ThreatExchange and the full list can be found [here](https://developers.facebook.com/docs/threat-exchange/reference/apis/indicator-type). There are, however, a few data types that are particularly common.