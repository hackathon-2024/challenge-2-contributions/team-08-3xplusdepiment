platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptor

# ThreatDescriptor

A subjective opinion about a [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator/) that was submitted by a [ThreatExchangeMember](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-exchange-member).