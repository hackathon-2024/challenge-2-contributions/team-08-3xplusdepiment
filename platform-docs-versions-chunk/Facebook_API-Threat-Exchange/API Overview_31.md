platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/ui

## Search Data Using the UI

A variety of search options are supported.

  

Search for all malicious URLs uploaded in the last week:

## Publish Data Using the UI

See the [Submit Data](https://developers.facebook.com/docs/threat-exchange/reference/submitting) page for examples.

## Feedback

Contact **threatexchange@meta.com** with any and all feedback on how we can better enable your success in using ThreatExchange.

You can also use the bugnub to report issues:

  

Learn more about the [UI Reference](https://developers.facebook.com/docs/threat-exchange/reference/ui).