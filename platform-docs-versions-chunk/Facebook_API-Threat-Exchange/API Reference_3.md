platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis


## Types

| Parameter | Description |
| --- | --- |
| [IndicatorType](https://developers.facebook.com/docs/threat-exchange/reference/apis/indicator-type) | Type of indicator being described by a [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator) object. |
| [PrecisionType](https://developers.facebook.com/docs/threat-exchange/reference/apis/precision-type) | Defines how accurately the threat intelligence detects its intended target, victim or actor. |
| [PrivacyType](https://developers.facebook.com/docs/threat-exchange/reference/apis/privacy-type) | Defines who can access the threat intelligence. |
| [ReviewStatusType](https://developers.facebook.com/docs/threat-exchange/reference/apis/review-status-type) | Description of how the threat intelligence was vetted. |
| [SeverityType](https://developers.facebook.com/docs/threat-exchange/reference/apis/severity-type) | Description of the threat dangerousness associated with a [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator) object. The order of the values below are ordered from least severe to most severe. |
| [SignatureType](https://developers.facebook.com/docs/threat-exchange/reference/apis/signature-type) | Type of signature format described by a [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator) object. |
| [ShareLevelType](https://developers.facebook.com/docs/threat-exchange/reference/apis/share-level-type) (aka Traffic Light Protocol or TLP) | Designation of how any object in ThreatExchange may be re-shared both within and outside of ThreatExchange, based on the [US-CERT's Traffic Light Protocol](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.us-cert.gov%2Ftlp%2F&h=AT0vDeSn_j7BSJGmtOmbSGq0VEXcFkIlwQuc-NmzRFK81MnIGABx6GsNxAdGc4_TU2Z89MFQWXWbZPaxXhqCGHDA-hLe5ucwGQU_j1cDYcri8oxIQENAW2soAtt-DAySxRP_DAe_saXsql_E). |
| [StatusType](https://developers.facebook.com/docs/threat-exchange/reference/apis/status-type) | Description of the maliciousness of any object within ThreatExchange. |