platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/best-practices

### Sample CSVs from the UI

Some privacy groups have a feature where samples of indicators can be downloaded from the UI, which is the fastest way to evaluate potential data. Learn more at [ThreatExchange UI.](https://developers.facebook.com/docs/threat-exchange/reference/ui/collaborations)

### Sampling from /threat\_descriptors API

[The /threat\_descriptors API](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptors) allows you to do complex searches on ThreatDescriptors. This can be useful to generate your own narrow samples, but the API is not guaranteed to be contain all data that matches the filters.