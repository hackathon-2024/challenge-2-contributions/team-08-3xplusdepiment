platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-access

## Add the ThreatExchange Product

If your application category is Business, you can find ThreatExchange on the [Products](https://developers.facebook.com/docs/development/create-an-app/app-dashboard#products-2) section of your application dashboard. If you don't see it there, confirm that your app type is "Business" (circled red area in the banner at the top of your dashboard). If your app type is not Business, unfortunately you will have to create a new application, as it is currently not possible to change it after creation.

When you click on "Set Up" on ThreatExchange, it will add the ThreatExchange product in your sidebar, where you can continue the submission.

### Start the Submission

[](https://scontent-cdg4-3.xx.fbcdn.net/v/t39.2365-6/292791084_475834287638346_3341843078967238976_n.png?_nc_cat=104&ccb=1-7&_nc_sid=e280be&_nc_ohc=F6uOReq5760AX_fforM&_nc_ht=scontent-cdg4-3.xx&oh=00_AfB-CPJUCoZ2HAn_oQyXVAnoPgOH8g2TNKyVvsJH7uOUdw&oe=65D58479)