platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/integrations

# Partner Integrations

To make data shared on ThreatExchange usable and actionable in existing workflows more easily, several third parties have built direct integrations with the ThreatExchange platform.

### Bit9 + CarbonBlack

* [CarbonBlack Integration Documentation](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.carbonblack.com%2Fsolutions%2Fecosystem%2Ffacebook-threatexchange%2F&h=AT1PKjFgIfTm0xce5zBdNzD825FKfTKN25wnyPf0lvsSFcwQ6eSWqM1ZJsYXDRYovZvmlEAEApM05llahhrztS5RNFVEKaok1woEZ4iAcYddSRhkEfjkCZ7-I-MvCjkora-e_z58nMJAXH9t)
    
* [CarbonBlack Connector on GitHub](https://l.facebook.com/l.php?u=https%3A%2F%2Fgithub.com%2Fcarbonblack%2Fcb-threatexchange-connector&h=AT1yHx5Iv2wKz5HvzLzDrdWdFaMBQdiw-mrgtbPMY5jG50Vy2126hyW91fGS7C1jgHxwAo2DO5jJFxZxPdPCDiEf5stz5AL3A3qYL4Vs4kuyvVy2xnVNRPSKBnGMCtL9apwSSiXm29_8hTQU)