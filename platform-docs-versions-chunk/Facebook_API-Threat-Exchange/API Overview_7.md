platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-started


## Why Would I Contribute?

There are many problems in the Trust & Safety space that affect all platforms jointly, and lead to real world harm. Signal sharing on ThreatExchange tries to reduce this harm by helping platforms find and remove more harmful content. Platforms come in all shapes and sizes, and not all can afford to hire a myriad of reviewers or invest millions in specialized machine learning models. For these platforms, investing in ThreatExchange can be an effective way to use their trust and safety resources.

Even for platform’s which already have robust trust and safety programs, there are still tangible benefits to joining and contributing to ThreatExchange. Namely, the harmful content found on those platform often doesn’t go away, it just goes somewhere else. A rising tide lifts all boats, and by all pitching in, we can improve the baseline safety level for the entire internet. Even if you aren’t uploading new signals to ThreatExchange simply confirming (or disputing!) labels will improve that baseline, build trust in our platforms, and help make the internet safer.