platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis


## Objects

| Parameter | Description |
| --- | --- |
| [ThreatDescriptor](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptor) | Subjective context provided by a [ThreatExchangeMember](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-exchange-member) for a [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator). |
| [ThreatExchangeMember](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-exchange-member) | Participant within ThreatExchange. |
| [ThreatExchangeImpactReport](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-exchange-impact-report) | Freeform record of outcomes as a result of participating in ThreatExchange. |
| [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator) | Indicator of compromise. |
| [ThreatPrivacyGroup](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-privacy-group) | Label to group threat objects together. |
| [ThreatTags](https://developers.facebook.com/docs/threat-exchange/reference/apis/threattags) | Label to group threat objects together. |