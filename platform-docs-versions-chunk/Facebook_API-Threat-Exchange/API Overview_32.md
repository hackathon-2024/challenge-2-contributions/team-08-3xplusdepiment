platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/reference/ui

# ThreatExchange UI Reference

Learn more about the information organized by the UI tab.

|     |     |
| --- | --- |
| #### [Collaborations Tab](https://developers.facebook.com/docs/threat-exchange/reference/ui/collaborations)<br><br>Download data and see statistics from collaborations in which you participate.<br><br>#### [Tags Tab](https://developers.facebook.com/docs/threat-exchange/reference/ui/tags/)<br><br>Find and organize the data you share.<br><br>#### [Members Tab](https://developers.facebook.com/docs/threat-exchange/reference/ui/members)<br><br>Discover a Partner. | #### [Descriptors Tab](https://developers.facebook.com/docs/threat-exchange/reference/ui/descriptors)<br><br>Understand fundamental components of threat-data sharing.<br><br>#### [Privacy Groups Tab](https://developers.facebook.com/docs/threat-exchange/reference/ui/privacy-groups/)<br><br>Control which partner organizations you want to have access to the threat data you share.<br><br>#### [App Review Tab](https://developers.facebook.com/docs/threat-exchange/reference/ui/app-review)<br><br>Apply to use ThreatExchange. |