platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-privacy-groups

# /threat\_privacy\_groups

This API call enables the creation of a [ThreatPrivacyGroup](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-privacy-group/) via an HTTP POST request. Privacy groups can be used to protect uploaded data. This feature is only available in versions 2.4+ of the Graph API.