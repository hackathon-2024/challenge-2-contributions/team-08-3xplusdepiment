platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/privacy-type

# PrivacyType

Defines who may view and search for a specific object within ThreatExchange.

## Values

| Name | Description |
| --- | --- |
| `HAS_PRIVACY_GROUP` | Only a [ThreatPrivacyGroup](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-privacy-group/) may view or search for the data. |
| `HAS_WHITELIST` | Only specific members of ThreatExchange may view or search for the data. |
| `VISIBLE` | All members of ThreatExchange may view and search for the data. |