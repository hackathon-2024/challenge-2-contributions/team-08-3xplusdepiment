platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/reaction-type


## Values

| Value | Meaning |
| --- | --- |
| `HELPFUL` | Indicates the piece of information was valuable. |
| `NOT_HELPFUL` | Indicates the piece of information was NOT valuable. |
| `OUTDATED` | Indicates the piece of information is no longer relevant and should be expired. |
| `SAW_THIS_TOO` | Indicates the reactor saw this in the wild. |
| `WANT_MORE_INFO` | Indicates the reactor wants additional information. |
| `DISAGREE_WITH_TAGS` | Indicates the reactor doesn't agree with the current tags on this object. If the tags change, this reaction is automatically removed. |
| `INGESTED` | Acknowledgement of receipt. Helps contributors get feedback on the usefulness of their data to others. |
| `IN_REVIEW` | Acknowledgement of intention to review. Helps contributors get feedback on the usefulness of their data to others. |
| `ALREADY_KNOWN` | Acknowledgement that a recipient was already aware of the signal. Helps contributors get feedback on the usefulness of their data to others. |
| `REVIEWED` | Acknowledgement of completed review. Helps contributors get feedback on the usefulness of their data to others. |