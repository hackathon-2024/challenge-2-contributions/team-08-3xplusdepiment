platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/share-level-type

# ShareLevelType

The following Share Level Type designations are based on the [US-CERT's Traffic Light Protocol](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.us-cert.gov%2Ftlp%2F&h=AT2bFS7IoM4X7MzZTemIzqAZ7fFyX3E3gJLThYReMWzUIZpm7Jq7I0OxtkJx8mgkZxMGSToKAi5b_kwrp8DkcMk-QlXMSHHliZXem_hWBDHqb3VjGxOrd_SmNtDgDflCEgeXpY4rYO2U9ZwV) and govern how ThreatData may be re-shared both within and outside of ThreatExchange.

These `ShareLevelType` designations are cited in and form part of the [ThreatExchange's Terms and Conditions](https://www.facebook.com/legal/threatexchange_terms) (the "Terms"). Nothing in the following `ShareLevelType` designations prohibit a Developer from using a security service provider to store and process ThreatData it receives, so long as the security service provider only uses the ThreatData on behalf of the Developer and fully complies with the Terms. All capitalized terms used but not defined on this page shall have the meanings assigned to them in the Terms.