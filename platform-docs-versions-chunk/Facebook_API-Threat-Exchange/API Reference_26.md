platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/precision-type

# PrecisionType

Defines how accurately an object detects its intended target, victim or actor.

## Values

| Name | Description |
| --- | --- |
| `UNKNOWN` | There is no known precision information. |
| `LOW` | The object is likely to detect false positives. |
| `MEDIUM` | The object may detect false positives. |
| `HIGH` | The object is unlikely to detect false positives. |