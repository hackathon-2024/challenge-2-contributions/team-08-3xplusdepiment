platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/threattags


## Popular Tags

Here is a list of the most popular tags categorizing data related to attacks:

| Name | Description |
| --- | --- |
| `access_token_theft` | Theft of an OAuth style or similar access token |
| `bogon` | A bogus IP address |
| `bot` | A bot |
| `brute_force` | Repeated attempts to access an authenticated resource |
| `clickjacking` | Any UI redressing or similar type of attack redirecting a person's clicks |
| `compromised` | The associated party has been compromised |
| `creeper` | A party which stalks another online |
| `drugs` | Associated with drugs |
| `email_spam` | Sending of unsolicited email |
| `explicit_content` | Pornographic or otherwise explicit content |
| `exploit_kit` | A set of tools used to take advantage of vulnerabilities |
| `fake_account` | An account associated with no real entity, often used for abuse |
| `financial` | Associated with financials, perhaps fraud |
| `ip_infringement` | Infringement on the rights of an intellectual property holder |
| `malicious_app` | A malicious web app |
| `malicious_nameserver` | A malicious name server |
| `malicious_webserver` | A malicious web server |
| `malvertising` | The use of online advertising to spread malware |
| `malware` | A malware-based attack |
| `passive_dns` | Interserver DNS messages are being captured, recorded, and potentially exfiltrated |
| `phishing` | An attempt to obtain credentials via a deceptive lure |
| `piracy` | Illegal replication of protected property |
| `prox` | A proxy host |
| `scam` | A generic type of scam |
| `scanning` | Port scanning to map a network |
| `scraping` | Systematic traversal of a network and recording of data |
| `self_xss` | Attack where a person is social engineered into pasting malicious code into their brower's address bar or developer console |
| `share_baiting` | A person is convinced to share spammy content in exchange for a fictitious product or content |
| `targeted` | An attack conducted by a sophisticated actor and directed at a specific target |
| `terrorism` | Associated with terrorist attacks or groups |
| `weapons` | Related to the illegal trade of arms |
| `web_app` | A malicious web app |

Here is a list of the most popular tags categorizing data by type:

| Name | Description |
| --- | --- |
| `bad_actor` | Details on a presumed bad actor (e.g. botherder, spammer) |
| `compromised_credential` | The credential compromised by an attack (must be already publicly accessible) |
| `ht_victim` | For high-value victim targeting |
| `malicious_ad` | A malicious advertisement |
| `malicious_api_key` | An API key which is being abused |
| `malicious_content` | A malicious post, image, or document |
| `malicious_domain` | A malicious Internet domain |
| `malicious_inject` | A malicious piece of code that injected into a another file, process, or DOM |
| `malicious_ip` | A malicious IP address |
| `malicious_subnet` | A malicious IP address range |
| `malicious_ssl_cert` | A malicious SSL certificate |
| `malware_sample` | A specific piece of [Malware](https://developers.facebook.com/docs/threat-exchange/reference/apis/malware) |
| `malware_victim` | A victim of [Malware](https://developers.facebook.com/docs/threat-exchange/reference/apis/malware) |
| `proxy_ip` | An IP address known to be a proxy or VPN |
| `signature` | Represents some means or pattern for detecting a threat |
| `web_request` | A full web request, optionally with GET query parameters |
| `whitelist_domain` | An Internet domain that should be treated as non-malicious |
| `whitelist_ip` | An IP address that should be treated as non-malicious |
| `whitelist_url` | An URI that should be treated as non-malicious |