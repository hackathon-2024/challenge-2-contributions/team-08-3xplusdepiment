platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange

## ThreatExchange UI

The UI contains point-and-click management for tags, privacy groups, small-scale descriptor download and upload, and more—while bulk data-processing is best deferred to the API.

|     |     |
| --- | --- |
| ### [UI Overview](https://developers.facebook.com/docs/threat-exchange/ui)<br><br>Essential information about the UI. | ### [UI Reference](https://developers.facebook.com/docs/threat-exchange/reference/ui)<br><br>Full documentation for the ThreatExchange UI. |