platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange


## ThreatExchange API

Your one-stop shop for ThreatExchange resources. The API contains the complete feature-set of what is possible in ThreatExchange.

|     |     |
| --- | --- |
| ### [API Reference](https://developers.facebook.com/docs/threat-exchange/reference/apis)<br><br>Full documentation for all of the ThreatExchange APIs. | ### [Privacy Controls](https://developers.facebook.com/docs/threat-exchange/reference/privacy)<br><br>Understand the ways you control who sees the data you publish. |
| ### [Resharing Controls](https://developers.facebook.com/docs/threat-exchange/reference/resharing)<br><br>Understand how to permit re-sharing of your data. | ### [Submit Data](https://developers.facebook.com/docs/threat-exchange/reference/submitting)<br><br>Learn how to submit your data. |
| ### [Submit Connections](https://developers.facebook.com/docs/threat-exchange/reference/submitting-connections)<br><br>Learn how to create connections between pieces of data to express relationships. | ### [Edit Data](https://developers.facebook.com/docs/threat-exchange/reference/editing)<br><br>Learn how to edit your data. |
| ### [Delete Data](https://developers.facebook.com/docs/threat-exchange/reference/deleting)<br><br>Learn how to delete your data. |     |