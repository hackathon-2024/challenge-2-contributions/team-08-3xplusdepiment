platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis

## Search Endpoints

| Parameter | Description |
| --- | --- |
| [/threat\_updates](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-updates/v9.0) | Prefered way of downloading all the data for a collaboration and staying in sync with updates. Not enabled for all privacy groups. See page for details. |
| [/threat\_descriptors](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptors) | Enables searching for descriptors (opinions on content or indicators). |
| [/threat\_indicators](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicators) | Enables searching for indicators. |
| [/threat\_tags](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-tags) | Enables searching for threat tags. |