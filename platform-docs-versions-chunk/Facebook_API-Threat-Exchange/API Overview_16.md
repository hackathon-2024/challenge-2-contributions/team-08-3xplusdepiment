platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-access

### Fill Out Your Use Case

[](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/292713607_397769922179949_2193932156581216740_n.png?_nc_cat=109&ccb=1-7&_nc_sid=e280be&_nc_ohc=91oAHI3_53MAX8K1Fr0&_nc_ht=scontent-cdg4-2.xx&oh=00_AfAyxEBoDv782gvKZvCrLqOmzL0VeDyBM5yIo5GLGzJZ7A&oe=65D58B15)

  

### Make the App Public

If you haven't already done so, check that you have changed the app's status to "Live".

[](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.2365-6/292609215_718318572763124_9222290415658146657_n.png?_nc_cat=105&ccb=1-7&_nc_sid=e280be&_nc_ohc=t9F9Dq9FY0kAX8_k8ns&_nc_ht=scontent-cdg4-1.xx&oh=00_AfBwGHCwxVpt98-3ZtpEMi4vvj9FcZhQliC1fJwPeyGbaA&oe=65D57DF4)