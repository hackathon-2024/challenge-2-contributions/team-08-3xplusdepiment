platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/reference/ui/descriptors

## Creating

Using the Create button you can upload a new descriptor, with tooltips to provide context:

Note : If you set a descriptor's privacy to has-whitelist and include no whitelist apps, the owner's app is automatically included. This is a "visible to self" or "storage mode" option.

## Uploading from CSV/JSON

Please see the [Submitting Data page](https://developers.facebook.com/docs/threat-exchange/reference/submitting#uploading) for file formats, examples, and more.