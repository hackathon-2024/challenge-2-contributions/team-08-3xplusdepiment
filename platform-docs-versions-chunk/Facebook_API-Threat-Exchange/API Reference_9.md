platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptor

### Connections

| Parameter | Description | Type |
| --- | --- | --- |
| `tags` | The tags applied to this descriptor. | `string` |

For additional documentation on ThreatTags, see [ThreatTag Object](https://developers.facebook.com/docs/threat-exchange/reference/apis/threattags/v2.8)