platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-started


## How do I Start Reading and Sharing Signals?

Here are the ways to share signals on ThreatExchange:

* **UI**: ThreatExchange has a graphical user interface you can use to quickly and interactively do things like read and share signals and run queries. This is the best place to quickly explore the data in ThreatExchange. Learn more about [ThreatExchange](https://developers.facebook.com/docs/threat-exchange/ui).
    
* **Python**: To build an inital integration and to preform validation we recommend using the python [open source library](https://l.facebook.com/l.php?u=https%3A%2F%2Fgithub.com%2Ffacebook%2FThreatExchange%2Ftree%2Fmaster%2Fpython-threatexchange&h=AT0FNlCfjoAu_8a12uPzlFQamnZawP11ti8fHsCiXEaKSn4iomiWaOdFlB3SUxDd0MwdVX7ZWKpSxxaWOWKsaokI0DzeinL4-VpRWovkZG3uaaRv6J3GOeiZRRm3HTndhyRQm_OQSK2vSjew) we’ve developed. This allows you fetch a copy of shared signals in a simple format.
    
* **API**: Lastly, there is also a powerful HTTP API which has greater functionality than the python wrapper for an advanced integration. Learn more about these [APIs](https://developers.facebook.com/docs/threat-exchange/reference/apis).

To use any of these methods you will first need to get access to ThreatExchange. ThreatExchange requires you (or someone on your team) to have a Facebook account, or to create one, and then will require creating a new application. Afterwards, you can apply for access to ThreatExchange, which requires you to confirm that your application belongs to your business. After that, you can add more accounts to the application, or store a token to gain access to the API.

Follow [these steps](https://developers.facebook.com/docs/threat-exchange/reference/ui/app-review) to create an App and get access to ThreatExchange.