platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-access


### Fill Out App Settings

[](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.2365-6/292890012_627854204928767_1789413238663381649_n.png?_nc_cat=102&ccb=1-7&_nc_sid=e280be&_nc_ohc=Zq_3nmuDDGYAX9HB6V6&_nc_ht=scontent-cdg4-1.xx&oh=00_AfBvoYkRjqUB7bbIPWSr68OyxrgKiugeOEWBgnSmhcFN_A&oe=65D55CFA)

  

ThreatExchange uses the app-ID framework, but there is no installable app per se.

* Enter URLs appropriate for your organization and any company logo for the "app icon".
* For "platform", use "website" and your company's URL.
* Business verification can take some amount of time to get through. Please contact us at threatexchange@meta.com with any issues.

  

[](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.2365-6/292466362_397227655570395_1131662562722176425_n.png?_nc_cat=102&ccb=1-7&_nc_sid=e280be&_nc_ohc=hgwAsS3qaGQAX_6wm-e&_nc_ht=scontent-cdg4-1.xx&oh=00_AfDSMjKsVcuf_JdPXFueuks1YcRyTrTGxPvUmPsgGKstQQ&oe=65D5796E)

  
  

[](https://scontent-cdg4-3.xx.fbcdn.net/v/t39.2365-6/292403367_3349426345285401_742102408421089417_n.png?_nc_cat=106&ccb=1-7&_nc_sid=e280be&_nc_ohc=khSHB2n3L-UAX9CHevu&_nc_ht=scontent-cdg4-3.xx&oh=00_AfDMgqMtoj-Wir6C0Qc56f3F-5b_ushnxjF4VhCVrTiWMQ&oe=65D56E8A)

  
  

[](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.2365-6/292694456_457967432359085_5508285514533748348_n.png?_nc_cat=102&ccb=1-7&_nc_sid=e280be&_nc_ohc=-ecStaVkgNIAX8KCNTy&_nc_ht=scontent-cdg4-1.xx&oh=00_AfBg6RlrHLhwOllrqOhc6QY-vvZMKjf5IEKHLJJTXUrSVw&oe=65D568C3)

  
  

[](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/292580731_412383744172096_2305975966123374119_n.png?_nc_cat=101&ccb=1-7&_nc_sid=e280be&_nc_ohc=qtOZO3NZNgwAX-itso6&_nc_ht=scontent-cdg4-2.xx&oh=00_AfAsmiJTyfkkTzmZdLpt8SdeTRVBjwcKHY8G1VK_Ej5dEg&oe=65D57B03)

  
  

[](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/292809995_764249381371075_1589826872236914997_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=QwEa6aJ85DEAX_2Beib&_nc_ht=scontent-cdg4-2.xx&oh=00_AfB0FrTNqIbXqHljeSt31OkVv6ZdCUDuNjDcljyBn4yPLg&oe=65D56508)