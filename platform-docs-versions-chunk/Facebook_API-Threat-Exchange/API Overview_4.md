platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange

## Learn More

Look here for anything else you may be missing.

|     |     |
| --- | --- |
| ### [Terms and Conditions](https://www.facebook.com/legal/threatexchange_terms)<br><br>The complete terms and conditions for ThreatExchange. | ### [Changelog](https://developers.facebook.com/docs/threat-exchange/reference/changelog)<br><br>See how the ThreatExchange API has changed. |
| ### [Open Source Terms of Use](https://opensource.facebook.com/legal/terms)<br><br>Terms of use for general Facebook Open Source projects. | ### [Open Source Privacy Policy](https://opensource.facebook.com/legal/privacy)<br><br>Privacy Policy for general Facebook Open Source projects. |