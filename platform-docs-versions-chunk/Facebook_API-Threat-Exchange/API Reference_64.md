platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/submitting

## Uploading from CSV/JSON

Both CSV and JSON formats are supported.

* See [below for information on column/attribute names](#parameters).
    
* Alternatively, you can simply save any descriptor-query result to CSV and use that as a template (and likewise for JSON).
    

Start by selecting the Bulk Upload button:

Select your file:

If you wish to revise your data before committing you can do so:

If there are errors detected before committing you'll be notified, and you can revise them. (Note that not all possible errors are surfaced here.)

Within the revision dialog you can fix the errors and hit OK to continue:

Once you hit the Confirm Upload button, your new descriptors are saved and their IDs are entered into the search bar. At that point, you can further revise them if you like.

The following screen recording shows the revise-before-upload feature in more detail:

Something Went Wrong

We're having trouble playing this video.

[Learn more](https://www.facebook.com/help/396404120401278/list)