platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/integrations

### RiskIQ's PassiveTotal

* [RiskIQ Integration Announcement](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.riskiq.com%2Fresources%2Fpress-releases%2Friskiq-makes-facebook-threatexchange-data-accessible-within-passivetotal&h=AT3F7GP1xvnkvm1WE0yswfRhLWBO2YwV29Y1GavPn1UkVcTaQiKYy7YGuedOWeMLu2nyl7X-8Mj_Uv6nEVRp8l14vkfqcJO950JEVhzsQVsjDqJC1QYCKjGl8DE2RK8pV7tYPCKP43H-gSxG)
    

### Demisto

* [Demisto Partner Integrations](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.demisto.com%2Fpartners%2F&h=AT0xB4FXflIicaJK4_uCgWUgW4uIM9JyVeH971C6Jv65-wPFYz_T8LB8CEIwc58gO8VH0hEyzahRaNNq25Wj6W34sXcvjuwMIilr1Sx_vDqTC3d3V8oB1HrHcu-iscnXH4Ch95MGF74595gv3SRa3FO8Vom0ew)
    

### ThreatStream

* [ThreatStream 'Facebook ThreatExchange' Trusted Circle](https://l.facebook.com/l.php?u=https%3A%2F%2Fui.threatstream.com%2Fsearch%3Ftrustedcircles%3D10023&h=AT0abKAWOLF4vrcCu-pArncfliCnGxk6XC0dvts-qa2y-kiONCDmpgE7925sBaEUoq8JtX0cAlQ8j_ETvqmJrpgVvbE4ETm3r0qHMw0KiV2LcUWr9wsQqGhx_OIrX6WgKUgKjxTDMhqBbOt8)