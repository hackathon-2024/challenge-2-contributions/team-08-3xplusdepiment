platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-access


## Adding Users to an ThreatExchange Account

If your company already has a business account, here are the steps for adding users:

1) Users must do the following before being added to the account:

* a) create their own Facebook account if they don't already have one.
* b) register as a Facebook developer by following [these steps](https://developers.facebook.com/docs/development/register/).

2) There are two options to be added to a ThreatExchange app account. One uses "business accounts", and the other is for an admin to directly add a personal account to the application. Using the business account allows you to separate your work and personal usage more cleanly. Learn more about adding people to a business account [here](https://www.facebook.com/business/help/2169003770027706). From [business.facebook.com/settings/people/](https://business.facebook.com/settings/people/) for your business, you can invite people to your business.

3) Using the person's work email, invite them to your business account. You don't need to give them any permissions at this point.

As of July 2023, this is what the "Add Person" flow looks like:

You'll be leaving all the defaults and not assigning any permissions at this stage.

4) After the person has accepted the invite, you can then assign them to the ThreatExchange Application:

You'll need to assign at least at the "Develop App" level, but "Manage App" can also simplify fixing settings.

  
  

Next, learn about [Best Practices for Using ThreatExchange](https://developers.facebook.com/docs/threat-exchange/best-practices).