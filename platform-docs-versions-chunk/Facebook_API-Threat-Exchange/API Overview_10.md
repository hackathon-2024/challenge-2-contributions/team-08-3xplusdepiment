platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-started

## What is the Cost of Integration?

Partners who have onboarded have reported the process takes take 1-2 weeks of engineering time to get a basic integration plus another 1-2 weeks for fully automated ingestion and contribution. The cost will vary by company and will depend on a number of factors including the maturity of internal systems and the number of signal types you are attempting to use.

Some questions that might be useful in determining how long it will take your company to integrate are:

* Which of the above signal types are you planning to integrate with? (Text tends to be quick, photos moderate)
* Can you currently search your platform for matches of those signal types? (You can likely piggyback on existing infrastructure, saving time)