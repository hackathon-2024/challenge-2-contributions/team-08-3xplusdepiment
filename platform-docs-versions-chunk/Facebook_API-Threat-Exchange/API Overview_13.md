platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/getting-access

## Creating the Application and Business Verification

To get access to Threat Exchange, you will need to have a business verified Application. We recommend creating a dedicated app to handle your ThreatExchange integration, with a name like "YourCompany ThreatExchange".

* To create a new Business app, follow the instructions [here](https://developers.facebook.com/docs/development/create-an-app/). Make sure to select type "Business" during creation.
* To get your business verified, follow the instructions [here](https://developers.facebook.com/docs/development/release/business-verification).

### Terms of Use

Please see the [Terms and Conditions](https://www.facebook.com/legal/threatexchange_terms)—depending on your company, you may want to obtain sign-off from your company's legal team.