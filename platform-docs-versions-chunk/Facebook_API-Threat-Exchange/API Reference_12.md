platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator

## Connections

| Name | Description | Type |
| --- | --- | --- |
| `descriptors` | Subjective opinions about the indicator | [`ThreatDescriptor`](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptor) |
| `related` | Other threat indicators that have been associated | [`ThreatIndicator`](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator) |