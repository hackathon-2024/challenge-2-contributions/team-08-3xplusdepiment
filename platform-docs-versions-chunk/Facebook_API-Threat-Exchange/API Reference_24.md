platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/indicator-type

# IndicatorType

The kind of indicator being described by a [ThreatIndicator](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicator) object. Despite the name IndicatorType, these values can also be used as values for [ThreatDescriptor's](https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-descriptor)`type` field.

.