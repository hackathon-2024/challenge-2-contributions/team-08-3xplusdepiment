platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/confidence-type

# Confidence

The `confidence` field describes the confidence in a particular piece of subjective data on ThreatExchange.

## Values

The `confidence` field can be any number between 1 and 100. When uploaded, the field must be specified numerically. However, you should use the following guidelines when uploading data: low confidence = 25, medium confidence = 50, high confidence = 75.