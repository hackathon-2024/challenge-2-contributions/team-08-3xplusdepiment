platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/reacting

# React to Existing Data

You can express a structured opinion on data you see on ThreatExchange by **reacting** to that data. This is a fully optional feature that can be used to provide more context or transparency about your ThreatExchange usage.

.

In general, `SAW_THIS_TOO`, `NON_MALICIOUS`, and `DISAGREE_WITH_TAGS` have well-undestood meaning, and are valuable contributions to any dataset. The rest are sometimes used as part of PrivacyGroup-specific conventions, or to provide a high level of transparency into your own usage of ThreatExchange data.