platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/share-level-type


## Values

| Name | Description |
| --- | --- |
| `RED` | Developer may permit access to view Threat Data it receives that is labeled with share level `RED` solely to those employees of Developer who have a strict need to know for the Purpose. Threat Data labeled with share level `RED` must not be reproduced, retransmitted, or otherwise re-distributed within Developer's organization or to any other party, including but not limited to Developer's affiliates, customers, partners or any other party, in each case, without the prior written consent of the original publisher. |
| `AMBER` | Developer may share Threat Data it receives that is labeled with share level `AMBER` solely to Developer and its subsidiaries who have a need to know for the Purpose (as that term is defined in the ThreatExchange Program Terms & Conditions), and solely as widely within Developer's organization(s) as is reasonably necessary for Developer to act on that information. |
| `GREEN` | Developer may share Threat Data it receives that is labeled with share level `GREEN` via a non-publicly accessible channel, solely to Developer's peer and partner organizations, preferred vendors, customers, and/or other entities who would find it useful as part of their existing business relationship with Developer, provided in each instance that the third party with whom Developer shares Threat Data has agreed in writing to keep all Threat Data confidential and not disclose Threat Data to any third party. |
| `WHITE` | Developer may share Threat Data it receives that is labeled with share level `WHITE` without restriction, subject to any attribution requirements specified by the original publisher(s). |