platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/reference/ui/members

# ThreatExchange UI Members Tab

The members tab shows the available list of participating members. This is an informative table; you can not change the state of member information from this tab.

You can sort members ascending or descending by id, name, and email by clicking on the table header for the corresponding field.

You can filter the member results based on the name of the member by using the text box above the table.

You can also download all query results by clicking the download query results drop-down box. Select either JSON or CSV to download the results in the corresponding format. Note that it will download the list of all members even if you have filtered by name.