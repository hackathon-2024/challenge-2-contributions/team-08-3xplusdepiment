platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/reaction-type

# ReactionType

A possible reaction to a visible piece of ThreatExchange information.

For more info, see the [Reacting To Existing Data](https://developers.facebook.com/docs/threat-exchange/reference/reacting/v2.8) documentation