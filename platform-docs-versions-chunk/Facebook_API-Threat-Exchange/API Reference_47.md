platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/apis/threat-indicators

# /threat\_indicators

This API call enables searching for indicators of compromise stored in ThreatExchange. With this call you can search for indicators by free text, type, or all in a specific time window. Combinations of these query types are also allowed.