platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/reference/ui/collaborations

# ThreatExchange UI Collaborations Tab

The **Collaborations** tab allows you to see statistics about and download data from collaborations in which you participate.

Things you can do on this tab:

* See all the collaborations in which you participate.
    
* Select a collaboration to show the number of indicators of each type that are currently a part of the collaboration's dataset.
    
* Select **Download Sample Dataset** to download indicators for one or all indicator types (CSV format).