platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/api


## Python/Ruby/Java/Curl wrappers

The above snippets showed you some examples of the bare REST API. For an easier path to integration please see [our Python wrapper](https://l.facebook.com/l.php?u=https%3A%2F%2Fgithub.com%2Ffacebook%2FThreatExchange%2Fblob%2Fmaster%2Fpython-threatexchange%2FREADME.md&h=AT0jk770feJ0r-RTGamjatv0K3tKdbs1g5_tWOJUaomxycr2KabfltG4dSC_DXYG4GjNMhK4i0TOT27dT-cgP5EusjIQPxcopVSsTjUJz5kDi6aajfaVvHYABz3arEAmHqu2r-kQ0DPD2PHk).

Please also see our descriptor-focused reference designs in [Ruby](https://l.facebook.com/l.php?u=https%3A%2F%2Fgithub.com%2Ffacebook%2FThreatExchange%2Ftree%2Fmaster%2Fapi-reference-examples%2Fruby%2Fte-tag-query%2FREADME.md&h=AT1skcv6YPQQkMZL91_37PNcQ8WK-2zaG94ogyO64OkxZLTMFQFOFV65vSFQkH2_rvbf_9aKXOU9_mKoifm8DgGOiQzLCFlQFKOKupqNR_VoLCB-btVMdvP_tYmLopYy212f776kp64QoytM), [Java](https://l.facebook.com/l.php?u=https%3A%2F%2Fgithub.com%2Ffacebook%2FThreatExchange%2Ftree%2Fmaster%2Fapi-reference-examples%2Fjava%2Fte-tag-query%2FREADME.md&h=AT3QfjT162GBj30ctsPhh3GlMM9XiJ-jItAinzOMY5g5s2TpLdw8bYpnkshMuv8lvnYboWQdAaIE9ACx6_8sxllSW_JFKnWLC5O7PaMFoyLkWi25c-SLXYkxHyPyr7-ZI4KSTRIFYrKIdPbR), and [bare-curl](https://l.facebook.com/l.php?u=https%3A%2F%2Fgithub.com%2Ffacebook%2FThreatExchange%2Ftree%2Fmaster%2Fapi-reference-examples%2Fcurl%2Fte-tag-query%2FREADME.md&h=AT0-S4dyeJLPdpz_GGpfWZI38--TPeqwGTru3e89DCK7BCAoPXLjv-os_JG_0HwaTcIRmRVgNC6DOmbqTRgjXAQL_msPTdI2eO2ENlLHRsYN2HTAOCi5etVYkJCHDqMtBCMiN1t32KtsKVre).