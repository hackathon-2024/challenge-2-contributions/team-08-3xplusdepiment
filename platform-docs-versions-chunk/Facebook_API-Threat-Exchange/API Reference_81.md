platform: Facebook
topic: API-Threat-Exchange
subtopic: API Reference
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Reference.md
url: https://developers.facebook.com/docs/threat-exchange/reference/reacting

## Bulk React Using the UI

You can update reactions for several descriptors at once.

2. Do any search; a search by tag.

  

The **Bulk react** button applies to all checkboxed rows, where your app doesn't own.

  

10. Select reactions to add to all rows or remove from all rows.

  

15. Click **OK** to commit:

  

20. Select **View** on any of the affected rows, where you can view the reaction.