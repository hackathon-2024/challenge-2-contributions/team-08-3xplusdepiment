platform: Facebook
topic: API-Threat-Exchange
subtopic: API Overview
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Facebook_API-Threat-Exchange/API Overview.md
url: https://developers.facebook.com/docs/threat-exchange/reference/ui/descriptors

## Saved searches

Given any search, simply poke the "Copy search URL to clipboard" button.

  

Then you can paste the URL into a message to share with coworkers, or what have you. Additionally, you can paste the URL into the browser bar and bookmark it:

  

You can even make a bookmark folder for your favorite searches:

## Editing

When you click Edit on a descriptor your app owns, you are able to mutate all editable attributes, with tooltips to provide context. (ID, indicator text, and indicator type are immutable after a particular descriptor is created.) Note: reactions and connections are not exposed yet in the UI -- see [status here](https://developers.facebook.com/docs/threat-exchange/ui#status).

## Downloading as CSV/JSON

You can download query results as CSV or JSON:

If you like, you can edit the CSV/JSON outside of the ThreatExchange UI and then re-upload the modified file to the ThreatExchange UI as one way of updating your data.