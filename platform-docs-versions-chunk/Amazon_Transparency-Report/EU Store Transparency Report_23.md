platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Safeguards applied to automation

To safeguard against potential errors made by our automated tools, we implement processes to ensure

that we have a high confidence rate that our automated tools operate as intended and to minimise

mistakes. We do this by ensuring our automated tools meet a high bar of accuracy before they arelaunched by testing the provision of the control, and by continuously auditing our automated tools

after they launch and removing from use automation that does not maintain a sufficiently high level

of accuracy. We also constantly improve our automated tools by training them using new information,including internal learnings and developments (including outcomes of expert manual decisions) andexternal risk signals, so they can learn and constantly get better at proactively identifying and blockingnon-compliant products automatically.