platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Our voluntary controls use advanced machine learning techniques and automation to monitor different

aspects of our store for potentially fraudulent, infringing, inauthentic, non-compliant or unsafe products

or content to maintain a trustworthy shopping experience. Our automated detection tools operate

continuously throughout every step of selling in our store, starting from when a prospective seller beginstheir registration process to listing or updating a product, changing key account information, receiving afunds disbursement, and more. In most cases, bad actors are stopped from even creating an account orlisting a single product for sale, and prohibited content is stopped before a customer ever sees it.



Our robust seller verification coupled with our efforts to hold bad actors accountable

are working. The number of bad actor attempts to create new selling accounts

decreased from 6 million attempts in 2020, to 2.5 million attempts in 2021, to800,000 attempts in 2022.

Robust, upfront vetting