platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Advertising

We proactively detect and remove advertising content that violates our Ad Policies, which are designed tomaintain a high customer experience bar for ads on the store. We require all advertising content to complywith all applicable laws, rules, and regulations; to be appropriate for a general audience, and honest about

the products or services that ad promotes. For example, we prohibit deceptive, misleading or offensiveads, as well as certain sexual, violent or offensive content.