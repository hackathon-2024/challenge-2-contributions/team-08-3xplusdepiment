platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

We also responded to 8,863 legal requests from EU Member States’ authorities for information about

users of our service in the legally mandated time-frames.



Legal requests from EU Member States’ authorities

Holding bad actorsaccountable



Member state country \# of Requests

Belgium 86

France 817

Germany 4,513

Italy 1,101

Luxembourg 9

Netherlands 68

Poland 38

Spain 2,206

Sweden 25

All others 0

20