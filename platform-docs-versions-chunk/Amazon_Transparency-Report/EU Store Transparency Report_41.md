platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Protecting our borders

Amazon also wants to see greater information-sharing to stop counterfeits at the borders. We continueto expand our work with customs agencies to mutually exchange information on counterfeit activity. We

can aid customs agencies in their detection, search and seizure efforts, and strengthen law enforcement’s

ability to dismantle criminal networks behind these illicit goods. Customs agencies can work with usto not only stop the shipments they seize, but to also help freeze other assets and inventory fromcounterfeiters that we may know about.