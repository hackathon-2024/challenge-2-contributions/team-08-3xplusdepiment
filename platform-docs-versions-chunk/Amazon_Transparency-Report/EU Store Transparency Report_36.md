platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

German law enforcement acted on intelligence from Amazon against ninesuspected members of a German-based counterfeit printer ink and toner ringthat attempted to deceive customers by selling fake toner cartridges that weremarketed as genuine products.



Amazon and Salvatore Ferragamo jointly filed two lawsuits against four individuals

(the “defendants”) and three entities for counterfeiting Ferragamo’s products. The

defendants attempted to offer the infringing products in Amazon’s store, violating

Amazon’s policies, Ferragamo’s intellectual property rights, and the law.



Amazon has already filed multiple litigations against fake review brokers across the

EU Member states. In Germany, for example, this litigation led to the fake reviewbrokers 100 Rabatt and Nice Rebate being shut down.



Holding bad actors accountable in the EU

21