platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Complaint resolution

In the first half of 2023, we received 156,369 complaints. 2,868 complaints were because the user

disagreed with our decision not to take action on a notice of alleged illegal content. 153,501 complaints

were because they disagreed with the specific action we took in response to the user uploaded content.Of the 810,170 actions we took on valid notices we received, we later reversed 41,167 of our originaldecisions due to complaints. Our median time to resolve a complaint was one day.



Out-of-court disputes

If sellers remain dissatisfied with an Amazon decision after reaching out to our support teams, they can

seek resolution for most disputes through an independent mediation process, facilitated by the Centre for

Effective Dispute Resolution. This redress mechanism enhances Amazon’s ability to appropriately protect

sellers’ interests and expression. We also have organised teams dedicated to ensuring that we hear andaddress selling partner pain points.