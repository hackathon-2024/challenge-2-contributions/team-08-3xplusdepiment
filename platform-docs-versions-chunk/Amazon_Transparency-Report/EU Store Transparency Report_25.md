platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Linguistic expertise and training

Although most of our expert investigators are able to make content moderation decisions without

specific linguistic expertise, we do have investigator teams with working proficiency in the nationallanguage of the Amazon EU store that they support, specifically German, French, Spanish, Italian, Dutch,Polish, and Swedish, in addition to English. Those with proficiency in national EU store languages assistwith implementing language-based automated controls and machine-translation technology, defining

local store requirements and policies, auditing corresponding decisions, and interacting with MemberState authorities.