platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Regulatory contacts

If there is an instance where a bad actor or content has evaded our proactive controls or notice systemsand we receive a contact from a regulator, we move quickly to respond and resolve the issue. In the

first half of 2023, we received 1,081 contacts from EU Member States’ authorities. The median time

to inform an authority we received their contact was less than one day, and the median time to resolve

the issue they surfaced was two days. Of those contacts, 1,081 were related to product and we had nocontacts related to App, Audio, Image, Synthetic Media, Text, Video, or Other.



Regulatory contacts received by EU Member State



Member state country \# of Contacts

Austria 9

Belgium 6

France 56

Germany 754

Ireland 82

Italy 24

Luxembourg 18

Netherlands 4

Poland 6

Spain 102

Sweden 20

All others 0

18