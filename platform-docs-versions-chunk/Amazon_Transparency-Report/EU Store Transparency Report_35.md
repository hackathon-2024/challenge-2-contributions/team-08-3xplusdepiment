platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>





Disrupting counterfeit networks across the globe

We continue to work with brands and law enforcement to hold more counterfeiters accountable, to deterthese criminals from abusing our store, and to stop them from selling counterfeits anywhere. Amazon’sCounterfeit Crimes Unit (CCU) works with brands, customs agencies, and law enforcement to track downcounterfeiters, shut down bad actors’ accounts, seize counterfeit inventory, and prosecute those involved.CCU has disrupted counterfeiters and their networks through civil suits, joint enforcement actions, and

seizures with law enforcement worldwide. When Amazon identifies an issue, we act quickly to protect

customers, brands, and our store, including removing the problematic content or listing and, whereappropriate, blocking accounts, withholding funds, quarantining physical inventory, or referring bad actorsto law enforcement.

In 2022, Amazon’s CCU sued or referred for criminal investigation over 1,300 counterfeiters globally. We

also work to find the factories and the warehouses where these goods are created or stored, and getthem shut down. In 2022, we identified, seized, and appropriately disposed of over 6 million counterfeits,

preventing them from being resold anywhere in the supply chain.