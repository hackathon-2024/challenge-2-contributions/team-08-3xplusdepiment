platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

In partnership with brands and law enforcement, we have been able to hold more bad actors accountablethrough civil litigation and criminal referrals to law enforcement organisations—working to stop them

from abusing our and other retailers’ stores across the industry in the future. Our efforts to identify and

help dismantle counterfeit organisations, fake reviews brokers, and other fraudsters are still early but are

working. We are proud of our efforts so far and how they have helped ensure that far more criminals are

held accountable, but we also believe that there is far more for industry and government to do in holdingbad actors accountable.

We also know honest users sometimes make mistakes. However, we have no tolerance for intentional and

repeated abuse of our systems and we take necessary action to stop abuse in our store. In the first half of

2023, Amazon took enforcement action against 15,774 users of our service for publishing illegal content.