platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

In the first half of 2023, EU selling partners submitted three out-of-court disputes. In these three

mediations, the independent mediator issued three settlement outcomes favourable to Amazon andthere were no recommendations to implement. The median time for a mediator to complete settlement

procedures was 56 days, which is the time from when the mediator notifies Amazon of the dispute to

when the mediator shares their recommendation with us.

19