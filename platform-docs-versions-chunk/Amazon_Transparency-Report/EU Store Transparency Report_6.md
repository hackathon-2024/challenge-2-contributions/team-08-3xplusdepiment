platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Seller verification

Amazon uses advanced technology and expert human reviewers to verify the identities of potentialsellers. Prospective sellers are required to provide a variety of information, such as government-issuedphoto IDs, taxpayer details, and banking information. In addition to verifying these, Amazon’s systemsalso analyse numerous data points including behaviour signals to detect and prevent risks, includingconnections to previously detected bad actors. To ensure authenticity of the individual identity, we also

employ live verification methods, like video-based verification or in-person appointments, ensuring that

it’s straightforward for honest small businesses to start selling while making it challenging for maliciousactors to create new selling accounts..

7