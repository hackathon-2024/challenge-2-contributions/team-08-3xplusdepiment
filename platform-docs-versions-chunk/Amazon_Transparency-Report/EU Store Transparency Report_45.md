platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>





Amazon is known for its customer obsession, and an essential part of that is earning and maintaining our

customers’ and selling partners’ trust. Inherent to that – we do not sacrifice customer safety or long-term

customer trust for short-term gain. It is the reason we invest far above and beyond our legal obligationsto ensure a trustworthy shopping and selling experience.

We recognise that our job of protecting our customers, brands, and selling partners is never done—inthis area, as with the rest of Amazon, we always perceive that it is Day 1 and that we must continue toinnovate and get even better than where we are today.

In addition to our robust and proactive controls, we are continuously creating new tools and advancingour technology to detect bad actors and illegal content and stop it from being found in our store. We

head off fraud, counterfeiting, and inappropriate content before customers ever see it our store. In

cases where we have missed something and it is reported to us, we swiftly remove the content, hold

bad actors accountable, and use such incidents to inform our prevention and monitoring efforts going

forward.

We will continue to innovate and join forces with industry and governments to improve outcomes for

consumers. We will continue to post updates to our ongoing efforts via this report every six months,

including those areas required for reporting by the DSA.