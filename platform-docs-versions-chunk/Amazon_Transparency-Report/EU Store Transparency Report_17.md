platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Innovative tools

11



Our Seller University helps European selling partners learn and master Amazon’s tools and grow theirbusinesses by offering courses on hundreds of topics, including how to start selling on Amazon, howFulfilment by Amazon (FBA) works, and advertising tips for brand owners.



The Amazon EU store also prompts sellers to provide relevant product safety and compliance materials,including product compliance warnings and markings on product pages and high-quality six-sided

images of their products and packaging. Often and where available, we leverage APIs and publicresources to help make compliance easy and reliable. For example, sellers can display energy efficiency

labelling by simply giving us their European Product Registry for Energy Labelling ID information.