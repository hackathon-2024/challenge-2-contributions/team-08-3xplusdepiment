platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

This is the first of the Amazon EU Store bi-annual Transparency Reports that will also cover

requirements as part of the EU Digital Services Act (DSA). This report sets out how Amazon hasinvested in ensuring a trustworthy shopping experience and continues to raise the bar in keeping ourEU store safe for customers, selling partners, brands, and advertisers and includes data from Januarythrough June of 2023.



Globally in 2022, we invested more than $1.2 billion and employed over 15,000people—including machine learning scientists, software developers, and expertinvestigators—dedicated to protecting customers, brands, selling partners, and ourstore from counterfeit, fraud, and other forms of abuse.



Investments in people and technology

4