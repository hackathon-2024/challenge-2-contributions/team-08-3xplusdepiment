platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

When our automated tools identify prohibited content with a high degree of confidence, they

automatically take enforcement action. We use the data and learnings gathered from these technologiesand valid notices of infringement or illegal content to innovate and improve our controls.



In the first half of 2023, 73MM of our voluntary actions were fully automated. 97% of our fully automated

voluntary actions were accurate.

14