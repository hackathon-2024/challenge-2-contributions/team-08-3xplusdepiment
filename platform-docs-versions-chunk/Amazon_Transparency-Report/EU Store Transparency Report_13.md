platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Voluntary content moderation

In the first half of 2023, we took 274MM actions on our own initiative, which include actions taken

through the proactive content moderation tools we have built to remove content from our EU store, aswell as those related to policy violations or other types of non-illegal content.



Number of actions taken on our own initiative by type of restriction



Type of restriction \# of Actions

Remove content 84.2MM

Disable access to content 133.6MM

Suspend monetary payments 313K

Partially suspend provision of the service 51.5MM

Totally suspend provision of the service 385K

Suspend the account 4.2MM

Make another restriction 258K

All others 0



Number of actions taken on our own initiative by type of content



Related to \# of Actions

Product 219.8MM

Multimedia (including Image, Text, and Video) 54.6MM

All others 0

10