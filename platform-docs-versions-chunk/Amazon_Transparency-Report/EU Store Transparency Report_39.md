platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

We know that we can be more effective by working together across the private and public sector.

We regularly engage with other interested parties from industry participants, consumer protectionorganisations, governments and regulators, academia, and others that share our desire to workcollaboratively to protect consumers and small businesses. We have launched private sector information-sharing agreements and participated in voluntary product safety pledges with governments all overthe world, and continue to seek out other opportunities to partner more closely with other industrymembers and governments where it can drive positive, substantive impact.



Private sector information-sharing

We believe that there should be more private sector information-sharing. As we laid out in our 2021blueprint for stopping counterfeiters and our 2023 blueprint for stopping fake reviews, we think it’scritical that private and public sector partnership includes greater sharing of information.