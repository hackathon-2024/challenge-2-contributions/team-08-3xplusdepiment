platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Product safety and compliance

Our content moderation systems aimed at product compliance include controls that function through

automated rules to identify and remove non-compliant products. We employ thousands of keyword-based algorithms and machine learning models that are continuously run against the EU store’s product

catalogue, considering linguistic differences and local compliance requirements by EU storefront location,

to identify potential policy violations. These controls aim to prevent non-compliant products from being

listed or flag them for Amazon’s expert investigators so listings can be stopped if compliance issues are

found or additional information is needed from sellers.