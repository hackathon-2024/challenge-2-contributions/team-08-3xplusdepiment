platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

When we innovate to improve the experience of selling on Amazon, we start by listening to our selling

partners. Our selling partner insights programmes seek feedback on our features and processes by

polling selling partners when they log in to their selling accounts, sharing ad-hoc surveys, and hostinginteractive workshops with our teams. Selling partners can contact us in a variety of ways, including by

email, phone, and chat, and we also analyse selling partner contacts to detect and fix the drivers of these

issues and improve our help content and processes.