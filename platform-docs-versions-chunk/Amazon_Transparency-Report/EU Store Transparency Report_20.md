platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

For small businesses that are just getting started and are looking for help in obtaining and protectingtheir intellectual property, our IP Accelerator programme connects these businesses with a vetted

network of trusted IP law firms in 39 different countries and 13 different languages, offering high-

quality, trusted trademark registration services at pre-negotiated competitive rates for these smallbusinesses.

We also prevent counterfeits from reaching customers through Transparency, a product serialisationservice that uses codes unique to every individual manufactured unit of a product to identify thoseindividual units. These codes can be scanned throughout the supply chain and by customers to verifyauthenticity using the Amazon Shopping App or Transparency App, regardless of where the products

were purchased. Amazon verifies these codes to ensure that only authentic units are shipped to

customers, virtually eliminating counterfeits for these products.

13