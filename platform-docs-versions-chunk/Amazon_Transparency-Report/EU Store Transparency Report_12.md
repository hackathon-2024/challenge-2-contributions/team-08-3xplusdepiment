platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Offensive and controversial products

Amazon prohibits the sale of products and books that promote, incite, or glorify hatred, violence, racial,sexual, or religious discrimination or promote organisations with such views; contain pornography, glorifyrape or paedophilia or promote the abuse or sexual exploitation of children; or graphically portray violence

or victims of violence, and advocate terrorism; among other material deemed inappropriate or offensive.We leverage machine learning and automation to filter listing submissions that we suspect of potential

policy violation, and then our content moderation teams manually review these suspect listings. We use

machine learning and manual review to filter potentially policy-violating listings.

9