platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

We invest heavily in people and technology to protect customers, brands, advertisers, and the EU storefrom fraud and other forms of abuse. Amazon deploys a number of measures to ensure compliancewith our Ad Policies and detect infringing ads, including through automated moderation tools thatcheck millions of ads and their visible ad elements per day worldwide (including advertiser-suppliedimages, product listing titles and images, and product descriptions). For example, we implement deny

lists on certain products that block all ads for customers who search for specific query terms e.g. “guns”.Ad Policies also block specific listings for being viable for advertising. To complement our automated

measures, expert teams also conduct human reviews of ads to identify any potential non-compliance andapply the learnings as feedback to continually improve our automated moderation tools.