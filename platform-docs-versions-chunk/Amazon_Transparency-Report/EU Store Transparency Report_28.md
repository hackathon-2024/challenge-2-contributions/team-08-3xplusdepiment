platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Anti-abuse measures

We take the notices and reports we receive from users seriously. Misuse of our systems can negativelyimpact customers and sellers, and we have measures in place to identify and take action when thesesystems are abused. We investigate and take action against submitters who we suspect or have been

found to submit false reports. In the first half of 2023, we took enforcement action against 1,841 users

of our service for repeatedly submitting unfounded notices and complaints.