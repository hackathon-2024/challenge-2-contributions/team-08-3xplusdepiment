platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Partnering with law enforcement

Product safety pledges

Amazon has signed four product safety pledges across the world in the EU, Australia, Japan, andCanada. Each pledge commits signatories to meet certain standards like actioning on recalled product

notifications from governments efficiently or providing data to regulatory partners to help inform and

improve their processes and compliance laws. As a founding signatory of the EU Product Safety Pledge

in 2018, Amazon was pleased to continue our cooperation with the European Commission by signingan updated agreement—Product Safety Pledge+ in March 2023, which will go into effect in December.The original 2018 Product Safety Pledge was the first of its kind, demonstrating the value of bringingtogether key stakeholders and taking a pragmatic approach with clear benefits for consumers.