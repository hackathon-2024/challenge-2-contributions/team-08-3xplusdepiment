platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

The new Pledge+ features commitments beyond what is established in EU safety legislation,strengthening cooperation and dialogue between signatories and authorities to protect consumers. Inaddition, the pledge has served as the backdrop to a pilot project between consumer groups and thepledge’s signatories, meant to facilitate the exchange of information and timely coordinated action forthe takedown of unsafe products.



Amazon’s Director of EU Public Policy, James Waterworth (far left), with representatives from other companies at the signingceremony for the European Commission’s ‘Product Safety Pledge+’

25