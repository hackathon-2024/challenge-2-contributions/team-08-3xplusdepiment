platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Amazon employs machine learning scientists, data analysts, software developers, and expertinvestigators dedicated to protecting customers, brands, selling partners, and our store from illegalcontent, including counterfeit, fraud, and other forms of abuse. These employees help us drive bothautomated and expert manual content moderation.



Automated and expertcontent moderation



Leveraging automation to drive scaled impact

Our automated tools help us scale our protections and take action more quickly. They help us operate

at scale to prevent bad actors from registering an account, and to detect and remove listings or othercontent that violate our policies or the law. These automated tools range from text-based algorithms

that identify specific keywords to sophisticated image recognition and machine learning models. Onceour tools have identified potentially infringing or illegal content, we use a mixture of automated tools and

expert investigators to determine the appropriate enforcement action.