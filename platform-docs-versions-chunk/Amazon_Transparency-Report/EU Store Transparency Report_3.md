platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>





We are proud of the progress we have made in preventing content that is illegal or violates our terms

and conditions from being available in our store. This has required significant resources, innovation by

Amazon, and partnerships that we have built with rights owners, government agencies, law enforcement,IP organisations, and many others. We have established best practices that can be applied acrossthe retail industry globally—in our proactive controls, our innovative tools, and for how the privateand public sector can work together to provide consumers, small businesses, and selling partners atrustworthy shopping experience. While we believe we have made a great deal of progress, we continueto invest in improving our shopping and selling experience. We also believe that the industry still has along way to go. Amazon continues to be committed to investing, innovating, and being a great partner.

Founded in 1994, Amazon started as a retailer for books. In 2001, Amazon opened its store to third-

party sellers. We opened our first store in the European Union (EU) in 1998, in Germany. Over the

last 25 years, we’ve contributed to the growth of local communities and created jobs and economicopportunities in most of the EU Member States and in all kinds of locations, from isolated rural and

neglected post-industrial areas to city centres and campuses. Today, Amazon operates stores in Germany,

Italy, France, Spain, Netherlands, Sweden, Poland, and Belgium, and we employ people across manyother EU Member States. We directly employ more than 150,000 people in permanent roles across 21EU Member States, including more than 35,000 people in professional functions. We have corporate

offices in approximately 50 European cities, including 11 cities in Germany, five in France, five in Italy, and

two in Spain. We’ve also invested in 15 research and development centres in nine Member States, andwe operate more than 250 logistics centres across the EU. These resources help to service an estimated

181,368,208 average monthly users across the EU.