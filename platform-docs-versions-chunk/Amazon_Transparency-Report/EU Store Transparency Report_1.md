platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

1

Published October 2023



EU StoreTransparency Report

2



03

06

10

13

16

18

19

22

25



Introduction

Robust proactive controls

Innovative tools

Automated and expert content moderation

Notices and regulatory contacts

Complaint and dispute resolution

Holding bad actors accountable

Collaborating across partners and industry

Conclusion



Table of contents

3



Introduction



Nearly three decades ago, Amazon set out to be Earth’s most customer-centric company, wherepeople can discover and purchase the widest possible selection of safe and authentic goods. As part ofthat mission, we obsess over earning and maintaining trust by ensuring that we provide a trustworthy

shopping experience. We believe that customer trust is difficult to earn and easy to lose. We invest

heavily in people and technology to protect customers, selling partners, brands, and advertisers fromany form of fraud or abuse.