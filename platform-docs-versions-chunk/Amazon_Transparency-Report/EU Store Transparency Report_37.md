platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Taking action against fake reviews brokers

Our goal is to ensure that every review in Amazon’s store is trustworthy and reflects customers’ actual

experiences. For that reason, Amazon welcomes authentic reviews—whether positive or negative—butstrictly prohibits fake reviews that intentionally mislead customers by providing information that is notimpartial, authentic, or intended for that product or service. Amazon has been pursuing legal actionsagainst fake reviews brokers to combat the root cause of fake reviews in the retail industry. Amazon haswon dozens of injunctions, particularly in Europe, resulting in several paid-review companies being shutdown and halting their activities.