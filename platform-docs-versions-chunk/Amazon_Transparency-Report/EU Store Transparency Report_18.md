platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Brand protection tools

Amazon creates powerful tools for rights owners to protect their brands by partnering with us. Wework with a large and ever-growing number of brands, and because they know their products best,we work together so we can be even more effective in proactively stopping counterfeit, fraud, andother forms of abuse.

We launched Amazon Brand Registry, a free service for brands, whether they sell in our store or not.The service provides brands the ability to better manage and grow their brand with Amazon, andprotect their brand and intellectual property rights. Through the Report a Violation tool, brand ownerscan more easily search for, identify, and report infringements and subsequently track theirsubmissions within the dedicated Submission History dashboard. They also get access to many otherbrand protection and brand building features.

12