platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>





Expert manual reviews

All Amazon staff, including staff dedicated to content moderation, are required to meet Amazon’s

Leadership Principles. The Leadership Principles are a set of guidelines that Amazon employees use

every day to solve problems, evaluate trade-offs, and make decisions. There are 16 in total, and they

are the framework of how we evaluate potential candidates for jobs and set the expectations of

performance across Amazon. In addition, the level of qualification and expertise our content moderatorshave is diverse and varies depending on their specific job role. Most of our content moderators have abachelor’s degree in relevant fields of study, including computer science, information technology, datascience, information security, finance, foreign studies, intellectual property, and risk management. Allstaff dedicated to content moderation have demonstrated experience performing research on a variety

of topics, including fraud, abuse, trust, and risk; and have the ability to investigate complex and highlytechnical problems, and perform root cause analysis.

15