platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>





Partnering with law enforcement

In addition to the efforts mentioned earlier on stopping counterfeiters and fake reviews brokers where

Amazon partners closely with law enforcement, Amazon also shares information on potential suspiciouscustomer transactions and relevant data points with law enforcement agencies across Europe, such ascustomer information in accordance with the Explosive Precursor Regulation. For this purpose, Amazon

invests significant efforts into identifying and reporting transactions that may be suspicious whencombined with information that law enforcement may have. Amazon has classified several hundred

thousand products for which transactions are monitored, and complex combination purchases are

flagged for potentially being suspicious. All of the results are reviewed by risk managers, to ensure

correct reporting considering the impact an incorrect report might have on our customers and on the

general public. Our efforts in working with the authorities and law enforcement agencies is underlinedby the fact that we are actively participating in the EU Standing Committee on Precursors, the German

Arbeitskreis on Explosive Precursors, and are in close contact with the relevant national contact points. In

light of this participation, we have contributed to the Guidance Documents released by the Commissionon the identification and reporting of suspicious precursors.

24