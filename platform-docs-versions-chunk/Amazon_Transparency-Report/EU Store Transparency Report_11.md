platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>





Trustworthy reviews

Our moderation processes for community content include machine learning models that detect contentthat violates our Community Guidelines and prevent it from being published. We strictly prohibit fake

reviews that intentionally mislead customers by providing information that is not impartial, authentic,

or intended for that product or service. We invest significant resources to proactively stop fake reviews.

This includes machine learning models that detect risk, including relationships between accounts, sign-inactivity, review history, and other indications of unusual behaviour, as well as expert investigators that usesophisticated fraud-detection tools to analyse and prevent fake reviews from ever appearing in our store.

Our machine learning models analyse millions of reviews each week using thousands of data points to

detect risk. The review ranking algorithm considers signals from Amazon’s fraud-detection tools relatedto the authenticity of a review. When we strongly suspect that a review is inauthentic, we suppress thereview completely, so it is not displayed in the Amazon EU store.