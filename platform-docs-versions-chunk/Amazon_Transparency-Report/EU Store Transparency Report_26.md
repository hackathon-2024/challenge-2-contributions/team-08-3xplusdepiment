platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>





Our expert investigators dedicated to content moderation, including the administration of Amazon’s

notice and action mechanisms, complaints and appeals procedures, are trained to identify illegal content

and content that infringes our terms and conditions. Our investigators receive: an exhaustive onboarding

process to familiarise themselves with the underlying policies and standardised operating procedures,which must be completed before they are able to take their own moderation decisions; robust continuedon-the-job training and periodical knowledge tests, including on any new tools or processes; and whenneeded, support from subject matter coaches and escalation paths to team managers. This includestraining in the relevant subject matter to better protect our customers and, additionally, regular training

on our company’s policies, terms and conditions, and their specific area of expertise, whether that’s

product safety and compliance, IP and brand protection, controversial content, or misleading customerreviews. For example, investigators dedicated to evaluating IP infringement notices receive training

and support in accurately identifying different types of infringing listing content, including trademarks,

copyright, design and patents. Similarly, investigators creating product listing rules have detailedknowledge of Amazon’s catalogue and are trained to accurately develop and apply product listing rules.

16