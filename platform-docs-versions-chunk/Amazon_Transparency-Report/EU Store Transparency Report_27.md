platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Through the tools we have built, selling partners, brands, and customers can submit notices that alertAmazon when they think they have found illegal content in our store. When we receive a notice, we takeaction quickly to investigate and if accurate, remove content from our store.



Notices and regulatorycontacts



Article 16 notices

In the first half of 2023, our reporting mechanisms and tools received 417,836 notices. We resolved283,220 notices through automated processes.

We took 810,170 actions on valid notices. Our terms and conditions prohibit any illegal content beingoffered for sale and so all of our actions are taken because the information or content violated both ourpolicies and applicable law. The median time to take action on a notice and confirm our actions with the

submitter was less than one day.



Number of Article 16 notices received by type



Related to \# of Notices

Image 27,980

Product 389,856

All others 0

17