platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

In 2023, Amazon has already filed multiple litigations across Europe, including in Germany, Spain, Poland,Austria, and France. Our legal actions globally are driving positive results as we have shut down some of

the largest global brokers, including Matronex and Climbazon. By taking such action, Amazon targetsthe source of the problem. Because fake review brokers use third-party services like social media andencrypted third-party messaging services to facilitate their illicit schemes, Amazon investigates and

regularly reports abusive groups, deceptive influencers, and other bad actors to these third parties social

media and message services.

22



Collaborating across partnersand industry