platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Average monthly active users



Member state country Average monthly active users

Austria 5,698,882

Belgium 2,781,420

Bulgaria 82,082

Croatia 143,992

Cyprus 76,504

Czech Republic 167,353

Denmark 269,845

Estonia 63,989

Finland 180,653

France 34,617,763

Germany 60,390,505

Greece 171,397

Hungary 116,326

5



Ireland 1,802,267

Italy 38,121,014

Latvia 61,579

Lithuania 69,259

Luxembourg 408,565

Malta 76,491

Netherlands 4,589,643

Poland 2,452,715

Portugal 1,536,009

Romania 140,609

Slovakia 51,728

Slovenia 163,706

Spain 25,101,320

Sweden 2,032,592

6



Robust proactive controls