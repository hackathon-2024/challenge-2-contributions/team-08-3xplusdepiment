platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Automated brand protections

Amazon’s Intellectual Property Policy prohibits listings that violate rights owners’ intellectual property

rights. Amazon Brand Registry, a free service launched in 2017, enables brands to more effectively

protect their intellectual property, whether or not they sell on Amazon. Through Brand Registry, brandscan share IP and product data, which Amazon uses to prevent potential infringements. The purpose ofthese automated brand protections is to detect content that likely infringes the intellectual propertyrights of brands and other rights owners. For example, our brand protection tools use advanced machinelearning to scan keywords, text, and logos which are identical or similar to registered trademarks orcopyrighted work, in order to prevent attempted listing of counterfeit or infringing products.



Globally, Amazon’s automated technology scans over 8 billion daily attemptedchanges to product detail pages for signs of potential abuse.



Continuous monitoring

8