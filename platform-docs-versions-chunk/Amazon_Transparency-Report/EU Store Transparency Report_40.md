platform: Amazon
topic: Transparency-Report
subtopic: EU Store Transparency Report
file_path: /home/bhuang/nlp/rag-race-challenge2-2024/platform-docs-versions/Amazon_Transparency-Report/EU Store Transparency Report.md
url: <EMPTY>

Our membership in the Anti-Counterfeiting Exchange, which is an industry collaboration that startedin the US, is designed to make it more difficult for counterfeiters to move among different stores, andsafer for consumers to shop anywhere they choose. We are eager to see the same or similar effortsacross the globe, so we can all use this type of information in our ongoing efforts to detect and addresscounterfeiting, and we look forward to leveraging jurisdiction and region-specific best practices here

in the EU, such as those laid out in the IP Toolbox against Counterfeiting and the framework of the EUMemorandum of Understanding, to enable conversations and drive European industry-wide data-sharing on counterfeiters. Similarly, our recent announcement of a global Coalition for Trusted Reviewsis an industry collaboration across the US, EU, and other countries to, among other things, shareinformation on how fraudsters operate so we can make even greater progress in decreasing fraudulentreviews.

23