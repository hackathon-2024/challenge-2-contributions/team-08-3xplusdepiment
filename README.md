<!--
SPDX-FileCopyrightText: 2024 troisfoisplus_piment
SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: EUPL-1.2
-->

# Team Name : Trois Fois Plus de Piment

## Licence / License

:fr: Ce code a été produit pour le Challenge 2 du Hackathon 2024 co-organisé par le PEReN et la Commission Européenne : DSA RAG Race. Il est délivré comme récupéré à la fin de l'événement et peut donc ne pas être exécutable.
Sauf mention contraire, le code source est placé sous licence publique de l'Union Européenne, version 1.2 (EUPL-1.2).
Les données des plateformes présentes sur ce projet sont la propriété des plateformes et ne sont fournies qu'à titre d'illustration pour le bon fonctionnement du projet. Elles ne seront en aucun cas mises à jour. Les données complètes peuvent être trouvées vers ce lien : https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions

:gb: This code was developed for Challenge 2 of the Hackathon 2024 co-organized by the PEReN and the European Commission: DSA RAG Race. It is released as retrieved at the end of the event and may therefore not be executable.
Unless otherwise specified, the source code is licensed under the European Union Public License, version 1.2 (EUPL-1.2).
The data of platforms contained in this project are the property of the platforms and are provided for illustrative purposes only. They will not be updated under any circumstances. The complete data can be found at the following link: https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions


## Bibliothèques et modèles utilisés

Bibliothèques utilisées :
- pandas  
- transformers  
- [rank_bm25](https://github.com/dorianbrown/rank_bm25)  
- [llama_index.retrievers.bm25_retriever](https://docs.llamaindex.ai/en/stable/examples/retrievers/bm25_retriever.html)  
- [FlagEmbedding](https://huggingface.co/BAAI/bge-m3#usage) (utilisé avec le modèle `bge-m3`)  

Modèles mis par défaut dans le code :
- [BAAI/bge-m3](https://huggingface.co/BAAI/bge-m3) pour créer les embeddings.
- [mistralai/Mistral-7B-Instruct-v0.2](https://huggingface.co/mistralai/Mistral-7B-Instruct-v0.2) pour classer les chunks pertinents.
- [TheBloke/Mixtral-8x7B-Instruct-v0.1-AWQ](https://huggingface.co/TheBloke/Mixtral-8x7B-Instruct-v0.1-AWQ) pour le RAG.


## Commandes


```python
python run.py \
    --input-csv-file $input_csv_file \
    --input-document-dir $input_document_dir \
    --output-dir $output_dir \
    --embed-model-name-or-path $embed_model_name_or_path \
    --reranker-model-name-or-path $reranker_model_name_or_path \
    --llm-model-name-or-path $llm_model_name_or_path
```